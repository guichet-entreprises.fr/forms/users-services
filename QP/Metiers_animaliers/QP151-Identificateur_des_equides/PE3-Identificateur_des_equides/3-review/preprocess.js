var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp151PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp151PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp151PE3.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp151PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp151PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp151PE3.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp151PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp151PE3.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp151PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp151PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp151PE3.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp151PE3.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp151PE3.adresse.adresseContact.complementAdresseDeclarant : ' ')+' '+($qp151PE3.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp151PE3.adresse.adresseContact.codePostalAdresseDeclarant : ' ');
cerfaFields['villePays']       					= $qp151PE3.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp151PE3.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp151PE3.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp151PE3.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp151PE3.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp151PE3.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp151PE3.signature.signature.signature;
cerfaFields['lieuSignature']                  	= 'Fait à '+$qp151PE3.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp151PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp151PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp151PE3.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp151PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom

	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier LE v1.6 MEL1.pdf') //
	.apply({
		date: $qp151PE3.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Identificateur_equides_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Identificateur des équidés - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'identificateur des équidés est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

