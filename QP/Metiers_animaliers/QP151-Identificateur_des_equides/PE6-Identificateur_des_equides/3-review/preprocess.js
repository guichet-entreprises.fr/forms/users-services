var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp151PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp151PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp151PE6.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp151PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp151PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp151PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp151PE6.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp151PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp151PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp151PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp151PE6.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp151PE6.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp151PE6.adresse.adresseContact.complementAdresseDeclarant : ' ')+' '+($qp151PE6.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp151PE6.adresse.adresseContact.codePostalAdresseDeclarant : ' ');
cerfaFields['villePays']       				= $qp151PE6.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp151PE6.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp151PE6.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp151PE6.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp151PE6.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp151PE6.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp151PE6.signature.signature.signature;
cerfaFields['lieuSignature']                = 'Fait à '+$qp151PE6.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp151PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp151PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp151PE6.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp151PE6.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom

	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier LPS v1.6 MEL1.pdf') //
	.apply({
		date: $qp151PE6.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier LPS V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPremierDeclaration);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Identificateur_equides_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Identificateur des équidés - renouvellement de la demande de déclaration préalable en vue d\’une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de renouvellement de la demande de déclaration préalable en vue d\’une libre prestation de services pour l\'exercice de la profession d\'identificateur des équidés est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

