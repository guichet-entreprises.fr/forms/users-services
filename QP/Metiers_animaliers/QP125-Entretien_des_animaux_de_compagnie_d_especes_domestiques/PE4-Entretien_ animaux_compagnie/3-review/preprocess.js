var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp125PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp125PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp125PE4.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp125PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp125PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp125PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp125PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp125PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp125PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp125PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp125PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp125PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp125PE4.adresse.adresseContact.complementAdresseDeclarant : ' ')+' '+($qp125PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp125PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '');
cerfaFields['villePays']       					= $qp125PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp125PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp125PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp125PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp125PE4.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['departementExercice']				= $qp125PE4.adresse.departementExerciceGroup.departementExercice


//signature
cerfaFields['date']                				= $qp125PE4.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp125PE4.signature.signature.signature;
cerfaFields['lieuSignature']                  	= 'Fait à '+$qp125PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp125PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp125PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp125PE4.etatCivil.identificationDeclarant.prenomDeclarant;



if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp125PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp125PE4.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.listeEspecesAnimaux);
appendPj($attachmentPreprocess.attachmentPreprocess.adresseEtablissement);
appendPj($attachmentPreprocess.attachmentPreprocess.copieDeclarationActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.copieJustificationModeExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNonCondamnation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjRespectAnimaux);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjFormationsContinues);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('entretien_animaux_domestiques_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Entretien des animaux de compagnie d\'espèces domestiques - demande de reconnaissance de qualifications professionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour exercer la profession d\'entretien des animaux de compagnie d\'espèces domestiques est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

}

function buildSimpleDocument() {
	
var cerfa = pdf.create('models/courrier LE V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Entretien des animaux de compagnie d\'espèces domestiques-  .pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Entretien des animaux de compagnie d\'espèces domestiques - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier libre de demande d\'habilitation pour exercer la profession d\'entretien des animaux de compagnie d\'espèces domestiques',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
}