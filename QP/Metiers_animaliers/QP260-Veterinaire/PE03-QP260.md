**Procédure élémentaire 3** : Vétérinaire (QP260)

**Procédure** : Enregistrement de son diplôme de vétérinaire

**Contexte :** RE-LE-titulaires du diplôme d’Etat de docteur vétérinaire ou d’un
diplôme, certificat ou titre de vétérinaire souhaitant faire enregistrer son
diplôme avant de procéder à son inscription à l’ordre des vétérinaires

-   RE titulaire d’un diplôme, certificat ou titre délivré par un EM ou autre
    Etat partie à l’accord sur l’EEE figurant sur la liste de la directive
    2005/36/CE antérieur au 18 décembre 1980 ou celle prévue par la directive
    sanctionnant une formation commencée avant ces dates, accompagné d’une
    attestation délivrée par l’AC de l’EM certifiant que le RE a exercé
    légalement les activités de vétérinaire pendant au moins 3 années
    consécutives sur les 5 années précédant la délivrance de l’attestation

**Référence-id :** Formalités SCN/GQ/Métiers animaliers/Vétérinaire/Libre
établissement/RE titulaire d’un diplôme délivré avant 18 décembre 1980 ou avant
celle de la liste et attestation d’expérience professionnelle

**Formulaire** : modèle d’enregistrement de son diplôme

**Mode de transmission** : dossier à adresser en recommander avec AR ou par
courriel

**Destinataire** : Madame Christine HERLIN - 34 rue Breguet – 75011 Paris ou
christine.herlin\@veterinaire.fr 

Niveau : national

**Délai** :

**Pièces justificatives** :

-   Copie de la pièce d’identité.\<description\>Extrait d'acte de naissance,
    fiche d'état civil, carte d'identité ou passeport, recto verso si nécessaire
    et en cours de validité.\</description\>

-   Copie de chacun des diplômes, titres ou certificats invoqués ou de
    l’autorisation d’exercice et, le cas échéant de l’attestation de révision en
    cours de validité pour les qualifications soumises à l’obligation de
    recyclage.

-   Copie de toute pièce justifiant que le déclarant a exercé l'activité dans
    cet Etat pendant au moins 3 ans consécutifs au cours des 5 dernières années.

**Commentaires** :

-   Les documents doivent être traduits en français
