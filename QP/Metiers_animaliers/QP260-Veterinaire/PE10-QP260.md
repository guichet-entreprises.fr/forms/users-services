**Procédure élémentaire 10** : Vétérinaire (QP260)

**Procédure** : déclaration préalable de libre prestation de services

**Contexte :** RE-LPS

-   RE légalement établi dans un EM ou autre Etat partie à l’accord sur l’EEE
    souhaitant effectuer une prestation de services

**Référence-id :** Formalités SCN/GQ/Métiers animaliers/Vétérinaire/Libre
prestation de services

**Formulaire** : Dossier\_declaration\_LPS

**Mode de transmission** : dossier à adresser en recommander avec AR ou par
courriel

**Destinataire** : Conseil supérieur de l’Ordre des vétérinaires - 34 rue
Breguet – 75011 Paris ou <cso.paris@veterinaire.fr>

Niveau : national

**Délai **:

-   1 mois : accusé de réception de la demande par le Conseil supérieur de
    l’Ordre des vétérinaires

**Pièces justificatives** :

-   Copie de la pièce d’identité. \<description\>Extrait d'acte de naissance,
    fiche d'état civil, carte d'identité ou passeport, recto verso si nécessaire
    et en cours de validité.\</description\>

-   Copie de chacun des diplômes, titres ou certificats invoqués ou de
    l’autorisation d’exercice et, le cas échéant de l’attestation de révision en
    cours de validité pour les qualifications soumises à l’obligation de
    recyclage

-   Copie de l’attestation de l’autorité compétente de l’Etat d’établissement,
    certifiant que l’intéressé est légalement établi dans cet Etat et qu’il
    n’encourt aucune interdiction d’exercer lorsque l’attestation est délivrée.

-   Copie d'attestation d'assurance ou moyen de protection personnelle ou
    collective souscrits par le déclarant pour couvrir sa responsabilité
    professionnelle.

-   Copie de l'attestation prouvant la maitrise suffisante de la langue
    française. \<description\>Attestation de qualification délivrée à l’issue
    d’une formation en français, attestation de niveau en français délivrée par
    une institution spécialisée ou document attestant d’une expérience
    professionnelle acquise en France.\</description\>

**Commentaires** :

-   Les documents doivent être traduits en français
