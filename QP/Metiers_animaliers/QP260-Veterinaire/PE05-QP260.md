**Procédure élémentaire 5** : Vétérinaire (QP260)

**Procédure** : Enregistrement de son diplôme de vétérinaire

**Contexte :** RE-LE-titulaires du diplôme d’Etat de docteur vétérinaire ou d’un
diplôme, certificat ou titre de vétérinaire souhaitant faire enregistrer son
diplôme avant de procéder à son inscription à l’ordre des vétérinaires

-   Soit d'un diplôme, certificat ou titre de vétérinaire délivré par un Etat
    tiers, dès lors qu'il a été reconnu par un EM ou par un autre Etat partie à
    l'accord sur l'EEE

-   Et que son titulaire a acquis une expérience professionnelle de trois années
    au moins dans cet Etat, et attesté par celui-ci

**Référence-id :** Formalités SCN/GQ/Métiers animaliers/Vétérinaire/Libre
établissement/Diplôme d'un Etat tiers reconnu par l'EM/Attestation de diplôme et
d'expérience professionnelle

**Formulaire** : modèle enregistrement du diplôme

**Mode de transmission** : dossier à adresser en recommander avec AR ou par
courriel

**Destinataire** : Madame Christine HERLIN - 34 rue Breguet – 75011 Paris ou
christine.herlin\@veterinaire.fr 

Niveau : national 

**Délai** :

**Pièces justificatives** :

-   Copie de la pièce d’identité. \<description\>Extrait d'acte de naissance,
    fiche d'état civil, carte d'identité ou passeport, recto verso si nécessaire
    et en cours de validité.\</description\>

-   Copie de chacun des diplômes, titres ou certificats invoqués ou de
    l’autorisation d’exercice et, le cas échéant de l’attestation de révision en
    cours de validité pour les qualifications soumises à l’obligation de
    recyclage

-   Copie de l’attestation d’équivalence de diplôme délivrée par l'autorité
    compétente de l'Etat membre

-   Copie de toute pièce justifiant que le déclarant a exercé l'activité dans
    cet Etat pendant au moins 3 ans à temps complet ou une durée équivalente

**Commentaires** :

-   Les documents doivent être traduits en français
