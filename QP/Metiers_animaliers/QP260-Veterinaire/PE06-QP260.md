**Procédure élémentaire 6** : Vétérinaire (QP260)

**Procédure :** Enregistrement de son diplôme de vétérinaire

**Contexte :** RE-LE-titulaires du diplôme d’Etat de docteur vétérinaire ou d’un
diplôme, certificat ou titre de vétérinaire souhaitant faire enregistrer son
diplôme avant de procéder à son inscription à l’ordre des vétérinaires

-   RE titulaire d’un titre de formation délivré par l’Estonie ou dont la
    formation a commencé avant le 1er mai 2004

-   Et le RE justifie d’une expérience professionnelle de 5 années consécutives
    au cours des 7 dernières années

**Référence-id :** Formalités SCN/GQ/Métiers animaliers/Vétérinaire/Libre
établissement/RE titulaire d’un titre délivré par l'Estonie pour une formation
commencé avant le 1er mai 2004

**Formulaire :** modèle enregistrement du diplôme

**Mode de transmission :** dossier à adresser en recommander avec AR ou par
courriel

**Destinataire** : Madame Christine HERLIN - 34 rue Breguet – 75011 Paris ou
christine.herlin\@veterinaire.fr 

Niveau : national

**Délai** :

**Pièces justificatives** :

-   Copie de la pièce d’identité. \<description\>Extrait d'acte de naissance,
    fiche d'état civil, carte d'identité ou passeport, recto verso si nécessaire
    et en cours de validité.\</description\>

-   Copie de chacun des diplômes, titres ou certificats invoqués ou de
    l’autorisation d’exercice et, le cas échéant de l’attestation de révision en
    cours de validité pour les qualifications soumises à l’obligation de
    recyclage

-   Copie de toute pièce justifiant que le déclarant a exercé l'activité dans
    cet Etat pendant au moins 5 ans consécutifs au cours des sept dernières
    années.

**Commentaires** :

-   Les documents doivent être traduits en français
