var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp076PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp076PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp076PE5.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp076PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp076PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp076PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp076PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp076PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp076PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp076PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp076PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp076PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp076PE5.adresse.adresseContact.complementAdresseDeclarant : ' ')+' '+($qp076PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp076PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') ;
cerfaFields['villePays']       					= $qp076PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp076PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp076PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp076PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp076PE5.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['departementExercice']				= $qp076PE5.adresse.departementExerciceGroup.departementExercice


//signature
cerfaFields['date']                				= $qp076PE5.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp076PE5.signature.signature.signature;
cerfaFields['lieuSignature']                  	= 'Fait à '+$qp076PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp076PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp076PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp076PE5.etatCivil.identificationDeclarant.prenomDeclarant;


if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp076PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LPS.pdf') //
	.apply({
		date: $qp076PE5.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LPS V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('experimentations_animaux_demande_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Conception et réalisation d’expérimentations sur les animaux - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de déclaration préalable en vue d\'une libre prestation de services pour exercer dans la conception et réalisation d’expérimentations sur les animaux est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

}

function buildSimpleDocument() {

var cerfa = pdf.create('models/courrier libre LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('experimentations_animaux_demande_RQP.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Conception et réalisation d’expérimentations sur les animaux - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour exercer dans la conception et réalisation d’expérimentations sur les animaux',
              description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
}