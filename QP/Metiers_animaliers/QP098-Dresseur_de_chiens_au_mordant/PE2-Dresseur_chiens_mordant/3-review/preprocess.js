var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp098PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp098PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp098PE2.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']                = $qp098PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp098PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp098PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']               = $qp098PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp098PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                      = $qp098PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                    = $qp098PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                          = $qp098PE2.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp098PE2.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp098PE2.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                        = ($qp098PE2.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp098PE2.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp098PE2.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp098PE2.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']                  = $qp098PE2.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                    = $qp098PE2.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                         = $qp098PE2.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                             = $qp098PE2.signature.signature.dateSignature;
cerfaFields['signature']                        = $qp098PE2.signature.signature.signature;
cerfaFields['lieuSignature']                    = $qp098PE2.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = $qp098PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp098PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp098PE2.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Ajout du cerfa
 */
var finalDoc = nash.doc //
    .load('models/Dresseur_de_chiens_au_mordant_LE.pdf') //
    .apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclarationHonneur);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitreFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLicence);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Dresseur_de_chiens_au_mordant_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Dresseur de chiens au mordant - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de dresseur de chiens au mordant ',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
