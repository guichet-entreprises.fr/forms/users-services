var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp155PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp155PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp155PE2.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp155PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp155PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp155PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp155PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp155PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp155PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp155PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle

var regionExercice = $qp155PE2.adresse.regionExerciceGroup.regionExercice;

cerfaFields['adresse'] 							= $qp155PE2.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp155PE2.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp155PE2.adresse.adresseContact.complementAdresseDeclarant : ' ')+' '+($qp155PE2.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp155PE2.adresse.adresseContact.codePostalAdresseDeclarant  : ' ');
cerfaFields['villePays']       					= $qp155PE2.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp155PE2.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp155PE2.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp155PE2.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp155PE2.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['regionExercice']               = regionExercice;


//signature
cerfaFields['date']                				= $qp155PE2.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp155PE2.signature.signature.signature;
cerfaFields['lieuSignature']                  	= 'Fait à '+$qp155PE2.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp155PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp155PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp155PE2.etatCivil.identificationDeclarant.prenomDeclarant;



if (regionExercice == "Auvergne-Rhône-Alpes") {
	var adAC1 = "Préfecture du Rhone";
	var adAC2 = "69419 Lyon Cedex 3";
	} else if (regionExercice == "Bourgogne-Franche-Comté") {
	var adAC1 = "Préfecture de la région Bourgogne-Franche-Comté";
	var adAC2 = "53 rue de la Préfecture";
	var adAC3 = "21041 Dijon Cedex";
} else if (regionExercice == "Bretagne") {
	var adAC1 = "Préfecture de la région Bretagne";
	var adAC2 = "3, avenue de la Préfecture";
	var adAC3 = "35026 Rennes Cedex 9";
} else if (regionExercice == "Centre-Val de Loire") {
	var adAC1 = "Préfecture de la région Centre-Val de Loire";
	var adAC2 = "181 rue de Bourgogne";
	var adAC3 = "45042 Orléans cedex 1";
} else if (regionExercice == "Corse") {
	var adAC1 = "Préfecture de la région Corse";
	var adAC2 = "Palais Lantivy";
	var adAC3 = "20188 Ajaccio Cedex 9";
} else if (regionExercice == "Grand Est") {
	var adAC1 = "Préfecture de la région Grand Est";
	var adAC2 = "5 place de la République";
	var adAC3 = "BP 1047";
	var adAC4 = "67073 Strasbourg Cedex";
} else if (regionExercice == "Guadeloupe") {
	var adAC1 = "Préfecture de la région Guadeloupe";
	var adAC2 = "Palais d'Orléans";
	var adAC3 = "Rue Antoine-de-Lardenoy";
	var adAC4 = "97109 Basse-Terre Cedex";
} else if (regionExercice == "Guyane") {
	var adAC1 = "Préfecture de la région Guyane";
	var adAC2 = "Rue Fiedmond";
	var adAC3 = "BP 7008";
	var adAC4 = "97307 Cayenne Cedex";
} else if (regionExercice == "Hauts-de-France") {
	var adAC1 = "Préfecture de la région Hauts de France";
	var adAC2 = "12 rue Jean-sans-Peur";
	var adAC3 = "CS 20003";
	var adAC4 = "59039 Lille Cedex";
} else if (regionExercice == "Île-de-France") {
	var adAC1 = "Préfecture de la région Ile de France";
	var adAC2 = "5 rue Leblanc";
	var adAC3 = "75015 Paris";
} else if (regionExercice == "Martinique") {
	var adAC1 = "Préfecture de la région Martinique";
	var adAC2 = "82 rue Victor-Sévère";
	var adAC3 = "BP 647-648";
	var adAC4 = "97262 Fort-de-France Cedex";
} else if (regionExercice == "Mayotte") {
	var adAC1 = "Préfecture de la région Mayotte";
	var adAC2 = "Avenue de la Préfecture";
	var adAC3 = "97000 Mamoudzou";
} else if (regionExercice == "Normandie") {
	var adAC1 = "Préfecture de la région Normandie";
	var adAC2 = "7 place de la Madeleine";
	var adAC3 = "76000 Rouen";
} else if (regionExercice == "Nouvelle-Aquitaine") {
	var adAC1 = "Préfecture de la région Nouvelle-Aquitaine";
	var adAC2 = "2 esplanade Charles-de-Gaulle";
	var adAC3 = "CS 41397";
	var adAC4 = "33077 Bordeaux Cedex";
} else if (regionExercice == "Occitanie") {
	var adAC1 = "Préfecture de la région Occitanie";
	var adAC2 = "1 place Saint-Etienne";
	var adAC3 = "31038 Toulouse Cedex 9";
} else if (regionExercice == "Pays de la Loire") {
	var adAC1 = "Préfecture de la région Pays de la Loire";
	var adAC2 = "6 quai Ceineray";
	var adAC3 = "BP 33515";
	var adAC4 = "44035 Nantes Cedex 1";	
} else if (regionExercice == "Provence-Alpes-Côte d'Azur") {
	var adAC1 = "Préfecture de la région Provence-Alpes-Côte d'Azur";
	var adAC2 = "Place Félix-Baret";
	var adAC3 = "CS30001";
	var adAC4 = "13259 Marseille Cedex 06";
} else if (regionExercice == "La Réunion") {
	var adAC1 = "Préfecture de la région Réunion";
	var adAC2 = "6 rue des Messageries";
	var adAC3 = "CS 51079";
	var adAC4 = "97404 Saint-Denis-de-la-Réunion Cedex";
}	

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp155PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp155PE2.signature.signature.dateSignature,
		adresseAC1: adAC1,
		adresseAC2: adAC2,
		adresseAC3: adAC3,
		adresseAC4: adAC4
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Inseminateur_equin_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Inséminateur équin - demande de reconnaissance de qualifications professionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'inséminateur équin est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
