**Procédure élémentaire 3** : Inséminateur pour les espèces bovine, ovine et
caprine (QP156)

**Procédure** : demande de reconnaissance de qualifications professionnelles

**Contexte** : Libre établissement – Demande initiale – EM ne règlemente pas la
profession

-   RE d’un EM ou d’un Etat partie à l’accord sur l’EEE justifiant être
    titulaire d'attestations de compétence ou de preuves de titre de formation
    délivré par un autre Etat membre qui ne réglemente pas la profession et
    justifiant avoir exercé cette profession dans un Etat membre à temps plein
    pendant une année ou à temps partiel pendant une durée totale équivalente au
    cours des dix années précédentes

**Référence-id** : Formalités SCN/GQ/Métiers animaliers/Inséminateur pour les
espèces bovine, ovine et caprine/Libre établissement/Demande initiale/EM ne
règlemente pas la profession/Attestation de compétences et d’expérience
professionnelle

**Formulaire** : déclaration papier libre

**Mode de transmission** : envoi papier

**Destinataire** : centre d'enseignement zootechnique (CEZ) de Rambouillet

**Délais** :

-   Le CEZ dispose d’un délai d’un mois pour accuser réception du dossier ou, à
    défaut de dossier complet, pour solliciter les pièces manquantes

-   Le CEZ dispose d’un délai de trois mois pour prendre sa décision à compter
    de la réception du dossier complet

-   Ce délai peut être prorogé d’un mois par décision expresse de la CEZ

-   En absence de réponse du CEZ dans les délais impartis, la demande de
    reconnaissance de qualifications professionnelles est réputée acceptée

**Pièces justificatives** :

-   Copie de la pièce d’identité (en cours de validité, recto verso), ainsi
    qu'un document attestant de la nationalité si cette pièce ne le prévoit pas)

-   Copie de toutes pièces utiles justifiant des formations continues, de
    l’expérience et des compétences acquises au cours de l’exercice
    professionnel dans un Etat membre ou partie à l’EEE ou dans un Etat tiers

-   Copie de toute pièce justifiant que le déclarant a exercé l'activité dans
    cet Etat pendant au moins 1 an à temps complet ou une durée équivalente au
    cours des dix dernières années

-   Copie de toute preuve attestant des qualifications professionnelles

**Commentaires** :

-   L'accès à la profession ou à son exercice peut être subordonné à un stage
    d'adaptation d'une durée maximale de trois ans ou à une épreuve d'aptitude
    réalisée dans le délai maximal de six mois à compter de la décision
    l’imposant au demandeur
