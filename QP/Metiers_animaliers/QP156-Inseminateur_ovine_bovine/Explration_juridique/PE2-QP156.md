**Procédure élémentaire 2** : Inséminateur pour les espèces bovine, ovine et
caprine (QP156)

**Procédure** : demande de reconnaissance de qualifications professionnelles

**Contexte** : Libre établissement – Demande initiale – EM règlemente la
profession

-   RE d’un EM ou d’un Etat partie à l’accord sur l’EEE justifiant de la
    possession d’un diplôme lui permettant l’exercice de la profession dans un
    EM qui règlemente la profession

**Référence-id** : Formalités SCN/GQ/Métiers animaliers/Inséminateur pour les
espèces bovine, ovine et caprine/Libre établissement/Demande initiale/EM
règlemente la profession/Attestation de diplôme

**Formulaire** : déclaration papier libre

**Mode de transmission** : envoi papier

**Destinataire** : centre d'enseignement zootechnique (CEZ) de Rambouillet

**Délais** :

-   Le CEZ dispose d’un délai d’un mois pour accuser réception du dossier ou, à
    défaut de dossier complet, pour solliciter les pièces manquantes

-   Le CEZ dispose d’un délai de trois mois pour prendre sa décision à compter
    de la réception du dossier complet

-   Ce délai peut être prorogé d’un mois par décision expresse de la CEZ

-   En absence de réponse du CEZ dans les délais impartis, la demande de
    reconnaissance de qualifications professionnelles est réputée acceptée

**Pièces justificatives** :

-   Copie de la pièce d’identité (en cours de validité, recto verso), ainsi
    qu'un document attestant de la nationalité si cette pièce ne le prévoit pas)

-   Copie de chacun des diplômes, titres ou certificats invoqués ou de
    l’autorisation d’exercice et, le cas échant de l’attestation de révision en
    cours de validité pour les qualifications soumises à l’obligation de
    recyclage

-   Copie de toute preuve attestant des qualifications professionnelles

**Commentaires** :

-   L'accès à la profession ou à son exercice peut être subordonné à un stage
    d'adaptation d'une durée maximale de trois ans ou à une épreuve d'aptitude
    réalisée dans le délai maximal de six mois à compter de la décision
    l’imposant au demandeur
