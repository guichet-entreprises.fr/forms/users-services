var cerfaFields = {};

var civNomPrenom = $qp156PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp156PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp156PE5.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']                = $qp156PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp156PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp156PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']               = $qp156PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp156PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                      = $qp156PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                    = $qp156PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                          = $qp156PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp156PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp156PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                        = ($qp156PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp156PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp156PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp156PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']                  = $qp156PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                    = $qp156PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                         = $qp156PE5.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                             = $qp156PE5.signature.signature.dateSignature;
cerfaFields['signature']                        = $qp156PE5.signature.signature.signature;
cerfaFields['lieuSignature']                    = $qp156PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = $qp156PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp156PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp156PE5.etatCivil.identificationDeclarant.prenomDeclarant;

/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp156PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Centre d'enseignement zootechnique (CEZ) de Rambouillet",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));



 /* Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/inseminateur_ovine_bovine_LPS.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLPS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('inseminateur_ovine_bovine_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Inséminateur ovine bovine - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'inséminateur ovine bovine.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});