var cerfaFields = {};

var civNomPrenom = $qp156PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp156PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp156PE1.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']                = $qp156PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp156PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp156PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']               = $qp156PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp156PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                      = $qp156PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                    = $qp156PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                          = $qp156PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp156PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp156PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                        = ($qp156PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp156PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp156PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp156PE1.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']                  = $qp156PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                    = $qp156PE1.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                         = $qp156PE1.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                             = $qp156PE1.signature.signature.dateSignature;
cerfaFields['signature']                        = $qp156PE1.signature.signature.signature;
cerfaFields['lieuSignature']                    = $qp156PE1.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = $qp156PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp156PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp156PE1.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp156PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Centre d'enseignement zootechnique (CEZ) de Rambouillet",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});


 /* Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/inseminateur_ovine_bovine_LE.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationsAutorites);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjQualifications);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('inseminateur_ovine_bovine_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Inséminateur ovine bovine - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'inséminateur ovine bovine.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});