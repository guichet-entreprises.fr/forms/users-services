var cerfaFields = {};
//etatCivil

var civNomPrenom							= $qp150PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp150PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp150PE7.etatCivil.identificationDeclarant.prenomDeclarant + ',';
cerfaFields['civiliteNomPrenom']          	= civNomPrenom;
cerfaFields['villePaysNaissance']  			= $qp150PE7.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp150PE7.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp150PE7.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp150PE7.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp150PE7.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp150PE7.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null ? ', ' + $qp150PE7.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp150PE7.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp150PE7.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant + ' ' : ''  )+$qp150PE7.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ', ' + $qp150PE7.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp150PE7.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp150PE7.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp150PE7.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp150PE7.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp150PE7.signature.signature.signature;
cerfaFields['lieuSignature']                = 'Fait à '+$qp150PE7.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp150PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp150PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp150PE7.etatCivil.identificationDeclarant.prenomDeclarant;




if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {
	
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp150PE7.signature.signature.dateSignature,
		civiliteNomPrenom:civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
	.apply({
		date: $qp150PE7.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier LPS V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.preprocess.pjID);
appendPj($attachmentPreprocess.preprocess.pjAttestationLE);
appendPj($attachmentPreprocess.preprocess.pjPreuveQualifications);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('ICAD renouvLPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Identificateur des carnivores domestiques - demande d’agrément.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de renouvellement de la déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'identificateur des carnivores domestiques est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

function buildSimpleDocument() {
	

var cerfa = pdf.create('models/courrier LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Entretien des animaux de compagnie d\'espèces domestiques - renouvellement de déclaration de libre prestation de services.pdf', cerfa); //Nom du fichier en sortie


var cerfa = pdf.create('models/courrier LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('ICAD renouvLPS.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Identificateur de carnivores domestiques - renouvellement de la déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier libre de renouvellement de la déclaration préalable de libre prestation de services en vue d\'exercer la profession d\'Identificateur de carnivores domestiques.',
            description : 'Courrier obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
}