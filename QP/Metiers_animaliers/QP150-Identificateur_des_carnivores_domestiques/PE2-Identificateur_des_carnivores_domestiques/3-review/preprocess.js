var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp150PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp150PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp150PE2.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']           		= civNomPrenom;
cerfaFields['villePaysNaissance']  			 	= $qp150PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp150PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp150PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp150PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp150PE2.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp150PE2.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null ? ', ' + $qp150PE2.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp150PE2.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp150PE2.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant + ' ' : ''  ) + $qp150PE2.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ', ' + $qp150PE2.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp150PE2.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp150PE2.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp150PE2.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp150PE2.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp150PE2.signature.signature.signature;
cerfaFields['lieuSignature']                  	= 'Fait à '+$qp150PE2.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp150PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp150PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp150PE2.etatCivil.identificationDeclarant.prenomDeclarant;




if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {
	
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp150PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
	.apply({
		date: $qp150PE2.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.preprocess.pjID);
appendPj($attachmentPreprocess.preprocess.pjCV);
appendPj($attachmentPreprocess.preprocess.pjDiplomes);
appendPj($attachmentPreprocess.preprocess.pjExercice);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('ICAD RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Identificateur des carnivores domestiques - demande d’agrément.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'identificateur des carnivores domestiques est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});


}

function buildSimpleDocument() {
	

var cerfa = pdf.create('models/courrier LE V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('ICAD RQP.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Identificateur de carnivores domestiques - demande d’habilitation à procéder au marquage de carnivores domestiques',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier libre de demande d\'habilitation pour exercer la profession d\'identificateur de carnivores domestiques.',
            description : 'Courrier obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});

}

