var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp074PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp074PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp074PE5.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp074PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp074PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp074PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp074PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp074PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp074PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp074PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp074PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp074PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp074PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp074PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp074PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp074PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp074PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp074PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp074PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp074PE5.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp074PE5.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp074PE5.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp074PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp074PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp074PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp074PE5.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier libre_LPS_V3.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp074PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});

 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp074PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil des ventes volontaires des meubles aux enchères publiques",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre_LPS_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationNonInterdiction);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDocumentRéalisation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('commissaire_priseur_ventes_volontaires_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Commissaire priseur de ventes volontaires - déclaration préalable en vue de libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue de libre prestation de services pour l\'exercice de la profession de commissaire priseur de ventes volontaires.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
