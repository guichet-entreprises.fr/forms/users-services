var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp074PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp074PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp074PE3.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp074PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp074PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp074PE3.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp074PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp074PE3.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp074PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp074PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp074PE3.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp074PE3.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp074PE3.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp074PE3.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp074PE3.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp074PE3.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp074PE3.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp074PE3.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp074PE3.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp074PE3.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp074PE3.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp074PE3.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp074PE3.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp074PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp074PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp074PE3.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_libre_LE_V3.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp074PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});

 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp074PE3.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil des ventes volontaires des meubles aux enchères publiques",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.docPenales);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('commissaire_priseur_ventes_volontaires_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Commissaire priseur de ventes volontaires - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de commissaire priseur de ventes volontaires.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
