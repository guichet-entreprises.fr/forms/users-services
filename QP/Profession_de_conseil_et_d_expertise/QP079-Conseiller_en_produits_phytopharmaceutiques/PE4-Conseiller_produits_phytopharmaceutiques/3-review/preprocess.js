var cerfaFields = {};

var civNomPrenom = $qp079PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp079PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp079PE4.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $qp079PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp079PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp079PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp079PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp079PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp079PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp079PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp079PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp079PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp079PE4.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp079PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp079PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp079PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp079PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp079PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp079PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp079PE4.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp079PE4.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp079PE4.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp079PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp079PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp079PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp079PE4.etatCivil.identificationDeclarant.prenomDeclarant;


cerfaFields['libelleProfession']				= "Conseiller en produits phytopharmaceutiques"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp079PE4.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp079PE4.signature.signature.dateSignature,
		autoriteHabilitee :"Direction Régionale de l’Alimentation, de l’Agriculture et de la Forêt",
		demandeContexte : "Renouvellement de la demande de reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/courrier_libre_LE_Renouv_V4.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);


var finalDocItem = finalDoc.save('Conseiller_en_produits_phytopharmaceutiques_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Conseiller en produits phytopharmaceutiques - renouvellement de la demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Renouvellement de la demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de conseiller en produits phytopharmaceutiques.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});