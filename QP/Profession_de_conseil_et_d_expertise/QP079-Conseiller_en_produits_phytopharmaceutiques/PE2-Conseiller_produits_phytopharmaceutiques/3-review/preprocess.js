var cerfaFields = {};

var civNomPrenom = $qp079PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp079PE2.etatCivil.identificationDeclarant.nomNaissanceDeclarant + ' ' + $qp079PE2.etatCivil.identificationDeclarant.prenomDeclarant;

//Etat Civil
cerfaFields['civiliteMadame']                                 = ($qp079PE2.etatCivil.identificationDeclarant.civilite=='Madame');
cerfaFields['civiliteMonsieur']                               = ($qp079PE2.etatCivil.identificationDeclarant.civilite=='Monsieur');
cerfaFields['nomNaissanceDeclarant']                          = $qp079PE2.etatCivil.identificationDeclarant.nomNaissanceDeclarant;
cerfaFields['nomUsageDeclarant']                              = $qp079PE2.etatCivil.identificationDeclarant.nomUsageDeclarant;
cerfaFields['prenomDeclarant']                                = $qp079PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['dateNaissanceDeclarant']                         = $qp079PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['departementNaissanceDeclarant']                  = $qp079PE2.etatCivil.identificationDeclarant.departementNaissanceDeclarant;
cerfaFields['villePaysNaissanceDeclarant']                    = $qp079PE2.etatCivil.identificationDeclarant.lieuNaissance + ' ' + ' , ' + ' ' +  $qp079PE2.etatCivil.identificationDeclarant.paysNaissance;

//Coordonnées
cerfaFields['numeroVoieAdresseDeclarant']         			  = $qp079PE2.adresse.adressePersonnelle.numeroVoieAdresseDeclarant;
cerfaFields['nomVoieAdresseDeclarant']                        = $qp079PE2.adresse.adressePersonnelle.nomVoieAdresseDeclarant;
cerfaFields['complementAdresseDeclarant']                     = $qp079PE2.adresse.adressePersonnelle.complementAdresseDeclarant;
cerfaFields['codePostalAdresseDeclarant']                     = $qp079PE2.adresse.adressePersonnelle.codePostalAdresseDeclarant;
cerfaFields['communeAdresseDeclarant']                        = $qp079PE2.adresse.adressePersonnelle.communeAdresseDeclarant;
cerfaFields['telephoneFixeAdresseDeclarant']                  = $qp079PE2.adresse.adressePersonnelle.telephoneFixeAdresseDeclarant;
cerfaFields['telephoneMobileAdresseDeclarant']                = $qp079PE2.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;
cerfaFields['adresseMailDeclarant']                           = $qp079PE2.adresse.adressePersonnelle.adresseMailDeclarant;

//Fonctions
cerfaFields['fonctions']                       			      = $qp079PE2.certificat.fonctions0.fonctions;

//Certificat individuel demande
cerfaFields['activitesProfessionnelles']                      = $qp079PE2.certificat.activitepro.activitesProfessionnelles;
cerfaFields['categorie']                      				  = $qp079PE2.certificat.activitepro.categorie;

cerfaFields['intituleDiplomeTitre']                      	  = $qp079PE2.certificat.diplomes.intituleDiplomeTitre;

/* cerfaFields['nomOrganismeFormation']                          = $qp079PE2.certificat.diplomes.nomOrganismeFormation; 
cerfaFields['lieuDitAdresseOrganismeFormation']               = $qp079PE2.certificat.diplomes.lieuDitAdresseOrganismeFormation; 
cerfaFields['numeroNomVoieAdresseOrganismeFormation']         = $qp079PE2.certificat.diplomes.numeroNomVoieAdresseOrganismeFormation; 
cerfaFields['codePostalAdresseOrganismeFormation']            = $qp079PE2.certificat.diplomes.codePostalAdresseOrganismeFormation; 
cerfaFields['villePaysAdresseOrganismeFormation']             = $qp079PE2.certificat.diplomes.villeAdresseOrganismeFormation + ' ' + ' , ' + ' ' + $qp079PE2.certificat.diplomes.paysAdresseOrganismeFormation;

cerfaFields['dateFormationDu']                        		  = $qp079PE2.certificat.diplomes.dateFormationDu.from; 
cerfaFields['dateFormationAu']                        		  = $qp079PE2.certificat.diplomes.dateFormationDu.to; */

//Signataire
//cerfaFields['nomSignataireAttestation']                       = $qp079PE2.certificat.diplomes.nomSignataireAttestation;

//Déclaration sur l'honneur
cerfaFields['civiliteNomPrenomDeclarant']                     = $qp079PE2.etatCivil.identificationDeclarant.nomNaissanceDeclarant + ' ' + $qp079PE2.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['dateSignature']                                  = $qp079PE2.signatureGroup.signature.dateSignature;
cerfaFields['signatureCoche']                                 = $qp079PE2.signatureGroup.signature.signatureCoche;
cerfaFields['texteSignature']                                 = "Je déclare sur l’honneur l'exactitude des informations de la formalité et signe la présente déclaration.";

//cerfaFields['attesteHonneurDemandeUnique']                         = $qp079PE2.signatureGroup.signature.attesteHonneurDemandeUnique;
//cerfaFields['regionExercice']                                      = $qp079PE2.signatureGroup.signature.regionExercice;
cerfaFields['libelleProfession']								  	  = "Conseiller en produits phytopharmaceutiques"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp079PE2.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp079PE2.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Direction Régionale de l’Alimentation, de l’Agriculture et de la Forêt",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/DEMANDE DE CERTIFICAT INDIVIDUEL.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);


var finalDocItem = finalDoc.save('Conseiller_en_produits_phytopharmaceutiques_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Conseiller en produits phytopharmaceutiques - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de conseiller en produits phytopharmaceutiques.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});