var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp079PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp079PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp079PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	= $qp079PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp079PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp079PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp079PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp079PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp079PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp079PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp079PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp079PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp079PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp079PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp079PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp079PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp079PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp079PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp079PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp079PE5.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp079PE5.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp079PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp079PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp079PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp079PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp079PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']								  	  = "Conseiller en produits phytopharmaceutiques."
cerfaFields['libelleProfession2']								  	  = "conseiller en produits phytopharmaceutiques."

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp079PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp079PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Direction Régionale de l’Alimentation, de l’Agriculture et de la Forêt",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

/*
 * Ajout du cerfa
 */

var cerfaDoc = nash.doc //
    .load('models/courrier_libre_LPS_V3.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Conseiller_en_produits_phytopharmaceutiques_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Conseiller en produits phytopharmaceutiques - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de conseiller en produits phytopharmaceutiques.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
