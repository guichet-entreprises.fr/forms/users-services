var cerfaFields = {};

//etatCivil

cerfaFields['civiliteMadame']           = $qp072PE7.etatCivil.identificationDeclarant.civilite == "Madame";
cerfaFields['civiliteMonsieur']         = $qp072PE7.etatCivil.identificationDeclarant.civilite == "Monsieur";
cerfaFields['nomDeclarant']             = $qp072PE7.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']          = $qp072PE7.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['dateNaissance']            = $qp072PE7.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['lieuNaissance']            =($qp072PE7.etatCivil.identificationDeclarant.departementNaissance != null ? ($qp072PE7.etatCivil.identificationDeclarant.departementNaissance + ' ') : '') + $qp072PE7.etatCivil.identificationDeclarant.lieuNaissance +  ', ' + $qp072PE7.etatCivil.identificationDeclarant.paysNaissance;
cerfaFields['nationalite']              = $qp072PE7.etatCivil.identificationDeclarant.nationalite;

cerfaFields['nomPrénomPère']            = ($qp072PE7.etatCivil.identificationDeclarant.nomPere != null ? ('Nom et prénom(s) du Père : ' + $qp072PE7.etatCivil.identificationDeclarant.nomPere + ' ' + $qp072PE7.etatCivil.identificationDeclarant.prenomPere) : '');
cerfaFields['nomPrénomMère']            = ($qp072PE7.etatCivil.identificationDeclarant.nomMere != null ? ('Nom et prénom(s) de la Mère : ' + $qp072PE7.etatCivil.identificationDeclarant.nomMere + ' ' + $qp072PE7.etatCivil.identificationDeclarant.prenomMere) : '');


//Informations

cerfaFields['numeroLibelleAdresse']     = $qp072PE7.informationsGroup.informations.numeroLibelleAdresse;
cerfaFields['complementAdresse']        = $qp072PE7.informationsGroup.informations.complementAdresse;
cerfaFields['codePostalAdresse']        = $qp072PE7.informationsGroup.informations.codePostalAdresse;
cerfaFields['villePaysAdresse']         = $qp072PE7.informationsGroup.informations.villeAdresse + ', ' + $qp072PE7.informationsGroup.informations.paysAdresse;
cerfaFields['telephoneMobile']          = $qp072PE7.informationsGroup.informations.telephoneMobile;
cerfaFields['telephoneFixe']            = $qp072PE7.informationsGroup.informations.telephoneFixe;
cerfaFields['telecopie']                = $qp072PE7.informationsGroup.informations.telecopie;
cerfaFields['courriel']                 = $qp072PE7.informationsGroup.informations.courriel;
cerfaFields['siteInternet']             = $qp072PE7.informationsGroup.informations.siteInternet;

//Adresse contact

cerfaFields['civiliteNomPrenom']        = $qp072PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp072PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp072PE7.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['adressecontact1']          = $qp072PE7.informationsGroup.informations.numeroLibelleAdresse + ($qp072PE7.informationsGroup.informations.complementAdresse != null ? (', ' + $qp072PE7.informationsGroup.informations.complementAdresse) : '');
cerfaFields['adressecontact2']          = ($qp072PE7.informationsGroup.informations.codePostalAdresse != null ? ($qp072PE7.informationsGroup.informations.codePostalAdresse + ' ') : '') + $qp072PE7.informationsGroup.informations.villeAdresse + ', ' + $qp072PE7.informationsGroup.informations.paysAdresse;
cerfaFields['telephoneContact']         = $qp072PE7.informationsGroup.informations.telephoneMobile;

//Adresse imposition

//	cerfaFields['adresseImpositionVoie']    = ($qp072PE7.informationsGroup.informations.adresseImpositionAutre1 ? ($qp072PE7.informationsGroup.adresseImposition.numeroLibelleAdresseImposition + ($qp072PE7.informationsGroup.adresseImposition.complementAdresseImposition != null ? (' ' + $qp072PE7.informationsGroup.adresseImposition.complementAdresseImposition) : '')) : ($qp072PE7.informationsGroup.informations.numeroLibelleAdresse + ($qp072PE7.informationsGroup.informations.complementAdresse != null ? (' ' + $qp072PE7.informationsGroup.informations.complementAdresse) : '')));
//	cerfaFields['adresseImpositionCPVille'] = ($qp072PE7.informationsGroup.informations.adresseImpositionAutre1 ? (($qp072PE7.informationsGroup.adresseImposition.codePostalAdresseImposition != null ? ($qp072PE7.informationsGroup.adresseImposition.codePostalAdresseImposition + ' ') : '') + $qp072PE7.informationsGroup.adresseImposition.villeAdresseImposition) : (($qp072PE7.informationsGroup.informations.codePostalAdresse != null ? ($qp072PE7.informationsGroup.informations.codePostalAdresse + ' ') : '') + $qp072PE7.informationsGroup.informations.villeAdresse));
//	cerfaFields['adresseImpositionPays']    = ($qp072PE7.informationsGroup.informations.adresseImpositionAutre1 ? $qp072PE7.informationsGroup.adresseImposition.paysAdresseImposition : $qp072PE7.informationsGroup.informations.paysAdresse);

if ($qp072PE7.informationsGroup.informations.adresseImpositionAutre1) {
	cerfaFields['adresseImpositionVoie']    = ($qp072PE7.informationsGroup.adresseImposition.numeroLibelleAdresseImposition + ($qp072PE7.informationsGroup.adresseImposition.complementAdresseImposition != null ? (' ' + $qp072PE7.informationsGroup.adresseImposition.complementAdresseImposition) : ''));
	cerfaFields['adresseImpositionCPVille'] = (($qp072PE7.informationsGroup.adresseImposition.codePostalAdresseImposition != null ? ($qp072PE7.informationsGroup.adresseImposition.codePostalAdresseImposition + ' ') : '') + $qp072PE7.informationsGroup.adresseImposition.villeAdresseImposition);
	cerfaFields['adresseImpositionPays']    = $qp072PE7.informationsGroup.adresseImposition.paysAdresseImposition;
} else {
	cerfaFields['adresseImpositionVoie']    = ($qp072PE7.informationsGroup.informations.numeroLibelleAdresse + ($qp072PE7.informationsGroup.informations.complementAdresse != null ? (' ' + $qp072PE7.informationsGroup.informations.complementAdresse) : '')));
	cerfaFields['adresseImpositionCPVille'] = (($qp072PE7.informationsGroup.informations.codePostalAdresse != null ? ($qp072PE7.informationsGroup.informations.codePostalAdresse + ' ') : '') + $qp072PE7.informationsGroup.informations.villeAdresse));
	cerfaFields['adresseImpositionPays']    = $qp072PE7.informationsGroup.informations.paysAdresse;	
}

//Adresse visite

cerfaFields['adresseVoie']              = $qp072PE6.adresseControleQualite.adresseControleQualite.numeroLibelleAdresseControle;
cerfaFields['adresseComplement']        = ($qp072PE6.adresseControleQualite.adresseControleQualite.complementAdresseControle != null ? $qp072PE6.adresseControleQualite.adresseControleQualite.complementAdresseControle : '');
cerfaFields['adresseCPVillePays']       = ($qp072PE6.adresseControleQualite.adresseControleQualite.codePostalAdresseControle != null ? ($qp072PE6.adresseControleQualite.adresseControleQualite.codePostalAdresseControle + ' ') : '') + $qp072PE6.adresseControleQualite.adresseControleQualite.villeAdresseControle + ', ' + $qp072PE6.adresseControleQualite.adresseControleQualite.paysAdresseControle;



//Signature

cerfaFields['engagement1']              =$qp072PE7.signature.signature.engagement1;
cerfaFields['engagement2']              =$qp072PE7.signature.signature.engagement2;
cerfaFields['engagement3']              =$qp072PE7.signature.signature.engagement3;
cerfaFields['attesteHonneur']           =$qp072PE7.signature.signature.attesteHonneur;
cerfaFields['lieuSignature']            =$qp072PE7.signature.signature.lieuSignature;
cerfaFields['dateSignature']            =$qp072PE7.signature.signature.dateSignature;
cerfaFields['signature']                =$qp072PE7.signature.signature.signature;


var cerfa = pdf.create('models/inscription liste nationale commissaires aux comptes.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Commissaire aux comptes - inscription liste nationale commissaires aux comptes,  demande de RQP.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Commissaire aux comptes - demande de RQP',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande d\'inscription sur la liste nationale des commissaires aux comptes, reconnaissance de qualifications professionnelles pour la profession de commissaire aux comptes',
            description : 'Formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
