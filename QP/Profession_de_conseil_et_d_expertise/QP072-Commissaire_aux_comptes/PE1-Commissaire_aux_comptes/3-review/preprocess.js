var cerfaFields = {};

//etatCivil

cerfaFields['civiliteMadame']           = $qp072PE1.etatCivil.identificationDeclarant.civilite == "Madame";
cerfaFields['civiliteMonsieur']         = $qp072PE1.etatCivil.identificationDeclarant.civilite == "Monsieur";
cerfaFields['nomDeclarant']             = $qp072PE1.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']          = $qp072PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['dateNaissance']            = $qp072PE1.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['lieuNaissance']            =($qp072PE1.etatCivil.identificationDeclarant.departementNaissance != null ? ($qp072PE1.etatCivil.identificationDeclarant.departementNaissance + ' ') : '') + $qp072PE1.etatCivil.identificationDeclarant.lieuNaissance +  ', ' + $qp072PE1.etatCivil.identificationDeclarant.paysNaissance;
cerfaFields['nationalite']              = $qp072PE1.etatCivil.identificationDeclarant.nationalite;

cerfaFields['nomPrénomPère']            = ($qp072PE1.etatCivil.identificationDeclarant.nomPere != null ? ('Nom et prénom(s) du Père : ' + $qp072PE1.etatCivil.identificationDeclarant.nomPere + ' ' + $qp072PE1.etatCivil.identificationDeclarant.prenomPere) : '');
cerfaFields['nomPrénomMère']            = ($qp072PE1.etatCivil.identificationDeclarant.nomMere != null ? ('Nom et prénom(s) de la Mère : ' + $qp072PE1.etatCivil.identificationDeclarant.nomMere + ' ' + $qp072PE1.etatCivil.identificationDeclarant.prenomMere) : '');


//Informations

cerfaFields['numeroLibelleAdresse']     = $qp072PE1.informationsGroup.informations.numeroLibelleAdresse;
cerfaFields['complementAdresse']        = $qp072PE1.informationsGroup.informations.complementAdresse;
cerfaFields['codePostalAdresse']        = $qp072PE1.informationsGroup.informations.codePostalAdresse;
cerfaFields['villePaysAdresse']         = $qp072PE1.informationsGroup.informations.villeAdresse + ', ' + $qp072PE1.informationsGroup.informations.paysAdresse;
cerfaFields['telephoneMobile']          = $qp072PE1.informationsGroup.informations.telephoneMobile;
cerfaFields['telephoneFixe']            = $qp072PE1.informationsGroup.informations.telephoneFixe;
cerfaFields['telecopie']                = $qp072PE1.informationsGroup.informations.telecopie;
cerfaFields['courriel']                 = $qp072PE1.informationsGroup.informations.courriel;
cerfaFields['siteInternet']             = $qp072PE1.informationsGroup.informations.siteInternet;

//Adresse contact

cerfaFields['civiliteNomPrenom']        = $qp072PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp072PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp072PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['adressecontact1']          = $qp072PE1.informationsGroup.informations.numeroLibelleAdresse + ($qp072PE1.informationsGroup.informations.complementAdresse != null ? (', ' + $qp072PE1.informationsGroup.informations.complementAdresse) : '');
cerfaFields['adressecontact2']          = ($qp072PE1.informationsGroup.informations.codePostalAdresse != null ? ($qp072PE1.informationsGroup.informations.codePostalAdresse + ' ') : '') + $qp072PE1.informationsGroup.informations.villeAdresse + ', ' + $qp072PE1.informationsGroup.informations.paysAdresse;
cerfaFields['telephoneContact']         = $qp072PE1.informationsGroup.informations.telephoneMobile;

//Adresse imposition

cerfaFields['adresseImpositionVoie']    = ($qp072PE1.informationsGroup.informations.adresseImpositionAutre1 ? ($qp072PE1.informationsGroup.adresseImposition.numeroLibelleAdresseImposition + ($qp072PE1.informationsGroup.adresseImposition.complementAdresseImposition != null ? (' ' + $qp072PE1.informationsGroup.adresseImposition.complementAdresseImposition) : '')) : ($qp072PE1.informationsGroup.informations.numeroLibelleAdresse + ($qp072PE1.informationsGroup.informations.complementAdresse != null ? (' ' + $qp072PE1.informationsGroup.informations.complementAdresse) : '')));
cerfaFields['adresseImpositionCPVille'] = ($qp072PE1.informationsGroup.informations.adresseImpositionAutre1 ? (($qp072PE1.informationsGroup.adresseImposition.codePostalAdresseImposition != null ? ($qp072PE1.informationsGroup.adresseImposition.codePostalAdresseImposition + ' ') : '') + $qp072PE1.informationsGroup.adresseImposition.villeAdresseImposition) : (($qp072PE1.informationsGroup.informations.codePostalAdresse != null ? ($qp072PE1.informationsGroup.informations.codePostalAdresse + ' ') : '') + $qp072PE1.informationsGroup.informations.villeAdresse));
cerfaFields['adresseImpositionPays']    = ($qp072PE1.informationsGroup.informations.adresseImpositionAutre1 ? $qp072PE1.informationsGroup.adresseImposition.paysAdresseImposition : $qp072PE1.informationsGroup.informations.paysAdresse);

//Adresse visite

cerfaFields['adresseVoie']              = $qp072PE1.adresseControleQualite.adresseControleQualite.numeroLibelleAdresseControle;
cerfaFields['adresseComplement']        = ($qp072PE1.adresseControleQualite.adresseControleQualite.complementAdresseControle != null ? $qp072PE1.adresseControleQualite.adresseControleQualite.complementAdresseControle : '');
cerfaFields['adresseCPVillePays']       = ($qp072PE1.adresseControleQualite.adresseControleQualite.codePostalAdresseControle != null ? ($qp072PE1.adresseControleQualite.adresseControleQualite.codePostalAdresseControle + ' ') : '') + $qp072PE1.adresseControleQualite.adresseControleQualite.villeAdresseControle + ', ' + $qp072PE1.adresseControleQualite.adresseControleQualite.paysAdresseControle;



//Signature

cerfaFields['engagement1']              =$qp072PE1.signature.signature.engagement1;
cerfaFields['engagement2']              =$qp072PE1.signature.signature.engagement2;
cerfaFields['engagement3']              =$qp072PE1.signature.signature.engagement3;
cerfaFields['attesteHonneur']           =$qp072PE1.signature.signature.attesteHonneur;
cerfaFields['lieuSignature']            =$qp072PE1.signature.signature.lieuSignature;
cerfaFields['dateSignature']            =$qp072PE1.signature.signature.dateSignature;
cerfaFields['signature']                =$qp072PE1.signature.signature.signature;


var cerfa = pdf.create('models/inscription liste nationale commissaires aux comptes.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Commissaire aux comptes - inscription liste nationale commissaires aux comptes, demande de RQP.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Commissaire aux comptes - inscription liste nationale commissaires aux comptes, demande de RQP',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande d\'inscription sur la liste nationale des commissaires aux comptes, reconnaissance de qualifications professionnelles pour la profession de comissaire aux comptes',
            description : 'Formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
