var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp080PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp080PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp080PE5.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']                = $qp080PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp080PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp080PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']               = $qp080PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp080PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                      = $qp080PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                    = $qp080PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 					  		= $qp080PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp080PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp080PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       			 	 	= ($qp080PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp080PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp080PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp080PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']                  = $qp080PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                    = $qp080PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                         = $qp080PE5.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['departementExercice']              = $qp080PE5.adresse.regionExerciceGroup.departementExercice


//signature
cerfaFields['date']                             = $qp080PE5.signature.signature.dateSignature;
cerfaFields['signature']                        = $qp080PE5.signature.signature.signature;
cerfaFields['lieuSignature']                    = 'Fait à '+$qp080PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = $qp080PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp080PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp080PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['attestationRegionExercice']  		= $qp080PE5.adresse.regionExerciceGroup.attestationLieuExercice;


if (nash.doc && nash.record && nash.record.description) {
    return buildMergedDocument();
} else {
    return buildSimpleDocument();
}

function buildMergedDocument() {

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
    .load('models/courrier_RE_dossierfinal.pdf') //
    .apply({
        numDossier: nash.record.description().recordUid,
        date: $qp080PE5.signature.signature.dateSignature,
        civiliteNomPrenom: civNomPrenom
    });
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
    .load('models/Courrier au premier dossier v1.6 LPS.pdf') //
    .apply({
        date: $qp080PE5.signature.signature.dateSignature

    });

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/courrier libre LPS V3.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Conseiller_genetique_renouv_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Conseiller en génétique - renouvellement de la déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de renouvellement de la déclaration préalable en vue d\'une libre prestation de services pour exercer le métier de conseiller en génétique est maintenant terminée.',
           description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

}

function buildSimpleDocument() {

var cerfa = pdf.create('models/courrier libre LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('experimentations_animaux_renouv_LPS.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Conception et réalisation d’expérimentations sur les animaux - renouvellement de la déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Renouvellement de la déclaration préalable en vue d\'une libre prestation de services pour exercer dans la conception et réalisation d\'expérimentations sur les animaux.',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
}