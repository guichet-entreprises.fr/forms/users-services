var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp080PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp080PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp080PE2.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['monsieurDeclarant']                    = ($qp080PE2.etatCivil.identificationDeclarant.civilite=='Monsieur');
cerfaFields['madameDeclarant']                      = ($qp080PE2.etatCivil.identificationDeclarant.civilite=='Madame');
cerfaFields['prenomDeclarant']                      = $qp080PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['lieuNaissanceDeclarant']               = $qp080PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']                 = $qp080PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['nomDeclarant']                         = $qp080PE2.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['nomEpouseDeclarant']                   = $qp080PE2.etatCivil.identificationDeclarant.nomEpouseDeclarant;
cerfaFields['paysNaissanceDeclarant']               = $qp080PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['dateNaissanceDeclarant']               = $qp080PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse
cerfaFields['numeroComplementAdresseDeclarant']   	= $qp080PE2.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant + 
													 ($qp080PE2.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ', ' + $qp080PE2.adresse.adressePersonnelle.complementAdresseDeclarant : '');
cerfaFields['villeAdresseDeclarant']                = $qp080PE2.adresse.adressePersonnelle.villeAdresseDeclarant;
cerfaFields['codePostalAdresseDeclarant']           = $qp080PE2.adresse.adressePersonnelle.codePostalAdresseDeclarant;
cerfaFields['paysAdresseDeclarant']                 = $qp080PE2.adresse.adressePersonnelle.paysAdresseDeclarant;
cerfaFields['telephoneAdresseDeclarant']            = $qp080PE2.adresse.adressePersonnelle.telephoneAdresseDeclarant;
cerfaFields['telephoneMobileAdresseDeclarant']      = $qp080PE2.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;
cerfaFields['mailAdresseDeclarant']                 = $qp080PE2.adresse.adressePersonnelle.mailAdresseDeclarant;


//diplomeProfession
cerfaFields['intituleDiplome']                      = $qp080PE2.diplomeProfession.diplomeProfession.intituleDiplome;
cerfaFields['delivreDiplome']                       = $qp080PE2.diplomeProfession.diplomeProfession.delivreDiplome;
cerfaFields['paysObtentionDiplome']                 = $qp080PE2.diplomeProfession.diplomeProfession.paysObtentionDiplome;
cerfaFields['dateObtentionDiplome']                 = $qp080PE2.diplomeProfession.diplomeProfession.dateObtentionDiplome;
cerfaFields['dateReconnaissanceDiplomeEtat']        = $qp080PE2.diplomeProfession.diplomeProfession.dateReconnaissanceDiplomeEtat;

//diplomeQualification
cerfaFields['paysObtentionDiplome1']                = $qp080PE2.diplomeQualification.diplomeQualification1.paysObtentionDiplome1;
cerfaFields['intituleObtentionDiplome1']            = $qp080PE2.diplomeQualification.diplomeQualification1.intituleObtentionDiplome1;
cerfaFields['dateObtentionDiplome1']                = $qp080PE2.diplomeQualification.diplomeQualification1.dateObtentionDiplome1;
cerfaFields['lieuFormationObtentionDiplome1']       = $qp080PE2.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome1;

cerfaFields['paysObtentionDiplome2']                = $qp080PE2.diplomeQualification.diplomeQualification1.paysObtentionDiplome2; 
cerfaFields['intituleObtentionDiplome2']            = $qp080PE2.diplomeQualification.diplomeQualification1.intituleObtentionDiplome2;
cerfaFields['dateObtentionDiplome2']                = $qp080PE2.diplomeQualification.diplomeQualification1.dateObtentionDiplome2;
cerfaFields['lieuFormationObtentionDiplome2']       = $qp080PE2.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome2;

cerfaFields['paysObtentionDiplome3']                = $qp080PE2.diplomeQualification.diplomeQualification1.paysObtentionDiplome3;
cerfaFields['intituleObtentionDiplome3']            = $qp080PE2.diplomeQualification.diplomeQualification1.intituleObtentionDiplome3;
cerfaFields['dateObtentionDiplome3']                = $qp080PE2.diplomeQualification.diplomeQualification1.dateObtentionDiplome3;
cerfaFields['lieuFormationObtentionDiplome3']       = $qp080PE2.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome3;

cerfaFields['paysObtentionDiplome4']                = $qp080PE2.diplomeQualification.diplomeQualification1.paysObtentionDiplome4;
cerfaFields['intituleObtentionDiplome4']            = $qp080PE2.diplomeQualification.diplomeQualification1.intituleObtentionDiplome4;
cerfaFields['dateObtentionDiplome4']                = $qp080PE2.diplomeQualification.diplomeQualification1.dateObtentionDiplome4;
cerfaFields['lieuFormationObtentionDiplome4']       = $qp080PE2.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome4;

cerfaFields['paysObtentionDiplome5']                = $qp080PE2.diplomeQualification.diplomeQualification1.paysObtentionDiplome5;
cerfaFields['intituleObtentionDiplome5']            = $qp080PE2.diplomeQualification.diplomeQualification1.intituleObtentionDiplome5;
cerfaFields['dateObtentionDiplome5']                = $qp080PE2.diplomeQualification.diplomeQualification1.dateObtentionDiplome5;
cerfaFields['lieuFormationObtentionDiplome5']       = $qp080PE2.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome5;

//diplomeAutresQualification
cerfaFields['paysObtentionAutresDiplome1']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome1;
cerfaFields['intituleObtentionAutresDiplome1']      = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome1;
cerfaFields['dateObtentionAutresDiplome1']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome1;
cerfaFields['lieuFormationObtentionAutresDiplome1'] = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome1;

cerfaFields['paysObtentionAutresDiplome2']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome2;
cerfaFields['intituleObtentionAutresDiplome2']      = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome2;
cerfaFields['dateObtentionAutresDiplome2']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome2;
cerfaFields['lieuFormationObtentionAutresDiplome2'] = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome2;

cerfaFields['paysObtentionAutresDiplome3']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome3;
cerfaFields['intituleObtentionAutresDiplome3']      = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome3;
cerfaFields['dateObtentionAutresDiplome3']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome3;
cerfaFields['lieuFormationObtentionAutresDiplome3'] = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome3;

cerfaFields['paysObtentionAutresDiplome4']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome4;
cerfaFields['intituleObtentionAutresDiplome4']      = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome4;
cerfaFields['dateObtentionAutresDiplome4']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome4;
cerfaFields['lieuFormationObtentionAutresDiplome4'] = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome4;

cerfaFields['paysObtentionAutresDiplome5']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome5;
cerfaFields['intituleObtentionAutresDiplome5']      = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome5;
cerfaFields['dateObtentionAutresDiplome5']          = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome5;
cerfaFields['lieuFormationObtentionAutresDiplome5'] = $qp080PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome5;

//exerciceProfessionnel
cerfaFields['natureFonctionExerceesEtranger1']      = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger1;
cerfaFields['lieuFonctionExerceesEtranger1']        = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger1;
cerfaFields['periodeFonctionExerceesEtranger1']     = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1;

cerfaFields['natureFonctionExerceesEtranger2']      = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger2;
cerfaFields['lieuFonctionExerceesEtranger2']        = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger2;
cerfaFields['periodeFonctionExerceesEtranger2']     = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2;

cerfaFields['natureFonctionExerceesEtranger3']      = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger3;
cerfaFields['lieuFonctionExerceesEtranger3']        = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger3;
cerfaFields['periodeFonctionExerceesEtranger3']     = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3;

cerfaFields['natureFonctionExerceesEtranger4']      = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger4;
cerfaFields['lieuFonctionExerceesEtranger4']        = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger4;
cerfaFields['periodeFonctionExerceesEtranger4']     = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4;

cerfaFields['natureFonctionExerceesEtranger5']      = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger5;
cerfaFields['lieuFonctionExerceesEtranger5']        = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger5;
cerfaFields['periodeFonctionExerceesEtranger5']     = $qp080PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger5;

//projetsProfessionnels
cerfaFields['projetsProfessionnels']                = $qp080PE2.projetsProfessionnels.projetsProfessionnels.projetsProfessionnels;
cerfaFields['regionExercice']                		= $qp080PE2.projetsProfessionnels.projetsProfessionnels.regionExercice;
cerfaFields['attestationLieuExercice']              = $qp080PE2.projetsProfessionnels.projetsProfessionnels.attestationLieuExercice;

//projetsProfessionnels
cerfaFields['dateSignature']                		= $qp080PE2.signature.signature.dateSignature;
cerfaFields['signature']           					= $qp080PE2.signature.signature.signature;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp080PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp080PE2.signature.signature.dateSignature
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire de demande d’autorisation d’exercice.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.titreFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.attestationAutoritesDelivreTitreFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.copieDiplomeComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.piecesJustifiantFormationsContinues);
appendPj($attachmentPreprocess.attachmentPreprocess.declaractionAutoriteCompetente);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Conseiller_genetique_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Conseiller en génétique - demande de reconnaissance de qualifications professionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de conseiller en génétique est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
