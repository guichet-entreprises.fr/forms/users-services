var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp080PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp080PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp080PE4.etatCivil.identificationDeclarant.prenomDeclarant + ',';


cerfaFields['civiliteNomPrenom']           = $qp080PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp080PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp080PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['civiliteNomPrenomSignature']  = $qp080PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp080PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp080PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']          = $qp080PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant +', ' + $qp080PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                 = $qp080PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
 
cerfaFields['dateNaissance']               = $qp080PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
                
cerfaFields['telephoneMobile']            = $qp080PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['adresse'] 					  = $qp080PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp080PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp080PE4.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       			  = ($qp080PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp080PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp080PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp080PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneFixe']              = $qp080PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']            = $qp080PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']                   = $qp080PE4.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['departementExercice']        = $qp080PE4.adresse.regionExerciceGroup.departementExercice;


//signature
cerfaFields['date']                       = $qp080PE4.signature.signature.dateSignature;
cerfaFields['signature']                  = $qp080PE4.signature.signature.signature;
cerfaFields['lieuSignature']              = $qp080PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['attestationRegionExercice']  = $qp080PE4.adresse.regionExerciceGroup.attestationLieuExercice;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
    .load('models/courrier_RE_dossierfinal.pdf') //
    .apply({
        numDossier: nash.record.description().recordUid,
        date: $qp080PE4.signature.signature.dateSignature,
        civiliteNomPrenom: civNomPrenom
    });
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
    .load('models/Courrier au premier dossier v1.6 LPS.pdf') //
    .apply({
        date: $qp080PE4.signature.signature.dateSignature
    });

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/courrier libre LPS V3.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.copieDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.activiteRestauration);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Conseiller_genetique.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Conseiller en génétique - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de conseiller en génétique est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
