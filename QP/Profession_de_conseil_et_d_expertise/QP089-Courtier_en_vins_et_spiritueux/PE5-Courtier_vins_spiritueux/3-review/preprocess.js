var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp089PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp089PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp089PE5.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	    = $qp089PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp089PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp089PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp089PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp089PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp089PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp089PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp089PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp089PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp089PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp089PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp089PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp089PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp089PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp089PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp089PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp089PE5.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp089PE5.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp089PE5.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp089PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp089PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp089PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp089PE5.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var accompDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp089PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp089PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Président de la chambre de commerce et d’industrie régionale",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjRegistreCommerce);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationSurHonneur);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Courtier_en_vins_et_spiritueux_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Courtier en vins et spiritueux - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de courtier en vins et spiritueux',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
