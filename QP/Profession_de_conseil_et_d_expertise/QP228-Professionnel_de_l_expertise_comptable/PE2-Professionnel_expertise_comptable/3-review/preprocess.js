function pad(s) { return (s < 10) ? '0' + s : s; }
var cerfaFields = {};

//Inscription

cerfaFields['premiereInscription']                      = "true";
cerfaFields['reinscription']                            = $qp228PE2.inscription.inscription.reinscription;
cerfaFields['omissionProvisoire']                       = Value('id').of($qp228PE2.inscription.inscription.motifReinscription).eq("omissionProvisoire") ? true : false;
cerfaFields['radiationOffice']                          = Value('id').of($qp228PE2.inscription.inscription.motifReinscription).eq("radiationOffice") ? true : false;
cerfaFields['radiationDemande']                         = Value('id').of($qp228PE2.inscription.inscription.motifReinscription).eq("radiationDemande") ? true : false;
cerfaFields['expertComptableIndependant']               = Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableIndependant") ? true : false;
cerfaFields['expertComptableSalarie']                   = Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableSalarie") ? true : false;
cerfaFields['statutEIRL']                               = Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("statutEIRL") ? true : false;
cerfaFields['ouiVisa']                                  = ($qp228PE2.inscription.inscription.visa) ? true : false;
cerfaFields['nonVisa']                                  = ($qp228PE2.inscription.inscription.visa) ? false : true;

//Etat civil
	
	//Identification
cerfaFields['monsieur']                                 = Value('id').of($qp228PE2.etatCivil.identificationDeclarant.civilite).eq("monsieur") ? true : false;
cerfaFields['madame']                                   = Value('id').of($qp228PE2.etatCivil.identificationDeclarant.civilite).eq("madame") ? true : false;
cerfaFields['nomDeclarant']                             = $qp228PE2.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['nomMaritalDeclarant']                      = $qp228PE2.etatCivil.identificationDeclarant.nomMaritalDeclarant;
cerfaFields['nomUsuel']                                 = $qp228PE2.etatCivil.identificationDeclarant.nomUsuel;
cerfaFields['prenomDeclarant']                          = $qp228PE2.etatCivil.identificationDeclarant.prenomDeclarant;
	
	//Naissance déclarant
if($qp228PE2.etatCivil.naissanceDeclarant.dateNaissanceDeclarant != null) {
	var dateTemp = new Date(parseInt ($qp228PE2.etatCivil.naissanceDeclarant.dateNaissanceDeclarant.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
cerfaFields['jour']   = day;
cerfaFields['mois']   = month;
cerfaFields['annee']  = year;

}

cerfaFields['villeNaissanceDeclarant']                  = $qp228PE2.etatCivil.naissanceDeclarant.villeNaissanceDeclarant;
cerfaFields['departementDeclarant']                     = $qp228PE2.etatCivil.naissanceDeclarant.departementDeclarant;
cerfaFields['paysNaissanceDeclarant']                   = $qp228PE2.etatCivil.naissanceDeclarant.paysNaissanceDeclarant;
var civNomPrenom                                        = ($qp228PE2.etatCivil.identificationDeclarant.nomUsuel !=null ? $qp228PE2.etatCivil.identificationDeclarant.nomUsuel : '') + ' '+ ($qp228PE2.etatCivil.identificationDeclarant.prenomDeclarant !=null ? $qp228PE2.etatCivil.identificationDeclarant.prenomDeclarant : '');
	//Domicile
cerfaFields['numeroLibelleVoie']                        = $qp228PE2.domicileDeclarant.domicileDeclarant.numeroLibelleVoie;
cerfaFields['complementAdresseDeclarant']               = $qp228PE2.domicileDeclarant.domicileDeclarant.complementAdresseDeclarant;
cerfaFields['codePostalDeclarant']                      = $qp228PE2.domicileDeclarant.domicileDeclarant.codePostalDeclarant;
cerfaFields['villeDomicileDeclarant']                   = $qp228PE2.domicileDeclarant.domicileDeclarant.villeDomicileDeclarant;
cerfaFields['paysDeclarant']                            = $qp228PE2.domicileDeclarant.domicileDeclarant.paysDeclarant;
cerfaFields['mobileDeclarant']                          = $qp228PE2.domicileDeclarant.domicileDeclarant.mobileDeclarant;
cerfaFields['telephoneDeclarant']                       = $qp228PE2.domicileDeclarant.domicileDeclarant.telephoneDeclarant;
cerfaFields['emailDeclarant']                           = $qp228PE2.domicileDeclarant.domicileDeclarant.emailDeclarant;

//Modalité exercice
cerfaFields['ouiIndividuel']                            = Value('id').of($qp228PE2.modaliteExercice.modaliteExercice.modaliteExerciceChoix).eq('titreIndividuel') ? true : false;
cerfaFields['nonIndividuel']                            = Value('id').of($qp228PE2.modaliteExercice.modaliteExercice.modaliteExerciceChoix).eq('titreIndividuel') ? false : true;
cerfaFields['ouiQualite']                               = Value('id').of($qp228PE2.modaliteExercice.modaliteExercice.modaliteExerciceChoix).eq('qualiteDirigeantAssocieSalarie') ? true : false;
cerfaFields['nonQualite']                               = Value('id').of($qp228PE2.modaliteExercice.modaliteExercice.modaliteExerciceChoix).eq('qualiteDirigeantAssocieSalarie') ? false : true;
cerfaFields['nonSalarie']                               = Value('id').of($qp228PE2.modaliteExercice.modaliteExercice.modaliteExerciceChoix).eq('qualiteSalarie') ? false : true;
cerfaFields['ouiSalarie']                               = Value('id').of($qp228PE2.modaliteExercice.modaliteExercice.modaliteExerciceChoix).eq('qualiteSalarie') ? true : false;
cerfaFields['denominationSociete']                      = $qp228PE2.modaliteExercice.modaliteExercice.denominationSociete;
cerfaFields['nomExpertComptable']                       = $qp228PE2.modaliteExercice.modaliteExercice.nomExpertComptable;
cerfaFields['prenomExpertComptable']                    = $qp228PE2.modaliteExercice.modaliteExercice.prenomExpertComptable;


//Adresse professionnelle déclarant
cerfaFields['numeroLibelleVoieProfessionnelle']         = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.numeroLibelleVoieProfessionnelle;
cerfaFields['complementAdresseProfessionnelle']         = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.complementAdresseProfessionnelle;
cerfaFields['villeDomicileProfessionnelle']             = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.villeDomicileProfessionnelle;
cerfaFields['codePostalProfessionnelle']                = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.codePostalProfessionnelle;
cerfaFields['paysProfessionnel']                        = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.paysProfessionnel;
cerfaFields['telephoneProfessionnel']                   = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.telephoneProfessionnel;
cerfaFields['faxProfessionnel']                         = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.faxProfessionnel;
cerfaFields['emailProfessionnel']                       = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.emailProfessionnel;
cerfaFields['siteInternet']                             = $qp228PE2.adresseProfessionnelle.adresseProfessionnelle.siteInternet;

//renseignements divers
	//Situation pro
cerfaFields['activiteLiberale']                         = Value('id').of($qp228PE2.renseignements1.situationProfessionnelleActuelle.situationPro).eq("activiteLiberale") ? true : false;
cerfaFields['salarieEntreprise']                        = Value('id').of($qp228PE2.renseignements1.situationProfessionnelleActuelle.situationPro).eq("salarieEntreprise") ? true : false;
cerfaFields['salarieOrdre']                             = Value('id').of($qp228PE2.renseignements1.situationProfessionnelleActuelle.situationPro).eq("salarieOrdre") ? true : false;
cerfaFields['dirigeantSociete']                         = Value('id').of($qp228PE2.renseignements1.situationProfessionnelleActuelle.situationPro).eq("dirigeantSociete") ? true : false;
cerfaFields['demandeurEmploi']                          = Value('id').of($qp228PE2.renseignements1.situationProfessionnelleActuelle.situationPro).eq("demandeurEmploi") ? true : false;

	//Autres
cerfaFields['ouiCommissairesComptes']                   = ($qp228PE2.renseignements2.autres.listeCommissaire) ? true : false;
cerfaFields['nonCommissairesComptes']                   = ($qp228PE2.renseignements2.autres.listeCommissaire) ? false : true;
cerfaFields['courAppel']                                = $qp228PE2.renseignements2.autres.courAppel;
cerfaFields['dateAppel']                                = $qp228PE2.renseignements2.autres.dateAppel;
cerfaFields['ouiExpertJudiciaire']                      = ($qp228PE2.renseignements2.autres.inscriptionExpertJudiciaire) ? true : false;
cerfaFields['nonExpertJudiciaire']                      = ($qp228PE2.renseignements2.autres.inscriptionExpertJudiciaire) ? false : true;
cerfaFields['expertJuridiction']                        = $qp228PE2.renseignements2.autres.expertJuridiction;
cerfaFields['dateJuridiction']                          = $qp228PE2.renseignements2.autres.dateJuridiction;
cerfaFields['ouiOrdreEtranger']                         = ($qp228PE2.renseignements2.autres.inscriptionOrdre) ? true : false;
cerfaFields['nonOrdreEtranger']                         = ($qp228PE2.renseignements2.autres.inscriptionOrdre) ? false : true;
cerfaFields['ordreProfessionnelEtranger']               = $qp228PE2.renseignements2.autres.ordreProfessionnelEtranger;
cerfaFields['paysOrdre']                                = $qp228PE2.renseignements2.autres.paysOrdre;

//Demande Autorisation et declaration CNIL
cerfaFields['demandeAutorisation']                      = ($qp228PE2.demandeAutorisationTitre.demandeAutorisationTitre.demandeAutorisation) ? true : false;


//Signature 1
cerfaFields['declarationCNIL']                          = $qp228PE2.signature1.signature1.declarationCNIL;
cerfaFields['attestationCondamnation']                  = $qp228PE2.signature1.signature1.attestationCondamnation;
cerfaFields['attestationObligationsFiscales']           = $qp228PE2.signature1.signature1.attestationObligationsFiscales;
cerfaFields['certificationInformations']                = $qp228PE2.signature1.signature1.certificationInformations;
cerfaFields['attestationDroitsCivils']                  = $qp228PE2.signature1.signature1.attestationDroitsCivils;
cerfaFields['villeSignature1']                          = $qp228PE2.signature1.signature1.villeSignature1;
cerfaFields['dateSignature1']                           = $qp228PE2.signature1.signature1.dateSignature1;
cerfaFields['civNomPrenom']                             = civNomPrenom;
cerfaFields['signatureF']								= $qp228PE2.signature1.signature1.signatureF;


//Declaration Independance
cerfaFields['adresse1']                                 = ($qp228PE2.domicileDeclarant.domicileDeclarant.numeroLibelleVoie !=null ? $qp228PE2.domicileDeclarant.domicileDeclarant.numeroLibelleVoie : '')
														+ ' ' +	($qp228PE2.domicileDeclarant.domicileDeclarant.complementAdresseDeclarant !=null ? $qp228PE2.domicileDeclarant.domicileDeclarant.complementAdresseDeclarant : '')
														+ ' ' +	($qp228PE2.domicileDeclarant.domicileDeclarant.codePostalDeclarant !=null ? $qp228PE2.domicileDeclarant.domicileDeclarant.codePostalDeclarant : '' ) 
														+ ' ' +	($qp228PE2.domicileDeclarant.domicileDeclarant.villeDomicileDeclarant !=null ? $qp228PE2.domicileDeclarant.domicileDeclarant.villeDomicileDeclarant : '');


cerfaFields['adresse2']                                 = ($qp228PE2.domicileDeclarant.domicileDeclarant.numeroLibelleVoie !=null ? $qp228PE2.domicileDeclarant.domicileDeclarant.numeroLibelleVoie : '')
														+ ' ' +	($qp228PE2.domicileDeclarant.domicileDeclarant.complementAdresseDeclarant !=null ? $qp228PE2.domicileDeclarant.domicileDeclarant.complementAdresseDeclarant : '')
														+ ' ' +	($qp228PE2.domicileDeclarant.domicileDeclarant.codePostalDeclarant !=null ? $qp228PE2.domicileDeclarant.domicileDeclarant.codePostalDeclarant : '' ) 
														+ ' ' +	($qp228PE2.domicileDeclarant.domicileDeclarant.villeDomicileDeclarant !=null ? $qp228PE2.domicileDeclarant.domicileDeclarant.villeDomicileDeclarant : '');

if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableIndependant") or Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableSalarie")){
cerfaFields['adresse2'] = '' ;	
}

	
cerfaFields['nomUsuel1']                                = $qp228PE2.etatCivil.identificationDeclarant.nomUsuel;
if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableIndependant") or Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableSalarie")){
cerfaFields['nomUsuel1'] = '' ;	
}	

cerfaFields['prenomDeclarant1']                                = $qp228PE2.etatCivil.identificationDeclarant.prenomDeclarant;
if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableIndependant") or Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableSalarie")){
cerfaFields['prenomDeclarant1'] = '' ;	
}	




cerfaFields['nomUsuel1']                                = $qp228PE2.declarationInd.declarationInd.nomUsuel1;
cerfaFields['prenomDeclarant1']                         = $qp228PE2.declarationInd.declarationInd.prenomDeclarant1	;
cerfaFields['adresse2']                                 = ($qp228PE2.declarationInd.declarationInd.domicileDeclarant2.numeroLibelleVoie2 !=null ? $qp228PE2.declarationInd.declarationInd.domicileDeclarant2.numeroLibelleVoie2 :'')+" "+($qp228PE2.declarationInd.declarationInd.domicileDeclarant2.complementAdresseDeclarant2 !=null ? $qp228PE2.declarationInd.declarationInd.domicileDeclarant2.complementAdresseDeclarant2 :'');
cerfaFields['adresse3']                                 = ($qp228PE2.declarationInd.declarationInd.domicileDeclarant2.codePostalDeclarant2 !=null ? $qp228PE2.declarationInd.declarationInd.domicileDeclarant2.codePostalDeclarant2 :'')+" "+($qp228PE2.declarationInd.declarationInd.domicileDeclarant2.villeDomicileDeclarant2 !=null ? $qp228PE2.declarationInd.declarationInd.domicileDeclarant2.villeDomicileDeclarant2 :'')+" "+($qp228PE2.declarationInd.declarationInd.domicileDeclarant2.paysDeclarant2 !=null ? $qp228PE2.declarationInd.declarationInd.domicileDeclarant2.paysDeclarant2 :'');								
cerfaFields['professionActuelle']                       = $qp228PE2.declarationInd.declarationInd.professionActuelle;



cerfaFields['declarationAttacheSalariale']              = $qp228PE2.declarationInd.declarationInd.declarationAttacheSalariale;
cerfaFields['declarationIndependance']                  = $qp228PE2.declarationInd.declarationInd.declarationIndependance;
cerfaFields['declarationSalarie']                       = Value('id').of($qp228PE2.declarationInd.declarationInd.declarationProfession).eq('declarationSalarie');
cerfaFields['declarationDirigeantSociete']              = Value('id').of($qp228PE2.declarationInd.declarationInd.declarationProfession).eq('declarationDirigeantSociete');
cerfaFields['declarationTitreIndependant']              = Value('id').of($qp228PE2.declarationInd.declarationInd.declarationProfession).eq('declarationTitreIndependant');
cerfaFields['declarationFonction']                      = $qp228PE2.declarationInd.declarationInd.declarationFonction;
cerfaFields['dateInd']                                  = $qp228PE2.declarationInd.declarationInd.dateInd;
cerfaFields['nomPrenomDirigeant']                       = $qp228PE2.declarationInd.declarationInd.nomPrenomDirigeant;
cerfaFields['nomSocieteDirigeant']                      = $qp228PE2.declarationInd.declarationInd.nomSocieteDirigeant;
cerfaFields['nomSocieteAGC']                            = $qp228PE2.declarationInd.declarationInd.nomSocieteAGC;
cerfaFields['certificationConnaissance']                = $qp228PE2.declarationInd.declarationInd.certificationConnaissance;

cerfaFields['villeSignature3']                          = $qp228PE2.signature1.signature1.villeSignature1;
cerfaFields['dateSignature3']                           = $qp228PE2.signature1.signature1.dateSignature1;
cerfaFields['certifInformations']                       = $qp228PE2.signature1.signature1.certificationInformations;
if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableIndependant") or Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("expertComptableSalarie")){
cerfaFields['villeSignature3']                          = '';
cerfaFields['dateSignature3']                           = '';
cerfaFields['certifInformations']                       = '';
}

//Questionnaire
cerfaFields['nomUsuel2']                                = $qp228PE2.etatCivil.identificationDeclarant.nomUsuel;
if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("statutEIRL")) {
cerfaFields['nomUsuel2'] = '' ;	
}

cerfaFields['prenomDeclarant2']                         = $qp228PE2.etatCivil.identificationDeclarant.prenomDeclarant;
if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("statutEIRL")) {
cerfaFields['prenomDeclarant2'] = '' ;	
}
cerfaFields['nomJeuneFille']                            = ($qp228PE2.questionnaire.questionnaire.nomJeuneFille !=null ? $qp228PE2.questionnaire.questionnaire.nomJeuneFille : '');
cerfaFields['nomPrenomPere']                            = $qp228PE2.questionnaire.questionnaire.nomPrenomPere;
cerfaFields['nomPrenomMere']                            = $qp228PE2.questionnaire.questionnaire.nomPrenomMere;
cerfaFields['DepVilleNaissanceDeclarant']               = $qp228PE2.etatCivil.naissanceDeclarant.departementDeclarant + ' ' + $qp228PE2.etatCivil.naissanceDeclarant.villeNaissanceDeclarant;

if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("statutEIRL")) {
cerfaFields['DepVilleNaissanceDeclarant'] = '' ;	
}
cerfaFields['dateNaissanceDeclarant2']                   = $qp228PE2.etatCivil.naissanceDeclarant.dateNaissanceDeclarant;
if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("statutEIRL")) {
cerfaFields['dateNaissanceDeclarant2'] = '' ;	
}
cerfaFields['nationalite']                              = $qp228PE2.questionnaire.questionnaire.nationalite;

cerfaFields['adresseAnneeCours']                        = $qp228PE2.questionnaire.questionnaire.adresseAnneeCours;
cerfaFields['villeAdresseCours']                        = $qp228PE2.questionnaire.questionnaire.villeAdresseCours;
cerfaFields['adresseActuelle']                          = $qp228PE2.questionnaire.questionnaire.adresseActuelle;
cerfaFields['villeAdresseActuelle']                     = $qp228PE2.questionnaire.questionnaire.villeAdresseActuelle;
cerfaFields['adresseProfessionnelleActuelle']           = $qp228PE2.questionnaire.questionnaire.adresseProfessionnelleActuelle;
cerfaFields['villeAdresseProfessionnelle']              = $qp228PE2.questionnaire.questionnaire.villeAdresseProfessionnelle;
cerfaFields['telephone']                                = $qp228PE2.questionnaire.questionnaire.telephone;

cerfaFields['adresseServicesFiscaux2']                  = $qp228PE2.questionnaire.servicesFiscaux.adresseServicesFiscaux2;
cerfaFields['adresseServicesFiscaux1']                  = $qp228PE2.questionnaire.servicesFiscaux.adresseServicesFiscaux1;
cerfaFields['villeServicesFiscaux1']                    = $qp228PE2.questionnaire.servicesFiscaux.villeServicesFiscaux1;
cerfaFields['villeServicesFiscaux2']                    = $qp228PE2.questionnaire.servicesFiscaux.villeServicesFiscaux2;

cerfaFields['villeSignature2']                          = $qp228PE2.signature1.signature1.villeSignature1;
cerfaFields['dateSignature2']                           = $qp228PE2.signature1.signature1.dateSignature1;
cerfaFields['certificationInformations2']               = $qp228PE2.signature1.signature1.certificationInformations;

if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("statutEIRL")) {
cerfaFields['villeSignature2']                          = '';
cerfaFields['dateSignature2']                           = '';
cerfaFields['certificationInformations2']               = '';
}

cerfaFields['civNomPrenom2'] = civNomPrenom ;
if (Value('id').of($qp228PE2.inscription.inscription.inscriptionQualite).eq("statutEIRL")) {
cerfaFields['civNomPrenom2'] = '' ;	
}


//var pdfModel = nash.doc.load('models/cerfa_11542-05.pdf').apply(formFields);

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp228PE2.signature1.signature1.dateSignature1,
		civNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp228PE2.signature1.signature1.dateSignature1,
		autoriteHabilitee : 'Conseil supérieur de l\'Ordre des experts-comptables',
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire d’inscription au tableau de l’ordre.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNonCondamnation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);

/*
 *PJ Nom Usuel ancien nom marital : oui
 */
var pj=$qp228PE2.etatCivil.identificationDeclarant.nomUsuelMarital;
if(pj) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjNomUsuel);
}
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Professionnel_expertise_comptable_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Professionnel d\'expertise-comptable - Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de professionnel d\'expertise-comptable.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Professionnel d\'expertise-comptable - Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de professionnel d\'expertise-comptable.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});