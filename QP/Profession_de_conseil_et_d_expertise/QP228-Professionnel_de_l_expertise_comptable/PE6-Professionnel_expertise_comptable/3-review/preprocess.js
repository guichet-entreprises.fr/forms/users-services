var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp228PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp228PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp228PE6.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp228PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp228PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp228PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp228PE6.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp228PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp228PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp228PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp228PE6.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp228PE6.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp228PE6.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp228PE6.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp228PE6.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp228PE6.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp228PE6.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp228PE6.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp228PE6.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp228PE6.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp228PE6.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp228PE6.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp228PE6.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp228PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp228PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp228PE6.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']            = 'Professionnel de l\'expertise-comptable';

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp228PE6.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp228PE6.signature.signature.dateSignature,
		autoriteHabilitee : 'Conseil supérieur de l\'Ordre des experts-comptables',
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNonCondamnation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Professionnel_expertise_comptable_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Professionnel de l\'expertise-comptable - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de professionnel de l\'expertise-comptable',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
