var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']           = $qp083PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp083PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp083PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']   	   = $qp083PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant +', ' + $qp083PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                 = $qp083PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']               = $qp083PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']   					= $qp083PE5.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp083PE5.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null?', ' + $qp083PE5.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : '');
cerfaFields['villePays']           			= ($qp083PE5.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp083PE5.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant +' ' : ' ') + $qp083PE5.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ', ' + $qp083PE5.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']            	= $qp083PE5.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp083PE5.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']          			= $qp083PE5.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp083PE5.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp083PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp083PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp083PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp083PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp083PE5.etatCivil.identificationDeclarant.prenomDeclarant;
  


var cerfa = pdf.create('models/courrier INPI LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('CPI demande LPS.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Conseil en propriété industrielle - renouvellement de déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de renouvellement de la déclaration préalable en vue d\'une libre prestation de services pour la profession de conseil en propriété industrielle',
            description : 'Voici le courrier obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
