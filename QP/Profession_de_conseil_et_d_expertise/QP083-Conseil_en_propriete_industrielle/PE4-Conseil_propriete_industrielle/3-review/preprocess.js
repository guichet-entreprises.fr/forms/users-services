var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']          	= $qp083PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp083PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp083PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp083PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp083PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp083PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp083PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp083PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp083PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp083PE4.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp083PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp083PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp083PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp083PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp083PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp083PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp083PE4.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp083PE4.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp083PE4.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp083PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp083PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp083PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp083PE4.etatCivil.identificationDeclarant.prenomDeclarant;



var cerfa = pdf.create('models/courrier INPI LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('CPI demande LPS.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Conseil en propriété industrielle - déclaration préalable en vue d\’une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de déclaration préalable en vue d\’une libre prestation de services pour la profession de conseil en propriété industrielle',
            description : 'Voici le courrier obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
