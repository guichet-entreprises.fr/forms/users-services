var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp002PE01.etatCivil.identificationDeclarant.civilite + ' ' + $qp002PE01.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp002PE01.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']  			 	= $qp002PE01.etatCivil.identificationDeclarant.civilite + ' ' + $qp002PE01.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp002PE01.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance'] 			 	= $qp002PE01.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp002PE01.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']        			  	= $qp002PE01.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']       			 	= $qp002PE01.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle

var regionExer = $qp002PE01.adresse.regionExercice.regionExe;

cerfaFields['adresse'] 							= $qp002PE01.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp002PE01.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp002PE01.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp002PE01.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp002PE01.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp002PE01.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp002PE01.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']    				= $qp002PE01.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']      				= $qp002PE01.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp002PE01.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['regionExercice']          			= regionExer;

//signature
cerfaFields['date']                				= $qp002PE01.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp002PE01.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp002PE01.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp002PE01.etatCivil.identificationDeclarant.civilite + ' ' + $qp002PE01.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp002PE01.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp002PE01.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp002PE01.signature.signature.dateSignature
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertifFinStage);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExamAptitude);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Administrateur_judiciaire_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Administrateur judiciaire - demande de reconnaissance de qualifications professionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'administrateur judiciaire est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});