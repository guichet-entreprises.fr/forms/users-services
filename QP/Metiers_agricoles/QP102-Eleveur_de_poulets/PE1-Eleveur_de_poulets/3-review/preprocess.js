var formFields = {};

//Etat civil

//var ident = qp102PE1.identification.identificationDeclarant

formFields['dateNaissanceDeclarant']     = $qp102PE1.identification.identificationDeclarant.dateNaissanceDeclarant;
formFields['lieuNaissanceDeclarant']     = $qp102PE1.identification.identificationDeclarant.lieuNaissanceDeclarant;
formFields['paysNaissanceDeclarant']     = $qp102PE1.identification.identificationDeclarant.paysNaissanceDeclarant;
formFields['nomDeclarant']               = $qp102PE1.identification.identificationDeclarant.nomDeclarant;
formFields['prenomDeclarant']            = $qp102PE1.identification.identificationDeclarant.prenomDeclarant;
formFields['civiliteDeclarant']          = $qp102PE1.identification.identificationDeclarant.civiliteDeclarant;

var civNomPrenom = $qp102PE1.identification.identificationDeclarant.civiliteDeclarant + ' ' + $qp102PE1.identification.identificationDeclarant.nomDeclarant + ' ' + $qp102PE1.identification.identificationDeclarant.prenomDeclarant

//Adresse

//var adresse = qp102PE1.identification.coordonneesDeclarant

formFields['adresseDeclarant']           = $qp102PE1.identification.coordonneesDeclarant.adresseDeclarant;
formFields['adresseComplementDeclarant'] = $qp102PE1.identification.coordonneesDeclarant.adresseComplementDeclarant;
formFields['codePostalDeclarant']        = $qp102PE1.identification.coordonneesDeclarant.codePostalDeclarant;
formFields['communeDeclarant']           = $qp102PE1.identification.coordonneesDeclarant.communeDeclarant;
formFields['telephoneFixeDeclarant']     = $qp102PE1.identification.coordonneesDeclarant.telephoneFixeDeclarant;
formFields['telephoneMobileDeclarant']   = $qp102PE1.identification.coordonneesDeclarant.telephoneMobileDeclarant;
formFields['emailDeclarant']             = $qp102PE1.identification.coordonneesDeclarant.emailDeclarant;

// Signature

//var sign = qp102PE1.signature.signatureDeclarant

formFields['engagementSignature1']       = $qp102PE1.signature.signatureDeclarant.engagementSignature1;
formFields['engagementSignature2']       = $qp102PE1.signature.signatureDeclarant.engagementSignature2;
formFields['engagementSignature3']       = $qp102PE1.signature.signatureDeclarant.engagementSignature3;
formFields['engagementSignature4']       = $qp102PE1.signature.signatureDeclarant.engagementSignature4;
formFields['dateSignatureDeclarant']     = $qp102PE1.signature.signatureDeclarant.dateSignatureDeclarant;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp102PE1.signature.signatureDeclarant.dateSignatureDeclarant,
		autoriteHabilitee :"Direction départementale en charge de la protection des populations",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));


/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/eleveur_poulet_LE.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Eleveur_poulets_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Eleveur de poulets - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'éleveur de poulets',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});


