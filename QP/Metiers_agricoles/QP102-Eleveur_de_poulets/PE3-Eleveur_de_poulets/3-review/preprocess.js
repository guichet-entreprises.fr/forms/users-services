var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp102PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp102PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp102PE3.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp102PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp102PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp102PE3.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp102PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp102PE3.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp102PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp102PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp102PE3.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp102PE3.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp102PE3.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp102PE3.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp102PE3.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp102PE3.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp102PE3.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp102PE3.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp102PE3.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp102PE3.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['lieuExercice']					= $qp102PE3.signature.signature.depExercice;
cerfaFields['date']                			= $qp102PE3.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp102PE3.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp102PE3.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp102PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp102PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp102PE3.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp102PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp102PE3.signature.signature.dateSignature,
		autoriteHabilitee :"Direction départementale en charge de la protection des populations",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Eleveur_de_poulets_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Eleveur de poulets - déclaration préalable en vue dune libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'eleveur de poulets',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
