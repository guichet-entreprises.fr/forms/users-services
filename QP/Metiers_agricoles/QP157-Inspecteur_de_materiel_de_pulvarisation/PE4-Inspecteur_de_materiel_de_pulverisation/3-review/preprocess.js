var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp157PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp157PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp157PE4.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $qp157PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp157PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp157PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp157PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp157PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp157PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp157PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp157PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp157PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp157PE4.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp157PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp157PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp157PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp157PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp157PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp157PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp157PE4.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp157PE4.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp157PE4.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp157PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp157PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp157PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp157PE4.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Inspecteur de matériel de pulvérisation de produits phytopharmaceutiques"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp157PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp157PE4.signature.signature.dateSignature,
		autoriteHabilitee :"DRAAF",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('inspecteur_matériel_pulvérisation_produits_phytopharmaceutiques_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Inspecteur de matériel de pulvérisation de produits phytopharmaceutiques - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'inspecteur de matériel de pulvérisation de produits phytopharmaceutiques',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
