var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp131PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp131PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp131PE3.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']            = $qp131PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp131PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp131PE3.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']           = $qp131PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp131PE3.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                  = $qp131PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                = $qp131PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                      = $qp131PE3.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp131PE3.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp131PE3.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                    = ($qp131PE3.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp131PE3.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp131PE3.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp131PE3.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']              = $qp131PE3.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                = $qp131PE3.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                     = $qp131PE3.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                         = $qp131PE3.signature.signature.dateSignature;
cerfaFields['signature']                    = $qp131PE3.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp131PE3.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']   = $qp131PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp131PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp131PE3.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Ajout du cerfa
 */
var finalDoc = nash.doc //
    .load('models/Expert_foncier_et_agricole.pdf') //
    .apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLPS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Expert_foncier_et_agricole_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Expert foncier et agricole - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'expert foncier et agricole',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
