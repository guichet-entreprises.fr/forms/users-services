attachment('pjID', 'pjID');
attachment('pjDiplomes', 'pjDiplomes');
attachment('pjJustificatifExercice', 'pjJustificatifExercice',{mandatory : "false"});
attachment('pjExpertises', 'pjExpertises');
attachment('pjExpertisesSignificative', 'pjExpertisesSignificative');
attachment('pjStages', 'pjStages');
attachment('pjAssurancePro', 'pjAssurancePro');
attachment('pjCV', 'pjCV');
attachment('pjLettrePresentation', 'pjLettrePresentation');
attachment('pjCasier', 'pjCasier');
attachment('pjAttestationExigences', 'pjAttestationExigences',{mandatory : "false"});

//Siret pour Autoentrepreneur
var pj=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;
if(Value('id').of(pj).contains('propreAutoEntrepreneur1')) {
    attachment('pjSiretExpertale', 'pjSiretExpertale', { mandatory:"true"});
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('propreAutoEntrepreneur3')) {
    attachment('pjSiretNonExpertale', 'pjSiretNonExpertale', { mandatory:"true"});
}

//Statut et KBIS pour Société&associé, salarié, gérant
var pj=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;
if(Value('id').of(pj).contains('societeAssocie1')) {
    attachment('pjStatutExpertale', 'pjStatutExpertale', { mandatory:"true"});
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('societeAssocie3')) {
    attachment('pjStatutNonExpertale', 'pjStatutNonExpertale', { mandatory:"true"});
}

var pj=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;
if(Value('id').of(pj).contains('salarie1')) {
    attachment('pjStatutExpertaleSalarie', 'pjStatutExpertaleSalarie', { mandatory:"true"});
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('salarie3')) {
    attachment('pjStatutNonExpertaleSalarie', 'pjStatutNonExpertaleSalarie', { mandatory:"true"});
}
var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('ouiGerantActionnaire')) {
    attachment('pjStatutGerant', 'pjStatutGerant', { mandatory:"true"});
}

//Fiche poste pour salarié
var pj=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;
if(Value('id').of(pj).contains('salarie1')) {
    attachment('pjFichePosteExpertale', 'pjFichePosteExpertale', { mandatory:"true"});
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('salarie3')) {
    attachment('pjFichePosteNonExpertale', 'pjFichePosteNonExpertale', { mandatory:"true"});
}

//Carte gestion immobilière
var pj=$qp131PE2.gestionTransaction.activiteGestionTransaction.gestionImmobiliere;
if(Value('id').of(pj).contains('ouiGestionImmobiliere')) {
    attachment('pjCarteGestion', 'pjCarteGestion', { mandatory:"true"});
}

//Carte transaction immobilière
var pj=$qp131PE2.gestionTransaction.activiteGestionTransaction.transactionImmobiliere;
if(Value('id').of(pj).contains('ouiTransactionImmobiliere')) {
    attachment('pjCarteTransaction', 'pjCarteTransaction', { mandatory:"true"});
}

































