var cerfaFields = {};

//etatCivil

var civNomPrenom = $qp131PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp131PE2.etatCivil.identificationDeclarant.nomNaissance + ' ' + $qp131PE2.etatCivil.identificationDeclarant.prenom;
var civilite = $qp131PE2.etatCivil.identificationDeclarant.civilite;

cerfaFields['foncierAgricole']            = true;
cerfaFields['foncier']                    = false;
cerfaFields['agricole']                   = false;
cerfaFields['civilite']                   = civilite;
cerfaFields['nomNaissance']               = $qp131PE2.etatCivil.identificationDeclarant.nomNaissance;
cerfaFields['nomUsage']                   = $qp131PE2.etatCivil.identificationDeclarant.nomUsage;
cerfaFields['prenom']                     = $qp131PE2.etatCivil.identificationDeclarant.prenom;
cerfaFields['dateNaissance']              = $qp131PE2.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['nationalite']                = $qp131PE2.etatCivil.identificationDeclarant.nationalite;
cerfaFields['lieuNaissance']              = $qp131PE2.etatCivil.identificationDeclarant.communeNaissance + ', ' + $qp131PE2.etatCivil.identificationDeclarant.paysNaissance;

//adresseProfessionnelle
cerfaFields['telephoneProfessionnel']     = $qp131PE2.adresse.adresseProfesionnelle.telephoneProfessionnel;
cerfaFields['emailProfessionnel']         = $qp131PE2.adresse.adresseProfesionnelle.emailProfessionnel;
cerfaFields['adressePostaleProfessionnelle']     = $qp131PE2.adresse.adresseProfesionnelle.adressePostaleProfessionnelle;
cerfaFields['codePostalProfessionnel']    = $qp131PE2.adresse.adresseProfesionnelle.codePostalProfessionnel;
cerfaFields['communeProfessionnel']       = $qp131PE2.adresse.adresseProfesionnelle.communeProfessionnel + ', ' + $qp131PE2.adresse.adresseProfesionnelle.paysProfessionnel;;
cerfaFields['mobileProfessionnel']        = $qp131PE2.adresse.adresseProfesionnelle.mobileProfessionnel;
cerfaFields['faxProfessionnel']           = $qp131PE2.adresse.adresseProfesionnelle.faxProfessionnel;

//adressePersonnelle
cerfaFields['adressePostalePersonnelle']  = $qp131PE2.adressePerso.adressePersonnelle.adressePostalePersonnelle;
cerfaFields['codePostalPersonnel']        = $qp131PE2.adressePerso.adressePersonnelle.codePostalPersonnel;
cerfaFields['communePersonnel']           = $qp131PE2.adressePerso.adressePersonnelle.communePersonnel + ', ' + $qp131PE2.adressePerso.adressePersonnelle.paysPersonnel;
cerfaFields['telephonePersonnel']         = $qp131PE2.adressePerso.adressePersonnelle.telephonePersonnel;
cerfaFields['emailPersonnel']             = $qp131PE2.adressePerso.adressePersonnelle.emailPersonnel;
cerfaFields['mobilePersonnel']            = $qp131PE2.adressePerso.adressePersonnelle.mobilePersonnel;

//Departement du domicile professionnel
cerfaFields['departement']                = $qp131PE2.departementDomicile.departementDomicileProfessionnel.departementExercice;

//Formation
cerfaFields['diplomeTitre1']              = $qp131PE2.diplomes.diplomes0.diplomeTitre1;
cerfaFields['etablissement1']             = $qp131PE2.diplomes.diplomes0.etablissement1;
cerfaFields['dateObtention1']             = $qp131PE2.diplomes.diplomes0.dateObtention1;
cerfaFields['diplomeTitre2']              = $qp131PE2.diplomes.diplomes0.diplomeTitre2;
cerfaFields['etablissement2']             = $qp131PE2.diplomes.diplomes0.etablissement2;
cerfaFields['dateObtention2']             = $qp131PE2.diplomes.diplomes0.dateObtention2;
cerfaFields['diplomeTitre3']              = $qp131PE2.diplomes.diplomes0.diplomeTitre3;
cerfaFields['etablissement3']             = $qp131PE2.diplomes.diplomes0.etablissement3;
cerfaFields['dateObtention3']             = $qp131PE2.diplomes.diplomes0.dateObtention3;


cerfaFields['dateFormation0']             = '';
cerfaFields['lieuFormation0']             = '';
cerfaFields['organismeFormation0']        = '';
cerfaFields['sujet0']                     = '';
cerfaFields['dateFormation1']             = '';
cerfaFields['lieuFormation1']             = '';
cerfaFields['organismeFormation1']        = '';
cerfaFields['sujet1']                     = '';
cerfaFields['dateFormation2']             = '';
cerfaFields['lieuFormation2']             = '';
cerfaFields['organismeFormation2']        = '';
cerfaFields['sujet2']                     = '';
cerfaFields['dateFormation3']             = '';
cerfaFields['lieuFormation3']             = '';
cerfaFields['organismeFormation3']        = '';
cerfaFields['sujet3']                     = '';

for (var i= 0; i < $qp131PE2.formations.formation.size(); i++ ){
	cerfaFields['dateFormation'+i]             = $qp131PE2.formations.formation[i].dateFormation != null ? $qp131PE2.formations.formation[i].dateFormation : '' ;
	cerfaFields['lieuFormation'+i]             = $qp131PE2.formations.formation[i].lieuFormation != null ? $qp131PE2.formations.formation[i].lieuFormation : '';
	cerfaFields['organismeFormation'+i]        = $qp131PE2.formations.formation[i].organismeFormation != null ? $qp131PE2.formations.formation[i].organismeFormation : '';
	cerfaFields['sujet'+i]   				   = $qp131PE2.formations.formation[i].sujet != null ? $qp131PE2.formations.formation[i].sujet : '';
}

//Pratique expertale
	//responsabilté d'un tiers
cerfaFields['dateExpertise1']             = $qp131PE2.expertise.expertiseTiers.dateExpertise1;
cerfaFields['nombreExpertise1']           = $qp131PE2.expertise.expertiseTiers.nombreExpertise1;
cerfaFields['nombreAmiable1']             = $qp131PE2.expertise.expertiseTiers.nombreAmiable1;
cerfaFields['datesAmi1']                  =$qp131PE2.expertise.expertiseTiers.dateAmi1;
cerfaFields['datesAmi2']                  =$qp131PE2.expertise.expertiseTiers.dateAmi2;
cerfaFields['datesAmi3']                  =$qp131PE2.expertise.expertiseTiers.dateAmi3
cerfaFields['datesAmi4']                  =$qp131PE2.expertise.expertiseTiers.dateAmi4;

cerfaFields['nombreJudiciaire1']         = $qp131PE2.expertise.expertiseTiers.nombreJudiciaire1;
cerfaFields['datesJud1']                 = $qp131PE2.expertise.expertiseTiers.dateJud1;
cerfaFields['datesJud2']                 = $qp131PE2.expertise.expertiseTiers.dateJud2;
cerfaFields['datesJud3']                 = $qp131PE2.expertise.expertiseTiers.dateJud3;
cerfaFields['datesJud4']                 = $qp131PE2.expertise.expertiseTiers.dateJud4;

cerfaFields['nombreAssurance1']          = $qp131PE2.expertise.expertiseTiers.nombreAssurance1;
cerfaFields['datesAss1']                 = $qp131PE2.expertise.expertiseTiers.dateAss1;
cerfaFields['datesAss2']                 = $qp131PE2.expertise.expertiseTiers.dateAss2;
cerfaFields['datesAss3']                 = $qp131PE2.expertise.expertiseTiers.dateAss3;
cerfaFields['datesAss4']                 = $qp131PE2.expertise.expertiseTiers.dateAss4;

 //Titre personnel
cerfaFields['dateExpertise2']        	  = $qp131PE2.expertise2.expertise2.dateExpertise2;
cerfaFields['nombreExpertise2']      	  = $qp131PE2.expertise2.expertise2.nombreExpertise2;
cerfaFields['nombreAmiable2']             = $qp131PE2.expertise2.expertise2.nombreAmiable2;
cerfaFields['datesAmia1']                 = $qp131PE2.expertise2.expertise2.dateAmia1;
cerfaFields['datesAmia2']                 = $qp131PE2.expertise2.expertise2.dateAmia2;
cerfaFields['datesAmia3']                 = $qp131PE2.expertise2.expertise2.dateAmia3;
cerfaFields['datesAmia4']                 = $qp131PE2.expertise2.expertise2.dateAmia4;

cerfaFields['nombreAssurance2']           = $qp131PE2.expertise2.expertise2.nombreAssurance2;
cerfaFields['datesAssu1']                  = $qp131PE2.expertise2.expertise2.dateAssu1;
cerfaFields['datesAssu2']                  = $qp131PE2.expertise2.expertise2.dateAssu2;
cerfaFields['datesAssu3']                  = $qp131PE2.expertise2.expertise2.dateAssu3;
cerfaFields['datesAssu4']                  = $qp131PE2.expertise2.expertise2.dateAssu4;

cerfaFields['nombreJudiciaire2']           = $qp131PE2.expertise2.expertise2.nombreJudiciaire2;
cerfaFields['datesJudi1']                  = $qp131PE2.expertise2.expertise2.dateJudi1;
cerfaFields['datesJudi2']                  = $qp131PE2.expertise2.expertise2.dateJudi2;
cerfaFields['datesJudi3']                  = $qp131PE2.expertise2.expertise2.dateJudi3;
cerfaFields['datesJudi4']                  = $qp131PE2.expertise2.expertise2.dateJudi4;

//Inscription liste

cerfaFields['ouiExpert']                  = Value('id').of($qp131PE2.liste.listeExpert.inscription).eq("ouiExpert") ? true : false;
cerfaFields['nonExpert']                  = Value('id').of($qp131PE2.liste.listeExpert.inscription).eq("nonExpert") ? true : false;
cerfaFields['juridiction1']               = $qp131PE2.liste.listeExpert.juridiction1;
cerfaFields['dateInscription1']           = $qp131PE2.liste.listeExpert.dateInscription1;
cerfaFields['nomenclature1']              = $qp131PE2.liste.listeExpert.nomenclature1;
cerfaFields['juridiction2']               = $qp131PE2.liste.listeExpert.juridiction2;
cerfaFields['juridiction3']               = $qp131PE2.liste.listeExpert.juridiction3;
cerfaFields['dateInscription2']           = $qp131PE2.liste.listeExpert.dateInscription2;
cerfaFields['dateInscription3']           = $qp131PE2.liste.listeExpert.dateInscription3;
cerfaFields['nomenclature2']              = $qp131PE2.liste.listeExpert.nomenclature2;
cerfaFields['nomenclature3']              = $qp131PE2.liste.listeExpert.nomenclature3;

//Forme activité expertale
cerfaFields['responsabiliteTiers']        = Value('id').of($qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle).eq("responsabiliteTiers") ? true : false;
cerfaFields['propreAutoEntrepreneur1']    = Value('id').of($qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle).eq("propreAutoEntrepreneur1") ? true : false;
cerfaFields['societeAssocie1']            = Value('id').of($qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle).eq("societeAssocie1") ? true : false;
cerfaFields['salarie1']                   = Value('id').of($qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle).eq("salarie1") ? true : false;
cerfaFields['propreAutoEntrepreneur2']    = Value('id').of($qp131PE2.activite.activiteExpertale.activiteExpertaleFormeFuture).eq("propreAutoEntrepreneur2") ? true : false;
cerfaFields['societeAssocie2']            = Value('id').of($qp131PE2.activite.activiteExpertale.activiteExpertaleFormeFuture).eq("societeAssocie2") ? true : false;
 
//Assurance
cerfaFields['ouiAssuranceResponsabilite'] = Value('id').of($qp131PE2.assurance.assuranceResponsabilite.assuranceResponsabiliteCivile).eq("ouiAssuranceResponsabilite") ? true : false;
cerfaFields['nonAssuranceResponsabilite'] = Value('id').of($qp131PE2.assurance.assuranceResponsabilite.assuranceResponsabiliteCivile).eq("nonAssuranceResponsabilite") ? true : false;

//Activite autre que expertale
cerfaFields['ouiActiviteProfessionnelle'] = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.activiteProfessionnelle0).eq("ouiActiviteProfessionnelle") ? true : false;
cerfaFields['nonActiviteProfessionnelle'] = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.activiteProfessionnelle0).eq("nonActiviteProfessionnelle") ? true : false;
cerfaFields['activiteProfesionnelle1']     = $qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.activiteProfessionnelle1;
cerfaFields['tempsExpertiseAnnuelle']     = $qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.tempsExpertiseAnnuelle;
cerfaFields['tempsExpertiseMensuelle']    = $qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.tempsExpertiseMensuelle;


cerfaFields['propreAutoEntrepreneur3']    = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle).eq("propreAutoEntrepreneur3") ? true : false;
cerfaFields['societeAssocie3']            = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle).eq("societeAssocie3") ? true : false;
cerfaFields['salarie3']                   = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle).eq("salarie3") ? true : false;
cerfaFields['ouiActivite']                = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.activiteNonExpertale).eq("ouiActivite") ? true : false;
cerfaFields['nonActivite']                = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.activiteNonExpertale).eq("nonActivite") ? true : false;
cerfaFields['activiteProfessionnelle1']    = $qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.activiteProfessionnelle1;

cerfaFields['ouiGerantActionnaire']       = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.gerantActionnaire0).eq("ouiGerantActionnaire") ? true : false;
cerfaFields['nonGerantActionnaire']       = Value('id').of($qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.gerantActionnaire0).eq("nonGerantActionnaire") ? true : false;
cerfaFields['entreprise']                = $qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.entreprise;

cerfaFields['fonctionsElectives']         = $qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.fonctionsElectives;

//Activités gestion et transaction immoblière
cerfaFields['ouiGestionImmobiliere']      = Value('id').of($qp131PE2.gestionTransaction.activiteGestionTransaction.gestionImmobiliere).eq("ouiGestionImmobiliere") ? true : false;
cerfaFields['nonGestionImmobiliere']      = Value('id').of($qp131PE2.gestionTransaction.activiteGestionTransaction.gestionImmobiliere).eq("nonGestionImmobiliere") ? true : false;
cerfaFields['ouiTransactionImmobiliere']  = Value('id').of($qp131PE2.gestionTransaction.activiteGestionTransaction.transactionImmobiliere).eq("ouiTransactionImmobiliere") ? true : false;
cerfaFields['nonTransactionImmobiliere']  = Value('id').of($qp131PE2.gestionTransaction.activiteGestionTransaction.transactionImmobiliere).eq("nonTransactionImmobiliere") ? true : false;
cerfaFields['ouiActiviteGestion']         = Value('id').of($qp131PE2.gestionTransaction.activiteGestionTransaction.activiteGestionImmobiliere).eq("ouiActiviteGestion") ? true : false;
cerfaFields['nonActiviteGestion']         = Value('id').of($qp131PE2.gestionTransaction.activiteGestionTransaction.activiteGestionImmobiliere).eq("nonActiviteGestion") ? true : false;
cerfaFields['ouiActiviteTransaction']     = Value('id').of($qp131PE2.gestionTransaction.activiteGestionTransaction.activiteTransactionImmobiliere).eq("ouiActiviteTransaction") ? true : false;
cerfaFields['nonActiviteTransaction']     = Value('id').of($qp131PE2.gestionTransaction.activiteGestionTransaction.activiteTransactionImmobiliere).eq("nonActiviteTransaction") ? true : false;


//Signature
cerfaFields['declarationFonction']        = true;
cerfaFields['declarationPenale']          = true;
cerfaFields['declarationFaillite']        = true;
cerfaFields['certifcationHonneur']        = true;
cerfaFields['dateSignature']              = $qp131PE2.signature.signature.dateSignature;
cerfaFields['villeSignature']             = $qp131PE2.signature.signature.villeSignature;
cerfaFields['nomPrenom']                  = civNomPrenom;
cerfaFields['cachetHonneur']              = 'Je déclare sur l’honneur l\'exactitude des informations de la formalité et signe la présente déclaration.';


cerfaFields['libelleProfession']				= "Expert foncier et agricole"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp131PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp131PE2.signature.signature.dateSignature,
		autoriteHabilitee :"CNEFAF",
		demandeContexte : "Demande d'inscription sur liste nationale",
		civiliteNomPrenom : civNomPrenom
	});
	
	/*
 * Chargement du courrier Déclaration sur l'honneur 
 */
 
var finalDoc2 = nash.doc //
	.load('models/Déclaration sur l\'honneur.pdf') //
	.apply({
		villeSignature: $qp131PE2.signature.signature.villeSignature,
		nomPrenom : civNomPrenom,
		dateSignature: $qp131PE2.signature.signature.dateSignature,
		declarationExigences : true
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Dossier CNEFAF.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 *Declaration Honneur si coché Associé/société
 */
var pj1=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;


var pj2=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj2).contains('societeAssocie3') || Value('id').of(pj1).contains('societeAssocie1')) {
	finalDoc.append(finalDoc2.save('cerfa2.pdf'));
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExpertises);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExpertisesSignificative);
appendPj($attachmentPreprocess.attachmentPreprocess.pjStages);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLettrePresentation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationExigences);

/*
 *PJ SIRET si coché : Autoentrepreneur
 */
var pj=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;
if(Value('id').of(pj).contains('propreAutoEntrepreneur1')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSiretExpertale);
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('propreAutoEntrepreneur3')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjSiretNonExpertale);
}

/*
 *PJ Statut et Kbis si coché : Société&associé, salarié
 */
var pj=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;
if(Value('id').of(pj).contains('societeAssocie1')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjStatutExpertale);
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('societeAssocie3')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjStatutNonExpertale);
}

var pj=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;
if(Value('id').of(pj).contains('salarie1')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjStatutExpertaleSalarie);
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('salarie3')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjStatutNonExpertaleSalarie);
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('ouiGerantActionnaire')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjStatutGerant);
}


/*
 *PJ Fiche de poste si coché : salarié
 */
 
 var pj=$qp131PE2.activite.activiteExpertale.activiteExpertaleFormeActuelle;
if(Value('id').of(pj).contains('salarie1')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjFichePosteExpertale);
}

var pj=$qp131PE2.activiteNonExpertale.activiteNonExpertaleProfessionnelle.formeActiviteNonExpertaleFormeActuelle;
if(Value('id').of(pj).contains('salarie3')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjFichePosteNonExpertale);
}

/*
 *PJ Carte gestion immobilière si coché : OUI
 */
 
 var pj=$qp131PE2.gestionTransaction.activiteGestionTransaction.gestionImmobiliere;
if(Value('id').of(pj).contains('ouiGestionImmobiliere')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCarteGestion);
}

/*
 *PJ Carte transaction immobilière si coché : OUI
 */
 
 var pj=$qp131PE2.gestionTransaction.activiteGestionTransaction.transactionImmobiliere;
if(Value('id').of(pj).contains('ouiTransactionImmobiliere')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjCarteTransaction);
}




/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('expert_foncier_agricole_IN.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Expert foncier et agricole - demande d\'inscription sur liste nationale',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\'inscription sur liste nationale pour l\'exercice de la profession d\'expert foncier et agricole',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
