var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp097PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp097PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp097PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	= $qp097PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp097PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp097PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp097PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp097PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp097PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp097PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp097PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp097PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp097PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp097PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp097PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp097PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp097PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp097PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp097PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp097PE5.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp097PE5.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp097PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp097PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp097PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp097PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp097PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']								  	  = "Distributeur de produits phytopharmaceutiques."
cerfaFields['libelleProfession2']								  	  = "distributeur de produits phytopharmaceutiques."

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp097PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp097PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Direction Régionale de l’Alimentation, de l’Agriculture et de la Forêt",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

/*
 * Ajout du cerfa
 */

var cerfaDoc = nash.doc //
    .load('models/courrier_libre_LPS_V3.pdf') //
    .apply(cerfaFields);
	
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Distributeur_de_produits_phytopharmaceutiques_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Distributeur de produits phytopharmaceutiques - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de distributeur de produits phytopharmaceutiques.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
