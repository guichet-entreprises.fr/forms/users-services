var cerfaFields = {};

var civNomPrenom = $qp097PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp097PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp097PE3.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $qp097PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp097PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp097PE3.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp097PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp097PE3.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp097PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp097PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp097PE3.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp097PE3.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp097PE3.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp097PE3.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp097PE3.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp097PE3.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp097PE3.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp097PE3.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp097PE3.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp097PE3.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp097PE3.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp097PE3.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp097PE3.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp097PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp097PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp097PE3.etatCivil.identificationDeclarant.prenomDeclarant;


cerfaFields['libelleProfession']				= "Distributeur de produits phytopharmaceutiques"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp097PE3.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp097PE3.signature.signature.dateSignature,
		autoriteHabilitee :"Direction Régionale de l’Alimentation, de l’Agriculture et de la Forêt",
		demandeContexte : "Renouvellement de la demande de reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/courrier_libre_LE_Renouv_V4.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);


var finalDocItem = finalDoc.save('Distributeur_de_produits_phytopharmaceutiques_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Distributeur de produits phytopharmaceutiques - renouvellement de la demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Renouvellement de la demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de distributeur de produits phytopharmaceutiques.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});