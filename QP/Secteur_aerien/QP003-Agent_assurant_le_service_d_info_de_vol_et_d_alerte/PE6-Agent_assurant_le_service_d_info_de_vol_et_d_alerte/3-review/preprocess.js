var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']			= $qp003PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp003PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp003PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp003PE6.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ' ' + $qp003PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']          		= $qp003PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']        		= $qp003PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']   					= $qp003PE6.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant 
											 + ($qp003PE6.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null ?', ' 
											 + $qp003PE6.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       	    		= ($qp003PE6.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp003PE6.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant + ' ' : ' ') 
											 + $qp003PE6.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant 
											 + ', ' + $qp003PE6.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']        		= $qp003PE6.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp003PE6.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']             		= $qp003PE6.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;


//signature
cerfaFields['date']                			= $qp003PE6.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp003PE6.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp003PE6.signature.signature.lieuSignature + ', le ';;
cerfaFields['civiliteNomPrenomSignature']	= $qp003PE6.etatCivil.identificationDeclarant.civilite + ' ' 
											 + $qp003PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' 
											 + $qp003PE6.etatCivil.identificationDeclarant.prenomDeclarant;

//cerfaFields['regionExercice']     			= $qp003PE6.adresse.regionExercice.regionExe;


var cerfa = pdf.create('models/courrier LE renouv Agent.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Agent information vol alerte - renouvellement LE.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Agent assurant le service d\'information de vol et d\'alerte - renouvellement de demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de renouvellement de demande de RQP en vue d\'un libre établissement pour la profession d\'agent assurant le service d\'information de vol et d\'alerte',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
