var cerfaFields = {};

cerfaFields['delivranceInitiale']          = true;
cerfaFields['nomDeclarant']                = $qp003PE3.information.etatCivil.nomDeclarant;
cerfaFields['nomUsageDeclarant']           = $qp003PE3.information.etatCivil.nomUsageDeclarant;
cerfaFields['prenomDeclarant']             = $qp003PE3.information.etatCivil.prenomDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp003PE3.information.etatCivil.lieuNaissanceDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp003PE3.information.etatCivil.dateNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp003PE3.information.etatCivil.nationaliteDeclarant;
cerfaFields['adressePersonnelleDeclarant'] = $qp003PE3.coordonneesGroup.coordonnees.numeroLibelleAdresse + ' ' + $qp003PE3.coordonneesGroup.coordonnees.complementAdresse + ' ' + $qp003PE3.coordonneesGroup.coordonnees.codePostalAdresse + ' ' + $qp003PE3.coordonneesGroup.coordonnees.villeAdresse + ' ' + $qp003PE3.coordonneesGroup.coordonnees.paysAdresse;

cerfaFields['prestataireAfis']             = Value('id').of($qp003PE3.formationInitiale.formationInitiale1.assureePar).eq('prestataireAfis');
cerfaFields['autrePrestataire']            = Value('id').of($qp003PE3.formationInitiale.formationInitiale1.assureePar).eq('autrePrestataire');
cerfaFields['enac']              		   = Value('id').of($qp003PE3.formationInitiale.formationInitiale1.assureePar).eq('enac');
cerfaFields['candidatLibre']               = Value('id').of($qp003PE3.formationInitiale.formationInitiale1.assureePar).eq('candidatLibre');


cerfaFields['autrePreciser']               = $qp003PE3.formationInitiale.formationInitiale1.preciser;
cerfaFields['dateReussiteExamen']          = $qp003PE3.formationInitiale.formationInitiale1.dateReussiteExamen;
cerfaFields['dateSignature']               = $qp003PE3.formationInitiale.formationInitiale1.dateSignature;

cerfaFields['nouvelleQualificationLocale'] = $qp003PE3.formationInitiale.formationInitiale1.nouvelleQualificationLocale;
cerfaFields['dateSignature']               = $qp003PE3.finSaisie.certifie.dateSignature;


var cerfa = pdf.create('models/Demande_de_certificat_qualif_AFIS_09_12.pdf', cerfaFields);

var cerfaPdf = pdf.save('Agent assurant le service d\'information de vol et d\'alerte - déclaration de libre établissement.pdf', cerfa);

return spec.create({
    id : 'review',
    label : 'Agent assurant le service d\'information de vol et d\'alerte - déclaration de libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Formulaire de première déclaration de libre établissement pour la profession d\agent assurant le service d\information de vol et d\alerte.',
            description : 'Voici le formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
