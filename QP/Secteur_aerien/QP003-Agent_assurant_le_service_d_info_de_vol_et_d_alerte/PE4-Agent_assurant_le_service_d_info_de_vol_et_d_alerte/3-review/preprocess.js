var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']			= $qp003PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp003PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp003PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp003PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ' ' + $qp003PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']          		= $qp003PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']        		= $qp003PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']   					= $qp003PE4.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant 
											 + ($qp003PE4.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null ?', ' 
											 + $qp003PE4.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       	    		= ($qp003PE4.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp003PE4.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant + ' ' : ' ') 
											 + $qp003PE4.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant 
											 + ', ' + $qp003PE4.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']        		= $qp003PE4.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp003PE4.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']             		= $qp003PE4.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;


//signature
cerfaFields['date']                			= $qp003PE4.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp003PE4.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp003PE4.signature.signature.lieuSignature + ', le ';;
cerfaFields['civiliteNomPrenomSignature']	= $qp003PE4.etatCivil.identificationDeclarant.civilite + ' ' 
											 + $qp003PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' 
											 + $qp003PE4.etatCivil.identificationDeclarant.prenomDeclarant;

//cerfaFields['regionExercice']     			= $qp003PE4.adresse.regionExercice.regionExe;



var cerfa = pdf.create('models/courrier libre LPS V3 Agent.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Agent service information vol alerte - déclaration LPS.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Agent assurant le service d\'information de vol et d\'alerte - première déclaration de libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de première déclaration de libre prestation de services pour la profession d\'agent assurant le service d\'information de vol et d\'alerte',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
