var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp222PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp222PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp222PE4.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']                = $qp222PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp222PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp222PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']               = $qp222PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp222PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                      = $qp222PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                    = $qp222PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                          = $qp222PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp222PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp222PE4.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                        = ($qp222PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp222PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp222PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp222PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']                  = $qp222PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                    = $qp222PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                         = $qp222PE4.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                             = $qp222PE4.signature.signature.dateSignature;
cerfaFields['signature']                        = $qp222PE4.signature.signature.signature;
cerfaFields['lieuSignature']                    = $qp222PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = $qp222PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp222PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp222PE4.etatCivil.identificationDeclarant.prenomDeclarant;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/Pompier_aerodrome_LPS.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp222PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});

/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp222PE4.signature.signature.dateSignature,
		autoriteHabilitee :"Préfet exerçant les pouvoirs de police sur l’aérodrome",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));


/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Pompier_aerodrome_LPS.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifs);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPermis);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Pompier_aerodrome_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Pompier d\'aérodrome - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de pompier d\'aérodrome',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
