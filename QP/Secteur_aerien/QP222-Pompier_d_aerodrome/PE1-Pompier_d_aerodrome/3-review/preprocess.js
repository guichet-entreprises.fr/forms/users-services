var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp222PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp222PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp222PE1.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']                = $qp222PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp222PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp222PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']               = $qp222PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp222PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                      = $qp222PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                    = $qp222PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                          = $qp222PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp222PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp222PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                        = ($qp222PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp222PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp222PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp222PE1.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']                  = $qp222PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                    = $qp222PE1.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                         = $qp222PE1.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                             = $qp222PE1.signature.signature.dateSignature;
cerfaFields['signature']                        = $qp222PE1.signature.signature.signature;
cerfaFields['lieuSignature']                    = $qp222PE1.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = $qp222PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp222PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp222PE1.etatCivil.identificationDeclarant.prenomDeclarant;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/Pompier_aerodrome_LE.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp222PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});

/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp222PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Préfet exerçant les pouvoirs de police sur l’aérodrome",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Pompier_aerodrome_LE.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPermis);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAgrements);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Pompier_aerodrome_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Pompier d\'aérodrome - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de pompier d\'aérodrome',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
