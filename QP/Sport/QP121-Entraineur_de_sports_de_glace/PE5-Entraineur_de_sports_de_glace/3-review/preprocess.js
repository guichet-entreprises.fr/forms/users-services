var cerfaFields = {};

var civNomPrenom = $qp121PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp121PE5.etatCivil.identificationDeclarant.nomNaissance + ' ' + $qp121PE5.etatCivil.identificationDeclarant.prenom;

//Demande
cerfaFields['declarationInitiale']                            = false;
cerfaFields['renouvellement']                                 = true;


//Profession
cerfaFields['accompagnateurMoyenneMontagne']                  = false;
cerfaFields['animateurActivitesGymniques']                    = false;
cerfaFields['animateurActivitesCyclisme']                     = false;
cerfaFields['animateurActivitesOrientation']                  = false;
cerfaFields['animateurActivitesCombinees']                    = false;
cerfaFields['animateurActivitesAquatiques']                   = false;
cerfaFields['animateurActivitesNautiques']                    = false;
cerfaFields['animateurActivitesPhysiques']                    = false;
cerfaFields['animateurArtsMartiaux']                          = false;
cerfaFields['animateurAthletisme']                            = false;
cerfaFields['animateurEquitation']                            = false;
cerfaFields['animateurGolf']                                  = false;
cerfaFields['animateurSportAdapte']                           = false;
cerfaFields['animateurSportsCollectifs']                      = false;
cerfaFields['animateurSportsSante']                           = false;
cerfaFields['animateurSportsAdresse']                         = false;
cerfaFields['animateurSportCombat']                           = false;
cerfaFields['animateurSportOpposition']                       = false;
cerfaFields['animateurSportsGlace']                           = false;
cerfaFields['animateurSportsRaquette']                        = false;
cerfaFields['animateurSportsMecaniques']                      = false;
cerfaFields['animateurHandiSport']                            = false;
cerfaFields['educateurSportif']                               = false;
cerfaFields['entraineurActivitesAquatiques']                  = false;
cerfaFields['entraineurActivitesCombinees']                   = false;
cerfaFields['entraineurActivitesOrientation']                 = false;
cerfaFields['entraineurActivitesCyclisme']                    = false;
cerfaFields['entraineurActivitesGynmniques']                  = false;
cerfaFields['entraineurActivitesNautiques']                   = false;
cerfaFields['entraineurArtsMartiaux']                         = false;
cerfaFields['entraineurAthletisme']                           = false;
cerfaFields['entraineurEquitation']                           = false;
cerfaFields['entraineurGolf']                                 = false;
cerfaFields['entraineurSportAdapte']                          = false;
cerfaFields['entraineurSportsCollectifs']                     = false;
cerfaFields['entraineurSportsAdresse']                        = false;
cerfaFields['entraineurSportOpposition']                      = false;
cerfaFields['entraineurSportCombat']                          = false;
cerfaFields['entraineurSportGlace']                           = true;
cerfaFields['entraineurSportsRaquettes']                      = false;
cerfaFields['entraineurSportsMecaniques']                     = false;
cerfaFields['entraineurHandiSport']                           = false;
cerfaFields['GuideHautemontagne']                             = false;
cerfaFields['MaitreNageurSauveteur']                          = false;
cerfaFields['MoniteurEscalade']                               = false;
cerfaFields['MoniteurCanoekayak']                             = false;
cerfaFields['MoniteurCanyoning']                              = false;
cerfaFields['MoniteurParachutisme']                           = false;
cerfaFields['MoniteurPlongee']                                = false;
cerfaFields['MoniteurSkiAlpin']                               = false;
cerfaFields['MoniteurSkiNordique']                            = false;
cerfaFields['MoniteurSpeleologie']                            = false;
cerfaFields['MoniteurSurfMer']                                = false;
cerfaFields['MoniteurVoile']                                  = false;
cerfaFields['MoniteurVolLibre']                               = false;


//Etat Civil
cerfaFields['civiliteMadame']                                 = ($qp121PE5.etatCivil.identificationDeclarant.civilite=='Madame');
cerfaFields['civiliteMonsieur']                               = ($qp121PE5.etatCivil.identificationDeclarant.civilite=='Monsieur');
cerfaFields['nomNaissance']                                   = $qp121PE5.etatCivil.identificationDeclarant.nomNaissance;
cerfaFields['nomUsage']                                       = $qp121PE5.etatCivil.identificationDeclarant.nomUsage;
cerfaFields['prenom']                                         = $qp121PE5.etatCivil.identificationDeclarant.prenom;
cerfaFields['dateNaissance']                                  = $qp121PE5.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['lieuNaissance']                                  = $qp121PE5.etatCivil.identificationDeclarant.lieuNaissance;
cerfaFields['paysNaissance']                                  = $qp121PE5.etatCivil.identificationDeclarant.paysNaissance;
cerfaFields['nationalite']                                    = $qp121PE5.etatCivil.identificationDeclarant.nationalite;
cerfaFields['nomPere']                                        = $qp121PE5.etatCivil.identificationDeclarant.naissanceEtranger.nomPere;
cerfaFields['prenomPere']                                     = $qp121PE5.etatCivil.identificationDeclarant.naissanceEtranger.prenomPere;
cerfaFields['nomMere']                                        = $qp121PE5.etatCivil.identificationDeclarant.naissanceEtranger.nomMere;
cerfaFields['prenomMere']                                     = $qp121PE5.etatCivil.identificationDeclarant.naissanceEtranger.prenomMere;


//Coordonnées
cerfaFields['adresseDeclarantNumeroNomRueComplement']         = $qp121PE5.adresse.adressePersonnelle.adresseDeclarantNumeroNomRueComplement;
cerfaFields['adresseDeclarantCodePostal']                     = $qp121PE5.adresse.adressePersonnelle.adresseDeclarantCodePostal;
cerfaFields['adresseDeclarantVille']                          = $qp121PE5.adresse.adressePersonnelle.adresseDeclarantVille;
cerfaFields['adresseDeclarantPays']                           = $qp121PE5.adresse.adressePersonnelle.adresseDeclarantPays;
cerfaFields['telephoneFixeDeclarant']                         = $qp121PE5.adresse.adressePersonnelle.telephoneFixeDeclarant;
cerfaFields['telephoneMobileDeclarant']                       = $qp121PE5.adresse.adressePersonnelle.telephoneMobileDeclarant;
cerfaFields['courrielDeclarant']                              = $qp121PE5.adresse.adressePersonnelle.courrielDeclarant;


//Situation professionnelle actuelle
cerfaFields['emploiActuel']                       			  = $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.emploiActuel;
cerfaFields['situationActuelleSalarie']                       = ($qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Salarié');
cerfaFields['situationActuelleTravailleurIndependant']        = ($qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Travailleur indépendant');
cerfaFields['situationActuelleAutre']                         = ($qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Autre');
cerfaFields['employeurNom']                                   = $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurNom;
cerfaFields['employeurPrenom']                                = $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurPrenom;
cerfaFields['employeurAdresse']                               = ($qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurAdresseNumNom != null ? $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurAdresseNumNom : '') + ' ' + ($qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurAdresseCP != null ? $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurAdresseCP : '') + ' ' + ($qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurAdresseVille != null ? $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurAdresseVille : '') + ' ' + ($qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurAdressePays != null ? $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurAdressePays : '');
cerfaFields['employeurRaisonSociale']                         = $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurRaisonSociale;
cerfaFields['employeurNatureJuridique']                       = $qp121PE5.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurNatureJuridique;


//Diplômes
cerfaFields['intituleDiplome1']                               = $qp121PE5.diplomes.diplomes0.intituleDiplome1;
cerfaFields['paysDelivranceDiplome1']                         = $qp121PE5.diplomes.diplomes0.paysDelivranceDiplome1;
cerfaFields['dateDelivranceDiplome1']                         = $qp121PE5.diplomes.diplomes0.dateDelivranceDiplome1;
cerfaFields['organismeDelivranceDiplome1']                    = $qp121PE5.diplomes.diplomes0.organismeDelivranceDiplome1;
cerfaFields['lieuFormationDiplome1']                          = $qp121PE5.diplomes.diplomes0.lieuFormationDiplome1;
cerfaFields['debutFormationDiplome1']                         = $qp121PE5.diplomes.diplomes0.debutFormationDiplome1.from;
cerfaFields['finFormationDiplome1']                           = $qp121PE5.diplomes.diplomes0.debutFormationDiplome1.to;
cerfaFields['dureeFormationDiplome1']                         = $qp121PE5.diplomes.diplomes0.dureeFormationDiplome1;
cerfaFields['nombreHeuresFormationDiplome1']                  = $qp121PE5.diplomes.diplomes0.nombreHeuresFormationDiplome1;
cerfaFields['pourcentageTheoriqueFormation1']                 = $qp121PE5.diplomes.diplomes0.pourcentageTheoriqueFormation1;
cerfaFields['pourcentagePratiqueFormation1']                  = $qp121PE5.diplomes.diplomes0.pourcentagePratiqueFormation1;

cerfaFields['intituleDiplome2']                               = $qp121PE5.diplomes.diplomes0.intituleDiplome2;
cerfaFields['paysDelivranceDiplome2']                         = $qp121PE5.diplomes.diplomes0.paysDelivranceDiplome2;
cerfaFields['dateDelivranceDiplome2']                         = $qp121PE5.diplomes.diplomes0.dateDelivranceDiplome2;
cerfaFields['organismeDelivranceDiplome2']                    = $qp121PE5.diplomes.diplomes0.organismeDelivranceDiplome2;
cerfaFields['lieuFormationDiplome2']                          = $qp121PE5.diplomes.diplomes0.lieuFormationDiplome2;
cerfaFields['debutFormationDiplome2']                         = $qp121PE5.diplomes.diplomes0.debutFormationDiplome2 != null ? $qp121PE5.diplomes.diplomes0.debutFormationDiplome2.from: '';
cerfaFields['finFormationDiplome2']                           = $qp121PE5.diplomes.diplomes0.debutFormationDiplome2 != null ? $qp121PE5.diplomes.diplomes0.debutFormationDiplome2.to: '';
cerfaFields['dureeFormationDiplome2']                         = $qp121PE5.diplomes.diplomes0.dureeFormationDiplome2;
cerfaFields['nombreHeuresFormationDiplome2']                  = $qp121PE5.diplomes.diplomes0.nombreHeuresFormationDiplome2;
cerfaFields['pourcentageTheoriqueFormation2']                 = $qp121PE5.diplomes.diplomes0.pourcentageTheoriqueFormation2;
cerfaFields['pourcentagePratiqueFormation2']                  = $qp121PE5.diplomes.diplomes0.pourcentagePratiqueFormation2;

cerfaFields['intituleDiplome3']                               = $qp121PE5.diplomes.diplomes0.intituleDiplome3;
cerfaFields['paysDelivranceDiplome3']                         = $qp121PE5.diplomes.diplomes0.paysDelivranceDiplome3;
cerfaFields['dateDelivranceDiplome3']                         = $qp121PE5.diplomes.diplomes0.dateDelivranceDiplome3;
cerfaFields['organismeDelivranceDiplome3']                    = $qp121PE5.diplomes.diplomes0.organismeDelivranceDiplome3;
cerfaFields['lieuFormationDiplome3']                          = $qp121PE5.diplomes.diplomes0.lieuFormationDiplome3;
cerfaFields['debutFormationDiplome3']                         = $qp121PE5.diplomes.diplomes0.debutFormationDiplome3 != null ? $qp121PE5.diplomes.diplomes0.debutFormationDiplome3.from: '';
cerfaFields['finFormationDiplome3']                           = $qp121PE5.diplomes.diplomes0.debutFormationDiplome3 != null ? $qp121PE5.diplomes.diplomes0.debutFormationDiplome3.to: '';
cerfaFields['dureeFormationDiplome3']                         = $qp121PE5.diplomes.diplomes0.dureeFormationDiplome3;
cerfaFields['nombreHeuresFormationDiplome3']                  = $qp121PE5.diplomes.diplomes0.nombreHeuresFormationDiplome3;
cerfaFields['pourcentageTheoriqueFormation3']                 = $qp121PE5.diplomes.diplomes0.pourcentageTheoriqueFormation3;
cerfaFields['pourcentagePratiqueFormation3']                  = $qp121PE5.diplomes.diplomes0.pourcentagePratiqueFormation3;

cerfaFields['intituleDiplome4']                               = $qp121PE5.diplomes.diplomes0.intituleDiplome4;
cerfaFields['paysDelivranceDiplome4']                         = $qp121PE5.diplomes.diplomes0.paysDelivranceDiplome4;
cerfaFields['dateDelivranceDiplome4']                         = $qp121PE5.diplomes.diplomes0.dateDelivranceDiplome4;
cerfaFields['organismeDelivranceDiplome4']                    = $qp121PE5.diplomes.diplomes0.organismeDelivranceDiplome4;
cerfaFields['lieuFormationDiplome4']                          = $qp121PE5.diplomes.diplomes0.lieuFormationDiplome4;
cerfaFields['debutFormationDiplome4']                         = $qp121PE5.diplomes.diplomes0.debutFormationDiplome4 != null ? $qp121PE5.diplomes.diplomes0.debutFormationDiplome4.from: '';
cerfaFields['finFormationDiplome4']                           = $qp121PE5.diplomes.diplomes0.debutFormationDiplome4 != null ? $qp121PE5.diplomes.diplomes0.debutFormationDiplome4.to: '';
cerfaFields['dureeFormationDiplome4']                         = $qp121PE5.diplomes.diplomes0.dureeFormationDiplome4;
cerfaFields['nombreHeuresFormationDiplome4']                  = $qp121PE5.diplomes.diplomes0.nombreHeuresFormationDiplome4;
cerfaFields['pourcentageTheoriqueFormation4']                 = $qp121PE5.diplomes.diplomes0.pourcentageTheoriqueFormation4;
cerfaFields['pourcentagePratiqueFormation4']                  = $qp121PE5.diplomes.diplomes0.pourcentagePratiqueFormation4;

cerfaFields['intituleDiplome5']                               = $qp121PE5.diplomes.diplomes0.intituleDiplome5;
cerfaFields['paysDelivranceDiplome5']                         = $qp121PE5.diplomes.diplomes0.paysDelivranceDiplome5;
cerfaFields['dateDelivranceDiplome5']                         = $qp121PE5.diplomes.diplomes0.dateDelivranceDiplome5;
cerfaFields['organismeDelivranceDiplome5']                    = $qp121PE5.diplomes.diplomes0.organismeDelivranceDiplome5;
cerfaFields['lieuFormationDiplome5']                          = $qp121PE5.diplomes.diplomes0.lieuFormationDiplome5;
cerfaFields['debutFormationDiplome5']                         = $qp121PE5.diplomes.diplomes0.debutFormationDiplome5 != null ? $qp121PE5.diplomes.diplomes0.debutFormationDiplome5.from: '';
cerfaFields['finFormationDiplome5']                           = $qp121PE5.diplomes.diplomes0.debutFormationDiplome5 != null ? $qp121PE5.diplomes.diplomes0.debutFormationDiplome5.to: '';
cerfaFields['dureeFormationDiplome5']                         = $qp121PE5.diplomes.diplomes0.dureeFormationDiplome5;
cerfaFields['nombreHeuresFormationDiplome5']                  = $qp121PE5.diplomes.diplomes0.nombreHeuresFormationDiplome5;
cerfaFields['pourcentageTheoriqueFormation5']                 = $qp121PE5.diplomes.diplomes0.pourcentageTheoriqueFormation5;
cerfaFields['pourcentagePratiqueFormation5']                  = $qp121PE5.diplomes.diplomes0.pourcentagePratiqueFormation5;


//Expériences professionnelles 
cerfaFields['experienceProfessionnelleIntitule1']             = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule1;
cerfaFields['experienceProfessionnelleNomEmployeur1']         = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur1;
cerfaFields['experienceProfessionnelleAdresseEmployeur1']     = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurNumNom1 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurNumNom1 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurCP1 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurCP1 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurVille1 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurVille1 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurPays1 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurPays1 : '');
cerfaFields['experienceProfessionnelleDateDebut1']            = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1.from : '';
cerfaFields['experienceProfessionnelleDateFin1']              = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1.to : '';
cerfaFields['experienceProfessionnelleJeunes1']               = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes1']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Adultes');
cerfaFields['experienceProfessionnelleSeniors1']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Séniors');
cerfaFields['experienceProfessionnelleHandicap1']             = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure1']      = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure1;
cerfaFields['experienceProfessionnelleNiveauResponsabilite1'] = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite1;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo1']    = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo1;

cerfaFields['experienceProfessionnelleIntitule2']             = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule2;
cerfaFields['experienceProfessionnelleNomEmployeur2']         = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur2;
cerfaFields['experienceProfessionnelleAdresseEmployeur2']     = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurNumNom2 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurNumNom2 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurCP2 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurCP2 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurVille2 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurVille2 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurPays2 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurPays2 : '');
cerfaFields['experienceProfessionnelleDateDebut2']            = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2.from : '';
cerfaFields['experienceProfessionnelleDateFin2']              = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2.to : '';
cerfaFields['experienceProfessionnelleJeunes2']               = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes2']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Adultes');
cerfaFields['experienceProfessionnelleSeniors2']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Séniors');
cerfaFields['experienceProfessionnelleHandicap2']             = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure2']      = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure2;
cerfaFields['experienceProfessionnelleNiveauResponsabilite2'] = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite2;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo2']    = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo2;

cerfaFields['experienceProfessionnelleIntitule3']             = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule3;
cerfaFields['experienceProfessionnelleNomEmployeur3']         = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur3;
cerfaFields['experienceProfessionnelleAdresseEmployeur3']     = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurNumNom3 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurNumNom3 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurCP3 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurCP3 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurVille3 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurVille3 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurPays3 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurPays3 : '');
cerfaFields['experienceProfessionnelleDateDebut3']            = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3.from : '';
cerfaFields['experienceProfessionnelleDateFin3']              = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3.to : '';
cerfaFields['experienceProfessionnelleJeunes3']               = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes3']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Adultes');
cerfaFields['experienceProfessionnelleSeniors3']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Séniors');
cerfaFields['experienceProfessionnelleHandicap3']             = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure3']      = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure3;
cerfaFields['experienceProfessionnelleNiveauResponsabilite3'] = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite3;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo3']    = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo3;

cerfaFields['experienceProfessionnelleIntitule4']             = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule4;
cerfaFields['experienceProfessionnelleNomEmployeur4']         = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur4;
cerfaFields['experienceProfessionnelleAdresseEmployeur4']     = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurNumNom4 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurNumNom4 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurCP4 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurCP4 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurVille4 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurVille4 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurPays4 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurPays4 : '');
cerfaFields['experienceProfessionnelleDateDebut4']            = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4.from : '';
cerfaFields['experienceProfessionnelleDateFin4']              = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4.to : '';
cerfaFields['experienceProfessionnelleJeunes4']               = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes4']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Adultes');
cerfaFields['experienceProfessionnelleSeniors4']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Séniors');
cerfaFields['experienceProfessionnelleHandicap4']             = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure4']      = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure4;
cerfaFields['experienceProfessionnelleNiveauResponsabilite4'] = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite4;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo4']    = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo4;

cerfaFields['experienceProfessionnelleIntitule5']             = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule5;
cerfaFields['experienceProfessionnelleNomEmployeur5']         = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur5;
cerfaFields['experienceProfessionnelleAdresseEmployeur5']     = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurNumNom5 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurNumNom5 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurCP5 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurCP5 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurVille5 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurVille5 : '') + ' ' + ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurPays5 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurPays5 : '');
cerfaFields['experienceProfessionnelleDateDebut5']            = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5.from : '';
cerfaFields['experienceProfessionnelleDateFin5']              = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5 != null ? $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5.to : '';
cerfaFields['experienceProfessionnelleJeunes5']               = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes5']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Adultes');
cerfaFields['experienceProfessionnelleSeniors5']              = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Séniors');
cerfaFields['experienceProfessionnelleHandicap5']             = ($qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure5']      = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure5;
cerfaFields['experienceProfessionnelleNiveauResponsabilite5'] = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite5;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo5']    = $qp121PE5.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo5;

//Objet de la demande de reconnaissance de qualifications professionnelles
cerfaFields['activiteDemandee']                               = $qp121PE5.objetDemandeRQP.objetDemandeRQP0.activiteDemandee;
cerfaFields['disciplineDemandee']                             = $qp121PE5.objetDemandeRQP.objetDemandeRQP0.disciplineDemandee;
cerfaFields['statutDemandeSalarie']                           = ($qp121PE5.objetDemandeRQP.objetDemandeRQP0.statutDemande=='Salarié');
cerfaFields['statutDemandeTravailleurIndependant']            = ($qp121PE5.objetDemandeRQP.objetDemandeRQP0.statutDemande=='Travailleur indépendant');
cerfaFields['statutDemandeAutre']                             = ($qp121PE5.objetDemandeRQP.objetDemandeRQP0.statutDemande=='Autre');
cerfaFields['statutDemandeSalarieNomEmployeur']               = $qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieNomEmployeur;
cerfaFields['statutDemandeSalariePrenomEmployeur']            = $qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalariePrenomEmployeur;
cerfaFields['statutDemandeSalarieAdresseEmployeur']           = ($qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieAdresseEmployeurNumNom != null ? $qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieAdresseEmployeurNumNom : '') + ' ' + ($qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieAdresseEmployeurCP != null ? $qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieAdresseEmployeurCP : '') + ' ' + ($qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieAdresseEmployeurVille != null ? $qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieAdresseEmployeurVille : '') + ' ' + ($qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieAdresseEmployeurPays != null ? $qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieAdresseEmployeurPays : '');
cerfaFields['statutDemandeSalarieRaisonSocialeEmployeur']     = $qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieRaisonSocialeEmployeur;
cerfaFields['statutDemandeSalarieNatureJuridiqueEmployeur']   = $qp121PE5.objetDemandeRQP.objetDemandeRQP0.coordonneesEmployeur.statutDemandeSalarieNatureJuridiqueEmployeur;


//Déclaration sur l'honneur
cerfaFields['civiliteNomPrenom']                              = $qp121PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp121PE5.etatCivil.identificationDeclarant.nomNaissance + ' ' + $qp121PE5.etatCivil.identificationDeclarant.prenom;
cerfaFields['faitA']                                          = $qp121PE5.signatureGroup.signature.faitA;
cerfaFields['faitLe']                                         = $qp121PE5.signatureGroup.signature.faitLe;
cerfaFields['signatureCoche']                                 = $qp121PE5.signatureGroup.signature.signatureCoche;
cerfaFields['signatureTexte']                                 = "Je déclare sur l’honneur l'exactitude des informations de la formalité et signe la présente déclaration.";


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp121PE5.signatureGroup.signature.faitLe,
		autoriteHabilitee :"Direction départementale de la cohésion sociale et de la protection des populations (DDCSPP)",
		demandeContexte : "Renouvellement de la reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire LE sport.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('entraineur_sports_glace_LE_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Entraîneur de sports de glace - renouvellement de la demande de reconnaissance de qualifications professionnelles  en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'entraîneur de sports de glace',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

