var cerfaFields = {};
var civNomPrenom = $qp111PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp111PE8.etatCivil.identificationDeclarant.nomNaissanceDeclarant + ' ' + $qp111PE8.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['declarationInitiale']                            = false ;
cerfaFields['renouvellement']            	                  = true ;

cerfaFields['accompagnateurMoyenneMontagne']                  = false;
cerfaFields['animateurActivitesAquatiques']                   = false;
cerfaFields['animateurActivitesCombinees']                    = false;
cerfaFields['animateurActivitesOrientation']                  = false;
cerfaFields['animateurActivitesCyclisme']                     = false;
cerfaFields['animateurActivitesGymniques']                    = false;
cerfaFields['animateurActivitesNautiques']                    = false;
cerfaFields['animateurActivitesPhysiques']                    = false;
cerfaFields['animateurArtsMartiaux']                          = false;
cerfaFields['animateurAthletisme']                            = false;
cerfaFields['animateurEquitation']                            = false;
cerfaFields['animateurGolf']                                  = false;
cerfaFields['animateurSportAdapte']                           = false;
cerfaFields['animateurSportsSante']                           = false;
cerfaFields['animateurSportsCollectifs']                      = false;
cerfaFields['entraineurActivitesGynmniques']                  = false;
cerfaFields['entraineurActivitesNautiques']                   = true;
cerfaFields['entraineurArtsMartiaux']                         = false;
cerfaFields['entraineurAthletisme']                           = false;
cerfaFields['entraineurEquitation']                           = false;
cerfaFields['entraineurGolf']                                 = false;
cerfaFields['entraineurSportAdapte']                          = false;
cerfaFields['entraineurSportsCollectifs']                     = false;
cerfaFields['entraineurSportsAdresse']                        = false;
cerfaFields['entraineurSportOpposition']                      = false;
cerfaFields['entraineurSportCombat']                          = false;
cerfaFields['entraineurSportGlace']                           = false;
cerfaFields['entraineurSportsRaquettes']                      = false;
cerfaFields['entraineurSportsMecaniques']                     = false;
cerfaFields['entraineurHandiSport']                           = false;
cerfaFields['GuideHautemontagne']                             = false;
cerfaFields['animateurSportsAdresse']                         = false;
cerfaFields['animateurSportOpposition']                       = false;
cerfaFields['animateurSportCombat']                           = false;
cerfaFields['animateurSportsGlace']                           = false;
cerfaFields['animateurSportsRaquette']                        = false;
cerfaFields['animateurSportsMecaniques']                      = false;
cerfaFields['animateurHandiSport']                            = false;
cerfaFields['educateurSportif']                               = false;
cerfaFields['entraineurActivitesAquatiques']                  = false;
cerfaFields['entraineurActivitesCombinees']                   = false;
cerfaFields['entraineurActivitesOrientation']                 = false;
cerfaFields['entraineurActivitesCyclisme']                    = false;
cerfaFields['MaitreNageurSauveteur']                          = false;
cerfaFields['MoniteurEscalade']                               = false;
cerfaFields['MoniteurCanoekayak']                             = false;
cerfaFields['MoniteurCanyoning']                              = false;
cerfaFields['MoniteurParachutisme']                           = false;
cerfaFields['MoniteurPlongee']                                = false;
cerfaFields['MoniteurSkiAlpin']                               = false;
cerfaFields['MoniteurSkiNordique']                            = false;
cerfaFields['MoniteurSpeleologie']                            = false;
cerfaFields['MoniteurSurfMer']                                = false;
cerfaFields['MoniteurVoile']                                  = false;
cerfaFields['MoniteurVolLibre']                               = false;

//Etat Civil
cerfaFields['civiliteMonsieur']                 = ($qp111PE8.etatCivil.identificationDeclarant.civilite=='Monsieur');
cerfaFields['civiliteMadame']                   = ($qp111PE8.etatCivil.identificationDeclarant.civilite=='Madame');
cerfaFields['nomNaissance']                     = $qp111PE8.etatCivil.identificationDeclarant.nomNaissanceDeclarant;
cerfaFields['nomUsage']                      	= $qp111PE8.etatCivil.identificationDeclarant.nomEpouseDeclarant;
cerfaFields['prenom']                  			= $qp111PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['dateNaissance']           			= $qp111PE8.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['paysNaissance']           			= $qp111PE8.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']             			= $qp111PE8.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['lieuNaissance']           			= $qp111PE8.etatCivil.identificationDeclarant.communeNaissanceDeclarant;

// adresse Déclarant
cerfaFields['adresseDeclarantNumeroNomRueComplement']               = $qp111PE8.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant 
																	+ ($qp111PE8.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ' ' + $qp111PE8.adresse.adressePersonnelle.complementAdresseDeclarant : '');
cerfaFields['adresseDeclarantVille']                                = $qp111PE8.adresse.adressePersonnelle.villeAdresseDeclarant;
cerfaFields['adresseDeclarantPays']                                 = $qp111PE8.adresse.adressePersonnelle.paysAdresseDeclarant;
cerfaFields['adresseDeclarantCodePostal']                           = $qp111PE8.adresse.adressePersonnelle.codePostalAdresseDeclarant;
cerfaFields['telephoneFixeDeclarant']                             	= $qp111PE8.adresse.adressePersonnelle.telephoneAdresseDeclarant;
cerfaFields['telephoneMobileDeclarant']                          	= $qp111PE8.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;
cerfaFields['courrielDeclarant']                              		= $qp111PE8.adresse.adressePersonnelle.mailAdresseDeclarant;

// situation professionnelle actuelle 
cerfaFields['emploiActuel']                                   = $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.emploiActuel;
cerfaFields['emploiActuelLieuEtablissement']                  = $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.emploiLieuEtablissement;
cerfaFields['situationActuelleSalarie']                       = ($qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Salarié');
cerfaFields['situationActuelleTravailleurIndependant']        = ($qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Travailleur indépendant');
cerfaFields['situationActuelleAutre']                         = ($qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Autre');
cerfaFields['employeurNom']                                   = $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurNom;
cerfaFields['employeurPrenom']                                = $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurPrenom;
cerfaFields['employeurAdresse']                               = ($qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.numeroLibelleAdresseEmployeur != null ? $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.numeroLibelleAdresseEmployeur : '') 
																+ ' ' + ($qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.codePostalAdresseEmployeur != null ? ' '+ $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.codePostalAdresseEmployeur : '') 
																+ ' ' + ($qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.villeAdresseEmployeur != null ? ' '+ $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.villeAdresseEmployeur : '')
																+ ' ' + ($qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.paysAdresseEmployeur != null ? ' '+ $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.paysAdresseEmployeur : '');
cerfaFields['employeurRaisonSociale']                         = $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurRaisonSociale;
cerfaFields['employeurNatureJuridique']                       = $qp111PE8.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurNatureJuridique;




//Diplômes
cerfaFields['intituleDiplome1']                               = $qp111PE8.diplomes.diplomes0.intituleDiplome1;
cerfaFields['paysDelivranceDiplome1']                         = $qp111PE8.diplomes.diplomes0.paysDelivranceDiplome1;
cerfaFields['dateDelivranceDiplome1']                         = $qp111PE8.diplomes.diplomes0.dateDelivranceDiplome1;
cerfaFields['organismeDelivranceDiplome1']                    = $qp111PE8.diplomes.diplomes0.organismeDelivrance1;
cerfaFields['lieuFormationDiplome1']                          = $qp111PE8.diplomes.diplomes0.lieuFormationDiplome1;
cerfaFields['debutFormationDiplome1']                         = $qp111PE8.diplomes.diplomes0.debutFormationDiplome1.from;
cerfaFields['finFormationDiplome1']                           = $qp111PE8.diplomes.diplomes0.debutFormationDiplome1.to;
cerfaFields['dureeFormationDiplome1']                         = $qp111PE8.diplomes.diplomes0.dureeFormationDiplome1;
cerfaFields['nombreHeuresFormationDiplome1']                  = $qp111PE8.diplomes.diplomes0.nombreHeuresFormationDiplome1;
cerfaFields['pourcentageTheoriqueFormation1']                 = $qp111PE8.diplomes.diplomes0.pourcentageTheoriqueFormation1;
cerfaFields['pourcentagePratiqueFormation1']                  = $qp111PE8.diplomes.diplomes0.pourcentagePratiqueFormation1;

cerfaFields['intituleDiplome2']                               = $qp111PE8.diplomes.diplomes0.intituleDiplome2;
cerfaFields['paysDelivranceDiplome2']                         = $qp111PE8.diplomes.diplomes0.paysDelivranceDiplome2;
cerfaFields['dateDelivranceDiplome2']                         = $qp111PE8.diplomes.diplomes0.dateDelivranceDiplome2;
cerfaFields['organismeDelivranceDiplome2']                    = $qp111PE8.diplomes.diplomes0.organismeDelivrance2;
cerfaFields['lieuFormationDiplome2']                          = $qp111PE8.diplomes.diplomes0.lieuFormationDiplome2;
cerfaFields['debutFormationDiplome2']                         = ($qp111PE8.diplomes.diplomes0.debutFormationDiplome2 != null ? $qp111PE8.diplomes.diplomes0.debutFormationDiplome2.from: '');
cerfaFields['finFormationDiplome2']                           = ($qp111PE8.diplomes.diplomes0.debutFormationDiplome2 != null ? $qp111PE8.diplomes.diplomes0.debutFormationDiplome2.to: '');
cerfaFields['dureeFormationDiplome2']                         = $qp111PE8.diplomes.diplomes0.dureeFormationDiplome2;
cerfaFields['nombreHeuresFormationDiplome2']                  = $qp111PE8.diplomes.diplomes0.nombreHeuresFormationDiplome2;
cerfaFields['pourcentageTheoriqueFormation2']                 = $qp111PE8.diplomes.diplomes0.pourcentageTheoriqueFormation2;
cerfaFields['pourcentagePratiqueFormation2']                  = $qp111PE8.diplomes.diplomes0.pourcentagePratiqueFormation2;

cerfaFields['intituleDiplome3']                               = $qp111PE8.diplomes.diplomes0.intituleDiplome3;
cerfaFields['paysDelivranceDiplome3']                         = $qp111PE8.diplomes.diplomes0.paysDelivranceDiplome3;
cerfaFields['dateDelivranceDiplome3']                         = $qp111PE8.diplomes.diplomes0.dateDelivranceDiplome3;
cerfaFields['organismeDelivranceDiplome3']                    = $qp111PE8.diplomes.diplomes0.organismeDelivrance3;
cerfaFields['lieuFormationDiplome3']                          = $qp111PE8.diplomes.diplomes0.lieuFormationDiplome3;
cerfaFields['debutFormationDiplome3']                         = ($qp111PE8.diplomes.diplomes0.debutFormationDiplome3 != null ? $qp111PE8.diplomes.diplomes0.debutFormationDiplome3.from: '');
cerfaFields['finFormationDiplome3']                           = ($qp111PE8.diplomes.diplomes0.debutFormationDiplome3 != null ? $qp111PE8.diplomes.diplomes0.debutFormationDiplome3.to: '');
cerfaFields['dureeFormationDiplome3']                         = $qp111PE8.diplomes.diplomes0.dureeFormationDiplome3;
cerfaFields['nombreHeuresFormationDiplome3']                  = $qp111PE8.diplomes.diplomes0.nombreHeuresFormationDiplome3;
cerfaFields['pourcentageTheoriqueFormation3']                 = $qp111PE8.diplomes.diplomes0.pourcentageTheoriqueFormation3;
cerfaFields['pourcentagePratiqueFormation3']                  = $qp111PE8.diplomes.diplomes0.pourcentagePratiqueFormation3;

cerfaFields['intituleDiplome4']                               = $qp111PE8.diplomes.diplomes0.intituleDiplome4;
cerfaFields['paysDelivranceDiplome4']                         = $qp111PE8.diplomes.diplomes0.paysDelivranceDiplome4;
cerfaFields['dateDelivranceDiplome4']                         = $qp111PE8.diplomes.diplomes0.dateDelivranceDiplome4;
cerfaFields['organismeDelivranceDiplome4']                    = $qp111PE8.diplomes.diplomes0.organismeDelivrance4;
cerfaFields['lieuFormationDiplome4']                          = $qp111PE8.diplomes.diplomes0.lieuFormationDiplome4;
cerfaFields['debutFormationDiplome4']                         = ($qp111PE8.diplomes.diplomes0.debutFormationDiplome4 != null ? $qp111PE8.diplomes.diplomes0.debutFormationDiplome4.from: '');
cerfaFields['finFormationDiplome4']                           = ($qp111PE8.diplomes.diplomes0.debutFormationDiplome4 != null ? $qp111PE8.diplomes.diplomes0.debutFormationDiplome4.to: '');
cerfaFields['dureeFormationDiplome4']                         = $qp111PE8.diplomes.diplomes0.dureeFormationDiplome4;
cerfaFields['nombreHeuresFormationDiplome4']                  = $qp111PE8.diplomes.diplomes0.nombreHeuresFormationDiplome4;
cerfaFields['pourcentageTheoriqueFormation4']                 = $qp111PE8.diplomes.diplomes0.pourcentageTheoriqueFormation4;
cerfaFields['pourcentagePratiqueFormation4']                  = $qp111PE8.diplomes.diplomes0.pourcentagePratiqueFormation4;

cerfaFields['intituleDiplome5']                               = $qp111PE8.diplomes.diplomes0.intituleDiplome5;
cerfaFields['paysDelivranceDiplome5']                         = $qp111PE8.diplomes.diplomes0.paysDelivranceDiplome5;
cerfaFields['dateDelivranceDiplome5']                         = $qp111PE8.diplomes.diplomes0.dateDelivranceDiplome5;
cerfaFields['organismeDelivranceDiplome5']                    = $qp111PE8.diplomes.diplomes0.organismeDelivrance5;
cerfaFields['lieuFormationDiplome5']                          = $qp111PE8.diplomes.diplomes0.lieuFormationDiplome5;
cerfaFields['debutFormationDiplome5']                         = ($qp111PE8.diplomes.diplomes0.debutFormationDiplome5 != null ? $qp111PE8.diplomes.diplomes0.debutFormationDiplome5.from: '');
cerfaFields['finFormationDiplome5']                           = ($qp111PE8.diplomes.diplomes0.debutFormationDiplome5 != null ? $qp111PE8.diplomes.diplomes0.debutFormationDiplome5.to: '');
cerfaFields['dureeFormationDiplome5']                         = $qp111PE8.diplomes.diplomes0.dureeFormationDiplome5;
cerfaFields['nombreHeuresFormationDiplome5']                  = $qp111PE8.diplomes.diplomes0.nombreHeuresFormationDiplome5;
cerfaFields['pourcentageTheoriqueFormation5']                 = $qp111PE8.diplomes.diplomes0.pourcentageTheoriqueFormation5;
cerfaFields['pourcentagePratiqueFormation5']                  = $qp111PE8.diplomes.diplomes0.pourcentagePratiqueFormation5;

//Expériences professionnelles 

cerfaFields['experienceProfessionnelleIntitule1']             = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule1;
cerfaFields['experienceProfessionnelleNomEmployeur1']         = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur1;
cerfaFields['experienceProfessionnelleAdresseEmployeur1']     = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurNumNom1 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurNumNom1 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurCP1 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurCP1 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurVille1 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurVille1 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurPays1 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurPays1 : '');
cerfaFields['experienceProfessionnelleDateDebut1']            = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1.from : '';
cerfaFields['experienceProfessionnelleDateFin1']              = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1.to : '';
cerfaFields['experienceProfessionnelleJeunes1']               = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes1']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Adultes');
cerfaFields['experienceProfessionnelleSeniors1']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Séniors');
cerfaFields['experienceProfessionnelleHandicap1']             = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure1']      = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure1;
cerfaFields['experienceProfessionnelleNiveauResponsabilite1'] = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite1;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo1']    = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo1;

cerfaFields['experienceProfessionnelleIntitule2']             = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule2;
cerfaFields['experienceProfessionnelleNomEmployeur2']         = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur2;
cerfaFields['experienceProfessionnelleAdresseEmployeur2']     = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurNumNom2 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurNumNom2 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurCP2 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurCP2 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurVille2 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurVille2 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurPays2 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurPays2 : '');
cerfaFields['experienceProfessionnelleDateDebut2']            = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2.from : '';
cerfaFields['experienceProfessionnelleDateFin2']              = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2.to : '';
cerfaFields['experienceProfessionnelleJeunes2']               = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes2']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Adultes');
cerfaFields['experienceProfessionnelleSeniors2']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Séniors');
cerfaFields['experienceProfessionnelleHandicap2']             = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure2']      = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure2;
cerfaFields['experienceProfessionnelleNiveauResponsabilite2'] = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite2;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo2']    = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo2;

cerfaFields['experienceProfessionnelleIntitule3']             = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule3;
cerfaFields['experienceProfessionnelleNomEmployeur3']         = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur3;
cerfaFields['experienceProfessionnelleAdresseEmployeur3']     = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurNumNom3 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurNumNom3 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurCP3 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurCP3 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurVille3 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurVille3 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurPays3 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurPays3 : '');
cerfaFields['experienceProfessionnelleDateDebut3']            = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3.from : '';
cerfaFields['experienceProfessionnelleDateFin3']              = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3.to : '';
cerfaFields['experienceProfessionnelleJeunes3']               = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes3']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Adultes');
cerfaFields['experienceProfessionnelleSeniors3']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Séniors');
cerfaFields['experienceProfessionnelleHandicap3']             = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure3']      = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure3;
cerfaFields['experienceProfessionnelleNiveauResponsabilite3'] = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite3;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo3']    = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo3;

cerfaFields['experienceProfessionnelleIntitule4']             = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule4;
cerfaFields['experienceProfessionnelleNomEmployeur4']         = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur4;
cerfaFields['experienceProfessionnelleAdresseEmployeur4']     = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurNumNom4 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurNumNom4 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurCP4 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurCP4 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurVille4 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurVille4 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurPays4 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurPays4 : '');
cerfaFields['experienceProfessionnelleDateDebut4']            = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4.from : '';
cerfaFields['experienceProfessionnelleDateFin4']              = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4.to : '';
cerfaFields['experienceProfessionnelleJeunes4']               = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes4']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Adultes');
cerfaFields['experienceProfessionnelleSeniors4']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Séniors');
cerfaFields['experienceProfessionnelleHandicap4']             = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure4']      = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure4;
cerfaFields['experienceProfessionnelleNiveauResponsabilite4'] = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite4;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo4']    = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo4;

cerfaFields['experienceProfessionnelleIntitule5']             = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule5;
cerfaFields['experienceProfessionnelleNomEmployeur5']         = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur5;
cerfaFields['experienceProfessionnelleAdresseEmployeur5']     = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurNumNom5 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurNumNom5 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurCP5 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurCP5 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurVille5 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurVille5 : '') + ' ' + ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurPays5 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurPays5 : '');
cerfaFields['experienceProfessionnelleDateDebut5']            = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5.from : '';
cerfaFields['experienceProfessionnelleDateFin5']              = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5 != null ? $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5.to : '';
cerfaFields['experienceProfessionnelleJeunes5']               = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes5']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Adultes');
cerfaFields['experienceProfessionnelleSeniors5']              = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Séniors');
cerfaFields['experienceProfessionnelleHandicap5']             = ($qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure5']      = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure5;
cerfaFields['experienceProfessionnelleNiveauResponsabilite5'] = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite5;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo5']    = $qp111PE8.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo5;

// Reconnaissance quanlifications professionnelles
cerfaFields['activiteDemandee']                               = $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.activiteDemande;
cerfaFields['disciplineDemandee']                  		      = $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.disciplineDemandee;
cerfaFields['prestationSouhaiteeTravailleurIndependant']      = Value('id').of($qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.situationActuelle1).eq('travailleurIndependant6');
cerfaFields['prestationSouhaiteeTourOperateur']        		  = Value('id').of($qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.situationActuelle1).eq('tourOperator');
cerfaFields['tourOperateurNomEmployeur']                      = $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.employeurNom6;
cerfaFields['tourOperateurPrenomEmployeur']                   = $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.employeurPrenom6;
cerfaFields['tourOperateurAdresseEmployeur']                  = ($qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.numeroLibelleAdresseEmployeur6 != null ? $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.numeroLibelleAdresseEmployeur6 : '') 
																+ ' ' + ($qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.codePostalAdresseEmployeur6 != null ? ' '+ $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.codePostalAdresseEmployeur6 : '') 
																+ ' ' + ($qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.villeAdresseEmployeur6 != null ? ' '+ $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.villeAdresseEmployeur6 : '')
																+ ' ' + ($qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.paysAdresseEmployeur6 != null ? ' '+ $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.paysAdresseEmployeur6 : '');
cerfaFields['tourOperateurRaisonSocialeEmployeur']            = $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.employeurRaisonSociale6;
cerfaFields['tourOperateurNatureJuridiqueEmployeur']          = $qp111PE8.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.employeurNatureJuridique6;

//Prestations de services prévues sur le territoire français
cerfaFields['dateDebutPrestationPrevue1']					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue1 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue1.from: '');
cerfaFields['dateFinPrestationPrevue1'] 					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue1 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue1.to: '');
cerfaFields['nombreJoursPrestationPrevue1']					 = $qp111PE8.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue1;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue1']	 = $qp111PE8.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue1; 
cerfaFields['lieuEtVillePrestationPrevue1'] 				 = $qp111PE8.prestationPrevue.prestationPrevue0.villePrestationPrevue1;

cerfaFields['dateDebutPrestationPrevue2']					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue2 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue2.from: '');
cerfaFields['dateFinPrestationPrevue2'] 					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue2 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue2.to: '');
cerfaFields['nombreJoursPrestationPrevue2']					 = $qp111PE8.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue2;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue2']	 = $qp111PE8.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue2; 
cerfaFields['lieuEtVillePrestationPrevue2'] 				 = $qp111PE8.prestationPrevue.prestationPrevue0.villePrestationPrevue2;

cerfaFields['dateDebutPrestationPrevue3']					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3.from: '');
cerfaFields['dateFinPrestationPrevue3'] 					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3.to: '');
cerfaFields['nombreJoursPrestationPrevue3']					 = $qp111PE8.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue3;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue3']	 = $qp111PE8.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue3; 
cerfaFields['lieuEtVillePrestationPrevue3'] 				 = $qp111PE8.prestationPrevue.prestationPrevue0.villePrestationPrevue3;

cerfaFields['dateDebutPrestationPrevue4']					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3.from: '');
cerfaFields['dateFinPrestationPrevue4'] 					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3.to: '');
cerfaFields['nombreJoursPrestationPrevue4']					 = $qp111PE8.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue4;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue4']	 = $qp111PE8.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue4; 
cerfaFields['lieuEtVillePrestationPrevue4'] 				 = $qp111PE8.prestationPrevue.prestationPrevue0.villePrestationPrevue4;

cerfaFields['dateDebutPrestationPrevue5']					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3.from: '');
cerfaFields['dateFinPrestationPrevue5'] 					 = ($qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp111PE8.prestationPrevue.prestationPrevue0.datePrestationPrevue3.to: '');
cerfaFields['nombreJoursPrestationPrevue5']					 = $qp111PE8.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue5;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue5']	 = $qp111PE8.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue5; 
cerfaFields['lieuEtVillePrestationPrevue5'] 				 = $qp111PE8.prestationPrevue.prestationPrevue0.villePrestationPrevue5;
//Déclaration sur l’honneur
cerfaFields['civiliteNomPrenom']                              = $qp111PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp111PE8.etatCivil.identificationDeclarant.nomNaissanceDeclarant + ' ' + $qp111PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['faitA']                                          = $qp111PE8.signature.signature.faitA;
cerfaFields['faitLe']                                         = $qp111PE8.signature.signature.faitLe;
cerfaFields['signatureCoche']                                 = $qp111PE8.signature.signature.signatureCoche;
cerfaFields['signatureTexte']                                 = "Je déclare sur l’honneur l'exactitude des informations de la formalité et signe la présente déclaration.";



var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp111PE8.signature.signature.faitLe,
		autoriteHabilitee :"Direction départementale de la cohésion sociale et de la protection des populations",
		demandeContexte : "Renouvellement de la déclaration préalable en vue d’une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire LPS sport.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
* Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationRevision);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);





var finalDocItem = finalDoc.save('Entraineur_activites_nautiques_renouvellement_LPS.pdf');


return spec.create({
    id : 'review',
    label : 'Entraineur d\'activités nautiques - renouvellement de la déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de renouvellement de la declaration préalable de libre prestation de services pour la profession d\'entraineur d\'activités nautiques.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
