**Procédure élémentaire 2** : Animateur de sports d’adresse (QP031)

**Procédure** : déclaration initiale de libre établissement, délivrance de la
carte professionnelle

**Contexte** : Libre établissement – Demande initiale – EM ne règlemente pas la
profession

-   RE pouvant justifier avoir exercé l’activité, dans un EM de l’UE ou partie à
    l’EEE qui ne réglemente pas l’accès à l’activité ou son exercice, à temps
    plein pendant un an au moins au cours des dix années précédentes ou pendant
    une durée équivalente en cas d’exercice à temps partiel et être titulaire
    d’une ou plusieurs attestations de compétences ou d’un ou plusieurs titres
    de formation délivrés par l’autorité compétente d’un de ces Etats

**Référence-id** : Formalités SCN/GQ/Sport/Animateur de sports d’adresse/Libre
établissement/Demande initiale/EM ne réglemente pas la profession/Attestation de
compétences et d’expérience professionnelle

**Formulaire** : Formulaire LE sport

**Mode de transmission** : envoi papier

**Destinataire** : direction départementale de la cohésion sociale (DDCS) ou
direction départementale de la cohésion sociale et de la protection des
populations (DDCSPP) du département d’exercice ou du principal exercice

Niveau : départemental

**Délais** :

-   L’autorité compétente délivre un accusé de réception, un mois après la
    réception du dossier

-   La carte professionnelle est délivrée dans un délai de 3 mois

-   Silence suivant ce délai vaut rejet

**Pièces justificatives** :

-   Copie d’une photographie d’identité récente

-   Copie de la pièce d’identité (en cours de validité, recto verso : extrait
    d'acte de naissance ou fiche d'état civil ou carte d'identité ou passeport)

-   Copie de chacun des diplômes, titres ou certificats invoqués ou de
    l’autorisation d’exercice et, le cas échant de l’attestation de révision en
    cours de validité pour les qualifications soumises à l’obligation de
    recyclage

-   Copie de toute pièce justifiant que le déclarant a exercé l'activité dans
    cet Etat pendant au moins un an à temps complet ou une durée équivalente au
    cours des dix dernières années

-   Copie de l'attestation prouvant la maitrise suffisante de la langue
    française (attestation de qualification délivrée à l’issue d’une formation
    en français, attestation de niveau en français délivrée par une institution
    spécialisée ou document attestant d’une expérience professionnelle acquise
    en France)

-   Copie du certificat médical de non contre-indication à la pratique et à
    l’encadrement des activités physiques ou sportives concernées, datant de
    moins d’un an

-   Copie de la déclaration de l’autorité compétente de l’Etat, membre ou
    partie, d’établissement, datant de moins d’un an, attestant de l’absence de
    sanctions

**Commentaires** :

-   Lorsque la reconnaissance de la qualification du candidat lui confère le
    droit d’exercer la profession en question, il se voit remettre une carte
    professionnelle, à renouveler tous les cinq ans

-   Si le requérant ne dispose pas d’éléments pour prouver son niveau de langue
    française, un entretien téléphonique en direct permettra de l’attester

-   Le préfet doit impérativement motiver une décision de refus de délivrance
    d’une carte professionnelle
