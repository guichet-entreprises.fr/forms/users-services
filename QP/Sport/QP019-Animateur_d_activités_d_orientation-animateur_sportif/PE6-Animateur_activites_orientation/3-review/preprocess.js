var cerfaFields = {};
var civNomPrenom = $qp019PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp019PE6.etatCivil.identificationDeclarant.nomNaissanceDeclarant + ' ' + $qp019PE6.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['declarationInitiale']                            = true ;
cerfaFields['renouvellement']            	                  = false ;

cerfaFields['accompagnateurMoyenneMontagne']                  = false;
cerfaFields['animateurActivitesAquatiques']                   = false;
cerfaFields['animateurActivitesCombinees']                    = false;
cerfaFields['animateurActivitesOrientation']                  = true;
cerfaFields['animateurActivitesCyclisme']                     = false;
cerfaFields['animateurActivitesGymniques']                    = false;
cerfaFields['animateurActivitesNautiques']                    = false;
cerfaFields['animateurActivitesPhysiques']                    = false;
cerfaFields['animateurArtsMartiaux']                          = false;
cerfaFields['animateurAthletisme']                            = false;
cerfaFields['animateurEquitation']                            = false;
cerfaFields['animateurGolf']                                  = false;
cerfaFields['animateurSportAdapte']                           = false;
cerfaFields['animateurSportsSante']                           = false;
cerfaFields['animateurSportsCollectifs']                      = false;
cerfaFields['entraineurActivitesGynmniques']                  = false;
cerfaFields['entraineurActivitesNautiques']                   = false;
cerfaFields['entraineurArtsMartiaux']                         = false;
cerfaFields['entraineurAthletisme']                           = false;
cerfaFields['entraineurEquitation']                           = false;
cerfaFields['entraineurGolf']                                 = false;
cerfaFields['entraineurSportAdapte']                          = false;
cerfaFields['entraineurSportsCollectifs']                     = false;
cerfaFields['entraineurSportsAdresse']                        = false;
cerfaFields['entraineurSportOpposition']                      = false;
cerfaFields['entraineurSportCombat']                          = false;
cerfaFields['entraineurSportGlace']                           = false;
cerfaFields['entraineurSportsRaquettes']                      = false;
cerfaFields['entraineurSportsMecaniques']                     = false;
cerfaFields['entraineurHandiSport']                           = false;
cerfaFields['GuideHautemontagne']                             = false;
cerfaFields['animateurSportsAdresse']                         = false;
cerfaFields['animateurSportOpposition']                       = false;
cerfaFields['animateurSportCombat']                           = false;
cerfaFields['animateurSportsGlace']                           = false;
cerfaFields['animateurSportsRaquette']                        = false;
cerfaFields['animateurSportsMecaniques']                      = false;
cerfaFields['animateurHandiSport']                            = false;
cerfaFields['educateurSportif']                               = false;
cerfaFields['entraineurActivitesAquatiques']                  = false;
cerfaFields['entraineurActivitesCombinees']                   = false;
cerfaFields['entraineurActivitesOrientation']                 = false;
cerfaFields['entraineurActivitesCyclisme']                    = false;
cerfaFields['MaitreNageurSauveteur']                          = false;
cerfaFields['MoniteurEscalade']                               = false;
cerfaFields['MoniteurCanoekayak']                             = false;
cerfaFields['MoniteurCanyoning']                              = false;
cerfaFields['MoniteurParachutisme']                           = false;
cerfaFields['MoniteurPlongee']                                = false;
cerfaFields['MoniteurSkiAlpin']                               = false;
cerfaFields['MoniteurSkiNordique']                            = false;
cerfaFields['MoniteurSpeleologie']                            = false;
cerfaFields['MoniteurSurfMer']                                = false;
cerfaFields['MoniteurVoile']                                  = false;
cerfaFields['MoniteurVolLibre']                               = false;

//Etat Civil
cerfaFields['civiliteMonsieur']                 = ($qp019PE6.etatCivil.identificationDeclarant.civilite=='Monsieur');
cerfaFields['civiliteMadame']                   = ($qp019PE6.etatCivil.identificationDeclarant.civilite=='Madame');
cerfaFields['nomNaissance']                     = $qp019PE6.etatCivil.identificationDeclarant.nomNaissanceDeclarant;
cerfaFields['nomUsage']                      	= $qp019PE6.etatCivil.identificationDeclarant.nomEpouseDeclarant;
cerfaFields['prenom']                  			= $qp019PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['dateNaissance']           			= $qp019PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['paysNaissance']           			= $qp019PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']             			= $qp019PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['lieuNaissance']           			= $qp019PE6.etatCivil.identificationDeclarant.communeNaissanceDeclarant;

// adresse Déclarant
cerfaFields['adresseDeclarantNumeroNomRueComplement']               = $qp019PE6.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant 
																	+ ($qp019PE6.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ' ' + $qp019PE6.adresse.adressePersonnelle.complementAdresseDeclarant : '');
cerfaFields['adresseDeclarantVille']                                = $qp019PE6.adresse.adressePersonnelle.villeAdresseDeclarant;
cerfaFields['adresseDeclarantPays']                                 = $qp019PE6.adresse.adressePersonnelle.paysAdresseDeclarant;
cerfaFields['adresseDeclarantCodePostal']                           = $qp019PE6.adresse.adressePersonnelle.codePostalAdresseDeclarant;
cerfaFields['telephoneFixeDeclarant']                             	= $qp019PE6.adresse.adressePersonnelle.telephoneAdresseDeclarant;
cerfaFields['telephoneMobileDeclarant']                          	= $qp019PE6.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;
cerfaFields['courrielDeclarant']                              		= $qp019PE6.adresse.adressePersonnelle.mailAdresseDeclarant;

// situation professionnelle actuelle 
cerfaFields['emploiActuel']                                   = $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.emploiActuel;
cerfaFields['emploiActuelLieuEtablissement']                  = $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.emploiLieuEtablissement;
cerfaFields['situationActuelleSalarie']                       = ($qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Salarié');
cerfaFields['situationActuelleTravailleurIndependant']        = ($qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Travailleur indépendant');
cerfaFields['situationActuelleAutre']                         = ($qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.situationActuelle=='Autre');
cerfaFields['employeurNom']                                   = $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurNom;
cerfaFields['employeurPrenom']                                = $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurPrenom;
cerfaFields['employeurAdresse']                               = ($qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.numeroLibelleAdresseEmployeur != null ? $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.numeroLibelleAdresseEmployeur : '') 
																+ ' ' + ($qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.codePostalAdresseEmployeur != null ? ' '+ $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.codePostalAdresseEmployeur : '') 
																+ ' ' + ($qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.villeAdresseEmployeur != null ? ' '+ $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.villeAdresseEmployeur : '')
																+ ' ' + ($qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.paysAdresseEmployeur != null ? ' '+ $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.paysAdresseEmployeur : '');
cerfaFields['employeurRaisonSociale']                         = $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurRaisonSociale;
cerfaFields['employeurNatureJuridique']                       = $qp019PE6.situationProfessionnelleActuelle.situationProfessionnelleActuelle0.employeurSalarie.employeurNatureJuridique;




//Diplômes
cerfaFields['intituleDiplome1']                               = $qp019PE6.diplomes.diplomes0.intituleDiplome1;
cerfaFields['paysDelivranceDiplome1']                         = $qp019PE6.diplomes.diplomes0.paysDelivranceDiplome1;
cerfaFields['dateDelivranceDiplome1']                         = $qp019PE6.diplomes.diplomes0.dateDelivranceDiplome1;
cerfaFields['organismeDelivranceDiplome1']                    = $qp019PE6.diplomes.diplomes0.organismeDelivrance1;
cerfaFields['lieuFormationDiplome1']                          = $qp019PE6.diplomes.diplomes0.lieuFormationDiplome1;
cerfaFields['debutFormationDiplome1']                         = $qp019PE6.diplomes.diplomes0.debutFormationDiplome1.from;
cerfaFields['finFormationDiplome1']                           = $qp019PE6.diplomes.diplomes0.debutFormationDiplome1.to;
cerfaFields['dureeFormationDiplome1']                         = $qp019PE6.diplomes.diplomes0.dureeFormationDiplome1;
cerfaFields['nombreHeuresFormationDiplome1']                  = $qp019PE6.diplomes.diplomes0.nombreHeuresFormationDiplome1;
cerfaFields['pourcentageTheoriqueFormation1']                 = $qp019PE6.diplomes.diplomes0.pourcentageTheoriqueFormation1;
cerfaFields['pourcentagePratiqueFormation1']                  = $qp019PE6.diplomes.diplomes0.pourcentagePratiqueFormation1;

cerfaFields['intituleDiplome2']                               = $qp019PE6.diplomes.diplomes0.intituleDiplome2;
cerfaFields['paysDelivranceDiplome2']                         = $qp019PE6.diplomes.diplomes0.paysDelivranceDiplome2;
cerfaFields['dateDelivranceDiplome2']                         = $qp019PE6.diplomes.diplomes0.dateDelivranceDiplome2;
cerfaFields['organismeDelivranceDiplome2']                    = $qp019PE6.diplomes.diplomes0.organismeDelivrance2;
cerfaFields['lieuFormationDiplome2']                          = $qp019PE6.diplomes.diplomes0.lieuFormationDiplome2;
cerfaFields['debutFormationDiplome2']                         = ($qp019PE6.diplomes.diplomes0.debutFormationDiplome2 != null ? $qp019PE6.diplomes.diplomes0.debutFormationDiplome2.from: '');
cerfaFields['finFormationDiplome2']                           = ($qp019PE6.diplomes.diplomes0.debutFormationDiplome2 != null ? $qp019PE6.diplomes.diplomes0.debutFormationDiplome2.to: '');
cerfaFields['dureeFormationDiplome2']                         = $qp019PE6.diplomes.diplomes0.dureeFormationDiplome2;
cerfaFields['nombreHeuresFormationDiplome2']                  = $qp019PE6.diplomes.diplomes0.nombreHeuresFormationDiplome2;
cerfaFields['pourcentageTheoriqueFormation2']                 = $qp019PE6.diplomes.diplomes0.pourcentageTheoriqueFormation2;
cerfaFields['pourcentagePratiqueFormation2']                  = $qp019PE6.diplomes.diplomes0.pourcentagePratiqueFormation2;

cerfaFields['intituleDiplome3']                               = $qp019PE6.diplomes.diplomes0.intituleDiplome3;
cerfaFields['paysDelivranceDiplome3']                         = $qp019PE6.diplomes.diplomes0.paysDelivranceDiplome3;
cerfaFields['dateDelivranceDiplome3']                         = $qp019PE6.diplomes.diplomes0.dateDelivranceDiplome3;
cerfaFields['organismeDelivranceDiplome3']                    = $qp019PE6.diplomes.diplomes0.organismeDelivrance3;
cerfaFields['lieuFormationDiplome3']                          = $qp019PE6.diplomes.diplomes0.lieuFormationDiplome3;
cerfaFields['debutFormationDiplome3']                         = ($qp019PE6.diplomes.diplomes0.debutFormationDiplome3 != null ? $qp019PE6.diplomes.diplomes0.debutFormationDiplome3.from: '');
cerfaFields['finFormationDiplome3']                           = ($qp019PE6.diplomes.diplomes0.debutFormationDiplome3 != null ? $qp019PE6.diplomes.diplomes0.debutFormationDiplome3.to: '');
cerfaFields['dureeFormationDiplome3']                         = $qp019PE6.diplomes.diplomes0.dureeFormationDiplome3;
cerfaFields['nombreHeuresFormationDiplome3']                  = $qp019PE6.diplomes.diplomes0.nombreHeuresFormationDiplome3;
cerfaFields['pourcentageTheoriqueFormation3']                 = $qp019PE6.diplomes.diplomes0.pourcentageTheoriqueFormation3;
cerfaFields['pourcentagePratiqueFormation3']                  = $qp019PE6.diplomes.diplomes0.pourcentagePratiqueFormation3;

cerfaFields['intituleDiplome4']                               = $qp019PE6.diplomes.diplomes0.intituleDiplome4;
cerfaFields['paysDelivranceDiplome4']                         = $qp019PE6.diplomes.diplomes0.paysDelivranceDiplome4;
cerfaFields['dateDelivranceDiplome4']                         = $qp019PE6.diplomes.diplomes0.dateDelivranceDiplome4;
cerfaFields['organismeDelivranceDiplome4']                    = $qp019PE6.diplomes.diplomes0.organismeDelivrance4;
cerfaFields['lieuFormationDiplome4']                          = $qp019PE6.diplomes.diplomes0.lieuFormationDiplome4;
cerfaFields['debutFormationDiplome4']                         = ($qp019PE6.diplomes.diplomes0.debutFormationDiplome4 != null ? $qp019PE6.diplomes.diplomes0.debutFormationDiplome4.from: '');
cerfaFields['finFormationDiplome4']                           = ($qp019PE6.diplomes.diplomes0.debutFormationDiplome4 != null ? $qp019PE6.diplomes.diplomes0.debutFormationDiplome4.to: '');
cerfaFields['dureeFormationDiplome4']                         = $qp019PE6.diplomes.diplomes0.dureeFormationDiplome4;
cerfaFields['nombreHeuresFormationDiplome4']                  = $qp019PE6.diplomes.diplomes0.nombreHeuresFormationDiplome4;
cerfaFields['pourcentageTheoriqueFormation4']                 = $qp019PE6.diplomes.diplomes0.pourcentageTheoriqueFormation4;
cerfaFields['pourcentagePratiqueFormation4']                  = $qp019PE6.diplomes.diplomes0.pourcentagePratiqueFormation4;

cerfaFields['intituleDiplome5']                               = $qp019PE6.diplomes.diplomes0.intituleDiplome5;
cerfaFields['paysDelivranceDiplome5']                         = $qp019PE6.diplomes.diplomes0.paysDelivranceDiplome5;
cerfaFields['dateDelivranceDiplome5']                         = $qp019PE6.diplomes.diplomes0.dateDelivranceDiplome5;
cerfaFields['organismeDelivranceDiplome5']                    = $qp019PE6.diplomes.diplomes0.organismeDelivrance5;
cerfaFields['lieuFormationDiplome5']                          = $qp019PE6.diplomes.diplomes0.lieuFormationDiplome5;
cerfaFields['debutFormationDiplome5']                         = ($qp019PE6.diplomes.diplomes0.debutFormationDiplome5 != null ? $qp019PE6.diplomes.diplomes0.debutFormationDiplome5.from: '');
cerfaFields['finFormationDiplome5']                           = ($qp019PE6.diplomes.diplomes0.debutFormationDiplome5 != null ? $qp019PE6.diplomes.diplomes0.debutFormationDiplome5.to: '');
cerfaFields['dureeFormationDiplome5']                         = $qp019PE6.diplomes.diplomes0.dureeFormationDiplome5;
cerfaFields['nombreHeuresFormationDiplome5']                  = $qp019PE6.diplomes.diplomes0.nombreHeuresFormationDiplome5;
cerfaFields['pourcentageTheoriqueFormation5']                 = $qp019PE6.diplomes.diplomes0.pourcentageTheoriqueFormation5;
cerfaFields['pourcentagePratiqueFormation5']                  = $qp019PE6.diplomes.diplomes0.pourcentagePratiqueFormation5;

//Expériences professionnelles 

cerfaFields['experienceProfessionnelleIntitule1']             = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule1;
cerfaFields['experienceProfessionnelleNomEmployeur1']         = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur1;
cerfaFields['experienceProfessionnelleAdresseEmployeur1']     = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurNumNom1 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurNumNom1 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurCP1 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurCP1 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurVille1 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurVille1 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurPays1 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur1.experienceProfessionnelleAdresseEmployeurPays1 : '');
cerfaFields['experienceProfessionnelleDateDebut1']            = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1.from : '';
cerfaFields['experienceProfessionnelleDateFin1']              = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut1.to : '';
cerfaFields['experienceProfessionnelleJeunes1']               = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes1']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Adultes');
cerfaFields['experienceProfessionnelleSeniors1']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Séniors');
cerfaFields['experienceProfessionnelleHandicap1']             = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne1=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure1']      = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure1;
cerfaFields['experienceProfessionnelleNiveauResponsabilite1'] = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite1;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo1']    = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo1;

cerfaFields['experienceProfessionnelleIntitule2']             = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule2;
cerfaFields['experienceProfessionnelleNomEmployeur2']         = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur2;
cerfaFields['experienceProfessionnelleAdresseEmployeur2']     = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurNumNom2 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurNumNom2 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurCP2 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurCP2 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurVille2 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurVille2 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurPays2 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur2.experienceProfessionnelleAdresseEmployeurPays2 : '');
cerfaFields['experienceProfessionnelleDateDebut2']            = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2.from : '';
cerfaFields['experienceProfessionnelleDateFin2']              = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut2.to : '';
cerfaFields['experienceProfessionnelleJeunes2']               = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes2']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Adultes');
cerfaFields['experienceProfessionnelleSeniors2']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Séniors');
cerfaFields['experienceProfessionnelleHandicap2']             = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne2=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure2']      = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure2;
cerfaFields['experienceProfessionnelleNiveauResponsabilite2'] = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite2;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo2']    = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo2;

cerfaFields['experienceProfessionnelleIntitule3']             = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule3;
cerfaFields['experienceProfessionnelleNomEmployeur3']         = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur3;
cerfaFields['experienceProfessionnelleAdresseEmployeur3']     = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurNumNom3 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurNumNom3 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurCP3 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurCP3 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurVille3 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurVille3 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurPays3 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur3.experienceProfessionnelleAdresseEmployeurPays3 : '');
cerfaFields['experienceProfessionnelleDateDebut3']            = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3.from : '';
cerfaFields['experienceProfessionnelleDateFin3']              = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut3.to : '';
cerfaFields['experienceProfessionnelleJeunes3']               = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes3']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Adultes');
cerfaFields['experienceProfessionnelleSeniors3']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Séniors');
cerfaFields['experienceProfessionnelleHandicap3']             = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne3=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure3']      = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure3;
cerfaFields['experienceProfessionnelleNiveauResponsabilite3'] = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite3;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo3']    = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo3;

cerfaFields['experienceProfessionnelleIntitule4']             = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule4;
cerfaFields['experienceProfessionnelleNomEmployeur4']         = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur4;
cerfaFields['experienceProfessionnelleAdresseEmployeur4']     = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurNumNom4 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurNumNom4 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurCP4 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurCP4 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurVille4 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurVille4 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurPays4 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur4.experienceProfessionnelleAdresseEmployeurPays4 : '');
cerfaFields['experienceProfessionnelleDateDebut4']            = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4.from : '';
cerfaFields['experienceProfessionnelleDateFin4']              = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut4.to : '';
cerfaFields['experienceProfessionnelleJeunes4']               = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes4']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Adultes');
cerfaFields['experienceProfessionnelleSeniors4']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Séniors');
cerfaFields['experienceProfessionnelleHandicap4']             = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne4=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure4']      = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure4;
cerfaFields['experienceProfessionnelleNiveauResponsabilite4'] = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite4;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo4']    = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo4;

cerfaFields['experienceProfessionnelleIntitule5']             = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleIntitule5;
cerfaFields['experienceProfessionnelleNomEmployeur5']         = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNomEmployeur5;
cerfaFields['experienceProfessionnelleAdresseEmployeur5']     = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurNumNom5 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurNumNom5 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurCP5 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurCP5 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurVille5 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurVille5 : '') + ' ' + ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurPays5 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.adresseEmployeur5.experienceProfessionnelleAdresseEmployeurPays5 : '');
cerfaFields['experienceProfessionnelleDateDebut5']            = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5.from : '';
cerfaFields['experienceProfessionnelleDateFin5']              = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5 != null ? $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleDateDebut5.to : '';
cerfaFields['experienceProfessionnelleJeunes5']               = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Jeunes');
cerfaFields['experienceProfessionnelleAdultes5']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Adultes');
cerfaFields['experienceProfessionnelleSeniors5']              = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Séniors');
cerfaFields['experienceProfessionnelleHandicap5']             = ($qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.publicConcerne5=='Personnes en situation de handicap');
cerfaFields['experienceProfessionnelleTailleStructure5']      = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleTailleStructure5;
cerfaFields['experienceProfessionnelleNiveauResponsabilite5'] = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNiveauResponsabilite5;
cerfaFields['experienceProfessionnelleNombreHeuresHebdo5']    = $qp019PE6.experiencesProfessionnelles.experiencesProfessionnelles0.experienceProfessionnelleNombreHeuresHebdo5;

// Reconnaissance qualifications professionnelles
cerfaFields['activiteDemandee']                               = $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.activiteDemande;
cerfaFields['disciplineDemandee']                  		      = $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.disciplineDemandee;
cerfaFields['prestationSouhaiteeTravailleurIndependant']      = Value('id').of($qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.situationActuelle1).eq('travailleurIndependant6');
cerfaFields['prestationSouhaiteeTourOperateur']        		  = Value('id').of($qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.situationActuelle1).eq('tourOperator');
cerfaFields['tourOperateurNomEmployeur']                      = $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.employeurNom6;
cerfaFields['tourOperateurPrenomEmployeur']                   = $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.employeurPrenom6;
cerfaFields['tourOperateurAdresseEmployeur']                  = ($qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.numeroLibelleAdresseEmployeur6 != null ? $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.numeroLibelleAdresseEmployeur6 : '') 
																+ ' ' + ($qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.codePostalAdresseEmployeur6 != null ? ' '+ $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.codePostalAdresseEmployeur6 : '') 
																+ ' ' + ($qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.villeAdresseEmployeur6 != null ? ' '+ $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.villeAdresseEmployeur6 : '')
																+ ' ' + ($qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.paysAdresseEmployeur6 != null ? ' '+ $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.paysAdresseEmployeur6 : '');
cerfaFields['tourOperateurRaisonSocialeEmployeur']            = $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.employeurRaisonSociale6;
cerfaFields['tourOperateurNatureJuridiqueEmployeur']          = $qp019PE6.reconnaissanceQualificationProfessionnelles.reconnaissanceQualificationProfessionnelles1.employeurTourOperator.employeurNatureJuridique6;

//Prestations de services prévues sur le territoire français
cerfaFields['dateDebutPrestationPrevue1']					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue1 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue1.from: '');
cerfaFields['dateFinPrestationPrevue1'] 					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue1 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue1.to: '');
cerfaFields['nombreJoursPrestationPrevue1']					 = $qp019PE6.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue1;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue1']	 = $qp019PE6.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue1; 
cerfaFields['lieuEtVillePrestationPrevue1'] 				 = $qp019PE6.prestationPrevue.prestationPrevue0.villePrestationPrevue1;

cerfaFields['dateDebutPrestationPrevue2']					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue2 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue2.from: '');
cerfaFields['dateFinPrestationPrevue2'] 					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue2 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue2.to: '');
cerfaFields['nombreJoursPrestationPrevue2']					 = $qp019PE6.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue2;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue2']	 = $qp019PE6.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue2; 
cerfaFields['lieuEtVillePrestationPrevue2'] 				 = $qp019PE6.prestationPrevue.prestationPrevue0.villePrestationPrevue2;

cerfaFields['dateDebutPrestationPrevue3']					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3.from: '');
cerfaFields['dateFinPrestationPrevue3'] 					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3.to: '');
cerfaFields['nombreJoursPrestationPrevue3']					 = $qp019PE6.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue3;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue3']	 = $qp019PE6.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue3; 
cerfaFields['lieuEtVillePrestationPrevue3'] 				 = $qp019PE6.prestationPrevue.prestationPrevue0.villePrestationPrevue3;

cerfaFields['dateDebutPrestationPrevue4']					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3.from: '');
cerfaFields['dateFinPrestationPrevue4'] 					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3.to: '');
cerfaFields['nombreJoursPrestationPrevue4']					 = $qp019PE6.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue4;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue4']	 = $qp019PE6.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue4; 
cerfaFields['lieuEtVillePrestationPrevue4'] 				 = $qp019PE6.prestationPrevue.prestationPrevue0.villePrestationPrevue4;

cerfaFields['dateDebutPrestationPrevue5']					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3.from: '');
cerfaFields['dateFinPrestationPrevue5'] 					 = ($qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3 != null ? $qp019PE6.prestationPrevue.prestationPrevue0.datePrestationPrevue3.to: '');
cerfaFields['nombreJoursPrestationPrevue5']					 = $qp019PE6.prestationPrevue.prestationPrevue0.nombreJourPrestationPrevue5;
cerfaFields['nombrePersonnesEncadreesPrestationPrevue5']	 = $qp019PE6.prestationPrevue.prestationPrevue0.nombrePersonnesPrestationPrevue5; 
cerfaFields['lieuEtVillePrestationPrevue5'] 				 = $qp019PE6.prestationPrevue.prestationPrevue0.villePrestationPrevue5;
//Déclaration sur l’honneur
cerfaFields['civiliteNomPrenom']                              = $qp019PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp019PE6.etatCivil.identificationDeclarant.nomNaissanceDeclarant + ' ' + $qp019PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['faitA']                                          = $qp019PE6.signature.signature.faitA;
cerfaFields['faitLe']                                         = $qp019PE6.signature.signature.faitLe;
cerfaFields['signatureCoche']                                 = $qp019PE6.signature.signature.signatureCoche;
cerfaFields['signatureTexte']                                 = "Je déclare sur l’honneur l'exactitude des informations de la formalité et signe la présente déclaration.";



var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp019PE6.signature.signature.faitLe,
		autoriteHabilitee :"Direction départementale de la cohésion sociale et de la protection des populations",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire LPS sport.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
* Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);



var finalDocItem = finalDoc.save('Animateur_activites_orientation_LPS.pdf');


return spec.create({
    id : 'review',
    label : 'Animateur d\'activités d\'orientation - première déclaration de libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de première demande de libre prestation de services pour la profession d\'animateur d\'activités d\'orientation.',
           description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
