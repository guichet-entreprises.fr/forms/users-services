var cerfaFields = {};
//etatCivil

cerfaFields['Civilité Nom Prénom']          = $qp206PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp206PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp206PE7.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['ville et pays de Naissance']   = $qp206PE7.etatCivil.identificationDeclarant.lieuNaissanceDeclarant +', ' + $qp206PE7.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                 = $qp206PE7.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']               = $qp206PE7.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['rue et complément adresse personnelle']   = $qp206PE7.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp206PE7.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null?', ' + $qp206PE7.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : '');
cerfaFields['telephoneMobile']      = $qp206PE7.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant; 
cerfaFields['CP et ville adresse personnelle']           = ($qp206PE7.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp206PE7.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant +' ' : ' ') + $qp206PE7.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ', ' + $qp206PE7.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']            = $qp206PE7.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']      = $qp206PE7.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['mail']           = $qp206PE7.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;

//adresse professionnelle

//cerfaFields['CP et ville adresse professionnelle']        = ($qp206PE7.adresseProfessionnelle.adresseProfessionnelle1.codePostalAdresseProfessionnelle != null ? $qp206PE7.adresseProfessionnelle.adresseProfessionnelle1.codePostalAdresseProfessionnelle + ' ' : '') + $qp206PE7.adresseProfessionnelle.adresseProfessionnelle1.villeAdresseProfessionnelle  + ', ' + $qp206PE7.adresseProfessionnelle.adresseProfessionnelle1.paysAdresseProfessionnelle;
//cerfaFields['rue et complément adresse professionnelle']  = $qp206PE7.adresseProfessionnelle.adresseProfessionnelle1.numeroLibelleAdresseProfessionnelle  + ', ' + ($qp206PE7.adresseProfessionnelle.adresseProfessionnelle1.complementAdresseProfessionnelle != null ? $qp206PE7.adresseProfessionnelle.adresseProfessionnelle1.complementAdresseProfessionnelle : '');









//signature
cerfaFields['dateJour']                		= $qp206PE7.signature.signature.dateSignature;
cerfaFields['signature']           					= $qp206PE7.signature.signature.signature;
cerfaFields['lieu']                                      	= $qp206PE7.signature.signature.lieuSignature + ', le ';


var cerfa = pdf.create('models/declaration libre.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Déclaration libre en vue d\'un libre établissement.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Review',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Formulaires générés',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration libre.pdf',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
