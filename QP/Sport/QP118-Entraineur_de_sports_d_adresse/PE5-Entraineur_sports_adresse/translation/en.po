msgid "Identification du déclarant"
msgstr "Identification of the declarant"

msgid "Formulaire de demande d'autorisation d'exercice"
msgstr "Application Form"
 
msgid "Civilité :"
msgstr "Civility"

msgid "Nom de naissance :"
msgstr "Birth name"

msgid "Nom d'usage :"
msgstr "Use name"


msgid "Prénom :"
msgstr "first name"


msgid "Date de naissance :"
msgstr "Birth date"

msgid "Commune ou ville de naissance :"
msgstr "Town or city of birth"


msgid "Pays de naissance :"
msgstr "Native country"


msgid "Nationalité :"
msgstr "Nationality"