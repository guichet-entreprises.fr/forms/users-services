msgid "Les informations suivantes permettront d'établir votre identité."
msgstr "The following information will allow to establish your identity"

msgid "Etat Civil"
msgstr "Civil status"

msgid "Civilité :"
msgstr "Civility"

msgid "Nom :"
msgstr "Family name"

msgid "Prénom(s) :"
msgstr "First name"

msgid "Date de naissance :"
msgstr "Birth date"

msgid "Lieu de naissance :"
msgstr "Town or city of birth"

msgid "Pays de naissance :"
msgstr "Country of birth"

msgid "Nationalité :"
msgstr "Nationality"

msgid "Sexe :"
msgstr "Gender"

msgid "Les informations suivantes concernent vos coordonnées dans votre pays d'origine, membre de l’Union Européenne ou partie à l’accord sur l’Espace économique européen (adresse personnelle, coordonnées téléphoniques et courriel)."
msgstr "The following information concerns your address and phone number(coordinates) in your country of origin, member(limb) of the European Union or left to the agreement on the European Economic Area (personal address, telephone number and e-mail).
msgstr "The following information are about coordinates in your country of origin (personal address, phone number and e-mail)."

msgid "Coordonnées dans le pays d'origine"
msgstr "Contact details in your country of origin"

msgid "Numéro et libellé de votre voie :"
msgstr "Street number and name"

msgid "Complément d'adresse :"
msgstr "Complement to your mailing address"

msgid "Commune ou ville :"
msgstr "Town"

msgid "Code Postal :"
msgstr "Postcode"

msgid "Pays d'origine :"
msgstr "Country of origin"

msgid "Numéro de téléphone :"
msgstr "Phone number"

msgid "Courriel dans votre pays d'origine :"
msgstr "E-mail"

msgid "Coordonnées en France"
msgstr "Contact details in France"

msgid "Numéro et libellé de votre voie en France :"
msgstr "Street number and name in France"

msgid "Complément d'adresse en France :"
msgstr "Complement to your mailing address in France"

msgid "Commune française:"
msgstr "Town in France"

msgid "Code Postal français :"
msgstr "French postcode"

msgid "Numéro de téléphone en France:"
msgstr "Phone number in France"

msgid "Courriel en France :"
msgstr "E-mail in France"