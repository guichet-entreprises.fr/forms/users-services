var cerfaFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; } 

//etatCivil

var civNomPrenom = $qp226PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp226PE5.etatCivil.identificationDeclarant.nomNaissanceDeclarant + ' ' + $qp226PE5.etatCivil.identificationDeclarant.prenomsDeclarant;

cerfaFields['nomNaissanceDeclarant']      					= $qp226PE5.etatCivil.identificationDeclarant.nomNaissanceDeclarant;
cerfaFields['nomEpouseDeclarant']                  			= $qp226PE5.etatCivil.identificationDeclarant.nomEpouseDeclarant;
cerfaFields['nomUsageDeclarant']      						= $qp226PE5.etatCivil.identificationDeclarant.nomUsageDeclarant;
cerfaFields['prenomsDeclarant']                  			= $qp226PE5.etatCivil.identificationDeclarant.prenomsDeclarant;
if($qp226PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant != null) {
	var dateTemp = new Date(parseInt($qp226PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant.getTimeInMillis()));
    var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
    var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['jourDateNaissanceDeclarant'] = day;
	cerfaFields['moisDateNaissanceDeclarant'] = month;
	cerfaFields['anneeDateNaissanceDeclarant'] = year;
}
cerfaFields['villePaysNaissanceDeclarant']         			= $qp226PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp226PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']                			= $qp226PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['demandeLE']                					= false;
cerfaFields['demandeLPS']                					= true;

//coordonnées

cerfaFields['numeroNomVoieComplementAdresseDeclarant']      = $qp226PE5.adresse.adresseContact.numeroNomVoieComplementAdresseDeclarant;
cerfaFields['telephoneDeclarant']            				= $qp226PE5.adresse.adresseContact.telephoneDeclarant;
cerfaFields['faxDeclarant']            						= $qp226PE5.adresse.adresseContact.faxDeclarant;
cerfaFields['villeAdresseDeclarant']              			= $qp226PE5.adresse.adresseContact.villeAdresseDeclarant;
cerfaFields['paysAdresseDeclarant']            				= $qp226PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['courrielDeclarant']                   			= $qp226PE5.adresse.adresseContact.courrielDeclarant;
cerfaFields['codePostalAdresseDeclarant']      				= $qp226PE5.adresse.adresseContact.codePostalAdresseDeclarant;


//demandes

cerfaFields['enseignementDanseClassique']                   = Value('id').of($qp226PE5.declaration.declaration.titreDemande).contains('classique') ? true : false;
cerfaFields['enseignementDanseContemporaine']               = Value('id').of($qp226PE5.declaration.declaration.titreDemande).contains('contemporaine') ? true : false;
cerfaFields['enseignementDanseJazz']                  		= Value('id').of($qp226PE5.declaration.declaration.titreDemande).contains('jazz') ? true : false;


//signature

cerfaFields['dateSignature']                       			= $qp226PE5.signature.signature.dateSignature;
cerfaFields['signatureCoche']                  				= $qp226PE5.signature.signature.signatureCoche;
cerfaFields['lieuSignature']              					= $qp226PE5.signature.signature.lieuSignature;
cerfaFields['signatureTexte']              					= 'Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp226PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp226PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Direction générale de la création artistique",
		demandeContexte : "Reconnaissance des qualifications en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire LPS.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveEtablissement);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveExercice);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('professeur_de_danse.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Professeur de danse - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de professeur de danse.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
