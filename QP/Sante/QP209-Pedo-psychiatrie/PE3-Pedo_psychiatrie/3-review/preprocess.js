var formFields = {};

var civNomPrenom = $qp209PE3.info2.etatCivile.civilite + ' ' + $qp209PE3.info2.etatCivile.declarantNomNaissance + ' ' + $qp209PE3.info2.etatCivile.declarantPrenoms;


//Profession considérée

formFields['typesActes']                                           = $qp209PE3.info1.profession.typesActes;
formFields['lieuExercice']                                         = $qp209PE3.info1.profession.lieuExercice;
formFields['nomProfessionLPS']                                     = $qp209PE3.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp209PE3.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp209PE3.info1.profession.specialite;
formFields['listeActes']                                           = $qp209PE3.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp209PE3.info1.profession.exerciceAutonome;
formFields['specialiteExerceeEM']                                  = $qp209PE3.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp209PE3.info1.profession.partieProfessionSiAccesPartiel;

//Prestation de service

formFields['declarationInitialeLPS']                               = false;
formFields['renouvellementLPS']                                    = true;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp209PE3.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp209PE3.info2.etatCivile.civilite).eq('monsieur') ? '' : '----';
formFields['declarantNomNaissance']                                = $qp209PE3.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp209PE3.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp209PE3.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp209PE3.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp209PE3.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp209PE3.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp209PE3.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoVille']                           = $qp209PE3.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCourriel']                        = $qp209PE3.info3.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoie']                      = $qp209PE3.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp209PE3.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoCP']                              = $qp209PE3.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp209PE3.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp209PE3.info3.coordonnees1.declarantAdressePersoTelephone;

formFields['declarantAdresseFranceNumNomVoie']                     = $qp209PE3.info3.coordonnees2.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp209PE3.info3.coordonnees2.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceCP']                             = $qp209PE3.info3.coordonnees2.declarantAdresseFranceCP;
formFields['declarantAdresseFranceVille']                          = $qp209PE3.info3.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFrancePays']                           = $qp209PE3.info3.coordonnees2.declarantAdresseFrancePays;
formFields['declarantAdresseFranceTelephone']                      = $qp209PE3.info3.coordonnees2.declarantAdresseFranceTelephone;


//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp209PE3.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp209PE3.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp209PE3.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp209PE3.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp209PE3.info4.diplome.dateReconnaissanceTitreFormationProfession;

formFields['intituleAutreTitreFormationProfession']                = $qp209PE3.info4.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp209PE3.info4.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp209PE3.info4.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp209PE3.info4.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp209PE3.info4.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

formFields['intituleTitreDiplomeSpecialisation']                   = $qp209PE3.info4.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp209PE3.info4.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp209PE3.info4.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp209PE3.info4.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp209PE3.info4.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp209PE3.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp209PE3.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp209PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp209PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp209PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp209PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp209PE3.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp209PE3.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp209PE3.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp209PE3.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp209PE3.info5.ordre.ordreProfessionnelAdressePays != null ? $qp209PE3.info5.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp209PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp209PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp209PE3.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp209PE3.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp209PE3.info6.assuranceProfessionnelle.commentaires;


formFields['prestation1Du']                     				   = $qp209PE3.detailsRenouvellement.renouvellement.periodePreste1.from;
formFields['prestation1Au']                                        = $qp209PE3.detailsRenouvellement.renouvellement.periodePreste1.to;
formFields['prestation2Du']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste2.from : '');
formFields['prestation2Au']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste2.to : '');
formFields['prestation3Du']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste3.from : '');
formFields['prestation3Au']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste3.to : '');
formFields['prestation4Du']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste4.from : '');
formFields['prestation4Au']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste4.to : '');
formFields['prestation5Du']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste5.from : '');
formFields['prestation5Au']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste5.to : '');
formFields['prestation6Du']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste6.from : '');
formFields['prestation6Au']                                        = ($qp209PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp209PE3.detailsRenouvellement.renouvellement.periodePreste6.to : '');


formFields['prestation1ActiviteExercee']                           = $qp209PE3.detailsRenouvellement.renouvellement.prestation1ActiviteExercee;
formFields['prestation2ActiviteExercee']                           = $qp209PE3.detailsRenouvellement.renouvellement.prestation2ActiviteExercee;
formFields['prestation3ActiviteExercee']                           = $qp209PE3.detailsRenouvellement.renouvellement.prestation3ActiviteExercee;
formFields['prestation4ActiviteExercee']                           = $qp209PE3.detailsRenouvellement.renouvellement.prestation4ActiviteExercee;
formFields['prestation5ActiviteExercee']                           = $qp209PE3.detailsRenouvellement.renouvellement.prestation5ActiviteExercee;
formFields['prestation6ActiviteExercee']                           = $qp209PE3.detailsRenouvellement.renouvellement.prestation6ActiviteExercee;

 
 
//Signature

formFields['commentairesSignature']                                = $qp209PE3.info7.signature.commentairesSignature;
formFields['dateSignature']                                        = $qp209PE3.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp209PE3.info7.signature.attesteSignature;


formFields['libelleProfession']				= "Pédo-psychiatrie"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp209PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp209PE3.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Demande de renouvellement de la déclaration en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/pedo_psychiatrie.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPrecedenteDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Pedo_psychiatrie_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Pédo-psychiatrie - renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de pedo-psychiatrie',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
