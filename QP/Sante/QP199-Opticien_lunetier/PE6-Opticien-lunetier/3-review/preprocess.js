var cerfaFields = {};

var civNomPrenom = $qp199PE6.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp199PE6.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp199PE6.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['audioprothesiste']                          = false;
cerfaFields['conseillerGenetique']                       = false;
cerfaFields['preparateurPharmacie']                      = false;
cerfaFields['preparateurPharmacieHospitaliere']          = false;
cerfaFields['infirmier']                       			 = false;
cerfaFields['infirmierSpecialise']                       = false;
cerfaFields['infirmierSpecialiseAnesthesiste']           = false;
cerfaFields['infirmierSpecialiseBlocOperatoire']         = false;
cerfaFields['infirmierSpecialisePuericultrice']          = false;
cerfaFields['pedicurePodologue']                       	 = false;
cerfaFields['ergotherapeuthe']                       	 = false;
cerfaFields['psychomotricien']                       	 = false;
cerfaFields['orthophoniste']                       	     = false;
cerfaFields['orthoptiste']                       		 = false;
cerfaFields['manipulateurElectroradiologie']             = false;
cerfaFields['opticienLunetier']                          = true;
cerfaFields['dieteticien']                       		 = false;

cerfaFields['declarationInitiale']                       = false;
cerfaFields['renouvellement']                       	= true;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp199PE6.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp199PE6.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp199PE6.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp199PE6.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp199PE6.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp199PE6.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp199PE6.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp199PE6.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp199PE6.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp199PE6.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') + ' ' + ($qp199PE6.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp199PE6.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp199PE6.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp199PE6.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' + $qp199PE6.coordonnees.coordonneesDeclarant.declarantAdresseVille + ' ' + $qp199PE6.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp199PE6.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp199PE6.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance: '') + ' ' 
														+ ($qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal: '') + ' ' 
														+ ($qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp199PE6.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionExerceeEM']     					= $qp199PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionExerceeEM;
cerfaFields['professionExerceeSpecialiteEM']  			= $qp199PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM != null ? $qp199PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM : '';
cerfaFields['professionConcernee']                      = $qp199PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee != null ? $qp199PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee : '';
cerfaFields['professionConcerneeSpecialite']            = $qp199PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance != null ? $qp199PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance : '';
cerfaFields['typesActesEnvisages']                      = $qp199PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;
//Lieu exercice
cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');
cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp199PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') 
														+ ' ' + ($qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') 
														+' '+ ($qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille : '') 
														+ ', ' + ($qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp199PE6.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp199PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp199PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp199PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale != null ? $qp199PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale :'';

//informations à fournir en cas de renouvellement 

cerfaFields['datePrestationDebut1']                     = $qp199PE6.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['datePrestationFin1']                       = $qp199PE6.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['datePrestationDebut2']                     = ($qp199PE6.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp199PE6.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['datePrestationFin2']                       = ($qp199PE6.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp199PE6.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['datePrestationDebut3']                     = ($qp199PE6.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp199PE6.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['datePrestationFin3']                       = ($qp199PE6.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp199PE6.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['datePrestationDebut4']                     = ($qp199PE6.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp199PE6.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['datePrestationFin4']                       = ($qp199PE6.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp199PE6.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['datePrestationDebut5']                     = ($qp199PE6.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp199PE6.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['datePrestationFin5']                       = ($qp199PE6.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp199PE6.detailsRenouvellement.renouvellement.periodePreste5.to : '');

cerfaFields['commentairesLPSRenouvellement']            = $qp199PE6.detailsRenouvellement.renouvellement.commentairesRenouvellement != null ? $qp199PE6.detailsRenouvellement.renouvellement.commentairesRenouvellement :'' ;
cerfaFields['activiteProfessionnelsLPSRevouvellement'] = $qp199PE6.detailsRenouvellement.renouvellement.activitesProfessionnelles != null ? $qp199PE6.detailsRenouvellement.renouvellement.activitesProfessionnelles :'' ;

// Observations et Signature
cerfaFields['autresObservations']              	= $qp199PE6.observation.observation.autresObservations != null ? $qp199PE6.observation.observation.autresObservations :'' ;
cerfaFields['signatureCoche']             		= $qp199PE6.signature.signature1.signatureDeclarant;
cerfaFields['signatureDate']             		= $qp199PE6.signature.signature1.signatureDate;
cerfaFields['signature']                        ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp199PE6.signature.signature1.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp199PE6.signature.signature1.signatureDate,
		autoriteHabilitee :"Ministère chargé de la santé : Direction Générale de l’Offre de Soins",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Formulaire_LPS.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);



/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('opticien-lunetier_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Opticien-lunetier - Renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession d\'opticien-lunetier.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});