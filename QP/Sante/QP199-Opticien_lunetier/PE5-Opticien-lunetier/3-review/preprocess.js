var cerfaFields = {};

var civNomPrenom = $qp199PE5.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp199PE5.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp199PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['audioprothesiste']                          = false;
cerfaFields['conseillerGenetique']                       = false;
cerfaFields['preparateurPharmacie']                      = false;
cerfaFields['preparateurPharmacieHospitaliere']          = false;
cerfaFields['infirmier']                       			 = false;
cerfaFields['infirmierSpecialise']                       = false;
cerfaFields['infirmierSpecialiseAnesthesiste']           = false;
cerfaFields['infirmierSpecialiseBlocOperatoire']         = false;
cerfaFields['infirmierSpecialisePuericultrice']          = false;
cerfaFields['pedicurePodologue']                       	 = false;
cerfaFields['ergotherapeuthe']                       	 = false;
cerfaFields['psychomotricien']                       	 = false;
cerfaFields['orthophoniste']                       	     = false;
cerfaFields['orthoptiste']                               = false;
cerfaFields['manipulateurElectroradiologie']             = false;
cerfaFields['opticienLunetier']                          = true;
cerfaFields['dieteticien']                       		 = false;

cerfaFields['declarationInitiale']                       = true;
cerfaFields['renouvellement']                       	 = false;



//Identité du demandeur

cerfaFields['declarantNom']                             = $qp199PE5.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp199PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp199PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp199PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp199PE5.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp199PE5.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp199PE5.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp199PE5.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp199PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp199PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') 
														+ ' ' + ($qp199PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp199PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp199PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp199PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' 
														+ $qp199PE5.coordonnees.coordonneesDeclarant.declarantAdresseVille + ' ' + $qp199PE5.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp199PE5.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp199PE5.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance : '') + ' ' 
														+ ($qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal: '') 
														+ ' ' + ($qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp199PE5.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionExerceeEM']     					= $qp199PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionExerceeEM;
cerfaFields['professionExerceeSpecialiteEM']  			= $qp199PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM != null ? $qp199PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM : '';
cerfaFields['professionConcernee']                      = $qp199PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee != null ? $qp199PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee : '';
cerfaFields['professionConcerneeSpecialite']            = $qp199PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance != null ? $qp199PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance : '';
cerfaFields['typesActesEnvisages']                      = $qp199PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');
cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp199PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') 
														+ ' ' + ($qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') 
														+' '+ ($qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille : '') 
														+ ', ' + ($qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp199PE5.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp199PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp199PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp199PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale != null ? $qp199PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale :'' ;


// Observations et Signature
cerfaFields['autresObservations']              	= $qp199PE5.observation.observation.autresObservations != null ? $qp199PE5.observation.observation.autresObservations :'';
cerfaFields['signatureCoche']             		= $qp199PE5.signature.signature1.signatureDeclarant;
cerfaFields['signatureDate']             		= $qp199PE5.signature.signature1.signatureDate;
cerfaFields['signature']                        ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

//informations à fournir en cas de renouvellement 
cerfaFields['datePrestationDebut1']                     = '';
cerfaFields['datePrestationFin1']                       = '';
cerfaFields['datePrestationDebut2']                     = '';
cerfaFields['datePrestationFin2']                       = '';
cerfaFields['datePrestationDebut3']                     = '';
cerfaFields['datePrestationFin3']                       = '';
cerfaFields['datePrestationDebut4']                     = '';
cerfaFields['datePrestationFin4']                       = '';
cerfaFields['datePrestationDebut5']                     = '';
cerfaFields['datePrestationFin5']                       = '';

cerfaFields['commentairesLPSRenouvellement']            = '';
cerfaFields['activiteProfessionnelsLPSRevouvellement']  = '';


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp199PE5.signature.signature1.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp199PE5.signature.signature1.signatureDate,
		autoriteHabilitee :"Ministère chargé de la santé : Direction Générale de l’Offre de Soins",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Formulaire_LPS.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));
function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivité);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('opticien-lunetier_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Opticien-lunetier - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'opticien-lunetier.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});