var cerfaFields = {};

var civNomPrenom = $qp229PE5.identiteGroup.identite.nomDeclarant + ' ' + $qp229PE5.identiteGroup.identite.prenomsDeclarant;

//etatCivil
cerfaFields['nomDeclarant']	               = $qp229PE5.identiteGroup.identite.nomDeclarant 
+ ($qp229PE5.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp229PE5.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp229PE5.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp229PE5.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp229PE5.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp229PE5.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp229PE5.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp229PE5.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp229PE5.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp229PE5.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp229PE5.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp229PE5.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp229PE5.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp229PE5.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp229PE5.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//metiers
cerfaFields['metierReglemente']            = $qp229PE5.metiers.metiers.metierReglemente;

//signature
cerfaFields['dateDeclaration']             = $qp229PE5.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp229PE5.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp229PE5.signatureGroup.signature.signatureElectronique;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp229PE5.signatureGroup.signature.dateDeclaration,
		autoriteHabilitee :"Chambre des métiers et de l’artisanat ",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Prothésiste dentaire LPS.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('prothesiste_dentaire_LPS.pdf');

/*
 * Persistance des données obtenues
 */


return spec.create({
    id : 'review',
    label : 'Prothésiste dentaire - Déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du dossier',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de prothésiste dentaire.',
					description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
					type : 'FileReadOnly',
					value : [ finalDocItem ]
			}) ]
	}) ]
});