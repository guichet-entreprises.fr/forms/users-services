var cerfaFields = {};

var civNomPrenom = $qp232PE6.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp232PE6.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp232PE6.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['audioprothesiste']                          = false;
cerfaFields['conseillerGenetique']                       = false;
cerfaFields['preparateurPharmacie']                      = false;
cerfaFields['preparateurPharmacieHospitaliere']          = false;
cerfaFields['infirmier']                       			 = false;
cerfaFields['infirmierSpecialise']                       = false;
cerfaFields['infirmierSpecialiseAnesthesiste']           = false;
cerfaFields['infirmierSpecialiseBlocOperatoire']         = false;
cerfaFields['infirmierSpecialisePuericultrice']          = false;
cerfaFields['pedicurePodologue']                       	 = false;
cerfaFields['ergotherapeuthe']                       	 = false;
cerfaFields['psychomotricien']                       	 = true;
cerfaFields['orthophoniste']                       	     = false;
cerfaFields['orthoptiste']                       		 = false;
cerfaFields['manipulateurElectroradiologie']             = false;
cerfaFields['opticienLunetier']                          = false;
cerfaFields['dieteticien']                       		 = false;

cerfaFields['declarationInitiale']                       = false;
cerfaFields['renouvellement']                       	 = true;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp232PE6.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp232PE6.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp232PE6.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp232PE6.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp232PE6.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp232PE6.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp232PE6.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp232PE6.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp232PE6.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp232PE6.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') + ' ' + ($qp232PE6.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp232PE6.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp232PE6.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp232PE6.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' + $qp232PE6.coordonnees.coordonneesDeclarant.declarantAdresseVille + ' ' + $qp232PE6.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp232PE6.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp232PE6.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance: '') + ' ' 
														+ ($qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal: '') + ' ' 
														+ ($qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp232PE6.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionExerceeEM']     					= $qp232PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionExerceeEM;
cerfaFields['professionExerceeSpecialiteEM']  			= $qp232PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM != null ? $qp232PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM : '';
cerfaFields['professionConcernee']                      = $qp232PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee != null ? $qp232PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee : '';
cerfaFields['professionConcerneeSpecialite']            = $qp232PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance != null ? $qp232PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance : '';
cerfaFields['typesActesEnvisages']                      = $qp232PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');
cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp232PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') 
														+ ' ' + ($qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') 
														+' '+ ($qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille : '') 
														+ ', ' + ($qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp232PE6.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;



// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp232PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp232PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp232PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale != null ? $qp232PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale :'';

//informations à fournir en cas de renouvellement 

cerfaFields['datePrestationDebut1']                     = $qp232PE6.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['datePrestationFin1']                       = $qp232PE6.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['datePrestationDebut2']                     = ($qp232PE6.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp232PE6.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['datePrestationFin2']                       = ($qp232PE6.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp232PE6.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['datePrestationDebut3']                     = ($qp232PE6.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp232PE6.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['datePrestationFin3']                       = ($qp232PE6.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp232PE6.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['datePrestationDebut4']                     = ($qp232PE6.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp232PE6.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['datePrestationFin4']                       = ($qp232PE6.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp232PE6.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['datePrestationDebut5']                     = ($qp232PE6.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp232PE6.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['datePrestationFin5']                       = ($qp232PE6.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp232PE6.detailsRenouvellement.renouvellement.periodePreste5.to : '');

cerfaFields['commentairesLPSRenouvellement']            = $qp232PE6.detailsRenouvellement.renouvellement.commentairesRenouvellement != null ? $qp232PE6.detailsRenouvellement.renouvellement.commentairesRenouvellement :'';
cerfaFields['activiteProfessionnelsLPSRevouvellement'] = $qp232PE6.detailsRenouvellement.renouvellement.activitesProfessionnelles != null ? $qp232PE6.detailsRenouvellement.renouvellement.activitesProfessionnelles :'';

// Observations et Signature
cerfaFields['autresObservations']                       = $qp232PE6.commentaire.commentaire.commentairesEventuels != null ? $qp232PE6.commentaire.commentaire.commentairesEventuels :'';
cerfaFields['signatureCoche']                           = $qp232PE6.observations.observationsSignature.signatureCoche;
cerfaFields['signatureDate']                            = $qp232PE6.observations.observationsSignature.signatureDate;
cerfaFields['signature']                                ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp232PE6.observations.observationsSignature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp232PE6.observations.observationsSignature.signatureDate,
		autoriteHabilitee :"Ministère chargé de la santé : Direction Générale de l’Offre de Soins",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Formulaire_LPS.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieDeclaration);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Psychomotricien_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Psychomotricien - Renouvellement de la déclaration en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de psychomotricien.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});