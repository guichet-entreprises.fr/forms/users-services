function pad(s) { return (s < 10) ? '0' + s : s; } 
var cerfaFields = {};
//etatCivil
var civNomPrenom = $qp198PE11.etatCivil.identificationDeclarant.civilite + ' ' + $qp198PE11.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp198PE11.etatCivil.identificationDeclarant.prenomDeclarant;

//Etat civil
cerfaFields['nomDeclarant']                           = $qp198PE11.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']                        = $qp198PE11.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['prenomDeclarantComp']                    = $qp198PE11.etatCivil.identificationDeclarant.prenomDeclarantComp;
cerfaFields['nomUsageDeclarant']                      = $qp198PE11.etatCivil.identificationDeclarant.nomUsageDeclarant;
cerfaFields['nomExerciceDeclarant']                   = $qp198PE11.etatCivil.identificationDeclarant.nomExerciceDeclarant;
cerfaFields['prenomExerciceDeclarant']                = $qp198PE11.etatCivil.identificationDeclarant.prenomExerciceDeclarant;
cerfaFields['masculin']                               = $qp198PE11.etatCivil.identificationDeclarant.civilite == "Monsieur";
cerfaFields['feminin']                                = $qp198PE11.etatCivil.identificationDeclarant.civilite == "Madame";
cerfaFields['nationalite']                            = $qp198PE11.etatCivil.identificationDeclarant.nationaliteDeclarant;

var dateChamp = $qp198PE11.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateNaissanceJour'] = day;
	cerfaFields['dateNaissanceMois'] = month;
	cerfaFields['dateNaissanceAnnee'] = year;
}

cerfaFields['lieuNaissanceVille']                     = $qp198PE11.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['dateNaissanceDepartement']               = $qp198PE11.etatCivil.identificationDeclarant.departementNaissanceDeclarant;
cerfaFields['dateNaissancePays']                      = $qp198PE11.etatCivil.identificationDeclarant.paysNaissanceDeclarant;

//Situation familiale
cerfaFields['celibataire']                            = $qp198PE11.situation.familiale.statut == "Célibataire";
cerfaFields['mariee']                                 = $qp198PE11.situation.familiale.statut == "Marié(e)";
cerfaFields['pacs']                                   = $qp198PE11.situation.familiale.statut == "PACS";
cerfaFields['concubine']                              = $qp198PE11.situation.familiale.statut == "Concubin(e)";
cerfaFields['separee']                                = $qp198PE11.situation.familiale.statut == "Séparé(e)";
cerfaFields['divorce']                                = $qp198PE11.situation.familiale.statut == "Divorcé(e)";
cerfaFields['veuf']                                   = $qp198PE11.situation.familiale.statut == "Veuf(ve)";

cerfaFields['professionConjoint']                     = $qp198PE11.situation.familiale.professionConjoint;
cerfaFields['nombreEnfants']                          = $qp198PE11.situation.familiale.nombreEnfants != null ? $qp198PE11.situation.familiale.nombreEnfants + ' enfant(s)' : '';
cerfaFields['datesNaissanceEnfants']                  = $qp198PE11.situation.familiale.datesNaissanceEnfants;

//langues parlées
cerfaFields['languesParlees']                         = $qp198PE11.situation.langues.parlee;

//adresse personnelle
cerfaFields['adresseDeclarant']                       = $qp198PE11.adresse.adresseContact.numeroLibelleAdresseDeclarant 
														+' '+ ($qp198PE11.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp198PE11.adresse.adresseContact.complementAdresseDeclarant : '')
														+' '+ ($qp198PE11.adresse.adresseContact.codePostalAdresseDeclarant != null ? ', ' + $qp198PE11.adresse.adresseContact.codePostalAdresseDeclarant : '')
														+' '+ $qp198PE11.adresse.adresseContact.villeAdresseDeclarant 
														+', '+ $qp198PE11.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneFixe']                          = $qp198PE11.adresse.adresseContact.telephoneAdresseDeclarant != null ? $qp198PE11.adresse.adresseContact.telephoneAdresseDeclarant : '';
cerfaFields['telephoneMobile']                        = $qp198PE11.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['email']                                  = $qp198PE11.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['fax']                                    = $qp198PE11.adresse.adresseContact.faxAdresseDeclarant != null ? $qp198PE11.adresse.adresseContact.faxAdresseDeclarant : '';

//Diplome de medecin
cerfaFields['diplomeFr']                              = $qp198PE11.diplomeMedecin.etape1.paysDiplome == 'Français';
cerfaFields['diplomeEU']                              = $qp198PE11.diplomeMedecin.etape1.paysDiplome == 'Européen (d\'un Etat de l\'UE ou de l\'EEE)';
cerfaFields['diplomeSuisse']                          = $qp198PE11.diplomeMedecin.etape1.paysDiplome == 'Suisse';
cerfaFields['diplomeAutre']                           = $qp198PE11.diplomeMedecin.etape1.paysDiplome == 'Autre';

var dateChamp = $qp198PE11.diplomeMedecin.etape1.dateObtentionDiplome;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionJour'] = day;
	cerfaFields['dateObtentionMois'] = month;
	cerfaFields['dateObtentionAnnee'] = year;
}
cerfaFields['lieuObtention']                          = $qp198PE11.diplomeMedecin.etape1.facObtentionDiplome;
cerfaFields['paysObtention']                          = $qp198PE11.diplomeMedecin.etape1.paysObtentionDiplome;

//Autorisation ministerielle
cerfaFields['autorisationMinisterielleOui']           = $qp198PE11.autorisation.ministerielle.autorisationMinisterielle == true;
cerfaFields['autorisationMinisterielleNon']           = $qp198PE11.autorisation.ministerielle.autorisationMinisterielle == false;

var dateChamp = $qp198PE11.autorisation.ministerielle.dateAutorisation;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateArreteAutorisationJour'] = day;
	cerfaFields['dateArreteAutorisationMois'] = month;
	cerfaFields['dateArreteAutorisationAnnee'] = year;
}

//Spécialité obtenue dans un etat de l'UE ou de l'EEE ou en Suisse
cerfaFields['specialiteObtenue1']                     = $qp198PE11.autorisation.specialiteObtenue.intituleSpecialite1;

var dateChamp = $qp198PE11.autorisation.specialiteObtenue.dateObtention1;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionSpecialite1Jour'] = day;
	cerfaFields['dateObtentionSpecialite1Mois'] = month;
	cerfaFields['dateObtentionSpecialite1Annee'] = year;
}

cerfaFields['paysObtentionSpecialite1']               = $qp198PE11.autorisation.specialiteObtenue.paysObtention1;
cerfaFields['universiteObtentionSpecialite1']         = $qp198PE11.autorisation.specialiteObtenue.universiteObtention1;

cerfaFields['specialiteObtenue2']                     = $qp198PE11.autorisation.specialiteObtenue.intituleSpecialite2 != null ? $qp198PE11.autorisation.specialiteObtenue.intituleSpecialite2 : '';

var dateChamp = $qp198PE11.autorisation.specialiteObtenue.dateObtention2;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionSpecialite2Jour'] = day;
	cerfaFields['dateObtentionSpecialite2Mois'] = month;
	cerfaFields['dateObtentionSpecialite2Annee'] = year;
}

cerfaFields['paysObtentionSpecialite2']               = $qp198PE11.autorisation.specialiteObtenue.paysObtention2 != null ? $qp198PE11.autorisation.specialiteObtenue.paysObtention2 : '';
cerfaFields['universiteObtentionSpecialite2']         = $qp198PE11.autorisation.specialiteObtenue.universiteObtention2 != null ? $qp198PE11.autorisation.specialiteObtenue.universiteObtention2 : '';

//Qualification en médecine générale
cerfaFields['universiteObtentionEEESuisseUE']         = $qp198PE11.qualification.medecineGenerale.universiteObtention;
cerfaFields['paysObtentionEEESuisseUE']               = $qp198PE11.qualification.medecineGenerale.paysObtention;

var dateChamp = $qp198PE11.qualification.medecineGenerale.dateObtention;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionEEESuisseUEJour'] = day;
	cerfaFields['dateObtentionEEESuisseUEMois'] = month;
	cerfaFields['dateObtentionEEESuisseUEAnnee'] = year;
}

//Inscription ou enregistrement à un ordre en France
cerfaFields['oui17']                                  = $qp198PE11.qualification.inscriptionOrdre.inscriptionOrdre == true;
cerfaFields['non17']                                  = $qp198PE11.qualification.inscriptionOrdre.inscriptionOrdre == false;
cerfaFields['etatsAnneesInscriptionEnregistrement17'] = $qp198PE11.qualification.inscriptionOrdre.inscriptionOrdreOui != null ? $qp198PE11.qualification.inscriptionOrdre.inscriptionOrdreOui : '';
cerfaFields['ouiRefus17']                             = $qp198PE11.qualification.inscriptionOrdre.inscriptionOrdreRejet == true;
cerfaFields['nonRefus17']                             = $qp198PE11.qualification.inscriptionOrdre.inscriptionOrdreRejet == false;
cerfaFields['etatsAnneesRefus17']                     = $qp198PE11.qualification.inscriptionOrdre.inscriptionOrdreRejetOui != null ? $qp198PE11.qualification.inscriptionOrdre.inscriptionOrdreRejetOui : '';

//Inscription ou enregistrement à un ordre à l'étranger
cerfaFields['ouiPaysEtranger18']                      = $qp198PE11.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtranger == true;
cerfaFields['nonPaysEtranger18']                      = $qp198PE11.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtranger == false;
cerfaFields['paysAnneeInscription18']                 = $qp198PE11.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerOui != null ? $qp198PE11.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerOui : '';
cerfaFields['ouiRefus18']                             = $qp198PE11.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejet == true;
cerfaFields['nonRefus18']                             = $qp198PE11.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejet == false;
cerfaFields['paysAnneeRefus18']                       = $qp198PE11.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejetOui != null ? $qp198PE11.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejetOui : '';

//Prestation de services
cerfaFields['prestationServiceOui']                   = $qp198PE11.qualification.prestationServices.enregistrement == true;
cerfaFields['prestationServiceNon']                   = $qp198PE11.qualification.prestationServices.enregistrement == false;
cerfaFields['anneePrestationServices19']              = $qp198PE11.qualification.prestationServices.anneeEnregistrement;
cerfaFields['enregistrementPrestationServices19']     = $qp198PE11.qualification.prestationServices.numeroEnregistrement;

//Situation professionnelle dans le departement
cerfaFields['medecineGenerale']                       = $qp198PE11.situationPro.dansDepartement.qualification == 'Médecine générale';
cerfaFields['autreSpecialite']                        = $qp198PE11.situationPro.dansDepartement.qualification == 'Autre spécialité';
cerfaFields['autreSpecialiteReponse']                 = $qp198PE11.situationPro.dansDepartement.autreSpecialitePrecision != null ? $qp198PE11.situationPro.dansDepartement.autreSpecialitePrecision: '';
cerfaFields['activiteMedicaleReguliere']              = $qp198PE11.situationPro.dansDepartement.activite == 'Activité médicale régulière';
cerfaFields['activiteMedicaleIntermittente']          = $qp198PE11.situationPro.dansDepartement.activite == 'Activité médicale intermittente ou remplacements réguliers';
cerfaFields['precisionLiberale']                      = Value('id').of($qp198PE11.situationPro.dansDepartement.activiteIntermittentePrecision).contains('liberal');
cerfaFields['precisionSalariee']                      = Value('id').of($qp198PE11.situationPro.dansDepartement.activiteIntermittentePrecision).contains('salariee');
cerfaFields['aucuneActiviteMedicale']                 = $qp198PE11.situationPro.dansDepartement.activite == 'Aucune activité médicale';

//Adresse de l’activite la plus importante en temps (activite principale)
cerfaFields['adresseActivitePrincipaleLiberal']       = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.typeActivite).eq('liberal') ? true : false;
cerfaFields['adresseActivitePrincipaleHospitalier']   = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['adresseActivitePrincipaleSalarie']       = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.typeActivite).eq('salarie') ? true : false;
cerfaFields['adresseActivitePrincipaleBenevole']      = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.typeActivite).eq('benevole') ? true : false;
cerfaFields['adresseActivitePrincipaleTempsPlein']    = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['adresseActivitePrincipaleTempsPartiel']  = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPartiel') ? true : false;
cerfaFields['fonctionActivitePrincipale']             = $qp198PE11.activitePrincipale.adresseActivite.fonction;
cerfaFields['statutActivitePrincipale']               = $qp198PE11.activitePrincipale.adresseActivite.statut;

var dateChamp = $qp198PE11.activitePrincipale.adresseActivite.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['jourDebutActivitePrincipale'] = day;
	cerfaFields['moisDebutActivitePrincipale'] = month;
	cerfaFields['anneeebutActivitePrincipale'] = year;
}

cerfaFields['denominationSociale']                    = $qp198PE11.activitePrincipale.adresseActivite.denominationSociale;
cerfaFields['adresseActivitePrincipale']              = $qp198PE11.activitePrincipale.adresseActivite.numeroLibelleAdresseDeclarant 
														+', '+ ($qp198PE11.activitePrincipale.adresseActivite.complementAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite.complementAdresseDeclarant : '') 
														+', '+ ($qp198PE11.activitePrincipale.adresseActivite.codePostalAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite.codePostalAdresseDeclarant : '')
														+' '+ $qp198PE11.activitePrincipale.adresseActivite.villeAdresseDeclarant
														+', '+ $qp198PE11.activitePrincipale.adresseActivite.paysAdresseDeclarant;
cerfaFields['telephoneActivitePrincipale']            = $qp198PE11.activitePrincipale.adresseActivite.telephoneAdresseDeclarant;
cerfaFields['faxActivitePrincipale']                  = $qp198PE11.activitePrincipale.adresseActivite.faxAdresseDeclarant;
cerfaFields['mobileActivitePrincipale']               = $qp198PE11.activitePrincipale.adresseActivite.telephoneMobileAdresseDeclarant;
cerfaFields['emailActivitePrincipale']                = $qp198PE11.activitePrincipale.adresseActivite.mailAdresseDeclarant;
 
 for (var i=0; i<7; i++){
	cerfaFields['autreLieuActiviteLiberal'+i] 						='';
	cerfaFields['autreLieuActiviteHospitalier'+i]					='';
	cerfaFields['autreLieuActiviteSalarie'+i]						='';
	cerfaFields['autreLieuActiviteBenevole'+i]						='';				
	cerfaFields['autreLieuActiviteJour'+i]							='';
	cerfaFields['autreLieuActiviteMois'+i]							='';
	cerfaFields['autreLieuActiviteAnnee'+i]							='';
	cerfaFields['autreLieuActiviteTempsPlein'+i]					='';	
	cerfaFields['autreLieuActiviteTempsPartiel'+i]					='';
	cerfaFields['autreLieuActiviteFonction'+i]						='';
	cerfaFields['autreLieuActiviteStatut'+i]						='';
	cerfaFields['autreLieuActiviteDS'+i]							='';	
	cerfaFields['autreLieuActiviteAdresse'+i]						='';
	cerfaFields['autreLieuActiviteTel'+i]							='';
	cerfaFields['autreLieuActiviteFax'+i]                  			='';	
	cerfaFields['autreLieuActiviteMobile'+i]               			='';
	cerfaFields['autreLieuActiviteEmail'+i]							='';
 }
 
// Lieux d'activités
if($qp198PE11.situationPro.dansDepartement.activite =='Activité médicale régulière'){
cerfaFields['autreLieuActiviteLiberal0']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier0']          = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie0']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole0']             = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp198PE11.activitePrincipale.adresseActivite.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour0'] = day;
	cerfaFields['autreLieuActiviteMois0'] = month;
	cerfaFields['autreLieuActiviteAnnee0'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein0']           = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel0']         = Value('id').of($qp198PE11.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction0']             = $qp198PE11.activitePrincipale.adresseActivite.fonction != null ? $qp198PE11.activitePrincipale.adresseActivite.fonction : '';
cerfaFields['autreLieuActiviteStatut0']               = $qp198PE11.activitePrincipale.adresseActivite.statut != null ? $qp198PE11.activitePrincipale.adresseActivite.statut : '';
cerfaFields['autreLieuActiviteDS0']                   = $qp198PE11.activitePrincipale.adresseActivite.denominationSociale != null ? $qp198PE11.activitePrincipale.adresseActivite.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse0']              = $qp198PE11.activitePrincipale.adresseActivite.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite.adressePostale.complementAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite.adressePostale.codePostalAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp198PE11.activitePrincipale.adresseActivite.adressePostale.villeAdresseDeclarant
														 +', '+ $qp198PE11.activitePrincipale.adresseActivite.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel0']                  = $qp198PE11.activitePrincipale.adresseActivite.telephoneAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax0']                  = $qp198PE11.activitePrincipale.adresseActivite.faxAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile0']               = $qp198PE11.activitePrincipale.adresseActivite.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail0']                = $qp198PE11.activitePrincipale.adresseActivite.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp198PE11.activitePrincipale.adresseActivite.autreLieuActivite1){
cerfaFields['autreLieuActiviteLiberal1']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite1.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier1']          = Value('id').of($qp198PE11.activitePrincipale.adresseActivite1.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie1']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite1.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole1']             = Value('id').of($qp198PE11.activitePrincipale.adresseActivite1.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp198PE11.activitePrincipale.adresseActivite1.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour1'] = day;
	cerfaFields['autreLieuActiviteMois1'] = month;
	cerfaFields['autreLieuActiviteAnnee1'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein1']           = Value('id').of($qp198PE11.activitePrincipale.adresseActivite1.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel1']         = Value('id').of($qp198PE11.activitePrincipale.adresseActivite1.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction1']             = $qp198PE11.activitePrincipale.adresseActivite1.fonction != null ? $qp198PE11.activitePrincipale.adresseActivite1.fonction : '';
cerfaFields['autreLieuActiviteStatut1']               = $qp198PE11.activitePrincipale.adresseActivite1.statut != null ? $qp198PE11.activitePrincipale.adresseActivite1.statut : '';
cerfaFields['autreLieuActiviteDS1']                   = $qp198PE11.activitePrincipale.adresseActivite1.denominationSociale != null ? $qp198PE11.activitePrincipale.adresseActivite1.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse1']              = $qp198PE11.activitePrincipale.adresseActivite1.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite1.adressePostale.complementAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite1.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite1.adressePostale.codePostalAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite1.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp198PE11.activitePrincipale.adresseActivite1.adressePostale.villeAdresseDeclarant
														 +', '+ $qp198PE11.activitePrincipale.adresseActivite1.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel1']                  = $qp198PE11.activitePrincipale.adresseActivite1.telephoneAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite1.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax1']                  = $qp198PE11.activitePrincipale.adresseActivite1.faxAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite1.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile1']               = $qp198PE11.activitePrincipale.adresseActivite1.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail1']                = $qp198PE11.activitePrincipale.adresseActivite1.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp198PE11.activitePrincipale.adresseActivite1.autreLieuActivite2){
cerfaFields['autreLieuActiviteLiberal2']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite2.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier2']          = Value('id').of($qp198PE11.activitePrincipale.adresseActivite2.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie2']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite2.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole2']             = Value('id').of($qp198PE11.activitePrincipale.adresseActivite2.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp198PE11.activitePrincipale.adresseActivite3.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour2'] = day;
	cerfaFields['autreLieuActiviteMois2'] = month;
	cerfaFields['autreLieuActiviteAnnee2'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein2']           = Value('id').of($qp198PE11.activitePrincipale.adresseActivite2.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel2']         = Value('id').of($qp198PE11.activitePrincipale.adresseActivite2.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction2']             = $qp198PE11.activitePrincipale.adresseActivite2.fonction != null ? $qp198PE11.activitePrincipale.adresseActivite2.fonction : '';
cerfaFields['autreLieuActiviteStatut2']               = $qp198PE11.activitePrincipale.adresseActivite2.statut != null ? $qp198PE11.activitePrincipale.adresseActivite2.statut : '';
cerfaFields['autreLieuActiviteDS2']                   = $qp198PE11.activitePrincipale.adresseActivite2.denominationSociale != null ? $qp198PE11.activitePrincipale.adresseActivite2.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse2']              = $qp198PE11.activitePrincipale.adresseActivite2.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite2.adressePostale.complementAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite2.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite2.adressePostale.codePostalAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite2.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp198PE11.activitePrincipale.adresseActivite2.adressePostale.villeAdresseDeclarant
														 +', '+ $qp198PE11.activitePrincipale.adresseActivite2.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel2']                  = $qp198PE11.activitePrincipale.adresseActivite2.telephoneAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite2.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax2']                  = $qp198PE11.activitePrincipale.adresseActivite2.faxAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite2.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile2']               = $qp198PE11.activitePrincipale.adresseActivite2.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail2']                = $qp198PE11.activitePrincipale.adresseActivite2.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp198PE11.activitePrincipale.adresseActivite2.autreLieuActivite3){
cerfaFields['autreLieuActiviteLiberal3']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite3.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier3']          = Value('id').of($qp198PE11.activitePrincipale.adresseActivite3.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie3']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite3.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole3']             = Value('id').of($qp198PE11.activitePrincipale.adresseActivite3.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp198PE11.activitePrincipale.adresseActivite3.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour3'] = day;
	cerfaFields['autreLieuActiviteMois3'] = month;
	cerfaFields['autreLieuActiviteAnnee3'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein3']           = Value('id').of($qp198PE11.activitePrincipale.adresseActivite3.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel3']         = Value('id').of($qp198PE11.activitePrincipale.adresseActivite3.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction3']             = $qp198PE11.activitePrincipale.adresseActivite3.fonction != null ? $qp198PE11.activitePrincipale.adresseActivite3.fonction : '';
cerfaFields['autreLieuActiviteStatut3']               = $qp198PE11.activitePrincipale.adresseActivite3.statut != null ? $qp198PE11.activitePrincipale.adresseActivite3.statut : '';
cerfaFields['autreLieuActiviteDS3']                   = $qp198PE11.activitePrincipale.adresseActivite3.denominationSociale != null ? $qp198PE11.activitePrincipale.adresseActivite3.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse3']              = $qp198PE11.activitePrincipale.adresseActivite3.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite3.adressePostale.complementAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite3.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite3.adressePostale.codePostalAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite3.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp198PE11.activitePrincipale.adresseActivite3.adressePostale.villeAdresseDeclarant
														 +', '+ $qp198PE11.activitePrincipale.adresseActivite3.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel3']                  = $qp198PE11.activitePrincipale.adresseActivite3.telephoneAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite3.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax3']                  = $qp198PE11.activitePrincipale.adresseActivite3.faxAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite3.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile3']               = $qp198PE11.activitePrincipale.adresseActivite3.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail3']                = $qp198PE11.activitePrincipale.adresseActivite3.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp198PE11.activitePrincipale.adresseActivite3.autreLieuActivite4){
cerfaFields['autreLieuActiviteLiberal4']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite4.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier4']          = Value('id').of($qp198PE11.activitePrincipale.adresseActivite4.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie4']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite4.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole4']             = Value('id').of($qp198PE11.activitePrincipale.adresseActivite4.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp198PE11.activitePrincipale.adresseActivite4.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour4'] = day;
	cerfaFields['autreLieuActiviteMois4'] = month;
	cerfaFields['autreLieuActiviteAnnee4'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein4']           = Value('id').of($qp198PE11.activitePrincipale.adresseActivite4.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel4']         = Value('id').of($qp198PE11.activitePrincipale.adresseActivite4.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction4']             = $qp198PE11.activitePrincipale.adresseActivite4.fonction != null ? $qp198PE11.activitePrincipale.adresseActivite4.fonction : '';
cerfaFields['autreLieuActiviteStatut4']               = $qp198PE11.activitePrincipale.adresseActivite4.statut != null ? $qp198PE11.activitePrincipale.adresseActivite4.statut : '';
cerfaFields['autreLieuActiviteDS4']                   = $qp198PE11.activitePrincipale.adresseActivite4.denominationSociale != null ? $qp198PE11.activitePrincipale.adresseActivite4.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse4']              = $qp198PE11.activitePrincipale.adresseActivite4.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite4.adressePostale.complementAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite4.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite4.adressePostale.codePostalAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite4.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp198PE11.activitePrincipale.adresseActivite4.adressePostale.villeAdresseDeclarant
														 +', '+ $qp198PE11.activitePrincipale.adresseActivite4.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel4']                  = $qp198PE11.activitePrincipale.adresseActivite4.telephoneAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite4.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax4']                  = $qp198PE11.activitePrincipale.adresseActivite4.faxAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite4.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile4']               = $qp198PE11.activitePrincipale.adresseActivite4.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail4']                = $qp198PE11.activitePrincipale.adresseActivite4.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp198PE11.activitePrincipale.adresseActivite4.autreLieuActivite5){
cerfaFields['autreLieuActiviteLiberal5']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite5.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier5']          = Value('id').of($qp198PE11.activitePrincipale.adresseActivite5.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie5']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite5.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole5']             = Value('id').of($qp198PE11.activitePrincipale.adresseActivite5.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp198PE11.activitePrincipale.adresseActivite5.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour5'] = day;
	cerfaFields['autreLieuActiviteMois5'] = month;
	cerfaFields['autreLieuActiviteAnnee5'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein5']           = Value('id').of($qp198PE11.activitePrincipale.adresseActivite5.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel5']         = Value('id').of($qp198PE11.activitePrincipale.adresseActivite5.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction5']             = $qp198PE11.activitePrincipale.adresseActivite5.fonction != null ? $qp198PE11.activitePrincipale.adresseActivite5.fonction : '';
cerfaFields['autreLieuActiviteStatut5']               = $qp198PE11.activitePrincipale.adresseActivite5.statut != null ? $qp198PE11.activitePrincipale.adresseActivite5.statut : '';
cerfaFields['autreLieuActiviteDS5']                   = $qp198PE11.activitePrincipale.adresseActivite5.denominationSociale != null ? $qp198PE11.activitePrincipale.adresseActivite5.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse5']              = $qp198PE11.activitePrincipale.adresseActivite5.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite5.adressePostale.complementAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite5.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite5.adressePostale.codePostalAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite5.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp198PE11.activitePrincipale.adresseActivite5.adressePostale.villeAdresseDeclarant
														 +', '+ $qp198PE11.activitePrincipale.adresseActivite5.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel5']                  = $qp198PE11.activitePrincipale.adresseActivite5.telephoneAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite5.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax5']                  = $qp198PE11.activitePrincipale.adresseActivite5.faxAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite5.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile5']               = $qp198PE11.activitePrincipale.adresseActivite5.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail5']                = $qp198PE11.activitePrincipale.adresseActivite5.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp198PE11.activitePrincipale.adresseActivite5.autreLieuActivite6){
cerfaFields['autreLieuActiviteLiberal6']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite6.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier6']          = Value('id').of($qp198PE11.activitePrincipale.adresseActivite6.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie6']              = Value('id').of($qp198PE11.activitePrincipale.adresseActivite6.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole6']             = Value('id').of($qp198PE11.activitePrincipale.adresseActivite6.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp198PE11.activitePrincipale.adresseActivite6.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour6'] = day;
	cerfaFields['autreLieuActiviteMois6'] = month;
	cerfaFields['autreLieuActiviteAnnee6'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein6']           = Value('id').of($qp198PE11.activitePrincipale.adresseActivite6.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel6']         = Value('id').of($qp198PE11.activitePrincipale.adresseActivite6.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction6']             = $qp198PE11.activitePrincipale.adresseActivite6.fonction != null ? $qp198PE11.activitePrincipale.adresseActivite6.fonction : '';
cerfaFields['autreLieuActiviteStatut6']               = $qp198PE11.activitePrincipale.adresseActivite6.statut != null ? $qp198PE11.activitePrincipale.adresseActivite6.statut : '';
cerfaFields['autreLieuActiviteDS6']                   = $qp198PE11.activitePrincipale.adresseActivite6.denominationSociale != null ? $qp198PE11.activitePrincipale.adresseActivite6.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse6']              = $qp198PE11.activitePrincipale.adresseActivite6.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite6.adressePostale.complementAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite6.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp198PE11.activitePrincipale.adresseActivite6.adressePostale.codePostalAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite6.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp198PE11.activitePrincipale.adresseActivite6.adressePostale.villeAdresseDeclarant
														 +', '+ $qp198PE11.activitePrincipale.adresseActivite6.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel6']                  = $qp198PE11.activitePrincipale.adresseActivite6.telephoneAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite6.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax6']                  = $qp198PE11.activitePrincipale.adresseActivite6.faxAdresseDeclarant != null ? $qp198PE11.activitePrincipale.adresseActivite6.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile6']               = $qp198PE11.activitePrincipale.adresseActivite6.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail6']                = $qp198PE11.activitePrincipale.adresseActivite6.mailAdresseDeclarant;
}

//Plaques, ordonnances et annuaires
cerfaFields['libellePlaques']                         = $qp198PE11.plaquesOrdonnances.etape.plaques;
cerfaFields['libelleOrdonnances']                     = $qp198PE11.plaquesOrdonnances.etape.ordonnances;
cerfaFields['annuaires']                              = $qp198PE11.plaquesOrdonnances.etape.annuaires;

//Adresse de correspondance
cerfaFields['adresseCorrespondancePrincipale1']       = Value('id').of($qp198PE11.adressesCorrespondance.adresse.courrier).eq('adresseExercicePrincipal') ? true : false;
cerfaFields['adresseCorrespondancePersonnelle1']      = Value('id').of($qp198PE11.adressesCorrespondance.adresse.courrier).eq('adressePersonnelle') ? true : false;
cerfaFields['adresseCorrespondanceAutre1']            = Value('id').of($qp198PE11.adressesCorrespondance.adresse.courrier).eq('adresseAutre') ? true : false;
cerfaFields['adresseCorrespondanceAutrePrecision1']   = $qp198PE11.adressesCorrespondance.adresse.precisionCourrier != null ? $qp198PE11.adressesCorrespondance.adresse.precisionCourrier : '';

cerfaFields['adresseCorrespondancePrincipale2']       = Value('id').of($qp198PE11.adressesCorrespondance.adresse.rpps).eq('adresseExercicePrincipal') ? true : false;
cerfaFields['adresseCorrespondancePersonnelle2']      = Value('id').of($qp198PE11.adressesCorrespondance.adresse.rpps).eq('adressePersonnelle') ? true : false;
cerfaFields['adresseCorrespondanceAutre2']            = Value('id').of($qp198PE11.adressesCorrespondance.adresse.rpps).eq('adresseAutre') ? true : false;
cerfaFields['adresseCorrespondanceAutrePrecision2']   = $qp198PE11.adressesCorrespondance.adresse.precisionRPPS != null ? $qp198PE11.adressesCorrespondance.adresse.precisionRPPS : '';

cerfaFields['adresseCorrespondanceEmail']             = $qp198PE11.adressesCorrespondance.adresse.email;

//Sanctions prononcées hors France
cerfaFields['sanctionsPrononceeOui']                  = $qp198PE11.sanctions.instances.caractereDefinitif == true;
cerfaFields['sanctionsPrononceeNon']                  = $qp198PE11.sanctions.instances.caractereDefinitif == false;
cerfaFields['sanctionsPrononceeDescription']          = $qp198PE11.sanctions.instances.sanctionsDescription;
cerfaFields['sanctionsPrononceepays']                 = $qp198PE11.sanctions.instances.sanctionsPays;
cerfaFields['sanctionsPrononceeJuridictions']         = $qp198PE11.sanctions.instances.sanctionsJuridiction;
cerfaFields['sanctionsPrononceeDates']                = $qp198PE11.sanctions.instances.sanctionsDates;

cerfaFields['condamnationPenaleOui']                  = $qp198PE11.sanctions.instances.condamnation == true;
cerfaFields['condamnationPenaleNon']                  = $qp198PE11.sanctions.instances.condamnation == false;
cerfaFields['condamnationPenaleDescription']          = $qp198PE11.sanctions.instances.condamnationDescription;
cerfaFields['condamnationPenalePays']                 = $qp198PE11.sanctions.instances.condamnationPays;
cerfaFields['condamnationPenaleJuridictions']         = $qp198PE11.sanctions.instances.condamnationJuridiction;
cerfaFields['condamnationPenaleDates']                = $qp198PE11.sanctions.instances.condamnationDates;

//Instances en cours à l'étranger
cerfaFields['instanceJudiciaireOui']                  = $qp198PE11.sanctions.typesInstances.instanceJuriciaire == true;
cerfaFields['instanceJudiciaireNon']                  = $qp198PE11.sanctions.typesInstances.instanceJuriciaire == false;
cerfaFields['instanceJudiciairePays']                 = $qp198PE11.sanctions.typesInstances.instancePays;
cerfaFields['instanceJudiciaireJuridictions']         = $qp198PE11.sanctions.typesInstances.instanceJuridiction;

cerfaFields['signatureHonneurCodeDeontologie']        = true;
cerfaFields['signatureHonneurAssurance']              = true;
cerfaFields['inscriptionOrdreDepartement']            = $qp198PE11.signature.signature.departementExercice;

var dateChamp = $qp198PE11.signature.signature.dateSignature;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear().toString();
	cerfaFields['inscriptionOrdreDepartementJour'] = day;
	cerfaFields['inscriptionOrdreDepartementMois'] = month;
	cerfaFields['inscriptionOrdreDepartementAnnee'] = year;
}

cerfaFields['luApprouve']                             = 'Lu et approuvé';
cerfaFields['signature']                              = civNomPrenom;
cerfaFields['oppositionTransmissionDonnees']          = $qp198PE11.signature.signature.signatureOpposition1;
cerfaFields['oppositionCoordonneesPro']               = $qp198PE11.signature.signature.signatureOpposition2;


cerfaFields['libelleProfession']				= "Ophtalmologie"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp198PE11.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp198PE11.signature.signature.dateSignature,
		autoriteHabilitee :"Président du Conseil départemental de l’Ordre des Médecins",
		demandeContexte : "Demande d'inscription au tableau de l’Ordre des Médecins",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/formulaire inscription ordre.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjContrats);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNotification);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJournal);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Ophtalmologie_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ophtalmologie - Inscription au tableau de l\'Ordre des Médecins',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\'inscription au tableau de l\'Ordre des Médecins',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});