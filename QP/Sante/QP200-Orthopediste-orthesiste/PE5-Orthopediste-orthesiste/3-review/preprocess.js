var formFields = {};

var civNomPrenom = $qp200PE5.info1.etatCivil.civilite + ' ' + $qp200PE5.info1.etatCivil.declarantNomNaissance + ' ' + $qp200PE5.info1.etatCivil.declarantPrenoms;

//Motif de la demande

formFields['professionConcernee']                                  = "Orthopédiste-orthésiste";

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;


//Identité du demandeur

formFields['declarantNomNaissance']                                = $qp200PE5.info1.etatCivil.declarantNomNaissance;
formFields['declarantPrenoms']                                     = $qp200PE5.info1.etatCivil.declarantPrenoms;
formFields['declarantNationalite']                                 = $qp200PE5.info1.etatCivil.declarantNationalite;
formFields['declarantDateNaissance']                               = $qp200PE5.info1.etatCivil.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp200PE5.info1.etatCivil.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp200PE5.info1.etatCivil.declarantPaysNaissance;


//Adresse

formFields['declarantAdressePersoCourriel']                        = $qp200PE5.info2.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoieComplement']            = ($qp200PE5.info2.coordonnees1.declarantAdressePersoNumNomVoie != null ? $qp200PE5.info2.coordonnees1.declarantAdressePersoNumNomVoie: '') + ' ' + ($qp200PE5.info2.coordonnees1.declarantAdressePersoComplement != null ? $qp200PE5.info2.coordonnees1.declarantAdressePersoComplement: '');
//formFields['declarantAdressePersoCP']                            = $qp200PE5.info2.coordonnees1.declarantAdressePersoCP;
//formFields['declarantAdressePersoVille']                         = $qp200PE5.info2.coordonnees1.declarantAdressePersoVille;
//formFields['declarantAdressePersoPays']                          = $qp200PE5.info2.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoCPVillePays']                     = ($qp200PE5.info2.coordonnees1.declarantAdressePersoCP != null ? $qp200PE5.info2.coordonnees1.declarantAdressePersoCP: '') + ' ' + ($qp200PE5.info2.coordonnees1.declarantAdressePersoVille != null ? $qp200PE5.info2.coordonnees1.declarantAdressePersoVille: '') +' '+ ($qp200PE5.info2.coordonnees1.declarantAdressePersoPays != null ? $qp200PE5.info2.coordonnees1.declarantAdressePersoPays: '');
formFields['declarantAdressePersoTelephone']                       = $qp200PE5.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp200PE5.info2.coordonnees1.declarantAdressePersoCourriel;

//Adresse France 

formFields['declarantAdresseFranceNumNomVoieComplement']           = ($qp200PE5.info2.coordonnees2.declarantAdresseFranceNumNomVoie != null ? $qp200PE5.info2.coordonnees2.declarantAdresseFranceNumNomVoie: '') + ' ' + ($qp200PE5.info2.coordonnees2.declarantAdresseFranceComplement != null ? $qp200PE5.info2.coordonnees2.declarantAdresseFranceComplement: '');
formFields['declarantAdresseFranceCPVillePays']                    = ($qp200PE5.info2.coordonnees2.declarantAdresseFranceCP != null ? $qp200PE5.info2.coordonnees2.declarantAdresseFranceCP: '') + ' ' + ($qp200PE5.info2.coordonnees2.declarantAdresseFranceVille != null ? $qp200PE5.info2.coordonnees2.declarantAdresseFranceVille: '');
formFields['declarantAdresseFranceTelephone']                      = $qp200PE5.info2.coordonnees2.declarantAdresseFranceTelephone;
formFields['declarantAdresseFranceCourriel']                       = $qp200PE5.info2.coordonnees2.declarantAdresseFranceCourriel;

//Profession concernée

formFields['professionExercee']                                    = $qp200PE5.info3.profession.professionExercee;
formFields['specialite']                                           = $qp200PE5.info3.profession.specialite != null ? $qp200PE5.info3.profession.specialite  : '';
formFields['professionDemandee']                                   = $qp200PE5.info3.profession.professionDemandee;
formFields['specialiteDemandee']                                   = $qp200PE5.info3.profession.specialiteDemandee != null ? $qp200PE5.info3.profession.specialiteDemandee : '';
formFields['actesEnvisages']                                       = $qp200PE5.info3.profession.actesEnvisages != null ? $qp200PE5.info3.profession.actesEnvisages : '';
formFields['lieupremiereLPS']                                      = $qp200PE5.info3.profession.lieupremiereLPS != null ? $qp200PE5.info3.profession.lieupremiereLPS : '';

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp200PE5.info4.ordre.ouiNon).eq('ordreNon') ? true : false;
formFields['ordreOui']                                             = Value('id').of($qp200PE5.info4.ordre.ouiNon).eq('ordreOui') ? true : false;

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp200PE5.info4.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp200PE5.info4.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp200PE5.info4.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp200PE5.info4.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp200PE5.info4.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp200PE5.info4.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp200PE5.info4.ordre.ordreProfessionnelAdresseVille != null ? $qp200PE5.info4.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp200PE5.info4.ordre.ordreProfessionnelAdressePays != null ? $qp200PE5.info4.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = ($qp200PE5.info4.ordre.ordreProfessionnelNom != null ? $qp200PE5.info4.ordre.ordreProfessionnelNom : '') + ' ' 
																	+ ($qp200PE5.info4.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp200PE5.info4.ordre.ordreProfessionnelNumEnregistrementNom: '');


//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp200PE5.info5.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp200PE5.info5.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp200PE5.info5.assuranceProfessionnelle.commentaires != null ? $qp200PE5.info5.assuranceProfessionnelle.commentaires :'' ;

//Signature&Observations

formFields['autresObservations']                                   = $qp200PE5.info6.observations.autresObservations != null ? $qp200PE5.info6.observations.autresObservations :'' ;
formFields['dateSignature']                                        = $qp200PE5.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp200PE5.info7.signature.attesteSignature;



formFields['libelleProfession']				                       = "Orthopédiste-orthésiste"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $info7s.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp200PE5.info7.signature.dateSignature,
		autoriteHabilitee :"Ministère des Solidarités et de la Santé",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire_sante_LPS.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Formulaire_sante_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Orthopédiste-orthésiste - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'orthopédiste-orthésiste.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
