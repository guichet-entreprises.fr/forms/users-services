var formFields = {};

var civNomPrenom = $qp171PE3.info2.etatCivile.civilite + ' ' + $qp171PE3.info2.etatCivile.declarantNomNaissance + ' ' + $qp171PE3.info2.etatCivile.declarantPrenoms;


//Profession considérée

formFields['typesActes']                                           = $qp171PE3.info1.profession.typesActes;
formFields['lieuExercice']                                         = $qp171PE3.info1.profession.lieuExercice;
formFields['nomProfessionLPS']                                     = $qp171PE3.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp171PE3.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp171PE3.info1.profession.specialite;
formFields['listeActes']                                           = $qp171PE3.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp171PE3.info1.profession.exerciceAutonome;
formFields['specialiteExerceeEM']                                  = $qp171PE3.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp171PE3.info1.profession.partieProfessionSiAccesPartiel;

//Prestation de service

formFields['declarationInitialeLPS']                               = false;
formFields['renouvellementLPS']                                    = true;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp171PE3.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp171PE3.info2.etatCivile.civilite).eq('monsieur') ? '' : '----';
formFields['declarantNomNaissance']                                = $qp171PE3.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp171PE3.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp171PE3.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp171PE3.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp171PE3.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp171PE3.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp171PE3.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoVille']                           = $qp171PE3.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCourriel']                        = $qp171PE3.info3.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoie']                      = $qp171PE3.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp171PE3.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoCP']                              = $qp171PE3.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp171PE3.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp171PE3.info3.coordonnees1.declarantAdressePersoTelephone;

formFields['declarantAdresseFranceNumNomVoie']                     = $qp171PE3.info3.coordonnees2.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp171PE3.info3.coordonnees2.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceCP']                             = $qp171PE3.info3.coordonnees2.declarantAdresseFranceCP;
formFields['declarantAdresseFranceVille']                          = $qp171PE3.info3.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFrancePays']                           = $qp171PE3.info3.coordonnees2.declarantAdresseFrancePays;
formFields['declarantAdresseFranceTelephone']                      = $qp171PE3.info3.coordonnees2.declarantAdresseFranceTelephone;


//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp171PE3.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp171PE3.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp171PE3.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp171PE3.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp171PE3.info4.diplome.dateReconnaissanceTitreFormationProfession;

formFields['intituleAutreTitreFormationProfession']                = $qp171PE3.info4.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp171PE3.info4.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp171PE3.info4.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp171PE3.info4.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp171PE3.info4.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

formFields['intituleTitreDiplomeSpecialisation']                   = $qp171PE3.info4.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp171PE3.info4.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp171PE3.info4.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp171PE3.info4.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp171PE3.info4.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp171PE3.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp171PE3.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp171PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp171PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp171PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp171PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp171PE3.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp171PE3.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp171PE3.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp171PE3.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp171PE3.info5.ordre.ordreProfessionnelAdressePays != null ? $qp171PE3.info5.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp171PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp171PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp171PE3.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp171PE3.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp171PE3.info6.assuranceProfessionnelle.commentaires;


formFields['prestation1Du']                     				   = $qp171PE3.detailsRenouvellement.renouvellement.periodePreste1.from;
formFields['prestation1Au']                                        = $qp171PE3.detailsRenouvellement.renouvellement.periodePreste1.to;
formFields['prestation2Du']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste2.from : '');
formFields['prestation2Au']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste2.to : '');
formFields['prestation3Du']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste3.from : '');
formFields['prestation3Au']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste3.to : '');
formFields['prestation4Du']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste4.from : '');
formFields['prestation4Au']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste4.to : '');
formFields['prestation5Du']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste5.from : '');
formFields['prestation5Au']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste5.to : '');
formFields['prestation6Du']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste6.from : '');
formFields['prestation6Au']                                        = ($qp171PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp171PE3.detailsRenouvellement.renouvellement.periodePreste6.to : '');


formFields['prestation1ActiviteExercee']                           = $qp171PE3.detailsRenouvellement.renouvellement.prestation1ActiviteExercee;
formFields['prestation2ActiviteExercee']                           = $qp171PE3.detailsRenouvellement.renouvellement.prestation2ActiviteExercee;
formFields['prestation3ActiviteExercee']                           = $qp171PE3.detailsRenouvellement.renouvellement.prestation3ActiviteExercee;
formFields['prestation4ActiviteExercee']                           = $qp171PE3.detailsRenouvellement.renouvellement.prestation4ActiviteExercee;
formFields['prestation5ActiviteExercee']                           = $qp171PE3.detailsRenouvellement.renouvellement.prestation5ActiviteExercee;
formFields['prestation6ActiviteExercee']                           = $qp171PE3.detailsRenouvellement.renouvellement.prestation6ActiviteExercee;

 
 
//Signature

formFields['commentairesSignature']                                = $qp171PE3.info7.signature.commentairesSignature;
formFields['dateSignature']                                        = $qp171PE3.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp171PE3.info7.signature.attesteSignature;


formFields['libelleProfession']				= "Médecin qualifié en médecine générale"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp171PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp171PE3.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Demande de renouvellement de la déclaration en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/medecin_qualifie_medecine_generale.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPrecedenteDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Medecin_qualifie_medecine_generale_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Médecin qualifié en médecine générale - renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de médecin qualifié en médecine générale',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
