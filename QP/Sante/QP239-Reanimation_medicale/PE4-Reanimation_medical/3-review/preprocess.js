var cerfaFields = {};

var civNomPrenom = $qp239PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp239PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp239PE4.etatCivil.identificationDeclarant.prenomsDeclarant ;

//Déclaration 

cerfaFields['nationaliteExtracommunautaire']  					= false;
cerfaFields['nationaliteCommunautaire1']      					= true;
cerfaFields['nationaliteCommunautaire2']      					= false;

//Cordonnées
cerfaFields['madame']											= ($qp239PE4.etatCivil.identificationDeclarant.civilite=='Madame');
cerfaFields['monsieur']                        					= ($qp239PE4.etatCivil.identificationDeclarant.civilite=='Monsieur');
cerfaFields['nomEpouse']                      					= $qp239PE4.etatCivil.identificationDeclarant.nomEpouse;
cerfaFields['dateNaissance']                  					= $qp239PE4.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['prenomsDeclarant']                					= $qp239PE4.etatCivil.identificationDeclarant.prenomsDeclarant;
cerfaFields['nomDeclarant']                   					= $qp239PE4.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['communeVilleNaissance']          					= $qp239PE4.etatCivil.identificationDeclarant.communeVilleNaissance;
cerfaFields['nationaliteDeclarant']           					= $qp239PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['paysNaisanceDeclarant']                  			= $qp239PE4.etatCivil.identificationDeclarant.paysNaisanceDeclarant;
cerfaFields['adresseDeclarantNumeroNomRueComplement']           = $qp239PE4.coordonnees.coordonnees.adresseDeclarantNumeroNomRueComplement +' '+ ($qp239PE4.coordonnees.coordonnees.complementAdresse != null ? $qp239PE4.coordonnees.coordonnees.complementAdresse : ' ');
cerfaFields['communeVille']                   					= $qp239PE4.coordonnees.coordonnees.communeVille;
cerfaFields['paysDeclarant']                  					= $qp239PE4.coordonnees.coordonnees.paysDeclarant;
cerfaFields['telephoneFixe']                  					= $qp239PE4.coordonnees.coordonnees.telephoneFixe;
cerfaFields['telephonePortable']              					= $qp239PE4.coordonnees.coordonnees.telephonePortable;
cerfaFields['codePostalDeclarant']            					= $qp239PE4.coordonnees.coordonnees.codePostalDeclarant;
cerfaFields['adresseMail']                   					= $qp239PE4.coordonnees.coordonnees.adresseMail;


//Diplômes
cerfaFields['diplomeDeclarant']               	= $qp239PE4.diplome.diplomePrincipal.diplomeDeclarant;
cerfaFields['paysObtentionDiplome']           	= $qp239PE4.diplome.diplomePrincipal.paysObtentionDiplome;
cerfaFields['delivrePar']                     	= $qp239PE4.diplome.diplomePrincipal.delivrePar;
cerfaFields['dateObtentionDiplome']      		= $qp239PE4.diplome.diplomePrincipal.dateObtentionDiplome;
cerfaFields['dateReconnaissanceDiplome']      	= $qp239PE4.diplome.diplomePrincipal.dateReconnaissanceDiplome;
cerfaFields['paysDiplome1']                   	= $qp239PE4.diplomeSpecialisation.diplome1.paysDiplome1;
cerfaFields['intituleDiplome1']               	= $qp239PE4.diplomeSpecialisation.diplome1.intituleDiplome1;
cerfaFields['dateDiplome1']                   	= $qp239PE4.diplomeSpecialisation.diplome1.dateDiplome1;
cerfaFields['universiteDiplome1']             	= $qp239PE4.diplomeSpecialisation.diplome1.universiteDiplome1;
cerfaFields['paysDiplome2']                   	= $qp239PE4.diplomeSpecialisation.diplome2.paysDiplome2;
cerfaFields['intituleDiplome2']               	= $qp239PE4.diplomeSpecialisation.diplome2.intituleDiplome2;
cerfaFields['dateDiplome2']                   	= $qp239PE4.diplomeSpecialisation.diplome2.dateDiplome2;
cerfaFields['universiteDiplome2']             	= $qp239PE4.diplomeSpecialisation.diplome2.universiteDiplome2;
cerfaFields['paysDiplome3']                   	= $qp239PE4.diplomeSpecialisation.diplome3.paysDiplome3;
cerfaFields['intituleDiplome3']               	= $qp239PE4.diplomeSpecialisation.diplome3.intituleDiplome3;
cerfaFields['dateDiplome3']                   	= $qp239PE4.diplomeSpecialisation.diplome3.dateDiplome3;
cerfaFields['universiteDiplome3']             	= $qp239PE4.diplomeSpecialisation.diplome3.universiteDiplome3;
cerfaFields['paysDiplomeAutre1']              	= $qp239PE4.autreDiplomes.autreDiplome1.paysDiplomeAutre1;
cerfaFields['intituleDiplomeAutre1']          	= $qp239PE4.autreDiplomes.autreDiplome1.intituleDiplomeAutre1;
cerfaFields['dateDiplomeAutre1']              	= $qp239PE4.autreDiplomes.autreDiplome1.dateDiplomeAutre1;
cerfaFields['universiteDiplomeAutre1']        	= $qp239PE4.autreDiplomes.autreDiplome1.universiteDiplomeAutre1;
cerfaFields['paysDiplomeAutre2']              	= $qp239PE4.autreDiplomes.autreDiplome2.paysDiplomeAutre2;
cerfaFields['intituleDiplomeAutre2']          	= $qp239PE4.autreDiplomes.autreDiplome2.intituleDiplomeAutre2;
cerfaFields['dateDiplomeAutre2']              	= $qp239PE4.autreDiplomes.autreDiplome2.dateDiplomeAutre2;
cerfaFields['universiteDiplomeAutre2']        	= $qp239PE4.autreDiplomes.autreDiplome2.universiteDiplomeAutre2;
cerfaFields['paysDiplomeAutre3']              	= $qp239PE4.autreDiplomes.autreDiplome3.paysDiplomeAutre3;
cerfaFields['intituleDiplomeAutre3']          	= $qp239PE4.autreDiplomes.autreDiplome3.intituleDiplomeAutre3;
cerfaFields['dateDiplomeAutre3']              	= $qp239PE4.autreDiplomes.autreDiplome3.dateDiplomeAutre3;
cerfaFields['universiteDiplomeAutre3']        	= $qp239PE4.autreDiplomes.autreDiplome3.universiteDiplomeAutre3;
cerfaFields['paysDiplomeAutre4']              	= $qp239PE4.autreDiplomes.autreDiplome4.paysDiplomeAutre4;
cerfaFields['intituleDiplomeAutre4']          	= $qp239PE4.autreDiplomes.autreDiplome4.intituleDiplomeAutre4;
cerfaFields['dateDiplomeAutre4']              	= $qp239PE4.autreDiplomes.autreDiplome4.dateDiplomeAutre4;
cerfaFields['universiteDiplomeAutre4']        	= $qp239PE4.autreDiplomes.autreDiplome4.universiteDiplomeAutre4;
cerfaFields['paysDiplomeAutre5']              	= $qp239PE4.autreDiplomes.autreDiplome5.paysDiplomeAutre5;
cerfaFields['intituleDiplomeAutre5']          	= $qp239PE4.autreDiplomes.autreDiplome5.intituleDiplomeAutre5;
cerfaFields['dateDiplomeAutre5']              	= $qp239PE4.autreDiplomes.autreDiplome5.dateDiplomeAutre5;
cerfaFields['universiteDiplomeAutre5']        	= $qp239PE4.autreDiplomes.autreDiplome5.universiteDiplomeAutre5;
cerfaFields['paysDiplomeAutre6']              	= $qp239PE4.autreDiplomes.autreDiplome6.paysDiplomeAutre6;
cerfaFields['intituleDiplomeAutre6']          	= $qp239PE4.autreDiplomes.autreDiplome6.intituleDiplomeAutre6;
cerfaFields['dateDiplomeAutre6']              	= $qp239PE4.autreDiplomes.autreDiplome6.dateDiplomeAutre6;
cerfaFields['universiteDiplomeAutre6']        	= $qp239PE4.autreDiplomes.autreDiplome6.universiteDiplomeAutre6;

//Expériences professionnelles 
cerfaFields['natureExercicePro1']             = $qp239PE4.exerciceProEtranger.fonctionEtranger1.natureExercicePro1;
cerfaFields['organismeLieuPaysExercicePro1']  = ($qp239PE4.exerciceProEtranger.fonctionEtranger1.organisme1 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger1.organisme1 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger1.lieuExercicePro1 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger1.lieuExercicePro1 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger1.paysExercicePro1 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger1.paysExercicePro1 : ' ');  
cerfaFields['periodeExercicePro1']            = $qp239PE4.exerciceProEtranger.fonctionEtranger1.periodeExercicePro1;
cerfaFields['natureExercicePro2']             = $qp239PE4.exerciceProEtranger.fonctionEtranger2.natureExercicePro2;
cerfaFields['organismeLieuPaysExercicePro2']  = ($qp239PE4.exerciceProEtranger.fonctionEtranger2.organisme2 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger2.organisme2 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger2.lieuExercicePro2 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger2.lieuExercicePro2 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger2.paysExercicePro2 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger2.paysExercicePro2 : ' ');
cerfaFields['periodeExercicePro2']            = $qp239PE4.exerciceProEtranger.fonctionEtranger2.periodeExercicePro2;
cerfaFields['natureExercicePro3']             = $qp239PE4.exerciceProEtranger.fonctionEtranger3.natureExercicePro3;
cerfaFields['organismeLieuPaysExercicePro3']  = ($qp239PE4.exerciceProEtranger.fonctionEtranger3.organisme3 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger3.organisme3 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger3.lieuExercicePro3 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger3.lieuExercicePro3 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger3.paysExercicePro3 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger3.paysExercicePro3 : ' ');  
cerfaFields['periodeExercicePro3']            = $qp239PE4.exerciceProEtranger.fonctionEtranger3.periodeExercicePro3;
cerfaFields['natureExercicePro4']             = $qp239PE4.exerciceProEtranger.fonctionEtranger4.natureExercicePro4;
cerfaFields['organismeLieuPaysExercicePro4']  = ($qp239PE4.exerciceProEtranger.fonctionEtranger4.organisme4 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger4.organisme4 + ', ' : ' ') +($qp239PE4.exerciceProEtranger.fonctionEtranger4.lieuExercicePro4 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger4.lieuExercicePro4 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger4.paysExercicePro4 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger4.paysExercicePro4 : ' ');  
cerfaFields['periodeExercicePro4']            = $qp239PE4.exerciceProEtranger.fonctionEtranger4.periodeExercicePro4;
cerfaFields['natureExercicePro5']             = $qp239PE4.exerciceProEtranger.fonctionEtranger5.natureExercicePro5;
cerfaFields['organismeLieuPaysExercicePro5']  = ($qp239PE4.exerciceProEtranger.fonctionEtranger5.organisme5 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger5.organisme5 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger5.lieuExercicePro5 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger5.lieuExercicePro5 + ', ' : ' ') + ($qp239PE4.exerciceProEtranger.fonctionEtranger5.paysExercicePro5 != null ? $qp239PE4.exerciceProEtranger.fonctionEtranger5.paysExercicePro5 : ' ');  
cerfaFields['periodeExercicePro5']            = $qp239PE4.exerciceProEtranger.fonctionEtranger5.periodeExercicePro5;
cerfaFields['etablissement5']                 = $qp239PE4.exerciceProEtranger.fonctionEtranger5.etablissement5;


cerfaFields['statut1']                        = $qp239PE4.exerciceFrance.fonctionFrance1.statut1;
cerfaFields['tempsPlein1']                    = ($qp239PE4.exerciceFrance.fonctionFrance1.horaires1=='Temps plein');
cerfaFields['etablissement1']                 = $qp239PE4.exerciceFrance.fonctionFrance1.etablissement1;
cerfaFields['tempsPartiel1']                  = ($qp239PE4.exerciceFrance.fonctionFrance1.horaires1=='Temps partiel');
cerfaFields['periodeExercice1']               = $qp239PE4.exerciceFrance.fonctionFrance1.periodeExercice1;
cerfaFields['quotite1']						  = $qp239PE4.exerciceFrance.fonctionFrance1.quotite1;
cerfaFields['statut2']                        = $qp239PE4.exerciceFrance.fonctionFrance2.statut2;
cerfaFields['tempsPlein2']                    = ($qp239PE4.exerciceFrance.fonctionFrance2.horaires2=='Temps plein')
cerfaFields['etablissement2']                 = $qp239PE4.exerciceFrance.fonctionFrance2.etablissement2;
cerfaFields['tempsPartiel2']                  = ($qp239PE4.exerciceFrance.fonctionFrance2.horaires2=='Temps partiel');
cerfaFields['periodeExercice2']               = $qp239PE4.exerciceFrance.fonctionFrance2.periodeExercice2;
cerfaFields['quotite2']						  = $qp239PE4.exerciceFrance.fonctionFrance2.quotite2;
cerfaFields['statut3']                        = $qp239PE4.exerciceFrance.fonctionFrance3.statut3;
cerfaFields['tempsPlein3']                    = ($qp239PE4.exerciceFrance.fonctionFrance3.horaires3=='Temps plein')
cerfaFields['etablissement3']                 = $qp239PE4.exerciceFrance.fonctionFrance3.etablissement3;
cerfaFields['tempsPartiel3']                  = ($qp239PE4.exerciceFrance.fonctionFrance3.horaires3=='Temps partiel');
cerfaFields['periodeExercice3']               = $qp239PE4.exerciceFrance.fonctionFrance3.periodeExercice3;
cerfaFields['quotite3']						  = $qp239PE4.exerciceFrance.fonctionFrance3.quotite3;
cerfaFields['statut4']                        = $qp239PE4.exerciceFrance.fonctionFrance4.statut4;
cerfaFields['tempsPlein4']                    = ($qp239PE4.exerciceFrance.fonctionFrance4.horaires4=='Temps plein')
cerfaFields['etablissement4']                 = $qp239PE4.exerciceFrance.fonctionFrance4.etablissement4;
cerfaFields['tempsPartiel4']                  = ($qp239PE4.exerciceFrance.fonctionFrance4.horaires4=='Temps partiel');
cerfaFields['periodeExercice4']               = $qp239PE4.exerciceFrance.fonctionFrance4.periodeExercice4;
cerfaFields['quotite4']						  = $qp239PE4.exerciceFrance.fonctionFrance4.quotite4;
cerfaFields['statut5']                        = $qp239PE4.exerciceFrance.fonctionFrance5.statut5;
cerfaFields['tempsPlein5']                    = ($qp239PE4.exerciceFrance.fonctionFrance5.horaires5=='Temps plein')
cerfaFields['etablissement5']                 = $qp239PE4.exerciceFrance.fonctionFrance5.etablissement5;
cerfaFields['tempsPartiel5']                  = ($qp239PE4.exerciceFrance.fonctionFrance5.horaires5=='Temps partiel');
cerfaFields['periodeExercice5']               = $qp239PE4.exerciceFrance.fonctionFrance5.periodeExercice5;
cerfaFields['quotite5']						  = $qp239PE4.exerciceFrance.fonctionFrance5.quotite5;
cerfaFields['statut6']                        = $qp239PE4.exerciceFrance.fonctionFrance6.statut6;
cerfaFields['tempsPlein6']                    = ($qp239PE4.exerciceFrance.fonctionFrance6.horaires6=='Temps plein')
cerfaFields['etablissement6']                 = $qp239PE4.exerciceFrance.fonctionFrance6.etablissement6;
cerfaFields['tempsPartiel6']                  = ($qp239PE4.exerciceFrance.fonctionFrance6.horaires6=='Temps partiel');
cerfaFields['periodeExercice6']               = $qp239PE4.exerciceFrance.fonctionFrance6.periodeExercice6;
cerfaFields['quotite6']						  = $qp239PE4.exerciceFrance.fonctionFrance6.quotite6;
cerfaFields['statut7']                        = $qp239PE4.exerciceFrance.fonctionFrance7.statut7;
cerfaFields['tempsPlein7']                    = ($qp239PE4.exerciceFrance.fonctionFrance7.horaires7=='Temps plein')
cerfaFields['etablissement7']                 = $qp239PE4.exerciceFrance.fonctionFrance7.etablissement7;
cerfaFields['tempsPartiel7']                  = ($qp239PE4.exerciceFrance.fonctionFrance7.horaires7=='Temps partiel');
cerfaFields['periodeExercice7']               = $qp239PE4.exerciceFrance.fonctionFrance7.periodeExercice7;
cerfaFields['quotite7']						  = $qp239PE4.exerciceFrance.fonctionFrance7.quotite7;

cerfaFields['projetsProfessionnelsEventuels'] = $qp239PE4.projetPro.projetsProfessionnelsEventuels;

//Signature
cerfaFields['lieuSignature']                  = $qp239PE4.signatureGroup.signature.lieuSignature;
cerfaFields['dateSignature']                  = $qp239PE4.signatureGroup.signature.dateSignature;
cerfaFields['declarationHonneur']         	  = $qp239PE4.signatureGroup.signature.declarationHonneur;

//Profession
cerfaFields['libelleProfession']			  = "Médecin";
cerfaFields['specialite']					  = "Réanimation médicale (médecin spécialiste)";

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp239PE4.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp239PE4.signatureGroup.signature.dateSignature,
		autoriteHabilitee1 :"Centre National de Gestion Santé",
		autoriteHabilitee2 :"Bureau chargé des commissions d'autorisation d'exercice",
		autoriteHabilitee3 :"Le Ponant B",
		autoriteHabilitee4 :"21 rue Leblanc",
		autoriteHabilitee5 :"75737 PARIS",
		demandeContexte : "Reconnaissance de qualifications professionnelles en vue d’un libre établissement.",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/formulaire CNG medecins sage femme chir dentiste pharma.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJ
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);


var finalDocItem = finalDoc.save('Reanimation_medicale_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Réanimation médicale (médecin spécialiste) - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement pour la spécialité médicale réanimation médicale (médecin spécialiste).',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

