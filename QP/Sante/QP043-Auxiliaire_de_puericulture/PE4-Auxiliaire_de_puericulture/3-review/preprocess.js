var cerfaFields = {};

var civNomPrenom = $qp043PE4.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp043PE4.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp043PE4.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['aideSoignant']                             = false;
cerfaFields['auxiliairePuericulture']                   = true;
cerfaFields['ambulancier']                              = false;
cerfaFields['lpsDemandeInitiale']                       = true;
cerfaFields['lpsRenouvellement']                        = false;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp043PE4.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp043PE4.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp043PE4.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp043PE4.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp043PE4.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp043PE4.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp043PE4.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp043PE4.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp043PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp043PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') 
														+ ' ' + ($qp043PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? ', ' + $qp043PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp043PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp043PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') 
														+ ' ' + $qp043PE4.coordonnees.coordonneesDeclarant.declarantAdresseVille + ', ' + $qp043PE4.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp043PE4.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp043PE4.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance : '') 
														  + ' ' + ($qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal : '') 
														  + ' ' + ($qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp043PE4.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionConcernee']                      = $qp043PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee;
cerfaFields['typesActesEnvisages']                      = $qp043PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

//Lieu Exercice
cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');

cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp043PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') + ' ' + ($qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']           = ($qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') +' '+ ($qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille + ', ' : '') + ($qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp043PE4.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp043PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp043PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp043PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale;

//informations à fournir en cas de renouvellement 
cerfaFields['datePrestationDebut1']                     = '';
cerfaFields['datePrestationFin1']                       = '';
cerfaFields['datePrestationDebut2']                     = '';
cerfaFields['datePrestationFin2']                       = '';
cerfaFields['datePrestationDebut3']                     = '';
cerfaFields['datePrestationFin3']                       = '';
cerfaFields['datePrestationDebut4']                     = '';
cerfaFields['datePrestationFin4']                       = '';
cerfaFields['datePrestationDebut5']                     = '';
cerfaFields['datePrestationFin5']                       = '';

cerfaFields['commentairesLPSRenouvellement']            = '';
cerfaFields['activiteProfessionnelsLPSRevouvellement']  = '';



// Observations et Signature
cerfaFields['autresObservations']              		= $qp043PE4.observation.observation.autresObservations != null ? $qp043PE4.observation.observation.autresObservations :'';
cerfaFields['signatureCoche']             		= $qp043PE4.signature.signature1.signatureDeclarant;
cerfaFields['signatureDate']             		= $qp043PE4.signature.signature1.signatureDate;
cerfaFields['signature']                         ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'
cerfaFields['libelleProfession']						= "Auxiliaire de puériculture"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp043PE4.observations.observationsSignature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier_ premier_dossier_v2.1_GQ.pdf') //
	.apply({
		date: $qp043PE4.signature.signature1.signatureDate,
		autoriteHabilitee : "Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Auxiliaire_puericulture.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('formulaire_declaration_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Auxiliaire de puériculture - Déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'auxiliaire de puériculture.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});