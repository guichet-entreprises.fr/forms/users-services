var formFields = {};

var civNomPrenom = $qp043PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp043PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp043PE2.etatCivil.identificationDeclarant.prenomDeclarant;

//Auxiliaire de puériculture
formFields['aideSoignant']                                        = false;
formFields['ambulancier']                                         = false;
formFields['assistantDentaire']                                   = false;
formFields['audioprothesiste']                                    = false;
formFields['auxiliairePuericulture']                              = true;
formFields['conseillerGenetique']                                 = false;
formFields['dieteticien']                                         = false;
formFields['ergotherapeuthe']                                     = false;
formFields['infirmier']                                           = false;
formFields['infirmierSpecialise']                                 = false;
formFields['infirmierSpecialiseAnesthesiste']                     = false;
formFields['infirmierSpecialiseBlocOperatoire']                   = false;
formFields['infirmierSpecialisePuericultrice']                    = false;
formFields['manipulateurElectroradiologie']                       = false;
formFields['masseurKinesitherapeute']                             = false;
formFields['opticienLunetier']                                    = false;
formFields['orthophoniste']                                       = false;
formFields['orthoptiste']                                         = false;
formFields['pedicurePodologue']                                   = false;
formFields['preparateurPharmacie']                                = false;
formFields['preparateurPharmacieHospitaliere']                    = false;
formFields['professionsAppareillage']                             = false;
formFields['oculariste']                                          = false;
formFields['orthopedisteOrthesiste']                              = false;
formFields['podoOrthesiste']                                      = false;
formFields['orthoprothesiste']                                    = false;
formFields['epithesiste']                                         = false;
formFields['psychomotricien']                                     = false;
formFields['radioPhysicienMedical']                               = false;
formFields['technicienLaboratoire']                               = false;

//etatCivil
formFields['civiliteMonsieur']                                    = ($qp043PE2.etatCivil.identificationDeclarant.civilite=='Monsieur');
formFields['civiliteMadame']                                      = ($qp043PE2.etatCivil.identificationDeclarant.civilite=='Madame');
formFields['declarantNomUsage']                                   = $qp043PE2.etatCivil.identificationDeclarant.nomEpouseDeclarant;
formFields['declarantNomNaissance']                               = $qp043PE2.etatCivil.identificationDeclarant.nomDeclarant;
formFields['declarantPrenoms']                                    = $qp043PE2.etatCivil.identificationDeclarant.prenomDeclarant;
formFields['declarantDateNaissance']                              = $qp043PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
formFields['declarantLieuNaissance']                              = $qp043PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
formFields['declarantPaysNaissance']                              = $qp043PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
formFields['declarantNationalite']                                = $qp043PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
formFields['declarantCourriel']                                   = $qp043PE2.adresse.adressePersonnelle.mailAdresseDeclarant;


//adresse
formFields['declarantAdresseRueComplement']                       = $qp043PE2.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant 
																	+ ($qp043PE2.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ' ' + $qp043PE2.adresse.adressePersonnelle.complementAdresseDeclarant : '');
formFields['declarantAdresseVille']                               = $qp043PE2.adresse.adressePersonnelle.villeAdresseDeclarant;
formFields['declarantAdressePays']                                = $qp043PE2.adresse.adressePersonnelle.paysAdresseDeclarant;
formFields['declarantAdresseCP']                                  = $qp043PE2.adresse.adressePersonnelle.codePostalAdresseDeclarant;
formFields['declarantTelephoneFixe']                              = $qp043PE2.adresse.adressePersonnelle.telephoneAdresseDeclarant;
formFields['declarantTelephoneMobile']                            = $qp043PE2.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;


//diplomeProfession
formFields['declarantDiplomeIntitule']                            = $qp043PE2.diplomeProfession.diplomeProfession.intituleDiplome;
formFields['declarantDiplomePaysObtention']                       = $qp043PE2.diplomeProfession.diplomeProfession.paysObtentionDiplome;
formFields['declarantDiplomeDelivrePar']                          = $qp043PE2.diplomeProfession.diplomeProfession.delivreDiplome;
formFields['declarantDiplomeDateObtention']                       = $qp043PE2.diplomeProfession.diplomeProfession.dateObtentionDiplome;
formFields['declarantDiplomeDateReconnaissance']                  = $qp043PE2.diplomeProfession.diplomeProfession.dateReconnaissanceDiplomeEtat;	

//diplomeDetailles
formFields['diplomeIntitule1']                                    = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome1;
formFields['diplomeDateObtention1']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome1;
formFields['diplomeLieuFormation1']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome1;
formFields['diplomePaysObtention1']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome1;

formFields['diplomeIntitule2']                                    = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome2;
formFields['diplomeDateObtention2']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome2;
formFields['diplomeLieuFormation2']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome2;
formFields['diplomePaysObtention2']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome2; 

formFields['diplomeIntitule3']                                    = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome3;
formFields['diplomeDateObtention3']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome3;
formFields['diplomeLieuFormation3']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome3;
formFields['diplomePaysObtention3']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome3;

formFields['diplomeIntitule4']                                    = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome4;
formFields['diplomeDateObtention4']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome4;
formFields['diplomeLieuFormation4']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome4;
formFields['diplomePaysObtention4']                               = $qp043PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome4;


//exerciceProfessionnel
formFields['exerciceProfessionnelNature1']                        = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurNom1']                  = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement1'] = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurAdresseVillePays1']     = ($qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 != null ? $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 + ' ' : '')
																	+ ($qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 != null ? $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 : '');
formFields['exerciceProfessionnelPeriode1']                       = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1;

formFields['exerciceProfessionnelNature2']                        = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurNom2']                  = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement2'] = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurAdresseVillePays2']     = ($qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 != null ? $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 + ' ' : '')
																	+ ($qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 != null ? $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 : '');
formFields['exerciceProfessionnelPeriode2']                       = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2;

formFields['exerciceProfessionnelNature3']                        = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurNom3']                  = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement3'] = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurAdresseVillePays3']     = ($qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 != null ? $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 + ' ' : '')
																	+ ($qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 != null ? $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 : '');
formFields['exerciceProfessionnelPeriode3']                       = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3;

formFields['exerciceProfessionnelNature4']                        = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurNom4']                  = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement4'] = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurAdresseVillePays4']     = ($qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 != null ? $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 + ' ' : '')
																	+ ($qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 != null ? $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 : '');
formFields['exerciceProfessionnelPeriode4']                       = $qp043PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4;


//projetsProfessionnels
formFields['projetsProfessionnels']                               = $qp043PE2.projetsProfessionnels.projetsProfessionnels.projetsProfessionnels;
formFields['attesteHonneurDemandeUnique']                         = $qp043PE2.signatureGroup.signature.attesteHonneurDemandeUnique;
formFields['regionExercice']                                      = $qp043PE2.signatureGroup.signature.regionExercice;
formFields['signatureDate']                                       = $qp043PE2.signatureGroup.signature.dateSignature;
formFields['signatureLieu']                                       = $qp043PE2.signatureGroup.signature.lieuSignature;
formFields['signature']                                           = 'Je déclare sur l’honneur l\'exactitude des informations de la formalité et signe la présente déclaration.';
formFields['signatureCoche']                                      = $qp043PE2.signatureGroup.signature.certifieHonneur;
formFields['libelleProfession']								  	  = "Auxiliaire de puériculture"
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp043PE2.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp043PE2.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire LE.pdf') //
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPieces);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);

var finalDocItem = finalDoc.save('Auxiliaire de puériculture_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Auxiliaire de puériculture - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'auxiliaire de puériculture.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});