var cerfaFields = {};

var civNomPrenom = $qp043PE5.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp043PE5.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp043PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['aideSoignant']                            = false;
cerfaFields['auxiliairePuericulture']                   = true;
cerfaFields['ambulancier']                              = false;
cerfaFields['lpsDemandeInitiale']                       = false;
cerfaFields['lpsRenouvellement']                        = true;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp043PE5.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp043PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp043PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp043PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp043PE5.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp043PE5.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp043PE5.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp043PE5.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp043PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp043PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') + ' ' + ($qp043PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? ', ' + $qp043PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp043PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp043PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' + $qp043PE5.coordonnees.coordonneesDeclarant.declarantAdresseVille + ', ' + $qp043PE5.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp043PE5.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp043PE5.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance : '') 
														  + ' ' + ($qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal : '') 
														  + ' ' + ($qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp043PE5.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionConcernee']                      = $qp043PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee;
cerfaFields['typesActesEnvisages']                      = $qp043PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

//Lieu Exercice
cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');

cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp043PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') + ' ' + ($qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']           = ($qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') +' '+ ($qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille + ', ' : '') + ($qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp043PE5.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;

// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp043PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp043PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp043PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale;


//informations à fournir en cas de renouvellement 
cerfaFields['datePrestationDebut1']                     = $qp043PE5.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['datePrestationFin1']                       = $qp043PE5.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['datePrestationDebut2']                     = ($qp043PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp043PE5.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['datePrestationFin2']                       = ($qp043PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp043PE5.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['datePrestationDebut3']                     = ($qp043PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp043PE5.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['datePrestationFin3']                       = ($qp043PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp043PE5.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['datePrestationDebut4']                     = ($qp043PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp043PE5.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['datePrestationFin4']                       = ($qp043PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp043PE5.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['datePrestationDebut5']                     = ($qp043PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp043PE5.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['datePrestationFin5']                       = ($qp043PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp043PE5.detailsRenouvellement.renouvellement.periodePreste5.to : '');

cerfaFields['commentairesLPSRenouvellement']            = $qp043PE5.detailsRenouvellement.renouvellement.commentairesRenouvellement != null ? $qp043PE5.detailsRenouvellement.renouvellement.commentairesRenouvellement :'';
cerfaFields['activiteProfessionnelsLPSRevouvellement']  = $qp043PE5.detailsRenouvellement.renouvellement.activitesProfessionnelles != null ? $qp043PE5.detailsRenouvellement.renouvellement.activitesProfessionnelles :'';

// Observations et Signature
cerfaFields['autresObservations']              		= $qp043PE5.observation.observation.autresObservations != null ? $qp043PE5.observation.observation.autresObservations :'';
cerfaFields['signatureCoche']             		= $qp043PE5.signature.signature1.signatureDeclarant;
cerfaFields['signatureDate']             		= $qp043PE5.signature.signature1.signatureDate;
cerfaFields['signature']                         ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

cerfaFields['libelleProfession']						= "Auxiliaire de puériculture"
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp043PE5.observations.observationsSignature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier_ premier_dossier_v2.1_GQ.pdf') //
	.apply({
		date: $qp043PE5.signature.signature1.signatureDate,
		autoriteHabilitee :"Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Auxiliaire_puericulture.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieDeclaration);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Auxiliaire_puericulture_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Auxiliaire de puériculture - renouvellement de la déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'auxiliaire de puériculture.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});