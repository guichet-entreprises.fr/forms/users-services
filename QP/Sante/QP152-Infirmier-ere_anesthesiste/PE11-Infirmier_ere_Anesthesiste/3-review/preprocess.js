function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

/************************************************************************
 * Etat civil
 ************************************************************************/
var cheminEtatCivil	= $qp152PE11.etatCivilPage.etatCivilGroup;

formFields['madame']                  			   = (cheminEtatCivil.civilite=='Monsieur');
formFields['monsieur']               			   = (cheminEtatCivil.civilite=='Madame');
formFields['nomNaissance']                         = cheminEtatCivil.nomNaissance;
formFields['nomUsage']                             = cheminEtatCivil.nomUsage;
formFields['prenoms']                              = cheminEtatCivil.prenoms;
formFields['nomPrenomExercice']                    = cheminEtatCivil.nomPrenomExercice;
if($qp152PE11.etatCivilPage.etatCivilGroup.dateNaissance != null) {
	var dateTemp = new Date(parseInt($qp152PE11.etatCivilPage.etatCivilGroup.dateNaissance.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourNaissance'] 	= day;
	formFields['moisNaissance'] 	= month;
	formFields['anneeNaissance']	= year;
}
formFields['lieuNaissance']                         = cheminEtatCivil.lieuNaissance;
formFields['numDeptNaissance']                      = cheminEtatCivil.deptNaissance;
formFields['paysNaissance']                         = cheminEtatCivil.paysNaissance;
formFields['nationalite']                           = cheminEtatCivil.nationalite;

formFields['jourAcquisitionNationalite'] 	    = '';
formFields['moisAcquisitionNationalite']	    = '';
formFields['anneeAcquisitionNationalite']	    = '';

if($qp152PE11.etatCivilPage.etatCivilGroup.dateAcquisitionNationaliteFr != null) {
	var dateTemp = new Date(parseInt($qp152PE11.etatCivilPage.etatCivilGroup.dateAcquisitionNationaliteFr.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourAcquisitionNationalite'] 	    = day;
	formFields['moisAcquisitionNationalite']	    = month;
	formFields['anneeAcquisitionNationalite']	    = year;
}

/************************************************************************
 * Les personnes née hors de France
 ************************************************************************/

formFields['nomPereEtranger']                      = cheminEtatCivil.etrangerGroup.nomPereEtranger;
formFields['prenomPereEtranger']                   = cheminEtatCivil.etrangerGroup.prenomPereEtranger;
formFields['nomMereEtranger']                      = cheminEtatCivil.etrangerGroup.nomMereEtranger;
formFields['prenomMereEtranger']                   = cheminEtatCivil.etrangerGroup.prenomMereEtranger;


/************************************************************************
 * Coordonnées déclarant
 ************************************************************************/
var cheminCoordonnees	= $qp152PE11.coordonnesPage.coordonnesGroup;

formFields['numAppart']                             = cheminCoordonnees.numAppartBP;
formFields['entreeImmeuble']                        = cheminCoordonnees.entreeBat;
formFields['numLibelleVoie']                        = cheminCoordonnees.adresseNumeroLibellee;
formFields['bpLieuDit']                             = cheminCoordonnees.bpLieuDit;
formFields['codePostal']                            = cheminCoordonnees.codePostal;
formFields['villeCommune']                          = cheminCoordonnees.ville;
formFields['pays']                                  = cheminCoordonnees.pays;
formFields['phone']                                 = cheminCoordonnees.numPhone;
formFields['numFixe']                               = cheminCoordonnees.numFixe;
formFields['courriel']                              = cheminCoordonnees.courriel;
formFields['numAdeli']                              = cheminCoordonnees.numAdeli;


/************************************************************************
 * Diplôme d'infirmier
 ************************************************************************/
var cheminDiplomes		= $qp152PE11.diplomePage.diplomeGroup;

formFields['intituleDiplome']                       = cheminDiplomes.intituleDiplome;
if($qp152PE11.diplomePage.diplomeGroup.dateObtentionDiplome != null) {
	var dateTemp = new Date(parseInt($qp152PE11.diplomePage.diplomeGroup.dateObtentionDiplome.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourObtentionDiplome'] 	= day;
	formFields['moisObtentionDiplome'] 	= month;
	formFields['anneeObtentionDiplome']	= year;
}
formFields['numDiplome']                		    = cheminDiplomes.numDiplome;
formFields['lieuObtentionDiplome']                  = cheminDiplomes.lieuObtentionDiplome;
formFields['paysDelivranceDiplome']                 = cheminDiplomes.paysDelivranceDiplome;

//Autorisation si le diplôme n'est pas équivalent au DE français
var cheminAutorisation	= $qp152PE11.diplomePage.diplomeGroup.autorisationGroup;

formFields['jourObtententionAutorisation'] 	    = '';
formFields['moisObtentionAutorisation']	        = '';
formFields['anneeObtentionAutorisation']	    = '';

if($qp152PE11.diplomePage.diplomeGroup.autorisationGroup.dateDelivranceAutorisation != null) {
	var dateTemp = new Date(parseInt($qp152PE11.diplomePage.diplomeGroup.autorisationGroup.dateDelivranceAutorisation.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourObtententionAutorisation'] 	    = day;
	formFields['moisObtentionAutorisation']	        = month;
	formFields['anneeObtentionAutorisation']	    = year;
}
formFields['prefectureAutorisation']                = cheminAutorisation.prefectureAutorisation;

//Autres diplômes

var cheminAutreDiplome = $qp152PE11.diplomePage.diplomeGroup;

formFields['autreDipome0'] 	 = cheminAutreDiplome.autreDiplomeGroup.intituleAutreDiplome;
formFields['deAutreDiplome0']                = Value('id').of(cheminAutreDiplome.autreDiplomeGroup.typeDiplome).eq("deAutreDiplome") ? true : false;
formFields['specialiteAutreDiplome0']        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup.typeDiplome).eq("specialiteAutreDiplome") ? true : false;
formFields['competanceAutreDiplome0']        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup.typeDiplome).eq("competenceAutreDiplome") ? true : false;
formFields['capaciteAutreDiplome0']          = Value('id').of(cheminAutreDiplome.autreDiplomeGroup.typeDiplome).eq("capacitAutreDiplome") ? true : false;

formFields['jourObtentionAutreDiplome0'] 	= '';
formFields['moisObtentionAutreDiplome0']	= '';
formFields['anneeObtentionAutreDiplome0']	= '';

if(cheminAutreDiplome.autreDiplomeGroup.dateObtentionAutreDiplome != null) {
	var dateTemp = new Date(parseInt(cheminAutreDiplome.autreDiplomeGroup.dateObtentionAutreDiplome.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourObtentionAutreDiplome0'] 	= day;
	formFields['moisObtentionAutreDiplome0']	= month;
	formFields['anneeObtentionAutreDiplome0']	= year;
}
formFields['organismeFormateur0'] 			= cheminAutreDiplome.autreDiplomeGroup.organismeFormateur != null ? cheminAutreDiplome.autreDiplomeGroup.organismeFormateur : '';
formFields['codePostalAutreDiplome0'] 		= cheminAutreDiplome.autreDiplomeGroup.codePostalAutreDiplome != null ? cheminAutreDiplome.autreDiplomeGroup.codePostalAutreDiplome : '';
formFields['villeAutreDiplome0'] 			= cheminAutreDiplome.autreDiplomeGroup.villeAutreDiplome != null ? cheminAutreDiplome.autreDiplomeGroup.villeAutreDiplome : '';
if(cheminAutreDiplome.autreDiplomeGroup.dateAbandonExercice0 != null) {
	var dateTemp = new Date(parseInt(cheminAutreDiplome.autreDiplomeGroup.dateAbandonExercice0.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourAbandonExercice0'] 	= day;
	formFields['moisAbandonExercice0']		= month;
	formFields['anneeAbandonExercice0']	= year;
}


formFields['autreDipome1']   = cheminAutreDiplome.autreDiplomeGroup1.intituleAutreDiplome1 != null ? cheminAutreDiplome.autreDiplomeGroup1.intituleAutreDiplome1 : '';
formFields['deautreDiplome1']                = Value('id').of(cheminAutreDiplome.autreDiplomeGroup1.typeDiplome).eq("deAutreDiplome") ? true : false;
formFields['specialiteAutreDiplome1']        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup1.typeDiplome).eq("specialiteAutreDiplome") ? true : false;
formFields['competanceAutreDiplome1']        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup1.typeDiplome).eq("competenceAutreDiplome") ? true : false;
formFields['capaciteAutreDiplome1']          = Value('id').of(cheminAutreDiplome.autreDiplomeGroup1.typeDiplome).eq("capacitAutreDiplome") ? true : false;

formFields['jourObtentionAutreDiplome1'] 	= '';
formFields['moisObtentionAutreDiplome1']	= '';
formFields['anneeObtentionAutreDiplome1']	= '';

if(cheminAutreDiplome.autreDiplomeGroup1.dateObtentionAutreDiplome1 != null) {
	var dateTemp = new Date(parseInt(cheminAutreDiplome.autreDiplomeGroup1.dateObtentionAutreDiplome1.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourObtentionAutreDiplome1'] 	= day;
	formFields['moisObtentionAutreDiplome1']	= month;
	formFields['anneeObtentionAutreDiplome1']	= year;
}
formFields['organismeFormateur1'] 			= cheminAutreDiplome.autreDiplomeGroup1.organismeFormateur1 != null ? cheminAutreDiplome.autreDiplomeGroup1.organismeFormateur1 : ''
formFields['codePostalAutreDiplome1'] 		= cheminAutreDiplome.autreDiplomeGroup1.codePostalAutreDiplome1 != null ? cheminAutreDiplome.autreDiplomeGroup1.codePostalAutreDiplome1 : ''
formFields['villeAutreDiplome1'] 			= cheminAutreDiplome.autreDiplomeGroup1.villeAutreDiplome1 != null ? cheminAutreDiplome.autreDiplomeGroup1.villeAutreDiplome1 : ''

formFields['jourAbandonExercice1'] 	        = '';
formFields['moisAbandonExercice1']			= '';
formFields['anneeAbondantExercice1']		= '';

	if(cheminAutreDiplome.autreDiplomeGroup1.dateAbandonExercice1 != null) {
	var dateTemp = new Date(parseInt(cheminAutreDiplome.autreDiplomeGroup1.dateAbandonExercice1.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourAbandonExercice1'] 	= day;
	formFields['moisAbandonExercice1']		= month;
	formFields['anneeAbandonExercice1']	= year;
}



// Langues étrangères

var langueEtrangere = 3;

for (var i = 0 ; i<langueEtrangere; i++){
	formFields['langueEtrangere'+i]                 = '';
}

for (var i = 0 ; i< $qp152PE11.diplomePage.languesEtrangeres.size(); i++ ){
	formFields['langueEtrangere'+i]                	= $qp152PE11.diplomePage.languesEtrangeres[i].langueEtrangere != null ? $qp152PE11.diplomePage.languesEtrangeres[i].langueEtrangere : '';
}

/************************************************************************
 * Mode d'exercice libéral
 ************************************************************************/
var cheminExerciceLiberal = $qp152PE11.modeExerciceLiberalPage.modeExerciceLiberalGroup;


formFields['liberalNonConventionne']               = Value('id').of(cheminExerciceLiberal.modeExercice).eq('liberalNonConventionne') ? true : false;
formFields['liberalConventionne']                  = Value('id').of(cheminExerciceLiberal.modeExercice).eq('liberalConventionne') ? true : false;

//Exercice individuel
formFields['liberalTitulaire']                     = Value('id').of(cheminExerciceLiberal.exerciceIndividuel).eq('liberalTitulaire') ? true : false;
formFields['libreralCollaborateur']                = Value('id').of(cheminExerciceLiberal.exerciceIndividuel).eq('libreralCollaborateur') ? true : false;

//Exercice en groupe
formFields['libralSCM']                            = Value('id').of(cheminExerciceLiberal.exerciceEnGroup).eq('libralSCM') ? true : false;
formFields['liberalAssociation']                   = Value('id').of(cheminExerciceLiberal.exerciceEnGroup).eq('liberalAssociation') ? true : false;

if(cheminExerciceLiberal.dateDeburActiviteLiberal != null) {
	var dateTemp = new Date(parseInt(cheminExerciceLiberal.dateDeburActiviteLiberal.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDeburActiviteLiberal'] 	    	= day;
	formFields['moisDeburActiviteLiberal']	        = month;
	formFields['anneeDeburActiviteLiberal']	    	= year;
}
//Adresse cabinet 

formFields['numAppartCabinet']                     = cheminExerciceLiberal.coordonneeCabinet.numAppartCabinet;
formFields['entreeImmeubleCabinet']                = cheminExerciceLiberal.coordonneeCabinet.entreeImmeubleCabinet;
formFields['numLibelleVoieCabinet']                = cheminExerciceLiberal.coordonneeCabinet.numLibelleVoieCabinet;
formFields['bpLieuDitCabinet']                     = cheminExerciceLiberal.coordonneeCabinet.bpLieuDitCabinet;
formFields['codePostalCabinet']                    = cheminExerciceLiberal.coordonneeCabinet.codePostalCabinet;
formFields['villeCabinet']                         = cheminExerciceLiberal.coordonneeCabinet.villeCabinet;
formFields['numTelPro']                            = cheminExerciceLiberal.coordonneeCabinet.numTelPro;
formFields['numPhoneCabinet']                      = cheminExerciceLiberal.coordonneeCabinet.numPhoneCabinet;
formFields['courrielPro']                          = cheminExerciceLiberal.coordonneeCabinet.numPhoneCabinet;
formFields['cabinetSecondaireOUI']                 = cheminExerciceLiberal.coordonneeCabinet.cabinetSecondaireOuiNon ? true : false;
formFields['cabinetSecondaireNON']                 = cheminExerciceLiberal.coordonneeCabinet.cabinetSecondaireOuiNon ? false : true;

formFields['numAppartCabinetSecondaire']           = cheminExerciceLiberal.coordoneesCabinetSecondaire.numAppartCabinetSecondaire;
formFields['entreeImmeubleCabinetSecondaire']      = cheminExerciceLiberal.coordoneesCabinetSecondaire.entreeImmeubleCabinetSecondaire;
formFields['numLibelleVoieCabinetSecondaire']      = cheminExerciceLiberal.coordoneesCabinetSecondaire.numLibelleVoieCabinetSecondaire;
formFields['bpLieuDitCabinetSecondaire']           = cheminExerciceLiberal.coordoneesCabinetSecondaire.bpLieuDitCabinetSecondaire;
formFields['codePostalCabinetSecondaire']          = cheminExerciceLiberal.coordoneesCabinetSecondaire.codePostalCabinetSecondaire;
formFields['villeCabinetSecondaire']               = cheminExerciceLiberal.coordoneesCabinetSecondaire.villeCabinetSecondaire;
formFields['numTelProSecondaire']                  = cheminExerciceLiberal.coordoneesCabinetSecondaire.numTelProSecondaire;


/************************************************************************
 * Secteur libéral avec le statut de remplaçant (e)
 ************************************************************************/
var chaminSecteurLiberalRemplacant = $qp152PE11.modeExerciceLiberalPage.secteurExerciceGroup.liberalRemplacantGroup;
formFields['remplacantPermannatOUI']               = chaminSecteurLiberalRemplacant.remplacantPermannatOuiNon ? true : false;
formFields['remplacantPermannatNon']               = chaminSecteurLiberalRemplacant.remplacantPermannatOuiNon ? false : true;

if(chaminSecteurLiberalRemplacant.dateDelivranceAutorisation != null) {
	var dateTemp = new Date(parseInt(chaminSecteurLiberalRemplacant.dateDelivranceAutorisation.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDelivranceAutorisation'] 	    = day;
	formFields['moisDelivranceAutorisation']	    = month;
	formFields['anneeDelivranceAutorisation']	    = year;
}
formFields['nomInfirmierRemplace']                 = chaminSecteurLiberalRemplacant.nomInfirmierRemplace;
formFields['prenomInfermierRemplace']              = chaminSecteurLiberalRemplacant.prenomInfermierRemplace;
formFields['numOrdinal']                           = chaminSecteurLiberalRemplacant.numOrdinal;
formFields['numAppartCabinetRemplacement']         = chaminSecteurLiberalRemplacant.numAppartCabinetRemplacement;
formFields['numLibelleAppartCabinetRemplacement']  = chaminSecteurLiberalRemplacant.numLibelleAppartCabinetRemplacement;
formFields['bpLieuDitCabinetRemplacement']         = chaminSecteurLiberalRemplacant.bpLieuDitCabinetRemplacement;
formFields['codePostalCabinetRemplacement']        = chaminSecteurLiberalRemplacant.codePostalCabinetRemplacement;
formFields['villeCabinetRemplacement']             = chaminSecteurLiberalRemplacant.villeCabinetRemplacement;

if (chaminSecteurLiberalRemplacant.remplacementAutreCabinetOuiNon){
	formFields['nomInfirmierRemplaceAutre']                 = chaminSecteurLiberalRemplacant.autreCabinetGroup.nomInfirmierRemplaceAutre;
	formFields['prenomInfermierRemplaceAutre']              = chaminSecteurLiberalRemplacant.autreCabinetGroup.prenomInfermierRemplaceAutre;
	formFields['numOrdinalAutre']                           = chaminSecteurLiberalRemplacant.autreCabinetGroup.numOrdinalAutre;
	formFields['numAppartCabinetRemplacementAutre']         = chaminSecteurLiberalRemplacant.autreCabinetGroup.numAppartCabinetRemplacementAutre;
	formFields['numLibelleAppartCabinetRemplacementAutre']  = chaminSecteurLiberalRemplacant.autreCabinetGroup.numLibelleAppartCabinetRemplacementAutre;
	formFields['bpLieuDitCabinetRemplacementAutre']         = chaminSecteurLiberalRemplacant.autreCabinetGroup.bpLieuDitCabinetRemplacementAutre;
	formFields['codePostalCabinetRemplacementAutre']        = chaminSecteurLiberalRemplacant.autreCabinetGroup.codePostalCabinetRemplacementAutre;
	formFields['villeCabinetRemplacementAutre']             = chaminSecteurLiberalRemplacant.autreCabinetGroup.villeCabinetRemplacementAutre;
}


/************************************************************************
 * Exercice en société
 ************************************************************************/
var cheminExerciceSociete = $qp152PE11.modeExerciceLiberalPage.secteurExerciceGroup.societeGroup;

formFields['societeSCP']                           = Value('id').of(cheminExerciceSociete.typeSociete).eq('societeSCP') ? true : false;
formFields['societeSelarlSelarlu']                 = Value('id').of(cheminExerciceSociete.typeSociete).eq('societeSelarlSelarlu') ? true : false;
formFields['societeSelas']                         = Value('id').of(cheminExerciceSociete.typeSociete).eq('societeSelas') ? true : false;
formFields['nomSocieteExercice']                   = cheminExerciceSociete.nomSocieteExercice;
formFields['numInscriptionSocieteOrdre']           = cheminExerciceSociete.numInscriptionSocieteOrdre;
formFields['numAppartSociete']                     = cheminExerciceSociete.coordonneesSociete.numAppartSociete;
formFields['entreeSociete']                        = cheminExerciceSociete.coordonneesSociete.entreeSociete;
formFields['numLibelleVoieSociete']                = cheminExerciceSociete.coordonneesSociete.numLibelleVoieSociete;
formFields['bpLieuDitSociete']                     = cheminExerciceSociete.coordonneesSociete.bpLieuDitSociete;
formFields['codePostalSociete']                    = cheminExerciceSociete.coordonneesSociete.codePostalSociete;
formFields['villeSociete']                         = cheminExerciceSociete.coordonneesSociete.villeSociete;
formFields['numTelSociete']                        = cheminExerciceSociete.coordonneesSociete.numTelSociete;
formFields['numPortableSociete']                   = cheminExerciceSociete.coordonneesSociete.numPortableSociete;
formFields['siretSociete']                         = cheminExerciceSociete.coordonneesSociete.siretSociete;
formFields['nombreAssocieSociete']                 = cheminExerciceSociete.coordonneesSociete.nombreAssocieSociete;

formFields['declarationHonneurEcrit']				   = "Je déclare sur l’honneur qu’aucune instance pouvant donner lieu à condamnation ou sanction susceptible d’avoir des conséquences sur mon inscription au tableau de l’Ordre n’est en cours à mon encontre.";
formFields['codeDeontologieEcrit']					   = "J’affirme avoir pris connaissance du code de déontologie des infirmiers et je fais serment de le respecter.";


/************************************************************************
 * Signature
 ************************************************************************/
var cheminSignature = $qp152PE11.signaturePage.signatureGroup;

formFields['numDeptExercicePrincipal']			   = cheminSignature.deptExercicePrincipal;
formFields['declarationHonneur']                   = cheminSignature.declarationHonneur;
formFields['codeDeontologie']                      = cheminSignature.codeDeontologie;
formFields['annuaireInfermier']                    = cheminSignature.annuaireInfermier;
formFields['lieuSignature']                        = cheminSignature.lieuSignature;

if(cheminSignature.dateSignature != null) {
	var dateTemp = new Date(parseInt(cheminSignature.dateSignature.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourSignature'] 		= day;
	formFields['moisSignature'] 		= month;
	formFields['anneeSignature']		= year;
}
formFields['signature']				            = $qp152PE11.etatCivilPage.etatCivilGroup.civilite +" "+ $qp152PE11.etatCivilPage.etatCivilGroup.nomNaissance +" "+ $qp152PE11.etatCivilPage.etatCivilGroup.prenoms;

/*******************************************************************************
 * Chargement du courrier d'accompagnement à destination de l'AC
 ******************************************************************************/

var cerfaDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
.apply({
	dateSignature : $qp152PE11.signaturePage.signatureGroup.dateSignature ,
	autoriteHabilitee :"Conseil national de l’ordre des infirmiers" ,
	demandeContexte : "Inscription à l’ordre régime général",
	civiliteNomPrenom : $qp152PE11.etatCivilPage.etatCivilGroup.civilite +" "+ $qp152PE11.etatCivilPage.etatCivilGroup.nomNaissance +" "+ $qp152PE11.etatCivilPage.etatCivilGroup.prenoms
});


var finalDoc = nash.doc //
.load('models/Formulaire inscription ordre liberal.pdf') //
.apply (formFields);


cerfaDoc.append(finalDoc.save('cerfa.pdf'));	

/*******************************************************************************
 * Pieces jointes
 ******************************************************************************/

function appendPj(fld) {
    fld.forEach(function (elm) {
    	cerfaDoc.append(elm);
    });
}

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationHonneur);
appendPj($attachmentPreprocess.attachmentPreprocess.pjMaitriseFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatRadiation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssuranceCivile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjToutContratLiberal);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorisationRemplacement);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);

/*******************************************************************************
 *  Enregistrement du fichier (en mémoire)
 ******************************************************************************/

var finalDocItem = cerfaDoc.save('Infirmier_Anesthesiste.pdf');	

/*******************************************************************************
 *  Persistance des données obtenues
 ******************************************************************************/

return spec.create({
id : 'review',
label : 'Infirmier anesthésiste  - Inscription à l’ordre régime général',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : "Demande d'inscription à l'ordre national pour la profession d'infirmier en vue d'un libre établissement en exercice libéral grâce au régime général.",
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});