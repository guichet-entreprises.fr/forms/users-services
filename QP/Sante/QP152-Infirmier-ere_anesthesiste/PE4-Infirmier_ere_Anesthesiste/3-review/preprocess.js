var cerfaFields = {};

var civNomPrenom = $qp152PE4.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp152PE4.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp152PE4.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['audioprothesiste']                          = false;
cerfaFields['conseillerGenetique']                       = false;
cerfaFields['preparateurPharmacie']                      = false;
cerfaFields['preparateurPharmacieHospitaliere']          = false;
cerfaFields['infirmier']                       			 = false;
cerfaFields['infirmierSpecialise']                       = true;
cerfaFields['infirmierSpecialiseAnesthesiste']           = true;
cerfaFields['infirmierSpecialiseBlocOperatoire']         = false;
cerfaFields['infirmierSpecialisePuericultrice']          = false;
cerfaFields['pedicurePodologue']                       	 = false;
cerfaFields['ergotherapeuthe']                       	 = false;
cerfaFields['psychomotricien']                       	 = false;
cerfaFields['orthophoniste']                       	     = false;
cerfaFields['orthoptiste']                       		 = false;
cerfaFields['manipulateurElectroradiologie']             = false;
cerfaFields['opticienLunetier']                          = false;
cerfaFields['dieteticien']                       		 = false;

cerfaFields['declarationInitiale']                       = true;
cerfaFields['renouvellement']                       	 = false;



//Identité du demandeur

cerfaFields['declarantNom']                             = $qp152PE4.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp152PE4.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp152PE4.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp152PE4.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp152PE4.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp152PE4.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp152PE4.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp152PE4.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp152PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp152PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') 
														+ ' ' + ($qp152PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp152PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp152PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp152PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' 
														+ $qp152PE4.coordonnees.coordonneesDeclarant.declarantAdresseVille + ' ' + $qp152PE4.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp152PE4.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp152PE4.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance : '') + ' ' 
														+ ($qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal: '') 
														+ ' ' + ($qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp152PE4.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionExerceeEM']     					= $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionExerceeEM;
cerfaFields['professionExerceeSpecialiteEM']  			= $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM;
cerfaFields['professionConcernee']                      = $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee;
cerfaFields['professionConcerneeSpecialite']            = $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance;
cerfaFields['typesActesEnvisages']                      = $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;
cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceComplementAdresse: '');
cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceVille != null ? $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExercicePays != null ? $qp152PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseNumeroLibellee != null ? $qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseNumeroLibellee : '') 
														+ ' ' + ($qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseComplementAdresse != null ? $qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseCodePostal != null ? $qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseCodePostal :'') 
														+' '+ ($qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseVille != null ? $qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseVille : '') 
														+ ', ' + ($qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdressePays != null ? $qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp152PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp152PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp152PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp152PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale;


// Observations et Signature

cerfaFields['autresObservations']                       = $qp152PE4.observations.observationsSignature.autresObservations;
cerfaFields['signatureCoche']                           = $qp152PE4.observations.observationsSignature.signatureCoche;
cerfaFields['signatureDate']                            = $qp152PE4.observations.observationsSignature.dateSignature;
cerfaFields['signature']                                ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

//informations à fournir en cas de renouvellement 
cerfaFields['datePrestationDebut1']                     = '';
cerfaFields['datePrestationFin1']                       = '';
cerfaFields['datePrestationDebut2']                     = '';
cerfaFields['datePrestationFin2']                       = '';
cerfaFields['datePrestationDebut3']                     = '';
cerfaFields['datePrestationFin3']                       = '';
cerfaFields['datePrestationDebut4']                     = '';
cerfaFields['datePrestationFin4']                       = '';
cerfaFields['datePrestationDebut5']                     = '';
cerfaFields['datePrestationFin5']                       = '';

cerfaFields['commentairesLPSRenouvellement']            = '';
cerfaFields['activiteProfessionnelsLPSRevouvellement']  = '';


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp152PE4.observations.observationsSignature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp152PE4.observations.observationsSignature.dateSignature,
		autoriteHabilitee :"Conseil national de l’ordre des infirmiers",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Formulaire_LPS.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));
function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Infirmier_anesthesiste_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Infirmier anesthésiste - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'infirmier anesthésiste.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});