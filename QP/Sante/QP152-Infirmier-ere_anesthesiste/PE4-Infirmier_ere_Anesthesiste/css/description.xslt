<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:n="http://www.ge.fr/schema/1.2/form-manager" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="common.xslt" />

    <xsl:template match="n:reference-id">
        <div class="row">
            <div class="col-md-12 text-center text-muted">
                <xsl:value-of select="." />
                <xsl:text> #</xsl:text>
                <xsl:value-of select="@revision" />
            </div>
        </div>
    </xsl:template>

    <xsl:template match="n:title">
        <h1 class="text-center">
            <xsl:value-of select="." />
        </h1>
        <xsl:apply-templates select="../n:reference-id" />
    </xsl:template>

    <xsl:template match="/n:description">
        <div class="container">
            <xsl:apply-templates select="n:title" />
            <xsl:apply-templates select="n:translations" />
            <xsl:apply-templates select="n:referentials" />
            <xsl:apply-templates select="n:steps" />
        </div>
    </xsl:template>

    <xsl:template match="n:referentials">
        <xsl:call-template name="buildSection">
            <xsl:with-param name="label" select="'Referentials'" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="n:referentials/n:referential">
        <xsl:call-template name="buildAttribute">
            <xsl:with-param name="label" select="@id" />
            <xsl:with-param name="value">
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="text()" />
                    </xsl:attribute>
                    <xsl:value-of select="text()" />
                </a>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="n:translations">
        <xsl:call-template name="buildSection">
            <xsl:with-param name="label" select="'Translations'" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="n:translations/n:for">
        <xsl:call-template name="buildAttribute">
            <xsl:with-param name="label" select="@lang" />
            <xsl:with-param name="value">
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="text()" />
                    </xsl:attribute>
                    <xsl:value-of select="text()" />
                </a>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="n:steps">
        <xsl:call-template name="buildSection">
            <xsl:with-param name="label" select="'Steps'" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="n:steps/n:step">
        <xsl:variable name="user">
            <xsl:choose>
                <xsl:when test="@user">
                    <xsl:value-of select="@user" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>user</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="status">
            <xsl:choose>
                <xsl:when test="@status">
                    <xsl:value-of select="@status" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>todo</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ico">
            <xsl:choose>
                <xsl:when test="@icon">
                    <xsl:value-of select="@icon" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>question</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <div class="panel panel-default">
            <xsl:choose>
                <xsl:when test="$status = 'done'">
                    <xsl:attribute name="class"><xsl:text>panel panel-success</xsl:text></xsl:attribute>
                </xsl:when>
                <xsl:when test="$status = 'in_progress'">
                    <xsl:attribute name="class"><xsl:text>panel panel-warning</xsl:text></xsl:attribute>
                </xsl:when>
                <xsl:when test="$status = 'error'">
                    <xsl:attribute name="class"><xsl:text>panel panel-danger</xsl:text></xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class"><xsl:text>panel panel-info</xsl:text></xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1">
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="@id" />
                    </div>
                    <div class="col-md-10">
                       <xsl:value-of select="@label" />
                    </div>
                    <div class="col-md-1">
                        <i>
                            <xsl:attribute name="class">
                                <xsl:text>fa-</xsl:text>
                                <xsl:value-of select="$ico" />
                            </xsl:attribute>
                        </i>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <xsl:apply-templates select="@data" />
                <xsl:apply-templates select="@preprocess" />
                <xsl:apply-templates select="@postprocess" />
                <xsl:call-template name="buildAttribute">
                    <xsl:with-param name="label" select="'Status'" />
                    <xsl:with-param name="value" select="$status" />
                </xsl:call-template>
                <xsl:call-template name="buildAttribute">
                    <xsl:with-param name="label" select="'Intervenant'" />
                    <xsl:with-param name="value" select="$user" />
                </xsl:call-template>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="n:step/@data | n:step/@preprocess | n:step/@postprocess">
        <xsl:call-template name="buildAttribute">
            <xsl:with-param name="label" select="concat(translate(substring(name(), 1, 1), $lowerCase, $upperCase), substring(name(), 2))" />
            <xsl:with-param name="value">
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="." />
                    </xsl:attribute>
                    <xsl:value-of select="." />
                </a>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

</xsl:stylesheet>
