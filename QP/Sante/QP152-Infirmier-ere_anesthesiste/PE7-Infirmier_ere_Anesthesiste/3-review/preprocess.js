function pad(s) { return (s < 10) ? '0' + s : s; }
var formFields = {};

/************************************************************************
 * Etat civil
 ************************************************************************/
 

var cheminEtatCivil	= $qp152PE7.etatCivilPage.etatCivilGroup;

formFields['madame']                  = (cheminEtatCivil.civilite=='Madame');
formFields['monsieur']                = (cheminEtatCivil.civilite=='Monsieur');
formFields['nomNaissance']                         = cheminEtatCivil.nomNaissance;
formFields['nomUsage']                             = cheminEtatCivil.nomUsage;
formFields['prenoms']                              = cheminEtatCivil.prenoms;
formFields['nomPrenomExercice']                    = cheminEtatCivil.nomPrenomExercice;
if(cheminEtatCivil.dateNaissance != null) {
	var dateTemp = new Date(parseInt(cheminEtatCivil.dateNaissance.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	    var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourNaissance'] 	= day;
	formFields['moisNaissance'] 	= month;
	formFields['anneeNaissance']	= year;
}
formFields['lieuNaissance']                         = cheminEtatCivil.lieuNaissance;
formFields['numDeptNaissance']                      = cheminEtatCivil.deptNaissance;
formFields['paysNaissance']                         = cheminEtatCivil.paysNaissance;
formFields['nationalite']                           = cheminEtatCivil.nationalite;

formFields['jourAcquisitionNationalite'] 	    = '';
formFields['moisAcquisitionNationalite']	    = '';
formFields['anneeAcquisitionNationalite']	    = '';
	
if(cheminEtatCivil.dateAcquisitionNationaliteFr != null) {
	var dateTemp = new Date(parseInt(cheminEtatCivil.dateAcquisitionNationaliteFr.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
		var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourAcquisitionNationalite'] 	    = day;
	formFields['moisAcquisitionNationalite']	    = month;
	formFields['anneeAcquisitionNationalite']	    = year;
}


/************************************************************************
 * Les personnes née hors de France
 ************************************************************************/
var cheminEtranger = cheminEtatCivil.etrangerGroup;

formFields['nomPereEtranger']                      = cheminEtranger.nomPereEtranger;
formFields['prenomPereEtranger']                   = cheminEtranger.prenomPereEtranger;
formFields['nomMereEtranger']                      = cheminEtranger.nomMereEtranger;
formFields['prenomMereEtranger']                   = cheminEtranger.prenomMereEtranger;


/************************************************************************
 * Coordonnées déclarant
 ************************************************************************/
var cheminCoordonnees	= $qp152PE7.coordonnesPage.coordonnesGroup;

formFields['numAppart']                             = cheminCoordonnees.numAppartBP;
formFields['entreeImmeuble']                        = cheminCoordonnees.entreeBat;
formFields['numLibelleVoie']                        = cheminCoordonnees.adresseNumeroLibellee;
formFields['bpLieuDit']                             = cheminCoordonnees.bpLieuDit;
formFields['codePostal']                            = cheminCoordonnees.codePostal;
formFields['villeCommune']                          = cheminCoordonnees.ville;
formFields['pays']                                  = cheminCoordonnees.pays;
formFields['phone']                                 = cheminCoordonnees.numPhone;
formFields['numFixe']                               = cheminCoordonnees.numFixe;
formFields['courriel']                              = cheminCoordonnees.courriel;
formFields['numAdeli']                              = cheminCoordonnees.numAdeli;


/************************************************************************
 * Diplôme d'infirmier
 ************************************************************************/
var cheminDiplomes		= $qp152PE7.diplomePage.diplomeGroup;

formFields['intituleDiplome']                       = cheminDiplomes.intituleDiplome;
if(cheminDiplomes.dateObtentionDiplome != null) {
	var dateTemp = new Date(parseInt(cheminDiplomes.dateObtentionDiplome.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
		var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourObtentionDiplome'] 	= day;
	formFields['moisObtentionDiplome'] 	= month;
	formFields['anneeObtentionDiplome']	= year;
}
formFields['numDiplome']                		    = cheminDiplomes.numDiplome;
formFields['lieuObtentionDiplome']                  = cheminDiplomes.lieuObtentionDiplome;
formFields['paysDelivranceDiplome']                 = cheminDiplomes.paysDelivranceDiplome;

//Autorisation si le diplôme n'est pas équivalent au DE français
var cheminAutorisation	= $qp152PE7.diplomePage.autorisationExercice;

if(cheminAutorisation.dateDelivranceAutorisation != null) {
	var dateTemp = new Date(parseInt(cheminAutorisation.dateDelivranceAutorisation.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
		var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourObtententionAutorisation'] 	    = day;
	formFields['moisObtentionAutorisation']	        = month;
	formFields['anneeObtentionAutorisation']	    = year;
}
formFields['prefectureAutorisation']                = cheminAutorisation.prefectureAutorisation;

//Autres diplômes

var cheminAutreDiplome = $qp152PE7.diplomePage;
/*	var r = cheminAutreDiplome.autreDiplomeGroup.size();
	for (var i = 0 ; i < r; i++){
		formFields['intituleAutreDiplome' + i] 	 = cheminAutreDiplome.autreDiplomeGroup[i].intituleAutreDiplome;
		
		formFields['deAutre' + i]                = Value('id').of(cheminAutreDiplome.autreDiplomeGroup[i].typeDiplome).eq('deAutreDiplome') ? true : false;
		formFields['specialiteAutre' + i]        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup[i].typeDiplome).eq('specialiteAutreDiplome') ? true : false;
		formFields['competanceAutre' + i]        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup[i].typeDiplome).eq('competenceAutreDiplome') ? true : false;
		formFields['capaciteAutre' + i]          = Value('id').of(cheminAutreDiplome.autreDiplomeGroup[i].typeDiplome).eq('capacitAutreDiplome') ? true : false;
		
		
		if($qp152PE7.diplomePage.autreDiplomeGroup[i].dateObtentionAutreDiplome != null) {
			var dateTemp = new Date(parseInt($qp152PE7.diplomePage.autreDiplomeGroup[i].dateObtentionAutreDiplome.getTimeInMillis()));
				var monthTemp = dateTemp.getMonth() + 1;
				var month = pad(monthTemp.toString());
				var day =  pad(dateTemp.getDate().toString());
				var year = dateTemp.getFullYear();
			formFields['jourObtentionAutreDiplome'+ i] 	= day;
			formFields['moisObtentionAutreDiplome'+ i]	= month;
			formFields['anneeObtentionAutreDiplome'+ i]	= year;
		}
		formFields['organismeFormateur' + i] 			= cheminAutreDiplome.autreDiplomeGroup[i].organismeFormateur;
		formFields['codePostalAutreDiplome' + i] 		= cheminAutreDiplome.autreDiplomeGroup[i].codePostalAutreDiplome;
		formFields['villeAutreDiplome' + i] 			= cheminAutreDiplome.autreDiplomeGroup[i].villeAutreDiplome;
		
		if($qp152PE7.diplomePage.autreDiplomeGroup[i].dateAbandonExercice != null) {
			var dateTemp = new Date(parseInt($qp152PE7.diplomePage.autreDiplomeGroup[i].dateAbandonExercice.getTimeInMillis()));
				var monthTemp = dateTemp.getMonth() + 1;
				var month = pad(monthTemp.toString());
				var day =  pad(dateTemp.getDate().toString());
				var year = dateTemp.getFullYear();
			formFields['jourAbondantExercice'+ i] 	= day;
			formFields['moisAbondantExercice'+ i]	= month;
			formFields['anneeAbondantExercice'+ i]	= year;
		}
	}  */
 
formFields['autreDipome0']   				= cheminAutreDiplome.autreDiplomeGroup.intituleAutreDiplome != null ? cheminAutreDiplome.autreDiplomeGroup.intituleAutreDiplome : '';
formFields['deAutreDiplome0']                = Value('id').of(cheminAutreDiplome.autreDiplomeGroup.typeDiplome).eq('deAutreDiplome') ? true : false;
formFields['specialiteAutreDiplome0']        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup.typeDiplome).eq('specialiteAutreDiplome') ? true : false;
formFields['competanceAutreDiplome0']        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup.typeDiplome).eq('competenceAutreDiplome') ? true : false;
formFields['capaciteAutreDiplome0']          = Value('id').of(cheminAutreDiplome.autreDiplomeGroup.typeDiplome).eq('capacitAutreDiplome') ? true : false;

	formFields['jourObtentionAutreDiplome0'] 	= '';
	formFields['moisObtentionAutreDiplome0']	= '';
	formFields['anneeObtentionAutreDiplome0']	= '';
	
if(cheminAutreDiplome.autreDiplomeGroup.dateObtentionAutreDiplome != null) {
	var dateTemp = new Date(parseInt($qp152PE7.diplomePage.autreDiplomeGroup.dateObtentionAutreDiplome.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
		var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourObtentionAutreDiplome0'] 	= day;
	formFields['moisObtentionAutreDiplome0']	= month;
	formFields['anneeObtentionAutreDiplome0']	= year;
}
formFields['organismeFormateur0'] 			= cheminAutreDiplome.autreDiplomeGroup.organismeFormateur != null ? cheminAutreDiplome.autreDiplomeGroup.organismeFormateur : '';
formFields['codePostalAutreDiplome0'] 		= cheminAutreDiplome.autreDiplomeGroup.codePostalAutreDiplome != null ? cheminAutreDiplome.autreDiplomeGroup.codePostalAutreDiplome : '';
formFields['villeAutreDiplome0'] 			= cheminAutreDiplome.autreDiplomeGroup.villeAutreDiplome != null ? cheminAutreDiplome.autreDiplomeGroup.villeAutreDiplome : '';

	formFields['jourAbandonExercice0'] 	= '';
	formFields['moisAbandonExercice0']	= '';
	formFields['anneeAbandonExercice0']	= '';
	
if($qp152PE7.diplomePage.autreDiplomeGroup.dateAbandonExercice != null) {
	var dateTemp = new Date(parseInt($qp152PE7.diplomePage.autreDiplomeGroup.dateAbandonExercice.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
		var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourAbandonExercice0'] 	= day;
	formFields['moisAbandonExercice0']	= month;
	formFields['anneeAbandonExercice0']	= year;
}

formFields['autreDipome1']   = cheminAutreDiplome.autreDiplomeGroup1.intituleAutreDiplome1 != null ? cheminAutreDiplome.autreDiplomeGroup1.intituleAutreDiplome1 : '';
formFields['deautreDiplome1']                = Value('id').of(cheminAutreDiplome.autreDiplomeGroup1.typeDiplome1).eq('deAutreDiplome') ? true : false;
formFields['specialiteAutreDiplome1']        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup1.typeDiplome1).eq('specialiteAutreDiplome') ? true : false;
formFields['competanceAutreDiplome1']        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup1.typeDiplome1).eq('competenceAutreDiplome') ? true : false;
formFields['capaciteAutreDiplome1']          = Value('id').of(cheminAutreDiplome.autreDiplomeGroup1.typeDiplome1).eq('capacitAutreDiplome') ? true : false;


	formFields['jourObtentionAutreDiplome1'] 	= '';
	formFields['moisObtentionAutreDiplome1']	= '';
	formFields['anneeObtentionAutreDiplome1']	= '';
	
if($qp152PE7.diplomePage.autreDiplomeGroup1.dateObtentionAutreDiplome1 != null) {
	var dateTemp = new Date(parseInt($qp152PE7.diplomePage.autreDiplomeGroup1.dateObtentionAutreDiplome1.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
		var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourObtentionAutreDiplome1'] 	= day;
	formFields['moisObtentionAutreDiplome1']	= month;
	formFields['anneeObtentionAutreDiplome1']	= year;
}
formFields['organismeFormateur1'] 			= cheminAutreDiplome.autreDiplomeGroup1.organismeFormateur1 != null ? cheminAutreDiplome.autreDiplomeGroup1.organismeFormateur1 : ''
formFields['codePostalAutreDiplome1'] 		= cheminAutreDiplome.autreDiplomeGroup1.codePostalAutreDiplome1 != null ? cheminAutreDiplome.autreDiplomeGroup1.codePostalAutreDiplome1 : ''
formFields['villeAutreDiplome1'] 			= cheminAutreDiplome.autreDiplomeGroup1.villeAutreDiplome1 != null ? cheminAutreDiplome.autreDiplomeGroup1.villeAutreDiplome1 : ''

	formFields['jourAbandonExercice1'] 	= '';
	formFields['moisAbandonExercice1']	= '';
	formFields['anneeAbandonExercice1']	= '';
	
if($qp152PE7.diplomePage.autreDiplomeGroup1.dateAbandonExercice1 != null) {
	var dateTemp = new Date(parseInt($qp152PE7.diplomePage.autreDiplomeGroup.dateAbandonExercice1.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
		var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourAbandonExercice1'] 	= day;
	formFields['moisAbandonExercice1']		= month;
	formFields['anneeAbandonExercice1']	= year;
} 

// Langues étrangères

var langueEtrangere = 3;

for (var i = 0 ; i<langueEtrangere; i++){
	formFields['langueEtrangere'+i]                 = '';
}

for (var i = 0 ; i< $qp152PE7.diplomePage.languesEtrangeres.size(); i++ ){
	formFields['langueEtrangere'+i]                	= $qp152PE7.diplomePage.languesEtrangeres[i].langueEtrangere;
}


/************************************************************************
 * Exercice professionnel
 ************************************************************************/
var cheminExercicePro	= $qp152PE7.exerciceProPage.exerciceGroup;

formFields['tempsPleineOUI']           			   = cheminExercicePro.exerciceTempsPartielOuiNon ? false : true;
formFields['tempsPleinNON']           			   = cheminExercicePro.exerciceTempsPartielOuiNon ? true: false;

formFields['pourcentageTempsPartiel']              = cheminExercicePro.pourcentageTempsPartiel;

if(cheminExercicePro.dateDerniereEmbauche != null) {
	var dateTemp = new Date(parseInt(cheminExercicePro.dateDerniereEmbauche.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDerniereEmbauche'] 	    		= day;
	formFields['moisDerniereEmbauche']	        	= month;
	formFields['anneeDerniereEmbauche']	    		= year;
}
formFields['numFiness']                            = '';
formFields['numSiret']							   = '';

formFields['fonctionExercicePro']                  = cheminExercicePro.fonctionExercicePro;
formFields['raisonSocialeEmployeur']               = cheminExercicePro.raisonSocialeEmployeur;
formFields['numFiness']                            = cheminExercicePro.numFiness;
formFields['numSiret']                             = cheminExercicePro.numSiret;

//Adresse de l’activité
var cheminAdresseActivite	= $qp152PE7.exerciceProPage.exerciceGroup.adresseActivite;

formFields['nomEtablissementActivite']             = cheminAdresseActivite.nomEtablissementActivite;
formFields['nomServiceActivite']                   = cheminAdresseActivite.nomServiceActivite;
formFields['numLibelleVoieActivite']               = cheminAdresseActivite.numLibelleVoieActivite;
formFields['bpLieuDit']                            = cheminAdresseActivite.bpLieuDit;
formFields['codePostalActivite']                   = cheminAdresseActivite.codePostalActivite;
formFields['villeActivite']                        = cheminAdresseActivite.villeActivite;
formFields['numTelephoneActivite']                 = cheminAdresseActivite.numTelephoneActivite;
formFields['posteActivite']                        = cheminAdresseActivite.posteActivite;

//Secteur d'activité
var cheminStatut = $qp152PE7.exerciceProPage.exerciceGroup;

formFields['titulaire']                       		= Value('id').of(cheminStatut.statut).eq('titulaire') ? true : false;
formFields['stagiaire']                		  		= Value('id').of(cheminStatut.statut).eq('stagiaire') ? true : false;
formFields['contractuel']                	  		= Value('id').of(cheminStatut.statut).eq('contractuel') ? true : false;

var cheminSecteurPrive = $qp152PE7.exerciceProPage.exerciceGroup;

formFields['cdi']                	  	  			= Value('id').of(cheminSecteurPrive.secteurPrive).eq('cdi') ? true : false;
formFields['cdd']                	  	  			= Value('id').of(cheminSecteurPrive.secteurPrive).eq('cdd') ? true : false;
formFields['vacataire']                  			= Value('id').of(cheminSecteurPrive.secteurPrive).eq('vacataire') ? true : false;
formFields['interimaire']                			= Value('id').of(cheminSecteurPrive.secteurPrive).eq('interimaire') ? true : false;

var cheminAutreSituation = $qp152PE7.exerciceProPage.exerciceGroup;

formFields['benevoleUniquement']                	= Value('id').of(cheminAutreSituation.autreSituation).eq('benevoleUniquement') ? true : false;
formFields['reserveSanitaire']               		= Value('id').of(cheminAutreSituation.autreSituation).eq('reserveSanitaire') ? true : false;
formFields['retraiteSansActivite']            		= Value('id').of(cheminAutreSituation.autreSituation).eq('retraiteSansActivite') ? true : false;

/************************************************************************
 * Mode d'exercice libéral
 ************************************************************************/
var cheminExerciceLiberal = $qp152PE7.modeExerciceLiberalPage.modeExerciceLiberalGroup;


formFields['liberalNonConventionne']               = Value('id').of(cheminExerciceLiberal.modeExercice).eq('liberalNonConventionne') ? true : false;
formFields['liberalConventionne']                  = Value('id').of(cheminExerciceLiberal.modeExercice).eq('liberalConventionne') ? true : false;

//Exercice individuel
formFields['liberalTitulaire']                     = Value('id').of(cheminExerciceLiberal.exerciceIndividuel).eq('liberalTitulaire') ? true : false;
formFields['libreralCollaborateur']                = Value('id').of(cheminExerciceLiberal.exerciceIndividuel).eq('libreralCollaborateur') ? true : false;

//Exercice en groupe
formFields['libralSCM']                            = Value('id').of(cheminExerciceLiberal.exerciceEnGroup).eq('libralSCM') ? true : false;
formFields['liberalAssociation']                   = Value('id').of(cheminExerciceLiberal.exerciceEnGroup).eq('liberalAssociation') ? true : false;

if(cheminExerciceLiberal.dateDeburActiviteLiberal != null) {
	var dateTemp = new Date(parseInt(cheminExerciceLiberal.dateDeburActiviteLiberal.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDeburActiviteLiberal'] 	    	= day;
	formFields['moisDeburActiviteLiberal']	        = month;
	formFields['anneeDeburActiviteLiberal']	    	= year;
}
//Adresse cabinet 

formFields['numAppartCabinet']                     = cheminExerciceLiberal.coordonneeCabinet.numAppartCabinet;
formFields['entreeImmeubleCabinet']                = cheminExerciceLiberal.coordonneeCabinet.entreeImmeubleCabinet;
formFields['numLibelleVoieCabinet']                = cheminExerciceLiberal.coordonneeCabinet.numLibelleVoieCabinet;
formFields['bpLieuDitCabinet']                     = cheminExerciceLiberal.coordonneeCabinet.bpLieuDitCabinet;
formFields['codePostalCabinet']                    = cheminExerciceLiberal.coordonneeCabinet.codePostalCabinet;
formFields['villeCabinet']                         = cheminExerciceLiberal.coordonneeCabinet.villeCabinet;
formFields['numTelPro']                            = cheminExerciceLiberal.coordonneeCabinet.numTelPro;
formFields['numPhoneCabinet']                      = cheminExerciceLiberal.coordonneeCabinet.numPhoneCabinet;
formFields['courrielPro']                          = cheminExerciceLiberal.coordonneeCabinet.numPhoneCabinet;
formFields['cabinetSecondaireOUI']                 = cheminExerciceLiberal.coordonneeCabinet.cabinetSecondaireOuiNon ? true : false;
formFields['cabinetSecondaireNON']                 = cheminExerciceLiberal.coordonneeCabinet.cabinetSecondaireOuiNon ? false : true;

formFields['numAppartCabinetSecondaire']           = cheminExerciceLiberal.coordoneesCabinetSecondaire.numAppartCabinetSecondaire;
formFields['entreeImmeubleCabinetSecondaire']      = cheminExerciceLiberal.coordoneesCabinetSecondaire.entreeImmeubleCabinetSecondaire;
formFields['numLibelleVoieCabinetSecondaire']      = cheminExerciceLiberal.coordoneesCabinetSecondaire.numLibelleVoieCabinetSecondaire;
formFields['bpLieuDitCabinetSecondaire']           = cheminExerciceLiberal.coordoneesCabinetSecondaire.bpLieuDitCabinetSecondaire;
formFields['codePostalCabinetSecondaire']          = cheminExerciceLiberal.coordoneesCabinetSecondaire.codePostalCabinetSecondaire;
formFields['villeCabinetSecondaire']               = cheminExerciceLiberal.coordoneesCabinetSecondaire.villeCabinetSecondaire;
formFields['numTelProSecondaire']                  = cheminExerciceLiberal.coordoneesCabinetSecondaire.numTelProSecondaire;


/************************************************************************
 * Secteur libéral avec le statut de remplaçant (e)
 ************************************************************************/
var chaminSecteurLiberalRemplacant = $qp152PE7.secteurExerciceGroup.liberalRemplacantGroup;
formFields['remplacantPermannatOUI']               = chaminSecteurLiberalRemplacant.remplacantPermannatOuiNon ? true : false;
formFields['remplacantPermannatNon']               = chaminSecteurLiberalRemplacant.remplacantPermannatOuiNon ? false : true;

if(chaminSecteurLiberalRemplacant.dateDelivranceAutorisation != null) {
	var dateTemp = new Date(parseInt(chaminSecteurLiberalRemplacant.dateDelivranceAutorisation.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourDelivranceAutorisation'] 	    = day;
	formFields['moisDelivranceAutorisation']	    = month;
	formFields['anneeDelivranceAutorisation']	    = year;
}
formFields['nomInfirmierRemplace']                 = chaminSecteurLiberalRemplacant.nomInfirmierRemplace;
formFields['prenomInfermierRemplace']              = chaminSecteurLiberalRemplacant.prenomInfermierRemplace;
formFields['numOrdinal']                           = chaminSecteurLiberalRemplacant.numOrdinal;
formFields['numAppartCabinetRemplacement']         = chaminSecteurLiberalRemplacant.numAppartCabinetRemplacement;
formFields['numLibelleAppartCabinetRemplacement']  = chaminSecteurLiberalRemplacant.numLibelleAppartCabinetRemplacement;
formFields['bpLieuDitCabinetRemplacement']         = chaminSecteurLiberalRemplacant.bpLieuDitCabinetRemplacement;
formFields['codePostalCabinetRemplacement']        = chaminSecteurLiberalRemplacant.codePostalCabinetRemplacement;
formFields['villeCabinetRemplacement']             = chaminSecteurLiberalRemplacant.villeCabinetRemplacement;

formFields['nomInfirmierRemplaceAutre']                 = '';
formFields['prenomInfermierRemplaceAutre']              = '';
formFields['numOrdinalAutre']                           = '';
formFields['numAppartCabinetRemplacementAutre']         = '';
formFields['numLibelleAppartCabinetRemplacementAutre']  = '';
formFields['bpLieuDitCabinetRemplacementAutre']         = '';
formFields['codePostalCabinetRemplacementAutre']        = '';
formFields['villeCabinetRemplacementAutre']             = '';

if (chaminSecteurLiberalRemplacant.remplacementAutreCabinetOuiNon){
	formFields['nomInfirmierRemplaceAutre']                 = chaminSecteurLiberalRemplacant.autreCabinetGroup.nomInfirmierRemplaceAutre;
	formFields['prenomInfermierRemplaceAutre']              = chaminSecteurLiberalRemplacant.autreCabinetGroup.prenomInfermierRemplaceAutre;
	formFields['numOrdinalAutre']                           = chaminSecteurLiberalRemplacant.autreCabinetGroup.numOrdinalAutre;
	formFields['numAppartCabinetRemplacementAutre']         = chaminSecteurLiberalRemplacant.autreCabinetGroup.numAppartCabinetRemplacementAutre;
	formFields['numLibelleAppartCabinetRemplacementAutre']  = chaminSecteurLiberalRemplacant.autreCabinetGroup.numLibelleAppartCabinetRemplacementAutre;
	formFields['bpLieuDitCabinetRemplacementAutre']         = chaminSecteurLiberalRemplacant.autreCabinetGroup.bpLieuDitCabinetRemplacementAutre;
	formFields['codePostalCabinetRemplacementAutre']        = chaminSecteurLiberalRemplacant.autreCabinetGroup.codePostalCabinetRemplacementAutre;
	formFields['villeCabinetRemplacementAutre']             = chaminSecteurLiberalRemplacant.autreCabinetGroup.villeCabinetRemplacementAutre;
}


/************************************************************************
 * Exercice en société
 ************************************************************************/
var cheminExerciceSociete = $qp152PE7.societeGroup1.societeGroup;

formFields['societeSCP']                           = Value('id').of(cheminExerciceSociete.typeSociete).eq('societeSCP') ? true : false;
formFields['societeSelarlSelarlu']                 = Value('id').of(cheminExerciceSociete.typeSociete).eq('societeSelarlSelarlu') ? true : false;
formFields['societeSelas']                         = Value('id').of(cheminExerciceSociete.typeSociete).eq('societeSelas') ? true : false;
formFields['nomSocieteExercice']                   = cheminExerciceSociete.nomSocieteExercice;
formFields['numInscriptionSocieteOrdre']           = cheminExerciceSociete.numInscriptionSocieteOrdre;
formFields['numAppartSociete']                     = cheminExerciceSociete.coordonneesSociete.numAppartSociete;
formFields['entreeSociete']                        = cheminExerciceSociete.coordonneesSociete.entreeSociete;
formFields['numLibelleVoieSociete']                = cheminExerciceSociete.coordonneesSociete.numLibelleVoieSociete;
formFields['bpLieuDitSociete']                     = cheminExerciceSociete.coordonneesSociete.bpLieuDitSociete;
formFields['codePostalSociete']                    = cheminExerciceSociete.coordonneesSociete.codePostalSociete;
formFields['villeSociete']                         = cheminExerciceSociete.coordonneesSociete.villeSociete;
formFields['numTelSociete']                        = cheminExerciceSociete.coordonneesSociete.numTelSociete;
formFields['numPortableSociete']                   = cheminExerciceSociete.coordonneesSociete.numPortableSociete;
formFields['siretSociete']                         = cheminExerciceSociete.coordonneesSociete.siretSociete;
formFields['nombreAssocieSociete']                 = cheminExerciceSociete.coordonneesSociete.nombreAssocieSociete;


formFields['declarationHonneurEcrit']				   = "Je déclare sur l’honneur qu’aucune instance pouvant donner lieu à condamnation ou sanction susceptible d’avoir des conséquences sur mon inscription au tableau de l’Ordre n’est en cours à mon encontre.";
formFields['codeDeontologieEcrit']					   = "J’affirme avoir pris connaissance du code de déontologie des infirmiers et je fais serment de le respecter.";


/************************************************************************
 * Signature
 ************************************************************************/
var cheminSignature = $qp152PE7.signaturePage.signatureGroup;

formFields['numDeptExercicePrincipal']             = cheminSignature.deptExercicePrincipal;

formFields['declarationHonneur']                   = cheminSignature.declarationHonneur;
formFields['codeDeontologie']                      = cheminSignature.codeDeontologie;
formFields['annuaireInfermier']                    = cheminSignature.annuaireInfermier;
formFields['lieuSignature']                        = cheminSignature.lieuSignature;

if(cheminSignature.dateSignature != null) {
	var dateTemp = new Date(parseInt(cheminSignature.dateSignature.getTimeInMillis()));
		var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
		var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	formFields['jourSignature'] 		= day;
	formFields['moisSignature'] 		= month;
	formFields['anneeSignature']		= year;
}
formFields['signature']				            = $qp152PE7.etatCivilPage.etatCivilGroup.civilite +" "+ $qp152PE7.etatCivilPage.etatCivilGroup.nomNaissance +" "+ $qp152PE7.etatCivilPage.etatCivilGroup.prenoms;

/*******************************************************************************
 * Chargement du courrier d'accompagnement à destination de l'AC
 ******************************************************************************/

var cerfaDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
.apply({
	dateSignature : $qp152PE7.signaturePage.signatureGroup.dateSignature ,
	autoriteHabilitee :"Conseil national de l’Ordre des infirmiers" ,
	demandeContexte : "Inscription à l’Ordre reconnaissance automatique",
	civiliteNomPrenom : $qp152PE7.etatCivilPage.etatCivilGroup.civilite +" "+ $qp152PE7.etatCivilPage.etatCivilGroup.nomNaissance +" "+ $qp152PE7.etatCivilPage.etatCivilGroup.prenoms
});


var finalDoc = nash.doc //
.load('models/Formulaire inscription ordre exercice mixte.pdf') //
.apply (formFields);


cerfaDoc.append(finalDoc.save('cerfa.pdf'));	

/*******************************************************************************
 * Pieces jointes
 ******************************************************************************/

function appendPj(fld) {
    fld.forEach(function (elm) {
    	cerfaDoc.append(elm);
    });
}

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationHonneur);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjMaitriseFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatRadiation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPaie);
appendPj($attachmentPreprocess.attachmentPreprocess.pjContratTravail);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssuranceCivile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjToutContratLiberal);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorisationRemplacement);

/*******************************************************************************
 *  Enregistrement du fichier (en mémoire)
 ******************************************************************************/

var finalDocItem = cerfaDoc.save('Infirmier_Anesthesiste.pdf');	

/*******************************************************************************
 *  Persistance des données obtenues
 ******************************************************************************/

return spec.create({
id : 'review',
label : 'Infirmier anesthésiste  - Inscription à l’Ordre reconnaissance automatique',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : "Demande d'inscription à l'Ordre national pour la profession d'infirmier en vue d'un libre établissement en exercice mixte grâce à la reconnaissance automatique de diplôme.",
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});