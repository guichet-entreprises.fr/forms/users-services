var formFields = {};
formFields['civiliteMadame']                = $step1.info2.CiviliteMadame;
formFields['civiliteMonsieur']              = $step1.info2.CiviliteMonsieur;
formFields['civiliteMademoiselle']          = $step1.info2.CiviliteMademoiselle;
formFields['nomExercice']                   = $step1.info2.NomExercice;
formFields['prenoms']                       = $step1.info2.Prenoms;
formFields['nomNaissance']                  = $step1.info2.NomNaissance;
formFields['lieuNaissance']                 = $step1.info2.LieuNaissance;
formFields['numDeptNaissance']              = $step1.info2.NumDeptNaissance;
formFields['dateNaissance']                 = $step1.info2.DateNaissance;
formFields['nationaliteFrancaise']          = $step1.info2.NationaliteFrancaise;
formFields['nationaliteUE']                 = $step1.info2.NationaliteUE;
formFields['preciserNationaliteUE']         = $step1.info2.PreciserNationaliteUE;
formFields['autreNationalite']              = $step1.info2.AutreNationalite;
formFields['numLibelle']                    = $step1.info2.NumLibelle;
formFields['commune']                       = $step1.info2.Commune;
formFields['langueParlees']                 = $step1.info2.LangueParlees;
formFields['premierAnneeActivite']          = $step1.info2.PremierAnneeActivite;
formFields['departementExercicePrecedent']  = $step1.info2.DepartementExercicePrecedent;
formFields['liberalExclusif']               = $step1.info2.LiberalExclusif;
formFields['salarieExclusif']               = $step1.info2.SalarieExclusif;
formFields['activiteMixte']                 = $step1.info2.ActiviteMixte;
formFields['autreActif']                    = $step1.info2.AutreActif;
formFields['retraite']                      = $step1.info2.Retraite;
formFields['autreInactif']                  = $step1.info2.AutreInactif;
formFields['dateObtentionTitrePro']         = $step1.info2.DateObtentionTitrePro;
formFields['lieuObtentionTitre']            = $step1.info2.LieuObtentionTitre;
formFields['specialiteNon']                 = $step1.info2.SpecialiteNon;
formFields['specialiteOUI']                 = $step1.info2.SpecialiteOUI;
formFields['licenceMaitriseDESS']           = $step1.info2.LicenceMaitriseDESS;
formFields['licenceMaitriseEquivalent']     = $step1.info2.LicenceMaitriseEquivalent;
formFields['licenceMaitriceDEA']            = $step1.info2.LicenceMaitriceDEA;
formFields['listeReglementaire']            = $step1.info2.ListeReglementaire;
formFields['autorisationDiplomeUE']         = $step1.info2.AutorisationDiplomeUE;
formFields['autorisationPrefectoral']       = $step1.info2.AutorisationPrefectoral;
formFields['usageRestreintTitrePsy']        = $step1.info2.UsageRestreintTitrePsy;
formFields['psycoClinique']                 = $step1.info2.PsycoClinique;
formFields['psycoSociale']                  = $step1.info2.PsycoSociale;
formFields['psycoEducationNationale']       = $step1.info2.PsycoEducationNationale;
formFields['psycoDev']                      = $step1.info2.PsycoDev;
formFields['psycoTravail']                  = $step1.info2.PsycoTravail;
formFields['psycoGerontologie']             = $step1.info2.PsycoGerontologie;
formFields['autreSpecialite']               = $step1.info2.AutreSpecialite;
formFields['exerciceIndividuel']            = $step1.info3.ExerciceIndividuel;
formFields['exerciceAssociation']           = $step1.info3.ExerciceAssociation;
formFields['nomRaisonSociale']              = $step1.info3.NomRaisonSociale;
formFields['numLibelleVoie']                = $step1.info3.NumLibelleVoie;
formFields['complementAdresse']             = $step1.info2.ComplementAdresse;
formFields['complementAdresse']             = $step1.info3.ComplementAdresse;
formFields['dateInstalation']               = $step1.info3.DateInstalation;
formFields['codePostal']                    = $step1.info2.CodePostal;
formFields['codePostal']                    = $step1.info3.CodePostal;
formFields['villeCommune']                  = $step1.info3.VilleCommune;
formFields['scp']                           = $step1.info3.Scp;
formFields['sel']                           = $step1.info3.Sel;
formFields['selarl']                        = $step1.info3.Selarl;
formFields['selafa']                        = $step1.info3.Selafa;
formFields['selca']                         = $step1.info3.Selca;
formFields['autre']                         = $step1.info3.Autre;
formFields['numTel']                        = $step1.info2.NumTel;
formFields['numTel']                        = $step1.info3.NumTel;
formFields['email']                         = $step1.info2.Email;
formFields['email']                         = $step1.info3.Email;
formFields['cabinetSecondaireOUI']          = $step1.info3.CabinetSecondaireOUI;
formFields['cabinetSecondaireNON']          = $step1.info3.CabinetSecondaireNON;
formFields['numLibelleVoieSecondaire']      = $step1.info3.NumLibelleVoieSecondaire;
formFields['complementAdresseSecondaire']   = $step1.info3.ComplementAdresseSecondaire;
formFields['codePostalSecondaire']          = $step1.info3.CodePostalSecondaire;
formFields['villeCommuneSecondaire']        = $step1.info3.VilleCommuneSecondaire;
formFields['numTelSecondaire']              = $step1.info3.NumTelSecondaire;
formFields['numFax']                        = $step1.info3.NumFax;
formFields['numFaxSecondaire']              = $step1.info3.NumFaxSecondaire;
formFields['emailSecondaire']               = $step1.info3.EmailSecondaire;
formFields['activiteSecteurLiberal']        = $step1.info3.ActiviteSecteurLiberal;
formFields['activiteSalarie']               = $step1.info3.ActiviteSalarie;
formFields['nomRaisonSocialeSalarie']       = $step1.info3.NomRaisonSocialeSalarie;
formFields['numLibelleVoieSalarie']         = $step1.info3.NumLibelleVoieSalarie;
formFields['complementAdresseSalarie']      = $step1.info3.ComplementAdresseSalarie;
formFields['datePriseFonctionSalarie']      = $step1.info3.DatePriseFonctionSalarie;
formFields['codePostalSalaire']             = $step1.info3.CodePostalSalaire;
formFields['villeCommuneSalarie']           = $step1.info3.VilleCommuneSalarie;
formFields['numTelSalarie']                 = $step1.info3.NumTelSalarie;
formFields['numFaxSalarie']                 = $step1.info3.NumFaxSalarie;
formFields['emailSalarie']                  = $step1.info3.EmailSalarie;
formFields['autreExerciceSalarie']          = $step1.info3.AutreExerciceSalarie;
formFields['nomRaisonSocialeAutre']         = $step1.info3.NomRaisonSocialeAutre;
formFields['datePriseFonctionAutreSalarie'] = $step1.info3.DatePriseFonctionAutreSalarie;
formFields['siretAutreSalarie']             = $step1.info3.SiretAutreSalarie;
formFields['numLibelleVoieAutreSalarie']    = $step1.info3.NumLibelleVoieAutreSalarie;
formFields['complementAutreSalarie']        = $step1.info3.ComplementAutreSalarie;
formFields['codePostalAutreSalarie']        = $step1.info3.CodePostalAutreSalarie;
formFields['villeCommuneAutreSalarie']      = $step1.info3.VilleCommuneAutreSalarie;
formFields['nulTelAutreSalarie']            = $step1.info3.NulTelAutreSalarie;
formFields['numFaxAutreSalarie']            = $step1.info3.NumFaxAutreSalarie;
formFields['emailAutreSalarie']             = $step1.info3.EmailAutreSalarie;
formFields['dateEnregistrement']            = $step1.info3.DateEnregistrement;
formFields['numAdeli']                      = $step1.info3.NumAdeli;
formFields['dateSignature']                 = $step1.info3.DateSignature;
formFields['signature']                     = $step1.info3.Signature;
formFields['siretLiberal']                  = $step1.info3.SiretLiberal;

var pdfModel = nash.doc.load('models/model.pdf').apply(formFields);

var data = [ spec.createData({
    id : 'record',
    label : 'Record',
    description : 'Record generated from the typed data',
    type : 'FileReadOnly',
    value : [ pdfModel.save('record.pdf') ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Generated records',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Review',
    groups : groups
});
