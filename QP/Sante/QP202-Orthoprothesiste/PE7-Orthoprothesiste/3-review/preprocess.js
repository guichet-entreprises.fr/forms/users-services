var formFields = {};

var civNomPrenom = $qp202PE7.info1.etatCivil.civilite + ' ' + $qp202PE7.info1.etatCivil.declarantNomNaissance + ' ' + $qp202PE7.info1.etatCivil.declarantPrenoms;

//Motif de la demande

formFields['professionConcernee']                                  = "Orthoprothésiste";

formFields['declarationInitialeLPS']                               = false;
formFields['renouvellementLPS']                                    = true;
formFields['modificationLPS']                                      = false;


//Identité du demandeur

formFields['declarantNomNaissance']                                = $qp202PE7.info1.etatCivil.declarantNomNaissance;
formFields['declarantPrenoms']                                     = $qp202PE7.info1.etatCivil.declarantPrenoms;
formFields['declarantNationalite']                                 = $qp202PE7.info1.etatCivil.declarantNationalite;
formFields['declarantDateNaissance']                               = $qp202PE7.info1.etatCivil.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp202PE7.info1.etatCivil.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp202PE7.info1.etatCivil.declarantPaysNaissance;


//Adresse

formFields['declarantAdressePersoCourriel']                        = $qp202PE7.info2.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoieComplement']            = ($qp202PE7.info2.coordonnees1.declarantAdressePersoNumNomVoie != null ? $qp202PE7.info2.coordonnees1.declarantAdressePersoNumNomVoie: '') + ' ' + ($qp202PE7.info2.coordonnees1.declarantAdressePersoComplement != null ? $qp202PE7.info2.coordonnees1.declarantAdressePersoComplement: '');
//formFields['declarantAdressePersoCP']                            = $qp202PE7.info2.coordonnees1.declarantAdressePersoCP;
//formFields['declarantAdressePersoVille']                         = $qp202PE7.info2.coordonnees1.declarantAdressePersoVille;
//formFields['declarantAdressePersoPays']                          = $qp202PE7.info2.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoCPVillePays']                     = ($qp202PE7.info2.coordonnees1.declarantAdressePersoCP != null ? $qp202PE7.info2.coordonnees1.declarantAdressePersoCP: '') + ' ' + ($qp202PE7.info2.coordonnees1.declarantAdressePersoVille != null ? $qp202PE7.info2.coordonnees1.declarantAdressePersoVille: '') +' '+ ($qp202PE7.info2.coordonnees1.declarantAdressePersoPays != null ? $qp202PE7.info2.coordonnees1.declarantAdressePersoPays: '');
formFields['declarantAdressePersoTelephone']                       = $qp202PE7.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp202PE7.info2.coordonnees1.declarantAdressePersoCourriel;

//Adresse France 

formFields['declarantAdresseFranceNumNomVoieComplement']           = ($qp202PE7.info2.coordonnees2.declarantAdresseFranceNumNomVoie != null ? $qp202PE7.info2.coordonnees2.declarantAdresseFranceNumNomVoie: '') + ' ' + ($qp202PE7.info2.coordonnees2.declarantAdresseFranceComplement != null ? $qp202PE7.info2.coordonnees2.declarantAdresseFranceComplement: '');
formFields['declarantAdresseFranceCPVillePays']                    = ($qp202PE7.info2.coordonnees2.declarantAdresseFranceCP != null ? $qp202PE7.info2.coordonnees2.declarantAdresseFranceCP: '') + ' ' + ($qp202PE7.info2.coordonnees2.declarantAdresseFranceVille != null ? $qp202PE7.info2.coordonnees2.declarantAdresseFranceVille: '');
formFields['declarantAdresseFranceTelephone']                      = $qp202PE7.info2.coordonnees2.declarantAdresseFranceTelephone;
formFields['declarantAdresseFranceCourriel']                       = $qp202PE7.info2.coordonnees2.declarantAdresseFranceCourriel;

//Profession concernée

formFields['professionExercee']                                    = $qp202PE7.info3.profession.professionExercee;
formFields['specialite']                                           = $qp202PE7.info3.profession.specialite != null ? $qp202PE7.info3.profession.specialite  : '';
formFields['professionDemandee']                                   = $qp202PE7.info3.profession.professionDemandee;
formFields['specialiteDemandee']                                   = $qp202PE7.info3.profession.specialiteDemandee != null ? $qp202PE7.info3.profession.specialiteDemandee : '';
formFields['actesEnvisages']                                       = $qp202PE7.info3.profession.actesEnvisages != null ? $qp202PE7.info3.profession.actesEnvisages : '';
formFields['lieupremiereLPS']                                      = $qp202PE7.info3.profession.lieupremiereLPS != null ? $qp202PE7.info3.profession.lieupremiereLPS : '';

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp202PE7.info4.ordre.ouiNon).eq('ordreNon') ? true : false;
formFields['ordreOui']                                             = Value('id').of($qp202PE7.info4.ordre.ouiNon).eq('ordreOui') ? true : false;

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp202PE7.info4.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp202PE7.info4.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp202PE7.info4.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp202PE7.info4.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp202PE7.info4.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp202PE7.info4.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp202PE7.info4.ordre.ordreProfessionnelAdresseVille != null ? $qp202PE7.info4.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp202PE7.info4.ordre.ordreProfessionnelAdressePays != null ? $qp202PE7.info4.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = ($qp202PE7.info4.ordre.ordreProfessionnelNom != null ? $qp202PE7.info4.ordre.ordreProfessionnelNom : '') + ' ' + ($qp202PE7.info4.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp202PE7.info4.ordre.ordreProfessionnelNumEnregistrementNom: '');

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp202PE7.info5.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp202PE7.info5.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp202PE7.info5.assuranceProfessionnelle.commentaires != null ? $qp202PE7.info5.assuranceProfessionnelle.commentaires :'';

//informations à fournir en cas de renouvellement 
formFields['prestation1Du']                       = $qp202PE7.detailsRenouvellement.renouvellement.periodePreste1.from;
formFields['prestation1Au']                       = $qp202PE7.detailsRenouvellement.renouvellement.periodePreste1.to;
formFields['prestation2Du']                       = ($qp202PE7.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp202PE7.detailsRenouvellement.renouvellement.periodePreste2.from : '');
formFields['prestation2Au']                       = ($qp202PE7.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp202PE7.detailsRenouvellement.renouvellement.periodePreste2.to : '');
formFields['prestation3Du']                       = ($qp202PE7.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp202PE7.detailsRenouvellement.renouvellement.periodePreste3.from : '');
formFields['prestation3Au']                       = ($qp202PE7.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp202PE7.detailsRenouvellement.renouvellement.periodePreste3.to : '');
formFields['prestation4Du']                       = ($qp202PE7.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp202PE7.detailsRenouvellement.renouvellement.periodePreste4.from : '');
formFields['prestation4Au']                       = ($qp202PE7.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp202PE7.detailsRenouvellement.renouvellement.periodePreste4.to : '');
formFields['prestation5Du']                       = ($qp202PE7.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp202PE7.detailsRenouvellement.renouvellement.periodePreste5.from : '');
formFields['prestation5Au']                       = ($qp202PE7.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp202PE7.detailsRenouvellement.renouvellement.periodePreste5.to : '');

formFields['commentairesEventuels']               = $qp202PE7.detailsRenouvellement.renouvellement.commentairesRenouvellement != null ? $qp202PE7.detailsRenouvellement.renouvellement.commentairesRenouvellement :'';
formFields['activitesPrestee']  				  = $qp202PE7.detailsRenouvellement.renouvellement.activitesProfessionnelles != null ? $qp202PE7.detailsRenouvellement.renouvellement.activitesProfessionnelles :'';


//Signature

formFields['autresObservations']                                   = $qp202PE7.commentaire.commentaire.commentairesEventuels != null ? $qp202PE7.commentaire.commentaire.commentairesEventuels :'';
formFields['dateSignature']                                        = $qp202PE7.info6.signature.dateSignature;
formFields['attesteSignature']                                     = $qp202PE7.info6.signature.attesteSignature;



formFields['libelleProfession']				                       = "Orthoprothésiste"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp202PE7.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp202PE7.info6.signature.dateSignature,
		autoriteHabilitee :"Ministère des Solidarités et de la Santé",
		demandeContexte : "Demande de renouvellement de la la déclaration en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire_sante_renouv.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);



/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Formulaire_sante_renouv_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Orthoprothésiste - Renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession d\'orthoprothésiste.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
