var formFields = {};

var civNomPrenom = $qp126PE6.info1.etatCivil.civilite + ' ' + $qp126PE6.info1.etatCivil.declarantNomNaissance + ' ' + $qp126PE6.info1.etatCivil.declarantPrenoms;

//Motif de la demande

formFields['professionConcernee']                                  = "Epithésiste";

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;


//Identité du demandeur

formFields['declarantNomNaissance']                                = $qp126PE6.info1.etatCivil.declarantNomNaissance;
formFields['declarantPrenoms']                                     = $qp126PE6.info1.etatCivil.declarantPrenoms;
formFields['declarantNationalite']                                 = $qp126PE6.info1.etatCivil.declarantNationalite;
formFields['declarantDateNaissance']                               = $qp126PE6.info1.etatCivil.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp126PE6.info1.etatCivil.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp126PE6.info1.etatCivil.declarantPaysNaissance;


//Adresse

formFields['declarantAdressePersoCourriel']                        = $qp126PE6.info2.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoieComplement']            = ($qp126PE6.info2.coordonnees1.declarantAdressePersoNumNomVoie != null ? $qp126PE6.info2.coordonnees1.declarantAdressePersoNumNomVoie: '') + ' ' + ($qp126PE6.info2.coordonnees1.declarantAdressePersoComplement != null ? $qp126PE6.info2.coordonnees1.declarantAdressePersoComplement: '');
//formFields['declarantAdressePersoCP']                            = $qp126PE6.info2.coordonnees1.declarantAdressePersoCP;
//formFields['declarantAdressePersoVille']                         = $qp126PE6.info2.coordonnees1.declarantAdressePersoVille;
//formFields['declarantAdressePersoPays']                          = $qp126PE6.info2.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoCPVillePays']                     = ($qp126PE6.info2.coordonnees1.declarantAdressePersoCP != null ? $qp126PE6.info2.coordonnees1.declarantAdressePersoCP: '') + ' ' + ($qp126PE6.info2.coordonnees1.declarantAdressePersoVille != null ? $qp126PE6.info2.coordonnees1.declarantAdressePersoVille: '') +' '+ ($qp126PE6.info2.coordonnees1.declarantAdressePersoPays != null ? $qp126PE6.info2.coordonnees1.declarantAdressePersoPays: '');
formFields['declarantAdressePersoTelephone']                       = $qp126PE6.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp126PE6.info2.coordonnees1.declarantAdressePersoCourriel;

//Adresse France 

formFields['declarantAdresseFranceNumNomVoieComplement']           = ($qp126PE6.info2.coordonnees2.declarantAdresseFranceNumNomVoie != null ? $qp126PE6.info2.coordonnees2.declarantAdresseFranceNumNomVoie: '') + ' ' + ($qp126PE6.info2.coordonnees2.declarantAdresseFranceComplement != null ? $qp126PE6.info2.coordonnees2.declarantAdresseFranceComplement: '');
formFields['declarantAdresseFranceCPVillePays']                    = ($qp126PE6.info2.coordonnees2.declarantAdresseFranceCP != null ? $qp126PE6.info2.coordonnees2.declarantAdresseFranceCP: '') + ' ' + ($qp126PE6.info2.coordonnees2.declarantAdresseFranceVille != null ? $qp126PE6.info2.coordonnees2.declarantAdresseFranceVille: '');
formFields['declarantAdresseFranceTelephone']                      = $qp126PE6.info2.coordonnees2.declarantAdresseFranceTelephone;
formFields['declarantAdresseFranceCourriel']                       = $qp126PE6.info2.coordonnees2.declarantAdresseFranceCourriel;

//Profession concernée

formFields['professionExercee']                                    = $qp126PE6.info3.profession.professionExercee;
formFields['specialite']                                           = $qp126PE6.info3.profession.specialite != null ? $qp126PE6.info3.profession.specialite  : '';
formFields['professionDemandee']                                   = $qp126PE6.info3.profession.professionDemandee;
formFields['specialiteDemandee']                                   = $qp126PE6.info3.profession.specialiteDemandee != null ? $qp126PE6.info3.profession.specialiteDemandee : '';
formFields['actesEnvisages']                                       = $qp126PE6.info3.profession.actesEnvisages != null ? $qp126PE6.info3.profession.actesEnvisages : '';
formFields['lieupremiereLPS']                                      = $qp126PE6.info3.profession.lieupremiereLPS != null ? $qp126PE6.info3.profession.lieupremiereLPS : '';

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp126PE6.info4.ordre.ouiNon).eq('ordreNon') ? true : false;
formFields['ordreOui']                                             = Value('id').of($qp126PE6.info4.ordre.ouiNon).eq('ordreOui') ? true : false;

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp126PE6.info4.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp126PE6.info4.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp126PE6.info4.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp126PE6.info4.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp126PE6.info4.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp126PE6.info4.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp126PE6.info4.ordre.ordreProfessionnelAdresseVille != null ? $qp126PE6.info4.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp126PE6.info4.ordre.ordreProfessionnelAdressePays != null ? $qp126PE6.info4.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = ($qp126PE6.info4.ordre.ordreProfessionnelNom != null ? $qp126PE6.info4.ordre.ordreProfessionnelNom : '') + ' ' 
																	+ ($qp126PE6.info4.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp126PE6.info4.ordre.ordreProfessionnelNumEnregistrementNom: '');

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp126PE6.info5.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp126PE6.info5.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp126PE6.info5.assuranceProfessionnelle.commentaires != null ? $qp126PE6.info5.assuranceProfessionnelle.commentaires : '';

//Signature&Observations

formFields['autresObservations']                                   = $qp126PE6.info6.observations.autresObservations != null ? $qp126PE6.info6.observations.autresObservations : '';
formFields['dateSignature']                                        = $qp126PE6.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp126PE6.info7.signature.attesteSignature;



formFields['libelleProfession']				                       = "Epithésiste"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp126PE6.info7.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp126PE6.info7.signature.dateSignature,
		autoriteHabilitee :"Ministère des Solidarités et de la Santé",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire_sante_LPS.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEquivalentDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjReconnaissanceFormation);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Epithésiste_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Epithésiste - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'epithésiste.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
