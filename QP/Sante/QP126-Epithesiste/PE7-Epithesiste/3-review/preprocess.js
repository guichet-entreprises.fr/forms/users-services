var formFields = {};

var civNomPrenom = $qp126PE7.info1.etatCivil.civilite + ' ' + $qp126PE7.info1.etatCivil.declarantNomNaissance + ' ' + $qp126PE7.info1.etatCivil.declarantPrenoms;

//Motif de la demande

formFields['professionConcernee']                                  = "epithesiste";

formFields['declarationInitialeLPS']                               = false;
formFields['renouvellementLPS']                                    = true;
formFields['modificationLPS']                                      = false;


//Identité du demandeur

formFields['declarantNomNaissance']                                = $qp126PE7.info1.etatCivil.declarantNomNaissance;
formFields['declarantPrenoms']                                     = $qp126PE7.info1.etatCivil.declarantPrenoms;
formFields['declarantNationalite']                                 = $qp126PE7.info1.etatCivil.declarantNationalite;
formFields['declarantDateNaissance']                               = $qp126PE7.info1.etatCivil.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp126PE7.info1.etatCivil.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp126PE7.info1.etatCivil.declarantPaysNaissance;


//Adresse

formFields['declarantAdressePersoCourriel']                        = $qp126PE7.info2.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoieComplement']            = ($qp126PE7.info2.coordonnees1.declarantAdressePersoNumNomVoie != null ? $qp126PE7.info2.coordonnees1.declarantAdressePersoNumNomVoie: '') + ' ' + ($qp126PE7.info2.coordonnees1.declarantAdressePersoComplement != null ? $qp126PE7.info2.coordonnees1.declarantAdressePersoComplement: '');
//formFields['declarantAdressePersoCP']                            = $qp126PE7.info2.coordonnees1.declarantAdressePersoCP;
//formFields['declarantAdressePersoVille']                         = $qp126PE7.info2.coordonnees1.declarantAdressePersoVille;
//formFields['declarantAdressePersoPays']                          = $qp126PE7.info2.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoCPVillePays']                     = ($qp126PE7.info2.coordonnees1.declarantAdressePersoCP != null ? $qp126PE7.info2.coordonnees1.declarantAdressePersoCP: '') + ' ' + ($qp126PE7.info2.coordonnees1.declarantAdressePersoVille != null ? $qp126PE7.info2.coordonnees1.declarantAdressePersoVille: '') +' '+ ($qp126PE7.info2.coordonnees1.declarantAdressePersoPays != null ? $qp126PE7.info2.coordonnees1.declarantAdressePersoPays: '');
formFields['declarantAdressePersoTelephone']                       = $qp126PE7.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp126PE7.info2.coordonnees1.declarantAdressePersoCourriel;

//Adresse France 

formFields['declarantAdresseFranceNumNomVoieComplement']           = ($qp126PE7.info2.coordonnees2.declarantAdresseFranceNumNomVoie != null ? $qp126PE7.info2.coordonnees2.declarantAdresseFranceNumNomVoie: '') + ' ' + ($qp126PE7.info2.coordonnees2.declarantAdresseFranceComplement != null ? $qp126PE7.info2.coordonnees2.declarantAdresseFranceComplement: '');
formFields['declarantAdresseFranceCPVillePays']                    = ($qp126PE7.info2.coordonnees2.declarantAdresseFranceCP != null ? $qp126PE7.info2.coordonnees2.declarantAdresseFranceCP: '') + ' ' + ($qp126PE7.info2.coordonnees2.declarantAdresseFranceVille != null ? $qp126PE7.info2.coordonnees2.declarantAdresseFranceVille: '');
formFields['declarantAdresseFranceTelephone']                      = $qp126PE7.info2.coordonnees2.declarantAdresseFranceTelephone;
formFields['declarantAdresseFranceCourriel']                       = $qp126PE7.info2.coordonnees2.declarantAdresseFranceCourriel;

//Profession concernée

formFields['professionExercee']                                    = $qp126PE7.info3.profession.professionExercee;
formFields['specialite']                                           = $qp126PE7.info3.profession.specialite;
formFields['professionDemandee']                                   = $qp126PE7.info3.profession.professionDemandee;
formFields['specialiteDemandee']                                   = $qp126PE7.info3.profession.specialiteDemandee;
formFields['actesEnvisages']                                       = $qp126PE7.info3.profession.actesEnvisages;
formFields['lieupremiereLPS']                                      = $qp126PE7.info3.profession.lieupremiereLPS;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp126PE7.info4.ordre.ouiNon).eq('ordreNon') ? true : false;
formFields['ordreOui']                                             = Value('id').of($qp126PE7.info4.ordre.ouiNon).eq('ordreOui') ? true : false;

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp126PE7.info4.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp126PE7.info4.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp126PE7.info4.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp126PE7.info4.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp126PE7.info4.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp126PE7.info4.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp126PE7.info4.ordre.ordreProfessionnelAdresseVille != null ? $qp126PE7.info4.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp126PE7.info4.ordre.ordreProfessionnelAdressePays != null ? $qp126PE7.info4.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = ($qp126PE7.info4.ordre.ordreProfessionnelNom != null ? $qp126PE7.info4.ordre.ordreProfessionnelNom : '') + ' ' + ($qp126PE7.info4.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp126PE7.info4.ordre.ordreProfessionnelNumEnregistrementNom: '');

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp126PE7.info5.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp126PE7.info5.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp126PE7.info5.assuranceProfessionnelle.commentaires != null ? $qp126PE7.info5.assuranceProfessionnelle.commentaires : '' ;

//informations à fournir en cas de renouvellement 
formFields['prestation1Du']                       = $qp126PE7.detailsRenouvellement.renouvellement.periodePreste1.from;
formFields['prestation1Au']                       = $qp126PE7.detailsRenouvellement.renouvellement.periodePreste1.to;
formFields['prestation2Du']                       = ($qp126PE7.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp126PE7.detailsRenouvellement.renouvellement.periodePreste2.from : '');
formFields['prestation2Au']                       = ($qp126PE7.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp126PE7.detailsRenouvellement.renouvellement.periodePreste2.to : '');
formFields['prestation3Du']                       = ($qp126PE7.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp126PE7.detailsRenouvellement.renouvellement.periodePreste3.from : '');
formFields['prestation3Au']                       = ($qp126PE7.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp126PE7.detailsRenouvellement.renouvellement.periodePreste3.to : '');
formFields['prestation4Du']                       = ($qp126PE7.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp126PE7.detailsRenouvellement.renouvellement.periodePreste4.from : '');
formFields['prestation4Au']                       = ($qp126PE7.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp126PE7.detailsRenouvellement.renouvellement.periodePreste4.to : '');
formFields['prestation5Du']                       = ($qp126PE7.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp126PE7.detailsRenouvellement.renouvellement.periodePreste5.from : '');
formFields['prestation5Au']                       = ($qp126PE7.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp126PE7.detailsRenouvellement.renouvellement.periodePreste5.to : '');

formFields['commentairesEventuels']               = $qp126PE7.detailsRenouvellement.renouvellement.commentairesRenouvellement != null ? $qp126PE7.detailsRenouvellement.renouvellement.commentairesRenouvellement : '';
formFields['activitesPrestee']  				  = $qp126PE7.detailsRenouvellement.renouvellement.activitesProfessionnelles != null ? $qp126PE7.detailsRenouvellement.renouvellement.activitesProfessionnelles : '';


//Signature&Observations

formFields['autresObservations']                                   = $qp126PE7.info6.observations.autresObservations != null ? $qp126PE7.info6.observations.autresObservations : '';
formFields['dateSignature']                                        = $qp126PE7.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp126PE7.info7.signature.attesteSignature;



formFields['libelleProfession']				                       = "Epithésiste"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp126PE7.info7.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp126PE7.info7.signature.dateSignature,
		autoriteHabilitee :"Ministère des Solidarités et de la Santé",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire_sante_renouv.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);



/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Epithésiste_renouv_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Epithésiste - Renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession d\'epithésiste.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
