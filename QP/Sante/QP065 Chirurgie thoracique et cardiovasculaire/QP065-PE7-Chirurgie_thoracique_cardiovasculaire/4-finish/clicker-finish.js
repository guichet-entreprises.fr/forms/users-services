log.info('Calling clicker finish');

var eventName = 'FINISH/' + nash.record.description().reference.code;
var recordUid = nash.record.description().recordUid;
var counter = 1;

//libs.clicker.click(eventName, recordUid, counter);

var clickerResponse = null;
var date = new Date();
var effectDate =[date .getFullYear(),
          ((date .getMonth() + 1) >9 ? '' : '0') + (date .getMonth() + 1),
          ((date .getDate())>9 ? '' : '0') + (date .getDate())
         ].join('');
try {
	clickerResponse = nash.service.request('${ws.clicker.url}/v1/event') //
		.dataType('application/json') //
		.accept('json') //
		.param('ref', eventName) //
		.param('effectDate', effectDate) //
		.param('counter', counter) //
		.param('comment', recordUid) //
		.post(null) //
	;

	if (clickerResponse != null && clickerResponse.getStatus() == 200) {
		nash.instance.load('clicker-finish.xml').bind('status', {
			'click' : true
		});
		log.info('Click for event : {}', eventName);
	}
} catch (e) {
	log.error('An technical error occured when calling clicker with reference {} at effect date {} with comment : {}', eventName, effectDate, recordUid);	
	log.error(e);
	return;
}
