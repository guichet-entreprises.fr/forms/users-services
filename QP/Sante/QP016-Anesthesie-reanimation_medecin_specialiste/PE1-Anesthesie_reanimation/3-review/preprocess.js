var formFields = {};

var civNomPrenom = $qp016PE1.info2.etatCivile.civilite + ' ' + $qp016PE1.info2.etatCivile.declarantNomNaissance + ' ' + $qp016PE1.info2.etatCivile.declarantPrenoms;


//Prestation de service

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp016PE1.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp016PE1.info2.etatCivile.civilite).eq('monsieur') ? '' : '-----';
formFields['declarantNomNaissance']                                = $qp016PE1.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp016PE1.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp016PE1.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp016PE1.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp016PE1.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp016PE1.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp016PE1.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoNumNomVoie']                      = $qp016PE1.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp016PE1.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoVille']                      	   = $qp016PE1.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCP']                              = $qp016PE1.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp016PE1.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp016PE1.info3.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp016PE1.info3.coordonnees1.declarantAdressePersoCourriel;

//Coordonnées en France
formFields['declarantAdresseFranceNumNomVoie']                     = $qp016PE1.coordonneesFrancePage.coordonnees2.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp016PE1.coordonneesFrancePage.coordonnees2.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceVille']                          = $qp016PE1.coordonneesFrancePage.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFranceCP']                             = $qp016PE1.coordonneesFrancePage.coordonnees2.declarantAdresseFranceCP;
formFields['declarantAdresseFranceTelephone']                      = $qp016PE1.coordonneesFrancePage.coordonnees2.declarantAdresseFranceTelephone;

//Profession considérée

formFields['typesActes']                                           = $qp016PE1.info1.profession.typesActes;
formFields['lieuExercice']                                         = $qp016PE1.info1.profession.lieuExercice;
formFields['nomProfessionLPS']                                     = $qp016PE1.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp016PE1.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp016PE1.info1.profession.specialite;
formFields['listeActes']                                           = $qp016PE1.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp016PE1.info1.profession.exerciceAutonome ? "Oui" : "Non"
formFields['specialiteExerceeEM']                                  = $qp016PE1.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp016PE1.info1.profession.partieProfessionSiAccesPartiel;

//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp016PE1.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp016PE1.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp016PE1.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp016PE1.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp016PE1.info4.diplome.dateReconnaissanceTitreFormationProfession;

formFields['intituleAutreTitreFormationProfession']                = $qp016PE1.autreDiplomePage.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp016PE1.autreDiplomePage.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp016PE1.autreDiplomePage.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp016PE1.autreDiplomePage.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp016PE1.autreDiplomePage.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

formFields['intituleTitreDiplomeSpecialisation']                   = $qp016PE1.specialiteDiplomePage.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp016PE1.specialiteDiplomePage.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp016PE1.specialiteDiplomePage.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp016PE1.specialiteDiplomePage.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp016PE1.specialiteDiplomePage.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp016PE1.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp016PE1.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNom']								   = $qp016PE1.info5.ordre.ordreProfessionnelNom != null ? $qp016PE1.info5.ordre.ordreProfessionnelNom : '';
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp016PE1.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp016PE1.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';
formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp016PE1.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp016PE1.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp016PE1.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp016PE1.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp016PE1.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp016PE1.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp016PE1.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp016PE1.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp016PE1.info5.ordre.ordreProfessionnelAdressePays != null ? $qp016PE1.info5.ordre.ordreProfessionnelAdressePays: ''); 

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp016PE1.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp016PE1.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp016PE1.info6.assuranceProfessionnelle.commentaires;
//Commentaires 

formFields['commentairesSignature']                                = $qp016PE1.commentairePage.commentaireGroupe.commentairesSignature;


//Signature

formFields['dateSignature']                                        = $qp016PE1.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp016PE1.info7.signature.attesteSignature;


formFields['prestation1Du']                     				   = '';
formFields['prestation1Au']                                        = '';
formFields['prestation2Du']                                        = '';
formFields['prestation2Au']                                        = '';
formFields['prestation3Du']                                        = '';
formFields['prestation3Au']                                        = '';
formFields['prestation4Du']                                        = '';
formFields['prestation4Au']                                        = '';
formFields['prestation5Du']                                        = '';
formFields['prestation5Au']                                        = '';
formFields['prestation6Du']                                        = '';
formFields['prestation6Au']                                        = '';


formFields['prestation1ActiviteExercee']                           = '';
formFields['prestation2ActiviteExercee']                           = '';
formFields['prestation3ActiviteExercee']                           = '';
formFields['prestation4ActiviteExercee']                           = '';
formFields['prestation5ActiviteExercee']                           = '';
formFields['prestation6ActiviteExercee']                           = '';



formFields['libelleProfession']				                       = "Anesthésie et réanimation"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp016PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp016PE1.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Anesthesie_reanimation.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationConformite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Anesthesie_reanimation_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Anesthésie et réanimation - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'Anesthésie et réanimation',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
