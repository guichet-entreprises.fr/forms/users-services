var formFields = {};

var civNomPrenom = $qp175PE3.info2.etatCivile.civilite + ' ' + $qp175PE3.info2.etatCivile.declarantNomNaissance + ' ' + $qp175PE3.info2.etatCivile.declarantPrenoms;


//Profession considérée

formFields['typesActes']                                           = $qp175PE3.info1.profession.typesActes;
formFields['lieuExercice']                                         = $qp175PE3.info1.profession.lieuExercice;
formFields['nomProfessionLPS']                                     = $qp175PE3.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp175PE3.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp175PE3.info1.profession.specialite;
formFields['listeActes']                                           = $qp175PE3.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp175PE3.info1.profession.exerciceAutonome;
formFields['specialiteExerceeEM']                                  = $qp175PE3.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp175PE3.info1.profession.partieProfessionSiAccesPartiel;

//Prestation de service

formFields['declarationInitialeLPS']                               = false;
formFields['renouvellementLPS']                                    = true;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp175PE3.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp175PE3.info2.etatCivile.civilite).eq('monsieur') ? '' : '----';
formFields['declarantNomNaissance']                                = $qp175PE3.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp175PE3.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp175PE3.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp175PE3.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp175PE3.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp175PE3.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp175PE3.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoVille']                           = $qp175PE3.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCourriel']                        = $qp175PE3.info3.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoie']                      = $qp175PE3.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp175PE3.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoCP']                              = $qp175PE3.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp175PE3.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp175PE3.info3.coordonnees1.declarantAdressePersoTelephone;

formFields['declarantAdresseFranceNumNomVoie']                     = $qp175PE3.info3.coordonnees2.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp175PE3.info3.coordonnees2.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceCP']                             = $qp175PE3.info3.coordonnees2.declarantAdresseFranceCP;
formFields['declarantAdresseFranceVille']                          = $qp175PE3.info3.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFrancePays']                           = $qp175PE3.info3.coordonnees2.declarantAdresseFrancePays;
formFields['declarantAdresseFranceTelephone']                      = $qp175PE3.info3.coordonnees2.declarantAdresseFranceTelephone;


//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp175PE3.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp175PE3.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp175PE3.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp175PE3.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp175PE3.info4.diplome.dateReconnaissanceTitreFormationProfession;

formFields['intituleAutreTitreFormationProfession']                = $qp175PE3.info4.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp175PE3.info4.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp175PE3.info4.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp175PE3.info4.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp175PE3.info4.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

formFields['intituleTitreDiplomeSpecialisation']                   = $qp175PE3.info4.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp175PE3.info4.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp175PE3.info4.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp175PE3.info4.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp175PE3.info4.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp175PE3.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp175PE3.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp175PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp175PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp175PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp175PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp175PE3.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp175PE3.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp175PE3.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp175PE3.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp175PE3.info5.ordre.ordreProfessionnelAdressePays != null ? $qp175PE3.info5.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp175PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp175PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp175PE3.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp175PE3.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp175PE3.info6.assuranceProfessionnelle.commentaires;


formFields['prestation1Du']                     				   = $qp175PE3.detailsRenouvellement.renouvellement.periodePreste1.from;
formFields['prestation1Au']                                        = $qp175PE3.detailsRenouvellement.renouvellement.periodePreste1.to;
formFields['prestation2Du']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste2.from : '');
formFields['prestation2Au']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste2.to : '');
formFields['prestation3Du']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste3.from : '');
formFields['prestation3Au']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste3.to : '');
formFields['prestation4Du']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste4.from : '');
formFields['prestation4Au']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste4.to : '');
formFields['prestation5Du']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste5.from : '');
formFields['prestation5Au']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste5.to : '');
formFields['prestation6Du']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste6.from : '');
formFields['prestation6Au']                                        = ($qp175PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp175PE3.detailsRenouvellement.renouvellement.periodePreste6.to : '');


formFields['prestation1ActiviteExercee']                           = $qp175PE3.detailsRenouvellement.renouvellement.prestation1ActiviteExercee;
formFields['prestation2ActiviteExercee']                           = $qp175PE3.detailsRenouvellement.renouvellement.prestation2ActiviteExercee;
formFields['prestation3ActiviteExercee']                           = $qp175PE3.detailsRenouvellement.renouvellement.prestation3ActiviteExercee;
formFields['prestation4ActiviteExercee']                           = $qp175PE3.detailsRenouvellement.renouvellement.prestation4ActiviteExercee;
formFields['prestation5ActiviteExercee']                           = $qp175PE3.detailsRenouvellement.renouvellement.prestation5ActiviteExercee;
formFields['prestation6ActiviteExercee']                           = $qp175PE3.detailsRenouvellement.renouvellement.prestation6ActiviteExercee;

 
 
//Signature

formFields['commentairesSignature']                                = $qp175PE3.info7.signature.commentairesSignature;
formFields['dateSignature']                                        = $qp175PE3.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp175PE3.info7.signature.attesteSignature;


formFields['libelleProfession']				= " Médecine nucléaire"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp175PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp175PE3.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Demande de renouvellement de la déclaration en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/medecine_nucleaire.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPrecedenteDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Medecine_nucleaire_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : ' Médecine nucléaire - renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de médecine nucléaire',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
