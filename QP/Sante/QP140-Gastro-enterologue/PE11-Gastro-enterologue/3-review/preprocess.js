function pad(s) { return (s < 10) ? '0' + s : s; } 
var cerfaFields = {};
//etatCivil
var civNomPrenom = $qp140PE11.etatCivil.identificationDeclarant.civilite + ' ' + $qp140PE11.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp140PE11.etatCivil.identificationDeclarant.prenomDeclarant;


/*******************************************************************************************************************************
 * Etat civil
 *******************************************************************************************************************************/
cerfaFields['nomDeclarant']                           = $qp140PE11.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']                        = $qp140PE11.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['prenomDeclarantComp']                    = $qp140PE11.etatCivil.identificationDeclarant.prenomDeclarantComp;
cerfaFields['nomUsageDeclarant']                      = $qp140PE11.etatCivil.identificationDeclarant.nomUsageDeclarant;
cerfaFields['nomExerciceDeclarant']                   = $qp140PE11.etatCivil.identificationDeclarant.nomExerciceDeclarant;
cerfaFields['prenomExerciceDeclarant']                = $qp140PE11.etatCivil.identificationDeclarant.prenomExerciceDeclarant;
cerfaFields['masculin']                               = $qp140PE11.etatCivil.identificationDeclarant.civilite == "Monsieur";
cerfaFields['feminin']                                = $qp140PE11.etatCivil.identificationDeclarant.civilite == "Madame";
cerfaFields['nationalite']                            = $qp140PE11.etatCivil.identificationDeclarant.nationaliteDeclarant;

var dateChamp = $qp140PE11.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateNaissanceJour'] = day;
	cerfaFields['dateNaissanceMois'] = month;
	cerfaFields['dateNaissanceAnnee'] = year;
}

cerfaFields['lieuNaissanceVille']                     = $qp140PE11.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['dateNaissanceDepartement']               = $qp140PE11.etatCivil.identificationDeclarant.departementNaissanceDeclarant;
cerfaFields['dateNaissancePays']                      = $qp140PE11.etatCivil.identificationDeclarant.paysNaissanceDeclarant;


/*******************************************************************************************************************************
 * Situation familiale
 *******************************************************************************************************************************/
cerfaFields['celibataire']                            = $qp140PE11.situation.familiale.statut == "Célibataire";
cerfaFields['mariee']                                 = $qp140PE11.situation.familiale.statut == "Marié(e)";
cerfaFields['pacs']                                   = $qp140PE11.situation.familiale.statut == "PACS";
cerfaFields['concubine']                              = $qp140PE11.situation.familiale.statut == "Concubin(e)";
cerfaFields['separee']                                = $qp140PE11.situation.familiale.statut == "Séparé(e)";
cerfaFields['divorce']                                = $qp140PE11.situation.familiale.statut == "Divorcé(e)";
cerfaFields['veuf']                                   = $qp140PE11.situation.familiale.statut == "Veuf(ve)";

cerfaFields['professionConjoint']                     = $qp140PE11.situation.familiale.professionConjoint;
cerfaFields['nombreEnfants']                          = $qp140PE11.situation.familiale.nombreEnfants != null ? $qp140PE11.situation.familiale.nombreEnfants + ' enfant(s)' : '';
cerfaFields['datesNaissanceEnfants']                  = $qp140PE11.situation.familiale.datesNaissanceEnfants;

/*******************************************************************************************************************************
 * langues parlées
 *******************************************************************************************************************************/
cerfaFields['languesParlees']                         = $qp140PE11.situation.langues.parlee;

/*******************************************************************************************************************************
 * adresse personnelle - Coordonnées
 *******************************************************************************************************************************/
cerfaFields['adresseDeclarant']                       = $qp140PE11.adresse.adresseContact.numeroLibelleAdresseDeclarant !=null ? $qp140PE11.adresse.adresseContact.numeroLibelleAdresseDeclarant :''
														+' '+ ($qp140PE11.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp140PE11.adresse.adresseContact.complementAdresseDeclarant : '')
														+' '+ ($qp140PE11.adresse.adresseContact.codePostalAdresseDeclarant != null ? ', ' + $qp140PE11.adresse.adresseContact.codePostalAdresseDeclarant : '')
														+' '+ $qp140PE11.adresse.adresseContact.villeAdresseDeclarant !=null ?  $qp140PE11.adresse.adresseContact.villeAdresseDeclarant :''
														+', '+ $qp140PE11.adresse.adresseContact.paysAdresseDeclarant !=null ?  $qp140PE11.adresse.adresseContact.paysAdresseDeclarant :'';
cerfaFields['telephoneFixe']                          = $qp140PE11.adresse.adresseContact.telephoneAdresseDeclarant != null ? $qp140PE11.adresse.adresseContact.telephoneAdresseDeclarant : '';
cerfaFields['telephoneMobile']                        = $qp140PE11.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['email']                                  = $qp140PE11.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['fax']                                    = $qp140PE11.adresse.adresseContact.faxAdresseDeclarant != null ? $qp140PE11.adresse.adresseContact.faxAdresseDeclarant : '';


/*******************************************************************************************************************************
 * Diplome de medecin
 *******************************************************************************************************************************/
cerfaFields['diplomeFr']                              = $qp140PE11.diplomeMedecin.etape1.paysDiplome == 'Français';
cerfaFields['diplomeEU']                              = $qp140PE11.diplomeMedecin.etape1.paysDiplome == 'Européen (d\'un Etat de l\'UE ou de l\'EEE)';
cerfaFields['diplomeSuisse']                          = $qp140PE11.diplomeMedecin.etape1.paysDiplome == 'Suisse';
cerfaFields['diplomeAutre']                           = $qp140PE11.diplomeMedecin.etape1.paysDiplome == 'Autre';

var dateChamp = $qp140PE11.diplomeMedecin.etape1.dateObtentionDiplome;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionJour'] = day;
	cerfaFields['dateObtentionMois'] = month;
	cerfaFields['dateObtentionAnnee'] = year;
}
cerfaFields['lieuObtention']                          = $qp140PE11.diplomeMedecin.etape1.facObtentionDiplome;
cerfaFields['paysObtention']                          = $qp140PE11.diplomeMedecin.etape1.paysObtentionDiplome;


/*******************************************************************************************************************************
 * Autorisation ministerielle
 *******************************************************************************************************************************/
cerfaFields['autorisationMinisterielleOui']           = true;

var dateChamp = $qp140PE11.autorisation.ministerielle.dateAutorisation;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateArreteAutorisationJour'] = day;
	cerfaFields['dateArreteAutorisationMois'] = month;
	cerfaFields['dateArreteAutorisationAnnee'] = year;
}


/*******************************************************************************************************************************
 * Spécialité obtenue dans un etat de l'UE ou de l'EEE ou en Suisse
 *******************************************************************************************************************************/
cerfaFields['specialiteObtenue1']                     = $qp140PE11.autorisation.specialiteObtenue.intituleSpecialite1;

var dateChamp = $qp140PE11.autorisation.specialiteObtenue.dateObtention1;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionSpecialite1Jour'] = day;
	cerfaFields['dateObtentionSpecialite1Mois'] = month;
	cerfaFields['dateObtentionSpecialite1Annee'] = year;
}

cerfaFields['paysObtentionSpecialite1']               = $qp140PE11.autorisation.specialiteObtenue.paysObtention1;
cerfaFields['universiteObtentionSpecialite1']         = $qp140PE11.autorisation.specialiteObtenue.universiteObtention1;

cerfaFields['specialiteObtenue2']                     = $qp140PE11.autorisation.specialiteObtenue.intituleSpecialite2 != null ? $qp140PE11.autorisation.specialiteObtenue.intituleSpecialite2 : '';

var dateChamp = $qp140PE11.autorisation.specialiteObtenue.dateObtention2;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionSpecialite2Jour'] = day;
	cerfaFields['dateObtentionSpecialite2Mois'] = month;
	cerfaFields['dateObtentionSpecialite2Annee'] = year;
}

cerfaFields['paysObtentionSpecialite2']               = $qp140PE11.autorisation.specialiteObtenue.paysObtention2 != null ? $qp140PE11.autorisation.specialiteObtenue.paysObtention2 : '';
cerfaFields['universiteObtentionSpecialite2']         = $qp140PE11.autorisation.specialiteObtenue.universiteObtention2 != null ? $qp140PE11.autorisation.specialiteObtenue.universiteObtention2 : '';


/*******************************************************************************************************************************
 * Qualification en médecine générale
 *******************************************************************************************************************************/
cerfaFields['universiteObtentionEEESuisseUE']         = $qp140PE11.qualification.medecineGenerale.universiteObtention;
cerfaFields['paysObtentionEEESuisseUE']               = $qp140PE11.qualification.medecineGenerale.paysObtention;

var dateChamp = $qp140PE11.qualification.medecineGenerale.dateObtention;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionEEESuisseUEJour'] = day;
	cerfaFields['dateObtentionEEESuisseUEMois'] = month;
	cerfaFields['dateObtentionEEESuisseUEAnnee'] = year;
}


/*******************************************************************************************************************************
 * Inscription ou enregistrement à un ordre ou organisme assimilé dans un etat membre de l’Union Europeenne ou partie a l’accord sur l’Espace Economique Europeen ou en Suisse
 *******************************************************************************************************************************/
cerfaFields['oui17']                                  = $qp140PE11.inscriptionOrdrePage.inscriptionOrdre.inscriptionOrdre == true;
cerfaFields['non17']                                  = $qp140PE11.inscriptionOrdrePage.inscriptionOrdre.inscriptionOrdre == false;
cerfaFields['etatsAnneesInscriptionEnregistrement17'] = $qp140PE11.inscriptionOrdrePage.inscriptionOrdre.inscriptionOrdreOui != null ? $qp140PE11.inscriptionOrdrePage.inscriptionOrdre.inscriptionOrdreOui : '';
cerfaFields['ouiRefus17']                             = $qp140PE11.inscriptionOrdrePage.inscriptionOrdre.inscriptionOrdreRejet == true;
cerfaFields['nonRefus17']                             = $qp140PE11.inscriptionOrdrePage.inscriptionOrdre.inscriptionOrdreRejet == false;
cerfaFields['etatsAnneesRefus17']                     = $qp140PE11.inscriptionOrdrePage.inscriptionOrdre.inscriptionOrdreRejetOui != null ? $qp140PE11.inscriptionOrdrePage.inscriptionOrdre.inscriptionOrdreRejetOui : '';


/*******************************************************************************************************************************
 * Inscription ou enregistrement à un ordre à l'étranger
 *******************************************************************************************************************************/
cerfaFields['ouiPaysEtranger18']                      = $qp140PE11.inscriptionOrdrePage.inscriptionOrdreEtranger.inscriptionOrdreEtranger == true;
cerfaFields['nonPaysEtranger18']                      = $qp140PE11.inscriptionOrdrePage.inscriptionOrdreEtranger.inscriptionOrdreEtranger == false;
cerfaFields['paysAnneeInscription18']                 = $qp140PE11.inscriptionOrdrePage.inscriptionOrdreEtranger.inscriptionOrdreEtrangerOui != null ? $qp140PE11.inscriptionOrdrePage.inscriptionOrdreEtranger.inscriptionOrdreEtrangerOui : '';
cerfaFields['ouiRefus18']                             = $qp140PE11.inscriptionOrdrePage.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejet == true;
cerfaFields['nonRefus18']                             = $qp140PE11.inscriptionOrdrePage.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejet == false;
cerfaFields['paysAnneeRefus18']                       = $qp140PE11.inscriptionOrdrePage.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejetOui != null ? $qp140PE11.inscriptionOrdrePage.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejetOui : '';


/*******************************************************************************************************************************
 * Prestation de services
 *******************************************************************************************************************************/
cerfaFields['prestationServiceOui']                   = $qp140PE11.inscriptionOrdrePage.prestationServices.enregistrement == true;
cerfaFields['prestationServiceNon']                   = $qp140PE11.inscriptionOrdrePage.prestationServices.enregistrement == false;
cerfaFields['anneePrestationServices19']              = $qp140PE11.inscriptionOrdrePage.prestationServices.anneeEnregistrement;
cerfaFields['enregistrementPrestationServices19']     = $qp140PE11.inscriptionOrdrePage.prestationServices.numeroEnregistrement;


/*******************************************************************************************************************************
 * Situation professionnelle dans le departement
 *******************************************************************************************************************************/
cerfaFields['medecineGenerale']                       = $qp140PE11.situationPro.dansDepartement.qualification == 'Médecine générale';
cerfaFields['autreSpecialite']                        = $qp140PE11.situationPro.dansDepartement.qualification == 'Autre spécialité';
cerfaFields['autreSpecialiteReponse']                 = $qp140PE11.situationPro.dansDepartement.autreSpecialitePrecision != null ? $qp140PE11.situationPro.dansDepartement.autreSpecialitePrecision: '';
cerfaFields['activiteMedicaleReguliere']              = $qp140PE11.situationPro.dansDepartement.activite == 'Activité médicale régulière';
cerfaFields['activiteMedicaleIntermittente']          = $qp140PE11.situationPro.dansDepartement.activite == 'Activité médicale intermittente ou remplacements réguliers';
cerfaFields['precisionLiberale']                      = Value('id').of($qp140PE11.situationPro.dansDepartement.activiteIntermittentePrecision).contains('liberal');
cerfaFields['precisionSalariee']                      = Value('id').of($qp140PE11.situationPro.dansDepartement.activiteIntermittentePrecision).contains('salariee');
cerfaFields['aucuneActiviteMedicale']                 = $qp140PE11.situationPro.dansDepartement.activite == 'Aucune activité médicale';


/*******************************************************************************************************************************
 * Coordonnées de l'activité principale (la plus importante en temps)
 *******************************************************************************************************************************/
cerfaFields['adresseActivitePrincipaleLiberal']       = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.typeActivite).eq('liberal') ? true : false;
cerfaFields['adresseActivitePrincipaleHospitalier']   = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['adresseActivitePrincipaleSalarie']       = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.typeActivite).eq('salarie') ? true : false;
cerfaFields['adresseActivitePrincipaleBenevole']      = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.typeActivite).eq('benevole') ? true : false;
cerfaFields['adresseActivitePrincipaleTempsPlein']    = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['adresseActivitePrincipaleTempsPartiel']  = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPartiel') ? true : false;
cerfaFields['fonctionActivitePrincipale']             = $qp140PE11.activitePrincipale.adresseActivite.fonction;
cerfaFields['statutActivitePrincipale']               = $qp140PE11.activitePrincipale.adresseActivite.statut;

var dateChamp = $qp140PE11.activitePrincipale.adresseActivite.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['jourDebutActivitePrincipale'] = day;
	cerfaFields['moisDebutActivitePrincipale'] = month;
	cerfaFields['anneeebutActivitePrincipale'] = year;
}
cerfaFields['autreLieuActiviteDS0']                   = $qp140PE11.activitePrincipale.adresseActivite.denominationSociale;
//------------------------------------------------------------------------------------------------------------------------------
cerfaFields['autreLieuActiviteAdresse0']              = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.numeroLibelleAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.numeroLibelleAdresseDeclarant  :''
														+', '+ ($qp140PE11.activitePrincipale.adresseActivite.adressePostale.complementAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.complementAdresseDeclarant : '') 
														+', '+ ($qp140PE11.activitePrincipale.adresseActivite.adressePostale.codePostalAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.codePostalAdresseDeclarant : '')
														+' '+ $qp140PE11.activitePrincipale.adresseActivite.adressePostale.villeAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.villeAdresseDeclarant :''
														+', '+ $qp140PE11.activitePrincipale.adresseActivite.adressePostale.paysAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.paysAdresseDeclarant :'';
cerfaFields['autreLieuActiviteTel0']            	  = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.telephoneAdresseDeclarant;
cerfaFields['autreLieuActiviteFax0']               	  = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteMobile0']               = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.faxAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail0']                = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.mailAdresseDeclarant;
 
 for (var i=0; i<7; i++){
	cerfaFields['autreLieuActiviteLiberal'+i] 						='';
	cerfaFields['autreLieuActiviteHospitalier'+i]					='';
	cerfaFields['autreLieuActiviteSalarie'+i]						='';
	cerfaFields['autreLieuActiviteBenevole'+i]						='';				
	cerfaFields['autreLieuActiviteJour'+i]							='';
	cerfaFields['autreLieuActiviteMois'+i]							='';
	cerfaFields['autreLieuActiviteAnnee'+i]							='';
	cerfaFields['autreLieuActiviteTempsPlein'+i]					='';	
	cerfaFields['autreLieuActiviteTempsPartiel'+i]					='';
	cerfaFields['autreLieuActiviteFonction'+i]						='';
	cerfaFields['autreLieuActiviteStatut'+i]						='';
	cerfaFields['autreLieuActiviteDS'+i]							='';	
	cerfaFields['autreLieuActiviteAdresse'+i]						='';
	cerfaFields['autreLieuActiviteTel'+i]							='';
	cerfaFields['autreLieuActiviteFax'+i]                  			='';	
	cerfaFields['autreLieuActiviteMobile'+i]               			='';
	cerfaFields['autreLieuActiviteEmail'+i]							='';
 }
 
 
 /*******************************************************************************************************************************
  * Autre lieux d'activités
  *******************************************************************************************************************************/
//if($qp140PE11.situationPro.dansDepartement.activite =='Activité médicale régulière'){
cerfaFields['autreLieuActiviteLiberal0']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier0']          = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie0']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole0']             = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp140PE11.activitePrincipale.adresseActivite1.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour0'] = day;
	cerfaFields['autreLieuActiviteMois0'] = month;
	cerfaFields['autreLieuActiviteAnnee0'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein0']           = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel0']         = Value('id').of($qp140PE11.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction0']             = $qp140PE11.activitePrincipale.adresseActivite.fonction != null ? $qp140PE11.activitePrincipale.adresseActivite.fonction : '';
cerfaFields['autreLieuActiviteStatut0']               = $qp140PE11.activitePrincipale.adresseActivite.statut != null ? $qp140PE11.activitePrincipale.adresseActivite.statut : '';
cerfaFields['autreLieuActiviteDS0']                   = $qp140PE11.activitePrincipale.adresseActivite.denominationSociale != null ? $qp140PE11.activitePrincipale.adresseActivite.denominationSociale : '';


cerfaFields['autreLieuActiviteAdresse0']              = $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.numeroLibelleAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.numeroLibelleAdresseDeclarant :'' 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite.adressePostale.complementAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite.adressePostale.codePostalAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp140PE11.activitePrincipale.adresseActivite.adressePostale.villeAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.villeAdresseDeclarant : ''
														 +', '+ $qp140PE11.activitePrincipale.adresseActivite.adressePostale.paysAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.paysAdresseDeclarant :'';
cerfaFields['autreLieuActiviteTel0']                  = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.telephoneAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax0']                  = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.faxAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite.adressePostale.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile0']               = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail0']                = $qp140PE11.activitePrincipale.adresseActivite.adressePostale.mailAdresseDeclarant;
//}

/*******************************************************************************************************************************
 * 
 *******************************************************************************************************************************/
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
//if($qp140PE11.activitePrincipale.adresseActivite.autreLieuActivite1){
cerfaFields['autreLieuActiviteLiberal1']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite1.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier1']          = Value('id').of($qp140PE11.activitePrincipale.adresseActivite1.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie1']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite1.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole1']             = Value('id').of($qp140PE11.activitePrincipale.adresseActivite1.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp140PE11.activitePrincipale.adresseActivite1.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour1'] = day;
	cerfaFields['autreLieuActiviteMois1'] = month;
	cerfaFields['autreLieuActiviteAnnee1'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein1']           = Value('id').of($qp140PE11.activitePrincipale.adresseActivite1.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel1']         = Value('id').of($qp140PE11.activitePrincipale.adresseActivite1.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction1']             = $qp140PE11.activitePrincipale.adresseActivite1.fonction != null ? $qp140PE11.activitePrincipale.adresseActivite1.fonction : '';
cerfaFields['autreLieuActiviteStatut1']               = $qp140PE11.activitePrincipale.adresseActivite1.statut != null ? $qp140PE11.activitePrincipale.adresseActivite1.statut : '';
cerfaFields['autreLieuActiviteDS1']                   = $qp140PE11.activitePrincipale.adresseActivite1.denominationSociale != null ? $qp140PE11.activitePrincipale.adresseActivite1.denominationSociale : '';


cerfaFields['autreLieuActiviteAdresse1']              = $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.numeroLibelleAdresseDeclarant != null ?$qp140PE11.activitePrincipale.adresseActivite1.adressePostale.numeroLibelleAdresseDeclarant :''
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite1.adressePostale.complementAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite1.adressePostale.codePostalAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.villeAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.villeAdresseDeclarant : ''
														 +', '+ $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.paysAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.paysAdresseDeclarant :'';
cerfaFields['autreLieuActiviteTel1']                  = $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.telephoneAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite1.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax1']                  = $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.faxAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite1.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile1']               = $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail1']                = $qp140PE11.activitePrincipale.adresseActivite1.adressePostale.mailAdresseDeclarant;
//}


/*******************************************************************************************************************************
 * 
 *******************************************************************************************************************************/
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
//if($qp140PE11.activitePrincipale.adresseActivite1.autreLieuActivite2){
cerfaFields['autreLieuActiviteLiberal2']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite2.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier2']          = Value('id').of($qp140PE11.activitePrincipale.adresseActivite2.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie2']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite2.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole2']             = Value('id').of($qp140PE11.activitePrincipale.adresseActivite2.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp140PE11.activitePrincipale.adresseActivite3.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour2'] = day;
	cerfaFields['autreLieuActiviteMois2'] = month;
	cerfaFields['autreLieuActiviteAnnee2'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein2']           = Value('id').of($qp140PE11.activitePrincipale.adresseActivite2.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel2']         = Value('id').of($qp140PE11.activitePrincipale.adresseActivite2.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction2']             = $qp140PE11.activitePrincipale.adresseActivite2.fonction != null ? $qp140PE11.activitePrincipale.adresseActivite2.fonction : '';
cerfaFields['autreLieuActiviteStatut2']               = $qp140PE11.activitePrincipale.adresseActivite2.statut != null ? $qp140PE11.activitePrincipale.adresseActivite2.statut : '';
cerfaFields['autreLieuActiviteDS2']                   = $qp140PE11.activitePrincipale.adresseActivite2.denominationSociale != null ? $qp140PE11.activitePrincipale.adresseActivite2.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse2']              = $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.numeroLibelleAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.numeroLibelleAdresseDeclarant :''
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite2.adressePostale.complementAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite2.adressePostale.codePostalAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.villeAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.villeAdresseDeclarant :''
														 +', '+ $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.paysAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.paysAdresseDeclarant : '';
cerfaFields['autreLieuActiviteTel2']                  = $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.telephoneAdresseDeclarant;
cerfaFields['autreLieuActiviteFax2']                  = $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.faxAdresseDeclarant ;
cerfaFields['autreLieuActiviteMobile2']               = $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail2']                = $qp140PE11.activitePrincipale.adresseActivite2.adressePostale.mailAdresseDeclarant;
//}

/*******************************************************************************************************************************
 * 
 *******************************************************************************************************************************/
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
//if($qp140PE11.activitePrincipale.adresseActivite2.autreLieuActivite3){
cerfaFields['autreLieuActiviteLiberal3']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite3.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier3']          = Value('id').of($qp140PE11.activitePrincipale.adresseActivite3.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie3']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite3.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole3']             = Value('id').of($qp140PE11.activitePrincipale.adresseActivite3.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp140PE11.activitePrincipale.adresseActivite3.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour3'] = day;
	cerfaFields['autreLieuActiviteMois3'] = month;
	cerfaFields['autreLieuActiviteAnnee3'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein3']           = Value('id').of($qp140PE11.activitePrincipale.adresseActivite3.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel3']         = Value('id').of($qp140PE11.activitePrincipale.adresseActivite3.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction3']             = $qp140PE11.activitePrincipale.adresseActivite3.fonction != null ? $qp140PE11.activitePrincipale.adresseActivite3.fonction : '';
cerfaFields['autreLieuActiviteStatut3']               = $qp140PE11.activitePrincipale.adresseActivite3.statut != null ? $qp140PE11.activitePrincipale.adresseActivite3.statut : '';
cerfaFields['autreLieuActiviteDS3']                   = $qp140PE11.activitePrincipale.adresseActivite3.denominationSociale != null ? $qp140PE11.activitePrincipale.adresseActivite3.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse3']              = $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.numeroLibelleAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.numeroLibelleAdresseDeclarant :'' 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite3.adressePostale.complementAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite3.adressePostale.codePostalAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.villeAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.villeAdresseDeclarant :''
														 +', '+ $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.paysAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.paysAdresseDeclarant :'';
cerfaFields['autreLieuActiviteTel3']                  = $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.telephoneAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite3.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax3']                  = $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.faxAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite3.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile3']               = $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail3']                = $qp140PE11.activitePrincipale.adresseActivite3.adressePostale.mailAdresseDeclarant;
//}

/*******************************************************************************************************************************
 * 
 *******************************************************************************************************************************/
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
//if($qp140PE11.activitePrincipale.adresseActivite3.autreLieuActivite4){
cerfaFields['autreLieuActiviteLiberal4']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite4.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier4']          = Value('id').of($qp140PE11.activitePrincipale.adresseActivite4.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie4']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite4.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole4']             = Value('id').of($qp140PE11.activitePrincipale.adresseActivite4.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp140PE11.activitePrincipale.adresseActivite4.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour4'] = day;
	cerfaFields['autreLieuActiviteMois4'] = month;
	cerfaFields['autreLieuActiviteAnnee4'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein4']           = Value('id').of($qp140PE11.activitePrincipale.adresseActivite4.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel4']         = Value('id').of($qp140PE11.activitePrincipale.adresseActivite4.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction4']             = $qp140PE11.activitePrincipale.adresseActivite4.fonction != null ? $qp140PE11.activitePrincipale.adresseActivite4.fonction : '';
cerfaFields['autreLieuActiviteStatut4']               = $qp140PE11.activitePrincipale.adresseActivite4.statut != null ? $qp140PE11.activitePrincipale.adresseActivite4.statut : '';
cerfaFields['autreLieuActiviteDS4']                   = $qp140PE11.activitePrincipale.adresseActivite4.denominationSociale != null ? $qp140PE11.activitePrincipale.adresseActivite4.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse4']              = $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.numeroLibelleAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.numeroLibelleAdresseDeclarant :''
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite4.adressePostale.complementAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite4.adressePostale.codePostalAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.villeAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.villeAdresseDeclarant : ''
														 +', '+ $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.paysAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.paysAdresseDeclarant : '';
cerfaFields['autreLieuActiviteTel4']                  = $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.telephoneAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite4.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax4']                  = $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.faxAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite4.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile4']               = $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail4']                = $qp140PE11.activitePrincipale.adresseActivite4.adressePostale.mailAdresseDeclarant;
//}

/*******************************************************************************************************************************
 * 
 *******************************************************************************************************************************/
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
//if($qp140PE11.activitePrincipale.adresseActivite4.autreLieuActivite5){
cerfaFields['autreLieuActiviteLiberal5']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite5.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier5']          = Value('id').of($qp140PE11.activitePrincipale.adresseActivite5.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie5']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite5.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole5']             = Value('id').of($qp140PE11.activitePrincipale.adresseActivite5.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp140PE11.activitePrincipale.adresseActivite5.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour5'] = day;
	cerfaFields['autreLieuActiviteMois5'] = month;
	cerfaFields['autreLieuActiviteAnnee5'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein5']           = Value('id').of($qp140PE11.activitePrincipale.adresseActivite5.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel5']         = Value('id').of($qp140PE11.activitePrincipale.adresseActivite5.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction5']             = $qp140PE11.activitePrincipale.adresseActivite5.fonction != null ? $qp140PE11.activitePrincipale.adresseActivite5.fonction : '';
cerfaFields['autreLieuActiviteStatut5']               = $qp140PE11.activitePrincipale.adresseActivite5.statut != null ? $qp140PE11.activitePrincipale.adresseActivite5.statut : '';
cerfaFields['autreLieuActiviteDS5']                   = $qp140PE11.activitePrincipale.adresseActivite5.denominationSociale != null ? $qp140PE11.activitePrincipale.adresseActivite5.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse5']              = $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.numeroLibelleAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.numeroLibelleAdresseDeclarant :'' 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite5.adressePostale.complementAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp140PE11.activitePrincipale.adresseActivite5.adressePostale.codePostalAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.villeAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.villeAdresseDeclarant : ''
														 +', '+ $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.paysAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.paysAdresseDeclarant : '';
cerfaFields['autreLieuActiviteTel5']                  = $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.telephoneAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite5.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax5']                  = $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.faxAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite5.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile5']               = $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail5']                = $qp140PE11.activitePrincipale.adresseActivite5.adressePostale.mailAdresseDeclarant;
//}

/*******************************************************************************************************************************
 * 
 *******************************************************************************************************************************/
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
//if($qp140PE11.activitePrincipale.adresseActivite5.autreLieuActivite6){
cerfaFields['autreLieuActiviteLiberal6']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite6.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier6']          = Value('id').of($qp140PE11.activitePrincipale.adresseActivite6.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie6']              = Value('id').of($qp140PE11.activitePrincipale.adresseActivite6.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole6']             = Value('id').of($qp140PE11.activitePrincipale.adresseActivite6.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp140PE11.activitePrincipale.adresseActivite6.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour6'] = day;
	cerfaFields['autreLieuActiviteMois6'] = month;
	cerfaFields['autreLieuActiviteAnnee6'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein6']           = Value('id').of($qp140PE11.activitePrincipale.adresseActivite6.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel6']         = Value('id').of($qp140PE11.activitePrincipale.adresseActivite6.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction6']             = $qp140PE11.activitePrincipale.adresseActivite6.fonction != null ? $qp140PE11.activitePrincipale.adresseActivite6.fonction : '';
cerfaFields['autreLieuActiviteStatut6']               = $qp140PE11.activitePrincipale.adresseActivite6.statut != null ? $qp140PE11.activitePrincipale.adresseActivite6.statut : '';
cerfaFields['autreLieuActiviteDS6']                   = $qp140PE11.activitePrincipale.adresseActivite6.denominationSociale != null ? $qp140PE11.activitePrincipale.adresseActivite6.denominationSociale : '';

cerfaFields['autreLieuActiviteAdresse6']              = $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.numeroLibelleAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.numeroLibelleAdresseDeclarant :'' 
	 +', '+ ($qp140PE11.activitePrincipale.adresseActivite6.adressePostale.complementAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.complementAdresseDeclarant : '') 
	 +', '+ ($qp140PE11.activitePrincipale.adresseActivite6.adressePostale.codePostalAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.codePostalAdresseDeclarant : '')
	 +' '+ $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.villeAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.villeAdresseDeclarant : ''
	 +', '+ $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.paysAdresseDeclarant !=null ? $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.paysAdresseDeclarant : '';
cerfaFields['autreLieuActiviteTel6']                  = $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.telephoneAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite6.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax6']                  = $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.faxAdresseDeclarant != null ? $qp140PE11.activitePrincipale.adresseActivite6.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile6']               = $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail6']                = $qp140PE11.activitePrincipale.adresseActivite6.adressePostale.mailAdresseDeclarant;
//}

/*******************************************************************************************************************************
 * Plaques, ordonnances et annuaires
 *******************************************************************************************************************************/

cerfaFields['libellePlaques']                         = $qp140PE11.plaquesOrdonnances.etape.plaques;
cerfaFields['libelleOrdonnances']                     = $qp140PE11.plaquesOrdonnances.etape.ordonnances;
cerfaFields['annuaires']                              = $qp140PE11.plaquesOrdonnances.etape.annuaires;


/*******************************************************************************************************************************
 * Adresse de correspondance
 *******************************************************************************************************************************/
cerfaFields['adresseCorrespondancePrincipale1']       = Value('id').of($qp140PE11.adressesCorrespondance.adresse.courrier).eq('adresseExercicePrincipal') ? true : false;
cerfaFields['adresseCorrespondancePersonnelle1']      = Value('id').of($qp140PE11.adressesCorrespondance.adresse.courrier).eq('adressePersonnelle') ? true : false;
cerfaFields['adresseCorrespondanceAutre1']            = Value('id').of($qp140PE11.adressesCorrespondance.adresse.courrier).eq('adresseAutre') ? true : false;
cerfaFields['adresseCorrespondanceAutrePrecision1']   = $qp140PE11.adressesCorrespondance.adresse.precisionCourrier != null ? $qp140PE11.adressesCorrespondance.adresse.precisionCourrier : '';

cerfaFields['adresseCorrespondancePrincipale2']       = Value('id').of($qp140PE11.adressesCorrespondance.adresse.rpps).eq('adresseExercicePrincipal') ? true : false;
cerfaFields['adresseCorrespondancePersonnelle2']      = Value('id').of($qp140PE11.adressesCorrespondance.adresse.rpps).eq('adressePersonnelle') ? true : false;
cerfaFields['adresseCorrespondanceAutre2']            = Value('id').of($qp140PE11.adressesCorrespondance.adresse.rpps).eq('adresseAutre') ? true : false;
cerfaFields['adresseCorrespondanceAutrePrecision2']   = $qp140PE11.adressesCorrespondance.adresse.precisionRPPS != null ? $qp140PE11.adressesCorrespondance.adresse.precisionRPPS : '';

cerfaFields['adresseCorrespondanceEmail']             = $qp140PE11.adressesCorrespondance.adresse.email;


/*******************************************************************************************************************************
 * Sanctions prononcées hors France
 *******************************************************************************************************************************/
cerfaFields['sanctionsPrononceeOui']                  = $qp140PE11.sanctions.instances.caractereDefinitif == true;
cerfaFields['sanctionsPrononceeNon']                  = $qp140PE11.sanctions.instances.caractereDefinitif == false;
cerfaFields['sanctionsPrononceeDescription']          = $qp140PE11.sanctions.instances.sanctionsDescription;
cerfaFields['sanctionsPrononceepays']                 = $qp140PE11.sanctions.instances.sanctionsPays;
cerfaFields['sanctionsPrononceeJuridictions']         = $qp140PE11.sanctions.instances.sanctionsJuridiction;
cerfaFields['sanctionsPrononceeDates']                = $qp140PE11.sanctions.instances.sanctionsDates;

cerfaFields['condamnationPenaleOui']                  = $qp140PE11.sanctions.instances.condamnation == true;
cerfaFields['condamnationPenaleNon']                  = $qp140PE11.sanctions.instances.condamnation == false;
cerfaFields['condamnationPenaleDescription']          = $qp140PE11.sanctions.instances.condamnationDescription;
cerfaFields['condamnationPenalePays']                 = $qp140PE11.sanctions.instances.condamnationPays;
cerfaFields['condamnationPenaleJuridictions']         = $qp140PE11.sanctions.instances.condamnationJuridiction;
cerfaFields['condamnationPenaleDates']                = $qp140PE11.sanctions.instances.condamnationDates;


/*******************************************************************************************************************************
 * Instances en cours à l'étranger
 *******************************************************************************************************************************/
cerfaFields['instanceJudiciaireOui']                  = $qp140PE11.sanctions.typesInstances.instanceJuriciaire == true;
cerfaFields['instanceJudiciaireNon']                  = $qp140PE11.sanctions.typesInstances.instanceJuriciaire == false;
cerfaFields['instanceJudiciairePays']                 = $qp140PE11.sanctions.typesInstances.instancePays;
cerfaFields['instanceJudiciaireJuridictions']         = $qp140PE11.sanctions.typesInstances.instanceJuridiction;

cerfaFields['signatureHonneurCodeDeontologie']        = true;
cerfaFields['signatureHonneurAssurance']              = true;
cerfaFields['inscriptionOrdreDepartement']            = $qp140PE11.signature.signature.departementExercice;

var dateChamp = $qp140PE11.signature.signature.dateSignature;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear().toString();
	cerfaFields['inscriptionOrdreDepartementJour'] 	= day;
	cerfaFields['inscriptionOrdreDepartementMois'] 	= month;
	cerfaFields['inscriptionOrdreDepartementAnnee'] = year;
}

cerfaFields['luApprouve']                             = 'Lu et approuvé';
cerfaFields['signature']                              = civNomPrenom;
cerfaFields['oppositionTransmissionDonnees']          = $qp140PE11.signature.signature.signatureOpposition1;
cerfaFields['oppositionCoordonneesPro']               = $qp140PE11.signature.signature.signatureOpposition2;


cerfaFields['libelleProfession']					  = "Gastro-entérologie et hépatologie";

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp140PE11.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp140PE11.signature.signature.dateSignature,
		autoriteHabilitee :"Président du Conseil départemental de l’Ordre des Médecins",
		demandeContexte : "Demande d'inscription au tableau de l’Ordre des Médecins",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/formulaire inscription ordre.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjContrats);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjNotification);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJournal);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Gastro-enterologue_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Gastro-entérologie et hépatologie - Inscription au tableau de l\'Ordre des Médecins',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\'inscription au tableau de l\'Ordre des Médecins',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});