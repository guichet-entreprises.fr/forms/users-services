var formFields = {};

var civNomPrenom = $qp015PE2.info2.etatCivile.civilite + ' ' + $qp015PE2.info2.etatCivile.declarantNomNaissance + ' ' + $qp015PE2.info2.etatCivile.declarantPrenoms;


//Prestation de service

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp015PE2.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp015PE2.info2.etatCivile.civilite).eq('monsieur') ? '' : '-----';
formFields['declarantNomNaissance']                                = $qp015PE2.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp015PE2.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp015PE2.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp015PE2.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp015PE2.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp015PE2.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp015PE2.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoNumNomVoie']                      = $qp015PE2.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp015PE2.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoVille']                      	   = $qp015PE2.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCP']                              = $qp015PE2.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp015PE2.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp015PE2.info3.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp015PE2.info3.coordonnees1.declarantAdressePersoCourriel;

//Coordonnées en France
formFields['declarantAdresseFranceNumNomVoie']                     = $qp015PE2.coordonneesFrancePage.coordonnees2.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp015PE2.coordonneesFrancePage.coordonnees2.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceVille']                          = $qp015PE2.coordonneesFrancePage.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFranceCP']                             = $qp015PE2.coordonneesFrancePage.coordonnees2.declarantAdresseFranceCP;
formFields['declarantAdresseFranceTelephone']                      = $qp015PE2.coordonneesFrancePage.coordonnees2.declarantAdresseFranceTelephone;

//Profession considérée

formFields['typesActes']                                           = $qp015PE2.info1.profession.typesActes;
formFields['lieuExercice']                                         = $qp015PE2.info1.profession.lieuExercice;
formFields['nomProfessionLPS']                                     = $qp015PE2.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp015PE2.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp015PE2.info1.profession.specialite;
formFields['listeActes']                                           = $qp015PE2.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp015PE2.info1.profession.exerciceAutonome ? "Oui" : "Non"
formFields['specialiteExerceeEM']                                  = $qp015PE2.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp015PE2.info1.profession.partieProfessionSiAccesPartiel;

//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp015PE2.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp015PE2.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp015PE2.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp015PE2.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp015PE2.info4.diplome.dateReconnaissanceTitreFormationProfession;

formFields['intituleAutreTitreFormationProfession']                = $qp015PE2.autreDiplomePage.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp015PE2.autreDiplomePage.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp015PE2.autreDiplomePage.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp015PE2.autreDiplomePage.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp015PE2.autreDiplomePage.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

formFields['intituleTitreDiplomeSpecialisation']                   = $qp015PE2.specialiteDiplomePage.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp015PE2.specialiteDiplomePage.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp015PE2.specialiteDiplomePage.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp015PE2.specialiteDiplomePage.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp015PE2.specialiteDiplomePage.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp015PE2.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp015PE2.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNom']								   = $qp015PE2.info5.ordre.ordreProfessionnelNom != null ? $qp015PE2.info5.ordre.ordreProfessionnelNom : '';
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp015PE2.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp015PE2.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';
formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp015PE2.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp015PE2.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp015PE2.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp015PE2.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp015PE2.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp015PE2.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp015PE2.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp015PE2.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp015PE2.info5.ordre.ordreProfessionnelAdressePays != null ? $qp015PE2.info5.ordre.ordreProfessionnelAdressePays: ''); 

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp015PE2.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp015PE2.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp015PE2.info6.assuranceProfessionnelle.commentaires;
//Commentaires 

formFields['commentairesSignature']                                = $qp015PE2.commentairePage.commentaireGroupe.commentairesSignature;


//Signature

formFields['dateSignature']                                        = $qp015PE2.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp015PE2.info7.signature.attesteSignature;


formFields['prestation1Du']                     				   = '';
formFields['prestation1Au']                                        = '';
formFields['prestation2Du']                                        = '';
formFields['prestation2Au']                                        = '';
formFields['prestation3Du']                                        = '';
formFields['prestation3Au']                                        = '';
formFields['prestation4Du']                                        = '';
formFields['prestation4Au']                                        = '';
formFields['prestation5Du']                                        = '';
formFields['prestation5Au']                                        = '';
formFields['prestation6Du']                                        = '';
formFields['prestation6Au']                                        = '';


formFields['prestation1ActiviteExercee']                           = '';
formFields['prestation2ActiviteExercee']                           = '';
formFields['prestation3ActiviteExercee']                           = '';
formFields['prestation4ActiviteExercee']                           = '';
formFields['prestation5ActiviteExercee']                           = '';
formFields['prestation6ActiviteExercee']                           = '';


formFields['libelleProfession']				                       = "Anatomie et cytologie pathologiques"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp015PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp015PE2.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Anatomie_cytologie.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEquivalentDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTraductionDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjObligationsCommunautaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Anatomie_cytologie_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Anatomie et cytologie - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'anatomie et cytologie',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
