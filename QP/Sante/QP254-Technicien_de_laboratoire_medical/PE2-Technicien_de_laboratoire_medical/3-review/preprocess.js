var formFields = {};

var civNomPrenom = $qp254PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp254PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp254PE2.etatCivil.identificationDeclarant.prenomDeclarant;

//Technicien de laboratoire
formFields['aideSoignant']                                        = false;
formFields['ambulancier']                                         = false;
formFields['assistantDentaire']                                   = false;
formFields['audioprothesiste']                                    = false;
formFields['auxiliairePuericulture']                              = false;
formFields['conseillerGenetique']                                 = false;
formFields['dieteticien']                                         = false;
formFields['ergotherapeuthe']                                     = false;
formFields['infirmier']                                           = false;
formFields['infirmierSpecialise']                                 = false;
formFields['infirmierSpecialiseAnesthesiste']                     = false;
formFields['infirmierSpecialiseBlocOperatoire']                   = false;
formFields['infirmierSpecialisePuericultrice']                    = false;
formFields['manipulateurElectroradiologie']                       = false;
formFields['masseurKinesitherapeute']                             = false;
formFields['opticienLunetier']                                    = false;
formFields['orthophoniste']                                       = false;
formFields['orthoptiste']                                         = false;
formFields['pedicurePodologue']                                   = false;
formFields['preparateurPharmacie']                                = false;
formFields['preparateurPharmacieHospitaliere']                    = false;
formFields['professionsAppareillage']                             = false;
formFields['oculariste']                                          = false;
formFields['orthopedisteOrthesiste']                              = false;
formFields['podoOrthesiste']                                      = false;
formFields['orthoprothesiste']                                    = false;
formFields['epithesiste']                                         = false;
formFields['psychomotricien']                                     = false;
formFields['radioPhysicienMedical']                               = false;
formFields['technicienLaboratoire']                               = true;

//etatCivil
formFields['civiliteMonsieur']                                    = ($qp254PE2.etatCivil.identificationDeclarant.civilite=='Monsieur');
formFields['civiliteMadame']                                      = ($qp254PE2.etatCivil.identificationDeclarant.civilite=='Madame');
formFields['declarantNomUsage']                                   = $qp254PE2.etatCivil.identificationDeclarant.nomEpouseDeclarant;
formFields['declarantNomNaissance']                               = $qp254PE2.etatCivil.identificationDeclarant.nomDeclarant;
formFields['declarantPrenoms']                                    = $qp254PE2.etatCivil.identificationDeclarant.prenomDeclarant;
formFields['declarantDateNaissance']                              = $qp254PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
formFields['declarantLieuNaissance']                              = $qp254PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
formFields['declarantPaysNaissance']                              = $qp254PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
formFields['declarantNationalite']                                = $qp254PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
formFields['declarantCourriel']                                   = $qp254PE2.adresse.adressePersonnelle.mailAdresseDeclarant;


//adresse
formFields['declarantAdresseRueComplement']                       = $qp254PE2.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant 
																	+ ($qp254PE2.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ' ' + $qp254PE2.adresse.adressePersonnelle.complementAdresseDeclarant : '');
formFields['declarantAdresseVille']                               = $qp254PE2.adresse.adressePersonnelle.villeAdresseDeclarant;
formFields['declarantAdressePays']                                = $qp254PE2.adresse.adressePersonnelle.paysAdresseDeclarant;
formFields['declarantAdresseCP']                                  = $qp254PE2.adresse.adressePersonnelle.codePostalAdresseDeclarant;
formFields['declarantTelephoneFixe']                              = $qp254PE2.adresse.adressePersonnelle.telephoneAdresseDeclarant;
formFields['declarantTelephoneMobile']                            = $qp254PE2.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;


//diplomeProfession
formFields['declarantDiplomeIntitule']                            = $qp254PE2.diplomeProfession.diplomeProfession.intituleDiplome;
formFields['declarantDiplomePaysObtention']                       = $qp254PE2.diplomeProfession.diplomeProfession.paysObtentionDiplome;
formFields['declarantDiplomeDelivrePar']                          = $qp254PE2.diplomeProfession.diplomeProfession.delivreDiplome;
formFields['declarantDiplomeDateObtention']                       = $qp254PE2.diplomeProfession.diplomeProfession.dateObtentionDiplome;
formFields['declarantDiplomeDateReconnaissance']                  = $qp254PE2.diplomeProfession.diplomeProfession.dateReconnaissanceDiplomeEtat;	

//diplomeDetailles
formFields['diplomeIntitule1']                                    = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome1;
formFields['diplomeDateObtention1']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome1;
formFields['diplomeLieuFormation1']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome1;
formFields['diplomePaysObtention1']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome1;

formFields['diplomeIntitule2']                                    = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome2;
formFields['diplomeDateObtention2']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome2;
formFields['diplomeLieuFormation2']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome2;
formFields['diplomePaysObtention2']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome2; 

formFields['diplomeIntitule3']                                    = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome3;
formFields['diplomeDateObtention3']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome3;
formFields['diplomeLieuFormation3']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome3;
formFields['diplomePaysObtention3']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome3;

formFields['diplomeIntitule4']                                    = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome4;
formFields['diplomeDateObtention4']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome4;
formFields['diplomeLieuFormation4']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome4;
formFields['diplomePaysObtention4']                               = $qp254PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome4;


//exerciceProfessionnel
formFields['exerciceProfessionnelNature1']                        = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurNom1']                  = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement1'] = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurAdresseVillePays1']     = ($qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 != null ? $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 + ' ' : '')
																	+ ($qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 != null ? $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 : '');
formFields['exerciceProfessionnelPeriode1']                       = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1;

formFields['exerciceProfessionnelNature2']                        = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurNom2']                  = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement2'] = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurAdresseVillePays2']     = ($qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 != null ? $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 + ' ' : '')
																	+ ($qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 != null ? $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 : '');
formFields['exerciceProfessionnelPeriode2']                       = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2;

formFields['exerciceProfessionnelNature3']                        = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurNom3']                  = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement3'] = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurAdresseVillePays3']     = ($qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 != null ? $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 + ' ' : '')
																	+ ($qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 != null ? $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 : '');
formFields['exerciceProfessionnelPeriode3']                       = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3;

formFields['exerciceProfessionnelNature4']                        = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurNom4']                  = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement4'] = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurAdresseVillePays4']     = ($qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 != null ? $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 + ' ' : '')
																	+ ($qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 != null ? $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 : '');
formFields['exerciceProfessionnelPeriode4']                       = $qp254PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4;


//projetsProfessionnels
formFields['projetsProfessionnels']                               = $qp254PE2.projetsProfessionnels.projetsProfessionnels.projetsProfessionnels;
formFields['attesteHonneurDemandeUnique']                         = $qp254PE2.signatureGroup.signature.attesteHonneurDemandeUnique;
formFields['regionExercice']                                      = $qp254PE2.signatureGroup.signature.regionExercice;
formFields['signatureDate']                                       = $qp254PE2.signatureGroup.signature.dateSignature;
formFields['signatureLieu']                                       = $qp254PE2.signatureGroup.signature.lieuSignature;
formFields['signature']                                           = 'Je déclare sur l’honneur l\'exactitude des informations de la formalité et signe la présente déclaration.';
formFields['signatureCoche']                                      = $qp254PE2.signatureGroup.signature.certifieHonneur;
formFields['libelleProfession']								  	  = "Technicien de laboratoire"

formFields['libelleProfession']								  = "Technicien de laboratoire"
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp254PE2.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossierv2.1 GQ.pdf') //
	.apply({
		date: $qp254PE2.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire LE.pdf') //
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPieces);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);

var finalDocItem = finalDoc.save('Technicien de laboratoire_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Technicien de laboratoire - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'Technicien de laboratoire.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});