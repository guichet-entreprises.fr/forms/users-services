var cerfaFields = {};

var civNomPrenom = $qp254PE6.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp254PE6.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp254PE6.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['audioprothesiste']                          = false;
cerfaFields['conseillerGenetique']                       = false;
cerfaFields['preparateurPharmacie']                      = false;
cerfaFields['preparateurPharmacieHospitaliere']          = false;
cerfaFields['infirmier']                       			 = false;
cerfaFields['infirmierSpecialise']                       = false;
cerfaFields['infirmierSpecialiseAnesthesiste']           = false;
cerfaFields['infirmierSpecialiseBlocOperatoire']         = false;
cerfaFields['infirmierSpecialisePuericultrice']          = false;
cerfaFields['pedicurePodologue']                       	 = false;
cerfaFields['ergotherapeuthe']                       	 = false;
cerfaFields['psychomotricien']                       	 = false;
cerfaFields['orthophoniste']                       	     = false;
cerfaFields['orthoptiste']                       		 = false;
cerfaFields['manipulateurElectroradiologie']             = false;
cerfaFields['opticienLunetier']                          = false;
cerfaFields['dieteticien']                       		 = false;
cerfaFields['technicienLaboratoire']                     = true;

cerfaFields['declarationInitiale']                       = false;
cerfaFields['renouvellement']                       	 = true;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp254PE6.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp254PE6.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp254PE6.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp254PE6.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp254PE6.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp254PE6.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp254PE6.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp254PE6.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp254PE6.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp254PE6.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') + ' ' + ($qp254PE6.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp254PE6.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp254PE6.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp254PE6.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' + $qp254PE6.coordonnees.coordonneesDeclarant.declarantAdresseVille + ' ' + $qp254PE6.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp254PE6.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp254PE6.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance: '') + ' ' 
														+ ($qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal: '') + ' ' 
														+ ($qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp254PE6.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionExerceeEM']     					= $qp254PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionExerceeEM;
cerfaFields['professionExerceeSpecialiteEM']  			= $qp254PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM != null ? $qp254PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM : '';
cerfaFields['professionConcernee']                      = $qp254PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee != null ? $qp254PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee : '';
cerfaFields['professionConcerneeSpecialite']            = $qp254PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance != null ? $qp254PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance : '';
cerfaFields['typesActesEnvisages']                      = $qp254PE6.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');
cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp254PE6.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') 
														+ ' ' + ($qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') 
														+' '+ ($qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille : '') 
														+ ', ' + ($qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp254PE6.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp254PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp254PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp254PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale != null ? $qp254PE6.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale :'';

//informations à fournir en cas de renouvellement 

cerfaFields['datePrestationDebut1']                     = $qp254PE6.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['datePrestationFin1']                       = $qp254PE6.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['datePrestationDebut2']                     = ($qp254PE6.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp254PE6.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['datePrestationFin2']                       = ($qp254PE6.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp254PE6.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['datePrestationDebut3']                     = ($qp254PE6.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp254PE6.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['datePrestationFin3']                       = ($qp254PE6.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp254PE6.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['datePrestationDebut4']                     = ($qp254PE6.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp254PE6.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['datePrestationFin4']                       = ($qp254PE6.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp254PE6.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['datePrestationDebut5']                     = ($qp254PE6.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp254PE6.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['datePrestationFin5']                       = ($qp254PE6.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp254PE6.detailsRenouvellement.renouvellement.periodePreste5.to : '');

cerfaFields['commentairesLPSRenouvellement']            = $qp254PE6.detailsRenouvellement.renouvellement.commentairesRenouvellement != null ? $qp254PE6.detailsRenouvellement.renouvellement.commentairesRenouvellement :'';
cerfaFields['activiteProfessionnelsLPSRevouvellement'] = $qp254PE6.detailsRenouvellement.renouvellement.activitesProfessionnelles != null ? $qp254PE6.detailsRenouvellement.renouvellement.activitesProfessionnelles :'';

// Observations et Signature
cerfaFields['autresObservations']                       = $qp254PE6.commentaire.commentaire.commentairesEventuels != null ? $qp254PE6.commentaire.commentaire.commentairesEventuels :'';
cerfaFields['signatureCoche']                           = $qp254PE6.observations.observationsSignature.signatureCoche;
cerfaFields['signatureDate']                            = $qp254PE6.observations.observationsSignature.signatureDate;
cerfaFields['signature']                                ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp254PE6.observations.observationsSignature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp254PE6.observations.observationsSignature.signatureDate,
		autoriteHabilitee :"Ministère chargé de la santé : Direction Générale de l’Offre de Soins",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Formulaire déclaration LPS.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Technicien de laboratoire médical_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Technicien de laboratoire médical - Renouvellement de la déclaration en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de technicien de laboratoire médical.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});