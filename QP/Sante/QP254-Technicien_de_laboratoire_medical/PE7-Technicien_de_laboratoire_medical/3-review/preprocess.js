var cerfaFields = {};

//etatCivil

cerfaFields['nom']                                  = $qp254PE7.information.etatCivil.nom;
cerfaFields['prenom']                               = $qp254PE7.information.etatCivil.prenom;
cerfaFields['nationalite']                          = $qp254PE7.information.etatCivil.nationalite;
cerfaFields['dateNaissance']                        = $qp254PE7.information.etatCivil.dateNaissance;
cerfaFields['villeNaissance']                       = $qp254PE7.information.etatCivil.villeNaissance;
cerfaFields['paysNaissance']                        = $qp254PE7.information.etatCivil.paysNaissance;
cerfaFields['madame']                               = ($qp254PE7.information.etatCivil.civilite=='Madame');
cerfaFields['monsieur']                             = ($qp254PE7.information.etatCivil.civilite=='Monsieur');
cerfaFields ['masculin']							= ($qp254PE7.information.etatCivil.civilite=='Monsieur') ? true : false
cerfaFields ['feminin']								= ($qp254PE7.information.etatCivil.civilite=='Madame') ? true : false

//coordonneesOrigine

cerfaFields['numeroLibelleComplementAdresseOrigine']= $qp254PE7.coordonneesO.coordonneesOrigine.numeroLibelleAdresseOrigine + ($qp254PE7.coordonneesO.coordonneesOrigine.complementAdresseOrigine != null ? ', ' + $qp254PE7.coordonneesO.coordonneesOrigine.complementAdresseOrigine : '');
//cerfaFields['complementAdresseOrigine']           = $qp254PE7.coordonneesO.coordonneesOrigine.complementAdresseOrigine;
cerfaFields['codePostalVillePaysAdresseOrigine']    = ($qp254PE7.coordonneesO.coordonneesOrigine.codePostalAdresseOrigine != null ? $qp254PE7.coordonneesO.coordonneesOrigine.codePostalAdresseOrigine + ', ' : '') + $qp254PE7.coordonneesO.coordonneesOrigine.villeAdresseOrigine + ', ' + $qp254PE7.coordonneesO.coordonneesOrigine.paysAdresseOrigine;
//cerfaFields['villeAdresseOrigine']                = $qp254PE7.coordonneesO.coordonneesOrigine.villeAdresseOrigine;
//cerfaFields['paysAdresseOrigine']                 = $qp254PE7.coordonneesO.coordonneesOrigine.paysAdresseOrigine;
cerfaFields['telephoneAdresseOrigine']              = $qp254PE7.coordonneesO.coordonneesOrigine.telephoneAdresseOrigine;
cerfaFields['mailAdresseOrigine']                   = $qp254PE7.coordonneesO.coordonneesOrigine.mailAdresseOrigine;

//coordonneesFrance

cerfaFields['numeroLibelleComplementAdresseFrance'] = ($qp254PE7.coordonneesF.coordonneesFrance.numeroLibelleAdresseFrance != null ? $qp254PE7.coordonneesF.coordonneesFrance.numeroLibelleAdresseFrance : '') + ($qp254PE7.coordonneesF.coordonneesFrance.complementAdresseFrance != null ? ', ' + $qp254PE7.coordonneesF.coordonneesFrance.complementAdresseFrance : '');
//cerfaFields['complementAdresseFrance']            = $qp254PE7.coordonneesF.coordonneesFrance.complementAdresseFrance;
cerfaFields['codePostalVilleAdresseFrance']         = ($qp254PE7.coordonneesF.coordonneesFrance.codePostalAdresseFrance != null ? $qp254PE7.coordonneesF.coordonneesFrance.codePostalAdresseFrance + ', ' : '') + ($qp254PE7.coordonneesF.coordonneesFrance.villeAdresseFrance != null ? $qp254PE7.coordonneesF.coordonneesFrance.villeAdresseFrance : '');
//cerfaFields['villeAdresseFrance']                 = $qp254PE7.coordonneesF.coordonneesFrance.villeAdresseFrance;
cerfaFields['telephoneAdresseFrance']                = $qp254PE7.coordonneesF.coordonneesFrance.telephoneAdresseFrance;
cerfaFields['mailAdresseFrance']                    = $qp254PE7.coordonneesF.coordonneesFrance.mailAdresseFrance;

//professionExercee

cerfaFields['professionPaysOrigineLangueOrigine']   = ($qp254PE7.profession.professionExercee.professionPaysOrigineLangueOrigine != null) ? ($qp254PE7.profession.professionExercee.professionPaysOrigineLangueOrigine) : ($qp254PE7.profession.professionExercee.professionPaysOrigineFrancophone);
cerfaFields['professionPaysOrigineLangueFrancaise'] = $qp254PE7.profession.professionExercee.professionPaysOrigineLangueFrancaise;
cerfaFields['professionSpecialites']                = ($qp254PE7.profession.professionExercee.professionSpecialites != null) ? ($qp254PE7.profession.professionExercee.professionSpecialites) : ($qp254PE7.profession.professionExercee.professionSpecialitesFrancophone);
cerfaFields['professionFrance']                     = ($qp254PE7.profession.professionExercee.professionFrance != null) ? ($qp254PE7.profession.professionExercee.professionFrance) : ($qp254PE7.profession.professionExercee.professionFrancophoneFrance);
cerfaFields['professionTypeActes']                  = ($qp254PE7.profession.professionExercee.professionTypeActes != null) ? ($qp254PE7.profession.professionExercee.professionTypeActes) : ($qp254PE7.profession.professionExercee.professionFrancophoneTypeActes);
cerfaFields['professionLieuExercice']               = $qp254PE7.profession.professionExercee.professionLieuExercice;
cerfaFields['professionOrdreOui']                	= ($qp254PE7.profession.professionExercee.professionOrdre==true);
cerfaFields['professionOrdreNon']                	= ($qp254PE7.profession.professionExercee.professionOrdre==false);
cerfaFields['professionNomNumOrdre']                = ($qp254PE7.profession.professionExercee.professionNomOrdre != null ? $qp254PE7.profession.professionExercee.professionNomOrdre : '') + ($qp254PE7.profession.professionExercee.professionNumEnrOrdre != null ? '. Numéro d\'inscription : ' + $qp254PE7.profession.professionExercee.professionNumEnrOrdre : '');
cerfaFields['professionCoordonneesOrdre']           = $qp254PE7.profession.professionExercee.professionCoordonneesOrdre;
//cerfaFields['professionNumEnrOrdre']              = $qp254PE7.profession.professionExercee.professionNumEnrOrdre;

//assurance

cerfaFields['nomCompagnieAssurance']                = $qp254PE7.assurancePro.assurance.nomCompagnieAssurance;
cerfaFields['numeroContratAssurance']               = $qp254PE7.assurancePro.assurance.numeroContratAssurance;

//Renouvellement

cerfaFields['du1']                                = $qp254PE7.renouvellement.infosRenouvellements.du1;
cerfaFields['au1']                                = $qp254PE7.renouvellement.infosRenouvellements.au1;
cerfaFields['du2']                                = $qp254PE7.renouvellement.infosRenouvellements.du2;
cerfaFields['au2']                                = $qp254PE7.renouvellement.infosRenouvellements.au2;
cerfaFields['du3']                                = $qp254PE7.renouvellement.infosRenouvellements.du3;
cerfaFields['au3']                                = $qp254PE7.renouvellement.infosRenouvellements.au3;
cerfaFields['du4']                                = $qp254PE7.renouvellement.infosRenouvellements.du4;
cerfaFields['au4']                                = $qp254PE7.renouvellement.infosRenouvellements.au4;
cerfaFields['du5']                                = $qp254PE7.renouvellement.infosRenouvellements.du5;
cerfaFields['au5']                                = $qp254PE7.renouvellement.infosRenouvellements.au5;
cerfaFields['du6']                                = $qp254PE7.renouvellement.infosRenouvellements.du6;
cerfaFields['au6']                                = $qp254PE7.renouvellement.infosRenouvellements.au6;
cerfaFields['du7']                                = $qp254PE7.renouvellement.infosRenouvellements.du7;
cerfaFields['au7']                                = $qp254PE7.renouvellement.infosRenouvellements.au7;
cerfaFields['du8']                                = $qp254PE7.renouvellement.infosRenouvellements.du8;
cerfaFields['au8']                                = $qp254PE7.renouvellement.infosRenouvellements.au8;
cerfaFields['du9']                                = $qp254PE7.renouvellement.infosRenouvellements.du9;
cerfaFields['au9']                                = $qp254PE7.renouvellement.infosRenouvellements.au9;
cerfaFields['commentairesRenouvellement']         = $qp254PE7.renouvellement.infosRenouvellements.commentaires;
cerfaFields['activitésexercees']                  = $qp254PE7.renouvellement.infosRenouvellements.activitesExercees;

//fin

cerfaFields['autresObservations2']                  = ($qp254PE7.finsaisie.fin.autresObservations1 != null ? 'Autres établissements : ' + $qp254PE7.finsaisie.fin.autresObservations1 : '');
cerfaFields['autresObservations1']                  = ($qp254PE7.finsaisie.fin.autresObservations2 != null ?  $qp254PE7.finsaisie.fin.autresObservations2 : '');
cerfaFields['datefin']                              = $qp254PE7.finsaisie.fin.datefin;
cerfaFields['signature']           					= $qp254PE7.finsaisie.fin.signature;


var cerfa = pdf.create('models/FORMULAIRE LPS Autorisation prealable.pdf', cerfaFields);
var cerfaPdf = pdf.save('Renouvellement LPS - Technicien de laboratoire médical.pdf', cerfa);

return spec.create({
    id : 'review',
    label : 'Technicien de laboratoire médical - Renouvellement déclaration de libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Formulaire de renouvellement de déclaration de libre prestation de services pour la profession de Préparateur en technicien de laboratoire médical',
            description : 'Voici le formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
