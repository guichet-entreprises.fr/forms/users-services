var cerfaFields = {};

var civNomPrenom = $qp154PE5.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp154PE5.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp154PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['audioprothesiste']                          = false;
cerfaFields['conseillerGenetique']                       = false;
cerfaFields['preparateurPharmacie']                      = false;
cerfaFields['preparateurPharmacieHospitaliere']          = false;
cerfaFields['infirmier']                       			 = false;
cerfaFields['infirmierSpecialise']                       = true;
cerfaFields['infirmierSpecialiseAnesthesiste']           = false;
cerfaFields['infirmierSpecialiseBlocOperatoire']         = true;
cerfaFields['infirmierSpecialisePuericultrice']          = false;
cerfaFields['pedicurePodologue']                       	 = false;
cerfaFields['ergotherapeuthe']                       	 = false;
cerfaFields['psychomotricien']                       	 = false;
cerfaFields['orthophoniste']                       	     = false;
cerfaFields['orthoptiste']                       		 = false;
cerfaFields['manipulateurElectroradiologie']             = false;
cerfaFields['opticienLunetier']                          = false;
cerfaFields['dieteticien']                       		 = false;

cerfaFields['declarationInitiale']                       = false;
cerfaFields['renouvellement']                       	 = true;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp154PE5.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp154PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp154PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp154PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp154PE5.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp154PE5.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp154PE5.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp154PE5.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp154PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp154PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') + ' ' + ($qp154PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp154PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp154PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp154PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' + $qp154PE5.coordonnees.coordonneesDeclarant.declarantAdresseVille + ' ' + $qp154PE5.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp154PE5.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp154PE5.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance: '') + ' ' 
														+ ($qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal: '') + ' ' 
														+ ($qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp154PE5.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionExerceeEM']     					= $qp154PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionExerceeEM;
cerfaFields['professionExerceeSpecialiteEM']  			= $qp154PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM != null ? $qp154PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM : '';
cerfaFields['professionConcernee']                      = $qp154PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee != null ? $qp154PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee : '';
cerfaFields['professionConcerneeSpecialite']            = $qp154PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance != null ? $qp154PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance : '';
cerfaFields['typesActesEnvisages']                      = $qp154PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

//Lieu exercice
cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');
cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp154PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') 
														+ ' ' + ($qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') 
														+' '+ ($qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille : '') 
														+ ', ' + ($qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp154PE5.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;

// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp154PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp154PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp154PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale != null ? $qp154PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale : '';

//informations à fournir en cas de renouvellement 

cerfaFields['datePrestationDebut1']                     = $qp154PE5.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['datePrestationFin1']                       = $qp154PE5.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['datePrestationDebut2']                     = ($qp154PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp154PE5.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['datePrestationFin2']                       = ($qp154PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp154PE5.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['datePrestationDebut3']                     = ($qp154PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp154PE5.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['datePrestationFin3']                       = ($qp154PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp154PE5.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['datePrestationDebut4']                     = ($qp154PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp154PE5.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['datePrestationFin4']                       = ($qp154PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp154PE5.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['datePrestationDebut5']                     = ($qp154PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp154PE5.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['datePrestationFin5']                       = ($qp154PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp154PE5.detailsRenouvellement.renouvellement.periodePreste5.to : '');

cerfaFields['commentairesLPSRenouvellement']            = $qp154PE5.detailsRenouvellement.renouvellement.commentairesRenouvellement != null ? $qp154PE5.detailsRenouvellement.renouvellement.commentairesRenouvellement : '';
cerfaFields['activiteProfessionnelsLPSRevouvellement'] = $qp154PE5.detailsRenouvellement.renouvellement.activitesProfessionnelles;

// Observations et Signature
cerfaFields['autresObservations']              	= $qp154PE5.observation.observation.autresObservations != null ? $qp154PE5.observation.observation.autresObservations :'';
cerfaFields['signatureCoche']             		= $qp154PE5.signature.signature1.signatureDeclarant;
cerfaFields['signatureDate']             		= $qp154PE5.signature.signature1.signatureDate;
cerfaFields['signature']                        ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp154PE5.signature.signature1.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		dateSignature: $qp154PE5.signature.signature1.signatureDate,
		autoriteHabilitee :"Conseil national de l’ordre des infirmiers",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom,
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Formulaire_LPS.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Infirmier_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Infirmier de bloc opératoire - Renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession d\'infirmier de bloc opératoire.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});