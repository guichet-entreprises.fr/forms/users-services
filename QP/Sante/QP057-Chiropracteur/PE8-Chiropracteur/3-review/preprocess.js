

var cerfaFields = {};

var civNomPrenom = $qp057PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp057PE8.etatCivil.identificationDeclarant.prenom + ' ' + $qp057PE8.etatCivil.identificationDeclarant.nomNaissance;

//Titre
cerfaFields['titreOstheopathe']            = false;
cerfaFields['titreChiropracteur']          = true;
cerfaFields['titrePsychotherapeute']       = false;

//Indications
cerfaFields['diplomeARS']                   = Value('id').of($qp057PE8.indication.indications.indication1).eq("diplomeARS") ? true : false;
cerfaFields['diplomeARSnon']                = Value('id').of($qp057PE8.indication.indications.indication1).eq("diplomeARSnon") ? true : false;
cerfaFields['ouiChangement']                = Value('id').of($qp057PE8.indication.indications.indication2).eq("ouiChangement") ? true : false;
cerfaFields['nonChangement']                = Value('id').of($qp057PE8.indication.indications.indication2).eq("nonChangement") ? true : false;

//EtatCivil
cerfaFields['madame']                      = Value('id').of($qp057PE8.etatCivil.identificationDeclarant.civilite).eq("madame") ? true : false;
cerfaFields['monsieur']                    = Value('id').of($qp057PE8.etatCivil.identificationDeclarant.civilite).eq("monsieur") ? true : false;
cerfaFields['prenom']                      = $qp057PE8.etatCivil.identificationDeclarant.prenom;
cerfaFields['nomNaissance']                = $qp057PE8.etatCivil.identificationDeclarant.nomNaissance;
cerfaFields['nomExercice']                 = $qp057PE8.etatCivil.identificationDeclarant.nomExercice;
cerfaFields['dateNaissance']               = $qp057PE8.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['nationaliteFrancaise']        = Value('id').of($qp057PE8.etatCivil.identificationDeclarant.nationaliteDeclarant).eq("nationaliteFrancaise") ? true : false;
cerfaFields['lieuNaissance']               = $qp057PE8.etatCivil.identificationDeclarant.lieuNaissance;
cerfaFields['departementNaissance']        = $qp057PE8.etatCivil.identificationDeclarant.departementNaissance;
cerfaFields['euroNationalite']             = Value('id').of($qp057PE8.etatCivil.identificationDeclarant.nationaliteDeclarant).eq("euroNationalite") ? true : false;
cerfaFields['autreNationalite']            = Value('id').of($qp057PE8.etatCivil.identificationDeclarant.nationaliteDeclarant).eq("autreNationalite") ? true : false;
cerfaFields['euroNationalitePrecisions']   = $qp057PE8.etatCivil.identificationDeclarant.euroNationalitePrecisions != null ? $qp057PE8.etatCivil.identificationDeclarant.euroNationalitePrecisions : '';
cerfaFields['autreNationalitePrecisions']  = $qp057PE8.etatCivil.identificationDeclarant.autreNationalitePrecisions != null ? $qp057PE8.etatCivil.identificationDeclarant.autreNationalitePrecisions : '';
cerfaFields['numeroAdeli']                 = $qp057PE8.etatCivil.identificationDeclarant.numeroAdeli != null ? $qp057PE8.etatCivil.identificationDeclarant.numeroAdeli : '';

//Adresse Domicile
cerfaFields['adresseDomicile']             = $qp057PE8.adresse.adresseContact.adresseDomicile;
cerfaFields['communeAdresse']              = $qp057PE8.adresse.adresseContact.communeAdresse;
cerfaFields['codePostalAdresse']           = $qp057PE8.adresse.adresseContact.codePostalAdresse != null ? $qp057PE8.adresse.adresseContact.codePostalAdresse : '';
cerfaFields['telephone']                   = $qp057PE8.adresse.adresseContact.telephone;
cerfaFields['email']                       = $qp057PE8.adresse.adresseContact.email;
cerfaFields['langues']                     = $qp057PE8.adresse.adresseContact.langues != null ? $qp057PE8.adresse.adresseContact.langues : '';

//Situation Professionnelle
cerfaFields['anneeDiplome']                = $qp057PE8.situationPro.situationPro.anneeDiplome;
cerfaFields['departementPrecedent']        = $qp057PE8.situationPro.situationPro.departementPrecedent;
cerfaFields['situationActif']              = Value('id').of($qp057PE8.situationPro.situationPro.situationProfessionnelle).eq("situationActif") ? true : false;
cerfaFields['situationInactif']            = Value('id').of($qp057PE8.situationPro.situationPro.situationProfessionnelle).eq("situationInactif") ? true : false;
cerfaFields['situationIndependant']        = Value('id').of($qp057PE8.situationPro.situationPro.situationProfessionnelle).eq("situationIndependant") ? true : false;
cerfaFields['situationMixte']              = Value('id').of($qp057PE8.situationPro.situationPro.situationProfessionnelle).eq("situationMixte") ? true : false;
cerfaFields['situationRetraite']           = Value('id').of($qp057PE8.situationPro.situationPro.situationProfessionnelle).eq("situationRetraite") ? true : false;
cerfaFields['situationLiberal']            = Value('id').of($qp057PE8.situationPro.situationPro.situationProfessionnelle).eq("situationLiberal") ? true : false;
cerfaFields['situationSalarie']            = Value('id').of($qp057PE8.situationPro.situationPro.situationProfessionnelle).eq("situationSalarie") ? true : false;

//Titres et qualifications professionnels
cerfaFields['lieuObtention']               = $qp057PE8.titreQualif.titreQualif.lieuObtention;
cerfaFields['libelleDiplome']              = $qp057PE8.titreQualif.titreQualif.libelleDiplome;
cerfaFields['dateObtention']               = $qp057PE8.titreQualif.titreQualif.dateObtention;

//Activités professionnelles exercées
//Activités secteur libéral
cerfaFields['activiteSecteurLiberal']      =Value('id').of($qp057PE8.activitesPro1.activitesSecteurLiberal.activitePrincipale).eq("activiteSecteurLiberal") ? true : false;
cerfaFields['activiteSalarie']             =Value('id').of($qp057PE8.activitesPro1.activitesSecteurLiberal.activitePrincipale).eq("activiteSalarie") ? true : false;
cerfaFields['remplacantExclusif']          =Value('id').of($qp057PE8.activitesPro1.activitesSecteurLiberal.remplacant).eq("remplacantExclusif") ? true : false;
cerfaFields['dateDebut']                   = $qp057PE8.activitesPro1.activitesSecteurLiberal.dateDebut;

	//ExerciceCabinet
cerfaFields['exerciceCabinetGroupeSCM']    = Value('id').of($qp057PE8.activitesPro1.exerciceCabinet.cabinet2).eq("exerciceCabinetGroupeSCM") ? true : false;
cerfaFields['exerciceIndividuel']          = Value('id').of($qp057PE8.activitesPro1.exerciceCabinet.cabinet2).eq("exerciceIndividuel") ? true : false;
cerfaFields['dateInstallation']            = $qp057PE8.activitesPro1.exerciceCabinet.dateInstallation;
cerfaFields['siretCabinet']                = $qp057PE8.activitesPro1.exerciceCabinet.siretCabinet;
cerfaFields['nomRaisonSocialeCabinet']     = $qp057PE8.activitesPro1.exerciceCabinet.nomRaisonSocialeCabinet;
cerfaFields['adresseCabinet']              = $qp057PE8.activitesPro1.exerciceCabinet.adresseCabinet;
cerfaFields['communeCabinet']              = $qp057PE8.activitesPro1.exerciceCabinet.communeCabinet;
cerfaFields['codePostalCabinet']           = $qp057PE8.activitesPro1.exerciceCabinet.codePostalCabinet;
cerfaFields['autreSocieteExercice']        = Value('id').of($qp057PE8.activitesPro1.exerciceCabinet.societeExercice).eq("autreSocieteExercice") ? true : false;
cerfaFields['SCP']                         = Value('id').of($qp057PE8.activitesPro1.exerciceCabinet.societeExercice).eq("SCP") ? true : false;
cerfaFields['SEL']                         = Value('id').of($qp057PE8.activitesPro1.exerciceCabinet.societeExercice).eq("SEL") ? true : false;
cerfaFields['telephoneCabinet']            = $qp057PE8.activitesPro1.exerciceCabinet.telephoneCabinet;
cerfaFields['faxCabinet']                  = $qp057PE8.activitesPro1.exerciceCabinet.faxCabinet;
cerfaFields['emailCabinet']                = $qp057PE8.activitesPro1.exerciceCabinet.emailCabinet;
cerfaFields['cabinetSecondaire']           = Value('id').of($qp057PE8.activitesPro1.exerciceCabinet.autreCabinet2).eq("cabinetSecondaire") ? true : false;
cerfaFields['autreImplantation']           = Value('id').of($qp057PE8.activitesPro1.exerciceCabinet.autreCabinet2).eq("autreImplantation") ? true : false;
cerfaFields['telephoneCabinetSecondaire']  = $qp057PE8.activitesPro1.exerciceCabinet.telephoneSecondaire;
cerfaFields['faxCabinetSecondaire']        = $qp057PE8.activitesPro1.exerciceCabinet.faxCabinetSecondaire;
cerfaFields['emailSecondaire']             = $qp057PE8.activitesPro1.exerciceCabinet.emailSecondaire;
cerfaFields['adresseSecondaire']           = $qp057PE8.activitesPro1.exerciceCabinet.adresseSecondaire;
cerfaFields['codePostalSecondaire']        = $qp057PE8.activitesPro1.exerciceCabinet.codePostalSecondaire;
cerfaFields['communeSecondaire']           = $qp057PE8.activitesPro1.exerciceCabinet.communeSecondaire;

//Exercice Etablissement
cerfaFields['nomRaisonSocialeEtablissement']    = $qp057PE8.activitesPro2.exerciceEtablissement.nomRaisonSocialeEtablissement;
cerfaFields['codePostalEtablissement']     = $qp057PE8.activitesPro2.exerciceEtablissement.codePostalEtablissement;
cerfaFields['communeEtablissement']        = $qp057PE8.activitesPro2.exerciceEtablissement.communeEtablissement;
cerfaFields['emailEtablissement']          = $qp057PE8.activitesPro2.exerciceEtablissement.emailEtablissement;
cerfaFields['datePriseFonction']           = $qp057PE8.activitesPro2.exerciceEtablissement.datePriseFonction;
cerfaFields['telephoneEtablissement']      = $qp057PE8.activitesPro2.exerciceEtablissement.telephoneEtablissement;
cerfaFields['faxEtablissement']            = $qp057PE8.activitesPro2.exerciceEtablissement.faxEtablissement;
cerfaFields['adresseEtablissement']        = $qp057PE8.activitesPro2.exerciceEtablissement.adresseEtablissement;

//Activite Salarié
cerfaFields['nomRaisonSocialeSalarie']     = $qp057PE8.activitesPro2.autreExerciceSalarie.nomRaisonSocialeSalarie;
cerfaFields['adresseSalarie']              = $qp057PE8.activitesPro2.autreExerciceSalarie.adresseSalarie;
cerfaFields['codePostalSalarie']           = $qp057PE8.activitesPro2.autreExerciceSalarie.codePostalSalarie;
cerfaFields['communeSalarie']              = $qp057PE8.activitesPro2.autreExerciceSalarie.communeSalarie;
cerfaFields['telephoneSalarie']            = $qp057PE8.activitesPro2.autreExerciceSalarie.telephoneSalarie;
cerfaFields['faxSalarie']                  = $qp057PE8.activitesPro2.autreExerciceSalarie.faxSalarie;
cerfaFields['emailSalarie']                = $qp057PE8.activitesPro2.autreExerciceSalarie.emailSalarie;
cerfaFields['datePriseFonctionSalarie']    = $qp057PE8.activitesPro2.autreExerciceSalarie.datePriseFonctionSalarie;
cerfaFields['siretSociete']                = $qp057PE8.activitesPro2.autreExerciceSalarie.siretSociete;

cerfaFields['nomRaisonSocialeIndependant'] = $qp057PE8.activitesPro2.activiteIndependante.nomRaisonSocialeIndependant;
cerfaFields['adresseIndependant']          = $qp057PE8.activitesPro2.activiteIndependante.adresseIndependant;
cerfaFields['communeIndependant']          = $qp057PE8.activitesPro2.activiteIndependante.communeIndependant;
cerfaFields['siretSocieteIndependant']     = $qp057PE8.activitesPro2.activiteIndependante.siretSocieteIndependant;
cerfaFields['telephoneIndependant']        = $qp057PE8.activitesPro2.activiteIndependante.telephoneIndependant;
cerfaFields['faxIndependant']              = $qp057PE8.activitesPro2.activiteIndependante.faxIndependant;
cerfaFields['emailIndependant']            = $qp057PE8.activitesPro2.activiteIndependante.emailIndependant;
cerfaFields['codePostalIndependant']       = $qp057PE8.activitesPro2.activiteIndependante.codePostalIndependant;
cerfaFields['dateDebutIndependant']        = $qp057PE8.activitesPro2.activiteIndependante.dateDebutIndependant;

//Signature
cerfaFields['dateSignature']               = $qp057PE8.signature.signature.dateSignature;
cerfaFields['declarationHonneur']          = $qp057PE8.signature.signature.dateSignature;
cerfaFields['signatureNom']                = $qp057PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp057PE8.etatCivil.identificationDeclarant.prenom + ' ' + $qp057PE8.etatCivil.identificationDeclarant.nomNaissance;




/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp057PE8.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp057PE8.signature.signature.dateSignature,
		autoriteHabilitee :"ARS",
		demandeContexte : "Inscription au répertoire ADELI",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa_13777-04.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopie);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustif);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('chiropracteur_inscription_Adeli.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Chiropracteur - demande d\'inscription au répertoire ADELI.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\'inscription au répertoire ADELI pour l\'exercice de la profession de chiropracteur',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
