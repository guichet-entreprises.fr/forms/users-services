var formFields = {};

var civNomPrenom = $qp057PE4.info1.etatCivil.civilite + ' ' + $qp057PE4.info1.etatCivil.declarantNomNaissance + ' ' + $qp057PE4.info1.etatCivil.declarantPrenoms;

//Motif de la demande

formFields['professionConcernee']                                  = "Chiropracteur";

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;


//Identité du demandeur

formFields['declarantNomNaissance']                                = $qp057PE4.info1.etatCivil.declarantNomNaissance;
formFields['declarantPrenoms']                                     = $qp057PE4.info1.etatCivil.declarantPrenoms;
formFields['declarantNationalite']                                 = $qp057PE4.info1.etatCivil.declarantNationalite;
formFields['declarantDateNaissance']                               = $qp057PE4.info1.etatCivil.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp057PE4.info1.etatCivil.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp057PE4.info1.etatCivil.declarantPaysNaissance;


//Adresse

formFields['declarantAdressePersoCourriel']                        = $qp057PE4.info2.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoieComplement']            = ($qp057PE4.info2.coordonnees1.declarantAdressePersoNumNomVoie != null ? $qp057PE4.info2.coordonnees1.declarantAdressePersoNumNomVoie: '') + ' ' + ($qp057PE4.info2.coordonnees1.declarantAdressePersoComplement != null ? $qp057PE4.info2.coordonnees1.declarantAdressePersoComplement: '');
//formFields['declarantAdressePersoCP']                            = $qp057PE4.info2.coordonnees1.declarantAdressePersoCP;
//formFields['declarantAdressePersoVille']                         = $qp057PE4.info2.coordonnees1.declarantAdressePersoVille;
//formFields['declarantAdressePersoPays']                          = $qp057PE4.info2.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoCPVillePays']                     = ($qp057PE4.info2.coordonnees1.declarantAdressePersoCP != null ? $qp057PE4.info2.coordonnees1.declarantAdressePersoCP: '') + ' ' + ($qp057PE4.info2.coordonnees1.declarantAdressePersoVille != null ? $qp057PE4.info2.coordonnees1.declarantAdressePersoVille: '') +' '+ ($qp057PE4.info2.coordonnees1.declarantAdressePersoPays != null ? $qp057PE4.info2.coordonnees1.declarantAdressePersoPays: '');
formFields['declarantAdressePersoTelephone']                       = $qp057PE4.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp057PE4.info2.coordonnees1.declarantAdressePersoCourriel;

//Adresse France 

formFields['declarantAdresseFranceNumNomVoieComplement']           = ($qp057PE4.info22.coordonnees2.declarantAdresseFranceNumNomVoie != null ? $qp057PE4.info22.coordonnees2.declarantAdresseFranceNumNomVoie: '') + ' ' + ($qp057PE4.info22.coordonnees2.declarantAdresseFranceComplement != null ? $qp057PE4.info22.coordonnees2.declarantAdresseFranceComplement: '');
//formFields['declarantAdresseFranceCP']                           = $qp057PE4.info22.coordonnees2.declarantAdresseFranceCP;
//formFields['declarantAdresseFranceVille']                        = $qp057PE4.info22.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFranceCPVillePays']                    = ($qp057PE4.info22.coordonnees2.declarantAdresseFranceCP != null ? $qp057PE4.info22.coordonnees2.declarantAdresseFranceCP: '') + ' ' + ($qp057PE4.info22.coordonnees2.declarantAdresseFranceVille != null ? $qp057PE4.info22.coordonnees2.declarantAdresseFranceVille: '');
formFields['declarantAdresseFranceTelephone']                      = $qp057PE4.info22.coordonnees2.declarantAdresseFranceTelephone;
formFields['declarantAdresseFranceCourriel']                       = $qp057PE4.info22.coordonnees2.declarantAdresseFranceCourriel;

//Profession concernée

formFields['professionExercee']                                    = $qp057PE4.info3.profession.professionExercee;
formFields['specialite']                                           = $qp057PE4.info3.profession.specialite != null ? $qp057PE4.info3.profession.specialite :'';
formFields['professionDemandee']                                   = $qp057PE4.info3.profession.professionDemandee;
formFields['specialiteDemandee']                                   = $qp057PE4.info3.profession.specialiteDemandee != null ? $qp057PE4.info3.profession.specialiteDemandee :'';
formFields['actesEnvisages']                                       = $qp057PE4.info3.profession.actesEnvisages != null ? $qp057PE4.info3.profession.actesEnvisages :'';
formFields['lieupremiereLPS']                                      = $qp057PE4.info3.profession.lieupremiereLPS != null ? $qp057PE4.info3.profession.lieupremiereLPS :'';

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp057PE4.info4.ordre.ouiNon).eq('ordreNon') ? true : false;
formFields['ordreOui']                                             = Value('id').of($qp057PE4.info4.ordre.ouiNon).eq('ordreOui') ? true : false;

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp057PE4.info4.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp057PE4.info4.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp057PE4.info4.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp057PE4.info4.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp057PE4.info4.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp057PE4.info4.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp057PE4.info4.ordre.ordreProfessionnelAdresseVille != null ? $qp057PE4.info4.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp057PE4.info4.ordre.ordreProfessionnelAdressePays != null ? $qp057PE4.info4.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = ($qp057PE4.info4.ordre.ordreProfessionnelNom != null ? $qp057PE4.info4.ordre.ordreProfessionnelNom : '') + ' ' 
																	+ ($qp057PE4.info4.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp057PE4.info4.ordre.ordreProfessionnelNumEnregistrementNom: '');


//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp057PE4.info5.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp057PE4.info5.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp057PE4.info5.assuranceProfessionnelle.commentaires != null ? $qp057PE4.info5.assuranceProfessionnelle.commentaires :'';

//Signature&Observations

formFields['autresObservations']                                    = $qp057PE4.info6.observations.autresObservations != null ? $qp057PE4.info6.observations.autresObservations :'';
formFields['dateSignature']                                        = $qp057PE4.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp057PE4.info7.signature.attesteSignature;




formFields['libelleProfession']				                       = "Chiropracteur"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp057PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp057PE4.info7.signature.dateSignature,
		autoriteHabilitee :"Agence régionale de santé",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire_sante_LPS.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Formulaire_sante_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Chiropracteur - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de chiropracteur',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
