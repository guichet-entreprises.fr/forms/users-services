var cerfaFields = {};
cerfaFields['profession']                           = 'Chiropracteur';

civiliteMonsieur                                = ($qp057PE3.etatCivil.identificationDeclarant.civilite=='Monsieur');
civiliteMadame                                  = ($qp057PE3.etatCivil.identificationDeclarant.civilite=='Madame');


cerfaFields['casEMReglemente']                      = false;
cerfaFields['casEMNeReglementePas']                 = false;
cerfaFields['casEtatTiers']                         = true;

cerfaFields['nomDeclarant']                         = $qp057PE3.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']                      = $qp057PE3.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['nationaliteDeclarant']                 = $qp057PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissanceDeclarant']               = $qp057PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['villeNaissanceDeclarant']              = $qp057PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['paysNaissanceDeclarant']               = $qp057PE3.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['adresseDeclarantNumNomVoieComplement'] = $qp057PE3.adresseEtatEtablissement.adresseContactEE.numeroLibelleAdresseDeclarant +' '+($qp057PE3.adresseEtatEtablissement.adresseContactEE.complementAdresseDeclarant != null ? $qp057PE3.adresseEtatEtablissement.adresseContactEE.complementAdresseDeclarant : '');
cerfaFields['adresseDeclarantCPVillePays']          = ($qp057PE3.adresseEtatEtablissement.adresseContactEE.codePostalAdresseDeclarant != null ? $qp057PE3.adresseEtatEtablissement.adresseContactEE.codePostalAdresseDeclarant : '') +' '+ $qp057PE3.adresseEtatEtablissement.adresseContactEE.villeAdresseDeclarant +', '+ $qp057PE3.adresseEtatEtablissement.adresseContactEE.paysAdresseDeclarant;
cerfaFields['telephoneDeclarant']                   = $qp057PE3.adresseEtatEtablissement.adresseContactEE.telephoneMobileAdresseDeclarant;
cerfaFields['adresseMailDeclarant']                 = $qp057PE3.adresseEtatEtablissement.adresseContactEE.mailAdresseDeclarant;
civNomPrenom 									    = $qp057PE3.etatCivil.identificationDeclarant.civilite + ' ' + cerfaFields['nomDeclarant'] +' '+ cerfaFields['prenomDeclarant']; 

cerfaFields['adresseFranceNumNomVoieComplement']    = ($qp057PE3.adresseFrance.adresseContactFR.numeroLibelleAdresseDeclarant != null ? $qp057PE3.adresseFrance.adresseContactFR.numeroLibelleAdresseDeclarant :'')+' '+ ($qp057PE3.adresseFrance.adresseContactFR.complementAdresseDeclarant != null ? $qp057PE3.adresseFrance.adresseContactFR.complementAdresseDeclarant :'');
cerfaFields['adresseFranceCPVille']                 = ($qp057PE3.adresseFrance.adresseContactFR.codePostalAdresseDeclarant != null ? $qp057PE3.adresseFrance.adresseContactFR.codePostalAdresseDeclarant :'')+' '+ ($qp057PE3.adresseFrance.adresseContactFR.villeAdresseDeclarant != null ? $qp057PE3.adresseFrance.adresseContactFR.villeAdresseDeclarant :'');
cerfaFields['telephoneFrance']                      = ($qp057PE3.adresseFrance.adresseContactFR.telephoneMobileAdresseDeclarant != null ? $qp057PE3.adresseFrance.adresseContactFR.telephoneMobileAdresseDeclarant :'');
cerfaFields['adresseMailFrance']                    = ($qp057PE3.adresseFrance.adresseContactFR.mailAdresseDeclarant != null ? $qp057PE3.adresseFrance.adresseContactFR.mailAdresseDeclarant :'');

cerfaFields['diplomeIntitule']                      = $qp057PE3.diplomeProfession.diplomeProfession1.intituleDiplome;
cerfaFields['diplomeDateObtention']                 = $qp057PE3.diplomeProfession.diplomeProfession1.dateObtentionDiplome;
cerfaFields['diplomePaysObtention']                 = $qp057PE3.diplomeProfession.diplomeProfession1.paysObtentionDiplome;
cerfaFields['diplomeDelivrePar']                    = $qp057PE3.diplomeProfession.diplomeProfession1.delivreDiplome;
cerfaFields['diplomeDateReconnaissance']            = ($qp057PE3.diplomeProfession.diplomeProfession1.dateReconnaissanceDiplomeEtat != null ? $qp057PE3.diplomeProfession.diplomeProfession1.dateReconnaissanceDiplomeEtat :'');

cerfaFields['autreDiplomeIntitule1']                = ($qp057PE3.diplomeQualification.diplomeQualification1.intituleObtentionDiplome1 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.intituleObtentionDiplome1 :'');
cerfaFields['autreDiplomeIntitule2']                = ($qp057PE3.diplomeQualification.diplomeQualification1.intituleObtentionDiplome2 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.intituleObtentionDiplome2 :'');
cerfaFields['autreDiplomeIntitule3']                = ($qp057PE3.diplomeQualification.diplomeQualification1.intituleObtentionDiplome3 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.intituleObtentionDiplome3 :'');
cerfaFields['autreDiplomeIntitule4']                = ($qp057PE3.diplomeQualification.diplomeQualification1.intituleObtentionDiplome4 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.intituleObtentionDiplome4 :'');

cerfaFields['autreDiplomePays1']                    = ($qp057PE3.diplomeQualification.diplomeQualification1.paysObtentionDiplome1 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.paysObtentionDiplome1 :'');
cerfaFields['autreDiplomePays2']                    = ($qp057PE3.diplomeQualification.diplomeQualification1.paysObtentionDiplome2 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.paysObtentionDiplome2 :'');
cerfaFields['autreDiplomePays3']                    = ($qp057PE3.diplomeQualification.diplomeQualification1.paysObtentionDiplome3 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.paysObtentionDiplome3 :'');
cerfaFields['autreDiplomePays4']                    = ($qp057PE3.diplomeQualification.diplomeQualification1.paysObtentionDiplome4 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.paysObtentionDiplome4 :'');

cerfaFields['autreDiplomeDateObtention1']           = ($qp057PE3.diplomeQualification.diplomeQualification1.dateObtentionDiplome1 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.dateObtentionDiplome1 :'');
cerfaFields['autreDiplomeDateObtention2']           = ($qp057PE3.diplomeQualification.diplomeQualification1.dateObtentionDiplome2 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.dateObtentionDiplome2 :'');
cerfaFields['autreDiplomeDateObtention3']           = ($qp057PE3.diplomeQualification.diplomeQualification1.dateObtentionDiplome3 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.dateObtentionDiplome3 :'');
cerfaFields['autreDiplomeDateObtention4']           = ($qp057PE3.diplomeQualification.diplomeQualification1.dateObtentionDiplome4 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.dateObtentionDiplome4 :'');

cerfaFields['autreDiplomeLieuFromation1']           = ($qp057PE3.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome1 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome1 :'');
cerfaFields['autreDiplomeLieuFromation2']           = ($qp057PE3.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome2 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome2 :'');
cerfaFields['autreDiplomeLieuFromation3']           = ($qp057PE3.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome3 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome3 :'');
cerfaFields['autreDiplomeLieuFromation4']           = ($qp057PE3.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome4 != null ? $qp057PE3.diplomeQualification.diplomeQualification1.lieuFormationObtentionDiplome4 :'');

cerfaFields['autreTitrePays1']                      = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome1 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome1 :'');
cerfaFields['autreTitrePays2']                      = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome2 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome2 :'');
cerfaFields['autreTitrePays3']                      = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome3 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome3 :'');
cerfaFields['autreTitrePays4']                      = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome4 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome4 :'');

cerfaFields['autreTitreIntitule1']                  = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome1 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome1 :'');
cerfaFields['autreTitreIntitule2']                  = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome2 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome2 :'');
cerfaFields['autreTitreIntitule3']                  = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome3 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome3 :'');
cerfaFields['autreTitreIntitule4']                  = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome4 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome4 :'');

cerfaFields['autreTitreDateObtention1']             = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome1 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome1 :'');
cerfaFields['autreTitreDateObtention2']             = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome2 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome2 :'');
cerfaFields['autreTitreDateObtention3']             = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome3 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome3 :'');
cerfaFields['autreTitreDateObtention4']             = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome4 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome4 :'');

cerfaFields['autreTitreLieuFromation1']             = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome1 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome1 :'');
cerfaFields['autreTitreLieuFromation2']             = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome2 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome2 :'');
cerfaFields['autreTitreLieuFromation3']             = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome3 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome3 :'');
cerfaFields['autreTitreLieuFromation4']             = ($qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome4 != null ? $qp057PE3.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome4 :'');

cerfaFields['exerciceProfessionnelVillePays1']      = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 :'') + ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 != null ? ', ' + $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 :'');
cerfaFields['exerciceProfessionnelVillePays2']      = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 :'') + ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 != null ? ', ' + $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 :'');
cerfaFields['exerciceProfessionnelVillePays3']      = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 :'') + ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 != null ? ', ' + $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 :'');
cerfaFields['exerciceProfessionnelVillePays4']      = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 :'') + ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 != null ? ', ' + $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 :'');

cerfaFields['exerciceProfessionnelNature1']         = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger1 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger1 :'');
cerfaFields['exerciceProfessionnelNature2']         = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger2 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger2 :'');
cerfaFields['exerciceProfessionnelNature3']         = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger3 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger3 :'');
cerfaFields['exerciceProfessionnelNature4']         = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger4 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger4 :'');

cerfaFields['exerciceProfessionnelDu1']             = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1.from :'');
cerfaFields['exerciceProfessionnelDu2']             = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2.from :'');
cerfaFields['exerciceProfessionnelDu3']             = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3.from :'');
cerfaFields['exerciceProfessionnelDu4']             = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4.from :'');

cerfaFields['exerciceProfessionnelAu1']             = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1.to :'');
cerfaFields['exerciceProfessionnelAu2']             = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2.to :'');
cerfaFields['exerciceProfessionnelAu3']             = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3.to :'');
cerfaFields['exerciceProfessionnelAu4']             = ($qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4 != null ? $qp057PE3.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4.to :'');

cerfaFields['projetsProfessionnels']                = $qp057PE3.projetsProfessionnels.projetsProfessionnels1.projetsProfessionnels2;

cerfaFields['dateSignature']                        = $qp057PE3.signature.signature1.dateSignature;
cerfaFields['signatureTexte']                       = 'Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.';
cerfaFields['signatureCoche']                       = $qp057PE3.signature.signature1.signature2;
cerfaFields['regionExercice']                       = $qp057PE3.signature.signature1.regionExercice;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp057PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp057PE3.signature.signature1.dateSignature,
		autoriteHabilitee :"Agence régionale de santé.",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/modelPDF.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatif);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Chiropracteur_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Chiropracteur - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de chiropracteur',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
