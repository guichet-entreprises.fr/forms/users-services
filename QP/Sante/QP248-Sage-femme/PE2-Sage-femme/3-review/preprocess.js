function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};

var civNomPrenom = $qp248PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp248PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp248PE2.etatCivil.identificationDeclarant.prenomDeclarant;


cerfaFields['changementSituatuinPrestataireCoche'] = false;
cerfaFields['renouvellementCoche']                 = true;
cerfaFields['premierePrestationServiceCoche']      = false;

//Etat civil

cerfaFields['nomDemandeur']                        = $qp248PE2.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDemandeur']                     = $qp248PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['nationaliteDemandeur']                = $qp248PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['lieuNaissanceVille']                  = $qp248PE2.etatCivil.identificationDeclarant.lieuNaissanceVilleDeclarant;
cerfaFields['paysNaissanceDemandeur']              = $qp248PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['madameCoche']                         = $qp248PE2.etatCivil.identificationDeclarant.civilite=='Madame';
cerfaFields['monsieurCoche']                       = $qp248PE2.etatCivil.identificationDeclarant.civilite=='Monsieur';


if($qp248PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['dateNaissanceJour']                    = day;
cerfaFields['dateNaissanceMois']                  	= month;
cerfaFields['dateNaissanceAnnee']                   = year;
}

//Adresse 


cerfaFields['adresseNomNumVoie']                   = $qp248PE2.adresse.adresseContact.numeroLibelleAdresseDeclarant +' '+ ($qp248PE2.adresse.adresseContact.complementAdresseDeclarant != null ? $qp248PE2.adresse.adresseContact.complementAdresseDeclarant :'') 
													+' '+ ($qp248PE2.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp248PE2.adresse.adresseContact.codePostalAdresseDeclarant :'') +' '+ $qp248PE2.adresse.adresseContact.villeAdresseDeclarant +' '+ $qp248PE2.adresse.adresseContact.paysAdresseDeclarant ;
cerfaFields['telephoneDemandeur']                  = $qp248PE2.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['courrielDemandeur']                   = $qp248PE2.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['adresseFranceDemandeur']              = ($qp248PE2.adresse.adresseFrance.numeroLibelleAdresseFrance != null ? $qp248PE2.adresse.adresseFrance.numeroLibelleAdresseFrance:'') +' '+ ($qp248PE2.adresse.adresseFrance.complementAdresseFrance != null ? $qp248PE2.adresse.adresseFrance.complementAdresseFrance :'') 
													+' '+ ($qp248PE2.adresse.adresseFrance.codePostalAdresseFrance != null ? $qp248PE2.adresse.adresseFrance.codePostalAdresseFrance :'') +' '+ ($qp248PE2.adresse.adresseFrance.villeAdresseFrance != null ? $qp248PE2.adresse.adresseFrance.villeAdresseFrance:'')
													+' '+ ($qp248PE2.adresse.adresseFrance.paysAdresseFrance != null ? $qp248PE2.adresse.adresseFrance.paysAdresseFrance:'') ;
cerfaFields['telephoneFranceDemandeur']            = $qp248PE2.adresse.adresseFrance.telephoneMobileAdresseFrance;
cerfaFields['CourrielfranceDemandeur']             = $qp248PE2.adresse.adresseFrance.mailAdresseFrance;




cerfaFields['professionExerceeFrance']             = $qp248PE2.professionConcernee.professionConcernee.professionExerceeFrance;
cerfaFields['acteEnvisageesFrance']                = $qp248PE2.professionConcernee.professionConcernee.acteEnvisageesFrance;
cerfaFields['lieuExercicePremierePrestation']      = $qp248PE2.professionConcernee.professionConcernee.lieuExercicePremierePrestation;

cerfaFields['ouiOrdreCoche']                       = $qp248PE2.professionConcernee.professionConcernee.oredreProfessionnel=='Oui';
cerfaFields['nonOrdreCoche']                       = $qp248PE2.professionConcernee.professionConcernee.oredreProfessionnel=='Non';

cerfaFields['nomCoordonneesOrdre']                 = $qp248PE2.professionConcernee.professionConcernee.nomCoordonneesOrdre;
cerfaFields['numeroEnregistrementOrdre']           = $qp248PE2.professionConcernee.professionConcernee.numeroEnregistrementOrdre;



cerfaFields['nomCompagnieAssurance']               = $qp248PE2.assurance.assurance.nomCompagnieAssurance;
cerfaFields['numeroContratAssurance']              = $qp248PE2.assurance.assurance.numeroContratAssurance;
cerfaFields['commentaireAssurance']                = ($qp248PE2.assurance.assurance.commentaireAssurance  != null ? $qp248PE2.assurance.assurance.commentaireAssurance:'');




if($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService1 != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService1.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['datePrestationJourDebut1']       = day;
cerfaFields['datePrestationMoisDebut1']       = month;
cerfaFields['datePrestationAnneeDebut1']      = year;

}

if($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService1 != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService1.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['datePrestationJourFin1']       = day;
cerfaFields['datePrestationMoisFin1']       = month;
cerfaFields['datePrestationAnneeFin1']      = year;

}




if($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService2 != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService2.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();

cerfaFields['datePrestationJourDebut2']       = day;
cerfaFields['datePrestationMoisDebut2']       = month;
cerfaFields['datePrestationAnneeDebut2']      = year;




}


if($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService2 != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService2.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['datePrestationJourFin2']       = day;
cerfaFields['datePrestationMoisFin2']       = month;
cerfaFields['datePrestationAnneeFin2']      = year;
}



if($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService3 != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService3.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['datePrestationJourDebut3']       = day;
cerfaFields['datePrestationMoisDebut3']       = month;
cerfaFields['datePrestationAnneeDebut3']      = year;
}

if($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService3 != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService3.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['datePrestationJourFin3']       = day;
cerfaFields['datePrestationMoisFin3']       = month;
cerfaFields['datePrestationAnneeFin3']      = year;
}



if($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService4 != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService4.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();

cerfaFields['datePrestationJourDebut4']       = day;
cerfaFields['datePrestationMoisDebut4']       = month;
cerfaFields['datePrestationAnneeDebut4']      = year;
}



if($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService4 != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.informationRenouvellement.informationRenouvellement.datePrestationService4.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['datePrestationJourFin4']       = day;
cerfaFields['datePrestationMoisFin4']       = month;
cerfaFields['datePrestationAnneeFin4']      = year;
}



cerfaFields['commentaireEventuels']                = $qp248PE2.informationRenouvellement.informationRenouvellement.commentairesEventuels2;
cerfaFields['activitesProfessionnellesExercees']   = $qp248PE2.informationRenouvellement.informationRenouvellement.activitesProfessionnellesExercees;






cerfaFields['autresObservations']  				   = $qp248PE2.commentaire.commentaire.commentairesEventuels;
cerfaFields['lieuSignature']                       = $qp248PE2.signature.signature.lieuSignature;
cerfaFields['signatureCoche']                      = $qp248PE2.signature.signature.attesteSignature;
cerfaFields['nomSignature']                        = $qp248PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp248PE2.etatCivil.identificationDeclarant.prenomDeclarant;


if($qp248PE2.signature.signature.dateSignature != null) {    
var dateTemp = new Date(parseInt ($qp248PE2.signature.signature.dateSignature.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['dateSignatureJour']                    = day;			
cerfaFields['dateSignatureMois']                    = month;
cerfaFields['dateSignatureAnnee']                   = year;

}
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature : $qp248PE2.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil National de l'Ordre des Sages-femmes",
		demandeContexte : "Renouvellement de la déclaration de libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire-declaration-LPS-Sage-femme.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPremiereDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('profession_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Sage-femme - renouvellement de la déclaration de libre prestation de services ',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Renouvellement de la déclaration de libre prestation de services  pour l\'exercice de la profession de sage-femme',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
