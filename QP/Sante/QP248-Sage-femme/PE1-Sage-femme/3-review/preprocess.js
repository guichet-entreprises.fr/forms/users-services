function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};

var civNomPrenom = $qp248PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp248PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp248PE1.etatCivil.identificationDeclarant.prenomDeclarant;


cerfaFields['changementSituatuinPrestataireCoche'] = false;
cerfaFields['renouvellementCoche']                 = false;
cerfaFields['premierePrestationServiceCoche']      = true;

//Etat civil

cerfaFields['nomDemandeur']                        = $qp248PE1.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDemandeur']                     = $qp248PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['nationaliteDemandeur']                = $qp248PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['lieuNaissanceVille']                  = $qp248PE1.etatCivil.identificationDeclarant.lieuNaissanceVilleDeclarant;
cerfaFields['paysNaissanceDemandeur']              = $qp248PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['madameCoche']                         = $qp248PE1.etatCivil.identificationDeclarant.civilite=='Madame';
cerfaFields['monsieurCoche']                       = $qp248PE1.etatCivil.identificationDeclarant.civilite=='Monsieur';


if($qp248PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant != null) {    
var dateTemp = new Date(parseInt ($qp248PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['dateNaissanceJour']                    = day;
cerfaFields['dateNaissanceMois']                  	= month;
cerfaFields['dateNaissanceAnnee']                   = year;
}

//Adresse 


cerfaFields['adresseNomNumVoie']                   = $qp248PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant +' '+ ($qp248PE1.adresse.adresseContact.complementAdresseDeclarant != null ? $qp248PE1.adresse.adresseContact.complementAdresseDeclarant :'') 
													+' '+ ($qp248PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp248PE1.adresse.adresseContact.codePostalAdresseDeclarant :'') +' '+ $qp248PE1.adresse.adresseContact.villeAdresseDeclarant +' '+ $qp248PE1.adresse.adresseContact.paysAdresseDeclarant ;
cerfaFields['telephoneDemandeur']                  = $qp248PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['courrielDemandeur']                   = $qp248PE1.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['adresseFranceDemandeur']              = ($qp248PE1.adresse.adresseFrance.numeroLibelleAdresseFrance != null ? $qp248PE1.adresse.adresseFrance.numeroLibelleAdresseFrance:'') +' '+ ($qp248PE1.adresse.adresseFrance.complementAdresseFrance != null ? $qp248PE1.adresse.adresseFrance.complementAdresseFrance :'') 
													+' '+ ($qp248PE1.adresse.adresseFrance.codePostalAdresseFrance != null ? $qp248PE1.adresse.adresseFrance.codePostalAdresseFrance :'') +' '+ ($qp248PE1.adresse.adresseFrance.villeAdresseFrance != null ? $qp248PE1.adresse.adresseFrance.villeAdresseFrance:'')
													+' '+ ($qp248PE1.adresse.adresseFrance.paysAdresseFrance != null ? $qp248PE1.adresse.adresseFrance.paysAdresseFrance:'') ;
cerfaFields['telephoneFranceDemandeur']            = $qp248PE1.adresse.adresseFrance.telephoneMobileAdresseFrance;
cerfaFields['CourrielfranceDemandeur']             = $qp248PE1.adresse.adresseFrance.mailAdresseFrance;




cerfaFields['professionExerceeFrance']             = $qp248PE1.professionConcernee.professionConcernee.professionExerceeFrance;
cerfaFields['acteEnvisageesFrance']                = $qp248PE1.professionConcernee.professionConcernee.acteEnvisageesFrance;
cerfaFields['lieuExercicePremierePrestation']      = $qp248PE1.professionConcernee.professionConcernee.lieuExercicePremierePrestation;

cerfaFields['ouiOrdreCoche']                       = $qp248PE1.professionConcernee.professionConcernee.oredreProfessionnel=='Oui';
cerfaFields['nonOrdreCoche']                       = $qp248PE1.professionConcernee.professionConcernee.oredreProfessionnel=='Non';

cerfaFields['nomCoordonneesOrdre']                 = $qp248PE1.professionConcernee.professionConcernee.nomCoordonneesOrdre;
cerfaFields['numeroEnregistrementOrdre']           = $qp248PE1.professionConcernee.professionConcernee.numeroEnregistrementOrdre;



cerfaFields['nomCompagnieAssurance']               = $qp248PE1.assurance.assurance.nomCompagnieAssurance;
cerfaFields['numeroContratAssurance']              = $qp248PE1.assurance.assurance.numeroContratAssurance;
cerfaFields['commentaireAssurance']                = ($qp248PE1.assurance.assurance.commentaireAssurance  != null ? $qp248PE1.assurance.assurance.commentaireAssurance:'');

cerfaFields['datePrestationJourDebut1']            = '';
cerfaFields['datePrestationMoisDebut1']            = '';
cerfaFields['datePrestationJourDebut2']            = '';
cerfaFields['datePrestationAnneeDebut1']           = '';
cerfaFields['datePrestationJourFin1']              = '';
cerfaFields['datePrestationMoisFin1']              = '';
cerfaFields['datePrestationAnneeFin1']             = '';
cerfaFields['datePrestationJourDebut3']            = '';
cerfaFields['datePrestationMoisDebut2']            = '';
cerfaFields['datePrestationAnneeDebut2']           = '';
cerfaFields['datePrestationAnneeDebut3']           = '';
cerfaFields['datePrestationAnneeDebut4']           = '';
cerfaFields['datePrestationJourFin2']              = '';
cerfaFields['datePrestationJourFin4']              = '';
cerfaFields['datePrestationJourFin3']              = '';
cerfaFields['datePrestationMoisFin2']              = '';
cerfaFields['datePrestationMoisFin3']              = '';
cerfaFields['datePrestationMoisFin4']              = '';
cerfaFields['datePrestationAnneeFin2']             = '';
cerfaFields['datePrestationAnneeFin3']             = '';
cerfaFields['datePrestationAnneeFin4']             = '';
cerfaFields['datePrestationJourDebut4']            = '';
cerfaFields['datePrestationMoisDebut4']            = '';
cerfaFields['commentaireEventuels']                = '';
cerfaFields['activitesProfessionnellesExercees']   = '';




cerfaFields['changementSituatuinPrestataireCoche'] = false;
cerfaFields['renouvellementCoche']                 = false;

cerfaFields['autreObservations']  				   = $qp248PE1.commentaire.commentaire.commentairesEventuels;
cerfaFields['lieuSignature']                       = $qp248PE1.signature.signature.lieuSignature;
cerfaFields['signatureCoche']                      = $qp248PE1.signature.signature.attesteSignature;
cerfaFields['nomSignature']                        = $qp248PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp248PE1.etatCivil.identificationDeclarant.prenomDeclarant;


if($qp248PE1.signature.signature.dateSignature != null) {    
var dateTemp = new Date(parseInt ($qp248PE1.signature.signature.dateSignature.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['dateSignatureJour']                    = day;			
cerfaFields['dateSignatureMois']                    = month;
cerfaFields['dateSignatureAnnee']                   = year;

}
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GE.pdf') //
	.apply({
		dateSignature : $qp248PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil National de l'Ordre des Sages-femmes",
		demandeContexte : "Déclaration initiale de libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire-declaration-LPS-Sage-femme.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomeComplementaire);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifsFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('profession_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Sage-femme - Déclaration initiale de libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration initiale de libre prestation de services pour l\'exercice de la profession de sage-femme',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
