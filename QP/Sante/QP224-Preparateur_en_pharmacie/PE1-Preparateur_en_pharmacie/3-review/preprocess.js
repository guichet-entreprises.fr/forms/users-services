var formFields = {};

var civNomPrenom = $qp224PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp224PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp224PE1.etatCivil.identificationDeclarant.prenomDeclarant;

//Préparateur en pharmacie
formFields['preparateurPharmacie']                                = true;


//etatCivil
formFields['civiliteMonsieur']                                    = ($qp224PE1.etatCivil.identificationDeclarant.civilite=='Monsieur');
formFields['civiliteMadame']                                      = ($qp224PE1.etatCivil.identificationDeclarant.civilite=='Madame');
formFields['declarantNomUsage']                                   = $qp224PE1.etatCivil.identificationDeclarant.nomEpouseDeclarant;
formFields['declarantNomNaissance']                               = $qp224PE1.etatCivil.identificationDeclarant.nomDeclarant;
formFields['declarantPrenoms']                                    = $qp224PE1.etatCivil.identificationDeclarant.prenomDeclarant;
formFields['declarantDateNaissance']                              = $qp224PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
formFields['declarantLieuNaissance']                              = $qp224PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
formFields['declarantPaysNaissance']                              = $qp224PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
formFields['declarantNationalite']                                = $qp224PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
formFields['declarantCourriel']                                   = $qp224PE1.adresse.adressePersonnelle.mailAdresseDeclarant;


//adresse
formFields['declarantAdresseRueComplement']                       = $qp224PE1.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant 
																	+ ($qp224PE1.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ' ' + $qp224PE1.adresse.adressePersonnelle.complementAdresseDeclarant : '');
formFields['declarantAdresseVille']                               = $qp224PE1.adresse.adressePersonnelle.villeAdresseDeclarant;
formFields['declarantAdressePays']                                = $qp224PE1.adresse.adressePersonnelle.paysAdresseDeclarant;
formFields['declarantAdresseCP']                                  = $qp224PE1.adresse.adressePersonnelle.codePostalAdresseDeclarant;
formFields['declarantTelephoneFixe']                              = $qp224PE1.adresse.adressePersonnelle.telephoneAdresseDeclarant;
formFields['declarantTelephoneMobile']                            = $qp224PE1.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;


//diplomeProfession
formFields['declarantDiplomeIntitule']                            = $qp224PE1.diplomeProfession.diplomeProfession.intituleDiplome;
formFields['declarantDiplomePaysObtention']                       = $qp224PE1.diplomeProfession.diplomeProfession.paysObtentionDiplome;
formFields['declarantDiplomeDelivrePar']                          = $qp224PE1.diplomeProfession.diplomeProfession.delivreDiplome;
formFields['declarantDiplomeDateObtention']                       = $qp224PE1.diplomeProfession.diplomeProfession.dateObtentionDiplome;
formFields['declarantDiplomeDateReconnaissance']                  = $qp224PE1.diplomeProfession.diplomeProfession.dateReconnaissanceDiplomeEtat;	

//diplomeDetailles
formFields['diplomeIntitule1']                                    = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome1;
formFields['diplomeDateObtention1']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome1;
formFields['diplomeLieuFormation1']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome1;
formFields['diplomePaysObtention1']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome1;

formFields['diplomeIntitule2']                                    = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome2;
formFields['diplomeDateObtention2']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome2;
formFields['diplomeLieuFormation2']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome2;
formFields['diplomePaysObtention2']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome2; 

formFields['diplomeIntitule3']                                    = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome3;
formFields['diplomeDateObtention3']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome3;
formFields['diplomeLieuFormation3']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome3;
formFields['diplomePaysObtention3']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome3;

formFields['diplomeIntitule4']                                    = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome4;
formFields['diplomeDateObtention4']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome4;
formFields['diplomeLieuFormation4']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome4;
formFields['diplomePaysObtention4']                               = $qp224PE1.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome4;


//exerciceProfessionnel
formFields['exerciceProfessionnelNature1']                        = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurNom1']                  = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement1'] = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurAdresseVillePays1']     = ($qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 != null ? $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 + ' ' : '')
																	+ ($qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 != null ? $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 : '');
formFields['exerciceProfessionnelPeriode1']                       = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1;

formFields['exerciceProfessionnelNature2']                        = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurNom2']                  = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement2'] = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurAdresseVillePays2']     = ($qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 != null ? $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 + ' ' : '')
																	+ ($qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 != null ? $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 : '');
formFields['exerciceProfessionnelPeriode2']                       = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2;

formFields['exerciceProfessionnelNature3']                        = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurNom3']                  = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement3'] = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurAdresseVillePays3']     = ($qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 != null ? $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 + ' ' : '')
																	+ ($qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 != null ? $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 : '');
formFields['exerciceProfessionnelPeriode3']                       = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3;

formFields['exerciceProfessionnelNature4']                        = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurNom4']                  = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement4'] = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurAdresseVillePays4']     = ($qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 != null ? $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 + ' ' : '')
																	+ ($qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 != null ? $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 : '');
formFields['exerciceProfessionnelPeriode4']                       = $qp224PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4;


//projetsProfessionnels
formFields['projetsProfessionnels']                               = $qp224PE1.projetsProfessionnels.projetsProfessionnels.projetsProfessionnels;
formFields['attesteHonneurDemandeUnique']                         = $qp224PE1.signatureGroup.signature.attesteHonneurDemandeUnique;
formFields['regionExercice']                                      = $qp224PE1.signatureGroup.signature.regionExercice;
formFields['signatureDate']                                       = $qp224PE1.signatureGroup.signature.dateSignature;
formFields['signatureLieu']                                       = $qp224PE1.signatureGroup.signature.lieuSignature;
formFields['signature']                                           = 'Je déclare sur l’honneur l\'exactitude des informations de la formalité et signe la présente déclaration.';
formFields['signatureCoche']                                      = $qp224PE1.signatureGroup.signature.certifieHonneur;
formFields['libelleProfession']								  	  = "Préparateur en pharmacie"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp224PE1.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp224PE1.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire LE.pdf') //
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);

var finalDocItem = finalDoc.save('Preparateur_pharmacie_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Préparateur en pharmacie - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de préparateur en pharmacie.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});