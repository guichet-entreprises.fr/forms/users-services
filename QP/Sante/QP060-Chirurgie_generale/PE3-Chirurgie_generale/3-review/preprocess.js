var formFields = {};

var civNomPrenom = $qp060PE3.info2.etatCivile.civilite + ' ' + $qp060PE3.info2.etatCivile.declarantNomNaissance + ' ' + $qp060PE3.info2.etatCivile.declarantPrenoms;


//Profession considérée

formFields['typesActes']                                           = $qp060PE3.info1.profession.typesActes;
formFields['lieuExercice']                                         = $qp060PE3.info1.profession.lieuExercice;
formFields['nomProfessionLPS']                                     = $qp060PE3.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp060PE3.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp060PE3.info1.profession.specialite;
formFields['listeActes']                                           = $qp060PE3.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp060PE3.info1.profession.exerciceAutonome;
formFields['specialiteExerceeEM']                                  = $qp060PE3.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp060PE3.info1.profession.partieProfessionSiAccesPartiel;

//Prestation de service

formFields['declarationInitialeLPS']                               = false;
formFields['renouvellementLPS']                                    = true;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp060PE3.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp060PE3.info2.etatCivile.civilite).eq('monsieur') ? '' : '----';
formFields['declarantNomNaissance']                                = $qp060PE3.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp060PE3.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp060PE3.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp060PE3.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp060PE3.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp060PE3.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp060PE3.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoVille']                           = $qp060PE3.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCourriel']                        = $qp060PE3.info3.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoie']                      = $qp060PE3.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp060PE3.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoCP']                              = $qp060PE3.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp060PE3.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp060PE3.info3.coordonnees1.declarantAdressePersoTelephone;

formFields['declarantAdresseFranceNumNomVoie']                     = $qp060PE3.info3.coordonnees2.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp060PE3.info3.coordonnees2.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceCP']                             = $qp060PE3.info3.coordonnees2.declarantAdresseFranceCP;
formFields['declarantAdresseFranceVille']                          = $qp060PE3.info3.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFrancePays']                           = $qp060PE3.info3.coordonnees2.declarantAdresseFrancePays;
formFields['declarantAdresseFranceTelephone']                      = $qp060PE3.info3.coordonnees2.declarantAdresseFranceTelephone;


//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp060PE3.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp060PE3.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp060PE3.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp060PE3.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp060PE3.info4.diplome.dateReconnaissanceTitreFormationProfession;

formFields['intituleAutreTitreFormationProfession']                = $qp060PE3.info4.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp060PE3.info4.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp060PE3.info4.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp060PE3.info4.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp060PE3.info4.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

formFields['intituleTitreDiplomeSpecialisation']                   = $qp060PE3.info4.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp060PE3.info4.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp060PE3.info4.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp060PE3.info4.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp060PE3.info4.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp060PE3.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp060PE3.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp060PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp060PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp060PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp060PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp060PE3.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp060PE3.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp060PE3.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp060PE3.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp060PE3.info5.ordre.ordreProfessionnelAdressePays != null ? $qp060PE3.info5.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp060PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp060PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp060PE3.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp060PE3.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp060PE3.info6.assuranceProfessionnelle.commentaires;


formFields['prestation1Du']                     				   = $qp060PE3.detailsRenouvellement.renouvellement.periodePreste1.from;
formFields['prestation1Au']                                        = $qp060PE3.detailsRenouvellement.renouvellement.periodePreste1.to;
formFields['prestation2Du']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste2.from : '');
formFields['prestation2Au']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste2.to : '');
formFields['prestation3Du']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste3.from : '');
formFields['prestation3Au']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste3.to : '');
formFields['prestation4Du']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste4.from : '');
formFields['prestation4Au']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste4.to : '');
formFields['prestation5Du']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste5.from : '');
formFields['prestation5Au']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste5.to : '');
formFields['prestation6Du']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste6.from : '');
formFields['prestation6Au']                                        = ($qp060PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp060PE3.detailsRenouvellement.renouvellement.periodePreste6.to : '');


formFields['prestation1ActiviteExercee']                           = $qp060PE3.detailsRenouvellement.renouvellement.prestation1ActiviteExercee;
formFields['prestation2ActiviteExercee']                           = $qp060PE3.detailsRenouvellement.renouvellement.prestation2ActiviteExercee;
formFields['prestation3ActiviteExercee']                           = $qp060PE3.detailsRenouvellement.renouvellement.prestation3ActiviteExercee;
formFields['prestation4ActiviteExercee']                           = $qp060PE3.detailsRenouvellement.renouvellement.prestation4ActiviteExercee;
formFields['prestation5ActiviteExercee']                           = $qp060PE3.detailsRenouvellement.renouvellement.prestation5ActiviteExercee;
formFields['prestation6ActiviteExercee']                           = $qp060PE3.detailsRenouvellement.renouvellement.prestation6ActiviteExercee;

 
 
//Signature

formFields['commentairesSignature']                                = $qp060PE3.info7.signature.commentairesSignature;
formFields['dateSignature']                                        = $qp060PE3.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp060PE3.info7.signature.attesteSignature;


formFields['libelleProfession']				= "Chirurgie générale (médecin spécialiste)"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp060PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp060PE3.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Demande de renouvellement de la déclaration en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Chirurgie generale.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPrecedenteDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Chirurgie_generale_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Chirurgie générale (médecin spécialiste) - renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de la Chirurgie générale (médecin spécialiste)',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
