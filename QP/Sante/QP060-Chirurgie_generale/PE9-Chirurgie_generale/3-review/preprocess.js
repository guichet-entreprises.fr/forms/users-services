function pad(s) { return (s < 10) ? '0' + s : s; } 
var cerfaFields = {};
//etatCivil
var civNomPrenom = $qp060PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp060PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp060PE9.etatCivil.identificationDeclarant.prenomDeclarant;

//Etat civil
cerfaFields['nomDeclarant']                           = $qp060PE9.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']                        = $qp060PE9.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['prenomDeclarantComp']                    = $qp060PE9.etatCivil.identificationDeclarant.prenomDeclarantComp;
cerfaFields['nomUsageDeclarant']                      = $qp060PE9.etatCivil.identificationDeclarant.nomUsageDeclarant;
cerfaFields['nomExerciceDeclarant']                   = $qp060PE9.etatCivil.identificationDeclarant.nomExerciceDeclarant;
cerfaFields['prenomExerciceDeclarant']                = $qp060PE9.etatCivil.identificationDeclarant.prenomExerciceDeclarant;
cerfaFields['masculin']                               = $qp060PE9.etatCivil.identificationDeclarant.civilite == "Monsieur";
cerfaFields['feminin']                                = $qp060PE9.etatCivil.identificationDeclarant.civilite == "Madame";
cerfaFields['nationalite']                            = $qp060PE9.etatCivil.identificationDeclarant.nationaliteDeclarant;

var dateChamp = $qp060PE9.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateNaissanceJour'] = day;
	cerfaFields['dateNaissanceMois'] = month;
	cerfaFields['dateNaissanceAnnee'] = year;
}

cerfaFields['lieuNaissanceVille']                     = $qp060PE9.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['dateNaissanceDepartement']               = $qp060PE9.etatCivil.identificationDeclarant.departementNaissanceDeclarant;
cerfaFields['dateNaissancePays']                      = $qp060PE9.etatCivil.identificationDeclarant.paysNaissanceDeclarant;

//Situation familiale
cerfaFields['celibataire']                            = $qp060PE9.situation.familiale.statut == "Célibataire";
cerfaFields['mariee']                                 = $qp060PE9.situation.familiale.statut == "Marié(e)";
cerfaFields['pacs']                                   = $qp060PE9.situation.familiale.statut == "PACS";
cerfaFields['concubine']                              = $qp060PE9.situation.familiale.statut == "Concubin(e)";
cerfaFields['separee']                                = $qp060PE9.situation.familiale.statut == "Séparé(e)";
cerfaFields['divorce']                                = $qp060PE9.situation.familiale.statut == "Divorcé(e)";
cerfaFields['veuf']                                   = $qp060PE9.situation.familiale.statut == "Veuf(ve)";

cerfaFields['professionConjoint']                     = $qp060PE9.situation.familiale.professionConjoint;
cerfaFields['nombreEnfants']                          = $qp060PE9.situation.familiale.nombreEnfants != null ? $qp060PE9.situation.familiale.nombreEnfants + ' enfant(s)' : '';
cerfaFields['datesNaissanceEnfants']                  = $qp060PE9.situation.familiale.datesNaissanceEnfants;

//langues parlées
cerfaFields['languesParlees']                         = $qp060PE9.situation.langues.parlee;

//adresse personnelle
cerfaFields['adresseDeclarant']                       = $qp060PE9.adresse.adresseContact.numeroLibelleAdresseDeclarant 
														+' '+ ($qp060PE9.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp060PE9.adresse.adresseContact.complementAdresseDeclarant : '')
														+' '+ ($qp060PE9.adresse.adresseContact.codePostalAdresseDeclarant != null ? ', ' + $qp060PE9.adresse.adresseContact.codePostalAdresseDeclarant : '')
														+' '+ $qp060PE9.adresse.adresseContact.villeAdresseDeclarant 
														+', '+ $qp060PE9.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneFixe']                          = $qp060PE9.adresse.adresseContact.telephoneAdresseDeclarant != null ? $qp060PE9.adresse.adresseContact.telephoneAdresseDeclarant : '';
cerfaFields['telephoneMobile']                        = $qp060PE9.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['email']                                  = $qp060PE9.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['fax']                                    = $qp060PE9.adresse.adresseContact.faxAdresseDeclarant != null ? $qp060PE9.adresse.adresseContact.faxAdresseDeclarant : '';

//Diplome de medecin
cerfaFields['diplomeFr']                              = $qp060PE9.diplomeMedecin.etape1.paysDiplome == 'Français';
cerfaFields['diplomeEU']                              = $qp060PE9.diplomeMedecin.etape1.paysDiplome == 'Européen (d\'un Etat de l\'UE ou de l\'EEE)';
cerfaFields['diplomeSuisse']                          = $qp060PE9.diplomeMedecin.etape1.paysDiplome == 'Suisse';
cerfaFields['diplomeAutre']                           = $qp060PE9.diplomeMedecin.etape1.paysDiplome == 'Autre';

var dateChamp = $qp060PE9.diplomeMedecin.etape1.dateObtentionDiplome;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionJour'] = day;
	cerfaFields['dateObtentionMois'] = month;
	cerfaFields['dateObtentionAnnee'] = year;
}
cerfaFields['lieuObtention']                          = $qp060PE9.diplomeMedecin.etape1.facObtentionDiplome;
cerfaFields['paysObtention']                          = $qp060PE9.diplomeMedecin.etape1.paysObtentionDiplome;

//Autorisation ministerielle
cerfaFields['autorisationMinisterielleOui']           = $qp060PE9.autorisation.ministerielle.autorisationMinisterielle == true;
cerfaFields['autorisationMinisterielleNon']           = $qp060PE9.autorisation.ministerielle.autorisationMinisterielle == false;

var dateChamp = $qp060PE9.autorisation.ministerielle.dateAutorisation;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateArreteAutorisationJour'] = day;
	cerfaFields['dateArreteAutorisationMois'] = month;
	cerfaFields['dateArreteAutorisationAnnee'] = year;
}

//Spécialité obtenue dans un etat de l'UE ou de l'EEE ou en Suisse
cerfaFields['specialiteObtenue1']                     = $qp060PE9.autorisation.specialiteObtenue.intituleSpecialite1;

var dateChamp = $qp060PE9.autorisation.specialiteObtenue.dateObtention1;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionSpecialite1Jour'] = day;
	cerfaFields['dateObtentionSpecialite1Mois'] = month;
	cerfaFields['dateObtentionSpecialite1Annee'] = year;
}

cerfaFields['paysObtentionSpecialite1']               = $qp060PE9.autorisation.specialiteObtenue.paysObtention1;
cerfaFields['universiteObtentionSpecialite1']         = $qp060PE9.autorisation.specialiteObtenue.universiteObtention1;

cerfaFields['specialiteObtenue2']                     = $qp060PE9.autorisation.specialiteObtenue.intituleSpecialite2 != null ? $qp060PE9.autorisation.specialiteObtenue.intituleSpecialite2 : '';

var dateChamp = $qp060PE9.autorisation.specialiteObtenue.dateObtention2;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionSpecialite2Jour'] = day;
	cerfaFields['dateObtentionSpecialite2Mois'] = month;
	cerfaFields['dateObtentionSpecialite2Annee'] = year;
}

cerfaFields['paysObtentionSpecialite2']               = $qp060PE9.autorisation.specialiteObtenue.paysObtention2 != null ? $qp060PE9.autorisation.specialiteObtenue.paysObtention2 : '';
cerfaFields['universiteObtentionSpecialite2']         = $qp060PE9.autorisation.specialiteObtenue.universiteObtention2 != null ? $qp060PE9.autorisation.specialiteObtenue.universiteObtention2 : '';

//Qualification en médecine générale
cerfaFields['universiteObtentionEEESuisseUE']         = $qp060PE9.qualification.medecineGenerale.universiteObtention;
cerfaFields['paysObtentionEEESuisseUE']               = $qp060PE9.qualification.medecineGenerale.paysObtention;

var dateChamp = $qp060PE9.qualification.medecineGenerale.dateObtention;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['dateObtentionEEESuisseUEJour'] = day;
	cerfaFields['dateObtentionEEESuisseUEMois'] = month;
	cerfaFields['dateObtentionEEESuisseUEAnnee'] = year;
}

//Inscription ou enregistrement à un ordre en France
cerfaFields['oui17']                                  = $qp060PE9.qualification.inscriptionOrdre.inscriptionOrdre == true;
cerfaFields['non17']                                  = $qp060PE9.qualification.inscriptionOrdre.inscriptionOrdre == false;
cerfaFields['etatsAnneesInscriptionEnregistrement17'] = $qp060PE9.qualification.inscriptionOrdre.inscriptionOrdreOui != null ? $qp060PE9.qualification.inscriptionOrdre.inscriptionOrdreOui : '';
cerfaFields['ouiRefus17']                             = $qp060PE9.qualification.inscriptionOrdre.inscriptionOrdreRejet == true;
cerfaFields['nonRefus17']                             = $qp060PE9.qualification.inscriptionOrdre.inscriptionOrdreRejet == false;
cerfaFields['etatsAnneesRefus17']                     = $qp060PE9.qualification.inscriptionOrdre.inscriptionOrdreRejetOui != null ? $qp060PE9.qualification.inscriptionOrdre.inscriptionOrdreRejetOui : '';

//Inscription ou enregistrement à un ordre à l'étranger
cerfaFields['ouiPaysEtranger18']                      = $qp060PE9.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtranger == true;
cerfaFields['nonPaysEtranger18']                      = $qp060PE9.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtranger == false;
cerfaFields['paysAnneeInscription18']                 = $qp060PE9.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerOui != null ? $qp060PE9.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerOui : '';
cerfaFields['ouiRefus18']                             = $qp060PE9.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejet == true;
cerfaFields['nonRefus18']                             = $qp060PE9.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejet == false;
cerfaFields['paysAnneeRefus18']                       = $qp060PE9.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejetOui != null ? $qp060PE9.qualification.inscriptionOrdreEtranger.inscriptionOrdreEtrangerRejetOui : '';

//Prestation de services
cerfaFields['prestationServiceOui']                   = $qp060PE9.qualification.prestationServices.enregistrement == true;
cerfaFields['prestationServiceNon']                   = $qp060PE9.qualification.prestationServices.enregistrement == false;
cerfaFields['anneePrestationServices19']              = $qp060PE9.qualification.prestationServices.anneeEnregistrement;
cerfaFields['enregistrementPrestationServices19']     = $qp060PE9.qualification.prestationServices.numeroEnregistrement;

//Situation professionnelle dans le departement
cerfaFields['medecineGenerale']                       = $qp060PE9.situationPro.dansDepartement.qualification == 'Médecine générale';
cerfaFields['autreSpecialite']                        = $qp060PE9.situationPro.dansDepartement.qualification == 'Autre spécialité';
cerfaFields['autreSpecialiteReponse']                 = $qp060PE9.situationPro.dansDepartement.autreSpecialitePrecision != null ? $qp060PE9.situationPro.dansDepartement.autreSpecialitePrecision: '';
cerfaFields['activiteMedicaleReguliere']              = $qp060PE9.situationPro.dansDepartement.activite == 'Activité médicale régulière';
cerfaFields['activiteMedicaleIntermittente']          = $qp060PE9.situationPro.dansDepartement.activite == 'Activité médicale intermittente ou remplacements réguliers';
cerfaFields['precisionLiberale']                      = Value('id').of($qp060PE9.situationPro.dansDepartement.activiteIntermittentePrecision).contains('liberal');
cerfaFields['precisionSalariee']                      = Value('id').of($qp060PE9.situationPro.dansDepartement.activiteIntermittentePrecision).contains('salariee');
cerfaFields['aucuneActiviteMedicale']                 = $qp060PE9.situationPro.dansDepartement.activite == 'Aucune activité médicale';

//Adresse de l’activite la plus importante en temps (activite principale)
cerfaFields['adresseActivitePrincipaleLiberal']       = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.typeActivite).eq('liberal') ? true : false;
cerfaFields['adresseActivitePrincipaleHospitalier']   = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['adresseActivitePrincipaleSalarie']       = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.typeActivite).eq('salarie') ? true : false;
cerfaFields['adresseActivitePrincipaleBenevole']      = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.typeActivite).eq('benevole') ? true : false;
cerfaFields['adresseActivitePrincipaleTempsPlein']    = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['adresseActivitePrincipaleTempsPartiel']  = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPartiel') ? true : false;
cerfaFields['fonctionActivitePrincipale']             = $qp060PE9.activitePrincipale.adresseActivite.fonction;
cerfaFields['statutActivitePrincipale']               = $qp060PE9.activitePrincipale.adresseActivite.statut;

var dateChamp = $qp060PE9.activitePrincipale.adresseActivite.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear();
	cerfaFields['jourDebutActivitePrincipale'] = day;
	cerfaFields['moisDebutActivitePrincipale'] = month;
	cerfaFields['anneeebutActivitePrincipale'] = year;
}

cerfaFields['denominationSociale']                    = $qp060PE9.activitePrincipale.adresseActivite.denominationSociale;
cerfaFields['adresseActivitePrincipale']              = $qp060PE9.activitePrincipale.adresseActivite.numeroLibelleAdresseDeclarant 
														+', '+ ($qp060PE9.activitePrincipale.adresseActivite.complementAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite.complementAdresseDeclarant : '') 
														+', '+ ($qp060PE9.activitePrincipale.adresseActivite.codePostalAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite.codePostalAdresseDeclarant : '')
														+' '+ $qp060PE9.activitePrincipale.adresseActivite.villeAdresseDeclarant
														+', '+ $qp060PE9.activitePrincipale.adresseActivite.paysAdresseDeclarant;
cerfaFields['telephoneActivitePrincipale']            = $qp060PE9.activitePrincipale.adresseActivite.telephoneAdresseDeclarant;
cerfaFields['faxActivitePrincipale']                  = $qp060PE9.activitePrincipale.adresseActivite.faxAdresseDeclarant;
cerfaFields['mobileActivitePrincipale']               = $qp060PE9.activitePrincipale.adresseActivite.telephoneMobileAdresseDeclarant;
cerfaFields['emailActivitePrincipale']                = $qp060PE9.activitePrincipale.adresseActivite.mailAdresseDeclarant;
 
 for (var i=0; i<7; i++){
	cerfaFields['autreLieuActiviteLiberal'+i] 						='';
	cerfaFields['autreLieuActiviteHospitalier'+i]					='';
	cerfaFields['autreLieuActiviteSalarie'+i]						='';
	cerfaFields['autreLieuActiviteBenevole'+i]						='';				
	cerfaFields['autreLieuActiviteJour'+i]							='';
	cerfaFields['autreLieuActiviteMois'+i]							='';
	cerfaFields['autreLieuActiviteAnnee'+i]							='';
	cerfaFields['autreLieuActiviteTempsPlein'+i]					='';	
	cerfaFields['autreLieuActiviteTempsPartiel'+i]					='';
	cerfaFields['autreLieuActiviteFonction'+i]						='';
	cerfaFields['autreLieuActiviteStatut'+i]						='';
	cerfaFields['autreLieuActiviteDS'+i]							='';	
	cerfaFields['autreLieuActiviteAdresse'+i]						='';
	cerfaFields['autreLieuActiviteTel'+i]							='';
	cerfaFields['autreLieuActiviteFax'+i]                  			='';	
	cerfaFields['autreLieuActiviteMobile'+i]               			='';
	cerfaFields['autreLieuActiviteEmail'+i]							='';
 }
 
// Lieux d'activités
if($qp060PE9.situationPro.dansDepartement.activite =='Activité médicale régulière'){
cerfaFields['autreLieuActiviteLiberal0']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier0']          = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie0']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole0']             = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp060PE9.activitePrincipale.adresseActivite.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour0'] = day;
	cerfaFields['autreLieuActiviteMois0'] = month;
	cerfaFields['autreLieuActiviteAnnee0'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein0']           = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel0']         = Value('id').of($qp060PE9.activitePrincipale.adresseActivite.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction0']             = $qp060PE9.activitePrincipale.adresseActivite.fonction != null ? $qp060PE9.activitePrincipale.adresseActivite.fonction : '';
cerfaFields['autreLieuActiviteStatut0']               = $qp060PE9.activitePrincipale.adresseActivite.statut != null ? $qp060PE9.activitePrincipale.adresseActivite.statut : '';
cerfaFields['autreLieuActiviteDS0']                   = $qp060PE9.activitePrincipale.adresseActivite.denominationSociale != null ? $qp060PE9.activitePrincipale.adresseActivite.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse0']              = $qp060PE9.activitePrincipale.adresseActivite.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite.adressePostale.complementAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite.adressePostale.codePostalAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp060PE9.activitePrincipale.adresseActivite.adressePostale.villeAdresseDeclarant
														 +', '+ $qp060PE9.activitePrincipale.adresseActivite.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel0']                  = $qp060PE9.activitePrincipale.adresseActivite.telephoneAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax0']                  = $qp060PE9.activitePrincipale.adresseActivite.faxAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile0']               = $qp060PE9.activitePrincipale.adresseActivite.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail0']                = $qp060PE9.activitePrincipale.adresseActivite.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp060PE9.activitePrincipale.adresseActivite.autreLieuActivite1){
cerfaFields['autreLieuActiviteLiberal1']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite1.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier1']          = Value('id').of($qp060PE9.activitePrincipale.adresseActivite1.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie1']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite1.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole1']             = Value('id').of($qp060PE9.activitePrincipale.adresseActivite1.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp060PE9.activitePrincipale.adresseActivite1.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour1'] = day;
	cerfaFields['autreLieuActiviteMois1'] = month;
	cerfaFields['autreLieuActiviteAnnee1'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein1']           = Value('id').of($qp060PE9.activitePrincipale.adresseActivite1.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel1']         = Value('id').of($qp060PE9.activitePrincipale.adresseActivite1.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction1']             = $qp060PE9.activitePrincipale.adresseActivite1.fonction != null ? $qp060PE9.activitePrincipale.adresseActivite1.fonction : '';
cerfaFields['autreLieuActiviteStatut1']               = $qp060PE9.activitePrincipale.adresseActivite1.statut != null ? $qp060PE9.activitePrincipale.adresseActivite1.statut : '';
cerfaFields['autreLieuActiviteDS1']                   = $qp060PE9.activitePrincipale.adresseActivite1.denominationSociale != null ? $qp060PE9.activitePrincipale.adresseActivite1.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse1']              = $qp060PE9.activitePrincipale.adresseActivite1.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite1.adressePostale.complementAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite1.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite1.adressePostale.codePostalAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite1.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp060PE9.activitePrincipale.adresseActivite1.adressePostale.villeAdresseDeclarant
														 +', '+ $qp060PE9.activitePrincipale.adresseActivite1.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel1']                  = $qp060PE9.activitePrincipale.adresseActivite1.telephoneAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite1.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax1']                  = $qp060PE9.activitePrincipale.adresseActivite1.faxAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite1.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile1']               = $qp060PE9.activitePrincipale.adresseActivite1.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail1']                = $qp060PE9.activitePrincipale.adresseActivite1.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp060PE9.activitePrincipale.adresseActivite1.autreLieuActivite2){
cerfaFields['autreLieuActiviteLiberal2']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite2.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier2']          = Value('id').of($qp060PE9.activitePrincipale.adresseActivite2.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie2']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite2.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole2']             = Value('id').of($qp060PE9.activitePrincipale.adresseActivite2.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp060PE9.activitePrincipale.adresseActivite3.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour2'] = day;
	cerfaFields['autreLieuActiviteMois2'] = month;
	cerfaFields['autreLieuActiviteAnnee2'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein2']           = Value('id').of($qp060PE9.activitePrincipale.adresseActivite2.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel2']         = Value('id').of($qp060PE9.activitePrincipale.adresseActivite2.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction2']             = $qp060PE9.activitePrincipale.adresseActivite2.fonction != null ? $qp060PE9.activitePrincipale.adresseActivite2.fonction : '';
cerfaFields['autreLieuActiviteStatut2']               = $qp060PE9.activitePrincipale.adresseActivite2.statut != null ? $qp060PE9.activitePrincipale.adresseActivite2.statut : '';
cerfaFields['autreLieuActiviteDS2']                   = $qp060PE9.activitePrincipale.adresseActivite2.denominationSociale != null ? $qp060PE9.activitePrincipale.adresseActivite2.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse2']              = $qp060PE9.activitePrincipale.adresseActivite2.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite2.adressePostale.complementAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite2.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite2.adressePostale.codePostalAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite2.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp060PE9.activitePrincipale.adresseActivite2.adressePostale.villeAdresseDeclarant
														 +', '+ $qp060PE9.activitePrincipale.adresseActivite2.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel2']                  = $qp060PE9.activitePrincipale.adresseActivite2.telephoneAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite2.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax2']                  = $qp060PE9.activitePrincipale.adresseActivite2.faxAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite2.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile2']               = $qp060PE9.activitePrincipale.adresseActivite2.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail2']                = $qp060PE9.activitePrincipale.adresseActivite2.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp060PE9.activitePrincipale.adresseActivite2.autreLieuActivite3){
cerfaFields['autreLieuActiviteLiberal3']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite3.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier3']          = Value('id').of($qp060PE9.activitePrincipale.adresseActivite3.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie3']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite3.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole3']             = Value('id').of($qp060PE9.activitePrincipale.adresseActivite3.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp060PE9.activitePrincipale.adresseActivite3.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour3'] = day;
	cerfaFields['autreLieuActiviteMois3'] = month;
	cerfaFields['autreLieuActiviteAnnee3'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein3']           = Value('id').of($qp060PE9.activitePrincipale.adresseActivite3.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel3']         = Value('id').of($qp060PE9.activitePrincipale.adresseActivite3.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction3']             = $qp060PE9.activitePrincipale.adresseActivite3.fonction != null ? $qp060PE9.activitePrincipale.adresseActivite3.fonction : '';
cerfaFields['autreLieuActiviteStatut3']               = $qp060PE9.activitePrincipale.adresseActivite3.statut != null ? $qp060PE9.activitePrincipale.adresseActivite3.statut : '';
cerfaFields['autreLieuActiviteDS3']                   = $qp060PE9.activitePrincipale.adresseActivite3.denominationSociale != null ? $qp060PE9.activitePrincipale.adresseActivite3.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse3']              = $qp060PE9.activitePrincipale.adresseActivite3.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite3.adressePostale.complementAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite3.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite3.adressePostale.codePostalAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite3.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp060PE9.activitePrincipale.adresseActivite3.adressePostale.villeAdresseDeclarant
														 +', '+ $qp060PE9.activitePrincipale.adresseActivite3.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel3']                  = $qp060PE9.activitePrincipale.adresseActivite3.telephoneAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite3.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax3']                  = $qp060PE9.activitePrincipale.adresseActivite3.faxAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite3.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile3']               = $qp060PE9.activitePrincipale.adresseActivite3.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail3']                = $qp060PE9.activitePrincipale.adresseActivite3.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp060PE9.activitePrincipale.adresseActivite3.autreLieuActivite4){
cerfaFields['autreLieuActiviteLiberal4']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite4.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier4']          = Value('id').of($qp060PE9.activitePrincipale.adresseActivite4.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie4']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite4.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole4']             = Value('id').of($qp060PE9.activitePrincipale.adresseActivite4.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp060PE9.activitePrincipale.adresseActivite4.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour4'] = day;
	cerfaFields['autreLieuActiviteMois4'] = month;
	cerfaFields['autreLieuActiviteAnnee4'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein4']           = Value('id').of($qp060PE9.activitePrincipale.adresseActivite4.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel4']         = Value('id').of($qp060PE9.activitePrincipale.adresseActivite4.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction4']             = $qp060PE9.activitePrincipale.adresseActivite4.fonction != null ? $qp060PE9.activitePrincipale.adresseActivite4.fonction : '';
cerfaFields['autreLieuActiviteStatut4']               = $qp060PE9.activitePrincipale.adresseActivite4.statut != null ? $qp060PE9.activitePrincipale.adresseActivite4.statut : '';
cerfaFields['autreLieuActiviteDS4']                   = $qp060PE9.activitePrincipale.adresseActivite4.denominationSociale != null ? $qp060PE9.activitePrincipale.adresseActivite4.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse4']              = $qp060PE9.activitePrincipale.adresseActivite4.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite4.adressePostale.complementAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite4.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite4.adressePostale.codePostalAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite4.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp060PE9.activitePrincipale.adresseActivite4.adressePostale.villeAdresseDeclarant
														 +', '+ $qp060PE9.activitePrincipale.adresseActivite4.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel4']                  = $qp060PE9.activitePrincipale.adresseActivite4.telephoneAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite4.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax4']                  = $qp060PE9.activitePrincipale.adresseActivite4.faxAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite4.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile4']               = $qp060PE9.activitePrincipale.adresseActivite4.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail4']                = $qp060PE9.activitePrincipale.adresseActivite4.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp060PE9.activitePrincipale.adresseActivite4.autreLieuActivite5){
cerfaFields['autreLieuActiviteLiberal5']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite5.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier5']          = Value('id').of($qp060PE9.activitePrincipale.adresseActivite5.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie5']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite5.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole5']             = Value('id').of($qp060PE9.activitePrincipale.adresseActivite5.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp060PE9.activitePrincipale.adresseActivite5.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour5'] = day;
	cerfaFields['autreLieuActiviteMois5'] = month;
	cerfaFields['autreLieuActiviteAnnee5'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein5']           = Value('id').of($qp060PE9.activitePrincipale.adresseActivite5.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel5']         = Value('id').of($qp060PE9.activitePrincipale.adresseActivite5.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction5']             = $qp060PE9.activitePrincipale.adresseActivite5.fonction != null ? $qp060PE9.activitePrincipale.adresseActivite5.fonction : '';
cerfaFields['autreLieuActiviteStatut5']               = $qp060PE9.activitePrincipale.adresseActivite5.statut != null ? $qp060PE9.activitePrincipale.adresseActivite5.statut : '';
cerfaFields['autreLieuActiviteDS5']                   = $qp060PE9.activitePrincipale.adresseActivite5.denominationSociale != null ? $qp060PE9.activitePrincipale.adresseActivite5.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse5']              = $qp060PE9.activitePrincipale.adresseActivite5.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite5.adressePostale.complementAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite5.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite5.adressePostale.codePostalAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite5.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp060PE9.activitePrincipale.adresseActivite5.adressePostale.villeAdresseDeclarant
														 +', '+ $qp060PE9.activitePrincipale.adresseActivite5.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel5']                  = $qp060PE9.activitePrincipale.adresseActivite5.telephoneAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite5.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax5']                  = $qp060PE9.activitePrincipale.adresseActivite5.faxAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite5.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile5']               = $qp060PE9.activitePrincipale.adresseActivite5.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail5']                = $qp060PE9.activitePrincipale.adresseActivite5.mailAdresseDeclarant;
}
/* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
if($qp060PE9.activitePrincipale.adresseActivite5.autreLieuActivite6){
cerfaFields['autreLieuActiviteLiberal6']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite6.typeActivite).eq('liberal') ? true : false;
cerfaFields['autreLieuActiviteHospitalier6']          = Value('id').of($qp060PE9.activitePrincipale.adresseActivite6.typeActivite).eq('hospitalier') ? true : false;
cerfaFields['autreLieuActiviteSalarie6']              = Value('id').of($qp060PE9.activitePrincipale.adresseActivite6.typeActivite).eq('salarie') ? true : false;
cerfaFields['autreLieuActiviteBenevole6']             = Value('id').of($qp060PE9.activitePrincipale.adresseActivite6.typeActivite).eq('benevole') ? true : false;

var dateChamp = $qp060PE9.activitePrincipale.adresseActivite6.dateDebut;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
	var monthTemp = dateTemp.getMonth() + 1;
	var month = pad(monthTemp.toString());
	var day =  pad(dateTemp.getDate().toString());
	var year = dateTemp.getFullYear();
	cerfaFields['autreLieuActiviteJour6'] = day;
	cerfaFields['autreLieuActiviteMois6'] = month;
	cerfaFields['autreLieuActiviteAnnee6'] = year;
}
cerfaFields['autreLieuActiviteTempsPlein6']           = Value('id').of($qp060PE9.activitePrincipale.adresseActivite6.tempsActivite).eq('tempsPlein') ? true : false;
cerfaFields['autreLieuActiviteTempsPartiel6']         = Value('id').of($qp060PE9.activitePrincipale.adresseActivite6.tempsActivite).eq('tempsPartiel') ? true : false;

cerfaFields['autreLieuActiviteFonction6']             = $qp060PE9.activitePrincipale.adresseActivite6.fonction != null ? $qp060PE9.activitePrincipale.adresseActivite6.fonction : '';
cerfaFields['autreLieuActiviteStatut6']               = $qp060PE9.activitePrincipale.adresseActivite6.statut != null ? $qp060PE9.activitePrincipale.adresseActivite6.statut : '';
cerfaFields['autreLieuActiviteDS6']                   = $qp060PE9.activitePrincipale.adresseActivite6.denominationSociale != null ? $qp060PE9.activitePrincipale.adresseActivite6.denominationSociale : '';
cerfaFields['autreLieuActiviteAdresse6']              = $qp060PE9.activitePrincipale.adresseActivite6.adressePostale.numeroLibelleAdresseDeclarant 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite6.adressePostale.complementAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite6.adressePostale.complementAdresseDeclarant : '') 
														 +', '+ ($qp060PE9.activitePrincipale.adresseActivite6.adressePostale.codePostalAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite6.adressePostale.codePostalAdresseDeclarant : '')
														 +' '+ $qp060PE9.activitePrincipale.adresseActivite6.adressePostale.villeAdresseDeclarant
														 +', '+ $qp060PE9.activitePrincipale.adresseActivite6.adressePostale.paysAdresseDeclarant;
cerfaFields['autreLieuActiviteTel6']                  = $qp060PE9.activitePrincipale.adresseActivite6.telephoneAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite6.telephoneAdresseDeclarant : '';
cerfaFields['autreLieuActiviteFax6']                  = $qp060PE9.activitePrincipale.adresseActivite6.faxAdresseDeclarant != null ? $qp060PE9.activitePrincipale.adresseActivite6.faxAdresseDeclarant : '';
cerfaFields['autreLieuActiviteMobile6']               = $qp060PE9.activitePrincipale.adresseActivite6.telephoneMobileAdresseDeclarant;
cerfaFields['autreLieuActiviteEmail6']                = $qp060PE9.activitePrincipale.adresseActivite6.mailAdresseDeclarant;
}

//Plaques, ordonnances et annuaires
cerfaFields['libellePlaques']                         = $qp060PE9.plaquesOrdonnances.etape.plaques;
cerfaFields['libelleOrdonnances']                     = $qp060PE9.plaquesOrdonnances.etape.ordonnances;
cerfaFields['annuaires']                              = $qp060PE9.plaquesOrdonnances.etape.annuaires;

//Adresse de correspondance
cerfaFields['adresseCorrespondancePrincipale1']       = Value('id').of($qp060PE9.adressesCorrespondance.adresse.courrier).eq('adresseExercicePrincipal') ? true : false;
cerfaFields['adresseCorrespondancePersonnelle1']      = Value('id').of($qp060PE9.adressesCorrespondance.adresse.courrier).eq('adressePersonnelle') ? true : false;
cerfaFields['adresseCorrespondanceAutre1']            = Value('id').of($qp060PE9.adressesCorrespondance.adresse.courrier).eq('adresseAutre') ? true : false;
cerfaFields['adresseCorrespondanceAutrePrecision1']   = $qp060PE9.adressesCorrespondance.adresse.precisionCourrier != null ? $qp060PE9.adressesCorrespondance.adresse.precisionCourrier : '';

cerfaFields['adresseCorrespondancePrincipale2']       = Value('id').of($qp060PE9.adressesCorrespondance.adresse.rpps).eq('adresseExercicePrincipal') ? true : false;
cerfaFields['adresseCorrespondancePersonnelle2']      = Value('id').of($qp060PE9.adressesCorrespondance.adresse.rpps).eq('adressePersonnelle') ? true : false;
cerfaFields['adresseCorrespondanceAutre2']            = Value('id').of($qp060PE9.adressesCorrespondance.adresse.rpps).eq('adresseAutre') ? true : false;
cerfaFields['adresseCorrespondanceAutrePrecision2']   = $qp060PE9.adressesCorrespondance.adresse.precisionRPPS != null ? $qp060PE9.adressesCorrespondance.adresse.precisionRPPS : '';

cerfaFields['adresseCorrespondanceEmail']             = $qp060PE9.adressesCorrespondance.adresse.email;

//Sanctions prononcées hors France
cerfaFields['sanctionsPrononceeOui']                  = $qp060PE9.sanctions.instances.caractereDefinitif == true;
cerfaFields['sanctionsPrononceeNon']                  = $qp060PE9.sanctions.instances.caractereDefinitif == false;
cerfaFields['sanctionsPrononceeDescription']          = $qp060PE9.sanctions.instances.sanctionsDescription;
cerfaFields['sanctionsPrononceepays']                 = $qp060PE9.sanctions.instances.sanctionsPays;
cerfaFields['sanctionsPrononceeJuridictions']         = $qp060PE9.sanctions.instances.sanctionsJuridiction;
cerfaFields['sanctionsPrononceeDates']                = $qp060PE9.sanctions.instances.sanctionsDates;

cerfaFields['condamnationPenaleOui']                  = $qp060PE9.sanctions.instances.condamnation == true;
cerfaFields['condamnationPenaleNon']                  = $qp060PE9.sanctions.instances.condamnation == false;
cerfaFields['condamnationPenaleDescription']          = $qp060PE9.sanctions.instances.condamnationDescription;
cerfaFields['condamnationPenalePays']                 = $qp060PE9.sanctions.instances.condamnationPays;
cerfaFields['condamnationPenaleJuridictions']         = $qp060PE9.sanctions.instances.condamnationJuridiction;
cerfaFields['condamnationPenaleDates']                = $qp060PE9.sanctions.instances.condamnationDates;

//Instances en cours à l'étranger
cerfaFields['instanceJudiciaireOui']                  = $qp060PE9.sanctions.typesInstances.instanceJuriciaire == true;
cerfaFields['instanceJudiciaireNon']                  = $qp060PE9.sanctions.typesInstances.instanceJuriciaire == false;
cerfaFields['instanceJudiciairePays']                 = $qp060PE9.sanctions.typesInstances.instancePays;
cerfaFields['instanceJudiciaireJuridictions']         = $qp060PE9.sanctions.typesInstances.instanceJuridiction;

cerfaFields['signatureHonneurCodeDeontologie']        = true;
cerfaFields['signatureHonneurAssurance']              = true;
cerfaFields['inscriptionOrdreDepartement']            = $qp060PE9.signature.signature.departementExercice;

var dateChamp = $qp060PE9.signature.signature.dateSignature;
if(dateChamp != null) {
var dateTemp = new Date(parseInt (dateChamp.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    var year = dateTemp.getFullYear().toString();
	cerfaFields['inscriptionOrdreDepartementJour'] = day;
	cerfaFields['inscriptionOrdreDepartementMois'] = month;
	cerfaFields['inscriptionOrdreDepartementAnnee'] = year;
}

cerfaFields['luApprouve']                             = 'Lu et approuvé';
cerfaFields['signature']                              = civNomPrenom;
cerfaFields['oppositionTransmissionDonnees']          = $qp060PE9.signature.signature.signatureOpposition1;
cerfaFields['oppositionCoordonneesPro']               = $qp060PE9.signature.signature.signatureOpposition2;


cerfaFields['libelleProfession']				= "Chirurgie générale"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp060PE9.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp060PE9.signature.signature.dateSignature,
		autoriteHabilitee :"Président du Conseil départemental de l’Ordre des Médecins",
		demandeContexte : "Demande d'inscription au tableau de l’Ordre des Médecins",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/formulaire inscription ordre.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjContrats);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationConformite);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Chirurgie_generale_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Chirurgie générale - Inscription au tableau de l\'Ordre des Médecins',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande d\'inscription au tableau de l\'Ordre des Médecins',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});