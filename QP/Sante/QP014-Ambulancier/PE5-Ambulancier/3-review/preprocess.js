var cerfaFields = {};

var civNomPrenom = $qp014PE5.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp014PE5.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp014PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;


cerfaFields['aideSoignant']                             = false;
cerfaFields['auxiliairePuericulture']                   = false;
cerfaFields['ambulancier']                              = true;
cerfaFields['lpsDemandeInitiale']                       = false;
cerfaFields['lpsRenouvellement']                        = true;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp014PE5.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp014PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp014PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp014PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp014PE5.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp014PE5.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp014PE5.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp014PE5.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp014PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp014PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') + ' ' + ($qp014PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? ', ' + $qp014PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp014PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp014PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' + $qp014PE5.coordonnees.coordonneesDeclarant.declarantAdresseVille + ', ' + $qp014PE5.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp014PE5.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp014PE5.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance : '') 
														  + ' ' + ($qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal : '') 
														  + ' ' + ($qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp014PE5.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionConcernee']                      = $qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee;
cerfaFields['typesActesEnvisages']                      = $qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceComplementAdresse: '');

cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceVille != null ? $qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExercicePays != null ? $qp014PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseNumeroLibellee != null ? $qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseNumeroLibellee : '') + ' ' + ($qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseComplementAdresse != null ? $qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseCodePostal != null ? $qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseCodePostal :'') +' '+ ($qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseVille != null ? $qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseVille : '') + ', ' + ($qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdressePays != null ? $qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp014PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp014PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp014PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp014PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale;


//informations à fournir en cas de renouvellement 
cerfaFields['datePrestationDebut1']                     = $qp014PE5.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['datePrestationFin1']                       = $qp014PE5.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['datePrestationDebut2']                     = ($qp014PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp014PE5.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['datePrestationFin2']                       = ($qp014PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp014PE5.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['datePrestationDebut3']                     = ($qp014PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp014PE5.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['datePrestationFin3']                       = ($qp014PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp014PE5.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['datePrestationDebut4']                     = ($qp014PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp014PE5.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['datePrestationFin4']                       = ($qp014PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp014PE5.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['datePrestationDebut5']                     = ($qp014PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp014PE5.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['datePrestationFin5']                       = ($qp014PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp014PE5.detailsRenouvellement.renouvellement.periodePreste5.to : '');

cerfaFields['commentairesLPSRenouvellement']            = $qp014PE5.detailsRenouvellement.renouvellement.commentairesRenouvellement;
cerfaFields['activiteProfessionnelsLPSRevouvellement']  = $qp014PE5.detailsRenouvellement.renouvellement.activitesProfessionnelles;


// Observations et Signature
cerfaFields['autresObservations']                       = $qp014PE5.observations.observationsSignature.autresObservations;
cerfaFields['signatureCoche']                           = $qp014PE5.observations.observationsSignature.signatureCoche;
cerfaFields['signatureDate']                            = $qp014PE5.observations.observationsSignature.signatureDate;
cerfaFields['signature']                                ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'
cerfaFields['libelleProfession']							= "Ambulancier"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp014PE5.observations.observationsSignature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp014PE5.observations.observationsSignature.signatureDate,
		autoriteHabilitee :"Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/ambulancier.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));




function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieDeclaration);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('ambulancier_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ambulancier - renouvellement de la déclaration en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession d\'ambulancier.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});