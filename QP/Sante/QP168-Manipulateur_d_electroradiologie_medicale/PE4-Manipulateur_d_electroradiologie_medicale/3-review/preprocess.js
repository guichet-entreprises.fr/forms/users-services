var cerfaFields = {};

var civNomPrenom = $qp168PE4.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp168PE4.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp168PE4.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['audioprothesiste']                          = false;
cerfaFields['conseillerGenetique']                       = false;
cerfaFields['preparateurPharmacie']                      = false;
cerfaFields['preparateurPharmacieHospitaliere']          = false;
cerfaFields['infirmier']                       			 = false;
cerfaFields['infirmierSpecialise']                       = false;
cerfaFields['infirmierSpecialiseAnesthesiste']           = false;
cerfaFields['infirmierSpecialiseBlocOperatoire']         = false;
cerfaFields['infirmierSpecialisePuericultrice']          = false;
cerfaFields['pedicurePodologue']                       	 = false;
cerfaFields['ergotherapeuthe']                       	 = false;
cerfaFields['psychomotricien']                       	 = false;
cerfaFields['orthophoniste']                       	     = false;
cerfaFields['orthoptiste']                       		 = false;
cerfaFields['manipulateurElectroradiologie']             = true;
cerfaFields['opticienLunetier']                          = false;
cerfaFields['dieteticien']                       		 = false;

cerfaFields['declarationInitiale']                       = true;
cerfaFields['renouvellement']                       	 = false;



//Identité du demandeur

cerfaFields['declarantNom']                             = $qp168PE4.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp168PE4.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp168PE4.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp168PE4.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp168PE4.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp168PE4.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp168PE4.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp168PE4.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp168PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp168PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') 
														+ ' ' + ($qp168PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp168PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp168PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp168PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' 
														+ $qp168PE4.coordonnees.coordonneesDeclarant.declarantAdresseVille + ' ' + $qp168PE4.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp168PE4.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp168PE4.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance : '') + ' ' 
														+ ($qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal: '') 
														+ ' ' + ($qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp168PE4.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionExerceeEM']     					= $qp168PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionExerceeEM;
cerfaFields['professionExerceeSpecialiteEM']  			= $qp168PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM != null ? $qp168PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM : '';
cerfaFields['professionConcernee']                      = $qp168PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee != null ? $qp168PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee : '';
cerfaFields['professionConcerneeSpecialite']            = $qp168PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance != null ? $qp168PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance : '';
cerfaFields['typesActesEnvisages']                      = $qp168PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');
cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp168PE4.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') 
														+ ' ' + ($qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') 
														+' '+ ($qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille : '') 
														+ ', ' + ($qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp168PE4.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp168PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp168PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp168PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale != null ? $qp168PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale :'';


// Observations et Signature
cerfaFields['autresObservations']              	= $qp168PE4.observation.observation.autresObservations != null ? $qp168PE4.observation.observation.autresObservations :'';
cerfaFields['signatureCoche']             		= $qp168PE4.signature.signature1.signatureDeclarant;
cerfaFields['signatureDate']             		= $qp168PE4.signature.signature1.signatureDate;
cerfaFields['signature']                        ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

//informations à fournir en cas de renouvellement 
cerfaFields['datePrestationDebut1']                     = '';
cerfaFields['datePrestationFin1']                       = '';
cerfaFields['datePrestationDebut2']                     = '';
cerfaFields['datePrestationFin2']                       = '';
cerfaFields['datePrestationDebut3']                     = '';
cerfaFields['datePrestationFin3']                       = '';
cerfaFields['datePrestationDebut4']                     = '';
cerfaFields['datePrestationFin4']                       = '';
cerfaFields['datePrestationDebut5']                     = '';
cerfaFields['datePrestationFin5']                       = '';

cerfaFields['commentairesLPSRenouvellement']            = '';
cerfaFields['activiteProfessionnelsLPSRevouvellement']  = '';


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp168PE4.signature.signature1.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp168PE4.signature.signature1.signatureDate,
		autoriteHabilitee :"Ministère chargé de la santé : Direction Générale de l’Offre de Soins",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Formulaire_LPS.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));
function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Manipulateur_electroradiologie_medicale_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Manipulateur d\'électroradiologie médicale - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de manipulateur d\'électroradiologie médicale.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});