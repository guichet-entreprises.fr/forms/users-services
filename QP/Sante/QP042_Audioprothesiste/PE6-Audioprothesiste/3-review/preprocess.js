var formFields = {};

var civNomPrenom = $qp042PE6.etatCivilPage.etatCivilGroupe.civilite +' '+ $qp042PE6.etatCivilPage.etatCivilGroupe.nomNaissance +' '+ $qp042PE6.etatCivilPage.etatCivilGroupe.prenoms;

/**********************************************************************************
 * Profession para-médicale
 **********************************************************************************/

formFields['audioprothesiste']                   = true;
formFields['infirmier']                          = false;
formFields['oculariste']                         = false;
formFields['orthoprothesiste']                   = false;
formFields['psychomotricien']                    = false;
formFields['dieteticien']                        = false;
formFields['infirmierPsy']                       = false;
formFields['opticienLunetier']                   = false;
formFields['orthoptiste']                        = false;
formFields['technicienLabo']                     = false;
formFields['epithesiste']                   	 = false;
formFields['manipulateurERM']                    = false;
formFields['orthopedisteOrthesiste']             = false;
formFields['pedicurePodologue']                  = false;
formFields['ergotherapeute']                     = false;
formFields['masseurKine']                        = false;
formFields['orthophoniste']                      = false;
formFields['podoOrthesiste']                     = false;


/**********************************************************************************
 * Etat civil
 **********************************************************************************/
var cheminEtatCivil = $qp042PE6.etatCivilPage.etatCivilGroupe;
//
formFields['madame']                     	     = (cheminEtatCivil.civilite=='Madame');
formFields['mademoiselle']                       = false;
formFields['monsieur']                   		 = (cheminEtatCivil.civilite=='Monsieur');
formFields['nomExercice']                        = cheminEtatCivil.nomExercice;
formFields['prenoms']                            = cheminEtatCivil.prenoms;
formFields['nomNaissance']                       = cheminEtatCivil.nomNaissance;
formFields['lieuNaissance']                      = cheminEtatCivil.lieuNaissance;
formFields['depNaissance']                       = cheminEtatCivil.depNaissance;
formFields['dateNaissance']                      = cheminEtatCivil.dateNaissance;
formFields['nationaliteFR']                      = Value('id').of(cheminEtatCivil.nationalite).eq('nationaliteFR') ? true : false;
formFields['nationaliteUEE']                     = Value('id').of(cheminEtatCivil.nationalite).eq('nationaliteUEE') ? true : false;
formFields['autreNationalite']                   = Value('id').of(cheminEtatCivil.nationalite).eq('autreNationalite') ? true : false;
formFields['autreNationalitePreciser']           = cheminEtatCivil.autreNationalitePreciser != null ? cheminEtatCivil.autreNationalitePreciser : ' ';
formFields['nationaliteUEEPreciser']             = cheminEtatCivil.nationaliteUEEPreciser;
formFields['languesParlees']                     = cheminEtatCivil.languesParlees;


/**********************************************************************************
 * Coordonnées
 **********************************************************************************/
var cheminCoordonnees = $qp042PE6.coordonnePage.coordonneGroupe;

formFields['adresse']                            = cheminCoordonnees.adresse;
formFields['complementAdresse']                  = cheminCoordonnees.complementAdresse != null ?cheminCoordonnees.complementAdresse : '';
formFields['codePostal']                         = cheminCoordonnees.codePostal;
formFields['communeVille']                       = cheminCoordonnees.communeVille +", " + cheminCoordonnees.pays;
formFields['numTel']                             = cheminCoordonnees.numTel;
formFields['email']                              = cheminCoordonnees.email;


/**********************************************************************************
 * Situation professionnelle 
 **********************************************************************************/
var cheminSituationPro = $qp042PE6.situationProPage.situationProGroupe;

formFields['premierAnneeActiviteDiplome']        = cheminSituationPro.premierAnneeActiviteDiplome;
formFields['liberal']                            = Value('id').of(cheminSituationPro.profession).eq('liberal') ? true : false;
formFields['salarie']                            = Value('id').of(cheminSituationPro.profession).eq('salarie') ? true : false;
formFields['autreActif']                         = Value('id').of(cheminSituationPro.profession).eq('autreActif') ? true : false;
formFields['autreInactif']                       = Value('id').of(cheminSituationPro.profession).eq('autreInactif') ? true : false;
formFields['indepArtisanCommercant']             = Value('id').of(cheminSituationPro.profession).eq('indepArtisanCommercant') ? true : false;
formFields['activiteMixte']                      = Value('id').of(cheminSituationPro.profession).eq('activiteMixte') ? true : false;
formFields['retraite']                           = Value('id').of(cheminSituationPro.profession).eq('retraite') ? true : false;
formFields['deptPrecedent']                      = cheminSituationPro.deptPrecedent;


/**********************************************************************************
 * Titres et qualifications professionnelles
 **********************************************************************************/
var cheminTitreQualifPro = $qp042PE6.titreQualifProPage.titreQualifProGroupe;

formFields['numDiplome']                         = cheminTitreQualifPro.numDiplome;
formFields['dateObtentionDiplome']               = cheminTitreQualifPro.dateObtentionDiplome;
formFields['lieuObtentionDiplome']               = cheminTitreQualifPro.lieuObtentionDiplome;
formFields['diplomeFrancais']                    = Value('id').of(cheminTitreQualifPro.typeDiplome).eq('diplomeFrancais') ? true : false;
formFields['diplomeUE']              		     = Value('id').of(cheminTitreQualifPro.typeDiplome).eq('diplomeUE') ? true : false;
formFields['diplomeEtranger']               	 = Value('id').of(cheminTitreQualifPro.typeDiplome).eq('diplomeEtranger') ? true : false;
formFields['dateAutorisationExercice']   		 = cheminTitreQualifPro.dateAutorisationExercice = null ? ' ' : cheminTitreQualifPro.dateAutorisationExercice ;
formFields['specialiteOUI']                      = cheminTitreQualifPro.specialiteOuiNon ? true : false;
formFields['specialiteNON']                      = cheminTitreQualifPro.specialiteOuiNon ? false: true;
formFields['anneeSpecialite']                    = cheminTitreQualifPro.anneeSpecialite = null ? '' : cheminTitreQualifPro.anneeSpecialite;
formFields['preciserSpecialite']                 = cheminTitreQualifPro.preciserSpecialite = null ? '' : cheminTitreQualifPro.preciserSpecialite;
formFields['agrementMinisteriel']                = Value('id').of(cheminTitreQualifPro.attestationCapacite).contains('agrementMinisteriel') ? true : false;
formFields['agrementCRAM']                       = Value('id').of(cheminTitreQualifPro.attestationCapacite).contains('agrementCRAM') ? true : false;
formFields['agrementAutre']                      = Value('id').of(cheminTitreQualifPro.attestationCapacite).contains('agrementAutre') ? true : false;
 

/**********************************************************************************
 * Secteur libéral
 **********************************************************************************/

//------------------------------------ Remplacement exclusif ------------------------------------//
var cheminRemplacementExclusif = $qp042PE6.remplacementExclusifPage.remplacementExclusifGroupe;

formFields['remplacementExclusif']               = cheminRemplacementExclusif.remplacementExclusif;
formFields['dateDebutRemplacement']              = cheminRemplacementExclusif.dateDebutRemplacement;


//------------------------------------ Activité principale ------------------------------------//
var cheminActivitePrincipale = $qp042PE6.activitePrincipalePage.activitePrincipaleGroupe;

formFields['exerciceEnCabinet']               	 = Value('id').of(cheminActivitePrincipale.activitePrincipale).eq('exerciceEnCabinet') ? true :false;
formFields['exerciceEnEtablissementDeSoins']     = Value('id').of(cheminActivitePrincipale.activitePrincipale).eq('exerciceEnEtablissementDeSoins') ? true :false;
formFields['autreExerciceSalarie']               = Value('id').of(cheminActivitePrincipale.activitePrincipale).eq('autreExerciceSalarie') ? true :false;
formFields['activitesIndependantes']             = Value('id').of(cheminActivitePrincipale.activitePrincipale).eq('activitesIndependantes') ? true :false;

//------------------------------------ Exercice en cabinet ------------------------------------//

var cheminExerciceCabinet = $qp042PE6.secteurLiberalPage.exerciceCabinet;

formFields['exoGroupe']                          = Value('id').of(cheminExerciceCabinet.typeExerciceCabinet).eq('exoGroupe') ? true : false;
formFields['exoIndividuel']                      = Value('id').of(cheminExerciceCabinet.typeExerciceCabinet).eq('exoIndividuel') ? true :false;
formFields['nomRaisonSociale']                   = cheminExerciceCabinet.nomRaisonSociale;
formFields['dateInstalationCabinet']             = cheminExerciceCabinet.dateInstalationCabinet;
formFields['siret']                              = cheminExerciceCabinet.siret;
formFields['adresseCabinet']                     = cheminExerciceCabinet.adresseCabinet;
formFields['complementAdresseCabinet']           = cheminExerciceCabinet.complementAdresseCabinet != null ? cheminExerciceCabinet.complementAdresseCabinet : ' ' ;
formFields['codePostalCabinet']                  = cheminExerciceCabinet.codePostalCabinet;
formFields['communeVilleCabinet']                = cheminExerciceCabinet.communeVilleCabinet;
formFields['numTelCabinet']                      = cheminExerciceCabinet.numTelCabinet;
formFields['faxCabinet']                         = cheminExerciceCabinet.faxCabinet;
formFields['emailCabinet']                       = cheminExerciceCabinet.emailCabinet;
formFields['societeSCP']                         = Value('id').of(cheminExerciceCabinet.typeSocieteExo).eq('societeSCP') ? true :false;
formFields['societeSEL']                         = Value('id').of(cheminExerciceCabinet.typeSocieteExo).eq('societeSEL') ? true :false;
formFields['autreSociete']                       = Value('id').of(cheminExerciceCabinet.typeSocieteExo).eq('autreSociete') ? true :false;

formFields['cabinetSecondaireOUI']               = Value('id').of(cheminExerciceCabinet.cabinetSecondaireOuiNon).eq('cabinetSecondaire') ? true :false;
formFields['autreImplentationOuiNon']            = Value('id').of(cheminExerciceCabinet.cabinetSecondaireOuiNon).eq('autreImplantation') ? true :false;	

//------------------------------------ Cabinet secondaire ------------------------------------//
var cheminExerciceCabinetSecondaire = $qp042PE6.secteurLiberalPage.exerciceCabinet.cabinetSecondaire;

formFields['adresseCabinetSecondaire']           = cheminExerciceCabinetSecondaire.adresseCabinetSecondaire;
formFields['complementAdresseCabinetSecondaire'] = cheminExerciceCabinetSecondaire.complementAdresseCabinetSecondaire != null ? cheminExerciceCabinetSecondaire.complementAdresseCabinetSecondaire : ' ';
formFields['codePostalCabinetSecondaire']        = cheminExerciceCabinetSecondaire.codePostalCabinetSecondaire;
formFields['communeVilleCabinetSecondaire']      = cheminExerciceCabinetSecondaire.communeVilleCabinetSecondaire;
formFields['numTelCabinetSecondaire']            = cheminExerciceCabinetSecondaire.numTelCabinetSecondaire;
formFields['faxCabinetSecondaire']               = cheminExerciceCabinetSecondaire.faxCabinetSecondaire;
formFields['emailCabinetSecondaire']             = cheminExerciceCabinetSecondaire.emailCabinetSecondaire;


/**********************************************************************************
 * Activités salariés 
 **********************************************************************************/

//------------------------------------ Exercice en établissement de soins ------------------------------------//
var cheminEtablissementSoin = $qp042PE6.etablissementSoinPage.etablissementSoinGroupe;

formFields['nomRaisonSocialeSalarie']            = cheminEtablissementSoin.nomRaisonSocialeSalarie;
formFields['datePriseFonction']                  = cheminEtablissementSoin.datePriseFonction;
formFields['adresseSalarie']                     = cheminEtablissementSoin.adresseSalarie;
formFields['complementAdresseSalarie']           = cheminEtablissementSoin.complementAdresseSalarie != null ? cheminEtablissementSoin.complementAdresseSalarie : ' ';
formFields['codePostalSalarie']                  = cheminEtablissementSoin.codePostalSalarie;
formFields['communeVilleSalarie']                = cheminEtablissementSoin.communeVilleSalarie;
formFields['numTelSalarie']                      = cheminEtablissementSoin.numTelSalarie;
formFields['faxSalarie']                         = cheminEtablissementSoin.faxSalarie;
formFields['emailSalarie']                       = cheminEtablissementSoin.emailSalarie;


//------------------------------------ Autre exercice salarié ------------------------------------//
var cheminAutreExerciceSalarie = $qp042PE6.autreExerciceSalariePage.autreExerciceSalarieGroupe;

formFields['nomRaisonSocialeAutreSalarie']       = cheminAutreExerciceSalarie.nomRaisonSocialeAutreSalarie;
formFields['datePriseFonctionAutreSalarie']      = cheminAutreExerciceSalarie.datePriseFonctionAutreSalarie;
formFields['siretAutreSalarie']                  = cheminAutreExerciceSalarie.siretAutreSalarie;
formFields['adresseAutreSalarie']                = cheminAutreExerciceSalarie.adresseAutreSalarie;
formFields['complementAdresseAutreSalarie']      = cheminAutreExerciceSalarie.complementAdresseAutreSalarie != null ? cheminAutreExerciceSalarie.complementAdresseAutreSalarie : ' ';
formFields['communeVilleAutreSalarie']           = cheminAutreExerciceSalarie.communeVilleAutreSalarie;
formFields['codePostalAutreSalarie']             = cheminAutreExerciceSalarie.codePostalAutreSalarie;
formFields['numTelAutreSalarie']                 = cheminAutreExerciceSalarie.numTelAutreSalarie;
formFields['faxAutreSalarie']                    = cheminAutreExerciceSalarie.faxAutreSalarie;
formFields['emailAutreSalarie']                  = cheminAutreExerciceSalarie.emailAutreSalarie;

//------------------------------------ Activité indépendante ------------------------------------//
var cheminActiviteIndependante = $qp042PE6.independantPage.independantGroupe;

formFields['nomRaisonSocialeIndependant']        = cheminActiviteIndependante.nomRaisonSocialeIndependant;
formFields['dateDebutActiviteIndependant']       = cheminActiviteIndependante.dateDebutActiviteIndependant;
formFields['siretIndependant']                   = cheminActiviteIndependante.siretIndependant;
formFields['adresseIndependant']                 = cheminActiviteIndependante.adresseIndependant;
formFields['complementAdresseIndependant']       = cheminActiviteIndependante.complementAdresseIndependant != null ? cheminActiviteIndependante.complementAdresseIndependant : ' ';
formFields['codePostalIndependant']              = cheminActiviteIndependante.codePostalIndependant;
formFields['numTelIndependant']                  = cheminActiviteIndependante.numTelIndependant;
formFields['faxIndependant']                     = cheminActiviteIndependante.faxIndependant;
formFields['communeVilleIndependant']            = cheminActiviteIndependante.communeVilleIndependant;
formFields['emailIndependant']                   = cheminActiviteIndependante.emailIndependant;

/**********************************************************************************
 * Signature
 **********************************************************************************/
var cheminSignature = $qp042PE6.signaturePage.signatureGroupe;

formFields['dateSignature']                      = cheminSignature.dateSignature;
formFields['declarationHonneur']                 = cheminSignature.declarationHonneur;
formFields['signature']                     	 = civNomPrenom;




var finalDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
.apply({
	dateSignature: $qp042PE6.signaturePage.signatureGroupe.dateSignature,
	autoriteHabilitee :"ARS",
	demandeContexte : "Inscription au répertoire ADELI",
	civiliteNomPrenom : civNomPrenom
});

var cerfaDoc = nash.doc //
.load('models/cerfa_10906-06.pdf') //
.apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));

/**********************************************************************************
 * Ajout des PJs
 **********************************************************************************/

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}


appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitreFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorisation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjActivite);



var finalDocItem = finalDoc.save('Audioprothesiste_RQP.pdf');


return spec.create({
id : 'review',
label : 'Audioprothésiste - Inscription au répertoire ADELI.',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : 'Demande d\'inscription au répertoire ADELI.',
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});