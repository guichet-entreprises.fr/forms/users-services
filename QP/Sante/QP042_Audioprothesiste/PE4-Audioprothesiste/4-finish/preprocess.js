

return spec.create({
    id : 'finish',
    label : 'Remerciements',
    groups : [ spec.createGroup({
        id : 'thanks',
        description: 'Merci d\'avoir utilisé le nouveau gestionnaire de formalité !'
    }) ]
});
