var formFields = {};

var civNomPrenom = $qp249PE2.info2.etatCivile.civilite + ' ' + $qp249PE2.info2.etatCivile.declarantNomNaissance + ' ' + $qp249PE2.info2.etatCivile.declarantPrenoms;


//Profession considérée

formFields['typesActes']                                           = $qp249PE2.info1.profession.typesActes;
formFields['lieuExercice']                                         = '';
formFields['nomProfessionLPS']                                     = $qp249PE2.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp249PE2.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp249PE2.info1.profession.specialite;
formFields['listeActes']                                           = $qp249PE2.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp249PE2.info1.profession.exerciceAutonome;
formFields['specialiteExerceeEM']                                  = $qp249PE2.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp249PE2.info1.profession.partieProfessionSiAccesPartiel;

//Prestation de service

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp249PE2.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp249PE2.info2.etatCivile.civilite).eq('monsieur') ? '' : '----';
formFields['declarantNomNaissance']                                = $qp249PE2.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp249PE2.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp249PE2.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp249PE2.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp249PE2.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp249PE2.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp249PE2.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoVille']                           = $qp249PE2.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCourriel']                        = $qp249PE2.info3.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoie']                      = $qp249PE2.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp249PE2.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoCP']                              = $qp249PE2.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp249PE2.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp249PE2.info3.coordonnees1.declarantAdressePersoTelephone;

formFields['declarantAdresseFranceNumNomVoie']                     = $qp249PE2.info3.coordonnees2.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp249PE2.info3.coordonnees2.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceCP']                             = $qp249PE2.info3.coordonnees2.declarantAdresseFranceCP;
formFields['declarantAdresseFranceVille']                          = $qp249PE2.info3.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFrancePays']                           = $qp249PE2.info3.coordonnees2.declarantAdresseFrancePays;
formFields['declarantAdresseFranceTelephone']                      = $qp249PE2.info3.coordonnees2.declarantAdresseFranceTelephone;


//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp249PE2.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp249PE2.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp249PE2.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp249PE2.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp249PE2.info4.diplome.dateReconnaissanceTitreFormationProfession;

formFields['intituleAutreTitreFormationProfession']                = $qp249PE2.info4.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp249PE2.info4.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp249PE2.info4.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp249PE2.info4.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp249PE2.info4.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

formFields['intituleTitreDiplomeSpecialisation']                   = $qp249PE2.info4.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp249PE2.info4.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp249PE2.info4.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp249PE2.info4.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp249PE2.info4.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp249PE2.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp249PE2.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp249PE2.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp249PE2.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp249PE2.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp249PE2.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp249PE2.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp249PE2.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp249PE2.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp249PE2.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp249PE2.info5.ordre.ordreProfessionnelAdressePays != null ? $qp249PE2.info5.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp249PE2.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp249PE2.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp249PE2.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp249PE2.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp249PE2.info6.assuranceProfessionnelle.commentaires;
 
//Signature

formFields['commentairesSignature']                                = $qp249PE2.info7.signature.commentairesSignature;
formFields['dateSignature']                                        = $qp249PE2.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp249PE2.info7.signature.attesteSignature;


formFields['prestation1Du']                     				   = '';
formFields['prestation1Au']                                        = '';
formFields['prestation2Du']                                        = '';
formFields['prestation2Au']                                        = '';
formFields['prestation3Du']                                        = '';
formFields['prestation3Au']                                        = '';
formFields['prestation4Du']                                        = '';
formFields['prestation4Au']                                        = '';
formFields['prestation5Du']                                        = '';
formFields['prestation5Au']                                        = '';
formFields['prestation6Du']                                        = '';
formFields['prestation6Au']                                        = '';


formFields['prestation1ActiviteExercee']                           = '';
formFields['prestation2ActiviteExercee']                           = '';
formFields['prestation3ActiviteExercee']                           = '';
formFields['prestation4ActiviteExercee']                           = '';
formFields['prestation5ActiviteExercee']                           = '';
formFields['prestation6ActiviteExercee']                           = '';



formFields['libelleProfession']				= "Santé publique"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp249PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp249PE2.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/sante_publique.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEquivalentDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTraductionDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjObligationsCommunautaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Sante_publique_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Santé publique - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de Santé publique',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
