var formFields = {};

var civNomPrenom = $qp141PE2.info2.etatCivile.civilite + ' ' + $qp141PE2.info2.etatCivile.declarantNomNaissance + ' ' + $qp141PE2.info2.etatCivile.declarantPrenoms;



//Prestation de service

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp141PE2.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp141PE2.info2.etatCivile.civilite).eq('monsieur') ? '' : '-----';
formFields['declarantNomNaissance']                                = $qp141PE2.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp141PE2.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp141PE2.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp141PE2.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp141PE2.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp141PE2.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp141PE2.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoVille']                           = $qp141PE2.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCourriel']                        = $qp141PE2.info3.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoie']                      = $qp141PE2.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp141PE2.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoCP']                              = $qp141PE2.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp141PE2.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp141PE2.info3.coordonnees1.declarantAdressePersoTelephone;

formFields['declarantAdresseFranceNumNomVoie']                     = $qp141PE2.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp141PE2.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceCP']                             = $qp141PE2.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceCP;
formFields['declarantAdresseFranceVille']                          = $qp141PE2.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceVille;
formFields['declarantAdresseFrancePays']                           = $qp141PE2.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFrancePays;
formFields['declarantAdresseFranceTelephone']                      = $qp141PE2.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceTelephone;

//Profession considérée

formFields['typesActes']                                           = $qp141PE2.info1.profession.typesActes;
formFields['lieuExercice']                                         = $qp141PE2.info1.profession.lieuExercice;
formFields['nomProfessionLPS']                                     = $qp141PE2.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp141PE2.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp141PE2.info1.profession.specialite;
formFields['listeActes']                                           = $qp141PE2.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp141PE2.info1.profession.exerciceAutonome ? "Oui" : "Non";
formFields['specialiteExerceeEM']                                  = $qp141PE2.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp141PE2.info1.profession.partieProfessionSiAccesPartiel;

//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp141PE2.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp141PE2.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp141PE2.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp141PE2.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp141PE2.info4.diplome.dateReconnaissanceTitreFormationProfession;

//Autre diplôme

formFields['intituleAutreTitreFormationProfession']                = $qp141PE2.autreDiplomePage.autreDiplomeGroupe.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp141PE2.autreDiplomePage.autreDiplomeGroupe.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp141PE2.autreDiplomePage.autreDiplomeGroupe.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp141PE2.autreDiplomePage.autreDiplomeGroupe.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp141PE2.autreDiplomePage.autreDiplomeGroupe.dateReconnaissanceAutreTitreFormationProfession;

//Diplôme de spécialité 

formFields['intituleTitreDiplomeSpecialisation']                   = $qp141PE2.specialiteDiplomePage.specialiteDiplomeGroupe.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp141PE2.specialiteDiplomePage.specialiteDiplomeGroupe.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp141PE2.specialiteDiplomePage.specialiteDiplomeGroupe.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp141PE2.specialiteDiplomePage.specialiteDiplomeGroupe.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp141PE2.specialiteDiplomePage.specialiteDiplomeGroupe.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp141PE2.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp141PE2.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';
formFields['ordreProfessionnelNom']								   = $qp141PE2.info5.ordre.ordreProfessionnelNom != null ? $qp141PE2.info5.ordre.ordreProfessionnelNom : '';
formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp141PE2.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp141PE2.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp141PE2.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp141PE2.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp141PE2.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp141PE2.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp141PE2.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp141PE2.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp141PE2.info5.ordre.ordreProfessionnelAdressePays != null ? $qp141PE2.info5.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp141PE2.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp141PE2.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp141PE2.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp141PE2.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp141PE2.info6.assuranceProfessionnelle.commentaires;

//Commentaires
formFields['commentairesSignature']                                = $qp141PE2.commentairePage.commentaireGroupe.commentairesSignature;

//Signature

formFields['dateSignature']                                        = $qp141PE2.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp141PE2.info7.signature.attesteSignature;


formFields['prestation1Du']                     				   = '';
formFields['prestation1Au']                                        = '';
formFields['prestation2Du']                                        = '';
formFields['prestation2Au']                                        = '';
formFields['prestation3Du']                                        = '';
formFields['prestation3Au']                                        = '';
formFields['prestation4Du']                                        = '';
formFields['prestation4Au']                                        = '';
formFields['prestation5Du']                                        = '';
formFields['prestation5Au']                                        = '';
formFields['prestation6Du']                                        = '';
formFields['prestation6Au']                                        = '';


formFields['prestation1ActiviteExercee']                           = '';
formFields['prestation2ActiviteExercee']                           = '';
formFields['prestation3ActiviteExercee']                           = '';
formFields['prestation4ActiviteExercee']                           = '';
formFields['prestation5ActiviteExercee']                           = '';
formFields['prestation6ActiviteExercee']                           = '';


formFields['libelleProfession']				                       = "Génétique médicale (médecin spécialiste)"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp141PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp141PE2.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Genetique_medicale.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEquivalentDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTraductionDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjObligationsCommunautaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Genetique_medicale_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Génétique médicale (médecin spécialiste) - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de génétique médicale (médecin spécialiste)',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
