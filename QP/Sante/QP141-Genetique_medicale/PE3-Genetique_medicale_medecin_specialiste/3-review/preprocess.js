var formFields = {};

var civNomPrenom = $qp141PE3.info2.etatCivile.civilite + ' ' + $qp141PE3.info2.etatCivile.declarantNomNaissance + ' ' + $qp141PE3.info2.etatCivile.declarantPrenoms;


//Prestation de service

formFields['declarationInitialeLPS']                               = false;
formFields['renouvellementLPS']                                    = true;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp141PE3.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp141PE3.info2.etatCivile.civilite).eq('monsieur') ? '' : '-----';
formFields['declarantNomNaissance']                                = $qp141PE3.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp141PE3.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp141PE3.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp141PE3.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp141PE3.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp141PE3.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp141PE3.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoVille']                           = $qp141PE3.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCourriel']                        = $qp141PE3.info3.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoie']                      = $qp141PE3.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp141PE3.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoCP']                              = $qp141PE3.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp141PE3.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp141PE3.info3.coordonnees1.declarantAdressePersoTelephone;

//Coordonnées en France

formFields['declarantAdresseFranceNumNomVoie']                     = $qp141PE3.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp141PE3.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceCP']                             = $qp141PE3.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceCP;
formFields['declarantAdresseFranceVille']                          = $qp141PE3.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceVille;
formFields['declarantAdresseFrancePays']                           = $qp141PE3.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFrancePays;
formFields['declarantAdresseFranceTelephone']                      = $qp141PE3.coordonneesFrancePage.coordonneesFranceGroupe.declarantAdresseFranceTelephone;

//Profession considérée

formFields['typesActes']                                           = $qp141PE3.info1.profession.typesActes;
formFields['lieuExercice']                                         = $qp141PE3.info1.profession.lieuExercice;
formFields['nomProfessionLPS']                                     = $qp141PE3.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp141PE3.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp141PE3.info1.profession.specialite;
formFields['listeActes']                                           = $qp141PE3.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp141PE3.info1.profession.exerciceAutonome ? "Oui" : "Non";
formFields['specialiteExerceeEM']                                  = $qp141PE3.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp141PE3.info1.profession.partieProfessionSiAccesPartiel;


//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp141PE3.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp141PE3.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp141PE3.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp141PE3.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp141PE3.info4.diplome.dateReconnaissanceTitreFormationProfession;

//Autre diplômes, titres ou certificats
formFields['intituleAutreTitreFormationProfession']                = $qp141PE3.autreDiplomePage.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp141PE3.autreDiplomePage.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp141PE3.autreDiplomePage.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp141PE3.autreDiplomePage.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp141PE3.autreDiplomePage.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

//Diplôme de spécialisation

formFields['intituleTitreDiplomeSpecialisation']                   = $qp141PE3.specialiteDiplomePage.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp141PE3.specialiteDiplomePage.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp141PE3.specialiteDiplomePage.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
//formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp141PE3.specialiteDiplomePage.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp141PE3.specialiteDiplomePage.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp141PE3.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp141PE3.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNom']								   = $qp141PE3.info5.ordre.ordreProfessionnelNom != null ? $qp141PE3.info5.ordre.ordreProfessionnelNom : '';
formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp141PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp141PE3.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp141PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp141PE3.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp141PE3.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp141PE3.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp141PE3.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp141PE3.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp141PE3.info5.ordre.ordreProfessionnelAdressePays != null ? $qp141PE3.info5.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp141PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp141PE3.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp141PE3.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp141PE3.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp141PE3.info6.assuranceProfessionnelle.commentaires;


formFields['prestation1Du']                     				   =  $qp141PE3.detailsRenouvellement.renouvellement.periodePreste1.from;
formFields['prestation1Au']                                        =  $qp141PE3.detailsRenouvellement.renouvellement.periodePreste1.to;
formFields['prestation2Du']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste2.from : '');
formFields['prestation2Au']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste2 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste2.to : '');
formFields['prestation3Du']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste3.from : '');
formFields['prestation3Au']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste3 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste3.to : '');
formFields['prestation4Du']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste4.from : '');
formFields['prestation4Au']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste4 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste4.to : '');
formFields['prestation5Du']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste5.from : '');
formFields['prestation5Au']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste5 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste5.to : '');
formFields['prestation6Du']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste6.from : '');
formFields['prestation6Au']                                        = ($qp141PE3.detailsRenouvellement.renouvellement.periodePreste6 ? $qp141PE3.detailsRenouvellement.renouvellement.periodePreste6.to : '');


formFields['prestation1ActiviteExercee']                           = $qp141PE3.detailsRenouvellement.renouvellement.prestation1ActiviteExercee;
formFields['prestation2ActiviteExercee']                           = $qp141PE3.detailsRenouvellement.renouvellement.prestation2ActiviteExercee;
formFields['prestation3ActiviteExercee']                           = $qp141PE3.detailsRenouvellement.renouvellement.prestation3ActiviteExercee;
formFields['prestation4ActiviteExercee']                           = $qp141PE3.detailsRenouvellement.renouvellement.prestation4ActiviteExercee;
formFields['prestation5ActiviteExercee']                           = $qp141PE3.detailsRenouvellement.renouvellement.prestation5ActiviteExercee;
formFields['prestation6ActiviteExercee']                           = $qp141PE3.detailsRenouvellement.renouvellement.prestation6ActiviteExercee;

//Commentaire 

formFields['commentairesSignature']                                = $qp141PE3.commentairePage.commentaireGroupe.commentairesSignature;
 
//Signature

formFields['dateSignature']                                        = $qp141PE3.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp141PE3.info7.signature.attesteSignature;


formFields['libelleProfession']				= "Génétique médicale (médecin spécialiste)";

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp141PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp141PE3.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Demande de renouvellement de la déclaration en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Genetique_medicale.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPrecedenteDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Genetique_medicale_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Génétique médicale (médecin spécialiste) - renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de génétique médicale (médecin spécialiste)',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
