var formFields = {};

var civNomPrenom = $qp233PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp233PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp233PE3.etatCivil.identificationDeclarant.prenomDeclarant;

//Psychothérapeute
formFields['emReglemente']                                             = false;
formFields['emNeReglementePas']                                        = true;
formFields['etatTiers']                                                = false;


//etatCivil
formFields['monsieur']                                    		     = ($qp233PE3.etatCivil.identificationDeclarant.civilite=='Monsieur');
formFields['madame']                                      		 	 = ($qp233PE3.etatCivil.identificationDeclarant.civilite=='Madame');
formFields['nomUsage']                                  			 = $qp233PE3.etatCivil.identificationDeclarant.nomEpouseDeclarant;
formFields['nomNaissance']                                           = $qp233PE3.etatCivil.identificationDeclarant.nomDeclarant;
formFields['prenoms']                                                = $qp233PE3.etatCivil.identificationDeclarant.prenomDeclarant;
formFields['dateNaissance']                                          = $qp233PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
formFields['villeNaissance']                                         = $qp233PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
formFields['paysNaissance']                                          = $qp233PE3.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
formFields['nationalite']                                            = $qp233PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;



//adresse
formFields['adresse']                      						  = $qp233PE3.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant 
formFields['complementAdresse'] 								  = $qp233PE3.adresse.adressePersonnelle.complementAdresseDeclarant;
formFields['villeAdresse']                                        = $qp233PE3.adresse.adressePersonnelle.villeAdresseDeclarant;
formFields['paysAdresse']                                		  = $qp233PE3.adresse.adressePersonnelle.paysAdresseDeclarant;
formFields['codePostal']                                  		  = $qp233PE3.adresse.adressePersonnelle.codePostalAdresseDeclarant;
formFields['telephoneFixe']                                       = $qp233PE3.adresse.adressePersonnelle.telephoneAdresseDeclarant;
formFields['telephonePortable']                                   = $qp233PE3.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;
formFields['email']                                               = $qp233PE3.adresse.adressePersonnelle.mailAdresseDeclarant;




//projetsProfessionnels

formFields['regionExercice']                                      = $qp233PE3.signatureGroup.signature.regionExercice;
formFields['dateSignature']                                       = $qp233PE3.signatureGroup.signature.dateSignature;
formFields['lieuSignature']                                       = $qp233PE3.signatureGroup.signature.lieuSignature;
formFields['signature']                                      	  = $qp233PE3.signatureGroup.signature.certifieHonneur;



 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp233PE3.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Agence régionale de santé",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire LE.pdf') //
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);

var finalDocItem = finalDoc.save('Psychothérapeute_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Psychothérapeute - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de psychothérapeute.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});