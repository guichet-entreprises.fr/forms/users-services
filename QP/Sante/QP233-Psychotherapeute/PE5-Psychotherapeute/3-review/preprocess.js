var cerfaFields = {}

var civiliteNomPrenom = $qp233PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp233PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp233PE5.etatCivil.identificationDeclarant.prenomDeclarant;
//Motif de la demande


cerfaFields['demandeInitiale']                              = false;
cerfaFields['renouvellement']                               = true;
cerfaFields['changement']                                   = false;

//Identité du demandeur

cerfaFields['nomDeclarant']              					= $qp233PE5.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']           					= $qp233PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['ville']                     					= $qp233PE5.etatCivil.identificationDeclarant.ville;
cerfaFields['pays']                      					= $qp233PE5.etatCivil.identificationDeclarant.pays;
cerfaFields['nationalite']               					= $qp233PE5.etatCivil.identificationDeclarant.nationalite;
cerfaFields['masculin']                  					= Value('id').of($qp233PE5.etatCivil.identificationDeclarant.civilite).eq('masculin') ? true : false;
cerfaFields['feminin']                   					= Value('id').of($qp233PE5.etatCivil.identificationDeclarant.civilite).eq('feminin') ? true : false;
cerfaFields['dateNaissance']             					= $qp233PE5.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['lieuNaissance']             					= $qp233PE5.etatCivil.identificationDeclarant.lieuNaissance;

//Adresse
cerfaFields['adresse']                  					= ($qp233PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp233PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : ' ' ) + ' ' + ($qp233PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp233PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : ' ') + ', ' + ($qp233PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp233PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : ' ')+ ' ' + ($qp233PE5.coordonnees.coordonneesDeclarant.declarantAdresseVille != null ? $qp233PE5.coordonnees.coordonneesDeclarant.declarantAdresseVille : ' ') + ', ' + ($qp233PE5.coordonnees.coordonneesDeclarant.declarantAdressePays != null ? $qp233PE5.coordonnees.coordonneesDeclarant.declarantAdressePays : ' ');
cerfaFields['telephone']                 					= $qp233PE5.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['courrier']                  			= $qp233PE5.coordonnees.coordonneesDeclarant.declarantCourriel;

//AdresseFrance
cerfaFields['adresseFrance']             					= ($qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseNumeroLibelleFrance != null ? $qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseNumeroLibelleFrance : '') + ' ' + ($qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '') + ' ' +($qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal : ' ') +' '+ ($qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceVille != null ? $qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceVille : ' ');
cerfaFields['telephoneFrance']           			= $qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['courrierFrance']            			= $qp233PE5.coordonneesFrance.coordonneesDeclarantFrance.declarantCourrielFrance;

//Usage titre 
cerfaFields['lieuExercice']              					= ($qp233PE5.usageTitre.usageTitreDeclarant.numNomLieuExercice != null ? $qp233PE5.usageTitre.usageTitreDeclarant.numNomLieuExercice : ' ') + ' ' + ($qp233PE5.usageTitre.usageTitreDeclarant.complementLieuExercice != null ? $qp233PE5.usageTitre.usageTitreDeclarant.complementLieuExercice: '')+ ', ' + ($qp233PE5.usageTitre.usageTitreDeclarant.codePostalLieuExercice != null ? $qp233PE5.usageTitre.usageTitreDeclarant.codePostalLieuExercice: ' ')+ ' ' + ($qp233PE5.usageTitre.usageTitreDeclarant.communeLieuExercice != null ? $qp233PE5.usageTitre.usageTitreDeclarant.communeLieuExercice: ' ')+ ', ' + ($qp233PE5.usageTitre.usageTitreDeclarant.paysLieuExercice != null ? $qp233PE5.usageTitre.usageTitreDeclarant.paysLieuExercice: ' ');

//Ordre professionnel ou organisme professionnel																
cerfaFields['non']                         					= Value('id').of($qp233PE5.ordreProfessionnel.ordreProfessionnel.ouiNon).eq('non') ? true : false;
cerfaFields['oui']                         					= Value('id').of($qp233PE5.ordreProfessionnel.ordreProfessionnel.ouiNon).eq('oui') ? true : false;
cerfaFields['infoOrdreProfessionnel']      					= ($qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelNom != null ? $qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelNom :'')
															+ ' ' + ($qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee :'') 
															+ ' ' + ($qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse :'') 
															+ ' ' + ($qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') 
															+ ' ' + ($qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdresseVille :'') 
															+ ' ' + ($qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelAdressePays :'') 
															+ ' numéro d\'enregistrement : ' + ($qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelNumEnregistrementNom != null ? $qp233PE5.ordreProfessionnel.ordreProfessionnel.ordreProfessionnelNumEnregistrementNom :'') ;

//Infos renouvellement
cerfaFields['commentaires']              		= $qp233PE5.detailsRenouvellement.renouvellement.commentairesRenouvellement;
cerfaFields['activitesProfessionnelles']   		= $qp233PE5.detailsRenouvellement.renouvellement.activitesProfessionnelles;

cerfaFields['dateDebut1']                     = $qp233PE5.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['dateFin1']                       = $qp233PE5.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['dateDebut2']                     = ($qp233PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp233PE5.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['dateFin2']                       = ($qp233PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp233PE5.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['dateDebut3']                     = ($qp233PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp233PE5.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['dateFin3']                       = ($qp233PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp233PE5.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['dateDebut4']                     = ($qp233PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp233PE5.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['dateFin4']                       = ($qp233PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp233PE5.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['dateDebut5']                     = ($qp233PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp233PE5.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['dateFin5']                       = ($qp233PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp233PE5.detailsRenouvellement.renouvellement.periodePreste5.to : '');


cerfaFields['observations']              		= $qp233PE5.observations.observations.observations;
cerfaFields['dateSignature']             		= $qp233PE5.signature.signature1.dateSignature;
cerfaFields['signatureDeclarant']               = civiliteNomPrenom;
cerfaFields['declaration']						= "Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.";

cerfaFields['libelleProfession']				= "Psychothérapeute"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp233PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civiliteNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp233PE5.signature.signature1.dateSignature,
		autoriteHabilitee :"Agence régionale de santé ",
		demandeContexte : "Renouvellement de la déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civiliteNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/model.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieDeclaration);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('psychothérapeute_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Psychothérapeute - Renouvellement de la déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de psychothérapeute.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});