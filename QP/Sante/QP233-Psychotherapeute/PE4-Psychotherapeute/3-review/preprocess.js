var cerfaFields = {}

var civiliteNomPrenom = $qp233PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp233PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp233PE4.etatCivil.identificationDeclarant.prenomDeclarant;
//Motif de la demande


cerfaFields['demandeInitiale']                              = true;
cerfaFields['renouvellement']                               = false;
cerfaFields['changement']                                   = false;

//Identité du demandeur

cerfaFields['nomDeclarant']              					= $qp233PE4.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']           					= $qp233PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['ville']                     					= $qp233PE4.etatCivil.identificationDeclarant.villeNaissance;
cerfaFields['pays']                      					= $qp233PE4.etatCivil.identificationDeclarant.paysNaissance;
cerfaFields['nationalite']               					= $qp233PE4.etatCivil.identificationDeclarant.nationalite;
cerfaFields['masculin']                  					= Value('id').of($qp233PE4.etatCivil.identificationDeclarant.civilite).eq('masculin') ? true : false;
cerfaFields['feminin']                   					= Value('id').of($qp233PE4.etatCivil.identificationDeclarant.civilite).eq('feminin') ? true : false;
cerfaFields['dateNaissance']             					= $qp233PE4.etatCivil.identificationDeclarant.dateNaissance;

//Adresse
cerfaFields['adresse']                  					= ($qp233PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp233PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : ' ' ) + ' ' + ($qp233PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp233PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : ' ') + ', ' + ($qp233PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp233PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : ' ')+ ' ' + ($qp233PE4.coordonnees.coordonneesDeclarant.declarantAdresseVille != null ? $qp233PE4.coordonnees.coordonneesDeclarant.declarantAdresseVille : ' ') + ', ' + ($qp233PE4.coordonnees.coordonneesDeclarant.declarantAdressePays != null ? $qp233PE4.coordonnees.coordonneesDeclarant.declarantAdressePays : ' ');
cerfaFields['telephone']                 					= $qp233PE4.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['courrier']                  			= $qp233PE4.coordonnees.coordonneesDeclarant.declarantCourriel;

//AdresseFrance
cerfaFields['adresseFrance']             					= ($qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseNumeroLibelleFrance != null ? $qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseNumeroLibelleFrance : '') + ' ' + ($qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '') + ', ' +($qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal : ' ') +' '+ ($qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceVille != null ? $qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantAdresseFranceVille : ' ');
cerfaFields['telephoneFrance']           			= $qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['courrierFrance']            			= $qp233PE4.coordonneesFrance.coordonneesDeclarantFrance.declarantCourrielFrance;

//Usage titre 
cerfaFields['lieuExercice']              					= ($qp233PE4.usageTitre.usageTitreDeclarant.numNomLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.numNomLieuExercice : ' ') + ' ' + ($qp233PE4.usageTitre.usageTitreDeclarant.complementLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.complementLieuExercice: ' ')+ ', ' + ($qp233PE4.usageTitre.usageTitreDeclarant.codePostalLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.codePostalLieuExercice: ' ')+ ' ' + ($qp233PE4.usageTitre.usageTitreDeclarant.communeLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.communeLieuExercice: ' ')+ ', ' + ($qp233PE4.usageTitre.usageTitreDeclarant.paysLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.paysLieuExercice: ' ');
cerfaFields['lieuExercice']              					= ($qp233PE4.usageTitre.usageTitreDeclarant.numNomLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.numNomLieuExercice : ' ') + ' ' + ($qp233PE4.usageTitre.usageTitreDeclarant.complementLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.complementLieuExercice: ' ')+ ', ' + ($qp233PE4.usageTitre.usageTitreDeclarant.codePostalLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.codePostalLieuExercice: ' ')+ ' ' + ($qp233PE4.usageTitre.usageTitreDeclarant.communeLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.communeLieuExercice: ' ')+ ', ' + ($qp233PE4.usageTitre.usageTitreDeclarant.paysLieuExercice != null ? $qp233PE4.usageTitre.usageTitreDeclarant.paysLieuExercice: ' ');

//Ordre professionnel ou organisme professionnel																
cerfaFields['non']                         					= Value('id').of($qp233PE4.usageTitre.ordreProfessionnel.ouiNon).eq('non') ? true : false;
cerfaFields['oui']                         					= Value('id').of($qp233PE4.usageTitre.ordreProfessionnel.ouiNon).eq('oui') ? true : false;
cerfaFields['infoOrdreProfessionnel']      					= ($qp233PE4.usageTitre.ordreProfessionnel.ordreProfessionnelNom)
															+ ' ' + ($qp233PE4.usageTitre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee) 
															+ ' ' + ($qp233PE4.usageTitre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse) 
															+ ', ' + ($qp233PE4.usageTitre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal) 
															+ ' ' + ($qp233PE4.usageTitre.ordreProfessionnel.ordreProfessionnelAdresseVille) 
															+ ', ' + ($qp233PE4.usageTitre.ordreProfessionnel.ordreProfessionnelAdressePays) 
															+ ', numéro d\'enregistrement : ' + ($qp233PE4.usageTitre.ordreProfessionnel.ordreProfessionnelNumEnregistrementNom) ;

//Infos renouvellement
cerfaFields['dateDebut1']                		= '';
cerfaFields['dateFin1']                  		= '';
cerfaFields['dateDebut2']                  		= '';
cerfaFields['dateFin2']                  		= '';
cerfaFields['dateDebut3']               	    = '';
cerfaFields['dateFin3']                  		= '';
cerfaFields['dateDebut4']                		= '';
cerfaFields['dateFin4']                  		= '';
cerfaFields['dateDebut5']                		= '';
cerfaFields['dateFin5']                  		= '';
cerfaFields['commentaires']              		= '';
cerfaFields['activitesProfessionnelles']   		= '';

cerfaFields['observations']              		= $qp233PE4.signature.signature1.observations;
cerfaFields['dateSignature']             		= $qp233PE4.signature.signature1.dateSignature;
cerfaFields['signatureDeclarant']               = civiliteNomPrenom;
cerfaFields['declaration']						= "Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.";

cerfaFields['libelleProfession']				= "Psychothérapeute"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp233PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civiliteNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp233PE4.signature.signature1.dateSignature,
		autoriteHabilitee :"Agence régionale de santé ",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civiliteNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/model.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('psychothérapeute_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Psychothérapeute - déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de psychothérapeute.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});