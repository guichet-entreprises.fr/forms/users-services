// prepare info to send to pushingBox
var recordUid 		= '<unknown>'
var formName 		= '<unknown>'
var formId 			= '<unknown>'
var dateTime 		= new Date()
var pushingBoxDevID	= _CONFIG_ ? _CONFIG_.get('pushingbox.devid') : null

if (!pushingBoxDevID) {
	pushingBoxDevID = 'v1E8F075A7DF1552';
}

if (nash.record && nash.record.description) {
	recordUid 	= nash.record.description().recordUid
	formName 	= nash.record.description().title
	formId		= nash.record.description().formUid
}

_log.info("recordUid is  {}", recordUid);
_log.info("formName is  {}", formName);
_log.info("formId is  {}", formId);
_log.info("dateTime is  {}", dateTime);

// call pushingBox
var response = nash.service.request('http://api.pushingbox.com/pushingbox')
	.param("recordId",	recordUid)
	.param("formName",	formName)
	.param("formId",	formId)
	.param("dateTime",	dateTime)
	.param("devid",		pushingBoxDevID)
	.get();

// output to confirm

return spec.create({
	id: 'ConfirmSendPushingBox',
	label: "Confirmation de l\'envoi via PushingBox ",
	groups: [spec.createGroup({
			id: 'postmail',
			label: "le statut de l\'envoi vers pushingBox",
			data: [spec.createData({
					id: 'pushing',
					label: "http status code",
					type: 'String',
					mandatory: true,
					value: '' + response.status
				})
			]
		})]
});
