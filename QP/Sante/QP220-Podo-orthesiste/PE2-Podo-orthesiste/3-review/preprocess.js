var formFields = {};

var civNomPrenom = $qp220PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp220PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp220PE2.etatCivil.identificationDeclarant.prenomDeclarant;

//Podo-orthésiste
formFields['podoOrthesiste']                                      = true;
formFields['professionsAppareillage']                             = true;


//etatCivil
formFields['civiliteMonsieur']                                    = ($qp220PE2.etatCivil.identificationDeclarant.civilite=='Monsieur');
formFields['civiliteMadame']                                      = ($qp220PE2.etatCivil.identificationDeclarant.civilite=='Madame');
formFields['declarantNomUsage']                                   = $qp220PE2.etatCivil.identificationDeclarant.nomEpouseDeclarant;
formFields['declarantNomNaissance']                               = $qp220PE2.etatCivil.identificationDeclarant.nomDeclarant;
formFields['declarantPrenoms']                                    = $qp220PE2.etatCivil.identificationDeclarant.prenomDeclarant;
formFields['declarantDateNaissance']                              = $qp220PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
formFields['declarantLieuNaissance']                              = $qp220PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
formFields['declarantPaysNaissance']                              = $qp220PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
formFields['declarantNationalite']                                = $qp220PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
formFields['declarantCourriel']                                   = $qp220PE2.adresse.adressePersonnelle.mailAdresseDeclarant;


//adresse
formFields['declarantAdresseRueComplement']                       = $qp220PE2.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant 
																	+ ($qp220PE2.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ' ' + $qp220PE2.adresse.adressePersonnelle.complementAdresseDeclarant : '');
formFields['declarantAdresseVille']                               = $qp220PE2.adresse.adressePersonnelle.villeAdresseDeclarant;
formFields['declarantAdressePays']                                = $qp220PE2.adresse.adressePersonnelle.paysAdresseDeclarant;
formFields['declarantAdresseCP']                                  = $qp220PE2.adresse.adressePersonnelle.codePostalAdresseDeclarant;
formFields['declarantTelephoneFixe']                              = $qp220PE2.adresse.adressePersonnelle.telephoneAdresseDeclarant;
formFields['declarantTelephoneMobile']                            = $qp220PE2.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;


//diplomeProfession
formFields['declarantDiplomeIntitule']                            = $qp220PE2.diplomeProfession.diplomeProfession.intituleDiplome;
formFields['declarantDiplomePaysObtention']                       = $qp220PE2.diplomeProfession.diplomeProfession.paysObtentionDiplome;
formFields['declarantDiplomeDelivrePar']                          = $qp220PE2.diplomeProfession.diplomeProfession.delivreDiplome;
formFields['declarantDiplomeDateObtention']                       = $qp220PE2.diplomeProfession.diplomeProfession.dateObtentionDiplome;
formFields['declarantDiplomeDateReconnaissance']                  = $qp220PE2.diplomeProfession.diplomeProfession.dateReconnaissanceDiplomeEtat;	

//diplomeDetailles
formFields['diplomeIntitule1']                                    = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome1;
formFields['diplomeDateObtention1']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome1;
formFields['diplomeLieuFormation1']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome1;
formFields['diplomePaysObtention1']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome1;

formFields['diplomeIntitule2']                                    = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome2;
formFields['diplomeDateObtention2']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome2;
formFields['diplomeLieuFormation2']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome2;
formFields['diplomePaysObtention2']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome2; 

formFields['diplomeIntitule3']                                    = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome3;
formFields['diplomeDateObtention3']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome3;
formFields['diplomeLieuFormation3']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome3;
formFields['diplomePaysObtention3']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome3;

formFields['diplomeIntitule4']                                    = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.intituleObtentionAutresDiplome4;
formFields['diplomeDateObtention4']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.dateObtentionAutresDiplome4;
formFields['diplomeLieuFormation4']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.lieuFormationObtentionAutresDiplome4;
formFields['diplomePaysObtention4']                               = $qp220PE2.diplomeAutresQualification.diplomeAutresQualification1.paysObtentionAutresDiplome4;


//exerciceProfessionnel
formFields['exerciceProfessionnelNature1']                        = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurNom1']                  = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement1'] = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger1;
formFields['exerciceProfessionnelEmployeurAdresseVillePays1']     = ($qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 != null ? $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger1 +' ' :'')
                                                                    +($qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 != null ? $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 : '');
formFields['exerciceProfessionnelPeriode1']                       = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1;

formFields['exerciceProfessionnelNature2']                        = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurNom2']                  = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement2'] = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger2;
formFields['exerciceProfessionnelEmployeurAdresseVillePays2']     = ($qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 != null ? $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger2 +' ' :'')
                                                                    +($qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 != null ? $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 : '');
formFields['exerciceProfessionnelPeriode2']                       = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2;

formFields['exerciceProfessionnelNature3']                        = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurNom3']                  = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement3'] = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger3;
formFields['exerciceProfessionnelEmployeurAdresseVillePays3']     = ($qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 != null ? $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger3 +' ' :'')
                                                                    +($qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 != null ? $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 : '');
formFields['exerciceProfessionnelPeriode3']                       = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3;

formFields['exerciceProfessionnelNature4']                        = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurNom4']                  = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.employeurFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurAdresseRueComplement4'] = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger4;
formFields['exerciceProfessionnelEmployeurAdresseVillePays4']     = ($qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 != null ? $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.villeFonctionExerceesEtranger4 +' ' :'')
                                                                    +($qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 != null ? $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 : '');
formFields['exerciceProfessionnelPeriode4']                       = $qp220PE2.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4;


//projetsProfessionnels
formFields['projetsProfessionnels']                               = $qp220PE2.projetsProfessionnels.projetsProfessionnels.projetsProfessionnels;
formFields['attesteHonneurDemandeUnique']                         = $qp220PE2.signatureGroup.signature.attesteHonneurDemandeUnique;
formFields['regionExercice']                                      = $qp220PE2.signatureGroup.signature.regionExercice;
formFields['signatureDate']                                       = $qp220PE2.signatureGroup.signature.dateSignature;
formFields['signatureLieu']                                       = $qp220PE2.signatureGroup.signature.lieuSignature;
formFields['signature']                                           = 'Je déclare sur l’honneur l\'exactitude des informations de la formalité et signe la présente déclaration.';
formFields['signatureCoche']                                      = $qp220PE2.signatureGroup.signature.certifieHonneur;
formFields['libelleProfession']								  	  = "Podo-orthésiste"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp220PE2.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp220PE2.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire LE.pdf') //
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPieces);

var finalDocItem = finalDoc.save('Formulaire LE_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Podo-orthésiste - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de podo-orthésiste',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});