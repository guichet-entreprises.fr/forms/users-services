var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']          	       = $qp231PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp231PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp231PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['civiliteNomPrenomSignature']          = $qp231PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp231PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp231PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	   = $qp231PE8.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp231PE8.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		   = $qp231PE8.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']             		   = $qp231PE8.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 					           = $qp231PE8.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp231PE8.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp231PE8.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					   = ($qp231PE8.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp231PE8.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp231PE8.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp231PE8.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			   = $qp231PE8.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			   = $qp231PE8.adresse.adresseContact.telephoneAdresseDeclarant;
// cerfaFields['telephoneMobile']    			   = $qp231PE8.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']          				   = $qp231PE8.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                				   = $qp231PE8.signature.signature.dateSignature;
cerfaFields['signature']           				   = $qp231PE8.signature.signature.signature;
cerfaFields['lieuSignature']                       = $qp231PE8.signature.signature.lieuSignature + ', le ';



var cerfa = pdf.create('models/courrier libre LE V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Psychologue RQP.pdf', cerfa); //Nom du fichier en sortie

return spec.create({
    id : 'review',
    label : 'Psychologue - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de demande de reconnaissance de qualifications professionnelles pour la profession de psychologue.',
            description : 'Voici le courrier obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
