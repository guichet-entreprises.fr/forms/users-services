var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']          	       = $qp231PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp231PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp231PE7.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['civiliteNomPrenomSignature']          = $qp231PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp231PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp231PE7.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	   = $qp231PE7.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp231PE7.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		   = $qp231PE7.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']             		   = $qp231PE7.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 					           = $qp231PE7.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp231PE7.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp231PE7.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					   = ($qp231PE7.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp231PE7.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp231PE7.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp231PE7.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			   = $qp231PE7.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			   = $qp231PE7.adresse.adresseContact.telephoneAdresseDeclarant;
// cerfaFields['telephoneMobile']    			   = $qp231PE7.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']          				   = $qp231PE7.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                				   = $qp231PE7.signature.signature.dateSignature;
cerfaFields['signature']           				   = $qp231PE7.signature.signature.signature;
cerfaFields['lieuSignature']                       = $qp231PE7.signature.signature.lieuSignature + ', le ';




var cerfa = pdf.create('models/courrier libre LE V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Psychologue RQP.pdf', cerfa); //Nom du fichier en sortie

return spec.create({
    id : 'review',
    label : 'Psychologue - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de demande de reconnaissance de qualifications professionnelles pour la profession de psychologue.',
            description : 'Voici le courrier obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
