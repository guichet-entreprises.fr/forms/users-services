

var cerfaFields = {};

var civNomPrenom = $qp231PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp231PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp231PE4.etatCivil.identificationDeclarant.prenomDeclarant;


cerfaFields['monsieur'] 	  	    = ($qp231PE4.etatCivil.identificationDeclarant.civilite=='Monsieur');
cerfaFields['madame']   		    = ($qp231PE4.etatCivil.identificationDeclarant.civilite=='Madame');
cerfaFields['nomDeclarant']         = $qp231PE4.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']      = $qp231PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['adresseDeclarant']     = $qp231PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ' ' + ($qp231PE4.adresse.adresseContact.complementAdresseDeclarant !=null ? $qp231PE4.adresse.adresseContact.complementAdresseDeclarant:' ');
cerfaFields['codePostal']           = $qp231PE4.adresse.adresseContact.villeAdresseDeclarant  + ' ' + ($qp231PE4.adresse.adresseContact.codePostalAdresseDeclarant !=null ? $qp231PE4.adresse.adresseContact.codePostalAdresseDeclarant :' ');
cerfaFields['pays']                 = $qp231PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['numeroTelephone']      = $qp231PE4.adresse.adresseContact.telephoneMobileDeclarant + ' / ' + ($qp231PE4.adresse.adresseContact.telephoneDeclarant !=null ? $qp231PE4.adresse.adresseContact.telephoneDeclarant:' ');
cerfaFields['courriel']             = $qp231PE4.adresse.adresseContact.mailDeclarant;
cerfaFields['dateLieuNaissance']    = $qp231PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant; 
cerfaFields['dateNaissance']        = $qp231PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['lieuNaissance']        = $qp231PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ' ' + $qp231PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationaliteDeclarant'] = $qp231PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['signature']            = "Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.";
cerfaFields['lieuSignature']        = $qp231PE4.signature1.signature1.lieuSignature;
cerfaFields['dateSignature']        = $qp231PE4.signature1.signature1.dateSignature;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp231PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp231PE4.signature1.signature1.dateSignature,
		autoriteHabilitee :"Ministère de l’éducation nationale",
		demandeContexte : "Demande de reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Demande_reconnaissance_qualifications.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjRelevesNotes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationStage);
appendPj($attachmentPreprocess.attachmentPreprocess.pjMemoireRecherche);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('psychologue_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Psychologue - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de psychologue.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
