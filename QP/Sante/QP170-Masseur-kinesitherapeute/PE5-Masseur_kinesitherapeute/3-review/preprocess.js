function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};
var civNomPrenom = $qp170PE5.information.etatCivil.civilite + ' ' + ($qp170PE5.information.etatCivil.nomUsage != null ? $qp170PE5.information.etatCivil.nomUsage : $qp170PE5.information.etatCivil.nom) + ' ' + $qp170PE5.information.etatCivil.prenom;

//Profession
cerfaFields['professionExerceeEtatEtablissement']   = "Masseur-kinésithérapeute";
cerfaFields['professionPrestationDemandee']         = "Masseur-kinésithérapeute";

//Prestation de service 
cerfaFields['premierePrestationServiceCoche']       = true ;
cerfaFields['renouvellementCoche']                  = false;
cerfaFields['changementSituationCoche']             = false;

//Titulaire titre de formation 

cerfaFields['titulaireTitreFormationCoche']         = true;
cerfaFields['exerciceProfessionCoche']              = false;
cerfaFields['titreFormationEtatTiersCoche']         = false;

//Profession exercée 

cerfaFields['acteExercice1']                        = $qp170PE5.profession.professionExercee.acteExercice;
cerfaFields['activiteExerceeAutonome']              = $qp170PE5.profession.professionExercee.activiteExerceeAutonome;


//etatCivil


cerfaFields['madame']                               =  ($qp170PE5.information.etatCivil.civilite=='Madame');
cerfaFields['monsieur']                             =  ($qp170PE5.information.etatCivil.civilite=='Monsieur');
cerfaFields['nom']                                  = $qp170PE5.information.etatCivil.nom;
cerfaFields['nomUsage']                             = $qp170PE5.information.etatCivil.nomUsage;
cerfaFields['prenom']                               = $qp170PE5.information.etatCivil.prenom;

if($qp170PE5.information.etatCivil.dateNaissance != null) {
var dateTemp = new Date(parseInt ($qp170PE5.information.etatCivil.dateNaissance.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['dateNaissanceJour']  = day;
	cerfaFields['dateNaissanceMois'] = month;
	cerfaFields['dateNaissanceAnnee']   = year;
}

cerfaFields['villeNaissance']                       = $qp170PE5.information.etatCivil.villeNaissance;
cerfaFields['paysNaissance']                        = $qp170PE5.information.etatCivil.paysNaissance;
cerfaFields['nationalite']                          = $qp170PE5.information.etatCivil.nationalite;

//Coordonnées dans l'état ou vous êtes etabli

cerfaFields['numeroLibelleComplementAdresse']       = $qp170PE5.coordonnees.coordonneesOrigine.numeroLibelleVoieAdresse + ' ' +($qp170PE5.coordonnees.coordonneesOrigine.complementAdresse != null ? $qp170PE5.coordonnees.coordonneesOrigine.complementAdresse : '') ;
cerfaFields['villeAdresse']                         = $qp170PE5.coordonnees.coordonneesOrigine.villeAdresse;
cerfaFields['codePostalAdresse']                    = $qp170PE5.coordonnees.coordonneesOrigine.codePostalAdresse;
cerfaFields['paysAdresse']                          = $qp170PE5.coordonnees.coordonneesOrigine.paysAdresse;
cerfaFields['telephoneAdresse']                     = $qp170PE5.coordonnees.coordonneesOrigine.telephoneAdresse;


var adresse = $qp170PE5.coordonnees.coordonneesOrigine.mailAdresse;
	cerfaFields['courriel1Adresse']                     = '';
	cerfaFields['courrielAdresse']                      = '';
if(adresse != null) {
	var adresseSplite = adresse.split("@");
	cerfaFields['courrielAdresse']                  = adresseSplite[0];
	cerfaFields['courriel1Adresse']                 = adresseSplite[1];
}

//Adresse en France 

cerfaFields['numeroLibelleComplementAdresseFrance'] = ($qp170PE5.coordonneesFrance.coordonneesFrance.numeroLibelleAdresseFrance != null ? $qp170PE5.coordonneesFrance.coordonneesFrance.numeroLibelleAdresseFrance : '') 
												  + ' ' + ($qp170PE5.coordonneesFrance.coordonneesFrance.complementAdresseFrance != null ? $qp170PE5.coordonneesFrance.coordonneesFrance.complementAdresseFrance : '') ;
cerfaFields['villeAdresseFrance']                   = $qp170PE5.coordonneesFrance.coordonneesFrance.villeAdresseFrance;
cerfaFields['codePostalAdresseFrance']              = $qp170PE5.coordonneesFrance.coordonneesFrance.codePostalAdresseFrance;
cerfaFields['paysAdresseFrance']                    = $qp170PE5.coordonneesFrance.coordonneesFrance.paysAdresseFrance;
cerfaFields['telephoneAdresseFrance']               = $qp170PE5.coordonneesFrance.coordonneesFrance.telephoneAdresseFrance;

//Adresse professionnel en France 

cerfaFields['numeroLibelleComplementAdressePro']    = ($qp170PE5.coordonneesProfessionnel.coordonneesProfessionnel.numeroLibelleAdressePro != null ? $qp170PE5.coordonneesProfessionnel.coordonneesProfessionnel.numeroLibelleAdressePro : '') 
											  + ' ' + ($qp170PE5.coordonneesProfessionnel.coordonneesProfessionnel.complementAdresseFrance != null ? $qp170PE5.coordonneesProfessionnel.coordonneesProfessionnel.complementAdresseFrance : '') ;
cerfaFields['villeAdressePro']                      = $qp170PE5.coordonneesProfessionnel.coordonneesProfessionnel.villeAdressePro;
cerfaFields['codePostalAdressePro']                 = $qp170PE5.coordonneesProfessionnel.coordonneesProfessionnel.codePostalAdressePro;
cerfaFields['paysAdressePro']                       = $qp170PE5.coordonneesProfessionnel.coordonneesProfessionnel.paysAdressePro;
cerfaFields['telephoneAdressePro']                  = $qp170PE5.coordonneesProfessionnel.coordonneesProfessionnel.telephoneAdressePro;


cerfaFields['precisionDureeFrequencePrestation']    = $qp170PE5.coordonneesPrecision.coordonneesPrecision.precisionDureeFrequencePrestation;

//Titre de formation de la profession considérée 

cerfaFields['titreFormation']                       = $qp170PE5.titreFormation.titreFormation.intituleTitreFormation;

if($qp170PE5.titreFormation.titreFormation.dateObtentionFormation != null) {
var dateTemp = new Date(parseInt ($qp170PE5.titreFormation.titreFormation.dateObtentionFormation.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['dateTitreFormationJour']  = day;
	cerfaFields['dateTitreFormationMois']  = month;
	cerfaFields['dateTitreFormationAnnee']   = year;
}

cerfaFields['lieuTitreFormation']                   = $qp170PE5.titreFormation.titreFormation.lieuTitreFormation;

cerfaFields['dateReconnaissanceFormationJour']      = '';
cerfaFields['dateReconnaissanceFormationMois']      = '';
cerfaFields['dateReconnaissanceFormationAnnee']     = '';

if($qp170PE5.titreFormation.titreFormation.dateObtentionFormation1 != null) {
var dateTemp = new Date(parseInt ($qp170PE5.titreFormation.titreFormation.dateObtentionFormation1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['dateReconnaissanceFormationJour']   = day;
	cerfaFields['dateReconnaissanceFormationMois']  = month;
	cerfaFields['dateReconnaissanceFormationAnnee']   = year;
}

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                = Value('id').of($qp170PE5.oredreProfessionnel.oredreProfessionnel.ordreProfessionnelOuiNon).eq('ordreProfessionnelOui')? true : false;
cerfaFields['ordreProffessionnelNon']               = Value('id').of($qp170PE5.oredreProfessionnel.oredreProfessionnel.ordreProfessionnelOuiNon).eq('ordreProfessionnelNon')? true : false;;

cerfaFields['informationOrdreProfessionnel']        = $qp170PE5.oredreProfessionnel.oredreProfessionnel.nomOrdreProfessionnel;


cerfaFields['nomCompagnieAssurance']                = $qp170PE5.assurancePro.assurancePro.nomCompagnieAssurance;
cerfaFields['numeroContratAssurance']               = $qp170PE5.assurancePro.assurancePro.numeroContratAssurance;





cerfaFields['commentairesEventuels']                = $qp170PE5.commentaire.commentaire.commentairesEventuels;


cerfaFields['activiteProfessionnellesExercees']     = '';
cerfaFields['debutDateRenouvellementJour2']         = '';
cerfaFields['debutDateRenouvellementJour3']         = '';
cerfaFields['debutDateRenouvellementJour4']         = '';
cerfaFields['debutDateRenouvellementJour5']         = '';
cerfaFields['debutDateRenouvellementJour6']         = '';
cerfaFields['debutDateRenouvellementMois1']         = '';
cerfaFields['debutDateRenouvellementMois2']         = '';
cerfaFields['debutDateRenouvellementMois3']         = '';
cerfaFields['debutDateRenouvellementMois4']         = '';
cerfaFields['debutDateRenouvellementMois5']         = '';
cerfaFields['debutDateRenouvellementMois6']         = '';
cerfaFields['debutDateRenouvellementAnnee1']        = '';
cerfaFields['debutDateRenouvellementAnnee2']        = '';
cerfaFields['debutDateRenouvellementAnnee3']        = '';
cerfaFields['debutDateRenouvellementAnnee4']        = '';
cerfaFields['debutDateRenouvellementAnnee5']        = '';
cerfaFields['debutDateRenouvellementAnnee6']        = '';
cerfaFields['finDateRenouvellementJour1']           = '';
cerfaFields['finDateRenouvellementMois1']           = '';
cerfaFields['finDateRenouvellementMois2']           = '';
cerfaFields['finDateRenouvellementMois3']           = '';
cerfaFields['finDateRenouvellementMois4']           = '';
cerfaFields['finDateRenouvellementMois5']           = '';
cerfaFields['finDateRenouvellementMois6']           = '';
cerfaFields['finDateRenouvellementAnnee1']          = '';
cerfaFields['finDateRenouvellementAnnee2']          = '';
cerfaFields['finDateRenouvellementAnnee3']          = '';
cerfaFields['finDateRenouvellementAnnee4']          = '';
cerfaFields['finDateRenouvellementAnnee5']          = '';
cerfaFields['finDateRenouvellementAnnee6']          = '';
cerfaFields['debutDateRenouvellementJour1']         = '';


if($qp170PE5.signature.signature.dateSignature != null) {
var dateTemp = new Date(parseInt ($qp170PE5.signature.signature.dateSignature.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['signatureDateJour']  = day;
	cerfaFields['signatureDateMois']   = month;
	cerfaFields['signatureDateAnnee']    = year;
}

cerfaFields['signature']                                           = civNomPrenom;



var cerfa = pdf.create('models/cerfa', cerfaFields);



cerfaFields['libelleProfession']				= "Masseur-kinésithérapeute"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp015PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp170PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’ordre des masseurs kinésithérapeutes",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */

var cerfaDoc = nash.doc //
	.load('models/Formulaire LPS Kinésithérapeute.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Masseur_kinesitherapeute_LPS_Modif.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Masseur-kinésithérapeute - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour la profession de masseur-kinésithérapeute.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
