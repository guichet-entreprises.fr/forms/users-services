function pad(s) { return (s < 10) ? '0' + s : s; }

var cerfaFields = {};
var civNomPrenom = $qp170PE8.information.etatCivil.civilite + ' ' + ($qp170PE8.information.etatCivil.nomUsage != null ? $qp170PE8.information.etatCivil.nomUsage : $qp170PE8.information.etatCivil.nom) + ' ' + $qp170PE8.information.etatCivil.prenom;

//Profession
cerfaFields['professionExerceeEtatEtablissement']   = "Masseur-kinésithérapeute";
cerfaFields['professionPrestationDemandee']         = "Masseur-kinésithérapeute";

//Prestation de service 
cerfaFields['premierePrestationServiceCoche']       = false ;
cerfaFields['renouvellementCoche']                  = true;
cerfaFields['changementSituationCoche']             = false;

//Titulaire titre de formation 

cerfaFields['titulaireTitreFormationCoche']         = false;
cerfaFields['exerciceProfessionCoche']              = false;
cerfaFields['titreFormationEtatTiersCoche']         = false;

//Profession exercée 

cerfaFields['acteExercice1']                        = $qp170PE8.profession.professionExercee.acteExercice;
cerfaFields['activiteExerceeAutonome']              = $qp170PE8.profession.professionExercee.activiteExerceeAutonome;


//etatCivil


cerfaFields['madame']                               =  ($qp170PE8.information.etatCivil.civilite=='Madame');
cerfaFields['monsieur']                             =  ($qp170PE8.information.etatCivil.civilite=='Monsieur');
cerfaFields['nom']                                  = $qp170PE8.information.etatCivil.nom;
cerfaFields['nomUsage']                             = $qp170PE8.information.etatCivil.nomUsage;
cerfaFields['prenom']                               = $qp170PE8.information.etatCivil.prenom;

if($qp170PE8.information.etatCivil.dateNaissance != null) {
var dateTemp = new Date(parseInt ($qp170PE8.information.etatCivil.dateNaissance.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['dateNaissanceJour']  = day;
	cerfaFields['dateNaissanceMois'] = month;
	cerfaFields['dateNaissanceAnnee']   = year;
}

cerfaFields['villeNaissance']                       = $qp170PE8.information.etatCivil.villeNaissance;
cerfaFields['paysNaissance']                        = $qp170PE8.information.etatCivil.paysNaissance;
cerfaFields['nationalite']                          = $qp170PE8.information.etatCivil.nationalite;

//Coordonnées dans l'état ou vous êtes etabli

cerfaFields['numeroLibelleComplementAdresse']       = $qp170PE8.coordonnees.coordonneesOrigine.numeroLibelleVoieAdresse + ' ' +($qp170PE8.coordonnees.coordonneesOrigine.complementAdresse != null ? $qp170PE8.coordonnees.coordonneesOrigine.complementAdresse : '') ;
cerfaFields['villeAdresse']                         = $qp170PE8.coordonnees.coordonneesOrigine.villeAdresse;
cerfaFields['codePostalAdresse']                    = $qp170PE8.coordonnees.coordonneesOrigine.codePostalAdresse;
cerfaFields['paysAdresse']                          = $qp170PE8.coordonnees.coordonneesOrigine.paysAdresse;
cerfaFields['telephoneAdresse']                     = $qp170PE8.coordonnees.coordonneesOrigine.telephoneAdresse;


var adresse = $qp170PE8.coordonnees.coordonneesOrigine.mailAdresse;
	cerfaFields['courriel1Adresse']                     = '';
	cerfaFields['courrielAdresse']                      = '';
if(adresse != null) {
	var adresseSplite = adresse.split("@");
	cerfaFields['courrielAdresse']                  = adresseSplite[0];
	cerfaFields['courriel1Adresse']                 = adresseSplite[1];
}

//Adresse en France 

cerfaFields['numeroLibelleComplementAdresseFrance'] = ($qp170PE8.coordonneesFrance.coordonneesFrance.numeroLibelleAdresseFrance != null ? $qp170PE8.coordonneesFrance.coordonneesFrance.numeroLibelleAdresseFrance : '') 
												  + ' ' + ($qp170PE8.coordonneesFrance.coordonneesFrance.complementAdresseFrance != null ? $qp170PE8.coordonneesFrance.coordonneesFrance.complementAdresseFrance : '') ;
cerfaFields['villeAdresseFrance']                   = $qp170PE8.coordonneesFrance.coordonneesFrance.villeAdresseFrance;
cerfaFields['codePostalAdresseFrance']              = $qp170PE8.coordonneesFrance.coordonneesFrance.codePostalAdresseFrance;
cerfaFields['paysAdresseFrance']                    = $qp170PE8.coordonneesFrance.coordonneesFrance.paysAdresseFrance;
cerfaFields['telephoneAdresseFrance']               = $qp170PE8.coordonneesFrance.coordonneesFrance.telephoneAdresseFrance;

//Adresse professionnel en France 

cerfaFields['numeroLibelleComplementAdressePro']    = ($qp170PE8.coordonneesProfessionnel.coordonneesProfessionnel.numeroLibelleAdressePro != null ? $qp170PE8.coordonneesProfessionnel.coordonneesProfessionnel.numeroLibelleAdressePro : '') 
											  + ' ' + ($qp170PE8.coordonneesProfessionnel.coordonneesProfessionnel.complementAdresseFrance != null ? $qp170PE8.coordonneesProfessionnel.coordonneesProfessionnel.complementAdresseFrance : '') ;
cerfaFields['villeAdressePro']                      = $qp170PE8.coordonneesProfessionnel.coordonneesProfessionnel.villeAdressePro;
cerfaFields['codePostalAdressePro']                 = $qp170PE8.coordonneesProfessionnel.coordonneesProfessionnel.codePostalAdressePro;
cerfaFields['paysAdressePro']                       = $qp170PE8.coordonneesProfessionnel.coordonneesProfessionnel.paysAdressePro;
cerfaFields['telephoneAdressePro']                  = $qp170PE8.coordonneesProfessionnel.coordonneesProfessionnel.telephoneAdressePro;


cerfaFields['precisionDureeFrequencePrestation']    = $qp170PE8.coordonneesPrecision.coordonneesPrecision.precisionDureeFrequencePrestation;

//Titre de formation de la profession considérée 

cerfaFields['titreFormation']                       = $qp170PE8.titreFormation.titreFormation.intituleTitreFormation;

if($qp170PE8.titreFormation.titreFormation.dateObtentionFormation != null) {
var dateTemp = new Date(parseInt ($qp170PE8.titreFormation.titreFormation.dateObtentionFormation.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['dateTitreFormationJour']  = day;
	cerfaFields['dateTitreFormationMois']  = month;
	cerfaFields['dateTitreFormationAnnee']   = year;
}

cerfaFields['lieuTitreFormation']                   = $qp170PE8.titreFormation.titreFormation.lieuTitreFormation;

cerfaFields['dateReconnaissanceFormationJour']      = '';
cerfaFields['dateReconnaissanceFormationMois']      = '';
cerfaFields['dateReconnaissanceFormationAnnee']     = '';

if($qp170PE8.titreFormation.titreFormation.dateObtentionFormation1 != null) {
var dateTemp = new Date(parseInt ($qp170PE8.titreFormation.titreFormation.dateObtentionFormation1.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['dateReconnaissanceFormationJour']   = day;
	cerfaFields['dateReconnaissanceFormationMois']  = month;
	cerfaFields['dateReconnaissanceFormationAnnee']   = year;
}

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                = Value('id').of($qp170PE8.oredreProfessionnel.oredreProfessionnel.ordreProfessionnelOuiNon).eq('ordreProfessionnelOui')? true : false;
cerfaFields['ordreProffessionnelNon']               = Value('id').of($qp170PE8.oredreProfessionnel.oredreProfessionnel.ordreProfessionnelOuiNon).eq('ordreProfessionnelNon')? true : false;;

cerfaFields['informationOrdreProfessionnel']        = $qp170PE8.oredreProfessionnel.oredreProfessionnel.nomOrdreProfessionnel;


cerfaFields['nomCompagnieAssurance']                = $qp170PE8.assurancePro.assurancePro.nomCompagnieAssurance;
cerfaFields['numeroContratAssurance']               = $qp170PE8.assurancePro.assurancePro.numeroContratAssurance;





cerfaFields['commentairesEventuels']                = $qp170PE8.commentaire.commentaire.commentairesEventuels;

//Information à fournir en cas de renouvellement 

//Prestation 1

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService1 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService1.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['debutDateRenouvellementJour1']   		= day;
cerfaFields['debutDateRenouvellementMois1']       	= month;
cerfaFields['debutDateRenouvellementAnnee1']       	= year;

}

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService1 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService1.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['finDateRenouvellementJour1']       = day;
cerfaFields['finDateRenouvellementMois1']       = month;
cerfaFields['finDateRenouvellementAnnee1']		= year;

}

//Prestation 2

cerfaFields['debutDateRenouvellementJour2']   		= '';
cerfaFields['debutDateRenouvellementMois2']       	= '';
cerfaFields['debutDateRenouvellementAnnee2']       	= '';

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService2 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService2.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['debutDateRenouvellementJour2']   		= day;
cerfaFields['debutDateRenouvellementMois2']       	= month;
cerfaFields['debutDateRenouvellementAnnee2']       	= year;

}
cerfaFields['finDateRenouvellementJour2']       = '';
cerfaFields['finDateRenouvellementMois2']       = '';
cerfaFields['finDateRenouvellementAnnee2']		= ''

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService2 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService2.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['finDateRenouvellementJour2']       = day;
cerfaFields['finDateRenouvellementMois2']       = month;
cerfaFields['finDateRenouvellementAnnee2']		= year;

}

//Prestation 3
cerfaFields['debutDateRenouvellementJour3']   		= '';
cerfaFields['debutDateRenouvellementMois3']       	= '';
cerfaFields['debutDateRenouvellementAnnee3']       	= '';

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService3 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService3.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['debutDateRenouvellementJour3']   		= day;
cerfaFields['debutDateRenouvellementMois3']       	= month;
cerfaFields['debutDateRenouvellementAnnee3']       	= year;

}
cerfaFields['finDateRenouvellementJour3']       = '';
cerfaFields['finDateRenouvellementMois3']       = '';
cerfaFields['finDateRenouvellementAnnee3']		= ''

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService3 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService3.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['finDateRenouvellementJour3']       = day;
cerfaFields['finDateRenouvellementMois3']       = month;
cerfaFields['finDateRenouvellementAnnee3']		= year;

}

	   
//Prestation 4	   
cerfaFields['debutDateRenouvellementJour4']   		= '';
cerfaFields['debutDateRenouvellementMois4']       	= '';
cerfaFields['debutDateRenouvellementAnnee4']       	= '';

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService4 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService4.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['debutDateRenouvellementJour4']   		= day;
cerfaFields['debutDateRenouvellementMois4']       	= month;
cerfaFields['debutDateRenouvellementAnnee4']       	= year;

}
cerfaFields['finDateRenouvellementJour4']       = '';
cerfaFields['finDateRenouvellementMois4']       = '';
cerfaFields['finDateRenouvellementAnnee4']		= ''

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService4 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService4.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['finDateRenouvellementJour4']       = day;
cerfaFields['finDateRenouvellementMois4']       = month;
cerfaFields['finDateRenouvellementAnnee4']		= year;

}	   
//Prestation 5	   
	   
cerfaFields['debutDateRenouvellementJour5']   		= '';
cerfaFields['debutDateRenouvellementMois5']       	= '';
cerfaFields['debutDateRenouvellementAnnee5']       	= '';

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService5 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService5.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['debutDateRenouvellementJour5']   		= day;
cerfaFields['debutDateRenouvellementMois5']       	= month;
cerfaFields['debutDateRenouvellementAnnee5']       	= year;

}
cerfaFields['finDateRenouvellementJour5']       = '';
cerfaFields['finDateRenouvellementMois5']       = '';
cerfaFields['finDateRenouvellementAnnee5']		= ''

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService5 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService5.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['finDateRenouvellementJour5']       = day;
cerfaFields['finDateRenouvellementMois5']       = month;
cerfaFields['finDateRenouvellementAnnee5']		= year;

}

//PRestation 6

cerfaFields['debutDateRenouvellementJour6']   		= '';
cerfaFields['debutDateRenouvellementMois6']       	= '';
cerfaFields['debutDateRenouvellementAnnee6']       	= '';

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService6 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService6.from.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['debutDateRenouvellementJour6']   		= day;
cerfaFields['debutDateRenouvellementMois6']       	= month;
cerfaFields['debutDateRenouvellementAnnee6']       	= year;

}
cerfaFields['finDateRenouvellementJour6']       = '';
cerfaFields['finDateRenouvellementMois6']       = '';
cerfaFields['finDateRenouvellementAnnee6']		= ''

if($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService6 != null) {    
var dateTemp = new Date(parseInt ($qp170PE8.informationRenouvellement.informationRenouvellement.datePrestationService6.to.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
		
cerfaFields['finDateRenouvellementJour6']       = day;
cerfaFields['finDateRenouvellementMois6']       = month;
cerfaFields['finDateRenouvellementAnnee6']		= year;

}



cerfaFields['activiteProfessionnellesExercees']     = $qp170PE8.informationRenouvellement.informationRenouvellement.activiteProfessionnellesExercees;

if($qp170PE8.signature.signature.dateSignature != null) {
var dateTemp = new Date(parseInt ($qp170PE8.signature.signature.dateSignature.getTimeInMillis()));
   	var monthTemp = dateTemp.getMonth() + 1;
    	var month = pad(monthTemp.toString());
   	var day =  pad(dateTemp.getDate().toString());
    	var year = dateTemp.getFullYear();
	cerfaFields['signatureDateJour']  = day;
	cerfaFields['signatureDateMois']   = month;
	cerfaFields['signatureDateAnnee']    = year;
}

cerfaFields['signature']                                           = civNomPrenom;



var cerfa = pdf.create('models/cerfa', cerfaFields);



cerfaFields['libelleProfession']				= "Masseur-kinésithérapeute"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp015PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp170PE8.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’ordre des masseurs kinésithérapeutes",
		demandeContexte : "Renouvellement de déclaration de libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */

var cerfaDoc = nash.doc //
	.load('models/Formulaire LPS Kinésithérapeute.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

 
 
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjRenouvellement);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Masseur_kinesitherapeute_LPS_Modif.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Masseur-kinésithérapeute - renouvellement de déclaration de libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Renouvellement de déclaration en vue d\'une libre prestation de services pour la profession de masseur-kinésithérapeute.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
