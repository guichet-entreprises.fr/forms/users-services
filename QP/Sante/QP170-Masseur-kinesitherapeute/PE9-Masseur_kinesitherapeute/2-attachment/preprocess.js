attachment('pjPhoto', 'pjPhoto');
attachment('pjID', 'pjID');
attachment('pjCV', 'pjCV');
attachment('pjJustifDomicile', 'pjJustifDomicile');
attachment('pjTitreFormation', 'pjTitreFormation');
attachment('pjDiplomeComplementaire', 'pjDiplomeComplementaire', {
	mandatory : "true"
});

attachment('pjCartePro', 'pjCartePro');
attachment('pjModeExercice', 'pjModeExercice');
attachment('pjAttestationHonneur', 'pjAttestationHonneur');
attachment('pjCertifRadiation', 'pjCertifRadiation');
attachment('pjCasier', 'pjCasier');

var cheminExerciceLiberalMixte = $qp170PE9.exerciceProfessionnel.exerciceProfessionnel;

if (Value('id').of(cheminExerciceLiberalMixte.typeExerciceProfessionnel).eq(
		'liberalCoche')
		|| Value('id').of(cheminExerciceLiberalMixte.typeExerciceProfessionnel)
				.eq('mixteCoche')) {
	attachment('pjAutorisationExercice', 'pjAutorisationExercice');
	attachment('pjAttestationAssurence', 'pjAttestationAssurence');
}
