var cerfaFields = {};
var civNomPrenom = $qp170PE9.information.etatCivil.civilite + ' ' + $qp170PE9.information.etatCivil.nom + ' ' + $qp170PE9.information.etatCivil.prenom;



//etatCivil


cerfaFields['madame']                               =  ($qp170PE9.information.etatCivil.civilite=='Madame');
cerfaFields['monsieur']                             =  ($qp170PE9.information.etatCivil.civilite=='Monsieur');
cerfaFields['nom']                                  = $qp170PE9.information.etatCivil.nom;
cerfaFields['nomUsage']                             = $qp170PE9.information.etatCivil.nomUsage;
cerfaFields['prenom']                               = $qp170PE9.information.etatCivil.prenom;
cerfaFields['prenom2']                              = $qp170PE9.information.etatCivil.prenom2;
cerfaFields['prenom3']                              = $qp170PE9.information.etatCivil.prenom3;
cerfaFields['dateNaissance']                        = $qp170PE9.information.etatCivil.dateNaissance;
cerfaFields['villeNaissance']                       = $qp170PE9.information.etatCivil.villeNaissance +' '+ ($qp170PE9.information.etatCivil.villeNaissance != null ? $qp170PE9.information.etatCivil.villeNaissance :'') 
													+' '+ $qp170PE9.information.etatCivil.paysNaissance ;  
cerfaFields['nationaliteFrancaisCoche']             = Value('id').of($qp170PE9.information.etatCivil.nationalite).eq('nationaliteFrancaisCoche')? true : false;
cerfaFields['nationaliteUECoche']                   = Value('id').of($qp170PE9.information.etatCivil.nationalite).eq('nationaliteUECoche')? true : false;
cerfaFields['nationalitehorsUECoche']               = Value('id').of($qp170PE9.information.etatCivil.nationalite).eq('nationalitehorsUECoche')? true : false;

cerfaFields['nationalitehorsUE']                    = $qp170PE9.information.etatCivil.nationalitehorsUE;
cerfaFields['nationaliteUE']                        = $qp170PE9.information.etatCivil.nationaliteUE;

cerfaFields['dateAcquisitionNationalite']           = $qp170PE9.information.etatCivil.dateAcquisitionNationalite;

//Coordonnées dans l'état ou vous êtes etabli

cerfaFields['complementAdresse']                           = $qp170PE9.coordonnees.coordonnees.complementAdresse;
cerfaFields['typeNomVoieAdresse']                          = $qp170PE9.coordonnees.coordonnees.typeNomVoieAdresse;
cerfaFields['lieuDitAdresse']                              = $qp170PE9.coordonnees.coordonnees.lieuDitAdresse;
cerfaFields['numeroVoieAdresse']                           = $qp170PE9.coordonnees.coordonnees.numeroVoieAdresse;
cerfaFields['codePostalAdresse']                           = $qp170PE9.coordonnees.coordonnees.codePostalAdresse;
cerfaFields['villeAdresse']                                = $qp170PE9.coordonnees.coordonnees.villeAdresse;
cerfaFields['paysAdresse']                                 = $qp170PE9.coordonnees.coordonnees.paysAdresse;
cerfaFields['telephoneFixeAdresse']                        = $qp170PE9.coordonnees.coordonnees.telephoneFixeAdresse;
cerfaFields['telephoneMobileAdresse']                      = $qp170PE9.coordonnees.coordonnees.telephoneMobileAdresse;

//Diplome  

cerfaFields['libelleDiplome']                              = $qp170PE9.diplome.diplome1.libelleDiplome;
cerfaFields['dateObtentionDiplome']                        = $qp170PE9.diplome.diplome1.dateObtentionDiplome;
cerfaFields['numeroDiplome']                               = $qp170PE9.diplome.diplome1.numeroDiplome;
cerfaFields['villePaysObtentionDiplome']                   = $qp170PE9.diplome.diplome1.villePaysObtentionDiplome;
cerfaFields['lieuFormationDiplome']                        = $qp170PE9.diplome.diplome1.lieuFormationDiplome;
cerfaFields['autoriteAutorisationExerciceDiplome']         = $qp170PE9.diplome.diplome1.autoriteAutorisationExerciceDiplome ;  
cerfaFields['dateAutorisationExerciceDiplome'] 			   = $qp170PE9.diplome.diplome1.dateAutorisationExerciceDiplome ;
//Diplome postebac

cerfaFields['libelleDiplomePostBac']                       = $qp170PE9.autresDiplomes.autresDiplomes.libelleDiplomePostBac;
cerfaFields['dateObtentionDiplomePostBac']                 = $qp170PE9.autresDiplomes.autresDiplomes.dateObtentionDiplomePostBac;
cerfaFields['numeroDiplomePostBac']                        = $qp170PE9.autresDiplomes.autresDiplomes.numeroDiplomePostBac;
cerfaFields['villePaysDiplomePostBac']                     = $qp170PE9.autresDiplomes.autresDiplomes.villePaysDiplomePostBac;
cerfaFields['lieuFormationDiplomePostBac']                 = $qp170PE9.autresDiplomes.autresDiplomes.lieuFormationDiplomePostBac;

//Exercice professionnel

cerfaFields['salarieCoche1']                               = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.typeExerciceProfessionnel).eq('salarieCoche1')? true : false;
cerfaFields['liberalCoche']                                = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.typeExerciceProfessionnel).eq('liberalCoche')? true : false;
cerfaFields['mixteCoche']                                  = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.typeExerciceProfessionnel).eq('mixteCoche')? true : false;
cerfaFields['autreExerciceProfessionnelCoche1']            = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.typeExerciceProfessionnel).eq('autreExerciceProfessionnelCoche1')? true : false;

cerfaFields['liberalConventionneCoche1']                   = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.liberalOption).eq('liberalConventionneCoche1')? true : false;
cerfaFields['liberalNonConventionneCoche1']                = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.liberalOption).eq('liberalNonConventionneCoche1')? true : false;


cerfaFields['salarieFonctionPubliqueCoche']                = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.salarieOption).eq('salarieFonctionPubliqueCoche')? true : false;
cerfaFields['salarieAgentContractuelCoche']                = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.salarieOption).eq('salarieAgentContractuelCoche')? true : false;
cerfaFields['salarieSecteurPriveCoche']                    = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.salarieOption).eq('salarieSecteurPriveCoche')? true : false;
cerfaFields['salarieAutreCoche']                           = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.salarieOption).eq('salarieAutreCoche')? true : false;
cerfaFields['salarieContractuelCDDCoche'] 				   = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.choixContrat1).eq('salarieContractuelCDDCoche')? true : false;
cerfaFields['salarieContractuelCDICoche'] 				   = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.choixContrat1).eq('salarieContractuelCDICoche')? true : false;
cerfaFields['salarieSecteurPriveCDICoche'] 				   = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.choixContrat2).eq('salarieSecteurPriveCDICoche')? true : false;
cerfaFields['salarieSecteurPriveCDDCoche'] 				   = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.choixContrat2).eq('salarieSecteurPriveCDDCoche')? true : false;

cerfaFields['mixteLiberalCoche']                           = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.choixContrat2).eq('mixteLiberalCoche')? true : false;
cerfaFields['mixteSalarieCoche']                           = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.choixContrat2).eq('mixteSalarieCoche')? true : false;
cerfaFields['mixteLiberalConventionneCoche']               = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteLiberalOption1).contains('mixteLiberalConventionneCoche')? true : false;
cerfaFields['mixteLiberalNonConventionneCoche']            = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteLiberalOption1).contains('mixteLiberalNonConventionneCoche')? true : false;

cerfaFields['mixteSalarieAgentContractueleCoche']          = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteSalarieOption).eq('mixteSalarieAgentContractueleCoche')? true : false;
cerfaFields['mixteSalarieSecteurPriveeCoche']              = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteSalarieOption).eq('mixteSalarieSecteurPriveeCoche')? true : false;
cerfaFields['mixteSalarieAutreCoche']                      = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteSalarieOption).eq('mixteSalarieAutreCoche')? true : false;
cerfaFields['mixteSalarieSecteurPriveeCDICoche']           = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteChoixContrat1).eq('mixteSalarieSecteurPriveeCDICoche')? true : false;
cerfaFields['mixteSalarieSecteurPriveeCDDCoche']           = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteChoixContrat1).eq('mixteSalarieSecteurPriveeCDDCoche')? true : false;
cerfaFields['mixteSalarieContractuelCDICoche']             = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteChoixContrat2).eq('mixteSalarieContractuelCDICoche')? true : false;
cerfaFields['mixteSalarieContractuelCDDCoche']             = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixteChoixContrat2).eq('mixteSalarieContractuelCDDCoche')? true : false;



cerfaFields['choixAdressePersonnelCoche1']                 = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.choixEnvoieCourrier).eq('choixAdressePersonnelCoche1')? true : false;
cerfaFields['choixAdresseProfessionnelCoche1']             = Value('id').of($qp170PE9.exerciceProfessionnel.exerciceProfessionnel.choixEnvoieCourrier).eq('choixAdresseProfessionnelCoche1')? true : false;

cerfaFields['autreExerciceProfessionnel']                  = $qp170PE9.exerciceProfessionnel.exerciceProfessionnel.autreExerciceProfessionnel;
cerfaFields['specificiteExercice']                         = $qp170PE9.exerciceProfessionnel.exerciceProfessionnel.specificiteExercice;
cerfaFields['dateDernierDPC']                              = $qp170PE9.exerciceProfessionnel.exerciceProfessionnel.dateDernierDPC;
cerfaFields['numeroRPPS']                                  = $qp170PE9.exerciceProfessionnel.exerciceProfessionnel.numeroRPPS;
cerfaFields['precisionAutre']                              = $qp170PE9.exerciceProfessionnel.exerciceProfessionnel.precisionAutre;
cerfaFields['mixtePrecisionAutre']                         = $qp170PE9.exerciceProfessionnel.exerciceProfessionnel.mixtePrecisionAutre;

//Langues

cerfaFields['langue1']                                     = $qp170PE9.languesEtrangeres.languesEtrangeres.langue1;
cerfaFields['langue2']                                     = $qp170PE9.languesEtrangeres.languesEtrangeres.langue2;
cerfaFields['langue3']                                     = $qp170PE9.languesEtrangeres.languesEtrangeres.langue3;
cerfaFields['langueCourantCoche1']                         = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue1).eq('langueCourantCoche1')? true : false;
cerfaFields['langueNiveauScolaireCoche1']                  = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue1).eq('langueNiveauScolaireCoche1')? true : false;
cerfaFields['langueBilingueCoche1']                        = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue1).eq('langueBilingueCoche1')? true : false;
cerfaFields['langueCourantCoche2']                         = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue2).eq('langueCourantCoche2')? true : false;
cerfaFields['langueNiveauScolaireCoche2']                  = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue2).eq('langueNiveauScolaireCoche2')? true : false;
cerfaFields['langueBilingueCoche2']                        = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue2).eq('langueBilingueCoche2')? true : false;
cerfaFields['langueCourantCoche3']                         = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue3).eq('langueCourantCoche3')? true : false;
cerfaFields['langueNiveauScolaireCoche3']                  = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue3).eq('langueNiveauScolaireCoche3')? true : false;
cerfaFields['langueBilingueCoche3']                        = Value('id').of($qp170PE9.languesEtrangeres.languesEtrangeres.niveauLangue3).eq('langueBilingueCoche3')? true : false;

//Activité principale

cerfaFields['activiteLiberalCoche']                        = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.activitePrincipale1).eq('activiteLiberalCoche')? true : false;
cerfaFields['activiteSalarieCoche']                        = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.activitePrincipale1).eq('activiteSalarieCoche')? true : false;
cerfaFields['dateDebutActivite']                           = $qp170PE9.activitePrincipale.activitePrincipale.dateDebutActivite;
cerfaFields['activiteFonctionTitulaireCoche']              = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.fonctionActivitePrincipale).eq('activiteFonctionTitulaireCoche')? true : false;
cerfaFields['activiteFonctionCollaborateurCoche']          = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.fonctionActivitePrincipale).eq('activiteFonctionCollaborateurCoche')? true : false;
cerfaFields['activiteFonctionAssocieCoche']                = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.fonctionActivitePrincipale).eq('activiteFonctionAssocieCoche')? true : false;
cerfaFields['activiteFonctionRemplacantCoche']             = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.fonctionActivitePrincipale).eq('activiteFonctionRemplacantCoche')? true : false;
cerfaFields['activiteFonctionSansExerciceCoche']           = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.fonctionActivitePrincipale).eq('activiteFonctionSansExerciceCoche')? true : false;
cerfaFields['activiteFonctionRetraiteCoche']               = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.fonctionActivitePrincipale).eq('activiteFonctionRetraiteCoche')? true : false;
cerfaFields['activiteFonctionAssistantCoche']              = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.fonctionActivitePrincipale).eq('activiteFonctionAssistantCoche')? true : false;
cerfaFields['activiteFonctionAutreCoche']                  = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.fonctionActivitePrincipale).eq('activiteFonctionAutreCoche')? true : false;
cerfaFields['activiteFonctionAutre']                       = $qp170PE9.activitePrincipale.activitePrincipale.activiteFonctionAutre;
cerfaFields['typeStructureCabinetIndividuelleCoche']       = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureCabinetIndividuelleCoche')? true : false;
cerfaFields['typeStructureCabinetGroupeCoche']             = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureCabinetGroupeCoche')? true : false;
cerfaFields['typeStructureExerciceDomicileCoche']          = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureExerciceDomicileCoche')? true : false;
cerfaFields['typeStructureSCPCoche']                       = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureSCPCoche')? true : false;
cerfaFields['typeStructureEtablissementPriveCoche']        = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure).eq('typeStructureEtablissementPriveCoche')? true : false;
cerfaFields['typeStructureSELCoche']                       = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureSELCoche')? true : false;
cerfaFields['typeStructureSISACoche']                      = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureSISACoche')? true : false;
cerfaFields['typeStructureSCMCoche']                       = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureSCMCoche')? true : false;
cerfaFields['typeStructureSPFPLCoche']                     = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureSPFPLCoche')? true : false;
cerfaFields['typeStructureIFMKCoche']                      = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureIFMKCoche')? true : false;
cerfaFields['typeStructureEtablissementPublicCoche']       = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureEtablissementPublicCoche')? true : false;
cerfaFields['typeStructureEtablissementNonLucratifCoche']  = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureEtablissementNonLucratifCoche')? true : false;
cerfaFields['typeStructureEntrepriseInterimCoche']         = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureEntrepriseInterimCoche')? true : false;
cerfaFields['typeStructureAutresCoche']                    = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.typeStructure).eq('typeStructureAutresCoche')? true : false;
cerfaFields['typeStructureAutres']                         = $qp170PE9.activitePrincipale.activitePrincipale.typeStructureAutres;
cerfaFields['raisonSocialeStructure']                      = $qp170PE9.activitePrincipale.activitePrincipale.raisonSocialeStructure;
cerfaFields['cabinetBassinCocheOui']                       = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.cabinetBassin1).eq('cabinetBassinCocheOui')? true : false;
cerfaFields['cabinetBassinCocheNon']                       = Value('id').of($qp170PE9.activitePrincipale.activitePrincipale.cabinetBassin1).eq('cabinetBassinCocheNon')? true : false;
cerfaFields['nomPrenomInscriptionOrdre']                   = $qp170PE9.activitePrincipale.activitePrincipale.nomPrenomInscriptionOrdre;
cerfaFields['numeroVoieAdresse1']                          = $qp170PE9.activitePrincipale.activitePrincipale.numeroVoieAdresse1;
cerfaFields['typeNomVoieAdresse1']                         = $qp170PE9.activitePrincipale.activitePrincipale.typeNomVoieAdresse1;
cerfaFields['complementAdresse1']                          = $qp170PE9.activitePrincipale.activitePrincipale.complementAdresse1;
cerfaFields['lieuDitAdresse1']                             = $qp170PE9.activitePrincipale.activitePrincipale.lieuDitAdresse1;
cerfaFields['codePostalAdresse1']                          = $qp170PE9.activitePrincipale.activitePrincipale.codePostalAdresse1;
cerfaFields['villeAdresse1']                               = $qp170PE9.activitePrincipale.activitePrincipale.villeAdresse1;
cerfaFields['paysAdresse1']                                = $qp170PE9.activitePrincipale.activitePrincipale.paysAdresse1;
cerfaFields['telephoneFixeAdresse1']                       = $qp170PE9.activitePrincipale.activitePrincipale.telephoneFixeAdresse1;
cerfaFields['telephoneMobileAdresse1']                     = $qp170PE9.activitePrincipale.activitePrincipale.telephoneMobileAdresse1;
cerfaFields['courrielAdresse1']                            = $qp170PE9.activitePrincipale.activitePrincipale.courrielAdresse1;
cerfaFields['numeroSiret1']                                = $qp170PE9.activitePrincipale.activitePrincipale.numeroSiret1;
cerfaFields['numeroFiness1']                               = $qp170PE9.activitePrincipale.activitePrincipale.numeroFiness1;

//Activité secondaire

cerfaFields['activiteLiberalCoche2']                       = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.activiteSecondaire1).eq('activiteLiberalCoche2')? true : false;
cerfaFields['activiteSalarieCoche2']                       = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.activiteSecondaire1).eq('activiteSalarieCoche2')? true : false;
cerfaFields['datedebutActivite2']                          = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.datedebutActivite2;
cerfaFields['activiteFonctionTitulaireCoche2']             = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.fonctionActiviteSecondaire).eq('activiteFonctionTitulaireCoche2')? true : false;
cerfaFields['activiteFonctionRetraiteCoche2']              = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.fonctionActiviteSecondaire).eq('activiteFonctionRetraiteCoche2')? true : false;
cerfaFields['activiteFonctionAssocieCoche2']               = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.fonctionActiviteSecondaire).eq('activiteFonctionAssocieCoche2')? true : false;
cerfaFields['activiteFonctionAssistantCoche2']             = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.fonctionActiviteSecondaire).eq('activiteFonctionAssistantCoche2')? true : false;
cerfaFields['activiteFonctionAutreCoche2']                 = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.fonctionActiviteSecondaire).eq('activiteFonctionAutreCoche2')? true : false;
cerfaFields['activiteFonctionRemplacantCoche2']            = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.fonctionActiviteSecondaire).eq('activiteFonctionRemplacantCoche2')? true : false;
cerfaFields['activiteFonctionSansExerciceCoche2']          = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.fonctionActiviteSecondaire).eq('activiteFonctionSansExerciceCoche2')? true : false;
cerfaFields['activiteFonctionCollaborateurCoche2']         = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.fonctionActiviteSecondaire).eq('activiteFonctionCollaborateurCoche2')? true : false;
cerfaFields['activiteFonctionAutre2']                      = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.activiteFonctionAutre2;
cerfaFields['typeStructureCabinetIndividuelleCoche2']      = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureCabinetIndividuelleCoche2')? true : false;
cerfaFields['typeStructureSISACoche2']                     = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureSISACoche2')? true : false;
cerfaFields['typeStructureSCMCoche2']                      = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureSCMCoche2')? true : false;
cerfaFields['typeStructureSPFPLCoche2']                    = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureSPFPLCoche2')? true : false;
cerfaFields['typeStructureCabinetGroupeCoche2']            = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureCabinetGroupeCoche2')? true : false;
cerfaFields['typeStructureIFMKCoche2']                     = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureIFMKCoche2')? true : false;
cerfaFields['typeStructureEtablissementPublicCoche2']      = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureEtablissementPublicCoche2')? true : false;
cerfaFields['typeStructureEntrepriseInterimCoche2']        = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureEntrepriseInterimCoche2')? true : false;
cerfaFields['typeStructureExerciceDomicileCoche2']         = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureExerciceDomicileCoche2')? true : false;
cerfaFields['typeStructureAutresCoche2']                   = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureAutresCoche2')? true : false;
cerfaFields['typeStructureSCPCoche2']                      = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureSCPCoche2')? true : false;
cerfaFields['typeStructureSELCoche2']                      = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureSELCoche2')? true : false;
cerfaFields['typeStructureEtablissementNonLucratifCoche2'] = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureEtablissementNonLucratifCoche2')? true : false;
cerfaFields['typeStructureEtablissementPriveCoche2']       = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructure2).eq('typeStructureEtablissementPriveCoche2')? true : false;
cerfaFields['typeStructureAutre2']                         = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeStructureAutre2;
cerfaFields['raisonSocialeStructure2']                     = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.raisonSocialeStructure2;
cerfaFields['cabinetBassinCocheOui2']                      = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.cabinetBassin2).eq('cabinetBassinCocheOui2')? true : false;
cerfaFields['cabinetBassinCocheNon2']                      = Value('id').of($qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.cabinetBassin2).eq('cabinetBassinCocheNon2')? true : false;
cerfaFields['nomPrenomInscriptionOrdre2']                  = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.nomPrenomInscriptionOrdre2;
cerfaFields['typeNomVoieAdresse2']                         = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.typeNomVoieAdresse2;
cerfaFields['numeroVoieAdresse2']                          = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.numeroVoieAdresse2;
cerfaFields['complementAdresse2']                          = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.complementAdresse2;
cerfaFields['lieuDitAdresse2']                             = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.lieuDitAdresse2;
cerfaFields['codePostalAdresse2']                          = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.codePostalAdresse2;
cerfaFields['villeAdresse2']                               = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.villeAdresse2;
cerfaFields['paysAdresse2']                                = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.paysAdresse2;
cerfaFields['telephoneFixeAdresse2']                       = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.telephoneFixeAdresse2;
cerfaFields['telephoneMobileAdresse2']                     = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.telephoneMobileAdresse2;
cerfaFields['courrielAdresse2']                            = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.courrielAdresse2;
cerfaFields['numeroSiret2']                                = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.numeroSiret2;
cerfaFields['numeroFiness2']                               = $qp170PE9.activiteSecondairePage.activiteSecondaireGroupe.numeroFiness2;

//Activité 3


cerfaFields['activiteLiberalCoche3']                       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.activite3).eq('activiteLiberalCoche3')? true : false;
cerfaFields['activiteSalarieCoche3']                       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.activite3).eq('activiteSalarieCoche3')? true : false;
cerfaFields['datedebutActivite3']                          = $qp170PE9.autreActivites3.autreActivites3.dateAutreActivites3;
cerfaFields['activiteFonctionTitulaireCoche3']             = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites3).eq('activiteFonctionTitulaireCoche3')? true : false;
cerfaFields['activiteFonctionRetraiteCoche3']              = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites3).eq('activiteFonctionRetraiteCoche3')? true : false;
cerfaFields['activiteFonctionCollaborateurCoche3']         = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites3).eq('activiteFonctionCollaborateurCoche3')? true : false;
cerfaFields['activiteFonctionAssistantCoche3']             = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites3).eq('activiteFonctionAssistantCoche3')? true : false;
cerfaFields['activiteFonctionAssocieCoche3']               = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites3).eq('activiteFonctionAssocieCoche3')? true : false;
cerfaFields['activiteFonctionRemplacantCoche3']            = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites3).eq('activiteFonctionRemplacantCoche3')? true : false;
cerfaFields['activiteFonctionAutreCoche3']                 = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites3).eq('activiteFonctionAutreCoche3')? true : false;
cerfaFields['activiteFonctionSansExerciceCoche3']          = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites3).eq('activiteFonctionSansExerciceCoche3')? true : false;
cerfaFields['activiteFonctionAutre3']                      = $qp170PE9.autreActivites3.autreActivites3.activiteFonctionAutre3;
cerfaFields['typeStructureCabinetGroupeCoche3']            = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureCabinetGroupeCoche3')? true : false;
cerfaFields['typeStructureCabinetIndividuelleCoche3']      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureCabinetIndividuelleCoche3')? true : false;
cerfaFields['typeStructureSISACoche3']                     = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureSISACoche3')? true : false;
cerfaFields['typeStructureEtablissementNonLucratifCoche3'] = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureEtablissementNonLucratifCoche3')? true : false;
cerfaFields['typeStructureSCMCoche3']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureSCMCoche3')? true : false;
cerfaFields['typeStructureSPFPLCoche3']                    = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureSPFPLCoche3')? true : false;
cerfaFields['typeStructureIFMKCoche3']                     = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureIFMKCoche3')? true : false;
cerfaFields['typeStructureEtablissementPublicCoche3']      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureEtablissementPublicCoche3')? true : false;
cerfaFields['typeStructureEntrepriseInterimCoche3']        = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureEntrepriseInterimCoche3')? true : false;
cerfaFields['typeStructureExerciceDomicileCoche3']         = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureExerciceDomicileCoche3')? true : false;
cerfaFields['typeStructureEtablissementPriveCoche3']       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureEtablissementPriveCoche3')? true : false;
cerfaFields['typeStructureAutresCoche3']                   = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureAutresCoche3')? true : false;
cerfaFields['typeStructureSCPCoche3']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureSCPCoche3')? true : false;
cerfaFields['typeStructureSELCoche3']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure3).eq('typeStructureSELCoche3')? true : false;
cerfaFields['typeStructureAutre3']                         = $qp170PE9.autreActivites3.autreActivites3.typeStructureAutre3;
cerfaFields['cabinetBassinCocheOui3']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.cabinetBassin3).eq('cabinetBassinCocheOui3')? true : false;
cerfaFields['cabinetBassinCocheNon3']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.cabinetBassin3).eq('cabinetBassinCocheNon3')? true : false;
cerfaFields['raisonSocialeStructure3']                     = $qp170PE9.autreActivites3.autreActivites3.raisonSocialeStructure3;
cerfaFields['nomPrenomInscriptionOrdre3']                  = $qp170PE9.autreActivites3.autreActivites3.nomPrenomInscriptionOrdre3;
cerfaFields['typeNomVoieAdresse3']                         = $qp170PE9.autreActivites3.autreActivites3.typeNomVoieAdresse3;
cerfaFields['numeroVoieAdresse3']                          = $qp170PE9.autreActivites3.autreActivites3.numeroVoieAdresse3;
cerfaFields['complementAdresse3']                          = $qp170PE9.autreActivites3.autreActivites3.complementAdresse3;
cerfaFields['lieuDitAdresse3']                             = $qp170PE9.autreActivites3.autreActivites3.lieuDitAdresse3;
cerfaFields['codePostalAdresse3']                          = $qp170PE9.autreActivites3.autreActivites3.codePostalAdresse3;
cerfaFields['villeAdresse3']                               = $qp170PE9.autreActivites3.autreActivites3.villeAdresse3;
cerfaFields['paysAdresse3']                                = $qp170PE9.autreActivites3.autreActivites3.paysAdresse3;
cerfaFields['telephoneMobileAdresse3']                     = $qp170PE9.autreActivites3.autreActivites3.telephoneMobileAdresse3;
cerfaFields['telephoneFixeAdresse3']                       = $qp170PE9.autreActivites3.autreActivites3.telephoneFixeAdresse3;
cerfaFields['courrielAdresse3']                            = $qp170PE9.autreActivites3.autreActivites3.courrielAdresse3;
cerfaFields['numeroSiret3']                                = $qp170PE9.autreActivites3.autreActivites3.numeroSiret3;
cerfaFields['numeroFiness3']                               = $qp170PE9.autreActivites3.autreActivites3.numeroFiness3;

//Activité 4

cerfaFields['activiteLiberalCoche4']                       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.activite4).eq('activiteLiberalCoche4')? true : false;
cerfaFields['activiteSalarieCoche4']                       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.activite4).eq('activiteSalarieCoche4')? true : false;
cerfaFields['datedebutActivite4']                          = $qp170PE9.autreActivites3.autreActivites3.datedebutActivite4;
cerfaFields['activiteFonctionCollaborateurCoche4']         = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites4).eq('activiteSalarieCoche4')? true : false;
cerfaFields['activiteFonctionTitulaireCoche4']             = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites4).eq('activiteFonctionTitulaireCoche4')? true : false;
cerfaFields['activiteFonctionRetraiteCoche4']              = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites4).eq('activiteFonctionRetraiteCoche4')? true : false;
cerfaFields['activiteFonctionAssistantCoche4']             = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites4).eq('activiteFonctionAssistantCoche4')? true : false;
cerfaFields['activiteFonctionAssocieCoche4']               = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites4).eq('activiteFonctionAssocieCoche4')? true : false;
cerfaFields['activiteFonctionRemplacantCoche4']            = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites4).eq('activiteFonctionRemplacantCoche4')? true : false;
cerfaFields['activiteFonctionAutreCoche4']                 = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites4).eq('activiteFonctionAutreCoche4')? true : false;
cerfaFields['activiteFonctionSansExerciceCoche4']          = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites4).eq('activiteFonctionSansExerciceCoche4')? true : false;
cerfaFields['activiteFonctionAutre4']                      = $qp170PE9.autreActivites3.autreActivites3.typeStructure4;
cerfaFields['typeStructureCabinetGroupeCoche4']            = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureCabinetGroupeCoche4')? true : false;
cerfaFields['typeStructureCabinetIndividuelleCoche4']      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureCabinetIndividuelleCoche4')? true : false;
cerfaFields['typeStructureSISACoche4']                     = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureSISACoche4')? true : false;
cerfaFields['typeStructureEtablissementNonLucratifCoche4'] = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureEtablissementNonLucratifCoche4')? true : false;
cerfaFields['typeStructureSCMCoche4']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureSCMCoche4')? true : false;
cerfaFields['typeStructureSPFPLCoche4']                    = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureSPFPLCoche4')? true : false;
cerfaFields['typeStructureIFMKCoche4']                     = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureIFMKCoche4')? true : false;
cerfaFields['typeStructureEtablissementPublicCoche4']      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureEtablissementPublicCoche4')? true : false;
cerfaFields['typeStructureEntrepriseInterimCoche4']        = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureEntrepriseInterimCoche4')? true : false;
cerfaFields['typeStructureExerciceDomicileCoche4']         = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureExerciceDomicileCoche4')? true : false;
cerfaFields['typeStructureEtablissementPriveCoche4']       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureEtablissementPriveCoche4')? true : false;
cerfaFields['typeStructureAutresCoche4']                   = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureAutresCoche4')? true : false;
cerfaFields['typeStructureSCPCoche4']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureSCPCoche4')? true : false;
cerfaFields['typeStructureSELCoche4']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure4).eq('typeStructureSELCoche4')? true : false;
cerfaFields['typeStructureAutre4']                         = $qp170PE9.autreActivites3.autreActivites3.typeStructureAutre4;
cerfaFields['cabinetBassinCocheOui4']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.cabinetBassin4).eq('cabinetBassinCocheOui4')? true : false;;
cerfaFields['cabinetBassinCocheNon4']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.cabinetBassin4).eq('cabinetBassinCocheNon4')? true : false;;
cerfaFields['raisonSocialeStructure4']                     = $qp170PE9.autreActivites3.autreActivites3.raisonSocialeStructure4;
cerfaFields['nomPrenomInscriptionOrdre4']                  = $qp170PE9.autreActivites3.autreActivites3.nomPrenomInscriptionOrdre4;
cerfaFields['typeNomVoieAdresse4']                         = $qp170PE9.autreActivites3.autreActivites3.typeNomVoieAdresse4;
cerfaFields['numeroVoieAdresse4']                          = $qp170PE9.autreActivites3.autreActivites3.numeroVoieAdresse4;
cerfaFields['complementAdresse4']                          = $qp170PE9.autreActivites3.autreActivites3.complementAdresse4;
cerfaFields['lieuDitAdresse4']                             = $qp170PE9.autreActivites3.autreActivites3.lieuDitAdresse4;
cerfaFields['codePostalAdresse4']                          = $qp170PE9.autreActivites3.autreActivites3.codePostalAdresse4;
cerfaFields['villeAdresse4']                               = $qp170PE9.autreActivites3.autreActivites3.villeAdresse4;
cerfaFields['paysAdresse4']                                = $qp170PE9.autreActivites3.autreActivites3.paysAdresse4;
cerfaFields['telephoneMobileAdresse4']                     = $qp170PE9.autreActivites3.autreActivites3.telephoneMobileAdresse4;
cerfaFields['telephoneFixeAdresse4']                       = $qp170PE9.autreActivites3.autreActivites3.telephoneFixeAdresse4;
cerfaFields['courrielAdresse4']                            = $qp170PE9.autreActivites3.autreActivites3.courrielAdresse4;
cerfaFields['numeroSiret4']                                = $qp170PE9.autreActivites3.autreActivites3.numeroSiret4;
cerfaFields['numeroFiness4']                               = $qp170PE9.autreActivites3.autreActivites3.numeroFiness4;

//Activite 5 

cerfaFields['activiteLiberalCoche5']                       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.activite5).eq('activiteLiberalCoche5')? true : false;
cerfaFields['activiteSalarieCoche5']                       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.activite5).eq('activiteSalarieCoche5')? true : false;
cerfaFields['datedebutActivite5']                          = $qp170PE9.autreActivites3.autreActivites3.datedebutActivite5;
cerfaFields['activiteFonctionTitulaireCoche5']             = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites5).eq('activiteFonctionTitulaireCoche5')? true : false;
cerfaFields['activiteFonctionRetraiteCoche5']              = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites5).eq('activiteFonctionRetraiteCoche5')? true : false;
cerfaFields['activiteFonctionCollaborateurCoche5']         = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites5).eq('activiteFonctionCollaborateurCoche5')? true : false;
cerfaFields['activiteFonctionAssistantCoche5']             = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites5).eq('activiteFonctionAssistantCoche5')? true : false;
cerfaFields['activiteFonctionAssocieCoche5']               = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites5).eq('activiteFonctionAssocieCoche5')? true : false;
cerfaFields['activiteFonctionRemplacantCoche5']            = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites5).eq('activiteFonctionRemplacantCoche5')? true : false;
cerfaFields['activiteFonctionAutreCoche5']                 = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites5).eq('activiteFonctionAutreCoche5')? true : false;
cerfaFields['activiteFonctionSansExerciceCoche5']          = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites5).eq('activiteFonctionSansExerciceCoche5')? true : false;
cerfaFields['activiteFonctionAutre5']                      = $qp170PE9.autreActivites3.autreActivites3.activiteFonctionAutre5;
cerfaFields['typeStructureCabinetIndividuelleCoche5']      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureCabinetIndividuelleCoche5')? true : false;
cerfaFields['typeStructureSISACoche5']                     = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureSISACoche5')? true : false;
cerfaFields['typeStructureSCMCoche5']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureSCMCoche5')? true : false;
cerfaFields['typeStructureSPFPLCoche5']                    = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureSPFPLCoche5')? true : false;
cerfaFields['typeStructureCabinetGroupeCoche5']            = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureCabinetGroupeCoche5')? true : false;
cerfaFields['typeStructureIFMKCoche5']                     = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureIFMKCoche5')? true : false;
cerfaFields['typeStructureEtablissementPublicCoche5']      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureEtablissementPublicCoche5')? true : false;
cerfaFields['typeStructureExerciceDomicileCoche5']         = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureExerciceDomicileCoche5')? true : false;
cerfaFields['typeStructureEtablissementPriveCoche5']       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureEtablissementPriveCoche5')? true : false;
cerfaFields['typeStructureSCPCoche5']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureSCPCoche5')? true : false;
cerfaFields['typeStructureSELCoche5']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureSELCoche5')? true : false;
cerfaFields['typeStructureEtablissementNonLucratifCoche5'] = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureEtablissementNonLucratifCoche5')? true : false;
cerfaFields['typeStructureEntrepriseInterimCoche5']        = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureEntrepriseInterimCoche5')? true : false;
cerfaFields['typeStructureAutresCoche5']                   = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure5).eq('typeStructureAutresCoche5')? true : false;
cerfaFields['typeStructureAutre5']                   	   = $qp170PE9.autreActivites3.autreActivites3.typeStructureAutre5;
cerfaFields['cabinetBassinCocheNon5']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.cabinetBassin5).eq('cabinetBassinCocheNon5')? true : false;
cerfaFields['cabinetBassinCocheOui5']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.cabinetBassin5).eq('cabinetBassinCocheOui5')? true : false;
cerfaFields['raisonSocialeStructure5']                     = $qp170PE9.autreActivites3.autreActivites3.raisonSocialeStructure5;
cerfaFields['nomPrenomInscriptionOrdre5']                  = $qp170PE9.autreActivites3.autreActivites3.nomPrenomInscriptionOrdre5;
cerfaFields['typeNomVoieAdresse5']                         = $qp170PE9.autreActivites3.autreActivites3.typeNomVoieAdresse5;
cerfaFields['numeroVoieAdresse5']                          = $qp170PE9.autreActivites3.autreActivites3.numeroVoieAdresse5;
cerfaFields['complementAdresse5']                          = $qp170PE9.autreActivites3.autreActivites3.complementAdresse5;
cerfaFields['lieuDitAdresse5']                             = $qp170PE9.autreActivites3.autreActivites3.lieuDitAdresse5;
cerfaFields['codePostalAdresse5']                          = $qp170PE9.autreActivites3.autreActivites3.codePostalAdresse5;
cerfaFields['villeAdresse5']                               = $qp170PE9.autreActivites3.autreActivites3.villeAdresse5;
cerfaFields['paysAdresse5']                                = $qp170PE9.autreActivites3.autreActivites3.paysAdresse5;
cerfaFields['telephoneMobileAdresse5']                     = $qp170PE9.autreActivites3.autreActivites3.telephoneMobileAdresse5;
cerfaFields['telephoneFixeAdresse5']                       = $qp170PE9.autreActivites3.autreActivites3.telephoneFixeAdresse5;
cerfaFields['courrielAdresse5']                            = $qp170PE9.autreActivites3.autreActivites3.courrielAdresse5;
cerfaFields['numeroSiret5']                                = $qp170PE9.autreActivites3.autreActivites3.numeroSiret5;
cerfaFields['numeroFiness5']                               = $qp170PE9.autreActivites3.autreActivites3.numeroFiness5;

//Activité 6

cerfaFields['activiteLiberalCoche6']                       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.activite6).eq('activiteLiberalCoche6')? true : false;
cerfaFields['activiteSalarieCoche6']                       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.activite6).eq('activiteSalarieCoche6')? true : false;
cerfaFields['datedebutActivite6']                          = $qp170PE9.autreActivites3.autreActivites3.datedebutActivite6;
cerfaFields['activiteFonctionTitulaireCoche6']             = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites6).eq('activiteFonctionTitulaireCoche6')? true : false;
cerfaFields['activiteFonctionCollaborateurCoche6']         = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites6).eq('activiteFonctionCollaborateurCoche6')? true : false;
cerfaFields['activiteFonctionAssocieCoche6']               = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites6).eq('activiteFonctionAssocieCoche6')? true : false;
cerfaFields['activiteFonctionRemplacantCoche6']            = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites6).eq('activiteFonctionRemplacantCoche6')? true : false;
cerfaFields['activiteFonctionSansExerciceCoche6']          = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites6).eq('activiteFonctionSansExerciceCoche6')? true : false;
cerfaFields['activiteFonctionRetraiteCoche6']              = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites6).eq('activiteFonctionRetraiteCoche6')? true : false;
cerfaFields['activiteFonctionAssistantCoche6']             = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites6).eq('activiteFonctionAssistantCoche6')? true : false;
cerfaFields['activiteFonctionAutreCoche6']                 = Value('id').of($qp170PE9.autreActivites3.autreActivites3.fonctionAutreActivites6).eq('activiteFonctionAutreCoche6')? true : false;
cerfaFields['activiteFonctionAutre6']                      = $qp170PE9.autreActivites3.autreActivites3.activiteFonctionAutre6;
cerfaFields['typeStructureCabinetIndividuelleCoche6']      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureCabinetIndividuelleCoche6')? true : false;
cerfaFields['typeStructureCabinetGroupeCoche6']            = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureCabinetGroupeCoche6')? true : false;
cerfaFields['typeStructureExerciceDomicileCoche6']         = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureExerciceDomicileCoche6')? true : false;
cerfaFields['typeStructureSCPCoche6']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureSCPCoche6')? true : false;
cerfaFields['typeStructureSELCoche6']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureSELCoche6')? true : false;
cerfaFields['typeStructureSISACoche6']                     = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureSISACoche6')? true : false;
cerfaFields['typeStructureSCMCoche6']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureSCMCoche6')? true : false;
cerfaFields['typeStructureSPFPLCoche6']                    = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureSPFPLCoche6')? true : false;
cerfaFields['typeStructureIFMKCoche6']                     = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureIFMKCoche6')? true : false;
cerfaFields['typeStructureEtablissementPublicCoche6']      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureEtablissementPublicCoche6')? true : false;
cerfaFields['typeStructureEtablissementPriveCoche6']       = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureEtablissementPriveCoche6')? true : false;
cerfaFields['typeStructureEtablissementNonLucratifCoche6'] = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureEtablissementNonLucratifCoche6')? true : false;
cerfaFields['typeStructureEntrepriseInterimCoche6']        = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureEntrepriseInterimCoche6')? true : false;
cerfaFields['typeStructureAutresCoche6']                   = Value('id').of($qp170PE9.autreActivites3.autreActivites3.typeStructure6).eq('typeStructureAutresCoche6')? true : false;
cerfaFields['typeStructureAutre6']                         = $qp170PE9.autreActivites3.autreActivites3.typeStructureAutre6;
cerfaFields['raisonSocialeStructure6']                     = $qp170PE9.autreActivites3.autreActivites3.raisonSocialeStructure6;
cerfaFields['cabinetBassinCocheOui6']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.cabinetBassin6).eq('cabinetBassinCocheOui6')? true : false;
cerfaFields['cabinetBassinCocheNon6']                      = Value('id').of($qp170PE9.autreActivites3.autreActivites3.cabinetBassin6).eq('cabinetBassinCocheNon6')? true : false;
cerfaFields['nomPrenomInscriptionOrdre6']                  = $qp170PE9.autreActivites3.autreActivites3.nomPrenomInscriptionOrdre6;
cerfaFields['numeroVoieAdresse6']                          = $qp170PE9.autreActivites3.autreActivites3.numeroVoieAdresse6;
cerfaFields['typeNomVoieAdresse6']                         = $qp170PE9.autreActivites3.autreActivites3.typeNomVoieAdresse6;
cerfaFields['complementAdresse6']                          = $qp170PE9.autreActivites3.autreActivites3.complementAdresse6;
cerfaFields['lieuDitAdresse6']                             = $qp170PE9.autreActivites3.autreActivites3.lieuDitAdresse6;
cerfaFields['codePostalAdresse6']                          = $qp170PE9.autreActivites3.autreActivites3.codePostalAdresse6;
cerfaFields['villeAdresse6']                               = $qp170PE9.autreActivites3.autreActivites3.villeAdresse6;
cerfaFields['paysAdresse6']                                = $qp170PE9.autreActivites3.autreActivites3.paysAdresse6;
cerfaFields['telephoneFixeAdresse6']                       = $qp170PE9.autreActivites3.autreActivites3.telephoneFixeAdresse6;
cerfaFields['telephoneMobileAdresse6']                     = $qp170PE9.autreActivites3.autreActivites3.telephoneMobileAdresse6;
cerfaFields['courrielAdresse6']                            = $qp170PE9.autreActivites3.autreActivites3.courrielAdresse6;
cerfaFields['numeroSiret6']                                = $qp170PE9.autreActivites3.autreActivites3.numeroSiret6;
cerfaFields['numeroFiness6']                               = $qp170PE9.autreActivites3.autreActivites3.numeroFiness6;

cerfaFields['luApprouveSignature']                         = "lu et approuvé";
cerfaFields['departementOrdreSignature']                   = $qp170PE9.signature.signature.departementsExercice;
cerfaFields['dateSignature']                               = $qp170PE9.signature.signature.dateSignature;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp015PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp170PE9.signature.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’ordre des masseurs kinésithérapeutes",
		demandeContexte : "Inscription au tableau de l’ordre des masseurs kinésithérapeutes",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */

var cerfaDoc = nash.doc //
	.load('models/formulaire inscription ordre.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitreFormation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomeComplementaire);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCartePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjModeExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationHonneur);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertifRadiation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);

var cheminExerciceLiberalMixte = $qp170PE9.exerciceProfessionnel.exerciceProfessionnel;

if (Value('id').of(cheminExerciceLiberalMixte.typeExerciceProfessionnel).eq(
		'liberalCoche')
		|| Value('id').of(cheminExerciceLiberalMixte.typeExerciceProfessionnel)
				.eq('mixteCoche')) {
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAutorisationExercice);
	appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationAssurence);
}

 
 
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Masseur_kinesitherapeute_LPS_Modif.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Masseur-kinésithérapeute - inscription au tableau de l’ordre des masseurs kinésithérapeutes',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Inscription au tableau de l’ordre des masseurs kinésithérapeutes.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
