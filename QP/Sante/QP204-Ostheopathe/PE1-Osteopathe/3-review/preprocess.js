var formFields = {};
function pad(s) { return (s < 10) ? '0' + s : s; }

var civNomPrenom = $qp204PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp204PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp204PE1.etatCivil.identificationDeclarant.prenomDeclarant;



//etatCivil
formFields['civiliteMonsieur']                                    = ($qp204PE1.etatCivil.identificationDeclarant.civilite=='Monsieur');
formFields['civiliteMadame']                                      = ($qp204PE1.etatCivil.identificationDeclarant.civilite=='Madame');
formFields['nomNaissanceDeclarant']                               = $qp204PE1.etatCivil.identificationDeclarant.nomDeclarant;
formFields['prenomDeclarant']                                     = $qp204PE1.etatCivil.identificationDeclarant.prenomDeclarant;
//formFields['dateNaissanceDeclarant']                              = $qp204PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
if($qp204PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant != null) {
	var dateTmp = new Date(parseInt($qp204PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant.getTimeInMillis()));
	var date = pad(dateTmp.getDate().toString());
	var month = dateTmp.getMonth() + 1;
	date = date.concat(pad(month.toString()));
	date = date.concat(dateTmp.getFullYear().toString());
	formFields['dateNaissanceDeclarant'] = date;
}
formFields['villeNaissanceDeclarant']                             = $qp204PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
formFields['paysNaissanceDeclarant']                              = $qp204PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
formFields['nationaliteDeclarant']                                = $qp204PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;


//adresse
formFields['adresseDeclarantResidence']                           = $qp204PE1.adresse.adressePersonnelle.adresseDeclarantResidance;
formFields['adresseDeclarantBatiment']                            = $qp204PE1.adresse.adressePersonnelle.adresseDeclarantBatiment;
formFields['adresseDeclarantEscalier']                            = $qp204PE1.adresse.adressePersonnelle.adresseDeclarantEscalier;
formFields['adresseDeclarantNumero']                              = $qp204PE1.adresse.adressePersonnelle.adresseDeclarantNumero;
formFields['adresseDeclarantRue']                                 = $qp204PE1.adresse.adressePersonnelle.adresseDeclarantRue;
formFields['adresseDeclarantCommune']                             = $qp204PE1.adresse.adressePersonnelle.villeAdresseDeclarant;
formFields['adresseDeclarantPays']                                = $qp204PE1.adresse.adressePersonnelle.paysAdresseDeclarant;
formFields['adresseDeclarantCodePostal']                          = $qp204PE1.adresse.adressePersonnelle.codePostalAdresseDeclarant;
formFields['coordonneesDeclarantTelephoneFixe']                   = $qp204PE1.adresse.adressePersonnelle.telephoneAdresseDeclarant;
formFields['coordonneesDeclarantTelephoneMobile']                 = $qp204PE1.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;
formFields['coordonneesDeclarantEmail']                           = $qp204PE1.adresse.adressePersonnelle.mailAdresseDeclarant;


//diplomeProfession
formFields['diplomeIntitule']                                     = $qp204PE1.diplomeProfession.diplomeProfession.intituleDiplome;
formFields['diplomePaysObtention']                                = $qp204PE1.diplomeProfession.diplomeProfession.paysObtentionDiplome;
formFields['diplomeEtablissementFormation']                       = $qp204PE1.diplomeProfession.diplomeProfession.delivreDiplome;
formFields['diplomeVillePaysFormation']                           = $qp204PE1.diplomeProfession.diplomeProfession.villeFormationDiplome;
formFields['diplomeDateObtention']                                = $qp204PE1.diplomeProfession.diplomeProfession.dateObtentionDiplome;
formFields['diplomeNBHeuresFormation']                            = $qp204PE1.diplomeProfession.diplomeProfession.nBHeuresFormation;
formFields['diplomeDateReconnaissance']                           = $qp204PE1.diplomeProfession.diplomeProfession.dateReconnaissanceDiplomeEtat;	

//diplomeDetailles

formFields['dateObtentionDiplomeMedecin']                         = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateObtentionAutresDiplome1;
formFields['paysObtentionDiplomeMedecin']                         = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.paysObtentionAutresDiplome1;
formFields['dateReconnaissanceDiplomeMedecin']                    = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateReconnaissanceDiplome1;
formFields['numAdeliDiplomeMedecin']                              = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.numReconnaissanceDiplome1;

formFields['dateObtentionDiplomeMasseur']                         = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateObtentionAutresDiplome2;
formFields['paysObtentionDiplomeMasseur']                         = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.paysObtentionAutresDiplome2;
formFields['dateReconnaissanceDiplomeMasseur']                    = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateReconnaissanceDiplome2;
formFields['numAdeliDiplomeMasseur']                              = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.numReconnaissanceDiplome2;

formFields['dateObtentionDiplomeInfirmier']                       = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateObtentionAutresDiplome3;
formFields['paysObtentionDiplomeInfirmier']                       = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.paysObtentionAutresDiplome3;
formFields['dateReconnaissanceDiplomeInfirmier']                  = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateReconnaissanceDiplome3;
formFields['numAdeliDiplomeInfirmier']                            = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.numReconnaissanceDiplome3;

formFields['dateObtentionDiplomeSageFemme']                       = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateObtentionAutresDiplome4;
formFields['paysObtentionDiplomeSageFemme']                       = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.paysObtentionAutresDiplome4;
formFields['dateReconnaissanceDiplomeSageFemme']                  = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateReconnaissanceDiplome4;
formFields['numAdeliDiplomeSageFemme']                            = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.numReconnaissanceDiplome4;

formFields['diplomeAutre']                           			  = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.intituleAutreDiplome5;
formFields['dateObtentionDiplomeAutre']                           = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateObtentionAutresDiplome5;
formFields['paysObtentionDiplomeAutre']                           = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.paysObtentionAutresDiplome5;
formFields['dateReconnaissanceDiplomeAutre']                      = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.dateReconnaissanceDiplome5;
formFields['numAdeliDiplomeAutre']                                = $qp204PE1.diplomeAutresQualification.diplomeAutresQualification1.diplomeProfessionMedical1.numReconnaissanceDiplome5;

//Autres formations complémentaire

formFields['formationComplementaire1']                           = $qp204PE1.formationComplementaire.formationComplementaire1.intituleFormationComplementaire1;
formFields['formationComplementaireDateObtention1']              = $qp204PE1.formationComplementaire.formationComplementaire1.dateFormationComplementaire1;
formFields['formationComplementairePaysObtention1']              = $qp204PE1.formationComplementaire.formationComplementaire1.paysFormationComplementaire1;
formFields['formationComplementaireNBHeures1']                   = $qp204PE1.formationComplementaire.formationComplementaire1.nbrHeuresFormationComplementaire1;

formFields['formationComplementaire2']                           = $qp204PE1.formationComplementaire.formationComplementaire1.intituleFormationComplementaire2;
formFields['formationComplementaireDateObtention2']              = $qp204PE1.formationComplementaire.formationComplementaire1.dateFormationComplementaire2;
formFields['formationComplementairePaysObtention2']              = $qp204PE1.formationComplementaire.formationComplementaire1.paysFormationComplementaire2;
formFields['formationComplementaireNBHeures2']                   = $qp204PE1.formationComplementaire.formationComplementaire1.nbrHeuresFormationComplementaire2;

formFields['formationComplementaire3']                           = $qp204PE1.formationComplementaire.formationComplementaire1.intituleFormationComplementaire3;
formFields['formationComplementaireDateObtention3']              = $qp204PE1.formationComplementaire.formationComplementaire1.dateFormationComplementaire3;
formFields['formationComplementairePaysObtention3']              = $qp204PE1.formationComplementaire.formationComplementaire1.paysFormationComplementaire3;
formFields['formationComplementaireNBHeures3']                   = $qp204PE1.formationComplementaire.formationComplementaire1.nbrHeuresFormationComplementaire3;


//exerciceProfessionnel
formFields['fonctionsExerceesNature1']                            = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger1;
formFields['fonctionsExerceesLieuPays1']                          = ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger1 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger1 + ' ' : '')
																	+ ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger1 : '');
formFields['fonctionsExerceesperiode1']                            = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger1;

formFields['fonctionsExerceesNature2']                            = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger2;
formFields['fonctionsExerceesLieuPays2']                          = ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger2 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger2 + ' ' : '')
																	+ ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger2 : '');
formFields['fonctionsExerceesperiode2']                           = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger2;

formFields['fonctionsExerceesNature3']                            = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger3;
formFields['fonctionsExerceesLieuPays3']                          = ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger3 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger3 + ' ' : '')
																	+ ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger3 : '');
formFields['fonctionsExerceesperiode3']                       	  = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger3;

formFields['fonctionsExerceesNature4']                       	  = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger4;
formFields['fonctionsExerceesLieuPays4']     					  = ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger4 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger4 + ' ' : '')
																	+ ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger4 : '');
formFields['fonctionsExerceesperiode4']                           = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger4;

formFields['fonctionsExerceesNature5']                       	  = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.natureFonctionExerceesEtranger5;
formFields['fonctionsExerceesLieuPays5']     					  = ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger5 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.lieuFonctionExerceesEtranger5 + ' ' : '')
																	+ ($qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger5 != null ? $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.paysFonctionExerceesEtranger5 : '');
formFields['fonctionsExerceesperiode5']                           = $qp204PE1.exerciceProfessionnel.exerciceProfessionnel1.periodeFonctionExerceesEtranger5;
//projetsProfessionnels
formFields['projetsProfessionnels']                               = $qp204PE1.projetsProfessionnels.projetsProfessionnels.projetsProfessionnels;
formFields['signature']                                           = $qp204PE1.signatureGroup.signature.certifieHonneur;
formFields['signatureFaitLe']                                     = $qp204PE1.signatureGroup.signature.dateSignature;
formFields['signatureFaitA']                                      = $qp204PE1.signatureGroup.signature.lieuSignature;
formFields['declarationHonneur']                                  = $qp204PE1.signatureGroup.signature.certifieHonneur1;


 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp204PE1.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Agence Régionale de Santé",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire_LE_osteopathe.pdf') //
    .apply(formFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLettreMotivation);

var finalDocItem = finalDoc.save('Formulaire_LE.pdf');


return spec.create({
    id : 'review',
   label : 'Ostéopathe - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'ostéopathe.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});