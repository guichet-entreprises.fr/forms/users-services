var formFields = {};

var civNomPrenom = $qp204PE5.info1.etatCivil.civilite + ' ' + $qp204PE5.info1.etatCivil.declarantNomNaissance + ' ' + $qp204PE5.info1.etatCivil.declarantPrenoms;

//Motif de la demande

formFields['professionConcernee']                                  = "Ostéopathe";

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;


//Identité du demandeur

formFields['declarantNomNaissance']                                = $qp204PE5.info1.etatCivil.declarantNomNaissance;
formFields['declarantPrenoms']                                     = $qp204PE5.info1.etatCivil.declarantPrenoms;
formFields['declarantNationalite']                                 = $qp204PE5.info1.etatCivil.declarantNationalite;
formFields['declarantDateNaissance']                               = $qp204PE5.info1.etatCivil.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp204PE5.info1.etatCivil.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp204PE5.info1.etatCivil.declarantPaysNaissance;


//Adresse

formFields['declarantAdressePersoCourriel']                        = $qp204PE5.info2.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoieComplement']            = ($qp204PE5.info2.coordonnees1.declarantAdressePersoNumNomVoie != null ? $qp204PE5.info2.coordonnees1.declarantAdressePersoNumNomVoie: '') + ' ' + ($qp204PE5.info2.coordonnees1.declarantAdressePersoComplement != null ? $qp204PE5.info2.coordonnees1.declarantAdressePersoComplement: '');
//formFields['declarantAdressePersoCP']                            = $qp204PE5.info2.coordonnees1.declarantAdressePersoCP;
//formFields['declarantAdressePersoVille']                         = $qp204PE5.info2.coordonnees1.declarantAdressePersoVille;
//formFields['declarantAdressePersoPays']                          = $qp204PE5.info2.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoCPVillePays']                     = ($qp204PE5.info2.coordonnees1.declarantAdressePersoCP != null ? $qp204PE5.info2.coordonnees1.declarantAdressePersoCP: '') + ' ' + ($qp204PE5.info2.coordonnees1.declarantAdressePersoVille != null ? $qp204PE5.info2.coordonnees1.declarantAdressePersoVille: '') +' '+ ($qp204PE5.info2.coordonnees1.declarantAdressePersoPays != null ? $qp204PE5.info2.coordonnees1.declarantAdressePersoPays: '');
formFields['declarantAdressePersoTelephone']                       = $qp204PE5.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp204PE5.info2.coordonnees1.declarantAdressePersoCourriel;

//Adresse France 

formFields['declarantAdresseFranceNumNomVoieComplement']           = ($qp204PE5.info2.coordonnees2.declarantAdresseFranceNumNomVoie != null ? $qp204PE5.info2.coordonnees2.declarantAdresseFranceNumNomVoie: '') + ' ' + ($qp204PE5.info2.coordonnees2.declarantAdresseFranceComplement != null ? $qp204PE5.info2.coordonnees2.declarantAdresseFranceComplement: '');
//formFields['declarantAdresseFranceCP']                           = $qp204PE5.info2.coordonnees2.declarantAdresseFranceCP;
//formFields['declarantAdresseFranceVille']                        = $qp204PE5.info2.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFranceCPVillePays']                    = ($qp204PE5.info2.coordonnees2.declarantAdresseFranceCP != null ? $qp204PE5.info2.coordonnees2.declarantAdresseFranceCP: '') + ' ' + ($qp204PE5.info2.coordonnees2.declarantAdresseFranceVille != null ? $qp204PE5.info2.coordonnees2.declarantAdresseFranceVille: '');
formFields['declarantAdresseFranceTelephone']                      = $qp204PE5.info2.coordonnees2.declarantAdresseFranceTelephone;
formFields['declarantAdresseFranceCourriel']                       = $qp204PE5.info2.coordonnees2.declarantAdresseFranceCourriel;

//Profession concernée

formFields['professionExercee']                                    = $qp204PE5.info3.profession.professionExercee;
formFields['specialite']                                           = $qp204PE5.info3.profession.specialite;
formFields['professionDemandee']                                   = $qp204PE5.info3.profession.professionDemandee;
formFields['specialiteDemandee']                                   = $qp204PE5.info3.profession.specialiteDemandee;
formFields['actesEnvisages']                                       = $qp204PE5.info3.profession.actesEnvisages;
formFields['lieupremiereLPS']                                      = $qp204PE5.info3.profession.lieupremiereLPS;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp204PE5.info4.ordre.ouiNon).eq('ordreNon') ? true : false;
formFields['ordreOui']                                             = Value('id').of($qp204PE5.info4.ordre.ouiNon).eq('ordreOui') ? true : false;

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp204PE5.info4.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp204PE5.info4.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp204PE5.info4.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp204PE5.info4.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp204PE5.info4.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp204PE5.info4.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp204PE5.info4.ordre.ordreProfessionnelAdresseVille != null ? $qp204PE5.info4.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp204PE5.info4.ordre.ordreProfessionnelAdressePays != null ? $qp204PE5.info4.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = ($qp204PE5.info4.ordre.ordreProfessionnelNom != null ? $qp204PE5.info4.ordre.ordreProfessionnelNom : '') + ' ' 
																	+ ($qp204PE5.info4.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp204PE5.info4.ordre.ordreProfessionnelNumEnregistrementNom: '');


//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp204PE5.info5.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp204PE5.info5.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp204PE5.info5.assuranceProfessionnelle.commentaires != null ? $qp204PE5.info5.assuranceProfessionnelle.commentaires :'';

//Signature&Observations

formFields['autresObservations']                                   = $qp204PE5.info6.observations.autresObservations != null ? $qp204PE5.info6.observations.autresObservations :'';
formFields['dateSignature']                                        = $qp204PE5.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp204PE5.info7.signature.attesteSignature;


// formFields['prestation1Du']                     				   = '';
// formFields['prestation1Au']                                        = '';
// formFields['prestation2Du']                                        = '';
// formFields['prestation2Au']                                        = '';
// formFields['prestation3Du']                                        = '';
// formFields['prestation3Au']                                        = '';
// formFields['prestation4Du']                                        = '';
// formFields['prestation4Au']                                        = '';
// formFields['prestation5Du']                                        = '';
// formFields['prestation5Au']                                        = '';
// formFields['prestation6Du']                                        = '';
// formFields['prestation6Au']                                        = '';


// formFields['prestation1ActiviteExercee']                           = '';
// formFields['prestation2ActiviteExercee']                           = '';
// formFields['prestation3ActiviteExercee']                           = '';
// formFields['prestation4ActiviteExercee']                           = '';
// formFields['prestation5ActiviteExercee']                           = '';
// formFields['prestation6ActiviteExercee']                           = '';


formFields['libelleProfession']				                       = "Ostéopathe"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp204PE5.info7.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp204PE5.info7.signature.dateSignature,
		autoriteHabilitee :"Agence Régionale de Santé ",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire_sante_LPS.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjLettreMotivation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Formulaire_sante_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ostéopathe - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'ostéopathe.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
