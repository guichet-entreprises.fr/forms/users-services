var cerfaFields = {};

var civNomPrenom = $qp013PE4.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp013PE4.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp013PE4.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['aideSoignant']                             = true;
cerfaFields['auxiliairePuericulture']                   = false;
cerfaFields['ambulancier']                              = false;
cerfaFields['lpsDemandeInitiale']                       = true;
cerfaFields['lpsRenouvellement']                        = false;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp013PE4.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp013PE4.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp013PE4.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp013PE4.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp013PE4.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp013PE4.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp013PE4.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp013PE4.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp013PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp013PE4.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') 
														+ ' ' + ($qp013PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? ', ' + $qp013PE4.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp013PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp013PE4.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') 
														+ ' ' + $qp013PE4.coordonnees.coordonneesDeclarant.declarantAdresseVille + ', ' + $qp013PE4.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp013PE4.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp013PE4.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance : '') 
														  + ' ' + ($qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal : '') 
														  + ' ' + ($qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp013PE4.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionConcernee']                      = $qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee;
cerfaFields['typesActesEnvisages']                      = $qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;

cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceComplementAdresse: '');

cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceVille != null ? $qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExercicePays != null ? $qp013PE4.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseNumeroLibellee != null ? $qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseNumeroLibellee : '') 
														+ ' ' + ($qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseComplementAdresse != null ? $qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']       = ($qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseCodePostal != null ? $qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseCodePostal :'') 
														+' '+ ($qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseVille != null ? $qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseVille : '') + ', ' + ($qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdressePays != null ? $qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp013PE4.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp013PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp013PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp013PE4.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale;

//informations à fournir en cas de renouvellement 
cerfaFields['datePrestationDebut1']                     = '';
cerfaFields['datePrestationFin1']                       = '';
cerfaFields['datePrestationDebut2']                     = '';
cerfaFields['datePrestationFin2']                       = '';
cerfaFields['datePrestationDebut3']                     = '';
cerfaFields['datePrestationFin3']                       = '';
cerfaFields['datePrestationDebut4']                     = '';
cerfaFields['datePrestationFin4']                       = '';
cerfaFields['datePrestationDebut5']                     = '';
cerfaFields['datePrestationFin5']                       = '';

cerfaFields['commentairesLPSRenouvellement']            = '';
cerfaFields['activiteProfessionnelsLPSRevouvellement']  = '';


// Observations et Signature
cerfaFields['autresObservations']                       = $qp013PE4.observations.observationsSignature.autresObservations;
cerfaFields['signatureCoche']                           = $qp013PE4.observations.observationsSignature.signatureCoche;
cerfaFields['signatureDate']                            = $qp013PE4.observations.observationsSignature.signatureDate;
cerfaFields['signature']                                ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'
cerfaFields['libelleProfession']						= "Aide-soignant"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp013PE4.observations.observationsSignature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier_ premier_dossier_v2.1 _GQ.pdf') //
	.apply({
		date: $qp013PE4.observations.observationsSignature.signatureDate,
		autoriteHabilitee : "Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/aide_soignant.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('aide_soignant_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Aide-soignant - déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'aide-soignant.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});