var cerfaFields = {};

var civNomPrenom = $qp013PE5.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp013PE5.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp013PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['aideSoignant']                             = true;
cerfaFields['auxiliairePuericulture']                   = false;
cerfaFields['ambulancier']                              = false;
cerfaFields['lpsDemandeInitiale']                       = false;
cerfaFields['lpsRenouvellement']                        = true;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp013PE5.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp013PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp013PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp013PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp013PE5.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp013PE5.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp013PE5.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp013PE5.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp013PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp013PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') + ' ' + ($qp013PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? ', ' + $qp013PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp013PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp013PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' + $qp013PE5.coordonnees.coordonneesDeclarant.declarantAdresseVille + ', ' + $qp013PE5.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp013PE5.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp013PE5.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance : '') 
														  + ' ' + ($qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal : '') 
														  + ' ' + ($qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp013PE5.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionConcernee']                      = $qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee;
cerfaFields['typesActesEnvisages']                      = $qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;
cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceComplementAdresse: '');

cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceVille != null ? $qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExercicePays != null ? $qp013PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.lieuPremierePrestation.lieuExercicePays : '');

// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseNumeroLibellee != null ? $qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseNumeroLibellee : '') + ' ' + ($qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseComplementAdresse != null ? $qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']           = ($qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseCodePostal != null ? $qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseCodePostal :'') +' '+ ($qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseVille != null ? $qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdresseVille + ', ' : '') + ($qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdressePays != null ? $qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp013PE5.ordre.ordreProfessionnel.ordreProfessionnelInformations.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp013PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp013PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp013PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale;


//informations à fournir en cas de renouvellement 
cerfaFields['datePrestationDebut1']                     = $qp013PE5.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['datePrestationFin1']                       = $qp013PE5.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['datePrestationDebut2']                     = ($qp013PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp013PE5.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['datePrestationFin2']                       = ($qp013PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp013PE5.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['datePrestationDebut3']                     = ($qp013PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp013PE5.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['datePrestationFin3']                       = ($qp013PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp013PE5.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['datePrestationDebut4']                     = ($qp013PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp013PE5.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['datePrestationFin4']                       = ($qp013PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp013PE5.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['datePrestationDebut5']                     = ($qp013PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp013PE5.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['datePrestationFin5']                       = ($qp013PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp013PE5.detailsRenouvellement.renouvellement.periodePreste5.to : '');

cerfaFields['commentairesLPSRenouvellement']            = $qp013PE5.detailsRenouvellement.renouvellement.commentairesRenouvellement;
cerfaFields['activiteProfessionnelsLPSRevouvellement']  = $qp013PE5.detailsRenouvellement.renouvellement.activitesProfessionnelles;

// Observations et Signature
cerfaFields['autresObservations']                       = $qp013PE5.observations.observationsSignature.autresObservations;
cerfaFields['signatureCoche']                           = $qp013PE5.observations.observationsSignature.signatureCoche;
cerfaFields['signatureDate']                            = $qp013PE5.observations.observationsSignature.signatureDate;
cerfaFields['signature']                                ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

cerfaFields['libelleProfession']						= "Aide-soignant"
/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp013PE5.observations.observationsSignature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier_ premier_dossier_v2.1 _GQ.pdf') //
	.apply({
		date: $qp013PE5.observations.observationsSignature.signatureDate,
		autoriteHabilitee :"Direction régionale de la jeunesse, des sports et de la cohésion sociale (DRJSCS)",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/aide_soignant.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurancePro);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieDeclaration);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('aide_soignant_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Aide-soignant - renouvellement de la déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'aide-soignant.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});