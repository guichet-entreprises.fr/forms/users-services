function pad(s) { return (s < 10) ? '0' + s : s; } 
var formFields = {};

//état Civil
var cheminEtatCivil = $qp153PE6.etatCivilPage.etatCivilGroup;

formFields['civiliteMonsieur']                = (cheminEtatCivil.civilite=='Monsieur');
formFields['civiliteMadame']                  = (cheminEtatCivil.civilite=='Madame');
formFields['nomNaissance']                    = cheminEtatCivil.nomNaissance;
formFields['nomUsage']                        = cheminEtatCivil.nomUsage;
formFields['prenoms']                         = cheminEtatCivil.prenoms;
formFields['nomPrenomExercice']               = cheminEtatCivil.nomPrenomExercice;
if(cheminEtatCivil.dateNaissance != null) {
	var dateTemp = new Date(parseInt (cheminEtatCivil.dateNaissance.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourNaissance'] = day;
	    	formFields['moisNaissance'] = month;
	    	formFields['anneeNaissance'] = year;
}
formFields['lieuNaissance']                   = cheminEtatCivil.lieuNaissance;
formFields['deptNaissance']                   = cheminEtatCivil.deptNaissance;
formFields['paysNaissance']                   = cheminEtatCivil.paysNaissance;
formFields['nationalite']                     = cheminEtatCivil.nationalite;

if(cheminEtatCivil.dateAcquisitionNationaliteFr != null) {
	var dateTemp = new Date(parseInt (cheminEtatCivil.dateAcquisitionNationaliteFr.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourOtentionNationalite'] = day;
	    	formFields['moisOtentionNationalite'] = month;
	    	formFields['anneeOtentionNationalite'] = year;
}

var cheminEtranger = $qp153PE6.etatCivilPage.etatCivilGroup.etrangerGroup;

formFields['nomPereEtranger']                 = cheminEtranger.nomPereEtranger;
formFields['prenomPereEtranger']              = cheminEtranger.prenomPereEtranger;
formFields['nomMereEtranger']                 = cheminEtranger.nomMereEtranger;
formFields['prenomMereEtranger']              = cheminEtranger.prenomMereEtranger;	


//Coordonnées
var cheminCoordonnees = $qp153PE6.coordonnesPage.coordonnesGroup;

formFields['numAppartBP']                     = cheminCoordonnees.numAppartBP;
formFields['entreeBat']                       = cheminCoordonnees.entreeBat;
formFields['adresseNumeroLibellee']           = cheminCoordonnees.adresseNumeroLibellee;
formFields['bpLieuDit']                       = cheminCoordonnees.bpLieuDit;
formFields['codePostal']                      = cheminCoordonnees.codePostal;
formFields['ville']                           = cheminCoordonnees.ville;
formFields['pays']                            = cheminCoordonnees.pays;
formFields['numPhone']                        = cheminCoordonnees.numPhone;
formFields['numFixe']                         = cheminCoordonnees.numFixe;
formFields['courriel']                        = cheminCoordonnees.courriel;
formFields['numAdeli']                        = cheminCoordonnees.numAdeli;


//Diplôme
var cheminDiplome = $qp153PE6.diplomePage.diplomeGroup;

formFields['IntituleDiplome']                 = cheminDiplome.intituleDiplome;

if(cheminDiplome.dateObtentionDiplome != null) {
	var dateTemp = new Date(parseInt (cheminDiplome.dateObtentionDiplome.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourOtentionDiplome'] = day;
	    	formFields['moisOtentionDiplome'] = month;
	    	formFields['anneeOtentionDiplome'] = year;
}

formFields['numDiplome']                      = cheminDiplome.numDiplome;
formFields['lieuObtentionDiplome']            = cheminDiplome.lieuObtentionDiplome;
formFields['paysDelivranceDiplome']           = cheminDiplome.paysDelivranceDiplome;


//AUTORISATION D’EXERCICE DÉLIVRÉE PAR LE PRÉFET DE RÉGION
var cheminAutorisation = $qp153PE6.diplomePage.autorisationExercice;

if(cheminAutorisation.dateDelivranceAutorisation != null) {
	var dateTemp = new Date(parseInt (cheminAutorisation.dateDelivranceAutorisation.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourDelivranceAutorisation'] = day;
	    	formFields['moisDelivranceAutorisation'] = month;
	    	formFields['anneeDelivranceAutorisation'] = year;
}

formFields['prefetDelivranceAutorisation']    = cheminAutorisation.prefetDelivranceAutorisation;

//Autres diplômes

var cheminAutreDiplome = $qp153PE6.diplomePage;

var tailleTabAutreDiplome = 2;
for (var i = 0 ; i <tailleTabAutreDiplome; i++){
	formFields['intituleAutreDiplome' + i] 			= '';
	
	formFields['deAutreDiplome' + i]                = '';
	formFields['specialiteAutreDiplome' + i]        = '';
	formFields['specialiteAutreDiplome' + i]        = '';
	formFields['capacitAutreDiplome' + i]           = '';
	
	
	if($qp153PE6.diplomePage.autreDiplomeGroup[i].dateObtentionAutreDiplome != null) {
		var dateTemp = new Date(parseInt($qp153PE6.diplomePage.autreDiplomeGroup[i].dateObtentionAutreDiplome.getTimeInMillis()));
			var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
		formFields['jourObtentionAutreDiplome'+ i] 	= '';
		formFields['moisObtentionAutreDiplome'+ i]	= '';
		formFields['anneeObtentionAutreDiplome'+ i]	= '';
	}
	formFields['organismeFormateur' + i] 			= '';
	formFields['codePostalAutreDiplome' + i] 		= '';
	formFields['villeAutreDiplome' + i] 			= '';
	
	if($qp153PE6.diplomePage.autreDiplomeGroup[i].dateAbandonExercice != null) {
		var dateTemp = new Date(parseInt($qp153PE6.diplomePage.autreDiplomeGroup[i].dateAbandonExercice.getTimeInMillis()));
			var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
		formFields['jourAbondantExercice'+ i] 	= '';
		formFields['moisAbondantExercice'+ i]	= '';
		formFields['anneeAbondantExercice'+ i]	= '';
	}
}

for (var i = 0 ; i <= cheminAutreDiplome.autreDiplomeGroup[i].size(); i++){
	formFields['intituleAutreDiplome' + i] 			= cheminAutreDiplome.autreDiplomeGroup[i].intituleAutreDiplome;
	
	formFields['deAutreDiplome' + i]                = Value('id').of(cheminAutreDiplome.autreDiplomeGroup[i].typeDiplome).eq('deAutreDiplome') ? true : false;
	formFields['specialiteAutreDiplome' + i]        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup[i].typeDiplome).eq('specialiteAutreDiplome') ? true : false;
	formFields['specialiteAutreDiplome' + i]        = Value('id').of(cheminAutreDiplome.autreDiplomeGroup[i].typeDiplome).eq('competenceAutreDiplome') ? true : false;
	formFields['capacitAutreDiplome' + i]           = Value('id').of(cheminAutreDiplome.autreDiplomeGroup[i].typeDiplome).eq('capacitAutreDiplome') ? true : false;
	
	
	if($qp153PE6.diplomePage.autreDiplomeGroup[i].dateObtentionAutreDiplome != null) {
		var dateTemp = new Date(parseInt($qp153PE6.diplomePage.autreDiplomeGroup[i].dateObtentionAutreDiplome.getTimeInMillis()));
			var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
		formFields['jourObtentionAutreDiplome'+ i] 	= day;
		formFields['moisObtentionAutreDiplome'+ i]	= month;
		formFields['anneeObtentionAutreDiplome'+ i]	= year;
	}
	formFields['organismeFormateur' + i] 			= cheminAutreDiplome.autreDiplomeGroup[i].organismeFormateur;
	formFields['codePostalAutreDiplome' + i] 		= cheminAutreDiplome.autreDiplomeGroup[i].codePostalAutreDiplome;
	formFields['villeAutreDiplome' + i] 			= cheminAutreDiplome.autreDiplomeGroup[i].villeAutreDiplome;
	
	if($qp153PE6.diplomePage.autreDiplomeGroup[i].dateAbandonExercice != null) {
		var dateTemp = new Date(parseInt($qp153PE6.diplomePage.autreDiplomeGroup[i].dateAbandonExercice.getTimeInMillis()));
			var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
		formFields['jourAbondantExercice'+ i] 	= day;
		formFields['moisAbondantExercice'+ i]	= month;
		formFields['anneeAbondantExercice'+ i]	= year;
	}
}


//Langues étrangères
var cheminLangueEtrangere = $qp153PE6.diplomePage.langueEtrangere;
							

var langueEtrangere = 3;

for (var i = 0 ; i<langueEtrangere; i++){
	formFields['langueEtrangere'+i]                     = '';
}

for (var i = 0 ; i<= $qp153PE6.diplomePage.languesEtrangeres.langueEtrangere[i].size(); i++ ){
	formFields['langueEtrangere'+i]                		= $qp153PE6.diplomePage.languesEtrangeres.langueEtrangere[i];
}


//Exercices
var cheminExercice = $qp153PE6.exerciceProPage.exerciceGroup;

formFields['exerciceTempsPleinOUI']           = cheminExercice.exerciceTempsPartielOuiNon ? false : true;
formFields['exerciceTempsPleinNon']           = cheminExercice.exerciceTempsPartielOuiNon ? true: false;
formFields['pourcentageTempsExercice']        = cheminExercice.pourcentageTempsExercice;	

if(cheminExercice.dateDerniereEmbauche != null) {
	var dateTemp = new Date(parseInt (cheminExercice.dateDerniereEmbauche.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourDerniereEmbauche'] = day;
	    	formFields['moisDerniereEmbauche'] = month;
	    	formFields['anneeDerniereEmbauche'] = year;
}
formFields['fonction']                        = cheminExercice.fonction;
formFields['raisonSocialeEmployeur']          = cheminExercice.raisonSocialeEmployeur;
formFields['numFiness']                       = cheminExercice.numFiness;
formFields['numSiret']                        = cheminExercice.numSiret;


//AdresseActivité
var cheminAdresseActivite = $qp153PE6.exerciceProPage.exerciceGroup.adresseActivite;

formFields['nomEtablissementActivite']        = cheminAdresseActivite.nomEtablissementActivite;
formFields['nomServiceExercice']              = cheminAdresseActivite.nomServiceExercice;
formFields['adresseExercice']                 = cheminAdresseActivite.adresseExercice;
formFields['bpLieuDitExercice']               = cheminAdresseActivite.bpLieuDitExercice;
formFields['codePostalExercice']              = cheminAdresseActivite.codePostalExercice;
formFields['villeExercice']                   = cheminAdresseActivite.villeExercice;
formFields['telephoneExercice']               = cheminAdresseActivite.telephoneExercice;
formFields['posteExercice']                   = cheminAdresseActivite.posteExercice;

var cheminStatut = $qp153PE6.exerciceProPage.exerciceGroup;

formFields['titulaire']                       = Value('id').of(cheminStatut.statut).eq('titulaire') ? true : false;
formFields['stagiaire']                		  = Value('id').of(cheminStatut.statut).eq('stagiaire') ? true : false;
formFields['contractuel']                	  = Value('id').of(cheminStatut.statut).eq('contractuel') ? true : false;

var cheminSecteurPrive = $qp153PE6.exerciceProPage.exerciceGroup;

formFields['cdiPrive']                	  	  = Value('id').of(cheminSecteurPrive.secteurPrive).eq('cdiPrive') ? true : false;
formFields['cddPrive']                	  	  = Value('id').of(cheminSecteurPrive.secteurPrive).eq('cddPrive') ? true : false;
formFields['vacatairePrive']                  = Value('id').of(cheminSecteurPrive.secteurPrive).eq('vacatairePrive') ? true : false;
formFields['interimairePrive']                = Value('id').of(cheminSecteurPrive.secteurPrive).eq('interimairePrive') ? true : false;

var cheminAutreSituation = $qp153PE6.exerciceProPage.exerciceGroup;

formFields['benevole']                	 	  = Value('id').of(cheminAutreSituation.autreSituation).eq('benevole') ? true : false;
formFields['reserveSanitaire']                = Value('id').of(cheminAutreSituation.autreSituation).eq('reserveSanitaire') ? true : false;
formFields['retraiteSansActivite']            = Value('id').of(cheminAutreSituation.autreSituation).eq('retraiteSansActivite') ? true : false;

formFields['declarationSurHonneur']			  ="Je déclare sur l’honneur qu’aucune instance pouvant donner lieu à condamnation ou sanction susceptible d’avoir des conséquences sur mon inscription au tableau de l’Ordre n’est en cours à mon encontre"
formFields['codeDeontologie']                 = "J’affirme avoir pris connaissance du code de déontologie des infirmiers et fais le serment de le respecter";


//Signature
var cheminSignature = $qp153PE6.signatureGroup.signature;
var civiliteNomPrenom = $qp153PE6.etatCivilPage.etatCivilGroup.civilite +" "+ $qp153PE6.etatCivilPage.etatCivilGroup.nomNaissance +" "+ $qp153PE6.etatCivilPage.etatCivilGroup.prenoms;

formFields['deptExercicePrincipal']           = cheminSignature.deptExercicePrincipal;

formFields['annuaireInfermier'] 			  = cheminSignature.annuaireInfirmier;
formFields['declarationSurHonneurSig']        = cheminSignature.declarationSurHonneur;
formFields['certifieHonneur']                 = cheminSignature.certifieHonneur;
formFields['lieuSignature']                   = cheminSignature.lieuSignature;

if(cheminSignature.dateSignature != null) {
	var dateTemp = new Date(parseInt (cheminSignature.dateSignature.getTimeInMillis()));
	   	var monthTemp = dateTemp.getMonth() + 1;
	    	var month = pad(monthTemp.toString());
	   	var day =  pad(dateTemp.getDate().toString());
	    	var year = dateTemp.getFullYear();
	    	formFields['jourSignature'] = day;
	    	formFields['moisSignature'] = month;
	    	formFields['anneeSignature'] = year;
}
formFields['signature']                       = civiliteNomPrenom;



var cerfaDoc = nash.doc //
.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
.apply({
	dateSignature : $qp153PE6.signatureGroup.signature.dateSignature ,
	autoriteHabilitee :"Conseil national de l’ordre des infirmiers" ,
	demandeContexte : "Inscription à l’ordre reconnaissance automatique",
	civiliteNomPrenom : civiliteNomPrenom
});


var finalDoc = nash.doc //
.load('models/model.pdf') //
.apply (formFields);


cerfaDoc.append(finalDoc.save('cerfa.pdf'));	

/*******************************************************************************
 * Pieces jointes
 ******************************************************************************/

function appendPj(fld) {
    fld.forEach(function (elm) {
    	cerfaDoc.append(elm);
    });
}

appendPj($attachmentPreprocess.attachmentPreprocess.pdID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationHonneur);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatRadiation);


/*******************************************************************************
 * Chargement du courrier d'accompagnement à destination de l'AC
 ******************************************************************************/



/*******************************************************************************
 *  Enregistrement du fichier (en mémoire)
 ******************************************************************************/

var finalDocItem = cerfaDoc.save('Infirmier.pdf');	

/*******************************************************************************
 *  Persistance des données obtenues
 ******************************************************************************/

return spec.create({
id : 'review',
label : 'Infirmier  - Inscription à l’ordre reconnaissance automatique',
groups : [ spec.createGroup({
    id : 'generated',
    label : 'Génération du dossier',
    data : [ spec.createData({
        id : 'formulaire',
        label : "Demande d'inscription à l'ordre national pour la profession d'infirmier en vue d'un libre établissement en tant que salarié grâce à la reconnaissance automatique de diplôme.",
        description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
        type : 'FileReadOnly',
        value : [ finalDocItem ]
    }) ]
}) ]
});
