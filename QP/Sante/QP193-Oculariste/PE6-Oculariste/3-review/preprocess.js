var formFields = {};

var civNomPrenom = $qp193PE6.info1.etatCivil.civilite + ' ' + $qp193PE6.info1.etatCivil.declarantNomNaissance + ' ' + $qp193PE6.info1.etatCivil.declarantPrenoms;

//Motif de la demande

formFields['professionConcernee']                                  = "Oculariste";

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;


//Identité du demandeur

formFields['declarantNomNaissance']                                = $qp193PE6.info1.etatCivil.declarantNomNaissance;
formFields['declarantPrenoms']                                     = $qp193PE6.info1.etatCivil.declarantPrenoms;
formFields['declarantNationalite']                                 = $qp193PE6.info1.etatCivil.declarantNationalite;
formFields['declarantDateNaissance']                               = $qp193PE6.info1.etatCivil.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp193PE6.info1.etatCivil.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp193PE6.info1.etatCivil.declarantPaysNaissance;

formFields['declarantAdressePersoNumNomVoieComplement']            = ($qp193PE6.info2.coordonnees1.declarantAdressePersoNumNomVoie != null ? $qp193PE6.info2.coordonnees1.declarantAdressePersoNumNomVoie: '') + ' ' + ($qp193PE6.info2.coordonnees1.declarantAdressePersoComplement != null ? $qp193PE6.info2.coordonnees1.declarantAdressePersoComplement: '');
formFields['declarantAdressePersoCPVillePays']                     = ($qp193PE6.info2.coordonnees1.declarantAdressePersoCP != null ? $qp193PE6.info2.coordonnees1.declarantAdressePersoCP: '') + ' ' + ($qp193PE6.info2.coordonnees1.declarantAdressePersoVille != null ? $qp193PE6.info2.coordonnees1.declarantAdressePersoVille: '')+ ' ' + ($qp193PE6.info2.coordonnees1.declarantAdressePersoPays != null ? $qp193PE6.info2.coordonnees1.declarantAdressePersoPays: '');
formFields['declarantAdressePersoTelephone']                       = $qp193PE6.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp193PE6.info2.coordonnees1.declarantAdressePersoCourriel;

formFields['declarantAdresseFranceNumNomVoieComplement']           = ($qp193PE6.info2.coordonnees2.declarantAdresseFranceNumNomVoie != null ? $qp193PE6.info2.coordonnees2.declarantAdresseFranceNumNomVoie: '') + ' ' + ($qp193PE6.info2.coordonnees2.declarantAdresseFranceComplement != null ? $qp193PE6.info2.coordonnees2.declarantAdresseFranceComplement: '');
formFields['declarantAdresseFranceCPVillePays']                    = ($qp193PE6.info2.coordonnees2.declarantAdresseFranceCP != null ? $qp193PE6.info2.coordonnees2.declarantAdresseFranceCP: '') + ' ' + ($qp193PE6.info2.coordonnees2.declarantAdresseFranceVille != null ? $qp193PE6.info2.coordonnees2.declarantAdresseFranceVille: '');
formFields['declarantAdresseFranceTelephone']                      = $qp193PE6.info2.coordonnees2.declarantAdresseFranceTelephone;
formFields['declarantAdresseFranceCourriel']                       = $qp193PE6.info2.coordonnees2.declarantAdresseFranceCourriel;

//Profession concernée

formFields['professionExercee']                                    = $qp193PE6.info3.profession.professionExercee;
formFields['specialite']                                           = $qp193PE6.info3.profession.specialite != null ? $qp193PE6.info3.profession.specialite : '' ;
formFields['professionDemandee']                                   = $qp193PE6.info3.profession.professionDemandee;
formFields['specialiteDemandee']                                   = $qp193PE6.info3.profession.specialiteDemandee != null ? $qp193PE6.info3.profession.specialiteDemandee : '' ;
formFields['actesEnvisages']                                       = $qp193PE6.info3.profession.actesEnvisages != null ? $qp193PE6.info3.profession.actesEnvisages: '' ;
formFields['lieupremiereLPS']                                      = $qp193PE6.info3.profession.lieupremiereLPS != null ? $qp193PE6.info3.profession.lieupremiereLPS : '' ;


//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp193PE6.info4.ordre.ouiNon).eq('ordreNon') ? true : false;
formFields['ordreOui']                                             = Value('id').of($qp193PE6.info4.ordre.ouiNon).eq('ordreOui') ? true : false;

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp193PE6.info4.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp193PE6.info4.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp193PE6.info4.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp193PE6.info4.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp193PE6.info4.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp193PE6.info4.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp193PE6.info4.ordre.ordreProfessionnelAdresseVille != null ? $qp193PE6.info4.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp193PE6.info4.ordre.ordreProfessionnelAdressePays != null ? $qp193PE6.info4.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp193PE6.info4.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp193PE6.info4.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp193PE6.info5.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp193PE6.info5.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp193PE6.info5.assuranceProfessionnelle.commentaires != null ? $qp193PE6.info5.assuranceProfessionnelle.commentaires:''  ;

//Signature&Observations

formFields['autresObservations']                                   = $qp193PE6.info6.observations.autresObservations != null ? $qp193PE6.info6.observations.autresObservations :'' ;
formFields['dateSignature']                                        = $qp193PE6.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp193PE6.info7.signature.attesteSignature;


// formFields['prestation1Du']                     				   = '';
// formFields['prestation1Au']                                        = '';
// formFields['prestation2Du']                                        = '';
// formFields['prestation2Au']                                        = '';
// formFields['prestation3Du']                                        = '';
// formFields['prestation3Au']                                        = '';
// formFields['prestation4Du']                                        = '';
// formFields['prestation4Au']                                        = '';
// formFields['prestation5Du']                                        = '';
// formFields['prestation5Au']                                        = '';
// formFields['prestation6Du']                                        = '';
// formFields['prestation6Au']                                        = '';


// formFields['prestation1ActiviteExercee']                           = '';
// formFields['prestation2ActiviteExercee']                           = '';
// formFields['prestation3ActiviteExercee']                           = '';
// formFields['prestation4ActiviteExercee']                           = '';
// formFields['prestation5ActiviteExercee']                           = '';
// formFields['prestation6ActiviteExercee']                           = '';


formFields['libelleProfession']				                       = "Oculariste"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp193PE6.info7.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp193PE6.info7.signature.dateSignature,
		autoriteHabilitee :"Ministère des Solidarités et de la Santé",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire_sante_LPS.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEquivalentDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.pjReconnaissanceFormation);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Oculariste_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Oculariste - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'oculariste.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
