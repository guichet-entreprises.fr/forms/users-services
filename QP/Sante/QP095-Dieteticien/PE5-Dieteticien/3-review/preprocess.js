var cerfaFields = {};

var civNomPrenom = $qp095PE5.identiteDemandeur1.identiteDemandeur.sexe + ' ' + $qp095PE5.identiteDemandeur1.identiteDemandeur.declarantNom + ' ' + $qp095PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;

cerfaFields['audioprothesiste']                          = false;
cerfaFields['conseillerGenetique']                       = false;
cerfaFields['preparateurPharmacie']                      = false;
cerfaFields['preparateurPharmacieHospitaliere']          = false;
cerfaFields['infirmier']                       			 = false;
cerfaFields['infirmierSpecialise']                       = false;
cerfaFields['infirmierSpecialiseAnesthesiste']           = false;
cerfaFields['infirmierSpecialiseBlocOperatoire']         = false;
cerfaFields['infirmierSpecialisePuericultrice']          = false;
cerfaFields['pedicurePodologue']                       	 = false;
cerfaFields['ergotherapeuthe']                       	 = false;
cerfaFields['psychomotricien']                       	 = false;
cerfaFields['orthophoniste']                       	     = false;
cerfaFields['orthoptiste']                       		 = false;
cerfaFields['manipulateurElectroradiologie']             = false;
cerfaFields['opticienLunetier']                          = false;
cerfaFields['dieteticien']                       		 = true;

cerfaFields['declarationInitiale']                       = false;
cerfaFields['renouvellement']                       	 = true;

//Identité du demandeur

cerfaFields['declarantNom']                             = $qp095PE5.identiteDemandeur1.identiteDemandeur.declarantNom;
cerfaFields['declarantPrenoms']                         = $qp095PE5.identiteDemandeur1.identiteDemandeur.declarantPrenoms;
cerfaFields['civiliteMonsieur']                         = Value('id').of($qp095PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMonsieur') ? true : false;
cerfaFields['civiliteMadame']                           = Value('id').of($qp095PE5.identiteDemandeur1.identiteDemandeur.sexe).eq('civiliteMadame') ? true : false;
cerfaFields['declarantDateNaissance']                   = $qp095PE5.identiteDemandeur1.identiteDemandeur.declarantDateNaissance;
cerfaFields['declarantLieuNaissance']                   = $qp095PE5.identiteDemandeur1.identiteDemandeur.declarantLieuNaissance;
cerfaFields['declarantPaysNaissance']                   = $qp095PE5.identiteDemandeur1.identiteDemandeur.declarantPaysNaissance;
cerfaFields['declarantNationalite'] 					= $qp095PE5.identiteDemandeur1.identiteDemandeur.declarantNationalite;

// Coordonnées 

cerfaFields['declarantAdresseRueComplement']            = ($qp095PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle != null ? $qp095PE5.coordonnees.coordonneesDeclarant.declarantAdresseNumeroLibelle : '') + ' ' + ($qp095PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse != null ? $qp095PE5.coordonnees.coordonneesDeclarant.declarantAdresseComplementAdresse : '');
cerfaFields['declarantAdresseVillePays']                = ($qp095PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal != null ? $qp095PE5.coordonnees.coordonneesDeclarant.declarantAdresseCodePostal : '') + ' ' + $qp095PE5.coordonnees.coordonneesDeclarant.declarantAdresseVille + ' ' + $qp095PE5.coordonnees.coordonneesDeclarant.declarantAdressePays;
cerfaFields['declarantTelephone']                       = $qp095PE5.coordonnees.coordonneesDeclarant.declarantTelephone;
cerfaFields['declarantcourriel']                        = $qp095PE5.coordonnees.coordonneesDeclarant.declarantcourriel;
cerfaFields['declarantAdresseFranceRueComplement']      = ($qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance != null ? $qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFrance: '') + ' ' 
														+ ($qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse != null ? $qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceComplementAdresse : '');
cerfaFields['declarantAdresseFranceVille']              = ($qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal != null ? $qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceCodePostal: '') + ' ' 
														+ ($qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille!= null ? $qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantAdresseFranceVille : '');
cerfaFields['declarantTelephoneFrance']                 = $qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantTelephoneFrance;
cerfaFields['declarantCourrielFrance']                  = $qp095PE5.coordonnees.coordonneesDeclarantFrance.declarantCourrielFrance;

//Profession 

cerfaFields['professionExerceeEM']     					= $qp095PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionExerceeEM;
cerfaFields['professionExerceeSpecialiteEM']  			= $qp095PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteEM;
cerfaFields['professionConcernee']                      = $qp095PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.professionConcernee;
cerfaFields['professionConcerneeSpecialite']            = $qp095PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.specialiteFrance;
cerfaFields['typesActesEnvisages']                      = $qp095PE5.professionConcerneeEtatEtablissement1.professionConcerneeEtatEtablissement.typesActesEnvisages;
cerfaFields['lieuExerciceLPSVoieComp']                  = ($qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle != null ? $qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceNumeroLibelle : '') 
														+ ' ' + ($qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse != null ? $qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceComplementAdresse: '');
cerfaFields['lieuExerciceLPSCPVillePays']               = ($qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal != null ? $qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceCodePostal : '') 
														+ ' ' + ($qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille != null ? $qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExerciceVille : '') 
														+', ' + ($qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays != null ? $qp095PE5.lieuPremierePrestation.lieuPremierePrestation.lieuExercicePays : '');


// Ordre professionnel 

cerfaFields['ordreProfessionnelOui']                    = ($qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==true);
cerfaFields['ordreProfessionnelNon']                    = ($qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelOuOrganismeEquivalent==false);
cerfaFields['ordreProfessionnelNom']                    = $qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelNom;
cerfaFields['ordreProfessionnelAdresseRueComplement']   = ($qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee != null ? $qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseNumeroLibellee : '') + ' ' + ($qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse != null ? $qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseComplementAdresse : '');
cerfaFields['ordreProfessionnelAdresseVillePays']           = ($qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal != null ? $qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseCodePostal :'') +' '+ ($qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille != null ? $qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdresseVille : '') + ', ' + ($qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdressePays != null ? $qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelAdressePays : '');
cerfaFields['ordreProfessionnelNumeroEnregistrement']   = $qp095PE5.ordre.ordreProfessionnel.ordreProfessionnelNumeroEnregistrement;


// Assurance 

cerfaFields['compagnieAssuranceNom']                    = $qp095PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNom;
cerfaFields['compagnieAssuranceNumeroContrat']          = $qp095PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.compagnieAssuranceNumeroContrat;
cerfaFields['commentairesLPSDemandeInitiale']           = $qp095PE5.couvertureAssuranceProfessionnelle.couvertureAssuranceProfessionnelleGroupe.commentairesLPSDemandeInitiale;

//informations à fournir en cas de renouvellement 

cerfaFields['datePrestationDebut1']                     = $qp095PE5.detailsRenouvellement.renouvellement.periodePreste1.from;
cerfaFields['datePrestationFin1']                       = $qp095PE5.detailsRenouvellement.renouvellement.periodePreste1.to;
cerfaFields['datePrestationDebut2']                     = ($qp095PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp095PE5.detailsRenouvellement.renouvellement.periodePreste2.from : '');
cerfaFields['datePrestationFin2']                       = ($qp095PE5.detailsRenouvellement.renouvellement.autrePeriode1 ? $qp095PE5.detailsRenouvellement.renouvellement.periodePreste2.to : '');
cerfaFields['datePrestationDebut3']                     = ($qp095PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp095PE5.detailsRenouvellement.renouvellement.periodePreste3.from : '');
cerfaFields['datePrestationFin3']                       = ($qp095PE5.detailsRenouvellement.renouvellement.autrePeriode2 ? $qp095PE5.detailsRenouvellement.renouvellement.periodePreste3.to : '');
cerfaFields['datePrestationDebut4']                     = ($qp095PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp095PE5.detailsRenouvellement.renouvellement.periodePreste4.from : '');
cerfaFields['datePrestationFin4']                       = ($qp095PE5.detailsRenouvellement.renouvellement.autrePeriode3 ? $qp095PE5.detailsRenouvellement.renouvellement.periodePreste4.to : '');
cerfaFields['datePrestationDebut5']                     = ($qp095PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp095PE5.detailsRenouvellement.renouvellement.periodePreste5.from : '');
cerfaFields['datePrestationFin5']                       = ($qp095PE5.detailsRenouvellement.renouvellement.autrePeriode4 ? $qp095PE5.detailsRenouvellement.renouvellement.periodePreste5.to : '');

cerfaFields['commentairesLPSRenouvellement']            = $qp095PE5.detailsRenouvellement.renouvellement.commentairesRenouvellement;
cerfaFields['activiteProfessionnelsLPSRevouvellement'] = $qp095PE5.detailsRenouvellement.renouvellement.activitesProfessionnelles;

// Observations 
cerfaFields['autresObservations']                       = $qp095PE5.observations.observations.autresObservations;

// Signature
cerfaFields['signatureCoche']                           = $qp095PE5.signature.signature.signatureCoche;
cerfaFields['signatureDate']                            = $qp095PE5.signature.signature.signatureDate;
cerfaFields['signature']                                ='Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.'

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp095PE5.signature.signature.signatureDate,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp095PE5.signature.signature.signatureDate,
		autoriteHabilitee :"La Direction Générale de l’Offre de Soins",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc 
	.load('models/Formulaire_LPS.pdf') 
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCopieDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Diététicien_LPS_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Diététicien - Renouvellement de la déclaration en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de diététicien.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});