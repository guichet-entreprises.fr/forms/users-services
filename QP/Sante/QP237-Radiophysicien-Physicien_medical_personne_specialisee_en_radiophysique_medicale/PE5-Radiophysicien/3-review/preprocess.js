var formFields = {};

var civNomPrenom = $qp237PE5.info1.etatCivil.civilite + ' ' + $qp237PE5.info1.etatCivil.declarantNomNaissance + ' ' + $qp237PE5.info1.etatCivil.declarantPrenoms;

//Motif de la demande

formFields['professionConcernee']                                  = "Radiophysicien";

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;


//Identité du demandeur

formFields['declarantNomNaissance']                                = $qp237PE5.info1.etatCivil.declarantNomNaissance;
formFields['declarantPrenoms']                                     = $qp237PE5.info1.etatCivil.declarantPrenoms;
formFields['declarantNationalite']                                 = $qp237PE5.info1.etatCivil.declarantNationalite;
formFields['declarantDateNaissance']                               = $qp237PE5.info1.etatCivil.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp237PE5.info1.etatCivil.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp237PE5.info1.etatCivil.declarantPaysNaissance;


//Adresse

formFields['declarantAdressePersoCourriel']                        = $qp237PE5.info2.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoieComplement']            = ($qp237PE5.info2.coordonnees1.declarantAdressePersoNumNomVoie != null ? $qp237PE5.info2.coordonnees1.declarantAdressePersoNumNomVoie: '') + ' ' + ($qp237PE5.info2.coordonnees1.declarantAdressePersoComplement != null ? $qp237PE5.info2.coordonnees1.declarantAdressePersoComplement: '');
//formFields['declarantAdressePersoCP']                            = $qp237PE5.info2.coordonnees1.declarantAdressePersoCP;
//formFields['declarantAdressePersoVille']                         = $qp237PE5.info2.coordonnees1.declarantAdressePersoVille;
//formFields['declarantAdressePersoPays']                          = $qp237PE5.info2.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoCPVillePays']                     = ($qp237PE5.info2.coordonnees1.declarantAdressePersoCP != null ? $qp237PE5.info2.coordonnees1.declarantAdressePersoCP: '') + ' ' + ($qp237PE5.info2.coordonnees1.declarantAdressePersoVille != null ? $qp237PE5.info2.coordonnees1.declarantAdressePersoVille: '') +' '+ ($qp237PE5.info2.coordonnees1.declarantAdressePersoPays != null ? $qp237PE5.info2.coordonnees1.declarantAdressePersoPays: '');
formFields['declarantAdressePersoTelephone']                       = $qp237PE5.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']                        = $qp237PE5.info2.coordonnees1.declarantAdressePersoCourriel;

//Adresse France 

formFields['declarantAdresseFranceNumNomVoieComplement']           = ($qp237PE5.info2.coordonnees2.declarantAdresseFranceNumNomVoie != null ? $qp237PE5.info2.coordonnees2.declarantAdresseFranceNumNomVoie: '') + ' ' + ($qp237PE5.info2.coordonnees2.declarantAdresseFranceComplement != null ? $qp237PE5.info2.coordonnees2.declarantAdresseFranceComplement: '');
formFields['declarantAdresseFranceCPVillePays']                    = ($qp237PE5.info2.coordonnees2.declarantAdresseFranceCP != null ? $qp237PE5.info2.coordonnees2.declarantAdresseFranceCP: '') + ' ' + ($qp237PE5.info2.coordonnees2.declarantAdresseFranceVille != null ? $qp237PE5.info2.coordonnees2.declarantAdresseFranceVille: '');
formFields['declarantAdresseFranceTelephone']                      = $qp237PE5.info2.coordonnees2.declarantAdresseFranceTelephone;
formFields['declarantAdresseFranceCourriel']                       = $qp237PE5.info2.coordonnees2.declarantAdresseFranceCourriel;

//Profession concernée

formFields['professionExercee']                                    = $qp237PE5.info3.profession.professionExercee;
formFields['specialite']                                           = $qp237PE5.info3.profession.specialite != null ? $qp237PE5.info3.profession.specialite  : '';
formFields['professionDemandee']                                   = $qp237PE5.info3.profession.professionDemandee;
formFields['specialiteDemandee']                                   = $qp237PE5.info3.profession.specialiteDemandee != null ? $qp237PE5.info3.profession.specialiteDemandee : '';
formFields['actesEnvisages']                                       = $qp237PE5.info3.profession.actesEnvisages != null ? $qp237PE5.info3.profession.actesEnvisages : '';
formFields['lieupremiereLPS']                                      = $qp237PE5.info3.profession.lieupremiereLPS != null ? $qp237PE5.info3.profession.lieupremiereLPS : '';

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp237PE5.info4.ordre.ouiNon).eq('ordreNon') ? true : false;
formFields['ordreOui']                                             = Value('id').of($qp237PE5.info4.ordre.ouiNon).eq('ordreOui') ? true : false;

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp237PE5.info4.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp237PE5.info4.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp237PE5.info4.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp237PE5.info4.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp237PE5.info4.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp237PE5.info4.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp237PE5.info4.ordre.ordreProfessionnelAdresseVille != null ? $qp237PE5.info4.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp237PE5.info4.ordre.ordreProfessionnelAdressePays != null ? $qp237PE5.info4.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = ($qp237PE5.info4.ordre.ordreProfessionnelNom != null ? $qp237PE5.info4.ordre.ordreProfessionnelNom : '') + ' ' 
																	+ ($qp237PE5.info4.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp237PE5.info4.ordre.ordreProfessionnelNumEnregistrementNom: '');


//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp237PE5.info5.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp237PE5.info5.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp237PE5.info5.assuranceProfessionnelle.commentaires != null ? $qp237PE5.info5.assuranceProfessionnelle.commentaires :'';

//Signature

formFields['autresObservations']                                    = $qp237PE5.commentaire.commentaire.commentairesEventuels != null ? $qp237PE5.commentaire.commentaire.commentairesEventuels :'';
formFields['dateSignature']                                        = $qp237PE5.info6.signature.dateSignature;
formFields['attesteSignature']                                     = $qp237PE5.info6.signature.attesteSignature;



formFields['libelleProfession']				                       = "Radiophysicien (Physicien médical, personne spécialisée en radiophysique médicale)"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp237PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp237PE5.info6.signature.dateSignature,
		autoriteHabilitee :"Ministère des Solidarités et de la Santé",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire_sante_LPS.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Radiophysicien_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Radiophysicien (Physicien médical, personne spécialisée en radiophysique médicale) - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de radiophysicien (Physicien médical, personne spécialisée en radiophysique médicale)',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
