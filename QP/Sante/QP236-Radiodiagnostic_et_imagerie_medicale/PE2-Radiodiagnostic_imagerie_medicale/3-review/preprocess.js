var formFields = {};

var civNomPrenom = $qp236PE2.info2.etatCivile.civilite + ' ' + $qp236PE2.info2.etatCivile.declarantNomNaissance + ' ' + $qp236PE2.info2.etatCivile.declarantPrenoms;


//Profession considérée

formFields['typesActes']                                           = $qp236PE2.info1.profession.typesActes;
formFields['lieuExercice']                                         = '';
formFields['nomProfessionLPS']                                     = $qp236PE2.info1.profession.nomProfessionLPS;
formFields['nomProfessionExerceeEM']                               = $qp236PE2.info1.profession.nomProfessionExerceeEM;
formFields['specialite']                                           = $qp236PE2.info1.profession.specialite;
formFields['listeActes']                                           = $qp236PE2.info1.profession.listeActes;
formFields['exerciceAutonome']                                     = $qp236PE2.info1.profession.exerciceAutonome;
formFields['specialiteExerceeEM']                                  = $qp236PE2.info1.profession.specialiteExerceeEM;
formFields['partieProfessionSiAccesPartiel']                       = $qp236PE2.info1.profession.partieProfessionSiAccesPartiel;

//Prestation de service

formFields['declarationInitialeLPS']                               = true;
formFields['renouvellementLPS']                                    = false;
formFields['modificationLPS']                                      = false;

//Etat civil

formFields['civiliteMadame']                                       = Value('id').of($qp236PE2.info2.etatCivile.civilite).eq('madame') ? '' : '---------';
formFields['civiliteMonsieur']                                     = Value('id').of($qp236PE2.info2.etatCivile.civilite).eq('monsieur') ? '' : '----';
formFields['declarantNomNaissance']                                = $qp236PE2.info2.etatCivile.declarantNomNaissance;
formFields['declarantNomUsage']                                    = $qp236PE2.info2.etatCivile.declarantNomUsage;
formFields['declarantPrenoms']                                     = $qp236PE2.info2.etatCivile.declarantPrenoms;
formFields['declarantDateNaissance']                               = $qp236PE2.info2.etatCivile.declarantDateNaissance;
formFields['declarantVilleNaissance']                              = $qp236PE2.info2.etatCivile.declarantVilleNaissance;
formFields['declarantPaysNaissance']                               = $qp236PE2.info2.etatCivile.declarantPaysNaissance;
formFields['declarantPaysNationalite']                             = $qp236PE2.info2.etatCivile.declarantPaysNationalite;

//Coordonnées

formFields['declarantAdressePersoVille']                           = $qp236PE2.info3.coordonnees1.declarantAdressePersoVille;
formFields['declarantAdressePersoCourriel']                        = $qp236PE2.info3.coordonnees1.declarantAdressePersoCourriel;
formFields['declarantAdressePersoNumNomVoie']                      = $qp236PE2.info3.coordonnees1.declarantAdressePersoNumNomVoie;
formFields['declarantAdressePersoComplement']                      = $qp236PE2.info3.coordonnees1.declarantAdressePersoComplement;
formFields['declarantAdressePersoCP']                              = $qp236PE2.info3.coordonnees1.declarantAdressePersoCP;
formFields['declarantAdressePersoPays']                            = $qp236PE2.info3.coordonnees1.declarantAdressePersoPays;
formFields['declarantAdressePersoTelephone']                       = $qp236PE2.info3.coordonnees1.declarantAdressePersoTelephone;

formFields['declarantAdresseFranceNumNomVoie']                     = $qp236PE2.info3.coordonnees2.declarantAdresseFranceNumNomVoie;
formFields['declarantAdresseFranceComplement']                     = $qp236PE2.info3.coordonnees2.declarantAdresseFranceComplement;
formFields['declarantAdresseFranceCP']                             = $qp236PE2.info3.coordonnees2.declarantAdresseFranceCP;
formFields['declarantAdresseFranceVille']                          = $qp236PE2.info3.coordonnees2.declarantAdresseFranceVille;
formFields['declarantAdresseFrancePays']                           = $qp236PE2.info3.coordonnees2.declarantAdresseFrancePays;
formFields['declarantAdresseFranceTelephone']                      = $qp236PE2.info3.coordonnees2.declarantAdresseFranceTelephone;


//Diplôme de la profession considérée

formFields['intituleTitreFormationProfession']                     = $qp236PE2.info4.diplome.intituleTitreFormationProfession;
formFields['dateObtentionTitreFormationProfession']                = $qp236PE2.info4.diplome.dateObtentionTitreFormationProfession;
formFields['paysObtentionTitreFormationProfession']                = $qp236PE2.info4.diplome.paysObtentionTitreFormationProfession;
formFields['etablissementDelivranceTitreFormationProfession']      = $qp236PE2.info4.diplome.etablissementDelivranceTitreFormationProfession;
formFields['dateReconnaissanceTitreFormationProfession']           = $qp236PE2.info4.diplome.dateReconnaissanceTitreFormationProfession;

formFields['intituleAutreTitreFormationProfession']                = $qp236PE2.info4.autreDiplome.intituleAutreTitreFormationProfession;
formFields['dateObtentionAutreTitreFormationProfession']           = $qp236PE2.info4.autreDiplome.dateObtentionAutreTitreFormationProfession;
formFields['paysObtentionAutreTitreFormationProfession']           = $qp236PE2.info4.autreDiplome.paysObtentionAutreTitreFormationProfession;
formFields['etablissementDelivranceAutreTitreFormationProfession'] = $qp236PE2.info4.autreDiplome.etablissementDelivranceAutreTitreFormationProfession;
formFields['dateReconnaissanceAutreTitreFormationProfession']      = $qp236PE2.info4.autreDiplome.dateReconnaissanceAutreTitreFormationProfession;

formFields['intituleTitreDiplomeSpecialisation']                   = $qp236PE2.info4.specialiteDiplome.intituleTitreDiplomeSpecialisation;
formFields['dateObtentionTitreDiplomeSpecialisation']              = $qp236PE2.info4.specialiteDiplome.dateObtentionTitreDiplomeSpecialisation;
formFields['paysObtentionTitreDiplomeSpecialisation']              = $qp236PE2.info4.specialiteDiplome.paysObtentionTitreDiplomeSpecialisation;
formFields['etablissementDelivranceDiplomeSpecialisation']         = $qp236PE2.info4.specialiteDiplome.etablissementDelivranceDiplomeSpecialisation;
formFields['dateReconnaissanceDiplomeSpecialisation']              = $qp236PE2.info4.specialiteDiplome.dateReconnaissanceDiplomeSpecialisation;

//Ordre professionnel

formFields['ordreNon']                                             = Value('id').of($qp236PE2.info5.ordre.ouiNon).eq('ordreNon') ? '' : '--------';
formFields['ordreOui']                                             = Value('id').of($qp236PE2.info5.ordre.ouiNon).eq('ordreOui') ? '' : '--------';

formFields['ordreProfessionnelNumNomRueComplementAdresse']         = ($qp236PE2.info5.ordre.ordreProfessionnelAdresseNumeroLibellee != null ? $qp236PE2.info5.ordre.ordreProfessionnelAdresseNumeroLibellee: '') + ' ' + ($qp236PE2.info5.ordre.ordreProfessionnelAdresseComplementAdresse != null ? $qp236PE2.info5.ordre.ordreProfessionnelAdresseComplementAdresse: '');
formFields['ordreProfessionnelCPVillePaysAdresse']                 = ($qp236PE2.info5.ordre.ordreProfessionnelAdresseCodePostal != null ? $qp236PE2.info5.ordre.ordreProfessionnelAdresseCodePostal: '') + ' ' + ($qp236PE2.info5.ordre.ordreProfessionnelAdresseVille != null ? $qp236PE2.info5.ordre.ordreProfessionnelAdresseVille: '') + ' ' + ($qp236PE2.info5.ordre.ordreProfessionnelAdressePays != null ? $qp236PE2.info5.ordre.ordreProfessionnelAdressePays: ''); 
formFields['ordreProfessionnelNumEnregistrementNom']               = $qp236PE2.info5.ordre.ordreProfessionnelNumEnregistrementNom != null ? $qp236PE2.info5.ordre.ordreProfessionnelNumEnregistrementNom: '';

//Assurance professionnelle

formFields['compagnieAssuranceNom']                                = $qp236PE2.info6.assuranceProfessionnelle.compagnieAssuranceNom;
formFields['compagnieAssuranceNumContrat']                         = $qp236PE2.info6.assuranceProfessionnelle.compagnieAssuranceNumContrat;
formFields['commentaires']                                         = $qp236PE2.info6.assuranceProfessionnelle.commentaires;

//Signature

formFields['commentairesSignature']                                = $qp236PE2.info7.signature.commentairesSignature;
formFields['dateSignature']                                        = $qp236PE2.info7.signature.dateSignature;
formFields['attesteSignature']                                     = $qp236PE2.info7.signature.attesteSignature;


formFields['prestation1Du']                     				   = '';
formFields['prestation1Au']                                        = '';
formFields['prestation2Du']                                        = '';
formFields['prestation2Au']                                        = '';
formFields['prestation3Du']                                        = '';
formFields['prestation3Au']                                        = '';
formFields['prestation4Du']                                        = '';
formFields['prestation4Au']                                        = '';
formFields['prestation5Du']                                        = '';
formFields['prestation5Au']                                        = '';
formFields['prestation6Du']                                        = '';
formFields['prestation6Au']                                        = '';


formFields['prestation1ActiviteExercee']                           = '';
formFields['prestation2ActiviteExercee']                           = '';
formFields['prestation3ActiviteExercee']                           = '';
formFields['prestation4ActiviteExercee']                           = '';
formFields['prestation5ActiviteExercee']                           = '';
formFields['prestation6ActiviteExercee']                           = '';


formFields['libelleProfession']				                       = "Radiodiagnostic et imagerie médicale (médecin spécialiste)"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp236PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp236PE2.info7.signature.dateSignature,
		autoriteHabilitee :"Conseil national de l’Ordre des médecins",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Radiodiagnostic_imagerie_medicale.pdf') //
	.apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitre);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAssurance);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEquivalentDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTraductionDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjObligationsCommunautaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Radiodiagnostic_imagerie_medicale_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Radiodiagnostic et imagerie médicale (médecin spécialiste) - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de la radiodiagnostic et imagerie médicale (médecin spécialiste)',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
