**Procédure élémentaire 3 : Praticien de l'art dentaire (QP223)**

**Procédure** : Renouvellement de la déclaration en vue d'une libre prestation
de services

**Contexte :** LPS - Renouvellement

**Référence-id** : Formalités SCN/GQ/Santé/Praticien de l'art dentaire/Libre prestation de services/Renouvellement

**Formulaire** : Formulaire LPS professions médicales et pharmaciens.

**Mode de transmission** : Transmission par courrier avec accusé de réception.

**Destinataire** : Conseil National de l’Ordre des Médecins.

**Délais** :

-   1 mois pour accuser réception du dossier.

-   1 mois supplémentaire si nécessaire pour solliciter des pièces
    supplémentaires.

-   2 mois à compter de la réception du dossier pour donner l’accord ou le
    refus.

-   Le silence vaut acceptation.

**Pièces justificatives** :

La déclaration doit être accompagnée des pièces justificatives suivantes :

-   Copie de la pièce d’identité ainsi qu'un document attestant de la
    nationalité si cette pièce ne le prévoit pas. Extrait d'acte de naissance,
    fiche d'état civil, carte d'identité ou passeport, recto verso si nécessaire
    et en cours de validité ;

-   Copie de la précédente déclaration préalable en vue d'une libre prestation
    de services

-   Copie de l’attestation de l’autorité compétente de l’Etat d’établissement,
    certifiant que l’intéressé est légalement établi dans cet Etat et qu’il
    n’encourt aucune interdiction d’exercer lorsque l’attestation est délivrée.

-   Copie d'attestation d'assurance ou déclaration d'engagement de fourniture
    d'une attestion d'assurance

-   Copie de l'attestation prouvant la maitrise suffisante de la langue
    française. Attestation de qualification délivrée à l’issue d’une formation
    en français, attestation de niveau en français délivrée par une institution
    spécialisée ou document attestant d’une expérience professionnelle acquise
    en France

**Commentaires** :

Le prestataire joint une déclaration concernant les connaissances linguistiques
nécessaires à la réalisation de la prestation.

Le caractère temporaire et occasionnel de la prestation de services est apprécié
au cas par cas, notamment en fonction de sa durée, de sa fréquence, sa
périodicité et sa continuité.

Les pièces justificatives doivent être rédigées en langue française, ou
traduites par un traducteur agréé.
