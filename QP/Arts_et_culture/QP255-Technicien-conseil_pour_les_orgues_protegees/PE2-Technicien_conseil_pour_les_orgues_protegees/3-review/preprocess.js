var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp255PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp255PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp255PE2.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']                       = civNomPrenom;
cerfaFields['civiliteNomPrenomSignature']              = $qp255PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp255PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp255PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']                      = $qp255PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant +', ' + $qp255PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                             = $qp255PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                           = $qp255PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle

cerfaFields['telephoneMobile']                         = $qp255PE2.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant; 
cerfaFields['adresse']                                 = $qp255PE2.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp255PE2.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null?', ' + $qp255PE2.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : '');
cerfaFields['telephoneFixe']                           = $qp255PE2.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']                         = $qp255PE2.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']                                = $qp255PE2.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;
cerfaFields['villePays']                               = ($qp255PE2.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp255PE2.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant +' ' : '') + $qp255PE2.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ' ' + $qp255PE2.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;

//signature 
cerfaFields['date']                		               = $qp255PE2.signature.signature.dateSignature;
cerfaFields['signature']           					   = $qp255PE2.signature.signature.signature;
cerfaFields['lieuSignature']                           = $qp255PE2.signature.signature.lieuSignature + ', le ';



if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {
	
/*
 * Chargement du model de document et injection des données
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp255PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
	.apply({
		date: $qp255PE2.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.ExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjReferences);
appendPj($attachmentPreprocess.attachmentPreprocess.pjMissions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjEtudePrealable);
appendPj($attachmentPreprocess.attachmentPreprocess.pjMaitriseOeuvre);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Technicien conseil orgues - demande RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Technicien conseil pour les orgues protégées - demande d’agrément de technicien-conseil pour les orgues protégées',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de technicien conseil pour les orgues protégées est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
}

function buildSimpleDocument() {
	
var cerfa = pdf.create('models/courrier libre LE V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Technicien conseil orgues - demande RQP.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Technicien conseil pour les orgues protégées - demande d’agrément',
   groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de demande de reconnaissance de qualifications professionnelles pour la profession de technicien conseil pour les orgues protégées',
            description : 'Voici le courrier obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
}