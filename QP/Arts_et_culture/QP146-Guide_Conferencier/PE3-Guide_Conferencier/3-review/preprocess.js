var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp146PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp146PE3.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp146PE3.etatCivil.identificationDeclarant.prenomDeclarant;
var departementExercice = $qp146PE3.signature.signature.departementExercice;

cerfaFields['nomUsageDeclarant']    = $qp146PE3.etatCivil.identificationDeclarant.nomUsageDeclarant;
cerfaFields['prenomDeclarant']      = $qp146PE3.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['nomDeclarant']         = $qp146PE3.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['dateNaissance']        = $qp146PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['lieuNaissance']        = $qp146PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['paysNaissance']        = $qp146PE3.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationaliteDeclarant'] = $qp146PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['adresse']              = $qp146PE3.adresse.adresseContact.numeroLibelleAdresseDeclarant +' '+ ($qp146PE3.adresse.adresseContact.complementAdresseDeclarant != null ? $qp146PE3.adresse.adresseContact.complementAdresseDeclarant:'')  +', '+ ($qp146PE3.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp146PE3.adresse.adresseContact.codePostalAdresseDeclarant:'') + ' ' + $qp146PE3.adresse.adresseContact.villeAdresseDeclarant +' ' +$qp146PE3.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['adresseMail']          = $qp146PE3.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['telephoneFixe']        = $qp146PE3.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']      = $qp146PE3.adresse.adresseContact.telephoneMobileAdresseDeclarant;

cerfaFields['titre1']               = $qp146PE3.justificatifsDeclarant.justificatifs.titreJustificatif1;
cerfaFields['titre2']               = $qp146PE3.justificatifsDeclarant.diplomes2.titreJustificatif2;
cerfaFields['titre3']               = $qp146PE3.justificatifsDeclarant.diplomes3.titreJustificatif3;
cerfaFields['titre4']               = $qp146PE3.justificatifsDeclarant.diplomes4.titreJustificatif4;
cerfaFields['anneeObtention1']      = $qp146PE3.justificatifsDeclarant.justificatifs.anneeObtention1;
cerfaFields['anneeObtention2']      = $qp146PE3.justificatifsDeclarant.diplomes2.anneeObtention2;
cerfaFields['anneeObtention3']      = $qp146PE3.justificatifsDeclarant.diplomes3.anneeObtention3;
cerfaFields['anneeObtention4']      = $qp146PE3.justificatifsDeclarant.diplomes4.anneeObtention4;
cerfaFields['mentions1']            = $qp146PE3.justificatifsDeclarant.justificatifs.mentions1;
cerfaFields['mentions2']            = $qp146PE3.justificatifsDeclarant.diplomes2.mentions2;
cerfaFields['mentions3']            = $qp146PE3.justificatifsDeclarant.diplomes3.mentions3;
cerfaFields['mentions4']            = $qp146PE3.justificatifsDeclarant.diplomes4.mentions4;

cerfaFields['faitA']                = $qp146PE3.signature.signature.lieuSignature;
cerfaFields['faitLe']               = $qp146PE3.signature.signature.dateSignature;
cerfaFields['signature']            = "Je déclare sur l’honneur l’exactitude des informations de la formalité et signe la présente déclaration.";
cerfaFields['signatureCoche']       = $qp146PE3.signature.signature.signature;

cerfaFields['libelleProfession']				= "Guide conférencier"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp146PE3.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp146PE3.signature.signature.dateSignature,
		autoriteHabilitee : "Préfecture du département",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/formulaire_model.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres2);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCV);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Guide_conferencier_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Guide conférencier - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de guide conférencier',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
