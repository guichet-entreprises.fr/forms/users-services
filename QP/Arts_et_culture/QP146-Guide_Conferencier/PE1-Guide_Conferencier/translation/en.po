msgid "Identification du déclarant"
msgstr "Identification of the declarant"

msgid "Demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement."
msgstr "Application for recognition of professional qualifications for free establishment."

msgid "Formulaire de demande d'autorisation d'exercice"
msgstr "Application Form"
 
msgid "Civilité :"
msgstr "Civility"

msgid "Nom de naissance :"
msgstr "Birth name"

msgid "Nom d'usage :"
msgstr "Use name"


msgid "Prénom(s) :"
msgstr "First name"


msgid "Déclaration préalable dans le cadre d’un libre établissement"
msgstr "Declaration in the context of a free establishment"


msgid "Date de naissance :"
msgstr "Birth date"

msgid "Commune ou ville de naissance :"
msgstr "Town or city of birth"


msgid "Pays de naissance :"
msgstr "Native country"


msgid "Nationalité :"
msgstr "Nationality"

msgid "Adresse personnelle"
msgstr "Personal address"

msgid "Adresse professionnelle"
msgstr "Professional address"

msgid "Signature de la formalité"
msgstr "Signature of formality"

msgid "Je déclare sur l'honneur l'exactitude des informations de la formalité"
msgstr "I declare on my honor the accuracy of the information on the formality"

msgid "Lieu :"
msgstr "Location :"

msgid "Date :"
msgstr "Date"

msgid "Numéro et libellé de voie :"
msgstr "Track number and label :"

msgid "Complément d'adresse :"
msgstr "Additional address :"

msgid "Code postal :"
msgstr "Postal code :"

msgid "Commune ou ville :"
msgstr "City or town :"

msgid "Pays :"
msgstr "Country :"

msgid "Téléphone fixe :"
msgstr "Phone :"
 
msgid "Téléphone mobile :"
msgstr "Mobile phone :"

msgid "Courriel :"
msgstr "E-mail address :"

msgid "Copie de la pièce d’identité (en cours de validité, recto verso : extrait d'acte de naissance ou fiche d'état civil ou carte d'identité ou passeport)."
msgstr "Copy of the identity document (valid, double-sided: extract of birth certificate or civil status card or identity card or passport)."

msgid "Copie d’un diplôme de second cycle de l’enseignement supérieur juridique, scientifique ou technique (ou équivalent)."
msgstr "Copy of a post-graduate degree in legal, scientific or technical higher education (or equivalent)."

msgid "Copie du diplôme du Centre d'études internationales de la propriété industrielle de Strasbourg (Ceipi) ou du titre reconnu équivalent."
msgstr "Copy of the diploma of the Center for International Studies of the Industrial Property of Strasbourg (Ceipi) or the equivalent recognized title."

msgid "Un diplôme national de troisième cycle ou un diplôme national de master sanctionnant une formation dans le domaine de la propriété industrielle."
msgstr "A national post-graduate diploma or a national master's degree awarded in the field of industrial property."

msgid "Copie(s) de l'attestation professionnelle."
msgstr "Copy (s) of professional attestation."

msgid "Le(s) certificat(s) doit(vent) préciser le lieu d’exercice de la pratique professionnelle, celle-ci doit avoir été acquise au sein d’un Etat de l’Union européenne ou de l’Espace économique européen ou en Suisse."
msgstr "The certificate (s) must specify the place of practice of the professional practice, the latter must have been acquired within a Member State of the European Union or the European Economic Area or Swiss."

msgid "Un diplôme national de troisième cycle ou un diplôme national de master sanctionnant une formation dans le domaine de la propriété industrielle"
msgstr "A national post-graduate diploma or a national master's degree in training in the field of industrial property"
  
msgid "les documents suivants doivent être joints à la demande (en format .PDF, .JPG ou .PNG) :"
msgstr "the following documents must be attached to the application (in .PDF, .JPG or .PNG format) :"

msgid "Demande de RQP en vue d'un libre établissement.pdf"
msgstr "Request for a RQP for Free Settlement"

msgid "Courrier obtenu à partir des données saisies"
msgstr "Mail obtained from the data entered"

msgid "Il s'agit d'indiquer votre titre : Monsieur si vous êtes un homme, Madame si vous êtes une femme. Remarque : le terme Mademoiselle n'est plus utilisé en France il convient donc dans ce cas de selectionner Madame."
msgstr "This is to indicate your title : Sir if you are a man, madam if you are a woman. Note: the term Mademoiselle is no longer used in France so it is advisable in this case to select Madame."

msgid "Il s'agit du nom qui vous a été donné à votre naissance tel qu'il apparaît sur votre justificatif d'identité."
msgstr "This is the name that was given to you at your birth as it appears on your proof of identity."

msgid "Il s'agit de l'ensemble des prénoms figurant sur votre justificatif d'identité. L'ordre indiqué sur votre justificatif d'identité doit être respecté et les prénoms doivent êtres séparés par une virgule."
msgstr "This is the set of first names on your proof of identity. The order shown on your proof of identity must be respected and the first names must be separated by a comma."

msgid "Il s'agit d'indiquer en toutes lettres la localité où vous êtes né."
msgstr "It is a question of indicating in full the place where you were born."

msgid "Il s'agit d'indiquer en toutes lettres le pays de naissance."
msgstr "It is a question of indicating the country of birth in full."

msgid "Il s'agit d'indiquer en toutes lettres votre nationalité."
msgstr "You have to indicate your nationality in full."

msgid "Il convient de renseigner l'adresse de votre domicile personnel."
msgstr "It is necessary to inform the address of your personal home."

msgid "Il convient d'indiquer dans l'ordre : le numéro de voie, le cas échéant l'indice de répétition, le type de voie et le nom de la voie. Exemple : 12 bis rue de Paris."
msgstr "The order of the channel number, if applicable the repeat index, the type of channel and the name of the channel should be indicated. Example: 12 bis rue de Paris."

msgid "Il convient d'indiquer ici tout complément d'adresse permettant de l'identifier tels que : lieu-dit, BP, CS, bâtiment, escalier, étage, appartement, etc…"
msgstr "It is necessary to indicate here any additional address allowing to identify it such as: place, BP, CS, building, staircase, floor, apartment, etc ..."

msgid "Il convient d'indiquer le code postal de la localité."
msgstr "The postal code of the locality should be indicated."

msgid "Il convient d'indiquer la localité."
msgstr "The locality should be indicated."

msgid "Il convient d'indiquer en toutes lettres le pays de résidence."
msgstr "The country of residence should be indicated in full."

msgid "Il convient de séléctionner le pays d'attribution du numéro et de renseigner le numéro de téléphone fixe."
msgstr "The country of assignment of the number should be selected and the fixed telephone number should be entered."

msgid "Il convient de séléctionner le pays d'attribution du numéro et de renseigner le numéro de téléphone mobile."
msgstr "It is necessary to select the country of assignment of the number and to fill in the mobile phone number."

msgid "Il convient de renseigner une adresse e-mail valide."
msgstr "A valid e-mail address is required."

msgid "Il convient de renseigner l'adresse professionnelle."
msgstr "The business address should be filled in."

msgid "Il convient d'indiquer en toutes lettres le pays d'exercice."
msgstr "The country of practice should be indicated in full."

msgid "Il convient d'indiquer ici le lieu de signature."
msgstr "The place of signature should be indicated here."


























