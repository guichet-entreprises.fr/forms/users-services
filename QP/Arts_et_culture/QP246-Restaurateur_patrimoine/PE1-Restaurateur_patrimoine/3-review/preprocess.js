var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp246PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp246PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp246PE1.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']                     = $qp246PE1.etatCivil.identificationDeclarant.civilite + ' ' 
													    + $qp246PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' 
														+ $qp246PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePays']              			       = $qp246PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant +', ' + $qp246PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                             = $qp246PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                           = $qp246PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['villePaysNaissance']                      = $qp246PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp246PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']   							   = $qp246PE1.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp246PE1.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null?', ' + $qp246PE1.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : '');
cerfaFields['telephoneMobile']                         = $qp246PE1.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant; 
cerfaFields['CP et ville adresse personnelle']         = ($qp246PE1.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp246PE1.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant +' ' : ' ') + $qp246PE1.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ', ' + $qp246PE1.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']                           = $qp246PE1.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']                         = $qp246PE1.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']                                    = $qp246PE1.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;

//adresse professionnelle

//cerfaFields['CP et ville adresse professionnelle']        = ($qp246PE1.adresseProfessionnelle.adresseProfessionnelle1.codePostalAdresseProfessionnelle != null ? $qp246PE1.adresseProfessionnelle.adresseProfessionnelle1.codePostalAdresseProfessionnelle + ' ' : '') + $qp246PE1.adresseProfessionnelle.adresseProfessionnelle1.villeAdresseProfessionnelle  + ', ' + $qp246PE1.adresseProfessionnelle.adresseProfessionnelle1.paysAdresseProfessionnelle;
//cerfaFields['rue et complément adresse professionnelle']  = $qp246PE1.adresseProfessionnelle.adresseProfessionnelle1.numeroLibelleAdresseProfessionnelle  + ', ' + ($qp246PE1.adresseProfessionnelle.adresseProfessionnelle1.complementAdresseProfessionnelle != null ? $qp246PE1.adresseProfessionnelle.adresseProfessionnelle1.complementAdresseProfessionnelle : '');


//signature
cerfaFields['lieuSignature']                           = $qp246PE1.signature.signature.lieuSignature + ', le ';
cerfaFields['date']                		               = $qp246PE1.signature.signature.dateSignature;
cerfaFields['signature']           					   = $qp246PE1.signature.signature.signature;
cerfaFields['civiliteNomPrenomSignature']    		   = $qp246PE1.etatCivil.identificationDeclarant.civilite + ' ' 
													    + $qp246PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' 
														+ $qp246PE1.etatCivil.identificationDeclarant.prenomDeclarant;

														

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp246PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp246PE1.signature.signature.dateSignature
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.copieDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.copieProgrammeEtude);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Restaurateur_patrimoine_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Restaurateur du patrimoine - demande de reconnaissance de qualifications professionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de restaurateur du patrimoine est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
