var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp246PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp246PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp246PE2.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']            = $qp246PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp246PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp246PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['civiliteNomPrenomSignature']   = $qp246PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp246PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp246PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']           = $qp246PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant +', ' + $qp246PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                  = $qp246PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']                = $qp246PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
//cerfaFields['adresse']                    
cerfaFields['telephoneMobile']            = $qp246PE2.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant; 
cerfaFields['adresse']                    = $qp246PE2.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp246PE2.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null?', ' + $qp246PE2.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : '')+","+($qp246PE2.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp246PE2.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant +' ' : ' ') + $qp246PE2.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant;
cerfaFields['villePays']                  = $qp246PE2.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']              = $qp246PE2.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']            = $qp246PE2.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']                   = $qp246PE2.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;

//adresse professionnelle

//cerfaFields['CP et ville adresse professionnelle']        = ($qp246PE2.adresseProfessionnelle.adresseProfessionnelle1.codePostalAdresseProfessionnelle != null ? $qp246PE2.adresseProfessionnelle.adresseProfessionnelle1.codePostalAdresseProfessionnelle + ' ' : '') + $qp246PE2.adresseProfessionnelle.adresseProfessionnelle1.villeAdresseProfessionnelle  + ', ' + $qp246PE2.adresseProfessionnelle.adresseProfessionnelle1.paysAdresseProfessionnelle;
//cerfaFields['rue et complément adresse professionnelle']  = $qp246PE2.adresseProfessionnelle.adresseProfessionnelle1.numeroLibelleAdresseProfessionnelle  + ', ' + ($qp246PE2.adresseProfessionnelle.adresseProfessionnelle1.complementAdresseProfessionnelle != null ? $qp246PE2.adresseProfessionnelle.adresseProfessionnelle1.complementAdresseProfessionnelle : '');




//signature
cerfaFields['date']                		  = $qp246PE2.signature.signature.dateSignature;
cerfaFields['signature']           	      = $qp246PE2.signature.signature.signature;
cerfaFields['lieuSignature']              = $qp246PE2.signature.signature.lieuSignature + ', le ';


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp246PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp246PE2.signature.signature.dateSignature
	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LE V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.copieDiplome);
appendPj($attachmentPreprocess.attachmentPreprocess.copieDocument);
appendPj($attachmentPreprocess.attachmentPreprocess.descriptifExperienceProfessionnelle);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Restaurateur_patrimoine_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Restaurateur du patrimoine - demande de reconnaissance de qualifications professionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de restaurateur du patrimoine est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
