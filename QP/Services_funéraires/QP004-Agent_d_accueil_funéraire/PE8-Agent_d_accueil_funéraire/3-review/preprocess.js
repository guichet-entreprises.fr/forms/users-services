var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp004PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp004PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp004PE8.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp004PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp004PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp004PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp004PE8.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp004PE8.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp004PE8.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp004PE8.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp004PE8.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp004PE8.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp004PE8.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp004PE8.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp004PE8.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp004PE8.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp004PE8.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp004PE8.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp004PE8.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp004PE8.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp004PE8.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp004PE8.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp004PE8.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp004PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp004PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp004PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['libelleProfession']			= "Agent d'accueil funéraire"
cerfaFields['libelleProfession2']			= "agent d'accueil funéraire."

/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp004PE8.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du departement",
		demandeContexte : "Déclaration préalable en vue dune libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjHabilitation);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Agent_accueil_funéraire_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Agent d\'accueil funéraire - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'agent d\'accueil funéraire',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
