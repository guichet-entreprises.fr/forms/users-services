var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp004PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp004PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp004PE6.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $qp004PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp004PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp004PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp004PE6.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp004PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp004PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp004PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp004PE6.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp004PE6.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp004PE6.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp004PE6.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp004PE6.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp004PE6.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp004PE6.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp004PE6.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp004PE6.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp004PE6.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp004PE6.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp004PE6.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp004PE6.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp004PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp004PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp004PE6.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Agent d'accueil funéraire"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp004PE6.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp004PE6.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du departement",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Agent_accueil_funéraire_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Agent d\'accueil funéraire - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'agent d\'accueil funéraire',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
