var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp161PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp161PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp161PE6.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $qp161PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp161PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp161PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp161PE6.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp161PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp161PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp161PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp161PE6.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp161PE6.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp161PE6.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp161PE6.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp161PE6.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp161PE6.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp161PE6.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp161PE6.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp161PE6.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp161PE6.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp161PE6.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp161PE6.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp161PE6.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp161PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp161PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp161PE6.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Maître de cérémonie"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp161PE6.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp161PE6.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Maître_de_cérémonie_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Maître de cérémonie - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de maître de cérémonie',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
