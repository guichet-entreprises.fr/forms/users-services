var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp161PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp161PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp161PE9.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp161PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp161PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp161PE9.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp161PE9.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp161PE9.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp161PE9.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp161PE9.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp161PE9.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp161PE9.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp161PE9.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp161PE9.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp161PE9.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp161PE9.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp161PE9.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp161PE9.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp161PE9.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp161PE9.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp161PE9.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp161PE9.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp161PE9.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp161PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp161PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp161PE9.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Maître de cérémonie"
cerfaFields['libelleProfession2']				= "maître de cérémonie."

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp161PE9.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp161PE9.signature.signature.dateSignature,
		autoriteHabilitee : 'Préfecture du département',
		demandeContexte : "Reconnaissance de qualifications professionnelles en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
		
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjHabilitation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Maître_de_cérémonie_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Maître de cérémonie  - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de maître de cérémonie ',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
