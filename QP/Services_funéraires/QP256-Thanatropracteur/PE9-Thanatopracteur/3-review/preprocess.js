var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp256PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp256PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp256PE9.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp256PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp256PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp256PE9.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp256PE9.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp256PE9.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp256PE9.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp256PE9.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp256PE9.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp256PE9.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp256PE9.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp256PE9.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp256PE9.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp256PE9.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp256PE9.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp256PE9.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp256PE9.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp256PE9.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp256PE9.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp256PE9.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp256PE9.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp256PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp256PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp256PE9.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession'] 			= "Thanatopracteur";

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp256PE9.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp256PE9.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjHabilitation);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Thanatopracteur_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Thanatopracteur - déclaration préalable en vue dune libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de thanatopracteur',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
