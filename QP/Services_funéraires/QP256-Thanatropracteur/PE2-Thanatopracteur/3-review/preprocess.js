var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp256PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp256PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp256PE2.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $qp256PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp256PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp256PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp256PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp256PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp256PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp256PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp256PE2.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp256PE2.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp256PE2.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp256PE2.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp256PE2.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp256PE2.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp256PE2.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp256PE2.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp256PE2.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp256PE2.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp256PE2.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp256PE2.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp256PE2.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp256PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp256PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp256PE2.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Thanatopracteur"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp256PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp256PE2.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('thanatopracteur_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Thanatopracteur - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de thanatopracteur',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
