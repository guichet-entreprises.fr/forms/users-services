var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp096PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp096PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp096PE8.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp096PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp096PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp096PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp096PE8.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp096PE8.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp096PE8.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp096PE8.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp096PE8.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp096PE8.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp096PE8.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp096PE8.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp096PE8.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp096PE8.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp096PE8.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp096PE8.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp096PE8.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp096PE8.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp096PE8.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp096PE8.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp096PE8.signature.signature.lieuSignature + ", le ";
cerfaFields['civiliteNomPrenomSignature']	= $qp096PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp096PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp096PE8.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']			= "Gestionnaire des établissements funéraires"
cerfaFields['libelleProfession2']			= "gestionnaire des établissements funéraires."

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp096PE8.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp096PE8.signature.signature.dateSignature,
		autoriteHabilitee : 'Prefecture du département',
		demandeContexte : "Reconnaissance de qualifications professionnelles en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
		
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Gestionnaire_des_etablissements_funeraires_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Gestionnaire des établissements funéraires - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de Gestionnaire des établissements funéraires.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
