var cerfaFields = {};

var civNomPrenom = $qp082PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp082PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp082PE7.etatCivil.identificationDeclarant.prenomDeclarant;
/* var region = $qp082PE7.signatureGroup.signature.regionExercice; */

//Etat civil
cerfaFields['civiliteNomPrenom']          	 	= $qp082PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp082PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp082PE7.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  				= $qp082PE7.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp082PE7.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp082PE7.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']             		= $qp082PE7.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//Adresse personnelle
cerfaFields['adresse'] 							= $qp082PE7.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp082PE7.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null ? ', ' + $qp082PE7.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp082PE7.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp082PE7.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant + ' ' : '') + $qp082PE7.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ', ' + $qp082PE7.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']          			= $qp082PE7.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']    				= $qp082PE7.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']          				= $qp082PE7.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;

//Signature
cerfaFields['date']                				= $qp082PE7.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp082PE7.signature.signature.signature;
cerfaFields['lieuSignature']                    = $qp082PE7.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = $qp082PE7.etatCivil.identificationDeclarant.civilite + ' ' + $qp082PE7.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp082PE7.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']				= "Conseiller funéraire"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp082PE7.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp082PE7.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/courrier_libre_LE_V4.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);

var finalDocItem = finalDoc.save('Conseiller_funeraire_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Conseiller funéraire - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de conseiller funéraire.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});