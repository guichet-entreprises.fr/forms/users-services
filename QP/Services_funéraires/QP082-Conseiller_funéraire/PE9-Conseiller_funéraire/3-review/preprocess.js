var cerfaFields = {};

var civNomPrenom = $qp082PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp082PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp082PE9.etatCivil.identificationDeclarant.prenomDeclarant;
/* var region = $qp082PE9.signatureGroup.signature.regionExercice; */

//Etat civil
cerfaFields['civiliteNomPrenom']          	 	= $qp082PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp082PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp082PE9.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  				= $qp082PE9.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp082PE9.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp082PE9.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']             		= $qp082PE9.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//Adresse personnelle
cerfaFields['adresse'] 							= $qp082PE9.adressePersonnelle.adressePersonnelle1.numeroLibelleAdresseDeclarant + ($qp082PE9.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant != null ? ', ' + $qp082PE9.adressePersonnelle.adressePersonnelle1.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp082PE9.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant != null ? $qp082PE9.adressePersonnelle.adressePersonnelle1.codePostalAdresseDeclarant + ' ' : '') + $qp082PE9.adressePersonnelle.adressePersonnelle1.villeAdresseDeclarant + ', ' + $qp082PE9.adressePersonnelle.adressePersonnelle1.paysAdresseDeclarant;
cerfaFields['telephoneFixe']          			= $qp082PE9.adressePersonnelle.adressePersonnelle1.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']    				= $qp082PE9.adressePersonnelle.adressePersonnelle1.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']          				= $qp082PE9.adressePersonnelle.adressePersonnelle1.mailAdresseDeclarant;

//Signature
cerfaFields['date']                				= $qp082PE9.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp082PE9.signature.signature.signature;
cerfaFields['lieuSignature']                    = $qp082PE9.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']       = $qp082PE9.etatCivil.identificationDeclarant.civilite + ' ' + $qp082PE9.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp082PE9.etatCivil.identificationDeclarant.prenomDeclarant;


cerfaFields['libelleProfession']				= "Conseiller funéraire"
cerfaFields['libelleProfession2']				= "conseiller funéraire."


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp082PE9.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp082PE9.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/courrier_libre_LPS_V3.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);

var finalDocItem = finalDoc.save('Conseiller_funeraire_LPS.pdf');


return spec.create({
    id : 'review',
   label : 'Conseiller funéraire - déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de conseiller funéraire.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});