var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp211PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp211PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp211PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	    = $qp211PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp211PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp211PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp211PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp211PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp211PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp211PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp211PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp211PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp211PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp211PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp211PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp211PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp211PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp211PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp211PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp211PE5.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp211PE5.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp211PE5.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp211PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp211PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp211PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp211PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']	            = "Personnel de pompes funèbres"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp211PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp211PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Personnel_De_Pompes_Funèbres_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Personnel de pompes funèbres - demande de reconnaissance de qualifications professionnnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications en vue d\'un libre établissement en tant que personnel de pompes funèbres',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
