var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp211PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp211PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp211PE8.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp211PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp211PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp211PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp211PE8.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp211PE8.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp211PE8.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp211PE8.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp211PE8.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp211PE8.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp211PE8.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp211PE8.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp211PE8.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp211PE8.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp211PE8.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp211PE8.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp211PE8.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp211PE8.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp211PE8.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp211PE8.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp211PE8.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp211PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp211PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp211PE8.etatCivil.identificationDeclarant.prenomDeclarant;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp211PE8.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp211PE8.signature.signature.dateSignature,
		autoriteHabilitee : 'Prefecture du département',
		demandeContexte : "Reconnaissance de qualifications professionnelles en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
		
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}
/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjHabilitation);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Personnel_pompes_funebres_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Personnel de pompes funèbres - déclaration préalable en vue dune libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de personnel de pompes funèbres',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
