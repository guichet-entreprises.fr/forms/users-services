var cerfaFields = {};

var civNomPrenom = $qp047PE8.identiteGroup.identite.nomDeclarant + ' ' + $qp047PE8.identiteGroup.identite.prenomsDeclarant;

//etatCivil
cerfaFields['nomDeclarant']	               = $qp047PE8.identiteGroup.identite.nomDeclarant 
+ ($qp047PE8.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp047PE8.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp047PE8.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp047PE8.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp047PE8.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp047PE8.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp047PE8.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp047PE8.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp047PE8.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp047PE8.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp047PE8.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp047PE8.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp047PE8.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp047PE8.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp047PE8.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp047PE8.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp047PE8.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp047PE8.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp047PE8.signatureGroup.signature.signatureElectronique;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp047PE8.signatureGroup.signature.dateDeclaration,
		autoriteHabilitee :"Chambre des métiers et de l’artisanat ",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/boucher LPS.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Boucher_LPS.pdf');

/*
 * Persistance des données obtenues
 */


return spec.create({
    id : 'review',
    label : 'Boucher - Déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du dossier',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de boucher',
					description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
					type : 'FileReadOnly',
					value : [ finalDocItem ]
			}) ]
	}) ]
});