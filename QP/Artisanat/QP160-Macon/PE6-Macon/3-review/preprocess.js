var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp160PE6.identiteGroup.identite.nomDeclarant 
+ ($qp160PE6.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp160PE6.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp160PE6.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp160PE6.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp160PE6.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp160PE6.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp160PE6.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp160PE6.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp160PE6.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp160PE6.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp160PE6.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp160PE6.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp160PE6.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp160PE6.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp160PE6.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp160PE6.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp160PE6.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp160PE6.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp160PE6.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/macon LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Maçon.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Maçon - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de maçon',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});