var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp069PE2.identiteGroup.identite.nomDeclarant + ($qp069PE2.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp069PE2.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp069PE2.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp069PE2.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp069PE2.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp069PE2.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp069PE2.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant +', '
+($qp069PE2.coordonneesGroup.coordonnees.complementAdresse != null ? $qp069PE2.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp069PE2.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp069PE2.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = ($qp069PE2.coordonneesGroup.coordonnees.codePostalDeclarant != null ? $qp069PE2.coordonneesGroup.coordonnees.codePostalDeclarant : ' ' )
cerfaFields['telephoneMobileDeclarant']    = $qp069PE2.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp069PE2.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp069PE2.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp069PE2.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp069PE2.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp069PE2.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp069PE2.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/climaticien LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Climaticien.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Climaticien - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de climaticien : ',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});