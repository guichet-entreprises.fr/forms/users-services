var cerfaFields = {};
var civNomPrenom = $qp244PE10.etatCivil.identificationDeclarant.civilite + ' ' + $qp244PE10.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp244PE10.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp244PE10.etatCivil.identificationDeclarant.civilite + ' ' + $qp244PE10.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp244PE10.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp244PE10.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp244PE10.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp244PE10.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp244PE10.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp244PE10.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp244PE10.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp244PE10.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp244PE10.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp244PE10.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp244PE10.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp244PE10.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp244PE10.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp244PE10.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp244PE10.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp244PE10.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp244PE10.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp244PE10.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp244PE10.etatCivil.identificationDeclarant.civilite + ' ' + $qp244PE10.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp244PE10.etatCivil.identificationDeclarant.prenomDeclarant;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp055PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
/*
var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp055PE1.signature.signature.dateSignature,
		adresseAC1: adAC1,
		adresseAC2: adAC2,
		adresseAC3: adAC3,
		adresseAC4: adAC4
	});

finalDoc.append(accompDoc.save('courrier.pdf'));
*/
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Reparateur_motocycles_LPS.pdf') //
	.apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = cerfaDoc.save('Declaration_LPS_Reparateur_motocycles.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Réparateur de motocycles - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
 			label : 'Déclaration préalable en vue d\'une libre prestation de services pour la profession de réparateur de motocycles',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
