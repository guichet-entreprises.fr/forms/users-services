var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp054PE9.identiteGroup.identite.nomDeclarant + ($qp054PE9.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp054PE9.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp054PE9.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp054PE9.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp054PE9.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp054PE9.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp054PE9.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp054PE9.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp054PE9.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp054PE9.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp054PE9.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp054PE9.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp054PE9.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp054PE9.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp054PE9.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp054PE9.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp054PE9.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp054PE9.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp054PE9.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/chauffagiste LPS.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LPS - Chauffagiste.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Chauffagiste - Demande de RQP en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de chauffagiste',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});