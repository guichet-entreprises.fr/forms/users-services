var cerfaFields = {};

//etatCivil
cerfaFields['nomPrenomDeclarant']	       = $qp054PE10.identiteGroup.identite.prenomsDeclarant+' ' +$qp054PE10.identiteGroup.identite.nomDeclarant;
cerfaFields['dateNaissance']			   = $qp054PE10.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['villePaysNaissance']		   = $qp054PE10.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp054PE10.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp054PE10.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant;
+($qp054PE10.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp054PE10.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['villeDeclarant']  		       = ($qp054PE10.coordonneesGroup.coordonnees.codePostalDeclarant != null ? $qp054PE10.coordonneesGroup.coordonnees.codePostalDeclarant : ' ')
+' ' + $qp054PE10.coordonneesGroup.coordonnees.communeResidenceDeclarant;
cerfaFields['paysDeclarant']			   = $qp054PE10.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['telephoneMobileDeclarant']    = $qp054PE10.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp054PE10.coordonneesGroup.coordonnees.adresseCourriel;

//signature
cerfaFields['dateDeclaration']             = $qp054PE10.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp054PE10.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp054PE10.signatureGroup.signature.signatureElectronique;
cerfaFields['prenomNomDeclarant']		   = $qp054PE10.identiteGroup.identite.prenomsDeclarant+' '+ $qp054PE10.identiteGroup.identite.nomDeclarant;

var cerfa = pdf.create('models/chauffagiste declaration libre LPS.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LPS - Chauffagiste.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Chauffagiste - Demande de RQP en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de chauffagiste',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});