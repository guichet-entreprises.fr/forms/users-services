var cerfaFields = {};

var civNomPrenom = $qp070PE5.identiteGroup.identite.civilite + ' ' + $qp070PE5.identiteGroup.identite.nomDeclarant + ' ' + $qp070PE5.identiteGroup.identite.prenomsDeclarant;

//etatCivil
cerfaFields['nomDeclarant']	               = $qp070PE5.identiteGroup.identite.nomDeclarant + ($qp070PE5.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp070PE5.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp070PE5.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp070PE5.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp070PE5.identiteGroup.identite.villeNaissanceDeclarant + ', ' + $qp070PE5.identiteGroup.identite.paysNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp070PE5.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp070PE5.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
											+($qp070PE5.coordonneesGroup.coordonnees.complementAdresse != null ? ', '
											+ $qp070PE5.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp070PE5.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp070PE5.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp070PE5.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp070PE5.coordonneesGroup.coordonnees.telephoneMobileDeclarant;

var adresse = $qp070PE5.coordonneesGroup.coordonnees.adresseCourriel;
cerfaFields['indexEmail']              = '';
cerfaFields['domaineEmail']            = '';
if(adresse != null) {
var adresseSplite = adresse.split("@");
cerfaFields['indexEmail']              = adresseSplite[0];
cerfaFields['domaineEmail']            = adresseSplite[1];
}


//entreprise
cerfaFields['denominationSociale']   	   = $qp070PE5.entrepriseGroup.entreprise.denominationSociale;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = "Coiffeur à domicile";

//signature
cerfaFields['dateDeclaration']             = $qp070PE5.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp070PE5.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp070PE5.signatureGroup.signature.signatureElectronique;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp070PE5.signatureGroup.signature.dateDeclaration,
		autoriteHabilitee :"Chambre des métiers et de l'artisanat",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire de demande de reconnaissance de qualification professionnelle_Coiffeur à domicile.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('coiffeur_a_domicile_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Coiffeur à domicile - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de coiffeur à domicile',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
