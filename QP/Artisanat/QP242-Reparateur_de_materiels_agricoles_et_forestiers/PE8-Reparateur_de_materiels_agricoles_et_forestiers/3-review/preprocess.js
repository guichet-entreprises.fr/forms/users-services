var cerfaFields = {};

var civNomPrenom = $qp242PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp242PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp242PE8.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp242PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp242PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp242PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp242PE8.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp242PE8.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp242PE8.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp242PE8.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp242PE8.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp242PE8.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp242PE8.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp242PE8.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp242PE8.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp242PE8.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp242PE8.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp242PE8.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp242PE8.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp242PE8.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp242PE8.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp242PE8.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp242PE8.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp242PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp242PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp242PE8.etatCivil.identificationDeclarant.prenomDeclarant;


 /* Ajout du cerfa
 */
var finalDoc = nash.doc //
    .load('models/reparateur_materiels_agricoles_LPS.pdf') //
    .apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLPS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('reparateur_materiels_agricoles_et_forestiers_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Réparateur de matériels agricoles et forestiers - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de réparateur de matériels agricoles et forestiers.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.s',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});