var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']                = $qp242PE9.identiteGroup.identite.nomDeclarant 
+($qp242PE9.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp242PE9.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp242PE9.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp242PE9.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp242PE9.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp242PE9.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp242PE9.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp242PE9.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp242PE9.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp242PE9.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp242PE9.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp242PE9.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp242PE9.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp242PE9.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp242PE9.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp242PE9.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp242PE9.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp242PE9.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp242PE9.signatureGroup.signature.signatureElectronique;

/*
 * Ajout du cerfa
 */
var finalDoc = nash.doc //
    .load('models/reparateur_materiels_agricoles_LPS.pdf') //
    .apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjQualificationsProfessionnelles);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('reparateur_materiels_agricoles_et_forestiers_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Réparateur de matériels agricoles et forestiers - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de réparateur de matériels agricoles et forestiers.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.s',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});