var cerfaFields = {};

//etatCivil
cerfaFields['nomPrenomDeclarant']	       = $qp159PE8.identiteGroup.identite.prenomsDeclarant+' ' +$qp159PE8.identiteGroup.identite.nomDeclarant;
cerfaFields['dateNaissance']			   = $qp159PE8.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['villePaysNaissance']		   = $qp159PE8.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp159PE8.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp159PE8.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant;
+($qp159PE8.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp159PE8.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['villeDeclarant']  		       = ($qp159PE8.coordonneesGroup.coordonnees.codePostalDeclarant != null ? $qp159PE8.coordonneesGroup.coordonnees.codePostalDeclarant : ' ')
+' ' + $qp159PE8.coordonneesGroup.coordonnees.communeResidenceDeclarant;
cerfaFields['paysDeclarant']			   = $qp159PE8.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['telephoneMobileDeclarant']    = $qp159PE8.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp159PE8.coordonneesGroup.coordonnees.adresseCourriel;

//signature
cerfaFields['dateDeclaration']             = $qp159PE8.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp159PE8.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp159PE8.signatureGroup.signature.signatureElectronique;
cerfaFields['prenomNomDeclarant']		   = $qp159PE8.identiteGroup.identite.prenomsDeclarant+' '+ $qp159PE8.identiteGroup.identite.nomDeclarant;

var cerfa = pdf.create('models/installateur gaz libre LPS.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LPS - Installateur de réseaux de gaz.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Installateur de réseaux de gaz - Demande de RQP en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'installateur de réseaux de gaz',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});