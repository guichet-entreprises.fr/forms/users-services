var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp159PE2.identiteGroup.identite.nomDeclarant 
+($qp159PE2.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp159PE2.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp159PE2.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp159PE2.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp159PE2.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp159PE2.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp159PE2.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp159PE2.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp159PE2.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp159PE2.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp159PE2.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp159PE2.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp159PE2.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp159PE2.coordonneesGroup.coordonnees.adresseCourriel;


//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp159PE2.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp159PE2.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp159PE2.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp159PE2.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp159PE2.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/installateur gaz LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Installateur de réseaux de gaz.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Installateur de réseaux de gaz - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'installateur de réseaux de gaz',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});