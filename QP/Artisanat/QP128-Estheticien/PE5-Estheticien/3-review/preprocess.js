var cerfaFields = {};

var civNomPrenom = $qp128PE5.identiteGroup.identite.civilite + ' ' + $qp128PE5.identiteGroup.identite.nomDeclarant + ' ' + $qp128PE5.identiteGroup.identite.prenomsDeclarant;
var region = $qp128PE5.signatureGroup.signature.regionExercice;

//etatCivil
cerfaFields['nomDeclarant']	               = $qp128PE5.identiteGroup.identite.nomDeclarant 
											+($qp128PE5.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp128PE5.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp128PE5.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp128PE5.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp128PE5.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp128PE5.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp128PE5.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
											+($qp128PE5.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp128PE5.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp128PE5.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp128PE5.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp128PE5.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp128PE5.coordonneesGroup.coordonnees.telephoneMobileDeclarant;

var adresse = $qp128PE5.coordonneesGroup.coordonnees.adresseCourriel;
cerfaFields['indexEmail']              = '';
cerfaFields['domaineEmail']            = '';
if(adresse != null) {
var adresseSplite = adresse.split("@");
cerfaFields['indexEmail']              = adresseSplite[0];
cerfaFields['domaineEmail']            = adresseSplite[1];
}



//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp128PE5.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = "Esthéticien"

//signature
cerfaFields['dateDeclaration']             = $qp128PE5.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp128PE5.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp128PE5.signatureGroup.signature.signatureElectronique;

cerfaFields['libelleProfession']								  	  = "Esthéticien"

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp128PE5.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp128PE5.signatureGroup.signature.dateDeclaration,
		autoriteHabilitee :"Chambre des métiers et de l'artisanat",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
	
var cerfaDoc = nash.doc //
    .load('models/Formulaire artisanat.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjSanctions);
appendPj($attachmentPreprocess.attachmentPreprocess.pjActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);


var finalDocItem = finalDoc.save('Estheticien_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Esthéticien - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'esthéticien.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});