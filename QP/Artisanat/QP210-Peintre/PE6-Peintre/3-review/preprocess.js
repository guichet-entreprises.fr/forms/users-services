var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp210PE6.identiteGroup.identite.nomDeclarant 
+ ($qp210PE6.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp210PE6.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp210PE6.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp210PE6.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp210PE6.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp210PE6.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp210PE6.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp210PE6.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp210PE6.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp210PE6.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp210PE6.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp210PE6.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp210PE6.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp210PE6.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp210PE6.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp210PE6.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp210PE6.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp210PE6.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp210PE6.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/peintre LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Peintre.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Peintre - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de peintre',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});