var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']                = $qp177PE7.identiteGroup.identite.nomDeclarant 
+ ($qp177PE7.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp177PE7.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp177PE7.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp177PE7.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp177PE7.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp177PE7.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp177PE7.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp177PE7.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp177PE7.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp177PE7.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp177PE7.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp177PE7.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp177PE7.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp177PE7.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp177PE7.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp177PE7.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp177PE7.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp177PE7.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp177PE7.signatureGroup.signature.signatureElectronique;


 /* Ajout du cerfa
 */
var finalDoc = nash.doc //
    .load('models/menuisier_charpentier_LPS.pdf') //
    .apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjQualificationsProfessionnelles);
/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('menuisier_charpentier_LPS.pdf');

/*
 * Persistance des données obtenues
 */
return spec.create({
    id : 'review',
    label : 'Menuisier charpentier - déclaration préalable en vue d’une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de menuisier charpentier.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});