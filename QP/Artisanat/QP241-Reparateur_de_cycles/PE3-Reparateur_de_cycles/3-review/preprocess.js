var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp241PE3.identiteGroup.identite.nomDeclarant 
+($qp241PE3.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp241PE3.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp241PE3.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp241PE3.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp241PE3.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp241PE3.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp241PE3.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp241PE3.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp241PE3.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp241PE3.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp241PE3.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp241PE3.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp241PE3.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp241PE3.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp241PE3.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp241PE3.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp241PE3.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp241PE3.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp241PE3.signatureGroup.signature.signatureElectronique;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp055PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
/*
var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp055PE1.signature.signature.dateSignature,
		adresseAC1: adAC1,
		adresseAC2: adAC2,
		adresseAC3: adAC3,
		adresseAC4: adAC4
	});

finalDoc.append(accompDoc.save('courrier.pdf'));
*/
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Reparateur_cycles_LE.pdf') //
	.apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = cerfaDoc.save('Declaration_LE_Reparateur_cycles.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Réparateur de cycles - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
 			label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour la profession de réparateur de cycles',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});

