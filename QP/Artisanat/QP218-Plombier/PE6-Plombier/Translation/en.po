msgid "Personnel des installations de combustion supérieures à 400 KW - Demande de RQP en vue d'un libre établissement"
msgstr "Personnel of combustion plants above 400 KW - Request for a RQP for a free establishment"

msgid "Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de personnel des installations de combustion supérieures à 400 kW"
msgstr "Application form for the recognition of professional qualifications for the personnel occupation of combustion plants exceeding 400 kW"



msgid "Demande de reconnaissance de qualifications professionnelles"
msgstr "Application for recognition of professional qualifications"

msgid "Etat civil"
msgstr "Civil status"

msgid "Nom de naissance :"
msgstr "Birth name :"

msgid "Il s'agit généralement du nom marital et il peut s'agir également du nom d'un des parents. Ce dernier doit figurer sur le justificatif d'identité."
msgstr "This is usually the marital name and may also be the name of one of the parents. The latter must appear on the proof of identity."

msgid "Nom de jeune fille :"
msgstr "Maiden name :"

msgid "Il s'agit du nom qui  a été donné à la naissance tel qu'il apparaît sur le justificatif d'identité."
msgstr "This is the name that was given at birth as it appears on the proof of identity."

msgid "Prénom(s) :"
msgstr "First name (s):"

msgid "Il s'agit de l'ensemble des prénoms figurant sur le justificatif d'identité. L'ordre indiqué sur le justificatif d'identité doit être respecté et les prénoms doivent êtres séparés par une virgule."
msgstr "This is the set of forenames on the credentials. The order shown on the proof of identity must be respected and the first names must be separated by a comma."

msgid "Date de naissance :"
msgstr "Birth date:"

msgid "Lieu de naissance :"
msgstr "Place of birth:"

msgid "Il s'agit d'indiquer la localité de naissance."
msgstr "This is to indicate the place of birth."

msgid "Nationalité :"
msgstr "Nationality:"

msgid "Il s'agit d'indiquer en toutes lettres la nationalité."
msgstr "It is a question of indicating in full nationality."

msgid "Coordonnées"
msgstr "Contact information"

msgid "Numéro et libellé de votre voie :"
msgstr "Number and label of your track:"

msgid "Il convient de renseigner l'adresse du domicile personnel."
msgstr "The home address must be filled in."

msgid "Complément d'adresse :"
msgstr "Additional address:"

msgid "Il convient d'indiquer ici tout complément d'adresse permettant de l'identifier tels que : lieu-dit, BP, CS, bâtiment, escalier, étage, appartement, etc."
msgstr "Please indicate here any additional addresses that can be identified, such as: locality, BP, CS, building, staircase, floor, apartment, etc."

msgid "Commune ou ville :"
msgstr "City or town:"

msgid "Il convient d'indiquer la localité du domicile personnel."
msgstr "The location of the personal residence should be indicated."

msgid "Code postal :"
msgstr "Postal code :"

msgid "Il convient d'indiquer ici en toutes lettres le pays de résidence."
msgstr "The country of residence should be indicated in full."

msgid "Téléphone mobile :"
msgstr "Mobile phone :"

msgid "Il convient de sélectionner le pays d'attribution du numéro et de renseigner le numéro de téléphone mobile."
msgstr "It is necessary to select the country of assignment of the number and to fill in the mobile phone number."

msgid "Courriel :"
msgstr "E-mail address:"

msgid "Il convient de renseigner une adresse electronique valide."
msgstr "A valid e-mail address should be filled in."

msgid "Entreprise"
msgstr "Business"

msgid "Nature juridique :"
msgstr "Juridical nature :"

msgid "Il convient ici de spécifier la forme juridique de l'entreprise (ex: SA, SARL, EI...)"
msgstr "It is appropriate here to specify the legal form of the company (eg SA, SARL, EI ...)"

msgid "Diplôme(s)"
msgstr "Degree (s)"

msgid "Diplôme de la profession considérée :"
msgstr "Diploma of the profession in question:"

msgid "Il convient de renseigner le diplôme relatif à la profession concernée par cette demande d'autorisation."
msgstr "The diploma for the profession concerned by this application for authorization must be filled in."

msgid "Signature"
msgstr "Signature"

msgid "Fait le :"
msgstr "Date:"

msgid "Il convient d'indiquer ici la date du jour."
msgstr "The current date should be entered here."

msgid "A :"
msgstr "Do at:"

msgid "Il convient d'indiquer ici votre lieu actuel."
msgstr "Please indicate here your current location."

msgid "Je déclare sur l'honneur l'exactitude des informations de la formalité et signe la présente déclaration."
msgstr "I declare on my honor the accuracy of the information of the formality and sign this declaration."




msgid "Voici le formulaire obtenu à partir des données saisies :"
msgstr "Here is the form obtained from the data entered:"



msgid "Remerciements"
msgstr "Greetings"

msgid "Merci d'avoir traité cette formalité sur le site www.guichet-qualifications.fr. Votre dossier sera transmis à l'autorité compétente pour traitement."
msgstr "Thank you for having processed this formality on the site www.guichet-qualifications.fr. Your file will be forwarded to the competent authority for processing."





msgid "Extrait d'acte de naissance, fiche d'état civil, carte d'identité ou passeport, recto verso si nécessaire et en cours de validité."
msgstr "Extract of birth certificate, civil status card, identity card or passport, double-sided if necessary and valid."

msgid "Copie des attestations des autorités ayant délivré le titre de formation, spécifiant le niveau de la formation et, année par année, le détail et le volume horaire des enseignements suivis ainsi que le contenu et la durée des stages validés."
msgstr "Copy of the certificates of the authorities which issued the training certificate, specifying the level of the training and, year by year, the details and the hourly volume of the courses followed and the content and duration of the validated training periods."

msgid "Copie du titre de formation permettant l’exercice de la profession dans le pays d’obtention."
msgstr "Copy of the title of training permitting the practice of the profession in the country of obtaining."

msgid "Copie de toute preuve attestant des qualifications professionnelles."
msgstr "Copy of any evidence of professional qualifications."

msgid "Copie de tout document permettant de justifier du mode d'exercice ou de l'activité."
msgstr "Copy of any document justifying the mode of exercise or activity."

msgid "Contrats de travail, feuilles de paie, certificats de travail ou attestations d'immatriculation à un répertoire comparable au RM ou au RCS ou à l'Urssaf."
msgstr "Contracts of employment, pay slips, work certificates or certificates of registration in a directory comparable to the RM or the SCR or Urssaf."

msgid "Copie de toute pièce justifiant que le déclarant a exercé l'activité dans cet Etat pendant au moins dix ans à temps complet ou une durée équivalente au cours des dix dernières années."
msgstr "Copy of any document proving that the declarant has carried on the activity in that State for at least ten years on a full-time basis or for an equivalent period during the last ten years."








msgid ""
msgstr ""

msgid ""
msgstr ""


msgid ""
msgstr ""

msgid ""
msgstr ""

msgid ""
msgstr ""
