var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp238PE4.identiteGroup.identite.nomDeclarant 
+($qp238PE4.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp238PE4.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp238PE4.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp238PE4.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp238PE4.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp238PE4.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp238PE4.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp238PE4.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp238PE4.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp238PE4.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp238PE4.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp238PE4.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp238PE4.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp238PE4.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp238PE4.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp238PE4.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp238PE4.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp238PE4.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp238PE4.signatureGroup.signature.signatureElectronique;


 /* Ajout du cerfa
 */
var finalDoc = nash.doc //
	.load('models/ramoneur_LE.pdf') //
	.apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveInstallation);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Ramoneur_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ramoneur - Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de ramoneur.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.s',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});