var cerfaFields = {};

//etatCivil
cerfaFields['nomPrenomDeclarant']	       = $qp238PE10.identiteGroup.identite.prenomsDeclarant+' ' +$qp238PE10.identiteGroup.identite.nomDeclarant;
cerfaFields['dateNaissance']			   = $qp238PE10.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['villePaysNaissance']		   = $qp238PE10.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp238PE10.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp238PE10.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant;
+($qp238PE10.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp238PE10.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['villeDeclarant']  		       = ($qp238PE10.coordonneesGroup.coordonnees.codePostalDeclarant != null ? $qp238PE10.coordonneesGroup.coordonnees.codePostalDeclarant : ' ')
+' ' + $qp238PE10.coordonneesGroup.coordonnees.communeResidenceDeclarant;
cerfaFields['paysDeclarant']			   = $qp238PE10.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['telephoneMobileDeclarant']    = $qp238PE10.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp238PE10.coordonneesGroup.coordonnees.adresseCourriel;

//signature
cerfaFields['dateDeclaration']             = $qp238PE10.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp238PE10.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp238PE10.signatureGroup.signature.signatureElectronique;
cerfaFields['prenomNomDeclarant']		   = $qp238PE10.identiteGroup.identite.prenomsDeclarant+' '+ $qp238PE10.identiteGroup.identite.nomDeclarant;


 /* Ajout du cerfa
 */
var finalDoc = nash.doc //
	.load('models/ramoneur_LPS.pdf') //
	.apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Ramoneur_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ramoneur - Déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de ramoneur.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});