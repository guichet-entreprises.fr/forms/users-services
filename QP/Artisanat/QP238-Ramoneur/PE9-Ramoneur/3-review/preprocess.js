var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp238PE9.identiteGroup.identite.nomDeclarant 
+($qp238PE9.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp238PE9.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp238PE9.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp238PE9.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp238PE9.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp238PE9.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp238PE9.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp238PE9.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp238PE9.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp238PE9.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp238PE9.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp238PE9.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp238PE9.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp238PE9.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp238PE9.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp238PE9.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp238PE9.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp238PE9.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp238PE9.signatureGroup.signature.signatureElectronique;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant


var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom

	});
 
 

 * Chargement du courrier d'accompagnement à destination de l'AC


var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
	.apply({
		date: $qpxxxPEx.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));


 * Ajout du cerfa
 */
var finalDoc = nash.doc //
	.load('models/ramoneur_LPS.pdf') //
	.apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjQualificationsProfessionnelles);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Déclaration_LPS_Ramoneur.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Ramoneur - Déclaration préalable en vue d\'une libre prestation de service',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de ramoneur.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
