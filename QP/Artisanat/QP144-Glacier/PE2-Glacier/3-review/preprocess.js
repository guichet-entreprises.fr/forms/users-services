var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp144PE2.identiteGroup.identite.nomDeclarant + ($qp144PE2.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp144PE2.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp144PE2.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp144PE2.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp144PE2.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp144PE2.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp144PE2.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant +', '
+($qp144PE2.coordonneesGroup.coordonnees.complementAdresse != null ? $qp144PE2.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp144PE2.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp144PE2.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = ($qp144PE2.coordonneesGroup.coordonnees.codePostalDeclarant != null ? $qp144PE2.coordonneesGroup.coordonnees.codePostalDeclarant : ' ' )
cerfaFields['telephoneMobileDeclarant']    = $qp144PE2.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp144PE2.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp144PE2.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp144PE2.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp144PE2.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp144PE2.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp144PE2.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/glacier LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Glacier.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Glacier - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de glacier : ',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});