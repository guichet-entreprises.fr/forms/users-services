var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp144PE7.identiteGroup.identite.nomDeclarant + ($qp144PE7.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp144PE7.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp144PE7.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp144PE7.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp144PE7.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp144PE7.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp144PE7.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp144PE7.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp144PE7.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp144PE7.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp144PE7.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp144PE7.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp144PE7.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp144PE7.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp144PE7.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp144PE7.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp144PE7.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp144PE7.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp144PE7.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/glacier LPS.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LPS - Glacier.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Glacier - Demande de RQP en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de glacier',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});