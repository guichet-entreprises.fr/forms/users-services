var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp169PE7.identiteGroup.identite.nomDeclarant 
+ ($qp169PE7.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp169PE7.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp169PE7.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp169PE7.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp169PE7.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp169PE7.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp169PE7.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp169PE7.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp169PE7.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp169PE7.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp169PE7.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp169PE7.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp169PE7.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp169PE7.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp169PE7.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp169PE7.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp169PE7.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp169PE7.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp169PE7.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/marechal-ferrant LPS.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LPS - Maréchal-ferrant.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Maréchal-ferrant - Demande de RQP en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de maréchal-ferrant',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});