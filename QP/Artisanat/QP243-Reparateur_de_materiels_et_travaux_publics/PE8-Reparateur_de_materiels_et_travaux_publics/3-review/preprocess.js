var cerfaFields = {};

var civNomPrenom = $qp243PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp243PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp243PE8.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']          	= $qp243PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp243PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp243PE8.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp243PE8.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp243PE8.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp243PE8.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp243PE8.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp243PE8.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp243PE8.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp243PE8.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp243PE8.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp243PE8.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp243PE8.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp243PE8.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp243PE8.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp243PE8.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp243PE8.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp243PE8.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp243PE8.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp243PE8.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp243PE8.etatCivil.identificationDeclarant.civilite + ' ' + $qp243PE8.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp243PE8.etatCivil.identificationDeclarant.prenomDeclarant;

 /* Ajout du cerfa
 */
var finalDoc = nash.doc //
    .load('models/reparateur_materiels_travaux_publics_LPS.pdf') //
    .apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLPS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('reparateur_materiels_travaux_publics_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Réparateur de matériels et travaux publics  - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de réparateur de matériels et travaux publics.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.s',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});