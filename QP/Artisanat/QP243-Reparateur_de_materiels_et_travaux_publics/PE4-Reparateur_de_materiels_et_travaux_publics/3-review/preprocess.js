var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']                = $qp243PE4.identiteGroup.identite.nomDeclarant 
+($qp243PE4.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp243PE4.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp243PE4.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp243PE4.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp243PE4.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp243PE4.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp243PE4.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp243PE4.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp243PE4.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp243PE4.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp243PE4.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp243PE4.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp243PE4.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp243PE4.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp243PE4.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp243PE4.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp243PE4.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp243PE4.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp243PE4.signatureGroup.signature.signatureElectronique;


 /* Ajout du cerfa
 */
var finalDoc = nash.doc //
    .load('models/reparateur_materiels_travaux_publics_LE.pdf') //
    .apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('reparateur_materiels_travaux_publics_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Réparateur de matériels et travaux publics - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de réparateur de matériels et travaux publics.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});