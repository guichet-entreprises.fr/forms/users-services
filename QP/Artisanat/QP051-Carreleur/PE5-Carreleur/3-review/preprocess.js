var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp051PE5.identiteGroup.identite.nomDeclarant 
+($qp051PE5.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp051PE5.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp051PE5.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp051PE5.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp051PE5.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp051PE5.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp051PE5.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp051PE5.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp051PE5.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp051PE5.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp051PE5.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp051PE5.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp051PE5.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp051PE5.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp051PE5.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp051PE5.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp051PE5.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp051PE5.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp051PE5.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/carreleur LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Carreleur.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Carreleur - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de carreleur',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});