var cerfaFields = {};

var civNomPrenom = $qp071PE1.identiteGroup.identite.civilite + ' ' + $qp071PE1.identiteGroup.identite.nomDeclarant + ' ' + $qp071PE1.identiteGroup.identite.prenomsDeclarant;

//etatCivil
cerfaFields['nomDeclarant']	               = $qp071PE1.identiteGroup.identite.nomDeclarant + ($qp071PE1.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp071PE1.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp071PE1.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp071PE1.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp071PE1.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp071PE1.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp071PE1.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp071PE1.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp071PE1.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp071PE1.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp071PE1.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp071PE1.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp071PE1.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp071PE1.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['denominationSociale']   	   = $qp071PE1.entrepriseGroup.entreprise.denominationSociale;

cerfaFields['natureJuridiqueEntreprise']   = $qp071PE1.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//signature
cerfaFields['dateDeclaration']             = $qp071PE1.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp071PE1.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp071PE1.signatureGroup.signature.signatureElectronique;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp071PE1.signatureGroup.signature.dateDeclaration,
		autoriteHabilitee :"Chambre des métiers et de l'artisanat",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Formulaire de demande d\'attestation de reconnaissance de qualification professionnelle.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitresComplementaires);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPiecesUtiles);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('coiffeur_salon_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Coiffeur en salon - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de coiffeur en salon',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
