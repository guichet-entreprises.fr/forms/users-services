var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp053PE2.identiteGroup.identite.nomDeclarant + ($qp053PE2.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp053PE2.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp053PE2.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp053PE2.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp053PE2.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp053PE2.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp053PE2.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant +', '
+($qp053PE2.coordonneesGroup.coordonnees.complementAdresse != null ? $qp053PE2.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp053PE2.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp053PE2.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = ($qp053PE2.coordonneesGroup.coordonnees.codePostalDeclarant != null ? $qp053PE2.coordonneesGroup.coordonnees.codePostalDeclarant : ' ' )
cerfaFields['telephoneMobileDeclarant']    = $qp053PE2.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp053PE2.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp053PE2.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp053PE2.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp053PE2.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp053PE2.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp053PE2.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/charcutier LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Charcutier.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Charcutier - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de charcutier : ',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});