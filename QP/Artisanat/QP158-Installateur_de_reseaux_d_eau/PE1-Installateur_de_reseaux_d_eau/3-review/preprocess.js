var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp158PE1.identiteGroup.identite.nomDeclarant 
+($qp158PE1.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp158PE1.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp158PE1.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp158PE1.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp158PE1.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp158PE1.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp158PE1.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp158PE1.coordonneesGroup.coordonnees.complementAdresse != null ? ', '+ $qp158PE1.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp158PE1.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp158PE1.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp158PE1.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp158PE1.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp158PE1.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp158PE1.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp158PE1.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp158PE1.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp158PE1.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp158PE1.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/installateur de réseaux d’eau LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Installateur de réseaux d’eau.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Installateur de réseaux d’eau - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'installateur de réseaux d’eau',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});