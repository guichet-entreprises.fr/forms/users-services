var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp240PE5.identiteGroup.identite.nomDeclarant 
+($qp240PE5.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp240PE5.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp240PE5.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp240PE5.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp240PE5.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp240PE5.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp240PE5.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp240PE5.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp240PE5.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp240PE5.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp240PE5.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp240PE5.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp240PE5.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp240PE5.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp240PE5.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp240PE5.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp240PE5.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp240PE5.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp240PE5.signatureGroup.signature.signatureElectronique;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp055PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
/*
var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp055PE1.signature.signature.dateSignature,
		adresseAC1: adAC1,
		adresseAC2: adAC2,
		adresseAC3: adAC3,
		adresseAC4: adAC4
	});

finalDoc.append(accompDoc.save('courrier.pdf'));
*/
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Reparateur_automobile_LE.pdf') //
	.apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatifExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = cerfaDoc.save('Declaration_LE_Reparateur_automobiles.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Réparateur d\'automobiles - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
 			label : 'Formulaire de demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour la profession de réparateur d\'automobiles',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
