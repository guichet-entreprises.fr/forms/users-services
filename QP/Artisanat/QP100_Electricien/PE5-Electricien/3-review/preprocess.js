var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp100PE5.identiteGroup.identite.nomDeclarant 
+($qp100PE5.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp100PE5.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['dateNaissanceDeclarant']      = $qp100PE5.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp100PE5.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp100PE5.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp100PE5.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp0100PE5.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp100PE5.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp100PE5.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp100PE5.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp100PE5.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp100PE5.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp100PE5.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp100PE5.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp100PE5.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp100PE5.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp100PE5.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp100PE5.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/electricien LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Electricien.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Electricien - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'électricien',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});