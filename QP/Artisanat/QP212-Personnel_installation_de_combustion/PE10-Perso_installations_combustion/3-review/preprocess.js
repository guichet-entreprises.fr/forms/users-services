var cerfaFields = {};

//etatCivil
cerfaFields['nomPrenomDeclarant']	       = $qp158PE10.identiteGroup.identite.prenomsDeclarant+' ' +$qp158PE10.identiteGroup.identite.nomDeclarant;
cerfaFields['dateNaissance']			   = $qp158PE10.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['villePaysNaissance']		   = $qp158PE10.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp158PE10.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp158PE10.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant;
+($qp158PE10.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp158PE10.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['villeDeclarant']  		       = ($qp158PE10.coordonneesGroup.coordonnees.codePostalDeclarant != null ? $qp158PE10.coordonneesGroup.coordonnees.codePostalDeclarant : ' ')
+' ' + $qp158PE10.coordonneesGroup.coordonnees.communeResidenceDeclarant;
cerfaFields['paysDeclarant']			   = $qp158PE10.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['telephoneMobileDeclarant']    = $qp158PE10.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp158PE10.coordonneesGroup.coordonnees.adresseCourriel;

//signature
cerfaFields['dateDeclaration']             = $qp158PE10.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp158PE10.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp158PE10.signatureGroup.signature.signatureElectronique;
cerfaFields['prenomNomDeclarant']		   = $qp158PE10.identiteGroup.identite.prenomsDeclarant+' '+ $qp158PE10.identiteGroup.identite.nomDeclarant;

var cerfa = pdf.create('models/installateur de réseaux d’eau déclaration libre LPS.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LPS - Installateur de réseaux d’eau.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Installateur de réseaux d’eau - Demande de RQP en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'installateur de réseaux d’eau',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});