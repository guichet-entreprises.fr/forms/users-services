var cerfaFields = {};

//etatCivil
cerfaFields['nomDeclarant']	               = $qp212PE4.identiteGroup.identite.nomDeclarant 
+($qp212PE4.identiteGroup.identite.nomJeuneFilleDeclarante != null ? ', née ' + $qp212PE4.identiteGroup.identite.nomJeuneFilleDeclarante : '');
cerfaFields['prenomsDeclarant']            = $qp212PE4.identiteGroup.identite.prenomsDeclarant;
cerfaFields['dateNaissanceDeclarant']      = $qp212PE4.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['lieuNaissanceDeclarant']      = $qp212PE4.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp212PE4.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp212PE4.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant  
+($qp212PE4.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp212PE4.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['communeResidenceDeclarant']   = $qp212PE4.coordonneesGroup.coordonnees.communeResidenceDeclarant+', ' +$qp212PE4.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['codePostalDeclarant']         = $qp212PE4.coordonneesGroup.coordonnees.codePostalDeclarant;
cerfaFields['telephoneMobileDeclarant']    = $qp212PE4.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp212PE4.coordonneesGroup.coordonnees.adresseCourriel;

//entreprise
cerfaFields['natureJuridiqueEntreprise']   = $qp212PE4.entrepriseGroup.entreprise.natureJuridiqueEntreprise;

//diplomesExperiencesProfessionnelles
cerfaFields['diplomeProfessionConsideree'] = $qp212PE4.diplomesExperiencesProfessionnellesGroup.diplomesExperiencesProfessionnelles.diplomeProfessionConsideree;

//signature
cerfaFields['dateDeclaration']             = $qp212PE4.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp212PE4.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp212PE4.signatureGroup.signature.signatureElectronique;


var cerfa = pdf.create('models/personnel installations combustion LE.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LE - Personnel des installations de combustion supérieures à 400 kW.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Personnel des installations de combustion supérieures à 400 kW - Demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de personnel des installations de combustion supérieures à 400 kW',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});
