var cerfaFields = {};

//etatCivil
cerfaFields['nomPrenomDeclarant']	       = $qp212PE8.identiteGroup.identite.prenomsDeclarant+' ' +$qp212PE8.identiteGroup.identite.nomDeclarant;
cerfaFields['dateNaissance']			   = $qp212PE8.identiteGroup.identite.dateNaissanceDeclarant;
cerfaFields['villePaysNaissance']		   = $qp212PE8.identiteGroup.identite.lieuNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']        = $qp212PE8.identiteGroup.identite.nationaliteDeclarant;

//coordonnees
cerfaFields['rueNumeroAdresseDeclarant']   = $qp212PE8.coordonneesGroup.coordonnees.rueNumeroAdresseDeclarant;
+($qp212PE8.coordonneesGroup.coordonnees.complementAdresse != null ? ', ' + $qp212PE8.coordonneesGroup.coordonnees.complementAdresse : ' ');
cerfaFields['villeDeclarant']  		       = ($qp212PE8.coordonneesGroup.coordonnees.codePostalDeclarant != null ? $qp212PE8.coordonneesGroup.coordonnees.codePostalDeclarant : ' ')
+' ' + $qp212PE8.coordonneesGroup.coordonnees.communeResidenceDeclarant;
cerfaFields['paysDeclarant']			   = $qp212PE8.coordonneesGroup.coordonnees.paysResidence;
cerfaFields['telephoneMobileDeclarant']    = $qp212PE8.coordonneesGroup.coordonnees.telephoneMobileDeclarant;
cerfaFields['adresseCourriel']             = $qp212PE8.coordonneesGroup.coordonnees.adresseCourriel;

//signature
cerfaFields['dateDeclaration']             = $qp212PE8.signatureGroup.signature.dateDeclaration;
cerfaFields['lieuDeclaration']             = $qp212PE8.signatureGroup.signature.lieuDeclaration;
cerfaFields['signatureElectronique']       = $qp212PE8.signatureGroup.signature.signatureElectronique;
cerfaFields['prenomNomDeclarant']		   = $qp212PE8.identiteGroup.identite.prenomsDeclarant+' '+ $qp212PE8.identiteGroup.identite.nomDeclarant;

var cerfa = pdf.create('models/personnel combustion declaration libre LPS.pdf', cerfaFields);//demande de reconnaissance de qualification professionnelle
var cerfaPdf = pdf.save('Déclaration LPS - Personnel des installations de combustion supérieures à 400 kW.pdf', cerfa);//Le PDF en sortie rempli avec les données saisies

return spec.create({
    id : 'review',
    label : 'Personnel des installations de combustion supérieures à 400 kW - Demande de RQP en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
			id : 'generated',
			label : 'Génération du formulaire',
			data : [ spec.createData({
					id : 'formulaire',
					label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de personnel des installations de combustion supérieures à 400 kW',
					description : 'Voici le formulaire obtenu à partir des données saisies :',
					type : 'FileReadOnly',
					value : [ cerfaPdf ]
			}) ]
	}) ]
});