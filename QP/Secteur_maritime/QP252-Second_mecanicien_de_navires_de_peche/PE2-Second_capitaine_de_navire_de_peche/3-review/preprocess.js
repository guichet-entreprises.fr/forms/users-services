var formFields = {};

//Civitlité

formFields['civiliteNomPrenom']             = $qp252PE2.information.etatCivil.civilite + ' ' + $qp252PE2.information.etatCivil.nomNaissance + ' ' + $qp252PE2.information.etatCivil.prenom;
formFields['nationalite']                   = $qp252PE2.information.etatCivil.nationalite;
formFields['numeroMarin']                   = $qp252PE2.information.etatCivil.numeroMarin;
formFields['ENIMAnterieur']                 = $qp252PE2.information.etatCivil.eNIMAnterieur;

//Adresse de contact

formFields['rueNumeroAdresseContact']       = $qp252PE2.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
formFields['rueExtensionAdresseContact']    = $qp252PE2.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
formFields['rueTypeAdresseContact']         = $qp252PE2.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
formFields['rueNomAdresseContact']          = $qp252PE2.adresseGroup.adresseContactGroup.rueNomAdresseContact;
formFields['rueComplementAdresseContact']   = $qp252PE2.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
formFields['codePostalAdresseContact']      = $qp252PE2.adresseGroup.adresseContactGroup.codePostalAdresseContact;
formFields['telephoneAdresseContact']       = $qp252PE2.adresseGroup.adresseContactGroup.telephoneAdresseContact;
formFields['villeAdresseContact']           = $qp252PE2.adresseGroup.adresseContactGroup.villeAdresseContact;
formFields['telecopieAdresseContact']       = $qp252PE2.adresseGroup.adresseContactGroup.telecopieAdresseContact;
formFields['courrielAdresseContact']        = $qp252PE2.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Qualifications

formFields['titreEtrangerOui']              = ($qp252PE2.qualificationsGroup.qualifications.titreEtranger ? true : '');
formFields['titreEtrangerNon']              = ($qp252PE2.qualificationsGroup.qualifications.titreEtranger ? '' : true);
formFields['nomTitre']                      = $qp252PE2.qualificationsGroup.qualifications.nomTitre;
formFields['dateValiditeTitre']             = $qp252PE2.qualificationsGroup.qualifications.dateValiditeTitre;

//Navire

formFields['fonction']                      = $qp252PE2.informationsCompNavire.navireGroup.fonction;
formFields['nomNavire']                     = $qp252PE2.informationsCompNavire.navireGroup.nomNavire;
formFields['jaugeBrute']                    = $qp252PE2.informationsCompNavire.navireGroup.jaugeBrute;
formFields['puissanceNavire']               = $qp252PE2.informationsCompNavire.navireGroup.puissanceNavire;
formFields['immatriculationNavire']			= $qp252PE2.informationsCompNavire.navireGroup.immatriculationNavire;
formFields['dateEmbarquementPrevue']        = $qp252PE2.informationsCompNavire.navireGroup.dateEmbarquementPrevue;

//Employeur

formFields['employeurNomPrenom']            = $qp252PE2.informationsCompEmployeur.employeurGroup.employeurNomPrenom;
formFields['employeurDenomination']         = $qp252PE2.informationsCompEmployeur.employeurGroup.employeurDenomination;
formFields['employeurRepresentant']         = $qp252PE2.informationsCompEmployeur.employeurGroup.employeurRepresentant;
formFields['numeroSiret']                   = $qp252PE2.informationsCompEmployeur.employeurGroup.numeroSiret;
formFields['employeurFormeJuridique']       = $qp252PE2.informationsCompEmployeur.employeurGroup.employeurFormeJuridique;
formFields['rueNumeroAdresseEmployeur']     = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueNumeroAdresseEmployeur;
formFields['rueExtensionAdresseEmployeur']  = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueExtensionAdresseEmployeur;
formFields['rueTypeAdresseEmployeur']       = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueTypeAdresseEmployeur;
formFields['rueNomAdresseEmployeur']        = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueNomAdresseEmployeur;
formFields['rueComplementAdresseEmployeur'] = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueComplementAdresseEmployeur;
formFields['codePostalAdresseEmployeur']    = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.codePostalAdresseEmployeur;
formFields['villeAdresseEmployeur']         = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.villeAdresseEmployeur;
formFields['telephoneAdresseEmployeur']     = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.telephoneAdresseEmployeur;
formFields['telecopieAdresseEmployeur']     = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.telecopieAdresseEmployeur;
formFields['courrielAdresseEmployeur']      = $qp252PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.courrielAdresseEmployeur;

//Signature

formFields['lieuSignature']                 = $qp252PE2.signatureGroup.signatureG.lieuSignature;
formFields['dateSignature']                 = $qp252PE2.signatureGroup.signatureG.dateSignature;
formFields['signature']                     = $qp252PE2.signatureGroup.signatureG.signature;


var pdfModel = pdf.create('models/cerfa_14750.pdf', formFields);
var pdfRecord = pdf.save('Second mécanicien sur les navires de pêche  - demande de RQP en vue LE.pdf', pdfModel);

var data = [ spec.createData({
    id : 'record',
    label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de second mécanicien sur les navires de pêche ',
	description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ pdfRecord ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du formulaire',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Second mécanicien sur les navires de pêche  - demande de RQP en vue d\'un libre établissement',
    groups : groups
});
