var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']          	= $qp252PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp252PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp252PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp252PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp252PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp252PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp252PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp252PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp252PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp252PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp252PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp252PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp252PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp252PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp252PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp252PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp252PE5.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp252PE5.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp252PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp252PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	=$qp252PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp252PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp252PE5.etatCivil.identificationDeclarant.prenomDeclarant;


var cerfa = pdf.create('models/courrier Capitaine navire LPS.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Second mécanicien sur les navires de pêche demande LPS.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Second mécanicien sur les navires de pêche - première déclaration de libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de première déclaration de libre prestation de services pour la profession de Second mécanicien sur les navires de pêche.',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
