var formFields = {};

//Civitlité

formFields['civiliteNomPrenom']                = $qp252PE3.information.etatCivil.civilite + ' ' + $qp252PE3.information.etatCivil.nomNaissance + ' ' + $qp252PE3.information.etatCivil.prenom;
formFields['dateNaissance']                    = $qp252PE3.information.etatCivil.dateNaissance;
formFields['lieuNaissance']                    = $qp252PE3.information.etatCivil.lieuNaissance;
formFields['nationalite']                      = $qp252PE3.information.etatCivil.nationalite;
formFields['numeroIdentification']             = $qp252PE3.information.etatCivil.numeroIdentification;

//Adresse de contact

formFields['rueNumeroAdresseContact']          = $qp252PE3.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
formFields['rueExtensionAdresseContact']       = $qp252PE3.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
formFields['rueTypeAdresseContact']            = $qp252PE3.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
formFields['rueNomAdresseContact']             = $qp252PE3.adresseGroup.adresseContactGroup.rueNomAdresseContact;
formFields['rueComplementAdresseContact']      = $qp252PE3.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
formFields['codePostalAdresseContact']         = $qp252PE3.adresseGroup.adresseContactGroup.codePostalAdresseContact;
formFields['villeAdresseContact']              = $qp252PE3.adresseGroup.adresseContactGroup.villeAdresseContact;
formFields['telephoneAdresseContact']          = $qp252PE3.adresseGroup.adresseContactGroup.telephoneAdresseContact;
formFields['telecopieAdresseContact']          = $qp252PE3.adresseGroup.adresseContactGroup.telecopieAdresseContact;
formFields['courrielAdresseContact']           = $qp252PE3.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Titre de formation

formFields['numTitre1']                        = $qp252PE3.formationGroup.titreFormation.numTitre1;
formFields['libelleTitre1']                    = $qp252PE3.formationGroup.titreFormation.libelleTitre1;
formFields['dateExpiration1']                  = $qp252PE3.formationGroup.titreFormation.dateExpiration1;
formFields['etatEmetteur1']                    = $qp252PE3.formationGroup.titreFormation.etatEmetteur1;
formFields['numTitre2']                        = $qp252PE3.formationGroup.titreFormation.numTitre2;
formFields['libelleTitre2']                    = $qp252PE3.formationGroup.titreFormation.libelleTitre2;
formFields['dateExpiration2']                  = $qp252PE3.formationGroup.titreFormation.dateExpiration2;
formFields['etatEmetteur2']                    = $qp252PE3.formationGroup.titreFormation.etatEmetteur2;
formFields['numTitre3']                        = $qp252PE3.formationGroup.titreFormation.numTitre3;
formFields['libelleTitre3']                    = $qp252PE3.formationGroup.titreFormation.libelleTitre3;
formFields['dateExpiration3']                  = $qp252PE3.formationGroup.titreFormation.dateExpiration3;
formFields['etatEmetteur3']                    = $qp252PE3.formationGroup.titreFormation.etatEmetteur3;
formFields['numTitre4']                        = $qp252PE3.formationGroup.titreFormation.numTitre4;
formFields['libelleTitre4']                    = $qp252PE3.formationGroup.titreFormation.libelleTitre4;
formFields['dateExpiration4']                  = $qp252PE3.formationGroup.titreFormation.dateExpiration4;
formFields['etatEmetteur4']                    = $qp252PE3.formationGroup.titreFormation.dateExpiration4;

//Renseignements sur l'armateur

formFields['denominationSocialeArmateur']      = $qp252PE3.armateurGroup.autreRenseignements.denominationSocialeArmateur;
formFields['representantLegalArmateur']        = $qp252PE3.armateurGroup.autreRenseignements.representantLegalArmateurNom + ' ' + $qp252PE3.armateurGroup.autreRenseignements.representantLegalArmateurPrenom;
formFields['numeroSiret']                      = $qp252PE3.armateurGroup.autreRenseignements.numeroSiret;
formFields['rueNumeroArmateur']                = $qp252PE3.armateurGroup.autreRenseignements.rueNumeroArmateur;
formFields['rueExtensionArmateur']             = $qp252PE3.armateurGroup.autreRenseignements.rueExtensionArmateur;
formFields['rueTypeArmateur']                  = $qp252PE3.armateurGroup.autreRenseignements.rueTypeArmateur;
formFields['rueNomArmateur']                   = $qp252PE3.armateurGroup.autreRenseignements.rueNomArmateur;
formFields['rueComplementArmateur']            = $qp252PE3.armateurGroup.autreRenseignements.rueComplementArmateur;
formFields['codePostalArmateur']               = $qp252PE3.armateurGroup.autreRenseignements.codePostalArmateur;
formFields['villeArmateur']                    = $qp252PE3.armateurGroup.autreRenseignements.villeArmateur;
formFields['telephoneArmateur']                = $qp252PE3.armateurGroup.autreRenseignements.telephoneArmateur;
formFields['telecopieArmateur']                = $qp252PE3.armateurGroup.autreRenseignements.telecopieArmateur;
formFields['courrielArmateur']                 = $qp252PE3.armateurGroup.autreRenseignements.courrielArmateur;

//Renseignements sur le navire

formFields['nomNavire']                        = $qp252PE3.navireGroup.navireRenseignements.nomNavire;
formFields['puissanceNavire']                  = $qp252PE3.navireGroup.navireRenseignements.puissanceNavire;
formFields['immatriculationNavire']            = $qp252PE3.navireGroup.navireRenseignements.immatriculationNavire;
formFields['jaugeBrute']                       = $qp252PE3.navireGroup.navireRenseignements.jaugeBrute;

//Promesse d'embarquement

formFields['datEmbarquementDebut']             = $qp252PE3.embarquementGroup.promesseEmbarquement.datEmbarquement.from;
formFields['datEmbarquementFin']               = $qp252PE3.embarquementGroup.promesseEmbarquement.datEmbarquement.to;
formFields['fonction']                         = $qp252PE3.embarquementGroup.promesseEmbarquement.fonction;


//Signature

formFields['signatureConnaissanceMarin']       = $qp252PE3.signatureGroup.signature.signatureConnaissanceMarin;
formFields['signatureConnaissancesJuridiques'] = $qp252PE3.signatureGroup.signature.signatureConnaissancesJuridiques;
formFields['signatureExigencesMoralite']       = $qp252PE3.signatureGroup.signature.signatureExigencesMoralite;
formFields['certifieHonneur']                  = $qp252PE3.signatureGroup.signature.certifieHonneur;
formFields['dateSignature']                    = $qp252PE3.signatureGroup.signature.dateSignature;
formFields['lieuSignature']                    = $qp252PE3.signatureGroup.signature.lieuSignature;


var pdfModel = pdf.create('models/cerfa 15333.pdf', formFields);
var pdfRecord = pdf.save('Second mécanicien sur les navires de pêche  - demande de RQP en vue LE.pdf', pdfModel);

var data = [ spec.createData({
    id : 'record',
    label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession de second mécanicien sur les navires de pêche ',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ pdfRecord ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du formulaire',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Second mécanicien sur les navires de pêche  - demande de RQP en vue d\'un libre établissement',
    groups : groups
});
