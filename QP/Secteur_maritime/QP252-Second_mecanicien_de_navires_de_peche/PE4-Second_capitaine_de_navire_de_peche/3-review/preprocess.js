var formFields = {};

//Civitlité

formFields['civiliteNom']                	   = $qp252PE4.information.etatCivil.civilite + ' ' + $qp252PE4.information.etatCivil.nomNaissance
formFields['civilitePrenom']                   = $qp252PE4.information.etatCivil.prenom;
formFields['dateNaissance']                    = $qp252PE4.information.etatCivil.dateNaissance;
formFields['nationalite']                      = $qp252PE4.information.etatCivil.nationalite;
formFields['numeroIdentification']             = $qp252PE4.information.etatCivil.numeroIdentification;
formFields['serviceMarin']             		   = $qp252PE4.information.etatCivil.service;

//Adresse de contact

formFields['rueNumeroAdresseContact']          = $qp252PE4.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
formFields['rueExtensionAdresseContact']       = $qp252PE4.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
formFields['rueTypeAdresseContact']            = $qp252PE4.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
formFields['rueNomAdresseContact']             = $qp252PE4.adresseGroup.adresseContactGroup.rueNomAdresseContact;
formFields['rueComplementAdresseContact']      = $qp252PE4.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
formFields['codePostalAdresseContact']         = $qp252PE4.adresseGroup.adresseContactGroup.codePostalAdresseContact;
formFields['villeAdresseContact']              = $qp252PE4.adresseGroup.adresseContactGroup.villeAdresseContact;
formFields['telephoneAdresseContact']          = $qp252PE4.adresseGroup.adresseContactGroup.telephoneAdresseContact;
formFields['telecopieAdresseContact']          = $qp252PE4.adresseGroup.adresseContactGroup.telecopieAdresseContact;
formFields['courrielAdresseContact']           = $qp252PE4.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Titre de formation

formFields['numTitre1']                        = $qp252PE4.formationGroup.titreFormation.numTitre1;
formFields['libelleTitre1']                    = $qp252PE4.formationGroup.titreFormation.libelleTitre1;
formFields['dateExpiration1']                  = $qp252PE4.formationGroup.titreFormation.dateExpiration1;
formFields['numTitre2']                        = $qp252PE4.formationGroup.titreFormation.numTitre2;
formFields['libelleTitre2']                    = $qp252PE4.formationGroup.titreFormation.libelleTitre2;
formFields['dateExpiration2']                  = $qp252PE4.formationGroup.titreFormation.dateExpiration2;
formFields['numTitre3']                        = $qp252PE4.formationGroup.titreFormation.numTitre3;
formFields['libelleTitre3']                    = $qp252PE4.formationGroup.titreFormation.libelleTitre3;
formFields['dateExpiration3']                  = $qp252PE4.formationGroup.titreFormation.dateExpiration3;
formFields['numTitre4']                        = $qp252PE4.formationGroup.titreFormation.numTitre4;
formFields['libelleTitre4']                    = $qp252PE4.formationGroup.titreFormation.libelleTitre4;
formFields['dateExpiration4']                  = $qp252PE4.formationGroup.titreFormation.dateExpiration4;
formFields['numTitre5']                        = $qp252PE4.formationGroup.titreFormation.numTitre5;
formFields['libelleTitre5']                    = $qp252PE4.formationGroup.titreFormation.libelleTitre5;
formFields['dateExpiration5']                  = $qp252PE4.formationGroup.titreFormation.dateExpiration5;
formFields['numTitre6']                        = $qp252PE4.formationGroup.titreFormation.numTitre6;
formFields['libelleTitre6']                    = $qp252PE4.formationGroup.titreFormation.libelleTitre6;
formFields['dateExpiration6']                  = $qp252PE4.formationGroup.titreFormation.dateExpiration6;
formFields['numTitre7']                        = $qp252PE4.formationGroup.titreFormation.numTitre7;
formFields['libelleTitre7']                    = $qp252PE4.formationGroup.titreFormation.libelleTitre7;
formFields['dateExpiration7']                  = $qp252PE4.formationGroup.titreFormation.dateExpiration7;

//Mise à disposition titre

formFields['choixEnvoiDomicile']               = Value('id').of($qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.choixMiseDisposition).eq('domicile');
formFields['choixEnvoiAutreAdresse']           = Value('id').of($qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.choixMiseDisposition).eq('autreAdresse');
formFields['choixRemisDemandeur']              = Value('id').of($qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.choixMiseDisposition).eq('remisDemandeur');
formFields['adresseEnvoiAutre1']               = ($qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseNumeroRue != null ? $qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseNumeroRue + ' ' : '') + ($qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseComplement != null ? ', ' + $qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseComplement : '');
formFields['adresseEnvoiAutre2']               = ($qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseCodePostal != null ? $qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseCodePostal + ' ' : '') + ($qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseVille != null ? $qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseVille + ' ' : '') + ($qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdressePays != null ? ', ' + $qp252PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdressePays + ' ' : '')

//Signature

formFields['signature']                 	   = $qp252PE4.signatureGroup.signature.certifie;
formFields['dateSignature']                    = $qp252PE4.signatureGroup.signature.dateSignature;
formFields['lieuSignature']                    = $qp252PE4.signatureGroup.signature.lieuSignature;


var pdfModel = pdf.create('models/cerfa_14949-01.pdf', formFields);
var pdfRecord = pdf.save('Second mécanicien sur les navires de pêche - renouvellement demande de RQP en vue LE.pdf', pdfModel);

var data = [ spec.createData({
    id : 'record',
    label : 'Formulaire de renouvellement de demande de reconnaissance de qualifications professionnelles pour la profession de second mécanicien sur les navires de pêche ',
    description : 'Voici le formulaire obtenu à partir des données saisies.',
    type : 'FileReadOnly',
    value : [ pdfRecord ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du formulaire',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Second mécanicien sur les navires de pêche  - renouvellement demande de RQP en vue d\'un libre établissement',
    groups : groups
});
