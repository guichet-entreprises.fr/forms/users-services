var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp056PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp056PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp056PE4.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']            = $qp056PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp056PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp056PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']           = $qp056PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp056PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                  = $qp056PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                = $qp056PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                      = $qp056PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp056PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp056PE4.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                    = ($qp056PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp056PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp056PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp056PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']              = $qp056PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                = $qp056PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                     = $qp056PE4.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                         = $qp056PE4.signature.signature.dateSignature;
cerfaFields['signature']                    = $qp056PE4.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp056PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']   = $qp056PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp056PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp056PE4.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp056PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp056PE4.signature.signature.dateSignature,
		autoriteHabilitee :"Direction interrégional de la mer",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services.",
		civiliteNomPrenom : civNomPrenom
	});


/*
 * Ajout du cerfa
 */
var finalDoc = nash.doc //
    .load('models/courrier_libre_LPS_V3.pdf') //
    .apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLPS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatMedical);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Chef_mecanicien_sur_les_navires_de_peche_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Chef mécanicien sur les navires de pêche - déclaration préalable en vue d\'une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de chef mécanicien sur les navires de pêche',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});