var cerfaFields = {};

//Etat civil

var civNomPrenom                     = $qp056PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp056PE1.etatCivil.identificationDeclarant.nom +' '+$qp056PE1.etatCivil.identificationDeclarant.prenoms + ',';

cerfaFields['nomPrenom']             = $qp056PE1.etatCivil.identificationDeclarant.nom +' '+$qp056PE1.etatCivil.identificationDeclarant.prenoms;
cerfaFields['numeroMarin']           = $qp056PE1.etatCivil.identificationDeclarant.numeroMarin;
cerfaFields['nationalite']           = $qp056PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['identificationENIM']    = $qp056PE1.etatCivil.identificationDeclarant.numeroEnim;

//Adresse contact marin
cerfaFields['typeVoie']              = $qp056PE1.adresse.adresseContact.typeVoie;
cerfaFields['extension']             = $qp056PE1.adresse.adresseContact.extension;
cerfaFields['numeroVoie']            = $qp056PE1.adresse.adresseContact.adressenumero;
cerfaFields['nomVoie']               = $qp056PE1.adresse.adresseContact.nomVoie;
cerfaFields['codePostal']            = $qp056PE1.adresse.adresseContact.adresseCodePostal;
cerfaFields['mail']                  = $qp056PE1.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['telecopie']             = $qp056PE1.adresse.adresseContact.telecopie;
cerfaFields['telephone']             = $qp056PE1.adresse.adresseContact.telephone;
cerfaFields['localite']              = $qp056PE1.adresse.adresseContact.adresseCommune;
cerfaFields['lieuDit']               = $qp056PE1.adresse.adresseContact.lieuDit;


//qualification
cerfaFields['nomTitre']              = $qp056PE1.qualifications.qualifications.nomTitre;
cerfaFields['dateValiditeTitre']     = (($qp056PE1.qualifications.qualifications.dateValiditeTitre !=null)?($qp056PE1.qualifications.qualifications.dateValiditeTitre):' ');
cerfaFields['oui']                   = true; //Cas PE1 : RE avec titre obtenu dans EM qui réglemente
cerfaFields['non']                   = false;


//information navire
cerfaFields['exerciceFonction']      = $qp056PE1.navire.navire.exerciceFonction;
cerfaFields['navire']                = $qp056PE1.navire.navire.navire1;
cerfaFields['jauge']                 = $qp056PE1.navire.navire.jauge;
cerfaFields['puissance']             = $qp056PE1.navire.navire.puissance+'cv';
cerfaFields['dateEmbarquement']      = $qp056PE1.navire.navire.dateEmbarquement;
cerfaFields['immatricule']           = $qp056PE1.navire.navire.immatricule;

//employeur


cerfaFields['nomPrenomEmployeur']    = (($qp056PE1.employeur.employeur.nomEmployeur !=null)?($qp056PE1.employeur.employeur.nomEmployeur):' ')+' '+(($qp056PE1.employeur.employeur.prenomEmployeur !=null)?($qp056PE1.employeur.employeur.prenomEmployeur):' ');
cerfaFields['representantNomPrenom'] = (($qp056PE1.employeur.employeur.representant !=null)?($qp056PE1.employeur.employeur.representant):' ')+' '+(($qp056PE1.employeur.employeur.representant1 !=null)?($qp056PE1.employeur.employeur.representant1):' ');
cerfaFields['numeroSiret']           = $qp056PE1.employeur.employeur.numeroSiret;
cerfaFields['formeJuridique']        = $qp056PE1.employeur.employeur.formeJuridique;
cerfaFields['denomination']          = (($qp056PE1.employeur.employeur.denomination !=null)?($qp056PE1.employeur.employeur.denomination):' ');

//adresse employeur
cerfaFields['mailEmployeur']         = (($qp056PE1.employeur.employeur.employeurAdresse.mailAdresseDeclarantEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.mailAdresseDeclarantEmployeur):' ');
cerfaFields['numeroVoieEmployeur']   = (($qp056PE1.employeur.employeur.employeurAdresse.adressenumeroEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.adressenumeroEmployeur):' ');
cerfaFields['extensionEmployeur']    = (($qp056PE1.employeur.employeur.employeurAdresse.extensionEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.extensionEmployeur):' ');
cerfaFields['typeVoieEmployeur']     = (($qp056PE1.employeur.employeur.employeurAdresse.typeVoieEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.typeVoieEmployeur):' ');
cerfaFields['nomVoieEmployeur']      = (($qp056PE1.employeur.employeur.employeurAdresse.nomVoieEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.nomVoieEmployeur):' ');
cerfaFields['codePostalEmployeur']   = (($qp056PE1.employeur.employeur.employeurAdresse.adresseCodePostalEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.adresseCodePostalEmployeur):' ');
cerfaFields['telephoneEmployeur']    = (($qp056PE1.employeur.employeur.employeurAdresse.telephoneEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.telephoneEmployeur):' ');
cerfaFields['localiteEmployeur']     = (($qp056PE1.employeur.employeur.employeurAdresse.adresseCommuneEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.adresseCommuneEmployeur):' ');
cerfaFields['lieuDitEmployeur']      = (($qp056PE1.employeur.employeur.employeurAdresse.lieuDitEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.lieuDitEmployeur):' ');
cerfaFields['telecopieEmployeur']    = (($qp056PE1.employeur.employeur.employeurAdresse.telecopieEmployeur !=null)?($qp056PE1.employeur.employeur.employeurAdresse.telecopieEmployeur):' ');

//Signature
cerfaFields['lieuSignature']         = $qp056PE1.signature.signature.lieuSignature;
cerfaFields['dateSignature']         = $qp056PE1.signature.signature.dateSignature;
cerfaFields['signature']             = $qp056PE1.signature.signature.signature;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp056PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp056PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Direction interrégional de la mer",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});


/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/CERFA 14750.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasierJudiciaire);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjniveauLangue);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Chef_mécanicien_sur_les_navires_de_peche_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Chef mécanicien sur les navires de pêche - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de chef mécanicien sur les navires de pêche est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
