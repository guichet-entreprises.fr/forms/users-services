var cerfaFields = {};

//Etat civil

var civNomPrenom					 = $qp195PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp195PE4.etatCivil.identificationDeclarant.nom + ' ' + $qp195PE4.etatCivil.identificationDeclarant.prenoms + ',';
cerfaFields['nom']                   = $qp195PE4.etatCivil.identificationDeclarant.nom;
cerfaFields['Text2']                 = $qp195PE4.etatCivil.identificationDeclarant.prenoms;
cerfaFields['numeroIdentification']  = $qp195PE4.etatCivil.identificationDeclarant.numeroMarin;
cerfaFields['nationalite']           = $qp195PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['serviceMarin']          = $qp195PE4.etatCivil.identificationDeclarant.serviceMarin;
cerfaFields['nationalite']           = $qp195PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']         = $qp195PE4.etatCivil.identificationDeclarant.dateNaissance;

//Adresse contact marin
cerfaFields['typeVoie']              = $qp195PE4.adresse.adresseContact.typeVoie;
cerfaFields['extension']             = $qp195PE4.adresse.adresseContact.extension;
cerfaFields['numeroVoie']            = $qp195PE4.adresse.adresseContact.adressenumero;
cerfaFields['nomVoie']               = $qp195PE4.adresse.adresseContact.nomVoie;
cerfaFields['codePostal']            = $qp195PE4.adresse.adresseContact.adresseCodePostal;
cerfaFields['mail']                  = $qp195PE4.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['telecopie']             = $qp195PE4.adresse.adresseContact.telecopie;
cerfaFields['telephone']             = $qp195PE4.adresse.adresseContact.telephone;
cerfaFields['localite']              = $qp195PE4.adresse.adresseContact.adresseCommune;
cerfaFields['lieuDit']               = $qp195PE4.adresse.adresseContact.lieuDit;


//titre
cerfaFields['numeroTitre']          = (($qp195PE4.navire.navire.numeroTitre !=null)?($qp195PE4.navire.navire.numeroTitre):' ');
cerfaFields['dateExpiration']       = (($qp195PE4.navire.navire.dateExpiration !=null)?($qp195PE4.navire.navire.dateExpiration):' ');
cerfaFields['libelleTitre']         = (($qp195PE4.navire.navire.libelleTitre !=null)?($qp195PE4.navire.navire.libelleTitre):' ');

cerfaFields['numeroTitre1']         = (($qp195PE4.navire.navire.numeroTitre1 != null)?($qp195PE4.navire.navire.numeroTitre1):' ');
cerfaFields['dateExpiration1']      = (($qp195PE4.navire.navire.dateExpiration1 !=null)?($qp195PE4.navire.navire.dateExpiration1):' ');
cerfaFields['libelleTitre1']        = (($qp195PE4.navire.navire.libelleTitre1 !=null)?($qp195PE4.navire.navire.libelleTitre1):' ');

cerfaFields['numeroTitre2']         = (($qp195PE4.navire.navire.numeroTitre2!=null)?($qp195PE4.navire.navire.numeroTitre2):' ');
cerfaFields['dateExpiration2']      = (($qp195PE4.navire.navire.dateExpiration2!=null)?($qp195PE4.navire.navire.dateExpiration2):' ');
cerfaFields['libelleTitre2']        = (($qp195PE4.navire.navire.libelleTitre2!=null)?($qp195PE4.navire.navire.libelleTitre2):' ');

cerfaFields['numeroTitre3']         = (($qp195PE4.navire.navire.numeroTitre3!=null)?($qp195PE4.navire.navire.numeroTitre3):' ');
cerfaFields['dateExpiration3']      = (($qp195PE4.navire.navire.dateExpiration3!=null)?($qp195PE4.navire.navire.dateExpiration3):' ');
cerfaFields['libelleTitre3']        = (($qp195PE4.navire.navire.libelleTitre3!=null)?($qp195PE4.navire.navire.libelleTitre3):' ');

cerfaFields['numeroTitre4']         = (($qp195PE4.navire.navire.numeroTitre4!=null)?($qp195PE4.navire.navire.numeroTitre4):' ');
cerfaFields['dateExpiration4']      = (($qp195PE4.navire.navire.dateExpiration4!=null)?($qp195PE4.navire.navire.dateExpiration4):' ');
cerfaFields['libelleTitre4']        = (($qp195PE4.navire.navire.libelleTitre4!=null)?($qp195PE4.navire.navire.libelleTitre4):' ');

cerfaFields['numeroTitre5']         = (($qp195PE4.navire.navire.numeroTitre5!=null)?($qp195PE4.navire.navire.numeroTitre5):' ');
cerfaFields['dateExpiration5']      = (($qp195PE4.navire.navire.dateExpiration5!=null)?($qp195PE4.navire.navire.dateExpiration5):' ');
cerfaFields['libelleTitre5']        = (($qp195PE4.navire.navire.libelleTitre5!=null)?($qp195PE4.navire.navire.libelleTitre5):' ');

cerfaFields['numeroTitre6']         = (($qp195PE4.navire.navire.numeroTitre6!=null)?($qp195PE4.navire.navire.numeroTitre6):' ');
cerfaFields['dateExpiration6']      = (($qp195PE4.navire.navire.dateExpiration6!=null)?($qp195PE4.navire.navire.dateExpiration6):' ');
cerfaFields['libelleTitre6']        = (($qp195PE4.navire.navire.libelleTitre6!=null)?($qp195PE4.navire.navire.libelleTitre6):' ');


//mise a disposition
cerfaFields['envoiDomicile']        = Value('id').of($qp195PE4.miseDisposition.miseDisposition.envoiDomicile).contains('domicile')?true:' ';
cerfaFields['envoiDIRM']            = Value('id').of($qp195PE4.miseDisposition.miseDisposition.envoiDomicile).contains('drim')?true:' ';
cerfaFields['envoiAutre']           = Value('id').of($qp195PE4.miseDisposition.miseDisposition.envoiDomicile).contains('autre')?true:' ';
cerfaFields['preciser']             = $qp195PE4.miseDisposition.miseDisposition.preciser;

//Signature
cerfaFields['lieuSignature']         = $qp195PE4.signature.signature.lieuSignature;
cerfaFields['dateSignature']         = $qp195PE4.signature.signature.dateSignature;
cerfaFields['signature']             = $qp195PE4.signature.signature.signature;
cerfaFields['signatureDeclarant']	 = $qp195PE4.etatCivil.identificationDeclarant.nom + ' ' + $qp195PE4.etatCivil.identificationDeclarant.prenoms;



if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {
/*
 * Chargement du modele de document et injection des données
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp195PE4.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
	.apply({
		date: $qp195PE4.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/Cerfa 14949-01.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.preprocess.pjID);
appendPj($attachmentPreprocess.preprocess.pjCertificat);
appendPj($attachmentPreprocess.preprocess.pjEmbarquement);
appendPj($attachmentPreprocess.preprocess.pjAttest);
appendPj($attachmentPreprocess.preprocess.pjDocument);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Officier quart passerelle RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Officier chargé du quart à la passerelle sur les navires de pêche - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'officier chargé du quart à la passerelle sur les navires de pêche est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
}

function buildSimpleDocument() {

var cerfa = pdf.create('models/Cerfa 14949-01.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Officier quart passerelle RQP.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Officier chargé du quart à la passerelle sur les navires de pêche - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'officier chargé du quart à la passerelle sur les navires de pêche.',
            description : 'Voici le formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
}