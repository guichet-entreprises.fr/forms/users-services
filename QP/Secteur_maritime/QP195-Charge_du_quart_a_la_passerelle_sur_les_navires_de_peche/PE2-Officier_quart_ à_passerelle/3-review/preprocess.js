var cerfaFields = {};

//Etat civil

var civNomPrenom 					 = $qp195PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp195PE2.etatCivil.identificationDeclarant.nom +' '+$qp195PE2.etatCivil.identificationDeclarant.prenoms + ',';

cerfaFields['nomPrenom']             = $qp195PE2.etatCivil.identificationDeclarant.nom +' '+$qp195PE2.etatCivil.identificationDeclarant.prenoms;
cerfaFields['numeroMarin']           = (($qp195PE2.etatCivil.identificationDeclarant.numeroMarin !=null)?($qp195PE2.etatCivil.identificationDeclarant.numeroMarin):' ');
cerfaFields['nationalite']           = $qp195PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['identificationENIM']    = (($qp195PE2.etatCivil.identificationDeclarant.numeroEnim !=null)?($qp195PE2.etatCivil.identificationDeclarant.numeroEnim):' ');

//Adresse contact marin
cerfaFields['typeVoie']              = $qp195PE2.adresse.adresseContact.typeVoie;
cerfaFields['extension']             = $qp195PE2.adresse.adresseContact.extension;
cerfaFields['numeroVoie']            = $qp195PE2.adresse.adresseContact.adressenumero;
cerfaFields['nomVoie']               = $qp195PE2.adresse.adresseContact.nomVoie;
cerfaFields['codePostal']            = $qp195PE2.adresse.adresseContact.adresseCodePostal;
cerfaFields['mail']                  = $qp195PE2.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['telecopie']             = $qp195PE2.adresse.adresseContact.telecopie;
cerfaFields['telephone']             = $qp195PE2.adresse.adresseContact.telephone;
cerfaFields['localite']              = $qp195PE2.adresse.adresseContact.adresseCommune;
cerfaFields['lieuDit']               = $qp195PE2.adresse.adresseContact.lieuDit;

//qualification
cerfaFields['nomTitre']              = $qp195PE2.qualifications.qualifications.nomTitre;
cerfaFields['dateValiditeTitre']     = (($qp195PE2.qualifications.qualifications.dateValiditeTitre !=null)?($qp195PE2.qualifications.qualifications.dateValiditeTitre):' ');
cerfaFields['oui']                   = ($qp195PE2.qualifications.qualifications.titreEtranger ? true : false);
cerfaFields['non']                   = ($qp195PE2.qualifications.qualifications.titreEtranger ? false : true);


//information navire
cerfaFields['exerciceFonction']      = $qp195PE2.navire.navire.exerciceFonction;
cerfaFields['navire']                = $qp195PE2.navire.navire.navire1;
cerfaFields['jauge']                 = $qp195PE2.navire.navire.jauge;
cerfaFields['puissance']             = $qp195PE2.navire.navire.puissance+'cv';
cerfaFields['dateEmbarquement']      = $qp195PE2.navire.navire.dateEmbarquement;
cerfaFields['immatricule']           = $qp195PE2.navire.navire.immatricule;

//employeur


cerfaFields['nomPrenomEmployeur']    = (($qp195PE2.employeur.employeur.nomEmployeur !=null)?($qp195PE2.employeur.employeur.nomEmployeur):' ')+' '+(($qp195PE2.employeur.employeur.prenomEmployeur !=null)?($qp195PE2.employeur.employeur.prenomEmployeur):' ');
cerfaFields['representantNomPrenom'] = (($qp195PE2.employeur.employeur.representant !=null)?($qp195PE2.employeur.employeur.representant):' ')+' '+(($qp195PE2.employeur.employeur.representant1 !=null)?($qp195PE2.employeur.employeur.representant1):' ');
cerfaFields['numeroSiret']           = $qp195PE2.employeur.employeur.numeroSiret;
cerfaFields['formeJuridique']        = $qp195PE2.employeur.employeur.formeJuridique;
cerfaFields['denomination']          = (($qp195PE2.employeur.employeur.denomination !=null)?($qp195PE2.employeur.employeur.denomination):' ');

//adresse employeur
cerfaFields['mailEmployeur']         = (($qp195PE2.employeur.employeur.employeurAdresse.mailAdresseDeclarantEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.mailAdresseDeclarantEmployeur):' ');
cerfaFields['numeroVoieEmployeur']   = (($qp195PE2.employeur.employeur.employeurAdresse.adressenumeroEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.adressenumeroEmployeur):' ');
cerfaFields['extensionEmployeur']    = (($qp195PE2.employeur.employeur.employeurAdresse.extensionEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.extensionEmployeur):' ');
cerfaFields['typeVoieEmployeur']     = (($qp195PE2.employeur.employeur.employeurAdresse.typeVoieEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.typeVoieEmployeur):' ');
cerfaFields['nomVoieEmployeur']      = (($qp195PE2.employeur.employeur.employeurAdresse.nomVoieEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.nomVoieEmployeur):' ');
cerfaFields['codePostalEmployeur']   = (($qp195PE2.employeur.employeur.employeurAdresse.adresseCodePostalEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.adresseCodePostalEmployeur):' ');
cerfaFields['telephoneEmployeur']    = (($qp195PE2.employeur.employeur.employeurAdresse.telephoneEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.telephoneEmployeur):' ');
cerfaFields['localiteEmployeur']     = (($qp195PE2.employeur.employeur.employeurAdresse.adresseCommuneEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.adresseCommuneEmployeur):' ');
cerfaFields['lieuDitEmployeur']      = (($qp195PE2.employeur.employeur.employeurAdresse.lieuDitEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.lieuDitEmployeur):' ');
cerfaFields['telecopieEmployeur']    = (($qp195PE2.employeur.employeur.employeurAdresse.telecopieEmployeur !=null)?($qp195PE2.employeur.employeur.employeurAdresse.telecopieEmployeur):' ');

//Signature
cerfaFields['lieuSignature']         = $qp195PE2.signature.signature.lieuSignature;
cerfaFields['dateSignature']         = $qp195PE2.signature.signature.dateSignature;
cerfaFields['signature']             = $qp195PE2.signature.signature.signature;
cerfaFields['signatureDeclarant']	 = $qp195PE2.etatCivil.identificationDeclarant.nom +' '+$qp195PE2.etatCivil.identificationDeclarant.prenoms;



if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {
/*
 * Chargement du modele de document et injection des données
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp195PE2.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
	.apply({
		date: $qp195PE2.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/CERFA 14750.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.preprocess.pjID);
appendPj($attachmentPreprocess.preprocess.pjProgramme);
appendPj($attachmentPreprocess.preprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.preprocess.pjPreuveQualifiEMNRP);
appendPj($attachmentPreprocess.preprocess.pjCertificat);
appendPj($attachmentPreprocess.preprocess.pjniveauLangue);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Officier quart passerelle RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Officier chargé du quart à la passerelle sur les navires de pêche - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'officier chargé du quart à la passerelle sur les navires de pêche est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
}

function buildSimpleDocument() {


var cerfa = pdf.create('models/CERFA 14750.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Officier quart navires pêche - RQP.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Officier chargé du quart à la passerelle sur les navires de pêche - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'officier chargé du quart à la passerelle sur les navires de pêche.',
            description : 'Voici le formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
}