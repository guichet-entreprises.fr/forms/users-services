var formFields = {};

//Etat civil

var civNomPrenom 					 		   = $qp195PE3.information.etatCivil.civilite + ' ' + $qp195PE3.information.etatCivil.nomNaissance +' '+$qp195PE3.information.etatCivil.prenom + ',';

formFields['civiliteNomPrenom']                = civNomPrenom;
formFields['dateNaissance']                    = $qp195PE3.information.etatCivil.dateNaissance;
formFields['lieuNaissance']                    = $qp195PE3.information.etatCivil.lieuNaissance;
formFields['nationalite']                      = $qp195PE3.information.etatCivil.nationalite;
formFields['numeroIdentification']             = $qp195PE3.information.etatCivil.numeroIdentification;

//Adresse de contact

formFields['rueNumeroAdresseContact']          = $qp195PE3.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
formFields['rueExtensionAdresseContact']       = $qp195PE3.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
formFields['rueTypeAdresseContact']            = $qp195PE3.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
formFields['rueNomAdresseContact']             = $qp195PE3.adresseGroup.adresseContactGroup.rueNomAdresseContact;
formFields['rueComplementAdresseContact']      = $qp195PE3.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
formFields['codePostalAdresseContact']         = $qp195PE3.adresseGroup.adresseContactGroup.codePostalAdresseContact;
formFields['villeAdresseContact']              = $qp195PE3.adresseGroup.adresseContactGroup.villeAdresseContact;
formFields['telephoneAdresseContact']          = $qp195PE3.adresseGroup.adresseContactGroup.telephoneAdresseContact;
formFields['telecopieAdresseContact']          = $qp195PE3.adresseGroup.adresseContactGroup.telecopieAdresseContact;
formFields['courrielAdresseContact']           = $qp195PE3.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Titre de formation

formFields['numTitre1']                        = $qp195PE3.formationGroup.titreFormation.numTitre1;
formFields['libelleTitre1']                    = $qp195PE3.formationGroup.titreFormation.libelleTitre1;
formFields['dateExpiration1']                  = $qp195PE3.formationGroup.titreFormation.dateExpiration1;
formFields['etatEmetteur1']                    = $qp195PE3.formationGroup.titreFormation.etatEmetteur1;
formFields['numTitre2']                        = $qp195PE3.formationGroup.titreFormation.numTitre2;
formFields['libelleTitre2']                    = $qp195PE3.formationGroup.titreFormation.libelleTitre2;
formFields['dateExpiration2']                  = $qp195PE3.formationGroup.titreFormation.dateExpiration2;
formFields['etatEmetteur2']                    = $qp195PE3.formationGroup.titreFormation.etatEmetteur2;
formFields['numTitre3']                        = $qp195PE3.formationGroup.titreFormation.numTitre3;
formFields['libelleTitre3']                    = $qp195PE3.formationGroup.titreFormation.libelleTitre3;
formFields['dateExpiration3']                  = $qp195PE3.formationGroup.titreFormation.dateExpiration3;
formFields['etatEmetteur3']                    = $qp195PE3.formationGroup.titreFormation.etatEmetteur3;
formFields['numTitre4']                        = $qp195PE3.formationGroup.titreFormation.numTitre4;
formFields['libelleTitre4']                    = $qp195PE3.formationGroup.titreFormation.libelleTitre4;
formFields['dateExpiration4']                  = $qp195PE3.formationGroup.titreFormation.dateExpiration4;
formFields['etatEmetteur4']                    = $qp195PE3.formationGroup.titreFormation.dateExpiration4;

//Renseignements sur l'armateur

formFields['denominationSocialeArmateur']      = $qp195PE3.armateurGroup.autreRenseignements.denominationSocialeArmateur;
formFields['representantLegalArmateur']        = $qp195PE3.armateurGroup.autreRenseignements.representantLegalArmateurNom + ' ' + $qp195PE3.armateurGroup.autreRenseignements.representantLegalArmateurPrenom;
formFields['numeroSiret']                      = $qp195PE3.armateurGroup.autreRenseignements.numeroSiret;
formFields['rueNumeroArmateur']                = $qp195PE3.armateurGroup.autreRenseignements.rueNumeroArmateur;
formFields['rueExtensionArmateur']             = $qp195PE3.armateurGroup.autreRenseignements.rueExtensionArmateur;
formFields['rueTypeArmateur']                  = $qp195PE3.armateurGroup.autreRenseignements.rueTypeArmateur;
formFields['rueNomArmateur']                   = $qp195PE3.armateurGroup.autreRenseignements.rueNomArmateur;
formFields['rueComplementArmateur']            = $qp195PE3.armateurGroup.autreRenseignements.rueComplementArmateur;
formFields['codePostalArmateur']               = $qp195PE3.armateurGroup.autreRenseignements.codePostalArmateur;
formFields['villeArmateur']                    = $qp195PE3.armateurGroup.autreRenseignements.villeArmateur;
formFields['telephoneArmateur']                = $qp195PE3.armateurGroup.autreRenseignements.telephoneArmateur;
formFields['telecopieArmateur']                = $qp195PE3.armateurGroup.autreRenseignements.telecopieArmateur;
formFields['courrielArmateur']                 = $qp195PE3.armateurGroup.autreRenseignements.courrielArmateur;

//Renseignements sur le navire

formFields['nomNavire']                        = $qp195PE3.navireGroup.navireRenseignements.nomNavire;
formFields['puissanceNavire']                  = $qp195PE3.navireGroup.navireRenseignements.puissanceNavire;
formFields['immatriculationNavire']            = $qp195PE3.navireGroup.navireRenseignements.immatriculationNavire;
formFields['jaugeBrute']                       = $qp195PE3.navireGroup.navireRenseignements.jaugeBrute;

//Promesse d'embarquement

formFields['datEmbarquementDebut']             = $qp195PE3.embarquementGroup.promesseEmbarquement.datEmbarquement.from;
formFields['datEmbarquementFin']               = $qp195PE3.embarquementGroup.promesseEmbarquement.datEmbarquement.to;
formFields['fonction']                         = $qp195PE3.embarquementGroup.promesseEmbarquement.fonction;


//Signature

formFields['signatureConnaissanceMarin']       = $qp195PE3.signatureGroup.signature.signatureConnaissanceMarin;
formFields['signatureConnaissancesJuridiques'] = $qp195PE3.signatureGroup.signature.signatureConnaissancesJuridiques;
formFields['signatureExigencesMoralite']       = $qp195PE3.signatureGroup.signature.signatureExigencesMoralite;
formFields['certifieHonneur']                  = $qp195PE3.signatureGroup.signature.certifieHonneur;
formFields['dateSignature']                    = $qp195PE3.signatureGroup.signature.dateSignature;
formFields['lieuSignature']                    = $qp195PE3.signatureGroup.signature.lieuSignature;
formFields['signatureDeclarant']			   = $qp195PE3.information.etatCivil.nomNaissance + ' ' + $qp195PE3.information.etatCivil.prenom;


if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {
/*
 * Chargement du modele de document et injection des données
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp195PE3.signatureGroup.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
	.apply({
		date: $qp195PE3.signatureGroup.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa 15333.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.preprocess.pjID);
appendPj($attachmentPreprocess.preprocess.pjTitres);
appendPj($attachmentPreprocess.preprocess.pjAttestationECDIS);
appendPj($attachmentPreprocess.preprocess.pjCertificatAptitude);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Officier quart passerelle RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Officier chargé du quart à la passerelle sur les navires de pêche - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'officier chargé du quart à la passerelle sur les navires de pêche est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
}

function buildSimpleDocument() {

var pdfModel = pdf.create('models/cerfa 15333.pdf', formFields);
var pdfRecord = pdf.save('Officier quart passerelle RQP.pdf', pdfModel);

var data = [ spec.createData({
    id : 'record',
    label : 'Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'officier chargé du quart à la passerelle sur les navires de pêche.',
    description : 'Voici le formulaire obtenu à partir des données saisies :',
    type : 'FileReadOnly',
    value : [ pdfRecord ]
}) ];

var groups = [ spec.createGroup({
    id : 'generated',
    label : 'Génération du formulaire',
    data : data
}) ];

return spec.create({
    id : 'review',
    label : 'Officier chargé du quart à la passerelle sur les navires de pêche - demande de RQP en vue d\'un libre établissement',
    groups : groups
});
}