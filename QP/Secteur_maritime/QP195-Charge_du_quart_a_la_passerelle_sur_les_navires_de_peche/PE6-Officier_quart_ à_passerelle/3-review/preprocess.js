var cerfaFields = {};
//etatCivil

var civNomPrenom					 		= $qp195PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp195PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp195PE6.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	= civNomPrenom;
cerfaFields['villePaysNaissance']  			= $qp195PE6.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp195PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp195PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp195PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp195PE6.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp195PE6.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp195PE6.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp195PE6.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp195PE6.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp195PE6.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp195PE6.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp195PE6.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp195PE6.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp195PE6.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp195PE6.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp195PE6.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp195PE6.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp195PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp195PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp195PE6.etatCivil.identificationDeclarant.prenomDeclarant;



if (nash.doc && nash.record && nash.record.description) {
	return buildMergedDocument();
} else {
	return buildSimpleDocument();
}

function buildMergedDocument() {
/*
 * Chargement du modele de document et injection des données
 */

var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp195PE6.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
	.apply({
		date: $qp195PE6.signature.signature.dateSignature

	});

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier libre LPS V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.preprocess.pjID);
appendPj($attachmentPreprocess.preprocess.pjAttestationLE);
appendPj($attachmentPreprocess.preprocess.pjExerciceActivite);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Officier quart passerelle LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Officier chargé du quart à la passerelle sur les navires de pêche - déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'officier chargé du quart à la passerelle sur les navires de pêche est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
}

function buildSimpleDocument() {
	
var cerfa = pdf.create('models/courrier libre LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Officier quart passerelle LPS.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Officier chargé du quart à la passerelle sur les navires de pêche - déclaration préalable en vue d’une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de déclaration préalable en vue d’une libre prestation de services pour exercer la profession d\'officier chargé du quart à la passerelle sur les navires de pêche.',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
}