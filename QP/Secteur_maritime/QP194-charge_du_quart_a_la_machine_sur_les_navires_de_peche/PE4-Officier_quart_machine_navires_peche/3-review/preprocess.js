var cerfaFields = {};

//Etat civil

var civNomPrenom                     = $qp194PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp194PE4.etatCivil.identificationDeclarant.nom + ' ' + $qp194PE4.etatCivil.identificationDeclarant.prenoms + ',';
cerfaFields['nom']                   = $qp194PE4.etatCivil.identificationDeclarant.nom;
cerfaFields['Text2']                 = $qp194PE4.etatCivil.identificationDeclarant.prenoms;
cerfaFields['numeroIdentification']  = $qp194PE4.etatCivil.identificationDeclarant.numeroMarin;
cerfaFields['nationalite']           = $qp194PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['serviceMarin']          = $qp194PE4.etatCivil.identificationDeclarant.serviceMarin;
cerfaFields['nationalite']           = $qp194PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']         = $qp194PE4.etatCivil.identificationDeclarant.dateNaissance;

//Adresse contact marin
cerfaFields['typeVoie']              = $qp194PE4.adresse.adresseContact.typeVoie;
cerfaFields['extension']             = $qp194PE4.adresse.adresseContact.extension;
cerfaFields['numeroVoie']            = $qp194PE4.adresse.adresseContact.adressenumero;
cerfaFields['nomVoie']               = $qp194PE4.adresse.adresseContact.nomVoie;
cerfaFields['codePostal']            = $qp194PE4.adresse.adresseContact.adresseCodePostal;
cerfaFields['mail']                  = $qp194PE4.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['telecopie']             = $qp194PE4.adresse.adresseContact.telecopie;
cerfaFields['telephone']             = $qp194PE4.adresse.adresseContact.telephone;
cerfaFields['localite']              = $qp194PE4.adresse.adresseContact.adresseCommune;
cerfaFields['lieuDit']               = $qp194PE4.adresse.adresseContact.lieuDit;


//titre
cerfaFields['numeroTitre']          = (($qp194PE4.navire.navire.numeroTitre !=null)?($qp194PE4.navire.navire.numeroTitre):' ');
cerfaFields['dateExpiration']       = (($qp194PE4.navire.navire.dateExpiration !=null)?($qp194PE4.navire.navire.dateExpiration):' ');
cerfaFields['libelleTitre']         = (($qp194PE4.navire.navire.libelleTitre !=null)?($qp194PE4.navire.navire.libelleTitre):' ');

cerfaFields['numeroTitre1']         = (($qp194PE4.navire.navire.numeroTitre1 != null)?($qp194PE4.navire.navire.numeroTitre1):' ');
cerfaFields['dateExpiration1']      = (($qp194PE4.navire.navire.dateExpiration1 !=null)?($qp194PE4.navire.navire.dateExpiration1):' ');
cerfaFields['libelleTitre1']        = (($qp194PE4.navire.navire.libelleTitre1 !=null)?($qp194PE4.navire.navire.libelleTitre1):' ');

cerfaFields['numeroTitre2']         = (($qp194PE4.navire.navire.numeroTitre2!=null)?($qp194PE4.navire.navire.numeroTitre2):' ');
cerfaFields['dateExpiration2']      = (($qp194PE4.navire.navire.dateExpiration2!=null)?($qp194PE4.navire.navire.dateExpiration2):' ');
cerfaFields['libelleTitre2']        = (($qp194PE4.navire.navire.libelleTitre2!=null)?($qp194PE4.navire.navire.libelleTitre2):' ');

cerfaFields['numeroTitre3']         = (($qp194PE4.navire.navire.numeroTitre3!=null)?($qp194PE4.navire.navire.numeroTitre3):' ');
cerfaFields['dateExpiration3']      = (($qp194PE4.navire.navire.dateExpiration3!=null)?($qp194PE4.navire.navire.dateExpiration3):' ');
cerfaFields['libelleTitre3']        = (($qp194PE4.navire.navire.libelleTitre3!=null)?($qp194PE4.navire.navire.libelleTitre3):' ');

cerfaFields['numeroTitre4']         = (($qp194PE4.navire.navire.numeroTitre4!=null)?($qp194PE4.navire.navire.numeroTitre4):' ');
cerfaFields['dateExpiration4']      = (($qp194PE4.navire.navire.dateExpiration4!=null)?($qp194PE4.navire.navire.dateExpiration4):' ');
cerfaFields['libelleTitre4']        = (($qp194PE4.navire.navire.libelleTitre4!=null)?($qp194PE4.navire.navire.libelleTitre4):' ');

cerfaFields['numeroTitre5']         = (($qp194PE4.navire.navire.numeroTitre5!=null)?($qp194PE4.navire.navire.numeroTitre5):' ');
cerfaFields['dateExpiration5']      = (($qp194PE4.navire.navire.dateExpiration5!=null)?($qp194PE4.navire.navire.dateExpiration5):' ');
cerfaFields['libelleTitre5']        = (($qp194PE4.navire.navire.libelleTitre5!=null)?($qp194PE4.navire.navire.libelleTitre5):' ');

cerfaFields['numeroTitre6']         = (($qp194PE4.navire.navire.numeroTitre6!=null)?($qp194PE4.navire.navire.numeroTitre6):' ');
cerfaFields['dateExpiration6']      = (($qp194PE4.navire.navire.dateExpiration6!=null)?($qp194PE4.navire.navire.dateExpiration6):' ');
cerfaFields['libelleTitre6']        = (($qp194PE4.navire.navire.libelleTitre6!=null)?($qp194PE4.navire.navire.libelleTitre6):' ');


//mise a disposition
cerfaFields['envoiDomicile']        = Value('id').of($qp194PE4.miseDisposition.miseDisposition.envoiDomicile).contains('domicile')?true:' ';
cerfaFields['envoiDIRM']            = Value('id').of($qp194PE4.miseDisposition.miseDisposition.envoiDomicile).contains('drim')?true:' ';
cerfaFields['envoiAutre']           = Value('id').of($qp194PE4.miseDisposition.miseDisposition.envoiDomicile).contains('autre')?true:' ';
cerfaFields['preciser']             = $qp194PE4.miseDisposition.miseDisposition.preciser;

//Signature
cerfaFields['lieuSignature']         = $qp194PE4.signature.signature.lieuSignature;
cerfaFields['dateSignature']         = $qp194PE4.signature.signature.dateSignature;
cerfaFields['signature']             = $qp194PE4.signature.signature.signature;
cerfaFields['signatureDeclarant']    = $qp194PE4.etatCivil.identificationDeclarant.nom + ' ' + $qp194PE4.etatCivil.identificationDeclarant.prenoms;



if (nash.doc && nash.record && nash.record.description) {
    return buildMergedDocument();
} else {
    return buildSimpleDocument();
}

function buildMergedDocument() {
/*
 * Chargement du modele de document et injection des données
 */
/*
var finalDoc = nash.doc //
    .load('models/courrier_RE_dossierfinal.pdf') //
    .apply({
        numDossier: nash.record.description().recordUid,
        date: $qp194PE4.signature.signature.dateSignature,
        civiliteNomPrenom: civNomPrenom
    });
 
 */ 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
/* 
var finalDoc = nash.doc //
    .load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
    .apply({
        date: $qp194PE4.signature.signature.dateSignature

    }); */
  var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp194PE4.signature.signature.dateSignature,
		autoriteHabilitee :"Direction interrégionale de la mer",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});
	
//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/Cerfa 14949-01.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuve);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Officier_chargé_du_quart_à_la_machine_sur_les_navires_de_pêche_RQP_Renouv.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Officier chargé du quart à la machine sur les navires de pêche - déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'officier chargé du quart à la machine sur les navires de pêche est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
}
