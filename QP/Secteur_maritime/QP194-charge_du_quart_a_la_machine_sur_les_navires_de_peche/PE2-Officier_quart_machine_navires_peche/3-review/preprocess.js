var cerfaFields = {};

//Etat civil

var civNomPrenom                     = $qp194PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp194PE2.etatCivil.identificationDeclarant.nom +' '+$qp194PE2.etatCivil.identificationDeclarant.prenoms + ',';

cerfaFields['nomPrenom']             = $qp194PE2.etatCivil.identificationDeclarant.nom +' '+$qp194PE2.etatCivil.identificationDeclarant.prenoms;
cerfaFields['numeroMarin']           = $qp194PE2.etatCivil.identificationDeclarant.numeroMarin;
cerfaFields['nationalite']           = $qp194PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['identificationENIM']    = $qp194PE2.etatCivil.identificationDeclarant.numeroEnim;

//Adresse contact marin
cerfaFields['typeVoie']              = $qp194PE2.adresse.adresseContact.typeVoie;
cerfaFields['extension']             = $qp194PE2.adresse.adresseContact.extension;
cerfaFields['numeroVoie']            = $qp194PE2.adresse.adresseContact.adressenumero;
cerfaFields['nomVoie']               = $qp194PE2.adresse.adresseContact.nomVoie;
cerfaFields['codePostal']            = $qp194PE2.adresse.adresseContact.adresseCodePostal;
cerfaFields['mail']                  = $qp194PE2.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['telecopie']             = $qp194PE2.adresse.adresseContact.telecopie;
cerfaFields['telephone']             = $qp194PE2.adresse.adresseContact.telephone;
cerfaFields['localite']              = $qp194PE2.adresse.adresseContact.adresseCommune;
cerfaFields['lieuDit']               = $qp194PE2.adresse.adresseContact.lieuDit;


//qualification
cerfaFields['nomTitre']              = $qp194PE2.qualifications.qualifications.nomTitre;
cerfaFields['dateValiditeTitre']     = (($qp194PE2.qualifications.qualifications.dateValiditeTitre !=null)?($qp194PE2.qualifications.qualifications.dateValiditeTitre):' ');
cerfaFields['oui']                   = true; //Cas PE1 : RE avec titre obtenu dans EM qui réglemente
cerfaFields['non']                   = false;


//information navire
cerfaFields['exerciceFonction']      = $qp194PE2.navire.navire.exerciceFonction;
cerfaFields['navire']                = $qp194PE2.navire.navire.navire1;
cerfaFields['jauge']                 = $qp194PE2.navire.navire.jauge;
cerfaFields['puissance']             = $qp194PE2.navire.navire.puissance+'cv';
cerfaFields['dateEmbarquement']      = $qp194PE2.navire.navire.dateEmbarquement;
cerfaFields['immatricule']           = $qp194PE2.navire.navire.immatricule;

//employeur


cerfaFields['nomPrenomEmployeur']    = (($qp194PE2.employeur.employeur.nomEmployeur !=null)?($qp194PE2.employeur.employeur.nomEmployeur):' ')+' '+(($qp194PE2.employeur.employeur.prenomEmployeur !=null)?($qp194PE2.employeur.employeur.prenomEmployeur):' ');
cerfaFields['representantNomPrenom'] = (($qp194PE2.employeur.employeur.representant !=null)?($qp194PE2.employeur.employeur.representant):' ')+' '+(($qp194PE2.employeur.employeur.representant1 !=null)?($qp194PE2.employeur.employeur.representant1):' ');
cerfaFields['numeroSiret']           = $qp194PE2.employeur.employeur.numeroSiret;
cerfaFields['formeJuridique']        = $qp194PE2.employeur.employeur.formeJuridique;
cerfaFields['denomination']          = (($qp194PE2.employeur.employeur.denomination !=null)?($qp194PE2.employeur.employeur.denomination):' ');

//adresse employeur
cerfaFields['mailEmployeur']         = (($qp194PE2.employeur.employeur.employeurAdresse.mailAdresseDeclarantEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.mailAdresseDeclarantEmployeur):' ');
cerfaFields['numeroVoieEmployeur']   = (($qp194PE2.employeur.employeur.employeurAdresse.adressenumeroEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.adressenumeroEmployeur):' ');
cerfaFields['extensionEmployeur']    = (($qp194PE2.employeur.employeur.employeurAdresse.extensionEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.extensionEmployeur):' ');
cerfaFields['typeVoieEmployeur']     = (($qp194PE2.employeur.employeur.employeurAdresse.typeVoieEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.typeVoieEmployeur):' ');
cerfaFields['nomVoieEmployeur']      = (($qp194PE2.employeur.employeur.employeurAdresse.nomVoieEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.nomVoieEmployeur):' ');
cerfaFields['codePostalEmployeur']   = (($qp194PE2.employeur.employeur.employeurAdresse.adresseCodePostalEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.adresseCodePostalEmployeur):' ');
cerfaFields['telephoneEmployeur']    = (($qp194PE2.employeur.employeur.employeurAdresse.telephoneEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.telephoneEmployeur):' ');
cerfaFields['localiteEmployeur']     = (($qp194PE2.employeur.employeur.employeurAdresse.adresseCommuneEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.adresseCommuneEmployeur):' ');
cerfaFields['lieuDitEmployeur']      = (($qp194PE2.employeur.employeur.employeurAdresse.lieuDitEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.lieuDitEmployeur):' ');
cerfaFields['telecopieEmployeur']    = (($qp194PE2.employeur.employeur.employeurAdresse.telecopieEmployeur !=null)?($qp194PE2.employeur.employeur.employeurAdresse.telecopieEmployeur):' ');

//Signature
cerfaFields['lieuSignature']         = $qp194PE2.signature.signature.lieuSignature;
cerfaFields['dateSignature']         = $qp194PE2.signature.signature.dateSignature;
cerfaFields['signature']             = $qp194PE2.signature.signature.signature;


if (nash.doc && nash.record && nash.record.description) {
    return buildMergedDocument();
} else {
    return buildSimpleDocument();
}

function buildMergedDocument() {
/*
 * Chargement du modele de document et injection des données
 */
/*
var finalDoc = nash.doc //
    .load('models/courrier_RE_dossierfinal.pdf') //
    .apply({
        numDossier: nash.record.description().recordUid,
        date: $qp194PE2.signature.signature.dateSignature,
        civiliteNomPrenom: civNomPrenom
    });
 
 */ 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

/* var finalDoc = nash.doc //
    .load('models/Courrier au premier dossier v1.6 MEL1.pdf') //
    .apply({
        date: $qp194PE2.signature.signature.dateSignature

    }); */

  var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp194PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Direction interrégionale de la mer",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/CERFA 14750.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasierJudiciaire);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjniveauLangue);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Officier_chargé_du_quart_à_la_machine_sur_les_navires_de_pêche_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Officier chargé du quart à la machine sur les navires de pêche - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'officier chargé du quart à la machine sur les navires de pêche est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
}
