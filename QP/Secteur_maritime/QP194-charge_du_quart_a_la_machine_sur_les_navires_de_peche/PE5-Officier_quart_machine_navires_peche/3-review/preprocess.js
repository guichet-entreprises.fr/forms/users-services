var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp194PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp194PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp194PE5.etatCivil.identificationDeclarant.prenomDeclarant + ',';

cerfaFields['civiliteNomPrenom']            = $qp194PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp194PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp194PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']           = $qp194PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp194PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                  = $qp194PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']                = $qp194PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse']                      = $qp194PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp194PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp194PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']                    = ($qp194PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp194PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp194PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp194PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']              = $qp194PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']                = $qp194PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']                     = $qp194PE5.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                         = $qp194PE5.signature.signature.dateSignature;
cerfaFields['signature']                    = $qp194PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp194PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']   = $qp194PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp194PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp194PE5.etatCivil.identificationDeclarant.prenomDeclarant;


if (nash.doc && nash.record && nash.record.description) {
    return buildMergedDocument();
} else {
    return buildSimpleDocument();
}

function buildMergedDocument() {
/*
* Chargement du modele de document et injection des données
*/
/*
var finalDoc = nash.doc //
    .load('models/courrier_RE_dossierfinal.pdf') //
    .apply({
        numDossier: nash.record.description().recordUid,
        date: $qp194PE1.signature.signature.dateSignature,
        civiliteNomPrenom: civNomPrenom
    });

*/
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp194PE4.signature.signature.dateSignature,
		autoriteHabilitee :"Direction interrégionale de la mer",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});
	
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/courrier_libre_LPS_V3.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLPS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExercice);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuve);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Officier_chargé_du_quart_à_la_machine_sur_les_navires_de_pêche_LPS.pdf');

/*
 * Persistance des données obtenues
 */
 return spec.create({
    id : 'review',
   label : 'Officier chargé du quart à la machine sur les navires de pêche - déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession d\'officier chargé du quart à la machine sur les navires de pêche est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
}