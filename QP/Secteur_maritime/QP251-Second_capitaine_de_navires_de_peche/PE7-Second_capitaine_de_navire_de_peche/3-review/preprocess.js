var cerfaFields = {};
var civNomPrenom = $qp251PE4.information.etatCivil.civilite + ' ' + $qp251PE4.information.etatCivil.nomNaissance + ' ' + $qp251PE4.information.etatCivil.prenom;

//Civitlité

cerfaFields['civiliteNomPrenom']             = $qp251PE7.information.etatCivil.civilite + ' ' + $qp251PE7.information.etatCivil.nomNaissance + ' ' + $qp251PE7.information.etatCivil.prenom;
cerfaFields['nationalite']                   = $qp251PE7.information.etatCivil.nationalite;
cerfaFields['numeroMarin']                   = $qp251PE7.information.etatCivil.numeroMarin;
cerfaFields['ENIMAnterieur']                 = $qp251PE7.information.etatCivil.eNIMAnterieur;

//Adresse de contact

cerfaFields['rueNumeroAdresseContact']       = $qp251PE7.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
cerfaFields['rueExtensionAdresseContact']    = $qp251PE7.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
cerfaFields['rueTypeAdresseContact']         = $qp251PE7.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
cerfaFields['rueNomAdresseContact']          = $qp251PE7.adresseGroup.adresseContactGroup.rueNomAdresseContact;
cerfaFields['rueComplementAdresseContact']   = $qp251PE7.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
cerfaFields['codePostalAdresseContact']      = $qp251PE7.adresseGroup.adresseContactGroup.codePostalAdresseContact;
cerfaFields['telephoneAdresseContact']       = $qp251PE7.adresseGroup.adresseContactGroup.telephoneAdresseContact;
cerfaFields['villeAdresseContact']           = $qp251PE7.adresseGroup.adresseContactGroup.villeAdresseContact;
cerfaFields['telecopieAdresseContact']       = $qp251PE7.adresseGroup.adresseContactGroup.telecopieAdresseContact;
cerfaFields['courrielAdresseContact']        = $qp251PE7.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Qualifications

cerfaFields['titreEtrangerOui']              = ($qp251PE7.qualificationsGroup.qualifications.titreEtranger ? true : '');
cerfaFields['titreEtrangerNon']              = ($qp251PE7.qualificationsGroup.qualifications.titreEtranger ? '' : true);
cerfaFields['nomTitre']                      = $qp251PE7.qualificationsGroup.qualifications.nomTitre;
cerfaFields['dateValiditeTitre']             = $qp251PE7.qualificationsGroup.qualifications.dateValiditeTitre;

//Navire

cerfaFields['fonction']                      = $qp251PE7.informationsCompNavire.navireGroup.fonction;
cerfaFields['nomNavire']                     = $qp251PE7.informationsCompNavire.navireGroup.nomNavire;
cerfaFields['jaugeBrute']                    = $qp251PE7.informationsCompNavire.navireGroup.jaugeBrute;
cerfaFields['puissanceNavire']               = $qp251PE7.informationsCompNavire.navireGroup.puissanceNavire;
cerfaFields['immatriculationNavire']			= $qp251PE7.informationsCompNavire.navireGroup.immatriculationNavire;
cerfaFields['dateEmbarquementPrevue']        = $qp251PE7.informationsCompNavire.navireGroup.dateEmbarquementPrevue;

//Employeur

cerfaFields['employeurNomPrenom']            = $qp251PE7.informationsCompEmployeur.employeurGroup.employeurNomPrenom;
cerfaFields['employeurDenomination']         = $qp251PE7.informationsCompEmployeur.employeurGroup.employeurDenomination;
cerfaFields['employeurRepresentant']         = $qp251PE7.informationsCompEmployeur.employeurGroup.employeurRepresentant;
cerfaFields['numeroSiret']                   = $qp251PE7.informationsCompEmployeur.employeurGroup.numeroSiret;
cerfaFields['employeurFormeJuridique']       = $qp251PE7.informationsCompEmployeur.employeurGroup.employeurFormeJuridique;
cerfaFields['rueNumeroAdresseEmployeur']     = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueNumeroAdresseEmployeur;
cerfaFields['rueExtensionAdresseEmployeur']  = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueExtensionAdresseEmployeur;
cerfaFields['rueTypeAdresseEmployeur']       = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueTypeAdresseEmployeur;
cerfaFields['rueNomAdresseEmployeur']        = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueNomAdresseEmployeur;
cerfaFields['rueComplementAdresseEmployeur'] = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueComplementAdresseEmployeur;
cerfaFields['codePostalAdresseEmployeur']    = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.codePostalAdresseEmployeur;
cerfaFields['villeAdresseEmployeur']         = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.villeAdresseEmployeur;
cerfaFields['telephoneAdresseEmployeur']     = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.telephoneAdresseEmployeur;
cerfaFields['telecopieAdresseEmployeur']     = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.telecopieAdresseEmployeur;
cerfaFields['courrielAdresseEmployeur']      = $qp251PE7.informationsCompEmployeur.employeurGroup.adresseEmployeur.courrielAdresseEmployeur;

//Signature

cerfaFields['lieuSignature']                 = $qp251PE7.signatureGroup.signatureG.lieuSignature;
cerfaFields['dateSignature']                 = $qp251PE7.signatureGroup.signatureG.dateSignature;
cerfaFields['signature']                     = $qp251PE7.signatureGroup.signatureG.signature;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp251PE7.signatureGroup.signatureG.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp251PE7.signatureGroup.signatureG.dateSignature,
		autoriteHabilitee :"Directeur interrégional de la mer",
		demandeContexte : "Renouvellement de la déclaration en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});
	
 var cerfaDoc = nash.doc //
    .load('models/cerfa_14750.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDeclaration);


var finalDocItem = finalDoc.save('Second_capitaine_de_navire_de_peche_RQP_Renouv.pdf');


return spec.create({
    id : 'review',
   label : 'Second capitaine de navire de pêche - renouvellement de la déclaration en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de renouvellement de la déclaration en vue d\'une libre prestation de services pour l\'exercice de la profession de Second capitaine de navire de pêche.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});