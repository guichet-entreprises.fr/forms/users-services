var cerfaFields = {};
var civNomPrenom = $qp251PE5.information.etatCivil.civilite + ' ' + $qp251PE5.information.etatCivil.nomNaissance + ' ' + $qp251PE5.information.etatCivil.prenom;

//Civitlité

cerfaFields['civiliteNomPrenom']             = $qp251PE5.information.etatCivil.civilite + ' ' + $qp251PE5.information.etatCivil.nomNaissance + ' ' + $qp251PE5.information.etatCivil.prenom;
cerfaFields['nationalite']                   = $qp251PE5.information.etatCivil.nationalite;
cerfaFields['numeroMarin']                   = $qp251PE5.information.etatCivil.numeroMarin;
cerfaFields['ENIMAnterieur']                 = $qp251PE5.information.etatCivil.eNIMAnterieur;

//Adresse de contact

cerfaFields['rueNumeroAdresseContact']       = $qp251PE5.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
cerfaFields['rueExtensionAdresseContact']    = $qp251PE5.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
cerfaFields['rueTypeAdresseContact']         = $qp251PE5.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
cerfaFields['rueNomAdresseContact']          = $qp251PE5.adresseGroup.adresseContactGroup.rueNomAdresseContact;
cerfaFields['rueComplementAdresseContact']   = $qp251PE5.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
cerfaFields['codePostalAdresseContact']      = $qp251PE5.adresseGroup.adresseContactGroup.codePostalAdresseContact;
cerfaFields['telephoneAdresseContact']       = $qp251PE5.adresseGroup.adresseContactGroup.telephoneAdresseContact;
cerfaFields['villeAdresseContact']           = $qp251PE5.adresseGroup.adresseContactGroup.villeAdresseContact;
cerfaFields['telecopieAdresseContact']       = $qp251PE5.adresseGroup.adresseContactGroup.telecopieAdresseContact;
cerfaFields['courrielAdresseContact']        = $qp251PE5.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Qualifications

cerfaFields['titreEtrangerOui']              = ($qp251PE5.qualificationsGroup.qualifications.titreEtranger ? true : '');
cerfaFields['titreEtrangerNon']              = ($qp251PE5.qualificationsGroup.qualifications.titreEtranger ? '' : true);
cerfaFields['nomTitre']                      = $qp251PE5.qualificationsGroup.qualifications.nomTitre;
cerfaFields['dateValiditeTitre']             = $qp251PE5.qualificationsGroup.qualifications.dateValiditeTitre;

//Navire

cerfaFields['fonction']                      = $qp251PE5.informationsCompNavire.navireGroup.fonction;
cerfaFields['nomNavire']                     = $qp251PE5.informationsCompNavire.navireGroup.nomNavire;
cerfaFields['jaugeBrute']                    = $qp251PE5.informationsCompNavire.navireGroup.jaugeBrute;
cerfaFields['puissanceNavire']               = $qp251PE5.informationsCompNavire.navireGroup.puissanceNavire;
cerfaFields['immatriculationNavire']			= $qp251PE5.informationsCompNavire.navireGroup.immatriculationNavire;
cerfaFields['dateEmbarquementPrevue']        = $qp251PE5.informationsCompNavire.navireGroup.dateEmbarquementPrevue;

//Employeur

cerfaFields['employeurNomPrenom']            = $qp251PE5.informationsCompEmployeur.employeurGroup.employeurNomPrenom;
cerfaFields['employeurDenomination']         = $qp251PE5.informationsCompEmployeur.employeurGroup.employeurDenomination;
cerfaFields['employeurRepresentant']         = $qp251PE5.informationsCompEmployeur.employeurGroup.employeurRepresentant;
cerfaFields['numeroSiret']                   = $qp251PE5.informationsCompEmployeur.employeurGroup.numeroSiret;
cerfaFields['employeurFormeJuridique']       = $qp251PE5.informationsCompEmployeur.employeurGroup.employeurFormeJuridique;
cerfaFields['rueNumeroAdresseEmployeur']     = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueNumeroAdresseEmployeur;
cerfaFields['rueExtensionAdresseEmployeur']  = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueExtensionAdresseEmployeur;
cerfaFields['rueTypeAdresseEmployeur']       = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueTypeAdresseEmployeur;
cerfaFields['rueNomAdresseEmployeur']        = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueNomAdresseEmployeur;
cerfaFields['rueComplementAdresseEmployeur'] = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueComplementAdresseEmployeur;
cerfaFields['codePostalAdresseEmployeur']    = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.codePostalAdresseEmployeur;
cerfaFields['villeAdresseEmployeur']         = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.villeAdresseEmployeur;
cerfaFields['telephoneAdresseEmployeur']     = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.telephoneAdresseEmployeur;
cerfaFields['telecopieAdresseEmployeur']     = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.telecopieAdresseEmployeur;
cerfaFields['courrielAdresseEmployeur']      = $qp251PE5.informationsCompEmployeur.employeurGroup.adresseEmployeur.courrielAdresseEmployeur;

//Signature

cerfaFields['lieuSignature']                 = $qp251PE5.signatureGroup.signatureG.lieuSignature;
cerfaFields['dateSignature']                 = $qp251PE5.signatureGroup.signatureG.dateSignature;
cerfaFields['signature']                     = $qp251PE5.signatureGroup.signatureG.signature;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp251PE5.signatureGroup.signatureG.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp251PE5.signatureGroup.signatureG.dateSignature,
		autoriteHabilitee :"Directeur interrégional de la mer",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d\'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});
	
 var cerfaDoc = nash.doc //
    .load('models/cerfa_14750.pdf') //
    .apply(cerfaFields);
finalDoc.append(cerfaDoc.save('cerfa.pdf'));
	
function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);


var finalDocItem = finalDoc.save('Second_capitaine_de_navire_de_peche_RQP.pdf');


return spec.create({
    id : 'review',
   label : 'Second capitaine de navire de pêche - déclaration préalable en vue d\'une libre prestation de services.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de Second capitaine de navire de pêche.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});