msgid "Région administrative du port d'armement du navire :"
msgstr "Administrative region of the port of registry of the ship"

msgid "Vous souhaitez faire reconnaitre vos qualifications professionnelles pour exercer la profession d'officier chargé du quart à la passerelle sur les navires de pêche en France."
msgstr "You wish to make recognize your professional qualifications to practise as officer in charge of the watch on the bridge of a fishing vessel"

msgid "Madame"
msgstr "Mrs"

msgid "Monsieur"
msgstr "Mr"

msgid "Vous confirmez :"
msgstr "You confirm:"

msgid "Etre ressortissant d'un Etat membre de l'Union européenne ou partie à l'Espace économique européen, sans qualification professionnelle obtenue en France."
msgstr "Being a national of a non-member state of the European Union or not a party to the European Economic Area."

msgid "Vouloir exercer en France de façon permanente ou pour un temps indéterminé."
msgstr "Want to practice in France in a permanent way or for a while indefinite."

msgid "Ne pas avoir déjà exercé en France (ne pas avoir déjà obtenu une autorisation d'exercer en France)."
msgstr "Not have already practiced in France (not have already obtained an authorization to practice in France)."

msgid "Avoir obtenu vos qualifications professionnelles dans un Etat membre de l'Union européenne ou partie à l'Espace économique européen réglementant la profession."
msgstr "Have obtained your professional qualifications in a member state of the European Union or left to the European Economic Area regulating the profession."

msgid "Officier chargé du quart à la passerelle sur les navires de pêche - demande de reconnaissance de qualifications professionnelles en vue d'un libre établissement."
msgstr "Officer in charge of the watch on the bridge of a fishing vessel - application for recognition of professional qualifications for a free establishment."

msgid "Etat civil"
msgstr "Civil status"

msgid "Identification du déclarant"
msgstr "Identification of the declarant"

msgid "Formulaire de demande d'autorisation d'exercice"
msgstr "Application Form"
 
msgid "Civilité :"
msgstr "Civility :"

msgid "Il s'agit d'indiquer : Monsieur pour un homme, Madame pour une femme. Remarque : le terme Mademoiselle n'est plus utilisé en France il convient donc dans ce cas de selectionner Madame."
msgstr "Indicate: Mr for a man, Mrs for a woman."

msgid "Nom de naissance :"
msgstr "Birth name :"

msgid "Il s'agit du nom qui a été donné à la naissance tel qu'il apparaît sur le justificatif d'identité."
msgstr "This is the name which was given to the birth such as it appears on the documentary evidence of identity."

msgid "Nom d'usage :"
msgstr "Use name :"

msgid "Prénom(s) :"
msgstr "First name(s) :"

msgid "Il s'agit de l'ensemble des prénoms figurant sur le justificatif d'identité. L'ordre indiqué sur le justificatif d'identité doit être respecté et les prénoms doivent êtres séparés par une virgule."
msgstr "All first names appearing on the documentary evidence of identity have to be indicated."

msgid "Date de naissance :"
msgstr "Birth date :"

msgid "Commune ou ville de naissance :"
msgstr "Town or city of birth :"

msgid "Pays de naissance :"
msgstr "Native country :"

msgid "Nationalité :"
msgstr "Nationality :"

msgid "Il s'agit d'indiquer en toutes lettres la nationalité."
msgstr "The nationality should be written in full."

msgid "Numéro de marin :"
msgstr "Sailor number :"

msgid "Il convient de renseigner ici votre numéro de marin. Il se compose de 8 chiffres (année + immatriculation. Ex : 20005544)."
msgstr "It is necessary to inform here your number of seaman. It consists of 8 digits (year + registration number, eg 20005544)"

msgid "Le cas échéant, numéro d'identification ENIM (régime social des marins) antérieur :"
msgstr "Where applicable, previous identification number (ENIM) of the seafarers :"

msgid "Le numéro d’identification est formé de deux lettres correspondant à la DML (Délégation de la Mer et du Littoral) ou à la DM (Délégation de la Mer) d’identification, de deux chiffres du millésime, d’une lettre clé et de quatre chiffres (exemple : MA 67 W 2217)."
msgstr "The identification number is made up of two letters corresponding to the Delegation of the Sea and the Littoral or to the Delegation of the Sea identification, two digits of the vintage, a key letter and four digits (example: MA 67 W 2217)"

msgid "Coordonnées"
msgstr "Contact information"

msgid "Numéro de voie :"
msgstr "Address number :"

msgid "Il convient d'indiquer le numéro de voie."
msgstr "Indicate here the address number."

msgid "Extension du numéro de voie :"
msgstr "Extension of the address number :"

msgid "Il convient de renseigner ici l'extension du numéro (bis, ter, etc)."
msgstr "The extension of the number (bis, ter, etc) should be given here."

msgid "Type de voie :"
msgstr "Street type :"

msgid "Il convient d'indiquer le type de voie. Ex : Rue, Avenue etc."
msgstr "Indicate here the type of way. Ex: street, Avenue etc."

msgid "Nom de voie :"
msgstr "Street name :"

msgid "Il convient d'indiquer le nom de voie."
msgstr "Indicate here the name of the street."

msgid "Lieu dit ou boite postale :"
msgstr "Place called or P.O. Box :"

msgid "Code postal :"
msgstr "Zip code :"

msgid "Il convient d'indiquer le code postal de la localité."
msgstr "Indicate here the postal code."

msgid "Commune ou ville :"
msgstr "City name :"

msgid "Il convient d'indiquer la localité."
msgstr "Indicate here the name of the town."

msgid "Téléphone :"
msgstr "Phone number :"

msgid "Il convient de séléctionner le pays d'attribution du numéro et de renseigner le numéro de téléphone fixe ou mobile."
msgstr "Indicate here the phone number."

msgid "Télécopie :"
msgstr "Fax number :"

msgid "Il convient de sélectionner le pays d'attribution du numéro et de renseigner le numéro de fax."
msgstr "Indicate here the fax number."

msgid "Courriel :"
msgstr "Email :"

msgid "Il convient de renseigner une adresse électronique valide."
msgstr "Indicate here a valid email address."

msgid "Qualifications acquises dans des Etats membres de l'Union européenne ou parties à l'accord sur l'Espace économique européen"
msgstr "Qualifications acquired in member States of the European Union or parties to the agreement on the European Economic Area."

msgid "Date de validité du titre :"
msgstr "Date of validity of the title :"

msgid "Nom et prérogatives du titre :"
msgstr "Name and prerogatives of title :"

msgid "Informations relatives au navire"
msgstr "Ship's informations"

msgid "Fonction exercée :"
msgstr "Function exerciced :"

msgid "Il convient d'indiquer la fonction exercée sur le navire."
msgstr "Indicate here the function performed on the vessel."

msgid "Nom du navire :"
msgstr "Ship's name :"

msgid "Il convient d'indiquer le nom du navire."
msgstr "Indicate here the vessel's name."

msgid "Jauge brute :"
msgstr "Gross tonnage :"

msgid "Il convient d'indiquer la capacité de transport d'un navire. Elle est exprimée en tonneaux de jauge brute (tjb) ou en UMS pour les navires d'une longueur supérieure à 24 mètres."
msgstr "Indicate here the transport capacity of the ship. She is expressed in barrels of gross tonnage (tjb) or in UMS for the ships of a length superior to 24 meters."

msgid "Puissance machine :"
msgstr "Machinery power :"

msgid "Il convient d'indiquer la puissance des machines embarquées. Elle est exprimée en chevaux (cv)."
msgstr "It is advisable to indicate the power of the embarked machines. She is expressed in horses (cv)."

msgid "Immatriculation du navire :"
msgstr "Ship registration :"

msgid "Il convient d'indiquer l'immatriculation du navire."
msgstr "Indicate here the registration of the ship."

msgid "Date prévue d'embarquement :"
msgstr "Schedule boarding date :"

msgid "Informations et coordonnées complémentaires"
msgstr "Additional information"

msgid "Votre employeur est :"
msgstr "Your employer is :"

msgid "Une personne physique (être humain majeur doté, en tant que tel, de la personnalité juridique (aptitude à être titulaire de droits et de devoirs))."
msgstr "A natural person (endowed major human being, as such, of the legal entity (capacity to be a holder of rights and duties))"

msgid "Une personne morale (entité juridique ou groupement d'individus à laquelle la loi confère des droits semblables à ceux des personnes physiques)."
msgstr "A legal entity (legal entity or grouping of individuals on which the law confers rights similar to those of the physical persons)."

msgid "Nom de l'employeur :"
msgstr "Last name of the employer :"

msgid "Prénom de l'employeur :"
msgstr "First name of the employer :"

msgid "Dénomination :"
msgstr "Corporate name :"

msgid "Nom du représentant de l'entité morale :"
msgstr "Last name of the representative of the moral entity :"

msgid "Prénom du représentant de l'entité morale :"
msgstr "First name of the representative of the moral entity :"

msgid "Numéro de SIRET :"
msgstr "SIRET number :"

msgid "Il convient d'indiquer le numéro de SIRET de l'employeur. Il se compose de 14 chiffres."
msgstr "Indicate here the SIRET number of the employer."

msgid "Forme juridique :"
msgstr "Legal status :"

msgid "Il convient ici de spécifier la forme juridique de l'entreprise (ex : SA, SARL, EI…)."
msgstr "Indicate here the legal form of the company."

msgid "Informations adresse employeur"
msgstr "Address information of the employer"

msgid "Dans quelle région de France exercerez-vous ?"
msgstr "In which region will you practice?"

msgid "Signature de la formalité"
msgstr "Signature"
msgid "Signature"
msgstr "Signature"

msgid "Fait à :"
msgstr "Done at :"

msgid "Il convient d'indiquer ici votre lieu actuel."
msgstr "It is advisable to indicate here your current place."

msgid "Le :"
msgstr "Date :"

msgid "Il convient d'indiquer ici la date du jour."
msgstr "It is advisable to indicate here the date of day"

msgid "Je reconnais avoir pris connaissance de l'arrêté du 8 février 2010 modifié relatif à la reconnaissance des qualifications professionnelles pour l'exercice de fonctions principales à bord des navires de pêche et des navires armés en cultures marines à bord d'un navire battant pavillon français par les titulaires de qualifications acquises dans les Etats membres de la communauté européenne autres que la France ou dans des Etats parties à l'accord sur l'Espace économique européen, notamment l'article 12 relatif au contrôle des connaissances linguistiques."
msgstr "I recognize to have acquainted with the order of February 8th, 2010 modified concerning the gratitude of the professional qualifications for the exercise of main functions aboard fishing ships and with ships armed in marine cultures aboard a ship flying the French flag by the holders of qualifications acquired in the member states of the European community other than France or in States parts in the agreement on the European Economic Area, in particular the article 12 concerning the assessment linguistics"

msgid "Je déclare sur l'honneur l'exactitude des informations de la formalité."
msgstr "I declare on the honor the accuracy of the information of the formality."

msgid "Copie de la pièce d’identité."
msgstr "Copy of the ID card."

msgid "Extrait d'acte de naissance, fiche d'état civil, carte d'identité ou passeport en cours de validité, recto verso si besoin."
msgstr "Birth certificate, birth and marriage certificate, ID card or valid, both sides passport if need."

msgid "Copie des attestations des autorités ayant délivré le titre de formation, spécifiant le niveau de la formation et, année par année, le détail et le volume horaire des enseignements suivis ainsi que le contenu et la durée des stages validés."
msgstr "Copy of the certificates of the authorities having freed the title of training, specifying the level of training and, year a year, the detail and the hourly volume of the followed teachings as well as the contents and the duration of the validated internships."

msgid "Copie du certificat d’aptitude physique à la navigation en cours de validité."
msgstr "Valid copy of a physical fitness examination certifiacte."

msgid "Copie d'une attestation prouvant la maitrise suffisante de la langue française."
msgstr "Copy of the certificate proving the sufficient control of the French language."

msgid "Attestation de qualification délivrée à l’issue d’une formation en français, attestation de niveau en français délivrée par une institution spécialisée, document attestant d’une expérience professionnelle acquise en France ou attestation de reconnaissance des capacités professionnelles à la pêche délivrée antérieurement à la parution de l'arrêté du 08 février 2010 qui prévoit, pour un marin employé sur un navire de pêche battant pavillon français, le contrôle de la langue français."
msgstr "Certificate of qualification freed at the end of a training in French, level certificate in French freed by a specialized institution or a document giving evidence of a work experience acquired in France."

msgid "Génération du formulaire"
msgstr "Generation of the form"

msgid "Formulaire de demande de reconnaissance de qualifications professionnelles pour la profession d\'officier chargé du quart à la passerelle sur les navires de pêche."
msgstr "Request form of recognition of professional qualifications for the profession of officer in charge of the watch on the bridge of a fishing vessel."

msgid "Voici le formulaire obtenu à partir des données saisies :"
msgstr "Here is the form obtained from the seized data :"

msgid "Remerciements"
msgstr "Thanks"

msgid "Merci d'avoir traité cette formalité sur le site www.guichet-qualifications.fr. Votre dossier sera transmis à l'autorité compétente pour traitement."
msgstr "Thank you for having handled this formality on the site to www.guichet-qualifications.fr. Your file will be transmitted in the competent authority for treatment."

msgid "Génération du dossier"
msgstr "Generation of the file"

msgid "La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession d\'officier chargé du quart à la passerelle sur les navires de pêche est maintenant terminée."
msgstr "The process of applying for recognition of professional qualifications for a free establishment for the occupation of officer in charge of the watch on the bridge of a fishing vessel is now ended"

msgid "Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie."
msgstr "Here is the application file, including the completed form from the data entered, attachments and an accompanying mail. Please check the accuracy of the information you can modify if necessary by returning to the input pages."
