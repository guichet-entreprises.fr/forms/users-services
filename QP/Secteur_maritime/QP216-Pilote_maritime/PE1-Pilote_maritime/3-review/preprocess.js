var formFields = {};

//Etat civil

var civNomPrenom                               = $qp216PE1.information.etatCivil.civilite + ' ' + $qp216PE1.information.etatCivil.nomNaissance +' '+$qp216PE1.information.etatCivil.prenom;

formFields['civiliteNomPrenom']                = civNomPrenom;
formFields['dateNaissance']                    = $qp216PE1.information.etatCivil.dateNaissance;
formFields['lieuNaissance']                    = $qp216PE1.information.etatCivil.lieuNaissance;
formFields['nationalite']                      = $qp216PE1.information.etatCivil.nationalite;
formFields['numeroIdentification']             = $qp216PE1.information.etatCivil.numeroIdentification;

//Adresse de contact

formFields['rueNumeroAdresseContact']          = $qp216PE1.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
formFields['rueExtensionAdresseContact']       = $qp216PE1.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
formFields['rueTypeAdresseContact']            = $qp216PE1.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
formFields['rueNomAdresseContact']             = $qp216PE1.adresseGroup.adresseContactGroup.rueNomAdresseContact;
formFields['rueComplementAdresseContact']      = $qp216PE1.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
formFields['codePostalAdresseContact']         = $qp216PE1.adresseGroup.adresseContactGroup.codePostalAdresseContact;
formFields['villeAdresseContact']              = $qp216PE1.adresseGroup.adresseContactGroup.villeAdresseContact;
formFields['telephoneAdresseContact']          = $qp216PE1.adresseGroup.adresseContactGroup.telephoneAdresseContact;
formFields['telecopieAdresseContact']          = $qp216PE1.adresseGroup.adresseContactGroup.telecopieAdresseContact;
formFields['courrielAdresseContact']           = $qp216PE1.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Titre de formation

formFields['numTitre1']                        = $qp216PE1.formationGroup.titreFormation.numTitre1;
formFields['libelleTitre1']                    = $qp216PE1.formationGroup.titreFormation.libelleTitre1;
formFields['dateExpiration1']                  = $qp216PE1.formationGroup.titreFormation.dateExpiration1;
formFields['etatEmetteur1']                    = $qp216PE1.formationGroup.titreFormation.etatEmetteur1;
formFields['numTitre2']                        = $qp216PE1.formationGroup.titreFormation.numTitre2;
formFields['libelleTitre2']                    = $qp216PE1.formationGroup.titreFormation.libelleTitre2;
formFields['dateExpiration2']                  = $qp216PE1.formationGroup.titreFormation.dateExpiration2;
formFields['etatEmetteur2']                    = $qp216PE1.formationGroup.titreFormation.etatEmetteur2;
formFields['numTitre3']                        = $qp216PE1.formationGroup.titreFormation.numTitre3;
formFields['libelleTitre3']                    = $qp216PE1.formationGroup.titreFormation.libelleTitre3;
formFields['dateExpiration3']                  = $qp216PE1.formationGroup.titreFormation.dateExpiration3;
formFields['etatEmetteur3']                    = $qp216PE1.formationGroup.titreFormation.etatEmetteur3;
formFields['numTitre4']                        = $qp216PE1.formationGroup.titreFormation.numTitre4;
formFields['libelleTitre4']                    = $qp216PE1.formationGroup.titreFormation.libelleTitre4;
formFields['dateExpiration4']                  = $qp216PE1.formationGroup.titreFormation.dateExpiration4;
formFields['etatEmetteur4']                    = $qp216PE1.formationGroup.titreFormation.dateExpiration4;

//Renseignements sur l'armateur

formFields['denominationSocialeArmateur']      = $qp216PE1.armateurGroup.autreRenseignements.denominationSocialeArmateur;
formFields['representantLegalArmateur']        = $qp216PE1.armateurGroup.autreRenseignements.representantLegalArmateurNom + ' ' + $qp216PE1.armateurGroup.autreRenseignements.representantLegalArmateurPrenom;
formFields['numeroSiret']                      = $qp216PE1.armateurGroup.autreRenseignements.numeroSiret;
formFields['rueNumeroArmateur']                = $qp216PE1.armateurGroup.autreRenseignements.rueNumeroArmateur;
formFields['rueExtensionArmateur']             = $qp216PE1.armateurGroup.autreRenseignements.rueExtensionArmateur;
formFields['rueTypeArmateur']                  = $qp216PE1.armateurGroup.autreRenseignements.rueTypeArmateur;
formFields['rueNomArmateur']                   = $qp216PE1.armateurGroup.autreRenseignements.rueNomArmateur;
formFields['rueComplementArmateur']            = $qp216PE1.armateurGroup.autreRenseignements.rueComplementArmateur;
formFields['codePostalArmateur']               = $qp216PE1.armateurGroup.autreRenseignements.codePostalArmateur;
formFields['villeArmateur']                    = $qp216PE1.armateurGroup.autreRenseignements.villeArmateur;
formFields['telephoneArmateur']                = $qp216PE1.armateurGroup.autreRenseignements.telephoneArmateur;
formFields['telecopieArmateur']                = $qp216PE1.armateurGroup.autreRenseignements.telecopieArmateur;
formFields['courrielArmateur']                 = $qp216PE1.armateurGroup.autreRenseignements.courrielArmateur;

//Renseignements sur le navire

formFields['nomNavire']                        = $qp216PE1.navireGroup.navireRenseignements.nomNavire;
formFields['puissanceNavire']                  = $qp216PE1.navireGroup.navireRenseignements.puissanceNavire;
formFields['immatriculationNavire']            = $qp216PE1.navireGroup.navireRenseignements.immatriculationNavire;
formFields['jaugeBrute']                       = $qp216PE1.navireGroup.navireRenseignements.jaugeBrute;

//Promesse d'embarquement

formFields['datEmbarquementDebut']             = $qp216PE1.embarquementGroup.promesseEmbarquement.datEmbarquement.from;
formFields['datEmbarquementFin']               = $qp216PE1.embarquementGroup.promesseEmbarquement.datEmbarquement.to;
formFields['fonction']                         = $qp216PE1.embarquementGroup.promesseEmbarquement.fonction;


//Signature

formFields['signatureConnaissanceMarin']       = $qp216PE1.signatureGroup.signature.signatureConnaissanceMarin;
formFields['signatureConnaissancesJuridiques'] = $qp216PE1.signatureGroup.signature.signatureConnaissancesJuridiques;
formFields['signatureExigencesMoralite']       = $qp216PE1.signatureGroup.signature.signatureExigencesMoralite;
formFields['certifieHonneur']                  = $qp216PE1.signatureGroup.signature.certifieHonneur;
formFields['dateSignature']                    = $qp216PE1.signatureGroup.signature.dateSignature;
formFields['lieuSignature']                    = $qp216PE1.signatureGroup.signature.lieuSignature;
formFields['signatureDeclarant']               = $qp216PE1.information.etatCivil.nomNaissance + ' ' + $qp216PE1.information.etatCivil.prenom;


/*
 * Chargement du modele de document et injection des données
 */

var finalDoc = nash.doc //
    .load('models/courrier_RE_dossierfinal.pdf') //
    .apply({
        numDossier: nash.record.description().recordUid,
        date: $qp216PE1.signatureGroup.signature.dateSignature,
        civiliteNomPrenom: civNomPrenom
    });
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
    .load('models/Courrier au premier dossier v2.1 GQ.pdf') //
    .apply({
        date: $qp216PE1.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Direction interrégionale de la mer",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom

    });

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/cerfa 15333.pdf') //
    .apply(formFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationECDIS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificatAptitude);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Pilote_maritime_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Pilote maritime - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de pilote maritime est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
