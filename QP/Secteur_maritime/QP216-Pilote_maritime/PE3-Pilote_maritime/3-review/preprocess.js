var cerfaFields = {};

//Etat civil

var civNomPrenom                     = $qp216PE3.etatCivil.identificationDeclarant.civilite + ' ' + $qp216PE3.etatCivil.identificationDeclarant.nom +' '+$qp216PE3.etatCivil.identificationDeclarant.prenoms;

cerfaFields['nomPrenom']             = $qp216PE3.etatCivil.identificationDeclarant.nom +' '+$qp216PE3.etatCivil.identificationDeclarant.prenoms;
cerfaFields['numeroMarin']           = $qp216PE3.etatCivil.identificationDeclarant.numeroMarin;
cerfaFields['nationalite']           = $qp216PE3.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['identificationENIM']    = $qp216PE3.etatCivil.identificationDeclarant.numeroEnim;

//Adresse contact marin
cerfaFields['typeVoie']              = $qp216PE3.adresse.adresseContact.typeVoie;
cerfaFields['extension']             = $qp216PE3.adresse.adresseContact.extension;
cerfaFields['numeroVoie']            = $qp216PE3.adresse.adresseContact.adressenumero;
cerfaFields['nomVoie']               = $qp216PE3.adresse.adresseContact.nomVoie;
cerfaFields['codePostal']            = $qp216PE3.adresse.adresseContact.adresseCodePostal;
cerfaFields['mail']                  = $qp216PE3.adresse.adresseContact.mailAdresseDeclarant;
cerfaFields['telecopie']             = $qp216PE3.adresse.adresseContact.telecopie;
cerfaFields['telephone']             = $qp216PE3.adresse.adresseContact.telephone;
cerfaFields['localite']              = $qp216PE3.adresse.adresseContact.adresseCommune;
cerfaFields['lieuDit']               = $qp216PE3.adresse.adresseContact.lieuDit;


//qualification
cerfaFields['nomTitre']              = $qp216PE3.qualifications.qualifications.nomTitre;
cerfaFields['dateValiditeTitre']     = (($qp216PE3.qualifications.qualifications.dateValiditeTitre !=null)?($qp216PE3.qualifications.qualifications.dateValiditeTitre):' ');
cerfaFields['oui']                   = true; //Cas PE1 : RE avec titre obtenu dans EM qui réglemente
cerfaFields['non']                   = false;


//information navire
cerfaFields['exerciceFonction']      = $qp216PE3.navire.navire.exerciceFonction;
cerfaFields['navire']                = $qp216PE3.navire.navire.navire1;
cerfaFields['jauge']                 = $qp216PE3.navire.navire.jauge;
cerfaFields['puissance']             = $qp216PE3.navire.navire.puissance+'cv';
cerfaFields['dateEmbarquement']      = $qp216PE3.navire.navire.dateEmbarquement;
cerfaFields['immatricule']           = $qp216PE3.navire.navire.immatricule;

//employeur


cerfaFields['nomPrenomEmployeur']    = (($qp216PE3.employeur.employeur.nomEmployeur !=null)?($qp216PE3.employeur.employeur.nomEmployeur):' ')+' '+(($qp216PE3.employeur.employeur.prenomEmployeur !=null)?($qp216PE3.employeur.employeur.prenomEmployeur):' ');
cerfaFields['representantNomPrenom'] = (($qp216PE3.employeur.employeur.representant !=null)?($qp216PE3.employeur.employeur.representant):' ')+' '+(($qp216PE3.employeur.employeur.representant1 !=null)?($qp216PE3.employeur.employeur.representant1):' ');
cerfaFields['numeroSiret']           = $qp216PE3.employeur.employeur.numeroSiret;
cerfaFields['formeJuridique']        = $qp216PE3.employeur.employeur.formeJuridique;
cerfaFields['denomination']          = (($qp216PE3.employeur.employeur.denomination !=null)?($qp216PE3.employeur.employeur.denomination):' ');

//adresse employeur
cerfaFields['mailEmployeur']         = (($qp216PE3.employeur.employeur.employeurAdresse.mailAdresseDeclarantEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.mailAdresseDeclarantEmployeur):' ');
cerfaFields['numeroVoieEmployeur']   = (($qp216PE3.employeur.employeur.employeurAdresse.adressenumeroEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.adressenumeroEmployeur):' ');
cerfaFields['extensionEmployeur']    = (($qp216PE3.employeur.employeur.employeurAdresse.extensionEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.extensionEmployeur):' ');
cerfaFields['typeVoieEmployeur']     = (($qp216PE3.employeur.employeur.employeurAdresse.typeVoieEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.typeVoieEmployeur):' ');
cerfaFields['nomVoieEmployeur']      = (($qp216PE3.employeur.employeur.employeurAdresse.nomVoieEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.nomVoieEmployeur):' ');
cerfaFields['codePostalEmployeur']   = (($qp216PE3.employeur.employeur.employeurAdresse.adresseCodePostalEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.adresseCodePostalEmployeur):' ');
cerfaFields['telephoneEmployeur']    = (($qp216PE3.employeur.employeur.employeurAdresse.telephoneEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.telephoneEmployeur):' ');
cerfaFields['localiteEmployeur']     = (($qp216PE3.employeur.employeur.employeurAdresse.adresseCommuneEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.adresseCommuneEmployeur):' ');
cerfaFields['lieuDitEmployeur']      = (($qp216PE3.employeur.employeur.employeurAdresse.lieuDitEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.lieuDitEmployeur):' ');
cerfaFields['telecopieEmployeur']    = (($qp216PE3.employeur.employeur.employeurAdresse.telecopieEmployeur !=null)?($qp216PE3.employeur.employeur.employeurAdresse.telecopieEmployeur):' ');

//Signature
cerfaFields['lieuSignature']         = $qp216PE3.signature.signature.lieuSignature;
cerfaFields['dateSignature']         = $qp216PE3.signature.signature.dateSignature;
cerfaFields['signature']             = $qp216PE3.signature.signature.signature;


/*
 * Chargement du modele de document et injection des données
 */

var finalDoc = nash.doc //
    .load('models/courrier_RE_dossierfinal.pdf') //
    .apply({
        numDossier: nash.record.description().recordUid,
        date: $qp216PE3.signature.signature.dateSignature,
        civiliteNomPrenom: civNomPrenom
    });
 
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var accompDoc = nash.doc //
    .load('models/Courrier au premier dossier v2.1 GQ.pdf') //
    .apply({
        date: $qp216PE3.signature.signature.dateSignature,
		autoriteHabilitee :"Direction interrégionale de la mer",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom

    });

finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/CERFA 14750.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));


function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}


/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPieces);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasierJudiciaire);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertificat);
appendPj($attachmentPreprocess.attachmentPreprocess.pjniveauLangue);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Pilote_maritime_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
   label : 'Pilote maritime - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'La démarche de demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de pilote maritime est maintenant terminée.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
