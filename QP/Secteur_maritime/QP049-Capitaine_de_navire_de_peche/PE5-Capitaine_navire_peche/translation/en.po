msgid "Identification du déclarant"
msgstr "Identification of the declarant"

msgid "Demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement."
msgstr "Application for recognition of professional qualifications for free establishment."

msgid "Formulaire de demande d'autorisation d'exercice"
msgstr "Application Form"
 
msgid "Civilité :"
msgstr "Civility"

msgid "Nom de naissance :"
msgstr "Birth name"

msgid "Nom d'usage :"
msgstr "Use name"


msgid "Prénom(s) :"
msgstr "First name"


msgid "Date de naissance :"
msgstr "Birth date"

msgid "Commune ou ville de naissance :"
msgstr "Town or city of birth"


msgid "Pays de naissance :"
msgstr "Native country"


msgid "Nationalité :"
msgstr "Nationality"

msgid "Adresse Personnelle"
msgstr "Personal address"

msgid "Adresse Professionnelle"
msgstr "Professional address"

msgid "Signature de la formalité"
msgstr "Signature of formality"

msgid "Je déclare sur l'honneur l'exactitude des informations de la formalité"
msgstr "I declare on my honor the accuracy of the information on the formality"

msgid "Lieu :"
msgstr "Location :"

msgid "Date :"
msgstr "Date"

msgid "Numéro et libellé de voie :"
msgstr "Track number and label :"

msgid "Complément d'adresse :"
msgstr "Additional address :"

msgid "Code Postal :"
msgstr "Postal code :"

msgid "Commune ou ville :"
msgstr "City or town :"

msgid "Pays :"
msgstr "Country :"

msgid "Téléphone fixe :"
msgstr "Phone :"
 
msgid "Téléphone mobile :"
msgstr "Mobile phone :"

msgid "Courriel :"
msgstr "E-mail address :"

msgid "Copie de la pièce d’identité (en cours de validité, recto verso : extrait d'acte de naissance ou fiche d'état civil ou carte d'identité ou passeport)."
msgstr "Copy of the identity document (valid, double-sided: extract of birth certificate or civil status card or identity card or passport)."

msgid "Copie d’un diplôme de second cycle de l’enseignement supérieur juridique, scientifique ou technique (ou équivalent)."
msgstr "Copy of a post-graduate degree in legal, scientific or technical higher education (or equivalent)."

msgid "Copie du diplôme du Centre d'études internationales de la propriété industrielle de Strasbourg (Ceipi) ou du titre reconnu équivalent."
msgstr "Copy of the diploma of the Center for International Studies of the Industrial Property of Strasbourg (Ceipi) or the equivalent recognized title."

msgid "Un diplôme national de troisième cycle ou un diplôme national de master sanctionnant une formation dans le domaine de la propriété industrielle."
msgstr "A national post-graduate diploma or a national master's degree awarded in the field of industrial property."

msgid "Copie(s) de l'attestation professionnelle."
msgstr "Copy (s) of professional attestation."

msgid "Le(s) certificat(s) doit(vent) préciser le lieu d’exercice de la pratique professionnelle, celle-ci doit avoir été acquise au sein d’un Etat de l’Union européenne ou de l’Espace économique européen ou en Suisse."
msgstr "The certificate (s) must specify the place of practice of the professional practice, the latter must have been acquired within a Member State of the European Union or the European Economic Area or Swiss."

msgid "Un diplôme national de troisième cycle ou un diplôme national de master sanctionnant une formation dans le domaine de la propriété industrielle"
msgstr "A national post-graduate diploma or a national master's degree in training in the field of industrial property"
  
msgid "les documents suivants doivent être joints à la demande (en format .PDF, .JPG ou .PNG) :"
msgstr "the following documents must be attached to the application (in .PDF, .JPG or .PNG format) :"

msgid "Demande de RQP en vue d'un libre établissement"
msgstr "Request for a RQP for Free Settlement"

msgid "Courrier obtenu à partir des données saisies"
msgstr "Mail obtained from the data entered"

msgid "Déclaration préalable dans le cadre d'une Libre Prestation Services.pdf"
msgstr "Prior declaration in the context of the freedom to provide services"

msgid "Copie du titre de formation permettant l’exercice de la profession dans le pays d’obtention."
msgstr "Copy of the title of training allowing the exercise of the profession in the country of obtaining."

msgid "Copie de l'attestation professionnelle justifiant que le ressortissant a exercé la profession au moins deux ans au cours des dix dernières années."
msgstr "Copy of the professional certificate proving that the national has practiced the profession for at least two years during the last ten years."

msgid "Attestation de l’autorité compétente de l’Etat d’origine certifiant que l’intéressé y est légalement établi dans cet Etat et qu’il n’encourt aucune interdiction d’exercer (temporaire ou définitive)."
msgstr "Certificate of the competent authority of the State of origin certifying that the person concerned is legally established in that State and that he is not subject to any prohibition to practice (temporary or permanent)."

msgid "Merci d'avoir traité cette formalité sur le site www.guichet-qualifications.fr. Votre dossier sera transmis à l'autorité compétente pour traitement."
msgstr "Thank you for having processed this formality on the site www.guichet-qualifications.fr. Your file will be forwarded to the competent authority for processing."
