var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp049PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp049PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp049PE5.etatCivil.identificationDeclarant.prenomDeclarant

cerfaFields['civiliteNomPrenom']          	= $qp049PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp049PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp049PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp049PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp049PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp049PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp049PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp049PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp049PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp049PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp049PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp049PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp049PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp049PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp049PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp049PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp049PE5.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp049PE5.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp049PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp049PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	=$qp049PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp049PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp049PE5.etatCivil.identificationDeclarant.prenomDeclarant;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp049PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Directeur interrégional de la mer (DIRM)",
		demandeContexte : "Déclaration préalable en vue d'une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier Capitaine navire LPS.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifExercice);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('capitaine_navire_peche_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Capitaine de navire de pêche - première déclaration de libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la profession de capitaine de navire de pêche',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
