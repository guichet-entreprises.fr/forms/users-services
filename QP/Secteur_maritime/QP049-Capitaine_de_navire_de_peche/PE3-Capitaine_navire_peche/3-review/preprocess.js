var cerfaFields = {};

//Civilité

var civNomPrenom = $qp049PE3.information.etatCivil.civilite + ' ' + $qp049PE3.information.etatCivil.nomNaissance + ' ' + $qp049PE3.information.etatCivil.prenom

cerfaFields['civiliteNomPrenom']                = $qp049PE3.information.etatCivil.civilite + ' ' + $qp049PE3.information.etatCivil.nomNaissance + ' ' + $qp049PE3.information.etatCivil.prenom;
cerfaFields['dateNaissance']                    = $qp049PE3.information.etatCivil.dateNaissance;
cerfaFields['lieuNaissance']                    = $qp049PE3.information.etatCivil.lieuNaissance;
cerfaFields['nationalite']                      = $qp049PE3.information.etatCivil.nationalite;
cerfaFields['numeroIdentification']             = $qp049PE3.information.etatCivil.numeroIdentification;

//Adresse de contact

cerfaFields['rueNumeroAdresseContact']          = $qp049PE3.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
cerfaFields['rueExtensionAdresseContact']       = $qp049PE3.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
cerfaFields['rueTypeAdresseContact']            = $qp049PE3.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
cerfaFields['rueNomAdresseContact']             = $qp049PE3.adresseGroup.adresseContactGroup.rueNomAdresseContact;
cerfaFields['rueComplementAdresseContact']      = $qp049PE3.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
cerfaFields['codePostalAdresseContact']         = $qp049PE3.adresseGroup.adresseContactGroup.codePostalAdresseContact;
cerfaFields['villeAdresseContact']              = $qp049PE3.adresseGroup.adresseContactGroup.villeAdresseContact;
cerfaFields['telephoneAdresseContact']          = $qp049PE3.adresseGroup.adresseContactGroup.telephoneAdresseContact;
cerfaFields['telecopieAdresseContact']          = $qp049PE3.adresseGroup.adresseContactGroup.telecopieAdresseContact;
cerfaFields['courrielAdresseContact']           = $qp049PE3.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Titre de formation

cerfaFields['numTitre1']                        = $qp049PE3.formationGroup.titreFormation.numTitre1;
cerfaFields['libelleTitre1']                    = $qp049PE3.formationGroup.titreFormation.libelleTitre1;
cerfaFields['dateExpiration1']                  = $qp049PE3.formationGroup.titreFormation.dateExpiration1;
cerfaFields['etatEmetteur1']                    = $qp049PE3.formationGroup.titreFormation.etatEmetteur1;
cerfaFields['numTitre2']                        = $qp049PE3.formationGroup.titreFormation.numTitre2;
cerfaFields['libelleTitre2']                    = $qp049PE3.formationGroup.titreFormation.libelleTitre2;
cerfaFields['dateExpiration2']                  = $qp049PE3.formationGroup.titreFormation.dateExpiration2;
cerfaFields['etatEmetteur2']                    = $qp049PE3.formationGroup.titreFormation.etatEmetteur2;
cerfaFields['numTitre3']                        = $qp049PE3.formationGroup.titreFormation.numTitre3;
cerfaFields['libelleTitre3']                    = $qp049PE3.formationGroup.titreFormation.libelleTitre3;
cerfaFields['dateExpiration3']                  = $qp049PE3.formationGroup.titreFormation.dateExpiration3;
cerfaFields['etatEmetteur3']                    = $qp049PE3.formationGroup.titreFormation.etatEmetteur3;
cerfaFields['numTitre4']                        = $qp049PE3.formationGroup.titreFormation.numTitre4;
cerfaFields['libelleTitre4']                    = $qp049PE3.formationGroup.titreFormation.libelleTitre4;
cerfaFields['dateExpiration4']                  = $qp049PE3.formationGroup.titreFormation.dateExpiration4;
cerfaFields['etatEmetteur4']                    = $qp049PE3.formationGroup.titreFormation.dateExpiration4;

//Renseignements sur l'armateur

cerfaFields['denominationSocialeArmateur']      = $qp049PE3.armateurGroup.autreRenseignements.denominationSocialeArmateur;
cerfaFields['representantLegalArmateur']        = $qp049PE3.armateurGroup.autreRenseignements.representantLegalArmateurNom + ' ' + $qp049PE3.armateurGroup.autreRenseignements.representantLegalArmateurPrenom;
cerfaFields['numeroSiret']                      = $qp049PE3.armateurGroup.autreRenseignements.numeroSiret;
cerfaFields['rueNumeroArmateur']                = $qp049PE3.armateurGroup.autreRenseignements.rueNumeroArmateur;
cerfaFields['rueExtensionArmateur']             = $qp049PE3.armateurGroup.autreRenseignements.rueExtensionArmateur;
cerfaFields['rueTypeArmateur']                  = $qp049PE3.armateurGroup.autreRenseignements.rueTypeArmateur;
cerfaFields['rueNomArmateur']                   = $qp049PE3.armateurGroup.autreRenseignements.rueNomArmateur;
cerfaFields['rueComplementArmateur']            = $qp049PE3.armateurGroup.autreRenseignements.rueComplementArmateur;
cerfaFields['codePostalArmateur']               = $qp049PE3.armateurGroup.autreRenseignements.codePostalArmateur;
cerfaFields['villeArmateur']                    = $qp049PE3.armateurGroup.autreRenseignements.villeArmateur;
cerfaFields['telephoneArmateur']                = $qp049PE3.armateurGroup.autreRenseignements.telephoneArmateur;
cerfaFields['telecopieArmateur']                = $qp049PE3.armateurGroup.autreRenseignements.telecopieArmateur;
cerfaFields['courrielArmateur']                 = $qp049PE3.armateurGroup.autreRenseignements.courrielArmateur;

//Renseignements sur le navire

cerfaFields['nomNavire']                        = $qp049PE3.navireGroup.navireRenseignements.nomNavire;
cerfaFields['puissanceNavire']                  = $qp049PE3.navireGroup.navireRenseignements.puissanceNavire;
cerfaFields['immatriculationNavire']            = $qp049PE3.navireGroup.navireRenseignements.immatriculationNavire;
cerfaFields['jaugeBrute']                       = $qp049PE3.navireGroup.navireRenseignements.jaugeBrute;

//Promesse d'embarquement

cerfaFields['datEmbarquementDebut']             = $qp049PE3.embarquementGroup.promesseEmbarquement.datEmbarquement.from;
cerfaFields['datEmbarquementFin']               = $qp049PE3.embarquementGroup.promesseEmbarquement.datEmbarquement.to;
cerfaFields['fonction']                         = $qp049PE3.embarquementGroup.promesseEmbarquement.fonction;


//Signature

cerfaFields['signatureConnaissanceMarin']       = $qp049PE3.signatureGroup.signature.signatureConnaissanceMarin;
cerfaFields['signatureConnaissancesJuridiques'] = $qp049PE3.signatureGroup.signature.signatureConnaissancesJuridiques;
cerfaFields['signatureExigencesMoralite']       = $qp049PE3.signatureGroup.signature.signatureExigencesMoralite;
cerfaFields['certifieHonneur']                  = $qp049PE3.signatureGroup.signature.certifieHonneur;
cerfaFields['dateSignature']                    = $qp049PE3.signatureGroup.signature.dateSignature;
cerfaFields['lieuSignature']                    = $qp049PE3.signatureGroup.signature.lieuSignature;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp049PE3.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Directeur interrégional de la mer (DIRM)",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa 15333.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationECDIS);
appendPj($attachmentPreprocess.attachmentPreprocess.pjConnaissancesReglementation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjcertificatAptitude);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('capitaine_navire_peche_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Capitaine de navire de pêche - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de capitaine de navire de pêche',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
