var cerfaFields = {};

//Civilité

var civNomPrenom = $qp049PE4.information.etatCivil.civilite + ' ' + $qp049PE4.information.etatCivil.nomNaissance + ' ' + $qp049PE4.information.etatCivil.prenom

cerfaFields['civiliteNom']                	   = $qp049PE4.information.etatCivil.civilite + ' ' + $qp049PE4.information.etatCivil.nomNaissance
cerfaFields['civilitePrenom']                   = $qp049PE4.information.etatCivil.prenom;
cerfaFields['dateNaissance']                    = $qp049PE4.information.etatCivil.dateNaissance;
cerfaFields['nationalite']                      = $qp049PE4.information.etatCivil.nationalite;
cerfaFields['numeroIdentification']             = $qp049PE4.information.etatCivil.numeroIdentification;
cerfaFields['serviceMarin']             		   = $qp049PE4.information.etatCivil.service;

//Adresse de contact

cerfaFields['rueNumeroAdresseContact']          = $qp049PE4.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
cerfaFields['rueExtensionAdresseContact']       = $qp049PE4.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
cerfaFields['rueTypeAdresseContact']            = $qp049PE4.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
cerfaFields['rueNomAdresseContact']             = $qp049PE4.adresseGroup.adresseContactGroup.rueNomAdresseContact;
cerfaFields['rueComplementAdresseContact']      = $qp049PE4.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
cerfaFields['codePostalAdresseContact']         = $qp049PE4.adresseGroup.adresseContactGroup.codePostalAdresseContact;
cerfaFields['villeAdresseContact']              = $qp049PE4.adresseGroup.adresseContactGroup.villeAdresseContact;
cerfaFields['telephoneAdresseContact']          = $qp049PE4.adresseGroup.adresseContactGroup.telephoneAdresseContact;
cerfaFields['telecopieAdresseContact']          = $qp049PE4.adresseGroup.adresseContactGroup.telecopieAdresseContact;
cerfaFields['courrielAdresseContact']           = $qp049PE4.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Titre de formation

cerfaFields['numTitre1']                        = $qp049PE4.formationGroup.titreFormation.numTitre1;
cerfaFields['libelleTitre1']                    = $qp049PE4.formationGroup.titreFormation.libelleTitre1;
cerfaFields['dateExpiration1']                  = $qp049PE4.formationGroup.titreFormation.dateExpiration1;
cerfaFields['numTitre2']                        = $qp049PE4.formationGroup.titreFormation.numTitre2;
cerfaFields['libelleTitre2']                    = $qp049PE4.formationGroup.titreFormation.libelleTitre2;
cerfaFields['dateExpiration2']                  = $qp049PE4.formationGroup.titreFormation.dateExpiration2;
cerfaFields['numTitre3']                        = $qp049PE4.formationGroup.titreFormation.numTitre3;
cerfaFields['libelleTitre3']                    = $qp049PE4.formationGroup.titreFormation.libelleTitre3;
cerfaFields['dateExpiration3']                  = $qp049PE4.formationGroup.titreFormation.dateExpiration3;
cerfaFields['numTitre4']                        = $qp049PE4.formationGroup.titreFormation.numTitre4;
cerfaFields['libelleTitre4']                    = $qp049PE4.formationGroup.titreFormation.libelleTitre4;
cerfaFields['dateExpiration4']                  = $qp049PE4.formationGroup.titreFormation.dateExpiration4;
cerfaFields['numTitre5']                        = $qp049PE4.formationGroup.titreFormation.numTitre5;
cerfaFields['libelleTitre5']                    = $qp049PE4.formationGroup.titreFormation.libelleTitre5;
cerfaFields['dateExpiration5']                  = $qp049PE4.formationGroup.titreFormation.dateExpiration5;
cerfaFields['numTitre6']                        = $qp049PE4.formationGroup.titreFormation.numTitre6;
cerfaFields['libelleTitre6']                    = $qp049PE4.formationGroup.titreFormation.libelleTitre6;
cerfaFields['dateExpiration6']                  = $qp049PE4.formationGroup.titreFormation.dateExpiration6;
cerfaFields['numTitre7']                        = $qp049PE4.formationGroup.titreFormation.numTitre7;
cerfaFields['libelleTitre7']                    = $qp049PE4.formationGroup.titreFormation.libelleTitre7;
cerfaFields['dateExpiration7']                  = $qp049PE4.formationGroup.titreFormation.dateExpiration7;

//Mise à disposition titre

cerfaFields['choixEnvoiDomicile']               = Value('id').of($qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.choixMiseDisposition).eq('domicile');
cerfaFields['choixEnvoiAutreAdresse']           = Value('id').of($qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.choixMiseDisposition).eq('autreAdresse');
cerfaFields['choixRemisDemandeur']              = Value('id').of($qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.choixMiseDisposition).eq('remisDemandeur');
cerfaFields['adresseEnvoiAutre1']               = ($qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseNumeroRue != null ? $qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseNumeroRue + ' ' : '') + ($qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseComplement != null ? ', ' + $qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseComplement : '');
cerfaFields['adresseEnvoiAutre2']               = ($qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseCodePostal != null ? $qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseCodePostal + ' ' : '') + ($qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseVille != null ? $qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdresseVille + ' ' : '') + ($qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdressePays != null ? ', ' + $qp049PE4.miseDispositionTitreGroup.miseDispositionTitre.envoiAutreAdresse.autreAdressePays + ' ' : '')

//Signature

cerfaFields['signature']                 	   = $qp049PE4.signatureGroup.signature.certifie;
cerfaFields['dateSignature']                    = $qp049PE4.signatureGroup.signature.dateSignature;
cerfaFields['lieuSignature']                    = $qp049PE4.signatureGroup.signature.lieuSignature;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp049PE4.signatureGroup.signature.dateSignature,
		autoriteHabilitee :"Directeur interrégional de la mer (DIRM)",
		demandeContexte : "Renouvellement de la reconnaissance des qualifications professionnelles en vue d'un LE",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa_14949-01.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAncienneAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationEmbarquement);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationReussiteTest);
appendPj($attachmentPreprocess.attachmentPreprocess.pjcertificatAptitude);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('capitaine_navire_peche_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Capitaine de navire de pêche - renouvellement de demande de RQP en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de capitaine de navire de pêche',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
