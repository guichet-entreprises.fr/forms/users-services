var cerfaFields = {};

//Civilité

var civNomPrenom = $qp049PE2.information.etatCivil.civilite + ' ' + $qp049PE2.information.etatCivil.nomNaissance + ' ' + $qp049PE2.information.etatCivil.prenom

cerfaFields['civiliteNomPrenom']             = $qp049PE2.information.etatCivil.civilite + ' ' + $qp049PE2.information.etatCivil.nomNaissance + ' ' + $qp049PE2.information.etatCivil.prenom;
cerfaFields['nationalite']                   = $qp049PE2.information.etatCivil.nationalite;
cerfaFields['numeroMarin']                   = $qp049PE2.information.etatCivil.numeroMarin;
cerfaFields['ENIMAnterieur']                 = $qp049PE2.information.etatCivil.eNIMAnterieur;

//Adresse de contact

cerfaFields['rueNumeroAdresseContact']       = $qp049PE2.adresseGroup.adresseContactGroup.rueNumeroAdresseContact;
cerfaFields['rueExtensionAdresseContact']    = $qp049PE2.adresseGroup.adresseContactGroup.rueExtensionAdresseContact;
cerfaFields['rueTypeAdresseContact']         = $qp049PE2.adresseGroup.adresseContactGroup.rueTypeAdresseContact;
cerfaFields['rueNomAdresseContact']          = $qp049PE2.adresseGroup.adresseContactGroup.rueNomAdresseContact;
cerfaFields['rueComplementAdresseContact']   = $qp049PE2.adresseGroup.adresseContactGroup.rueComplementAdresseContact;
cerfaFields['codePostalAdresseContact']      = $qp049PE2.adresseGroup.adresseContactGroup.codePostalAdresseContact;
cerfaFields['telephoneAdresseContact']       = $qp049PE2.adresseGroup.adresseContactGroup.telephoneAdresseContact;
cerfaFields['villeAdresseContact']           = $qp049PE2.adresseGroup.adresseContactGroup.villeAdresseContact;
cerfaFields['telecopieAdresseContact']       = $qp049PE2.adresseGroup.adresseContactGroup.telecopieAdresseContact;
cerfaFields['courrielAdresseContact']        = $qp049PE2.adresseGroup.adresseContactGroup.courrielAdresseContact;

//Qualifications

cerfaFields['titreEtrangerOui']              = ($qp049PE2.qualificationsGroup.qualifications.titreEtranger ? true : '');
cerfaFields['titreEtrangerNon']              = ($qp049PE2.qualificationsGroup.qualifications.titreEtranger ? '' : true);
cerfaFields['nomTitre']                      = $qp049PE2.qualificationsGroup.qualifications.nomTitre;
cerfaFields['dateValiditeTitre']             = $qp049PE2.qualificationsGroup.qualifications.dateValiditeTitre;

//Navire

cerfaFields['fonction']                      = $qp049PE2.informationsCompNavire.navireGroup.fonction;
cerfaFields['nomNavire']                     = $qp049PE2.informationsCompNavire.navireGroup.nomNavire;
cerfaFields['jaugeBrute']                    = $qp049PE2.informationsCompNavire.navireGroup.jaugeBrute;
cerfaFields['puissanceNavire']               = $qp049PE2.informationsCompNavire.navireGroup.puissanceNavire;
cerfaFields['immatriculationNavire']			= $qp049PE2.informationsCompNavire.navireGroup.immatriculationNavire;
cerfaFields['dateEmbarquementPrevue']        = $qp049PE2.informationsCompNavire.navireGroup.dateEmbarquementPrevue;

//Employeur

cerfaFields['employeurNomPrenom']            = $qp049PE2.informationsCompEmployeur.employeurGroup.employeurNomPrenom;
cerfaFields['employeurDenomination']         = $qp049PE2.informationsCompEmployeur.employeurGroup.employeurDenomination;
cerfaFields['employeurRepresentant']         = $qp049PE2.informationsCompEmployeur.employeurGroup.employeurRepresentant;
cerfaFields['numeroSiret']                   = $qp049PE2.informationsCompEmployeur.employeurGroup.numeroSiret;
cerfaFields['employeurFormeJuridique']       = $qp049PE2.informationsCompEmployeur.employeurGroup.employeurFormeJuridique;
cerfaFields['rueNumeroAdresseEmployeur']     = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueNumeroAdresseEmployeur;
cerfaFields['rueExtensionAdresseEmployeur']  = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueExtensionAdresseEmployeur;
cerfaFields['rueTypeAdresseEmployeur']       = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueTypeAdresseEmployeur;
cerfaFields['rueNomAdresseEmployeur']        = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueNomAdresseEmployeur;
cerfaFields['rueComplementAdresseEmployeur'] = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.rueComplementAdresseEmployeur;
cerfaFields['codePostalAdresseEmployeur']    = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.codePostalAdresseEmployeur;
cerfaFields['villeAdresseEmployeur']         = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.villeAdresseEmployeur;
cerfaFields['telephoneAdresseEmployeur']     = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.telephoneAdresseEmployeur;
cerfaFields['telecopieAdresseEmployeur']     = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.telecopieAdresseEmployeur;
cerfaFields['courrielAdresseEmployeur']      = $qp049PE2.informationsCompEmployeur.employeurGroup.adresseEmployeur.courrielAdresseEmployeur;

//Signature

cerfaFields['lieuSignature']                 = $qp049PE2.signatureGroup.signatureG.lieuSignature;
cerfaFields['dateSignature']                 = $qp049PE2.signatureGroup.signatureG.dateSignature;
cerfaFields['signature']                     = $qp049PE2.signatureGroup.signatureG.signature;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qpxxxPEx.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp049PE2.signatureGroup.signatureG.dateSignature,
		autoriteHabilitee :"Directeur interrégional de la mer (DIRM)",
		demandeContexte : "Reconnaissance des qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/cerfa_14750.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDetailDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjcertificatAptitude);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationCapacites);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('capitaine_navire_peche_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Capitaine de navire de pêche - demande de reconnaissance de qualifications professionnelles en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de capitaine de navire de pêche',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
