var cerfaFields = {};


//cerfaFields['1ère demande']   = true;
//cerfaFields['Renouvellement'] = false;
//cerfaFields['Extension']      = false;
//cerfaFields['Duplicata']      = false;


var civNomPrenom = $qp105PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp105PE2.etatCivil.identificationDeclarant.prenomDeclarant;


//civilité

cerfaFields['civiliteNomPrenom']      = $qp105PE2.etatCivil.identificationDeclarant.civilite 
								+ ' ' + $qp105PE2.etatCivil.identificationDeclarant.prenomDeclarant
																	  
								+' '+ ($qp105PE2.etatCivil.identificationDeclarant.nomEpouseDeclarant != null ? $qp105PE2.etatCivil.identificationDeclarant.nomEpouseDeclarant : $qp105PE2.etatCivil.identificationDeclarant.nomDeclarant);
cerfaFields['dateNaissance']  = $qp105PE2.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['villePaysNaissance'] = $qp105PE2.etatCivil.identificationDeclarant.lieuNaissance;
cerfaFields['mail']       = $qp105PE2.etatCivil.identificationDeclarant.mail;
cerfaFields['nationalite']       = $qp105PE2.etatCivil.identificationDeclarant.mail;

cerfaFields['adresse']        = $qp105PE2.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant
								+ ($qp105PE2.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ' ' + $qp105PE2.adresse.adressePersonnelle.complementAdresseDeclarant : '');
cerfaFields['villePays']       = ($qp105PE2.adresse.adressePersonnelle.codePostalAdresseDeclarant != null ? $qp105PE2.adresse.adressePersonnelle.codePostalAdresseDeclarant + ' ' :'')
								+ $qp105PE2.adresse.adressePersonnelle.villeAdresseDeclarant + ', '
								+ $qp105PE2.adresse.adressePersonnelle.paysAdresseDeclarant;
cerfaFields['telephoneFixe']      = $qp105PE2.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;


//cerfaFields['numeroPermis']   = $qp105PE2.permis.permis.numeroPermis;
//cerfaFields['delivrePar']     = $qp105PE2.permis.permis.delivrePar;
//cerfaFields['par']            = $qp105PE2.permis.permis.par;
//cerfaFields['diplome']        = $qp105PE2.permis.permis.diplome;
//cerfaFields['dateObtention']  = $qp105PE2.permis.permis.dateObtention;
//cerfaFields['employeur']      = (Value('id').of($qp105PE2.permis.permis.employer).eq('employeur') ? 'Employeur : ' + $qp105PE2.permis.permis.nomEmployeur : 'Auto-école : ' + $qp105PE2.permis.permis.nomExploitant);

//Signature
cerfaFields['lieuSignature']  = $qp105PE2.signature.signature.lieuSignature;
cerfaFields['dateSignature']  = $qp105PE2.signature.signature.dateSignature;
cerfaFields['signature']      = $qp105PE2.signature.signature.signature;
cerfaFields['libelleProfession']      = "Enseignement de la conduite et de la sécurité routière";
cerfaFields['civiliteNomPrenomSignature'] = $qp105PE2.etatCivil.identificationDeclarant.prenomDeclarant
								+' '+ ($qp105PE2.etatCivil.identificationDeclarant.nomEpouseDeclarant != null ? $qp105PE2.etatCivil.identificationDeclarant.nomEpouseDeclarant : $qp105PE2.etatCivil.identificationDeclarant.nomDeclarant);

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp105PE2.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture ",
		demandeContexte : "Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});
//var cerfa = pdf.create('models/courrier_libre_LE_V4.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
//var cerfaPdf = pdf.save('Enseignement conduite - LE.pdf', cerfa); //Nom du fichier en sortie
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LE_V4.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));

function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhotoID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestation);
appendPj($attachmentPreprocess.attachmentPreprocess.pjMaitriseFR);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPermis);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Enseignement conduite - LE.pdf');



return spec.create({
    id : 'review',
    label : 'Enseignement de la conduite et de la sécurité routière  - Demande de RQP en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande RQP en vue d\'un libre établissement pour la profession d\'enseignement de la conduite et de la sécurité routière',
            description : 'Formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
