var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']          	= $qp105PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp105PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp105PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp105PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp105PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp105PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp105PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp105PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp105PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp105PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp105PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp105PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp105PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp105PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp105PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp105PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp105PE5.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp105PE5.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp105PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp105PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp105PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp105PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp105PE5.etatCivil.identificationDeclarant.prenomDeclarant;


var cerfa = pdf.create('models/courrier Enseignement conduite LPS.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Enseignement de la conduite et de la sécurité routière demande LPS.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Enseignement de la conduite et de la sécurité routière  - demande d’accès partiel en vue d’une libre prestation de service',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Courrier de demande d’accès partiel en vue d\'une  libre prestation de services pour la profession d\'enseignement de la conduite et de la sécurité routière.',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
