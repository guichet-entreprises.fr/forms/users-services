var cerfaFields = {};


cerfaFields['1ère demande']   = true;
cerfaFields['Renouvellement'] = false;
cerfaFields['Extension']      = false;
cerfaFields['Duplicata']      = false;




//civilité

cerfaFields['prenomNom']      = $qp105PE9.etatCivil.identificationDeclarant.civilite 
								+ ' ' + $qp105PE9.etatCivil.identificationDeclarant.prenomDeclarant
								+' '+ $qp105PE9.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['nomDeclarant']   = $qp105PE9.etatCivil.identificationDeclarant.nomEpouseDeclarant;
cerfaFields['dateNaissance']  = $qp105PE9.etatCivil.identificationDeclarant.dateNaissance;
cerfaFields['villeNaissance'] = $qp105PE9.etatCivil.identificationDeclarant.lieuNaissance;
cerfaFields['Courriel']       = $qp105PE9.etatCivil.identificationDeclarant.mail;

cerfaFields['adresse']        = $qp105PE9.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant
								+ ($qp105PE9.adresse.adressePersonnelle.complementAdresseDeclarant != null ? ' ' + $qp105PE9.adresse.adressePersonnelle.complementAdresseDeclarant : '');
cerfaFields['adresse1']       = ($qp105PE9.adresse.adressePersonnelle.codePostalAdresseDeclarant != null ? $qp105PE9.adresse.adressePersonnelle.codePostalAdresseDeclarant + ' ' :'')
								+ $qp105PE9.adresse.adressePersonnelle.villeAdresseDeclarant + ', '
								+ $qp105PE9.adresse.adressePersonnelle.paysAdresseDeclarant;
cerfaFields['telephone']      = $qp105PE9.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;


cerfaFields['numeroPermis']   = $qp105PE9.permis.permis.numeroPermis;
cerfaFields['delivrePar']     = $qp105PE9.permis.permis.delivrePar;
cerfaFields['par']            = $qp105PE9.permis.permis.par;
cerfaFields['diplome']        = $qp105PE9.permis.permis.diplome;
cerfaFields['dateObtention']  = $qp105PE9.permis.permis.dateObtention;
cerfaFields['employeur']      = (Value('id').of($qp105PE9.permis.permis.employer).eq('employeur') ? 'Employeur : ' + $qp105PE9.permis.permis.nomEmployeur : 'Auto-école : ' + $qp105PE9.permis.permis.nomExploitant);

//Signature
cerfaFields['lieuSignature']  = $qp105PE9.signature.signature.lieuSignature;
cerfaFields['dateSignature']  = $qp105PE9.signature.signature.dateSignature;
cerfaFields['signature']      = $qp105PE9.signature.signature.signature;



var cerfa = pdf.create('models/FORMULAIRE AUTORISATION ENSEIGNER.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Enseignement conduite et sécurité routière  - demande accès partiel LE.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Enseignement de la conduite et de la sécurité routière  - demande d’accès partiel en vue d’un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire demande d’accès partiel à la profession enseignement de la conduite et de la sécurité routière en vue d’un libre établissement',
            description : 'Formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
