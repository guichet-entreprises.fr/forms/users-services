var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp086PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp086PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp086PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['civiliteNomPrenom']          	= $qp086PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp086PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp086PE5.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			= $qp086PE5.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp086PE5.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                	= $qp086PE5.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             	= $qp086PE5.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 						= $qp086PE5.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp086PE5.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp086PE5.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       				= ($qp086PE5.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp086PE5.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp086PE5.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp086PE5.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      		= $qp086PE5.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          		= $qp086PE5.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          			= $qp086PE5.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                			= $qp086PE5.signature.signature.dateSignature;
cerfaFields['signature']           			= $qp086PE5.signature.signature.signature;
cerfaFields['lieuSignature']                = $qp086PE5.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']	= $qp086PE5.etatCivil.identificationDeclarant.civilite + ' ' + $qp086PE5.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp086PE5.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['libelleProfession']								  	  = "Contrôleur technique de véhicules."
cerfaFields['libelleProfession2']								  	  = "contrôleur technique de véhicules."

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp086PE5.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp086PE5.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Déclaration préalable en vue d’une libre prestation de services",
		civiliteNomPrenom : civNomPrenom
	});

// finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/courrier_libre_LPS_V3.pdf') //
	.apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationLE);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCentre);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Controleur_technique_vehicules_LPS.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Contrôleur technique de véhicules - déclaration préalable en vue dune libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Déclaration préalable en vue d\'une libre prestation de services pour l\'exercice de la contrôleur technique de véhicules.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
