var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp086PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp086PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp086PE1.etatCivil.identificationDeclarant.prenomDeclarant;

// Contrôleur
cerfaFields['nom']          		= $qp086PE1.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenom']          		= $qp086PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['adresse1'] 			= $qp086PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ' ' + ($qp086PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp086PE1.adresse.adresseContact.complementAdresseDeclarant : ' ') + ' ' + ($qp086PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp086PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + ' ' + $qp086PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp086PE1.adresse.adresseContact.paysAdresseDeclarant;

// Profession 
cerfaFields['vehiculesLegers']      = ($qp086PE1.profession.profession0.typeVehicules=='Véhicules légers');
cerfaFields['vehiculesLourds']      = ($qp086PE1.profession.profession0.typeVehicules=='Véhicules lourds');
cerfaFields['reseau']        		= $qp086PE1.profession.profession0.reseau;
cerfaFields['qualite']        		= $qp086PE1.profession.profession0.qualite;

// Centre de contrôle
cerfaFields['raisonSociale']        = $qp086PE1.centre.centre0.raisonSociale;
cerfaFields['adresse2'] 			= $qp086PE1.centre.centre0.num2 + ' ' + ($qp086PE1.centre.centre0.complement2 != null ? ', ' + $qp086PE1.centre.centre0.complement2 : ' ') + ' ' + ($qp086PE1.centre.centre0.codepostal2 != null ? $qp086PE1.centre.centre0.codePostal2 + ' ' : '') + ' ' + $qp086PE1.centre.centre0.ville2;

//signature
cerfaFields['dateSignature']        = $qp086PE1.signature.signature.dateSignature;
cerfaFields['lieuSignature']        = $qp086PE1.signature.signature.lieuSignature;
cerfaFields['civNomPrenom']			= $qp086PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp086PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp086PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['signatureCoche']       = $qp086PE1.signature.signature.signature1;


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp086PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
 
 var finalDoc1 = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp086PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture du département",
		demandeContexte : "Demande de reconnaissance de qualifications professionnelles en vue d'un libre établissement",
		civiliteNomPrenom : civNomPrenom
	});

 var finalDoc2 = nash.doc //
	.load('models/Declaration sur honneur.pdf') //
	.apply({
		nomPrenom: $qp086PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp086PE1.etatCivil.identificationDeclarant.prenomDeclarant,
		adresse1: $qp086PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ' ' + ($qp086PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp086PE1.adresse.adresseContact.complementAdresseDeclarant : ' ') + ' ' + ($qp086PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp086PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + ' ' + $qp086PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp086PE1.adresse.adresseContact.paysAdresseDeclarant,
		nomCentre: $qp086PE1.centre.centre0.raisonSociale,
		adresse2: $qp086PE1.centre.centre0.num2 + ' ' + ($qp086PE1.centre.centre0.complement2 != null ? ', ' + $qp086PE1.centre.centre0.complement2 : ' ') + ' ' + ($qp086PE1.centre.centre0.codepostal2 != null ? $qp086PE1.centre.centre0.codePostal2 + ' ' : '') + ' ' + $qp086PE1.centre.centre0.ville2,
		numeroAgrement : $qp086PE1.profession.profession0.numeroAgrement,
		lieuSignature : $qp086PE1.signature.signature.lieuSignature,
		dateSignature : $qp086PE1.signature.signature.dateSignature,
		signatureCoche : $qp086PE1.signature.signature.signature1,
		civNomPrenom : civNomPrenom
	});

// finalDoc.append(accompDoc.save('courrier.pdf'));

/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/demande d’agrément en qualité de contrôleur.pdf') //
	.apply(cerfaFields);

finalDoc1.append(finalDoc2.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreuveQualifications);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasierJudiciaire);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustificatif);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAvis);

finalDoc1.append(cerfaDoc.save('cerfa.pdf'));

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc1.save('Controleur_technique_vehicules_LE.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Contrôleur technique de véhicules - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la contrôleur technique de véhicules.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
