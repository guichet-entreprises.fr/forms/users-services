  var formFields = {};

var civNomPrenom = $certificatinformation.info1.etatCivil.civilite + ' ' + $certificatinformation.info1.etatCivil.declarantNomNaissance + ' ' + $certificatinformation.info1.etatCivil.declarantPrenoms;


//Etat civil
formFields['civiliteNomPrenom']                       = $certificatinformation.info1.etatCivil.declarantNomUsage != null ?
														($certificatinformation.info1.etatCivil.civilite + ' ' 
													    + $certificatinformation.info1.etatCivil.declarantPrenoms + ' '
														+ $certificatinformation.info1.etatCivil.declarantNomUsage + ' '
														+ "née" + ' ' + $certificatinformation.info1.etatCivil.declarantNomNaissance) : 
														($certificatinformation.info1.etatCivil.civilite + ' ' 
													    + $certificatinformation.info1.etatCivil.declarantPrenoms + ' '
														+ $certificatinformation.info1.etatCivil.declarantNomNaissance);
formFields['villePays']              			      = $certificatinformation.info1.etatCivil.declarantVilleNaissance +', ' 
														+ $certificatinformation.info1.etatCivil.declarantPaysNaissance;
formFields['declarantPaysNationalite']                = $certificatinformation.info1.etatCivil.declarantPaysNationalite;
formFields['declarantDateNaissance']                  = $certificatinformation.info1.etatCivil.declarantDateNaissance;
formFields['declarantVillePaysNaissance']             = $certificatinformation.info1.etatCivil.declarantVilleNaissance + ', ' 
														+ $certificatinformation.info1.etatCivil.declarantPaysNaissance;


//Coordonnées

formFields['declarantAdressePersoNumNomVoie']          = $certificatinformation.info2.coordonnees1.declarantAdressePersoComplement != null ?
														($certificatinformation.info2.coordonnees1.declarantAdressePersoNumNomVoie + ' '
														+ $certificatinformation.info2.coordonnees1.declarantAdressePersoComplement) : 
														($certificatinformation.info2.coordonnees1.declarantAdressePersoNumNomVoie);
formFields['declarantAdresseCpVillePays']        	   = $certificatinformation.info2.coordonnees1.declarantAdressePersoCP != null ?
														($certificatinformation.info2.coordonnees1.declarantAdressePersoCP + ' '
														+ $certificatinformation.info2.coordonnees1.declarantAdressePersoVille + ' '
														+ $certificatinformation.info2.coordonnees1.declarantAdressePersoPays) :
														($certificatinformation.info2.coordonnees1.declarantAdressePersoVille + ' '
														+ $certificatinformation.info2.coordonnees1.declarantAdressePersoPays);
formFields['declarantPersoTelephone']          		   = $certificatinformation.info2.coordonnees1.declarantAdressePersoTelephone;
formFields['declarantAdressePersoCourriel']            = $certificatinformation.info2.coordonnees1.declarantAdressePersoCourriel;



//Profession considérée

formFields['professionConcernee']                      = $certificatinformation.info3.profession.professionConcernee;




//Signature

formFields['dateSignature']                            = $certificatinformation.info4.signature.dateSignature;
formFields['lieuSignature']                            = $certificatinformation.info4.signature.lieuSignature;
formFields['prenomNomDeclarant']					   = $certificatinformation.info1.etatCivil.declarantPrenoms + ' '
													    + ($certificatinformation.info1.etatCivil.declarantNomUsage != null ? $certificatinformation.info1.etatCivil.declarantNomUsage : $certificatinformation.info1.etatCivil.declarantNomNaissance);


/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $certificatinformation.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */




/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
	.load('models/demande_certificat_information.pdf') //
	.apply(formFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        cerfaDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);



/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = cerfaDoc.save('demande_certificat_information.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Demande de certificat d\'information',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'certificatinformation',
            label : 'Demande de certificat d\'information',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies et les pièces jointes. Veuillez vérifier l\'exactitude des informations, que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
