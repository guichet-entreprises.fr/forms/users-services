var cerfaFields = {};



//etatCivil

cerfaFields['civiliteNomPrenom']          		     		= $qp037PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp037PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp037PE6.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  					 		= $qp037PE6.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp037PE6.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                			 		= $qp037PE6.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']             			 		= $qp037PE6.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 										= $qp037PE6.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp037PE6.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp037PE6.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       								= ($qp037PE6.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp037PE6.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp037PE6.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp037PE6.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneFixe']          				 		= $qp037PE6.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']    					 		= $qp037PE6.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']          					 		= $qp037PE6.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                							= $qp037PE6.signature.signature.dateSignature;
cerfaFields['signature']           							= $qp037PE6.signature.signature.signature;
cerfaFields['lieuSignature']                               	= $qp037PE6.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']                  	= $qp037PE6.etatCivil.identificationDeclarant.civilite + ' ' + $qp037PE6.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp037PE6.etatCivil.identificationDeclarant.prenomDeclarant;





var cerfa = pdf.create('models/courrier libre LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Animateur de stages sécurité routière - demande accès partiel LPS expert en sécurité routière.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Animateur de stages de sensibilisation à la sécurité routière - demande d\'accès partiel en vue d’une libre prestation de services en tant qu\'expert en sécurité routière',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande d\'accès partiel en vue d’une libre prestation de services en tant qu\'expert en sécurité routière pour la profession d\'animateur de stages de sensibilisation à la sécurité routière.',
            description : 'Formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
