var cerfaFields = {};



//etatCivil

cerfaFields['civiliteNomPrenom']          		     		= $qp037PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp037PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp037PE4.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  					 		= $qp037PE4.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp037PE4.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                			 		= $qp037PE4.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']             			 		= $qp037PE4.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 										= $qp037PE4.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp037PE4.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp037PE4.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       								= ($qp037PE4.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp037PE4.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp037PE4.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp037PE4.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneFixe']          				 		= $qp037PE4.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']    					 		= $qp037PE4.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']          					 		= $qp037PE4.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                							= $qp037PE4.signature.signature.dateSignature;
cerfaFields['signature']           							= $qp037PE4.signature.signature.signature;
cerfaFields['lieuSignature']                               	= $qp037PE4.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']                  	= $qp037PE4.etatCivil.identificationDeclarant.civilite + ' ' + $qp037PE4.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp037PE4.etatCivil.identificationDeclarant.prenomDeclarant;





var cerfa = pdf.create('models/courrier libre LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Animateur de stages sécurité routière - declaration LPS.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Animateur de stages de sensibilisation à la sécurité routière - déclaration préalable en vue d’une libre prestation de services',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de déclaration préalable en vue d\'une libre prestation de services pour la profession d\'animateur de stages de sensibilisation à la sécurité routière.',
            description : 'Formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
