var cerfaFields = {};
//etatCivil


cerfaFields['prenomDeclarant']                      = $qp037PE9.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['nomDeclarant']                         = $qp037PE9.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['nomEpouseDeclarant']                   = $qp037PE9.etatCivil.identificationDeclarant.nomEpouseDeclarant;
cerfaFields['desireObtenirRenouvellement']          = false;
cerfaFields['desireObtenirAutorisation']            = true;



//adresse
cerfaFields['villeAdresseDeclarant']                = $qp037PE9.adresse.adressePersonnelle.villeAdresseDeclarant;
cerfaFields['adressePersonnelleAdresseDeclarant']   = $qp037PE9.adresse.adressePersonnelle.adressePersonnelleAdresseDeclarant;
cerfaFields['telephoneMobileAdresseDeclarant']      = $qp037PE9.adresse.adressePersonnelle.telephoneMobileAdresseDeclarant;
cerfaFields['codePostalAdresseDeclarant']           = $qp037PE9.adresse.adressePersonnelle.codePostalAdresseDeclarant;
cerfaFields['numeroLibelleAdresseDeclarant']        = $qp037PE9.adresse.adressePersonnelle.numeroLibelleAdresseDeclarant;
cerfaFields['complementadressedeclarant']           = $qp037PE9.adresse.adressePersonnelle.complementadressedeclarant;



//Signature
cerfaFields['dateSignature']                		= $qp037PE9.signature.signature.dateSignature;
cerfaFields['signature']           					= $qp037PE9.signature.signature.signature;
cerfaFields['lieuSignature']                        = $qp037PE9.signature.signature.lieuSignature;


var cerfa = pdf.create('models/2-FORMULAIRE_AUTORISATION_ANIMER.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Animateur de stages sécurité routière - RQP acces partiel expert en sécurité routière.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Animateur de stages de sensibilisation à la sécurité routière - demande de RQP en vue d\'un libre établissement en accès partiel en tant qu\'expert en sécurité routière',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande de RQP en vue d\’un libre établissement en accès partiel en tant qu\'expert en sécurité routière pour la profession d\'animateur de stages de sensibilisation à la sécurité routière.',
            description : 'Formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
