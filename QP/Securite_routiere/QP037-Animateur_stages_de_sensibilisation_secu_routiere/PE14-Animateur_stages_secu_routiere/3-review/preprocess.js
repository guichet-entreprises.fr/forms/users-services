var cerfaFields = {};



//etatCivil

cerfaFields['civiliteNomPrenom']          		     		= $qp037PE14.etatCivil.identificationDeclarant.civilite + ' ' + $qp037PE14.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp037PE14.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  					 		= $qp037PE14.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp037PE14.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                			 		= $qp037PE14.etatCivil.identificationDeclarant.nationaliteDeclarant;

cerfaFields['dateNaissance']             			 		= $qp037PE14.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 										= $qp037PE14.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp037PE14.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp037PE14.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       								= ($qp037PE14.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp037PE14.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp037PE14.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp037PE14.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneFixe']          				 		= $qp037PE14.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['telephoneMobile']    					 		= $qp037PE14.adresse.adresseContact.telephoneMobileAdresseDeclarant;
cerfaFields['courriel']          					 		= $qp037PE14.adresse.adresseContact.mailAdresseDeclarant;

//signature
cerfaFields['date']                							= $qp037PE14.signature.signature.dateSignature;
cerfaFields['signature']           							= $qp037PE14.signature.signature.signature;
cerfaFields['lieuSignature']                               	= $qp037PE14.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']                  	= $qp037PE14.etatCivil.identificationDeclarant.civilite + ' ' + $qp037PE14.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp037PE14.etatCivil.identificationDeclarant.prenomDeclarant;





var cerfa = pdf.create('models/courrier libre LPS V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Animateur de stages sécurité routière - demande accès partiel LPS psychologue.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Animateur de stages de sensibilisation à la sécurité routière - demande d\'accès partiel en vue d’une libre prestation de services en tant que psychologue',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande d\'accès partiel en vue d’une libre prestation de services en tant que psychologue pour la profession d\'animateur de stages de sensibilisation à la sécurité routière.',
            description : 'Formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
