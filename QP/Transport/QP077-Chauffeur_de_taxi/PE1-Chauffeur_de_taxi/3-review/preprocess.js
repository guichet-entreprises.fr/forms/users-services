var cerfaFields = {};
//etatCivil

var civNomPrenom = $qp077PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp077PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp077PE1.etatCivil.identificationDeclarant.prenomDeclarant;

cerfaFields['nomNaissanceDeclarant']            = $qp077PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp077PE1.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['nomUsageDeclarant']                 = $qp077PE1.etatCivil.identificationDeclarant.nomUsage;
cerfaFields['prenomsDeclarant']                 = $qp077PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['lieuNaissanceDeclarant']  			 = $qp077PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['paysNaissanceDeclarant']            =  $qp077PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationaliteDeclarant']              = $qp077PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissanceDeclarant']             = $qp077PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;
cerfaFields['departementNaissanceDeclarant']      = $qp077PE1.etatCivil.identificationDeclarant.departementNaissance;
cerfaFields['nomPrenomPereDeclarant']            = $qp077PE1.etatCivil.identificationDeclarant.nomPrenomPereDeclarant;
cerfaFields['nomPrenomMereDeclarant']            = $qp077PE1.etatCivil.identificationDeclarant.nomPrenomMereDeclarant;

//adresse personnelle
cerfaFields['numeroNomRueAdresseDeclarant'] 	= $qp077PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant;
cerfaFields['villeAdresseDeclarant']       		= $qp077PE1.adresse.adresseContact.villeAdresseDeclarant;
cerfaFields['codePostalAdresseDeclarant']       = $qp077PE1.adresse.adresseContact.codePostalAdresseDeclarant;
cerfaFields['telephoneAdresseDeclarant']      	= $qp077PE1.adresse.adresseContact.telephoneAdresseDeclarant; 
cerfaFields['courrielDeclarant']          		= $qp077PE1.adresse.adresseContact.mailAdresseDeclarant;

//Permis de conduire

cerfaFields['numeroPermisConduire']             =$qp077PE1.permisDeConduire.permisDeConduire.numeroPermisConduire;
cerfaFields['lieuDelivrancePermisConduire']     =$qp077PE1.permisDeConduire.permisDeConduire.lieuDelivrancePermis;
cerfaFields['dateDelivrancePermisConduire']     =$qp077PE1.permisDeConduire.permisDeConduire.dateDelivrancePermis;

//signature
cerfaFields['dateSignature']                	= $qp077PE1.signature.signature.dateSignature;
cerfaFields['signatureCoche']           		= $qp077PE1.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp077PE1.signature.signature.lieuSignature;
cerfaFields['civiliteNomPrenomDeclarant']		= $qp077PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp077PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp077PE1.etatCivil.identificationDeclarant.prenomDeclarant;



/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
 
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp085PE3.signature.signature.lieuSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */

var finalDoc = nash.doc //
	.load('models/Courrier au premier dossier v2.1 GQ.pdf') //
	.apply({
		date: $qp077PE1.signature.signature.dateSignature,
		autoriteHabilitee :"Préfecture de département",
		demandeContexte : "Demande de reconnaissance de qualifications professionnelles en vue d'un libre établissement pour l'exercice de la profession de chauffeur de taxi ",
		civiliteNomPrenom : civNomPrenom
	});

//finalDoc.append(accompDoc.save('courrier.pdf'));
/*
 * Ajout du cerfa
 */
var cerfaDoc = nash.doc //
    .load('models/demande_de_carte_professionnelle_de_conducteur_de_taxi.pdf') //
    .apply(cerfaFields);

finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
    fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */
appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPermis);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCertimedicaltaxi);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPreventionsecours);
appendPj($attachmentPreprocess.attachmentPreprocess.pjTitres);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);
appendPj($attachmentPreprocess.attachmentPreprocess.pjPhoto);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);


/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Conducteur_de_taxi_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Chauffeur de taxi - Demande de reconnaissance de qualifications professionnelles',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande d\'habilitation pour exercer la profession de chauffeur de taxi',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});
