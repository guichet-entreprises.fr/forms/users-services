var cerfaFields = {};

//Etat Civil

cerfaFields['madameEtatCivil']                                                         = ($qp009PE1.information.etatCivil.civilite =='Madame');
cerfaFields['monsieurEtatCivil']                                                       = ($qp009PE1.information.etatCivil.civilite =='Monsieur');
cerfaFields['nomUsageEtatCivil']                                                       = $qp009PE1.information.etatCivil.nomUsage;
cerfaFields['nomEtatCivil1']                                                           = $qp009PE1.information.etatCivil.nomNaissance;
cerfaFields['prenomEtatCivil1']                                                        = $qp009PE1.information.etatCivil.prenom;
cerfaFields['dateNaissanceEtatCivil']                                                  = $qp009PE1.information.etatCivil.dateNaissance;
cerfaFields['lieuNaissanceEtatCivil']                                                  = $qp009PE1.information.etatCivil.villeNaissance;
cerfaFields['paysNaissanceEtatCivil']                                                  = $qp009PE1.information.etatCivil.paysNaissance;

cerfaFields['numeroLibelleAdresseDeclarant']                                           = $qp009PE1.coordonneesGroup.coordonnees.numeroLibelleAdresse;

cerfaFields['complementAdresseDeclarant']                                              = $qp009PE1.coordonneesGroup.coordonnees.complementAdresse;
cerfaFields['codePostalAdresseDeclarant']                                              = $qp009PE1.coordonneesGroup.coordonnees.codePostalAdresse;
cerfaFields['villeAdresseDeclarant']                                                   = $qp009PE1.coordonneesGroup.coordonnees.villeAdresse;
cerfaFields['telephoneMobileAdresseDeclarant']                                         = $qp009PE1.coordonneesGroup.coordonnees.telephoneAdresse;
cerfaFields['mailAdresseDeclarant']                                                    = $qp009PE1.coordonneesGroup.coordonnees.courrielAdresse;
cerfaFields['distributionCourrier']                                                    = $qp009PE1.coordonneesGroup.coordonnees.distributionCourrierAdresse;
cerfaFields['hebergeur']                                                               = (($qp009PE1.coordonneesGroup.heberger.civilite !=null)?($qp009PE1.coordonneesGroup.heberger.civilite):" ")+ ' ' + (($qp009PE1.coordonneesGroup.heberger.nomHebergeur !=null)?($qp009PE1.coordonneesGroup.heberger.nomHebergeur):" ") + ' ' + (($qp009PE1.coordonneesGroup.heberger.prenomHebergeur !=null)?($qp009PE1.coordonneesGroup.heberger.prenomHebergeur): " ");

cerfaFields['lieuSignature']                                                           = $qp009PE1.finSaisie.certifie.lieuSignature;
cerfaFields['dateSignature']                                                           = $qp009PE1.finSaisie.certifie.dateSignature;
cerfaFields['attesteHonneur']                                                          = $qp009PE1.finSaisie.certifie.attesteHonneur;
cerfaFields['agentRecherchesPrivees']                                                  = true;



var cerfa = pdf.create('models/demande_CP.pdf', cerfaFields);

var cerfaPdf = pdf.save('Agent de transport de fonds - Première Déclaration de libre établissement.pdf', cerfa);
return spec.create({
    id : 'review',
    label : 'Agent de transport de fonds - Première Déclaration de libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Formulaire de première déclaration de libre établissement pour la profession d\'agent de transport de fonds.',
            description : 'Voici le formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
