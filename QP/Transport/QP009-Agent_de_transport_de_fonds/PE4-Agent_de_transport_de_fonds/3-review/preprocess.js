var cerfaFields = {};

//Etat Civil


cerfaFields['nub']                                                                     = $qp009PE4.information.etatCivil.nub;
cerfaFields['nomUsage']                                                                = $qp009PE4.information.etatCivil.nomUsage;

cerfaFields['nomNaissance']                                                            = $qp009PE4.information.etatCivil.nomNaissance;
cerfaFields['prenomNaissance']                                                         = $qp009PE4.information.etatCivil.prenom;
cerfaFields['dateNaissance']                                                           = $qp009PE4.information.etatCivil.dateNaissance;
cerfaFields['villeNaissance']                                                          = $qp009PE4.information.etatCivil.villeNaissance;
cerfaFields['paysNaissance']                                                           = $qp009PE4.information.etatCivil.paysNaissance;

cerfaFields['numeroLibelleAdresseDeclarant']                                           = $qp009PE4.coordonneesGroup.coordonnees.numeroLibelleAdresse;

cerfaFields['complementAdresseDeclarant']                                              = $qp009PE4.coordonneesGroup.coordonnees.complementAdresse;
cerfaFields['codePostalAdresseDeclarant']                                              = $qp009PE4.coordonneesGroup.coordonnees.codePostalAdresse;
cerfaFields['villeAdresseDeclarant']                                                   = $qp009PE4.coordonneesGroup.coordonnees.villeAdresse;
cerfaFields['telephoneMobileAdresseDeclarant']                                         = $qp009PE4.coordonneesGroup.coordonnees.telephoneAdresse;
cerfaFields['mailAdresseDeclarant']                                                    = $qp009PE4.coordonneesGroup.coordonnees.courrielAdresse;
cerfaFields['distributionCourrier']                                                    = $qp009PE4.coordonneesGroup.coordonnees.distributionCourrierAdresse;

cerfaFields['hebergeur']                                                               = (($qp009PE4.coordonneesGroup.heberger.civilite !=null)?($qp009PE4.coordonneesGroup.heberger.civilite):" ") + ' ' + (($qp009PE4.coordonneesGroup.heberger.nomHebergeur !=null)?($qp009PE4.coordonneesGroup.heberger.nomHebergeur):" ") + ' ' + (($qp009PE4.coordonneesGroup.heberger.prenomHebergeur !=null)?($qp009PE4.coordonneesGroup.heberger.prenomHebergeur):" ");

cerfaFields['lieuSignature']                                                           = $qp009PE4.finSaisie.certifie.lieuSignature;
cerfaFields['dateSignature']                                                           = $qp009PE4.finSaisie.certifie.dateSignature;
cerfaFields['attesteHonneur']                                                          = $qp009PE4.finSaisie.certifie.attesteHonneur;


var cerfa = pdf.create('models/formulaire renouvellement CP.pdf', cerfaFields);

var cerfaPdf = pdf.save('Agent de transport de fonds - Renouvellement de la déclaration de libre établissement.pdf', cerfa);

return spec.create({
    id : 'review',
    label : 'Agent de transport de fonds - Renouvellement de la déclaration de libre établissement.',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Formulaire de renouvellement de carte professionnelle pour la profession d\'agent de transport de fonds.',
            description : 'Voici le formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
