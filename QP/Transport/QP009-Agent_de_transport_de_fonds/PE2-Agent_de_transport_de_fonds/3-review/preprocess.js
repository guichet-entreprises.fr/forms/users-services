var cerfaFields = {};

//Etat Civil

cerfaFields['madameEtatCivil']                                                         = ($qp009PE2.information.etatCivil.civilite =='Madame');
cerfaFields['monsieurEtatCivil']                                                       = ($qp009PE2.information.etatCivil.civilite =='Monsieur');
cerfaFields['nomUsageEtatCivil']                                                       = $qp009PE2.information.etatCivil.nomUsage;
cerfaFields['nomEtatCivil1']                                                           = $qp009PE2.information.etatCivil.nomNaissance;
cerfaFields['prenomEtatCivil1']                                                        = $qp009PE2.information.etatCivil.prenom;
cerfaFields['dateNaissanceEtatCivil']                                                  = $qp009PE2.information.etatCivil.dateNaissance;
cerfaFields['lieuNaissanceEtatCivil']                                                  = $qp009PE2.information.etatCivil.villeNaissance;
cerfaFields['paysNaissanceEtatCivil']                                                  = $qp009PE2.information.etatCivil.paysNaissance;

cerfaFields['numeroLibelleAdresseDeclarant']                                           = $qp009PE2.coordonneesGroup.coordonnees.numeroLibelleAdresse;

cerfaFields['complementAdresseDeclarant']                                              = $qp009PE2.coordonneesGroup.coordonnees.complementAdresse;
cerfaFields['codePostalAdresseDeclarant']                                              = $qp009PE2.coordonneesGroup.coordonnees.codePostalAdresse;
cerfaFields['villeAdresseDeclarant']                                                   = $qp009PE2.coordonneesGroup.coordonnees.villeAdresse;
cerfaFields['telephoneMobileAdresseDeclarant']                                         = $qp009PE2.coordonneesGroup.coordonnees.telephoneAdresse;
cerfaFields['mailAdresseDeclarant']                                                    = $qp009PE2.coordonneesGroup.coordonnees.courrielAdresse;
cerfaFields['distributionCourrier']                                                    = $qp009PE2.coordonneesGroup.coordonnees.distributionCourrierAdresse;

cerfaFields['lieuSignature']                                                           = $qp009PE2.finSaisie.certifie.lieuSignature;
cerfaFields['dateSignature']                                                           = $qp009PE2.finSaisie.certifie.dateSignature;
cerfaFields['attesteHonneur']                                                          = $qp009PE2.finSaisie.certifie.attesteHonneur;
cerfaFields['agentRecherchesPrivees']                                                  = true;


cerfaFields['hebergeur']                                                               = (($qp009PE2.coordonneesGroup.heberger.civilite != null) ?($qp009PE2.coordonneesGroup.heberger.civilite): " ") + ' ' +(($qp009PE2.coordonneesGroup.heberger.nomHebergeur !=null)?($qp009PE2.coordonneesGroup.heberger.nomHebergeur):" ")+ ' ' + (($qp009PE2.coordonneesGroup.heberger.prenomHebergeur !=null)?($qp009PE2.coordonneesGroup.heberger.prenomHebergeur):" ");

var cerfa = pdf.create('models/demande_CP.pdf', cerfaFields);

var cerfaPdf = pdf.save('Agent de transport de fonds - Première Déclaration de libre établissement.pdf', cerfa);
return spec.create({
    id : 'review',
    label : 'Agent de transport de fonds - Première Déclaration de libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'lpsAutor',
            label : 'Formulaire de première déclaration de libre établissement pour la profession d\'agent de transport de fonds.',
            description : 'Voici le formulaire obtenu à partir des données saisies :',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
