var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']          	    = $qp078PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp078PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp078PE2.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp078PE2.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp078PE2.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp078PE2.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp078PE2.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp078PE2.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp078PE2.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp078PE2.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp078PE2.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp078PE2.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp078PE2.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp078PE2.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp078PE2.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp078PE2.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp078PE2.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp078PE2.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp078PE2.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp078PE2.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		= $qp078PE2.etatCivil.identificationDeclarant.civilite + ' ' + $qp078PE2.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp078PE2.etatCivil.identificationDeclarant.prenomDeclarant;


var cerfa = pdf.create('models/courrier INPI LE V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Conducteur routier professionnel de véhicules poids lourds.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Conducteur routier professionnel de véhicules poids lourds - Renouvellement demande de carte de qualification de conducteur',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de renouvellement de demande de carte de qualification de conducteur pour  la profession de conducteur routier professionnel de véhicules poids lourds.',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
