var cerfaFields = {};



//etatCivil
cerfaFields['M']                        = ($qp078PE3.etatCivil.identificationDeclarant.civilite=='Monsieur');
cerfaFields['Mme']                      = ($qp078PE3.etatCivil.identificationDeclarant.civilite=='Madame');

cerfaFields['nomDeclarant']             = $qp078PE3.etatCivil.identificationDeclarant.nomDeclarant;
cerfaFields['prenomDeclarant']          = $qp078PE3.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['lieuNaissance']  	        = $qp078PE3.etatCivil.identificationDeclarant.lieuNaissanceDeclarant;
cerfaFields['dateNaissance']         	= $qp078PE3.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//Coordonnées d'expédition

cerfaFields['numeroRueDeclarant']       = $qp078PE3.adresse.adresseContact.numeroRueDeclarant;
cerfaFields['rueDeclarant'] 	        = $qp078PE3.adresse.adresseContact.numeroLibelleAdresseDeclarant ;
cerfaFields['codePostal']       	    = $qp078PE3.adresse.adresseContact.codePostalAdresseDeclarant;
cerfaFields['villeDeclarant1']          = $qp078PE3.adresse.adresseContact.villeAdresseDeclarant;
cerfaFields['paysDeclarant']            = $qp078PE3.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobileDeclarant'] = $qp078PE3.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['mailDeclarant']          	= $qp078PE3.adresse.adresseContact.mailAdresseDeclarant;

//reglement
cerfaFields['numeroCarteDisparue']      = $qp078PE3.carteDisparue.carteDisparue.numeroCarteDisparue;

cerfaFields['cheque']                   = Value('id').of($qp078PE3.reglement.reglement.cheque).contains('cheque1')?true:' ';
cerfaFields['Check Box18']              = Value('id').of($qp078PE3.reglement.reglement.cheque).contains('reglementCFA')?true:' ';
cerfaFields['Check Box19']              = Value('id').of($qp078PE3.reglement.reglement.cheque).contains('autre')?true:' ';

cerfaFields['numeroCheque']             = (($qp078PE3.reglement.reglement.numeroCheque !=null)?($qp078PE3.reglement.reglement.numeroCheque):' ');
cerfaFields['autre']                    = (($qp078PE3.reglement.reglement.autreMoyen !=null)?($qp078PE3.reglement.reglement.autreMoyen):' ');
cerfaFields['reglementCFA']             = (($qp078PE3.reglement.reglement.reglementCFA1 !=null)?($qp078PE3.reglement.reglement.reglementCFA1):' ');

//signature
cerfaFields['dateSignature']            = $qp078PE3.signature.signature.dateSignature;
cerfaFields['signature']           		= $qp078PE3.signature.signature.signature;
cerfaFields['lieu']                     = '';
cerfaFields['declare']                  = $qp078PE3.signature.signature.declare;





var cerfa = pdf.create('models/remplacement_cqc_declaration_vol_ou_perte.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Conducteur routier professionnel de véhicules poids lourds - Demande de remplacement à l’identique de la CQC.pdf', cerfa); //Nom du fichier en sortie



return spec.create({
    id : 'review',
    label : 'Conducteur routier professionnel de véhicules poids lourds - Demande de remplacement à l’identique de la CQC',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du formulaire',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande de remplacement à l’identique de la CQC pour la profession de conducteur routier professionnel de véhicules poids lourds',
            description : 'Formulaire obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
