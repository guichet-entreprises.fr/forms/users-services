var cerfaFields = {};
//etatCivil

cerfaFields['civiliteNomPrenom']          	    = $qp078PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp078PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp078PE1.etatCivil.identificationDeclarant.prenomDeclarant;
cerfaFields['villePaysNaissance']  			 	= $qp078PE1.etatCivil.identificationDeclarant.lieuNaissanceDeclarant + ', ' + $qp078PE1.etatCivil.identificationDeclarant.paysNaissanceDeclarant;
cerfaFields['nationalite']                		= $qp078PE1.etatCivil.identificationDeclarant.nationaliteDeclarant;
cerfaFields['dateNaissance']             		= $qp078PE1.etatCivil.identificationDeclarant.dateNaissanceDeclarant;

//adresse personnelle
cerfaFields['adresse'] 							= $qp078PE1.adresse.adresseContact.numeroLibelleAdresseDeclarant + ($qp078PE1.adresse.adresseContact.complementAdresseDeclarant != null ? ', ' + $qp078PE1.adresse.adresseContact.complementAdresseDeclarant : ' ');
cerfaFields['villePays']       					= ($qp078PE1.adresse.adresseContact.codePostalAdresseDeclarant != null ? $qp078PE1.adresse.adresseContact.codePostalAdresseDeclarant + ' ' : '') + $qp078PE1.adresse.adresseContact.villeAdresseDeclarant + ', ' + $qp078PE1.adresse.adresseContact.paysAdresseDeclarant;
cerfaFields['telephoneMobile']      			= $qp078PE1.adresse.adresseContact.telephoneMobileAdresseDeclarant; 
cerfaFields['telephoneFixe']          			= $qp078PE1.adresse.adresseContact.telephoneAdresseDeclarant;
cerfaFields['courriel']          				= $qp078PE1.adresse.adresseContact.mailAdresseDeclarant;


//signature
cerfaFields['date']                				= $qp078PE1.signature.signature.dateSignature;
cerfaFields['signature']           				= $qp078PE1.signature.signature.signature;
cerfaFields['lieuSignature']                  	= $qp078PE1.signature.signature.lieuSignature + ', le ';
cerfaFields['civiliteNomPrenomSignature']		=$qp078PE1.etatCivil.identificationDeclarant.civilite + ' ' + $qp078PE1.etatCivil.identificationDeclarant.nomDeclarant + ' ' + $qp078PE1.etatCivil.identificationDeclarant.prenomDeclarant;


var cerfa = pdf.create('models/courrier INPI LE V3.pdf', cerfaFields); //Chemin vers lequel il va chercher le CERFA
var cerfaPdf = pdf.save('Conducteur routier professionnel de véhicules poids lourds.pdf', cerfa); //Nom du fichier en sortie


return spec.create({
    id : 'review',
    label : 'Conducteur routier professionnel de véhicules poids lourds - Première demande de carte de qualification de conducteur',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du courrier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Formulaire de demande d\'habilitation pour exercer la profession de conducteur routier professionnel de véhicules poids lourds.',
            description : 'Courrier obtenu à partir des données saisies',
            type : 'FileReadOnly',
            value : [ cerfaPdf ]
        }) ]
    }) ]
});
