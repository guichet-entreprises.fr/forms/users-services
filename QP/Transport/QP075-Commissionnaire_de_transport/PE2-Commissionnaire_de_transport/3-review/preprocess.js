var cerfaFields = {};

//Etat civil

cerfaFields['civiliteMadame']                       = Value('id').of($qp075PE2.etatCivil.etatCivilDeclarant.civilite).eq('civiliteMadame') ? true : false;
cerfaFields['civiliteMonsieur']                     = Value('id').of($qp075PE2.etatCivil.etatCivilDeclarant.civilite).eq('civiliteMonsieur') ? true : false;
cerfaFields['dateNaissanceDeclarant']               = $qp075PE2.etatCivil.etatCivilDeclarant.dateNaissanceDeclarant;
cerfaFields['nomDeNaissanceDeclarant']              = $qp075PE2.etatCivil.etatCivilDeclarant.nomDeNaissanceDeclarant;
cerfaFields['prenomsDeclarant']                     = $qp075PE2.etatCivil.etatCivilDeclarant.prenomsDeclarant;
cerfaFields['lieuNaissanceDeclarant']               = $qp075PE2.etatCivil.etatCivilDeclarant.lieuNaissanceDeclarant;
cerfaFields['nomMarital']                           = $qp075PE2.etatCivil.etatCivilDeclarant.nomMarital;
cerfaFields['nationaliteDeclarant']                 = $qp075PE2.etatCivil.etatCivilDeclarant.nationaliteDeclarant;
cerfaFields['nomEtatPourLesAutres']                 = $qp075PE2.etatCivil.etatCivilDeclarant.nomEtatPourLesAutres;
cerfaFields['numeroDepartementEnFrance']            = $qp075PE2.etatCivil.etatCivilDeclarant.numeroDepartementEnFrance;

//Coordonnées
cerfaFields['numeroVoie']                           = $qp075PE2.coordonnees.coordonneesDeclarant.numeroVoie;
cerfaFields['extension']                            = $qp075PE2.coordonnees.coordonneesDeclarant.extension;
cerfaFields['typeDeVoie']                           = $qp075PE2.coordonnees.coordonneesDeclarant.typeDeVoie;
cerfaFields['nomDeVoie']                            = $qp075PE2.coordonnees.coordonneesDeclarant.nomDeVoie;
cerfaFields['codePostal']                           = $qp075PE2.coordonnees.coordonneesDeclarant.codePostal;
cerfaFields['localite']                             = $qp075PE2.coordonnees.coordonneesDeclarant.localite;
cerfaFields['boitePostale']                         = $qp075PE2.coordonnees.coordonneesDeclarant.boitePostale;
cerfaFields['pays']                                 = $qp075PE2.coordonnees.coordonneesDeclarant.pays;
cerfaFields['numeroDeTelephone']                    = $qp075PE2.coordonnees.coordonneesDeclarant.numeroDeTelephone;
cerfaFields['numeroDeTelecopie']                    = $qp075PE2.coordonnees.coordonneesDeclarant.numeroDeTelecopie;
cerfaFields['adresseElectronique']                  = $qp075PE2.coordonnees.coordonneesDeclarant.adresseElectronique;

//Souhaite obtenir

cerfaFields['capaciteTransportMarchandises']			= false;
cerfaFields['capaciteTransportMarchandisesMayotte']		= false;
cerfaFields['capaciteTransportPersonnes']				= false;
cerfaFields['capaciteTransportPersonnesOutreMer']		= false;
cerfaFields['capaciteTransportLegerMarchandises']		= false;
cerfaFields['capaciteTransportMaxiNeufPersonnes']		= false;
cerfaFields['capaciteDeCommissionnaireDeTransport'] 	= true;

//Engagement et signature
cerfaFields['dateDeclaration']                      = $qp075PE2.signature.egagementSignature.dateDeclaration;
cerfaFields['lieuDeclaration']                      = $qp075PE2.signature.egagementSignature.lieuDeclaration;
cerfaFields['certificationSurHonneur']              = $qp075PE2.signature.egagementSignature.certificationSurHonneur;
cerfaFields['signatureDeclarant']                   = $qp075PE2.signature.egagementSignature.certificationSurHonneur;
cerfaFields['engagementDeLaSignature']              = $qp075PE2.etatCivil.etatCivilDeclarant.civilite + ' ' + $qp075PE2.etatCivil.etatCivilDeclarant.nomDeNaissanceDeclarant + ' ' + $qp075PE2.etatCivil.etatCivilDeclarant.prenomsDeclarant;
//cerfaFields['engagementDeLaSignature']              = $qp075PE2.signature.engagementDeLaSignature;

/*
 * Chargement du courrier d'accompagnement à destination du déclarant
 */
/*
var finalDoc = nash.doc //
	.load('models/courrier_RE_dossierfinal.pdf') //
	.apply({
		numDossier: nash.record.description().recordUid,
		date: $qp130PE1.signature.signature.dateSignature,
		civiliteNomPrenom: civNomPrenom
	});
 */
 
/*
 * Chargement du courrier d'accompagnement à destination de l'AC
 */
/*
var accompDoc = nash.doc //
	.load('models/Courrier au premier dossier v1.6 LE.pdf') //
	.apply({
		date: $qp130PE1.signature.signature.dateSignature
	});

finalDoc.append(accompDoc.save('courrier.pdf'));
*/
/*
 * Ajout du cerfa
 */
var finalDoc = nash.doc //
	.load('models/commissionnaire_de_transport.pdf') //
	.apply(cerfaFields);

//finalDoc.append(cerfaDoc.save('cerfa.pdf'));



function appendPj(fld) {
	fld.forEach(function (elm) {
        finalDoc.append(elm);
    });
}

/*
 * Ajout des PJs
 */

appendPj($attachmentPreprocess.attachmentPreprocess.pjID);
appendPj($attachmentPreprocess.attachmentPreprocess.pjJustifDomicile);
appendPj($attachmentPreprocess.attachmentPreprocess.pjDiplomes);
appendPj($attachmentPreprocess.attachmentPreprocess.pjExerciceActivite);
appendPj($attachmentPreprocess.attachmentPreprocess.pjAttestationFrancais);
appendPj($attachmentPreprocess.attachmentPreprocess.pjCasier);

/*
 * Enregistrement du fichier (en mémoire)
 */
var finalDocItem = finalDoc.save('Commissionnaire_transport_RQP.pdf');

/*
 * Persistance des données obtenues
 */

return spec.create({
    id : 'review',
    label : 'Commissionnaire de transport - demande de reconnaissance de qualifications professionnelles en vue d\'un libre établissement',
    groups : [ spec.createGroup({
        id : 'generated',
        label : 'Génération du dossier',
        data : [ spec.createData({
            id : 'formulaire',
            label : 'Demande de reconnaissances de qualifications professionnelles en vue d\'un libre établissement pour l\'exercice de la profession de commissionnaire de transport.',
            description : 'Voici le dossier de demande, comprenant le formulaire complété à partir des données saisies, les pièces jointes et un courrier d\'accompagnement. Veuillez vérifier l\'exactitude des informations que vous pouvez modifier si besoin en revenant sur les pages de saisie.',
            type : 'FileReadOnly',
            value : [ finalDocItem ]
        }) ]
    }) ]
});